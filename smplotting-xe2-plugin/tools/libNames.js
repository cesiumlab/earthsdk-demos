const config = {
    namespace: 'XE2',
    moduleName: 'smplotting-xe2-plugin',
    libNames: [
        "smplotting-xe2-plugin-main",
        "smplotting-xe2-plugin",
    ]
};

module.exports = { config };