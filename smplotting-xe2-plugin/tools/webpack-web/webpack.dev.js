const merge = require('webpack-merge');
const common = require('./webpack.common.js');
const webpack = require('webpack');
const CopyWebpackPlugin = require('copy-webpack-plugin');

function getProxy(ports) {
  const proxy = {};
  for (let port of ports) {
    const pathRewrite = {};
    pathRewrite[`^/static/bigData/${port}`] = '';
    proxy[`/static/bigData/${port}`] = {
      target: `http://localhost:${port}`,
      secure: false,
      changeOrigin: true,
      pathRewrite,
    }
  }

  return proxy;
}

const proxy = getProxy([8091, 8092, 8093, 8094, 8095]);

module.exports = merge(common, {
  // devtool: 'eval-source-map',
  devtool: 'source-map',
  plugins: [
    new CopyWebpackPlugin([
    ]),
    new webpack.HotModuleReplacementPlugin(),
  ],
  devServer: {
    hot: true,
    proxy,
    port: 8194,
    allowedHosts: 'all'
  },
  mode: 'development',
});