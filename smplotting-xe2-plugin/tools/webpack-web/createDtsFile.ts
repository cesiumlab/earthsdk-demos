// 声明文件中的 @/xr-utils必须替换掉，否则发布成npm包以后就读取不了了 vtxf 20220221

// 注意这个程序主要是为了处理声明文件的，不是处理js文件本身的。js文件中 @/xxx 都会被翻译成 ../xxx，这个是webpack自己操作的，externals中配置的。

import fs from 'fs';
import path from 'path';
import yargs from 'yargs'
const { config } = require('../libNames');
const libNames = config.libNames;
const namespace = config.namespace;

function writeFile(namespace: string, outputFileDir: string, names: string[]) {
    const upperCaseNameSpace = namespace.toUpperCase();

    fs.writeFileSync(`${outputFileDir}/${namespace}.d.ts`, `
interface ${upperCaseNameSpace} {}
declare var ${namespace}: ${upperCaseNameSpace};
    `);

    for (let name of names) {
        const underlineName = name.replace(/-/g, '_');
        const umdFileStr = `
import * as _umd_${namespace}_${underlineName} from '../dist-node/${name}';
export as namespace _umd_${namespace}_${underlineName};
export = _umd_${namespace}_${underlineName};
        `;
        fs.writeFileSync(`${outputFileDir}/_umd_${namespace}_${underlineName}.d.ts`, umdFileStr);

        const dtsFileStr = `
/// <reference path="./_umd_${namespace}_${underlineName}.d.ts" />
/// <reference path="./${namespace}.d.ts" />

interface ${upperCaseNameSpace} {
    "${name}": typeof _umd_${namespace}_${underlineName};
}
        `;
        fs.writeFileSync(`${outputFileDir}/${name}.d.ts`, dtsFileStr);
    }
}

function processCommand() {
    const argv = yargs
    .option('o', {
      alias: 'output',
      demandOption: true,
      describe: '输出文件路径',
      type: 'string'
    })
    .parse(process.argv);

    if (argv instanceof Promise) {
        throw new Error(`argv instanceof Promise`);
    }

    const outputFileDir = argv.o;

    writeFile(namespace, outputFileDir, libNames);
}

processCommand();
