const merge = require('webpack-merge');
const common = require('./webpack.common.js');
const path = require('path');
const CopyWebpackPlugin = require('copy-webpack-plugin');

const r = merge(common, {
  plugins: [
    new CopyWebpackPlugin([
    ]),
  ],
  optimization: {
    minimize: true,
  },
  mode: "production",
  output: {
    path: path.resolve(__dirname, '../../dist-web-minified'),
  },
});

r.module.rules.unshift({
  test: /\.ts$/,
  loader: './tools/webpack-web/shader-minify-loader',
});

module.exports = r;
