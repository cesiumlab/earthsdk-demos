module.exports = function (context) {
    function replacer(match) {
        return match
        .replace(/^\s*\/\/.*\r\n/mg, '') // 去掉整行注释
        .replace(/\/\/.*/g, '') // 去掉部分注释
        .replace(/[\r\n]*/g, '') // 去掉回车符
        .replace(/\s+/g, ' ') // 多个连续空格合并为一个
        .replace(/\s*([^_\w\s]+)\s*/g, '$1'); // 去掉非单词字符周围的所有空格
    }
    // const r = context.replace(/[\r\n]+[.]*\/\/xx\sshader-begin(.*?)\/\/xx\sshader-end[.]*[\r\n]+/sg, replacer); // 找到 //xx shader-begin 和 //xx shader-end之间的内容
    const r = context.replace(/[^\\'"`]?(['"`])\/\/#!glsl[\s;\r\n](.*?)[^\\'"`]?\1/sg, replacer); // 找到 //#!glsl 字符串中的内容

    return r;
}