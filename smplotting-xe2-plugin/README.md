# smplotting-czmtoy-plugin

## 概览
czmtoy插件

## web项目

1. 调试
命令：npm run dev-web  
浏览器打开页面，效果如下：  
![](mkimages/2021-07-17-14-10-31.png)

2. 打包
执行命令：npm run build-web  
打包目录: ./dist-web  

## node项目
1. 通过vscode调试源码  
![](mkimages/2021-07-17-14-15-14.png)

2. 通过vscode调试发行程序  
![](mkimages/2021-07-17-14-14-06.png)

![](mkimages/2021-07-17-14-14-47.png)

3. 打包使用
执行命令：npm run build-node  
打包目录：./dist-node  
