import { getCurrentScriptPath } from "./getCurrentScriptPath";

function preloadLibs() {
    const currentScriptPath = getCurrentScriptPath();
    const scriptName = 'smplotting-xe2-plugin.js';
    const xe2RootPath = currentScriptPath.slice(0, -scriptName.length);

    // @ts-ignore
    if (!window.SuperMap) {
        document.write(`\
            <script src="${xe2RootPath}js/SuperMapSymbols/libs/SuperMap.Include.js"></script>
            <script src="${xe2RootPath}js/SuperMapSymbols/iClientJavaScriptPlottingSymbols.js"></script>
        `);
    }

    // 发布时使用
    document.write(`\
        <script src="${xe2RootPath}smplotting-xe2-plugin-main.js"></script>
    `);
}

preloadLibs();