import { SMGeoPlottingBase } from "../SMGeoPlottingBase";

/**
 * 多边形  
 * 使用三个或三个以上控制点直接创建多边形  
 */

export class SMGeoPolygonEx extends SMGeoPlottingBase<SuperMap.Geometry.GeoPolygonEx> {
    static readonly type = this.register('SMGeoPolygonEx', this, { chsName: 'SM多边形', tags: ['SMSymbols'], description: "通过SuperMap的JavaScript态势标绘实现的多边形。" });
    get typeName() { return 'SMGeoPolygonEx'; }
    constructor(id?: string) {
        super(SuperMap.Geometry.GeoPolygonEx, undefined, id);
    }
}
