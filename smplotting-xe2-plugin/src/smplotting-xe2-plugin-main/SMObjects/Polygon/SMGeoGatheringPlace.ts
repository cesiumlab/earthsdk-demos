import { SMGeoPlottingBase } from "../SMGeoPlottingBase";

/**
 * 聚集地符号  
 * 使用两个控制点直接创建聚集地符号  
 */

export class SMGeoGatheringPlace extends SMGeoPlottingBase<SuperMap.Geometry.GeoGatheringPlace> {
    static readonly type = this.register('SMGeoGatheringPlace', this, { chsName: '聚集区', tags: ['SMSymbols'], description: "通过SuperMap的JavaScript态势标绘实现的区域绘制。" });
    get typeName() { return 'SMGeoGatheringPlace' }
    constructor(id?: string) {
        super(SuperMap.Geometry.GeoGatheringPlace, 2, id);
    }
}
