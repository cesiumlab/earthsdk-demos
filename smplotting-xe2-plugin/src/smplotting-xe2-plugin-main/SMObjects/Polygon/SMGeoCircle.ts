import { SMGeoPlottingBase } from "../SMGeoPlottingBase";

/**
 * 圆  
 * 使用圆心和圆上一点绘制出一个圆  
 */

export class SMGeoCircle extends SMGeoPlottingBase<SuperMap.Geometry.GeoCircle> {
    static readonly type: string = this.register('SMGeoCircle', this, { chsName: 'SM圆形', tags: ['SMSymbols'], description: "通过SuperMap的JavaScript态势标绘实现的圆形。" });
    get typeName() { return 'SMGeoCircle'; }
    constructor(id?: string) {
        super(SuperMap.Geometry.GeoCircle, 2, id);
    }
}
