import { SMGeoPlottingBase } from "../SMGeoPlottingBase";

/**
 * 手绘面
 * 由鼠标移动轨迹而形成的手绘面
 */

export class SMGeoFreePolygon extends SMGeoPlottingBase<SuperMap.Geometry.GeoFreePolygon> {
    static readonly type = this.register('SMGeoFreePolygon', this, { chsName: '手绘面', tags: ['SMSymbols'], description: "通过SuperMap的JavaScript态势标绘实现的多边形。" });
    get typeName() { return 'SMGeoFreePolygon' }
    constructor(id?: string) {
        super(SuperMap.Geometry.GeoFreePolygon, undefined, id);
    }
}
