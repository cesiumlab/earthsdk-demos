import { GroupProperty, NumberProperty } from "xbsj-xe2/dist-node/xe2-base-objects";
import { extendClassProps, PartialWithUndefinedReactivePropsToNativeProps, ReactivePropsToNativePropsAndChanged } from "xbsj-xe2/dist-node/xe2-base-utils";
import { SMGeoPlottingBase } from "../SMGeoPlottingBase";

/**
 * 弓形符号  
 * 使用三点绘制弓形符号  
*/
export class SMGeoLune extends SMGeoPlottingBase<SuperMap.Geometry.GeoLune> {
    static readonly type = this.register('SMGeoLune', this, { chsName: '弓形', tags: ['SMSymbols'], description: "通过SuperMap的JavaScript态势标绘实现的弓形符号。" });
    get typeName() { return 'SMGeoLune'; }

    static override defaults = {
        ...SMGeoPlottingBase.defaults,
        sides: 720
    }

    constructor(id?: string) {
        super(SuperMap.Geometry.GeoLune, 2, id);
        const updateSides = () => {
            this._smGeoPlotting.sides = this.sides ?? SMGeoLune.defaults.sides;
            this.updateFinalPositions();
        }
        updateSides();
        this.dispose(this.sidesChanged.disposableOn(updateSides));
    }

    override getProperties(language?: string) {
        return [
            ...super.getProperties(language),
            new GroupProperty('通用', '通用', [
                new NumberProperty('点密度', '弓形上圆弧的点密度。默认为720，即每隔1°绘制两个点', false, false, [this, 'sides'], SMGeoLune.defaults.sides),
            ]),

        ]
    }
}

export namespace SMGeoLune {
    export const createDefaultProps = () => {
        return {
            sides: undefined as number | undefined,
            ...SMGeoPlottingBase.createDefaultProps(),
        };
    };
}
extendClassProps(SMGeoLune.prototype, SMGeoLune.createDefaultProps);
export interface SMGeoLune extends ReactivePropsToNativePropsAndChanged<ReturnType<typeof SMGeoLune.createDefaultProps>> { }
type JsonType = PartialWithUndefinedReactivePropsToNativeProps<ReturnType<typeof SMGeoLune.createDefaultProps> & { type: string; }>;

