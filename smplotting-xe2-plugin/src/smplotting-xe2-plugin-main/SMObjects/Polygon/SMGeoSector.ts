import { BooleanProperty, GroupProperty } from "xbsj-xe2/dist-node/xe2-base-objects";
import { PartialWithUndefinedReactivePropsToNativeProps, ReactivePropsToNativePropsAndChanged, extendClassProps } from "xbsj-xe2/dist-node/xe2-base-utils";
import { SMGeoPlottingBase } from "../SMGeoPlottingBase";

/**
 * 扇形  
 * 使用圆心和圆上两点绘制出一个扇形  
*/
export class SMGeoSector extends SMGeoPlottingBase<SuperMap.Geometry.GeoSector> {
    static readonly type = this.register('SMGeoSector', this, { chsName: 'SM扇形', tags: ['SMSymbols'], description: "通过SuperMap的JavaScript态势标绘实现的扇形。" });
    get typeName() { return 'SMGeoSector'; }
    constructor(id?: string) {
        super(SuperMap.Geometry.GeoSector, 3, id);
        const update = () => {
            const bool = this.direction ?? false;
            this._smGeoPlotting.direction = bool ? 1 : -1;
            this.updateFinalPositions();
        }
        update();
        this.dispose(this.directionChanged.disposableOn(update));
    }

    override getProperties(language?: string) {
        return [
            ...super.getProperties(language),
            new GroupProperty('通用', '通用', [
                new BooleanProperty('扇形的绘制方向', '扇形的绘制方向，顺时针绘制true，或逆时针绘制（值为false）。默认为false，即逆时针绘制。', true, false, [this, 'direction'], false),
            ]),
        ]
    }
}

export namespace SMGeoSector {
    export const createDefaultProps = () => {
        return {
            direction: undefined as boolean | undefined,
            ...SMGeoPlottingBase.createDefaultProps(),
        };
    };
    export const propNames = Object.keys(createDefaultProps());
}
extendClassProps(SMGeoSector.prototype, SMGeoSector.createDefaultProps);
export interface SMGeoSector extends ReactivePropsToNativePropsAndChanged<ReturnType<typeof SMGeoSector.createDefaultProps>> { }
type JsonType = PartialWithUndefinedReactivePropsToNativeProps<ReturnType<typeof SMGeoSector.createDefaultProps> & { type: string; }>;
