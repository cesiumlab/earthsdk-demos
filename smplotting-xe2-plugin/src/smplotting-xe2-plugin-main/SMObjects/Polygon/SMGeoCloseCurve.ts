import { SMGeoPlottingBase } from "../SMGeoPlottingBase";

/**
 * 闭合曲线  
 * 使用三个或三个以上控制点直接创建闭合曲线  
*/

export class SMGeoCloseCurve extends SMGeoPlottingBase<SuperMap.Geometry.GeoCloseCurve> {
    static readonly type = this.register('SMGeoCloseCurve', this, { chsName: '闭合曲线', tags: ['SMSymbols'], description: "通过SuperMap的JavaScript态势标绘实现的闭合曲线。" });
    get typeName() { return 'SMGeoCloseCurve' }
    constructor(id?: string) {
        super(SuperMap.Geometry.GeoCloseCurve, undefined, id);
    }
}
