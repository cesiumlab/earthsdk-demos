import { SMGeoPlottingBase } from "../SMGeoPlottingBase";

/**
 * 圆角矩形  
 * 使用两个控制点直接创建圆角矩形  
 */

export class SMGeoRectangle extends SMGeoPlottingBase<SuperMap.Geometry.GeoRectangle> {
    static readonly type = this.register('SMGeoRectangle', this, { chsName: 'SM矩形', tags: ['SMSymbols'], description: "通过SuperMap的JavaScript态势标绘实现的矩形。" });
    get typeName() { return 'SMGeoRectangle'; }
    constructor(id?: string) {
        super(SuperMap.Geometry.GeoRectangle, 2, id);
    }
}

