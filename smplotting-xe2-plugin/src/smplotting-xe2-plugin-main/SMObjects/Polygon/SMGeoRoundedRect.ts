import { SMGeoPlottingBase } from "../SMGeoPlottingBase";

/**
 * 矩形
 * 使用两个控制点直接创建矩形
 */

export class SMGeoRoundedRect extends SMGeoPlottingBase<SuperMap.Geometry.GeoRoundedRect> {
    static readonly type = this.register('SMGeoRoundedRect', this, { chsName: 'SM圆角矩形', tags: ['SMSymbols'], description: "通过SuperMap的JavaScript态势标绘实现的圆角矩形。" });
    get typeName() { return 'SMGeoRoundedRect'; }
    constructor(id?: string) {
        super(SuperMap.Geometry.GeoRoundedRect, 2, id);
    }
}
