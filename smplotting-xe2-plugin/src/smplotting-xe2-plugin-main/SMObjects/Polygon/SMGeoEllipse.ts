import { GroupProperty, NumberProperty } from "xbsj-xe2/dist-node/xe2-base-objects";
import { PartialWithUndefinedReactivePropsToNativeProps, ReactivePropsToNativePropsAndChanged, extendClassProps } from "xbsj-xe2/dist-node/xe2-base-utils";
import { SMGeoPlottingBase } from "../SMGeoPlottingBase";

/**
 * 椭圆  
 * 使用椭圆上的两个点绘制出一个椭圆  
*/

export class SMGeoEllipse extends SMGeoPlottingBase<SuperMap.Geometry.GeoEllipse> {
    static readonly type: string = this.register('SMGeoEllipse', this, { chsName: 'SM椭圆', tags: ['SMSymbols'], description: "通过SuperMap的JavaScript态势标绘实现的椭圆。" });
    get typeName() { return 'SMGeoEllipse'; }
    constructor(id?: string) {
        super(SuperMap.Geometry.GeoEllipse, 2, id);
        const updateSides = () => {
            this._smGeoPlotting.sides = this.sides ?? 360;
            this.updateFinalPositions();
        }
        updateSides();
        this.dispose(this.sidesChanged.disposableOn(updateSides));
    }

    override getProperties(language?: string) {
        return [
            ...super.getProperties(language),
            new GroupProperty('通用', '通用', [
                new NumberProperty('控制数量', '椭圆控制点点的数量，要求必须大于2。默认的值为360', false, false, [this, 'sides'], 360),
            ]),

        ]
    }
}

export namespace SMGeoEllipse {
    export const createDefaultProps = () => {
        return {
            sides: undefined as number | undefined,
            ...SMGeoPlottingBase.createDefaultProps(),
        };
    };
    export const propNames = Object.keys(createDefaultProps());
}
extendClassProps(SMGeoEllipse.prototype, SMGeoEllipse.createDefaultProps);
export interface SMGeoEllipse extends ReactivePropsToNativePropsAndChanged<ReturnType<typeof SMGeoEllipse.createDefaultProps>> { }
type JsonType = PartialWithUndefinedReactivePropsToNativeProps<ReturnType<typeof SMGeoEllipse.createDefaultProps> & { type: string; }>;
