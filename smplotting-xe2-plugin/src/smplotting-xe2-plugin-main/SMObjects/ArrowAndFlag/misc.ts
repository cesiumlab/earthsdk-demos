import { SMGeoMultiLinePlottingBase } from "../SMGeoMultiLinePlottingBase";
import { SMGeoPlottingBase } from "../SMGeoPlottingBase";

export class SMGeoBezierCurveArrow extends SMGeoMultiLinePlottingBase<SuperMap.Geometry.GeoCardinalCurveArrow> {
    static readonly type = this.register('SMGeoBezierCurveArrow', this, { chsName: '贝塞尔曲线箭头', tags: ['SMSymbols'], description: "通过SuperMap的JavaScript态势标绘实现的贝塞尔曲线箭头。" });
    get typeName() { return 'SMGeoBezierCurveArrow'; }
    constructor(id?: string) {
        super(SuperMap.Geometry.GeoCardinalCurveArrow, undefined, id);

    }
}

export class SMGeoCardinalCurveArrow extends SMGeoMultiLinePlottingBase<SuperMap.Geometry.GeoCardinalCurveArrow> {
    static readonly type = this.register('SMGeoCardinalCurveArrow', this, { chsName: 'Cardinal曲线箭头', tags: ['SMSymbols'], description: "通过SuperMap的JavaScript态势标绘实现的Cardinal曲线箭头。" });
    get typeName() { return 'SMGeoCardinalCurveArrow'; }
    constructor(id?: string) {
        super(SuperMap.Geometry.GeoCardinalCurveArrow, undefined, id);
    }
}

export class SMGeoCurveFlag extends SMGeoPlottingBase<SuperMap.Geometry.GeoCurveFlag> {
    static readonly type = this.register('SMCzmGeoCurveFlag', this, { chsName: '曲线旗标', tags: ['SMSymbols'], description: "通过SuperMap的JavaScript态势标绘实现的曲线旗标。" });
    get typeName() { return 'SMCzmGeoCurveFlag'; }
    constructor(id?: string) {
        super(SuperMap.Geometry.GeoCurveFlag, 3, id);
    }

    protected override _updateFinalPositions() {
        const smGeoPlotting = this._smGeoPlotting;
        let finalPositions: [number, number, number][] | undefined;
        do {
            if (!this.positions || this.positions.length <= 2) {
                break;
            }
            const height = this.positions[0][2];
            const points = this.positions.map(e => new SuperMap.Geometry.Point(e[0], e[1]));
            smGeoPlotting.setControlPoint(points);
            const finalPoints = smGeoPlotting.getVertices();
            finalPositions = finalPoints.map(e => [e.x, e.y, height] as [number, number, number]);

            if (finalPositions.length < 3) {
                console.warn(`finalPositions.length < 3`);
            }

            // @ts-ignore
            smGeoPlotting.calculateBounds();
            // @ts-ignore
            const { left, right, top, bottom } = smGeoPlotting.components[0].bounds as { left: number, right: number, top: number, bottom: number };
            const longitude = left * (1.05) + right * (-0.05);
            const lastFinalPosition = finalPositions[finalPositions.length - 1];
            finalPositions.push([longitude, lastFinalPosition[1], lastFinalPosition[2]]);
            finalPositions.unshift([longitude, finalPositions[0][1], finalPositions[0][2]]);
        } while (false);
        this._finalPositions.value = finalPositions;
    };
}

export class SMGeoDiagonalArrow extends SMGeoPlottingBase<SuperMap.Geometry.GeoDiagonalArrow> {
    static readonly type = this.register('SMGeoDiagonalArrow', this, { chsName: '斜箭头', tags: ['SMSymbols'], description: "通过SuperMap的JavaScript态势标绘实现的斜箭头。" });
    get typeName() { return 'SMGeoDiagonalArrow'; }
    constructor(id?: string) {
        super(SuperMap.Geometry.GeoDiagonalArrow, undefined, id);
    }

    // /**
    //  * APIMethod: getRatio
    //  * 获取箭头长宽比值，默认为6倍
    //  */
    //  getRatio(): number;
    //  /**
    //   * APIMethod: setRatio
    //   * 设置箭头长宽比值，默认为6倍
    //   *
    //   * Parameters:
    //   * value - {Number} 箭头长宽比值
    //   */
    //  setRatio(value: number): void;
    // /**
    //  * Property: _tailRatio
    //  * 箭头起始两个节点长度与箭头尾巴的比值
    //  */
    //  _tailRatio: number;
}

export class SMGeoDoubleArrow extends SMGeoPlottingBase<SuperMap.Geometry.GeoDoubleArrow> {
    static readonly type = this.register('SMGeoDoubleArrow', this, { chsName: '双箭头', tags: ['SMSymbols'], description: "通过SuperMap的JavaScript态势标绘实现的双箭头。" });
    get typeName() { return 'SMGeoDoubleArrow'; }
    constructor(id?: string) {
        super(SuperMap.Geometry.GeoDoubleArrow, 4, id);
    }
}

export class SMGeoDoveTailDiagonalArrow extends SMGeoPlottingBase<SuperMap.Geometry.GeoDoveTailDiagonalArrow> {
    static readonly type = this.register('SMGeoDoveTailDiagonalArrow', this, { chsName: '燕尾斜箭头', tags: ['SMSymbols'], description: "通过SuperMap的JavaScript态势标绘实现的燕尾斜箭头。" });
    get typeName() { return 'SMGeoDoveTailDiagonalArrow'; }
    constructor(id?: string) {
        super(SuperMap.Geometry.GeoDoveTailDiagonalArrow, undefined, id);
    }
    // /**
    //  * APIMethod: getRatio
    //  * 获取箭头长宽比值，默认为6倍
    //  */
    //  getRatio(): number;
    //  /**
    //   * APIMethod: setRatio
    //   * 设置箭头长宽比值，默认为6倍
    //   *
    //   * Parameters:
    //   * value - {Number} 箭头长宽比值
    //   */
    //  setRatio(value: number): void;
    // /**
    //  * Property: _tailRatio
    //  * 箭头起始两个节点长度与箭头尾巴的比值
    //  */
    //  _tailRatio: number;
}

export class SMGeoDoveTailStraightArrow extends SMGeoPlottingBase<SuperMap.Geometry.GeoDoveTailStraightArrow> {
    static readonly type = this.register('SMGeoDoveTailStraightArrow', this, { chsName: '燕尾直箭头', tags: ['SMSymbols'], description: "通过SuperMap的JavaScript态势标绘实现的燕尾直箭头。" });
    get typeName() { return 'SMGeoDoveTailStraightArrow'; }
    constructor(id?: string) {
        super(SuperMap.Geometry.GeoDoveTailStraightArrow, undefined, id);
    }

    // /**
    //  * APIMethod: getRatio
    //  * 获取箭头长宽比值，默认为6倍
    //  */
    //  getRatio(): number;
    //  /**
    //   * APIMethod: setRatio
    //   * 设置箭头长宽比值，默认为6倍
    //   *
    //   * Parameters:
    //   * value - {Number} 箭头长宽比值
    //   */
    //  setRatio(value: number): void;
    // /**
    //  * Property: _tailRatio
    //  * 箭头起始两个节点长度与箭头尾巴的比值
    //  */
    //  _tailRatio: number;
}

export class SMGeoParallelSearch extends SMGeoMultiLinePlottingBase<SuperMap.Geometry.GeoParallelSearch> {
    static readonly type = this.register('SMGeoParallelSearch', this, { chsName: '平行搜寻区', tags: ['SMSymbols'], description: "通过SuperMap的JavaScript态势标绘实现的平行搜寻区。" });
    get typeName() { return 'SMGeoParallelSearch'; }
    constructor(id?: string) {
        super(SuperMap.Geometry.GeoParallelSearch, undefined, id);
    }
}

export class SMGeoPolylineArrow extends SMGeoMultiLinePlottingBase<SuperMap.Geometry.GeoPolylineArrow> {
    static readonly type = this.register('SMGeoPolylineArrow', this, { chsName: '折线箭头', tags: ['SMSymbols'], description: "通过SuperMap的JavaScript态势标绘实现的折线箭头。" });
    get typeName() { return 'SMGeoPolylineArrow'; }
    constructor(id?: string) {
        super(SuperMap.Geometry.GeoPolylineArrow, undefined, id);
    }
}

export class SMGeoRectFlag extends SMGeoPlottingBase<SuperMap.Geometry.GeoRectFlag> {
    static readonly type = this.register('SMGeoRectFlag', this, { chsName: '直角旗标', tags: ['SMSymbols'], description: "通过SuperMap的JavaScript态势标绘实现的直角旗标。" });
    get typeName() { return 'SMGeoRectFlag'; }
    constructor(id?: string) {
        super(SuperMap.Geometry.GeoRectFlag, 2, id);
    }

    protected override _updateFinalPositions() {
        const smGeoPlotting = this._smGeoPlotting;
        let finalPositions: [number, number, number][] | undefined;
        do {
            if (!this.positions || this.positions.length < 2) {
                break;
            }
            const height = this.positions[0][2];
            const points = this.positions.map(e => new SuperMap.Geometry.Point(e[0], e[1]));
            smGeoPlotting.setControlPoint(points);
            const finalPoints = smGeoPlotting.getVertices();
            finalPositions = finalPoints.map(e => [e.x, e.y, height] as [number, number, number]);

            if (finalPositions.length !== 5) {
                console.warn(`finalPositions.length !== 5`);
            }

            // TODO(vtxf): 临时的写法，无法穿越边界
            const longitude = finalPositions[2][0] * 0.02 + finalPositions[3][0] * 0.98;
            const newFinalPosition3 = [longitude, finalPositions[3][1], finalPositions[3][2]] as [number, number, number];
            const newFinalPosition4 = [longitude, finalPositions[4][1], finalPositions[4][2]] as [number, number, number];
            finalPositions.splice(3, 1, newFinalPosition3, newFinalPosition4);
        } while (false);
        this._finalPositions.value = finalPositions;
    };
}

export class SMGeoSectorSearch extends SMGeoMultiLinePlottingBase<SuperMap.Geometry.GeoSectorSearch> {
    static readonly type = this.register('SMGeoSectorSearch', this, { chsName: '扇形搜寻区', tags: ['SMSymbols'], description: "通过SuperMap的JavaScript态势标绘实现的扇形搜寻区。" });
    get typeName() { return 'SMGeoSectorSearch'; }
    constructor(id?: string) {
        super(SuperMap.Geometry.GeoSectorSearch, 2, id);
    }
}

export class SMGeoStraightArrow extends SMGeoPlottingBase<SuperMap.Geometry.GeoStraightArrow> {
    static readonly type = this.register('SMGeoStraightArrow', this, { chsName: '直箭头', tags: ['SMSymbols'], description: "通过SuperMap的JavaScript态势标绘实现的直箭头。" });
    get typeName() { return 'SMGeoStraightArrow'; }
    constructor(id?: string) {
        super(SuperMap.Geometry.GeoStraightArrow, undefined, id);
    }
    // /**
    //  * APIMethod: getRatio
    //  * 获取箭头长宽比值，默认为6倍
    //  */
    // getRatio(): number;
    // /**
    //  * APIMethod: setRatio
    //  * 设置箭头长宽比值，默认为6倍
    //  *
    //  * Parameters:
    //  * value - {Number} 箭头长宽比值
    //  */
    // setRatio(value: number): void;
}

export class SMGeoTriangleFlag extends SMGeoPlottingBase<SuperMap.Geometry.GeoTriangleFlag> {
    static readonly type = this.register('SMGeoTriangleFlag', this, { chsName: '三角旗标', tags: ['SMSymbols'], description: "通过SuperMap的JavaScript态势标绘实现的三角旗标。" });
    get typeName() { return 'SMGeoTriangleFlag'; }
    constructor(id?: string) {
        super(SuperMap.Geometry.GeoTriangleFlag, 2, id);
    }

    protected override _updateFinalPositions() {
        const smGeoPlotting = this._smGeoPlotting;
        let finalPositions: [number, number, number][] | undefined;
        do {
            if (!this.positions || this.positions.length < 2) {
                break;
            }
            const height = this.positions[0][2];
            const points = this.positions.map(e => new SuperMap.Geometry.Point(e[0], e[1]));
            smGeoPlotting.setControlPoint(points);
            const finalPoints = smGeoPlotting.getVertices();
            finalPositions = finalPoints.map(e => [e.x, e.y, height] as [number, number, number]);

            if (finalPositions.length !== 4) {
                console.warn(`finalPositions.length !== 4`);
            }

            // TODO(vtxf): 临时的写法，无法穿越边界
            const longitude = finalPositions[1][0] * 0.02 + finalPositions[2][0] * 0.98;
            const newFinalPosition3 = [longitude, finalPositions[2][1], finalPositions[2][2]] as [number, number, number];
            const newFinalPosition4 = [longitude, finalPositions[3][1], finalPositions[3][2]] as [number, number, number];
            finalPositions.splice(2, 1, newFinalPosition3, newFinalPosition4);
        } while (false);
        this._finalPositions.value = finalPositions;
    };
}
