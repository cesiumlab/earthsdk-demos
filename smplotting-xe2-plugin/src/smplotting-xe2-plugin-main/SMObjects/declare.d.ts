declare global {
    namespace SuperMap {
        export class Geometry {
            destroy(): void;
        }

        namespace Geometry {
            export class Point extends Geometry {
                constructor(longitude: number, latitude: number);
                x: number;
                y: number;
            }

            export class Polygon {
                destroy(): void;
            }

            export class Collection extends Geometry {
                components: Geometry[];
            }

            export class MultiPoint extends Collection {

            }

            export class Curve extends MultiPoint {

            }

            export class LineString extends Curve {
                bounds: any;
                id: string;
            }

            export class LinearRing extends LineString {
            }

            export class GeoPlotting extends Polygon {
                constructor(points: Point[]);
                initialize(points: Point[]): void;
                setControlPoint(points: Point[]): void;
                getVertices(): Point[];
            }

            export class GeoLinePlotting extends LineString {
                constructor(points: Point[]);
                initialize(points: Point[]): void;
                setControlPoint(points: Point[]): void;
                getVertices(): Point[];
            }

            export class MultiLineString {
                destroy(): void;
            }

            export class GeoMultiLinePlotting extends LineString {
                constructor(points: Point[]);
                initialize(points: Point[]): void;
                setControlPoint(points: Point[]): void;
                getVertices(): Point[];
            }

            // Polygon    
            export class GeoCircle extends GeoPlotting { 
            }
            export class GeoCloseCurve extends GeoPlotting {
            }
            export class GeoEllipse extends GeoPlotting {
                sides: number;
            }
            export class GeoFreePolygon extends GeoPlotting {
            }
            export class GeoGatheringPlace extends GeoPlotting {
            }
            export class GeoLune extends GeoPlotting {
                sides: number;
            }
            export class GeoPolygonEx extends GeoPlotting {
            }
            export class GeoRectangle extends GeoPlotting {
            }
            export class GeoRoundedRect extends GeoPlotting {
            }
            export class GeoSector extends GeoPlotting {
                direction: -1 | 1;
            }

            // ArrowAndFlag
            export class GeoBezierCurveArrow extends GeoMultiLinePlotting {
            }
            export class GeoCardinalCurveArrow extends GeoMultiLinePlotting {
            }
            export class GeoCurveFlag extends GeoPlotting {
            }
            export class GeoDiagonalArrow extends GeoPlotting {
                /**
                 * APIMethod: getRatio
                 * 获取箭头长宽比值，默认为6倍
                 */
                 getRatio(): number;
                 /**
                  * APIMethod: setRatio
                  * 设置箭头长宽比值，默认为6倍
                  *
                  * Parameters:
                  * value - {Number} 箭头长宽比值
                  */
                 setRatio(value: number): void;
                /**
                 * Property: _tailRatio
                 * 箭头起始两个节点长度与箭头尾巴的比值
                 */
                 _tailRatio: number;
            }
            export class GeoDoubleArrow extends GeoPlotting {
            }
            export class GeoDoveTailDiagonalArrow extends GeoPlotting {
                /**
                 * APIMethod: getRatio
                 * 获取箭头长宽比值，默认为6倍
                 */
                 getRatio(): number;
                 /**
                  * APIMethod: setRatio
                  * 设置箭头长宽比值，默认为6倍
                  *
                  * Parameters:
                  * value - {Number} 箭头长宽比值
                  */
                 setRatio(value: number): void;
                /**
                 * Property: _tailRatio
                 * 箭头起始两个节点长度与箭头尾巴的比值
                 */
                 _tailRatio: number;
            }
            export class GeoDoveTailStraightArrow extends GeoPlotting {
                /**
                 * APIMethod: getRatio
                 * 获取箭头长宽比值，默认为6倍
                 */
                 getRatio(): number;
                 /**
                  * APIMethod: setRatio
                  * 设置箭头长宽比值，默认为6倍
                  *
                  * Parameters:
                  * value - {Number} 箭头长宽比值
                  */
                 setRatio(value: number): void;
                /**
                 * Property: _tailRatio
                 * 箭头起始两个节点长度与箭头尾巴的比值
                 */
                 _tailRatio: number;
            }
            export class GeoParallelSearch extends GeoMultiLinePlotting {
            }
            export class GeoPolylineArrow extends GeoMultiLinePlotting {
            }
            export class GeoRectFlag extends GeoPlotting {
            }
            export class GeoSectorSearch extends GeoMultiLinePlotting {
            }
            export class GeoStraightArrow extends GeoPlotting {
                /**
                 * APIMethod: getRatio
                 * 获取箭头长宽比值，默认为6倍
                 */
                getRatio(): number;
                /**
                 * APIMethod: setRatio
                 * 设置箭头长宽比值，默认为6倍
                 *
                 * Parameters:
                 * value - {Number} 箭头长宽比值
                 */
                setRatio(value: number): void;
            }
            export class GeoTriangleFlag extends GeoPlotting {
            }

            // Line
            export class GeoArc extends GeoLinePlotting {}
            export class GeoBezierCurve2 extends GeoLinePlotting {}
            export class GeoBezierCurve3 extends GeoLinePlotting {}
            export class GeoBezierCurveN extends GeoLinePlotting {}
            export class GeoCardinalCurve extends GeoLinePlotting {}
            export class GeoFreeline extends GeoLinePlotting {}
            export class GeoPolyline extends GeoLinePlotting {}
        }
    }
}
 
export {};