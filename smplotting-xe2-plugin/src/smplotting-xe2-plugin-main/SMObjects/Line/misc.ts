import { SMGeoLinePlottingBase } from "../SMGeoLinePlottingBase";

export class SMGeoArc extends SMGeoLinePlottingBase<SuperMap.Geometry.GeoArc> {
    static readonly type = this.register('SMGeoArc', this, { chsName: '圆弧', tags: ['SMSymbols'], description: "通过SuperMap的JavaScript态势标绘实现的圆弧。" });
    get typeName() { return 'SMGeoArc'; }
    constructor(id?: string) {
        super(SuperMap.Geometry.GeoArc, 3, id);

    }
}

export class SMGeoBezierCurve2 extends SMGeoLinePlottingBase<SuperMap.Geometry.GeoBezierCurve2> {
    static readonly type = this.register('SMGeoBezierCurve2', this, { chsName: '贝塞尔2次曲线', tags: ['SMSymbols'], description: "通过SuperMap的JavaScript态势标绘实现的贝塞尔2次曲线。" });
    get typeName() { return 'SMGeoBezierCurve2'; }
    constructor(id?: string) {
        super(SuperMap.Geometry.GeoBezierCurve2, 3, id);
    }
}

export class SMGeoBezierCurve3 extends SMGeoLinePlottingBase<SuperMap.Geometry.GeoBezierCurve3> {
    static readonly type = this.register('SMGeoBezierCurve3', this, { chsName: '贝塞尔3次曲线', tags: ['SMSymbols'], description: "通过SuperMap的JavaScript态势标绘实现的贝塞尔3次曲线。" });
    get typeName() { return 'SMGeoBezierCurve3'; }
    constructor(id?: string) {
        super(SuperMap.Geometry.GeoBezierCurve3, 4, id);
    }
}

export class SMGeoBezierCurveN extends SMGeoLinePlottingBase<SuperMap.Geometry.GeoBezierCurveN> {
    static readonly type = this.register('SMGeoBezierCurveN', this, { chsName: '贝塞尔N次曲线', tags: ['SMSymbols'], description: "通过SuperMap的JavaScript态势标绘实现的贝塞尔N次曲线。" });
    get typeName() { return 'SMGeoBezierCurveN'; }
    constructor(id?: string) {
        super(SuperMap.Geometry.GeoBezierCurveN, undefined, id);
    }
}

export class SMGeoCardinalCurve extends SMGeoLinePlottingBase<SuperMap.Geometry.GeoCardinalCurve> {
    static readonly type = this.register('SMGeoCardinalCurve', this, { chsName: 'Cardinal曲线', tags: ['SMSymbols'], description: "通过SuperMap的JavaScript态势标绘实现的Cardinal曲线。" });
    get typeName() { return 'SMGeoCardinalCurve'; }
    constructor(id?: string) {
        super(SuperMap.Geometry.GeoCardinalCurve, undefined, id);
    }
}

export class SMGeoFreeline extends SMGeoLinePlottingBase<SuperMap.Geometry.GeoFreeline> {
    static readonly type = this.register('SMGeoFreeline', this, { chsName: '自由线', tags: ['SMSymbols'], description: "通过SuperMap的JavaScript态势标绘实现的自由线。" });
    get typeName() { return 'SMGeoFreeline'; }
    constructor(id?: string) {
        super(SuperMap.Geometry.GeoFreeline, undefined, id);
    }
}

export class SMGeoPolyline extends SMGeoLinePlottingBase<SuperMap.Geometry.GeoPolyline> {
    static readonly type = this.register('SMGeoPolyline', this, { chsName: '折线', tags: ['SMSymbols'], description: "通过SuperMap的JavaScript态势标绘实现的折线。" });
    get typeName() { return 'SMGeoPolyline'; }
    constructor(id?: string) {
        super(SuperMap.Geometry.GeoPolyline, undefined, id);
    }
}
