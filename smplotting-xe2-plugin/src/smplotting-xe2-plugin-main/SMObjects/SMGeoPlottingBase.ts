import { BooleanProperty, ColorProperty, ESSceneObject, FunctionProperty, GeoPolygon, GroupProperty, JsonProperty, NonreactiveJsonStringProperty, Number4Property, NumberProperty, PickedInfo, PointEditing, PositionsEditing, SceneObjectPickedInfo, getGeoJson, setGeoJson } from "xbsj-xe2/dist-node/xe2-base-objects";
import { Event, JsonValue, Listener, PartialWithUndefinedReactivePropsToNativeProps, ReactivePropsToNativePropsAndChanged, extendClassProps, reactArray, reactArrayWithUndefined, reactPositions, track,bind } from "xbsj-xe2/dist-node/xe2-base-utils";
import { createNextAnimateFrame } from "xbsj-xe2/dist-node/xe2-utils";

export abstract class SMGeoPlottingBase<T extends SuperMap.Geometry.GeoPlotting> extends ESSceneObject {
    override get defaultProps() { return SMGeoPlottingBase.createDefaultProps(); }
    get json() { return this._innerGetJson() as JsonType; }
    set json(value: JsonType) { this._innerSetJson(value); }

    get geoJson() {
        return getGeoJson('MultiPoint', this.positions, this.name);
    }
    set geoJson(value: JsonValue) {
        const objValue = setGeoJson(value)
        this.name = objValue.name
        this.positions = objValue.position
    }

    get geoJsonStr() {
        try {
            return JSON.stringify(this.geoJson, undefined, '  ');
        } catch (error) {
            console.error(error);
            return '';
        }
    }
    set geoJsonStr(value: string) {
        if (!value) {
            return;
        }
        try {
            this.geoJson = JSON.parse(value);
        } catch (error) {
            console.error(error);
        }
    }

    private _pickedEvent = this.disposeVar(new Event<[PickedInfo]>());
    get pickedEvent() { return this._pickedEvent; }

    private _flyToEvent = this.disposeVar(new Event<[number | undefined]>());
    get flyToEvent(): Listener<[number | undefined]> { return this._flyToEvent; }
    flyTo(duration?: number) { this._flyToEvent.emit(duration); }

    protected _finalPositions = reactPositions(undefined);
    get finalPositions() { return this._finalPositions.value; }
    get finalPositionsChanged() { return this._finalPositions.changed; }

    protected _smGeoPlotting: T;
    // get maxPointsNum() { return this._maxPointsNum; }

    private _updateFinalPositionsProcessing = this.disposeVar(createNextAnimateFrame());
    updateFinalPositions() { this._updateFinalPositionsProcessing.restartIfNotRunning(); }

    static override defaults = {
        ...ESSceneObject.defaults,
        viewDistanceRange: [1000, 10000, 30000, 60000] as [number, number, number, number],
    }

    private _sPositionsEditing = this.disposeVar(new PositionsEditing([this, 'positions'], true, [this, 'editing'], this.components, this._maxPointsNum));
    get sPositionsEditing() { return this._sPositionsEditing; }

    constructor(
        smGeoPlottingConstructor: new (points: SuperMap.Geometry.Point[]) => SuperMap.Geometry.GeoPlotting,
        private _maxPointsNum?: number,
        id?: string,
        private _needSecondControlPoint = false,
    ) {
        super(id);
        console.log('_maxPointsNum', this._maxPointsNum)

        const smGeoPlotting = this.disposeVar(new smGeoPlottingConstructor([]) as T);
        this._smGeoPlotting = smGeoPlotting;

        this._updateFinalPositions();
        this.dispose(this._updateFinalPositionsProcessing.completeEvent.disposableOn(() => this._updateFinalPositions()));
        this.dispose(this.positionsChanged.disposableOn(() => this.updateFinalPositions()));

        {
            const sMGeoPolygonBase = this;

            const geoPolygon = this.disposeVar(new GeoPolygon());
            this.dispose(sMGeoPolygonBase.components.disposableAdd(geoPolygon));

            this.dispose(track([geoPolygon, 'allowPicking'], [sMGeoPolygonBase, 'allowPicking']));
            this.dispose(geoPolygon.pickedEvent.disposableOn(pickedInfo => {
                if (sMGeoPolygonBase.allowPicking ?? false) {
                    sMGeoPolygonBase.pickedEvent.emit(new SceneObjectPickedInfo(sMGeoPolygonBase, pickedInfo));
                }
            }));

            this.dispose(track([geoPolygon, 'show'], [sMGeoPolygonBase, 'show']));
            this.dispose(track([geoPolygon, 'ground'], [sMGeoPolygonBase, 'ground']));
            this.dispose(track([geoPolygon, 'outline'], [sMGeoPolygonBase, 'outline']));
            this.dispose(track([geoPolygon, 'outlineColor'], [sMGeoPolygonBase, 'outlineColor']));
            this.dispose(track([geoPolygon, 'outlineWidth'], [sMGeoPolygonBase, 'outlineWidth']));
            this.dispose(track([geoPolygon, 'color'], [sMGeoPolygonBase, 'color']));
            this.dispose(track([geoPolygon, 'depth'], [sMGeoPolygonBase, 'depth']));
            this.dispose(track([geoPolygon, 'positions'], [sMGeoPolygonBase, 'finalPositions']));
            this.dispose(track([geoPolygon, 'fill'], [sMGeoPolygonBase, 'fill']));
            this.dispose(track([geoPolygon, 'viewDistanceRange'], [sMGeoPolygonBase, 'viewDistanceRange']));
            this.dispose(track([geoPolygon, 'viewDistanceDebug'], [sMGeoPolygonBase, 'viewDistanceDebug']));
            this.dispose(bind([geoPolygon, 'depthTest'], [sMGeoPolygonBase, 'depthTest']));


            // const positionsEditingRef = this.disposeVar(createPositionsEditingRefForComponent([sMGeoPolygonBase, 'positions'], true, sMGeoPolygonBase.components, sMGeoPolygonBase._maxPointsNum));
            // this.dispose(bind([sMGeoPolygonBase, 'editing'], positionsEditingRef));

            this.dispose(sMGeoPolygonBase.flyToEvent.disposableOn(duration => {
                geoPolygon.flyTo(duration);
            }));

            const points = this.disposeVar(reactArrayWithUndefined<[number, number, number][]>(undefined));
            this.disposeVar(new PointEditing(points, [this, 'pointEditing'], this.components));
            this.dispose(points.changed.disposableOn(() => { this.positions = points.value }));
            this.dispose(this.positionsChanged.disposableOn(() => { points.value = this.positions }));
        }
    }

    protected _updateFinalPositions() {
        const smGeoPlotting = this._smGeoPlotting;
        let finalPositions: [number, number, number][] | undefined;
        do {
            if (!this.positions || this.positions.length < 2) {
                break;
            }
            const height = this.positions[0][2];
            const points = this.positions.map(e => new SuperMap.Geometry.Point(e[0], e[1]));
            smGeoPlotting.setControlPoint(points);
            const finalPoints = smGeoPlotting.getVertices();
            finalPositions = finalPoints.map(e => [e.x, e.y, height] as [number, number, number]);
        } while (false);
        this._finalPositions.value = finalPositions;
    };

    get propNames() {
        return SMGeoPlottingBase.propNames;
    }

    override getProperties(language?: string) {
        return [
            ...super.getProperties(language),
            new GroupProperty('GeoJSON', 'GeoJSON', [
                new NonreactiveJsonStringProperty('geoJson', '生成GeoJSON数据。', false, false, () => this.geoJsonStr, (value: string | undefined) => value && (this.geoJsonStr = value)),
            ]),
            new GroupProperty('通用', '通用', [
                new FunctionProperty("飞入", "飞入", ['number'], (duration: number) => this.flyTo(duration), [1000]),
                new BooleanProperty('是否显示', '是否显示', false, false, [this, 'show']),
                new Number4Property('可视范围控制', '可视范围控制[near0, near1, far1, far0]', true, false, [this, 'viewDistanceRange'], SMGeoPlottingBase.defaults.viewDistanceRange),
                new BooleanProperty('可视距离调试', '可视距离调试', false, false, [this, 'viewDistanceDebug']),
                new BooleanProperty('是否编辑', '是否编辑', false, false, [this, 'editing']),
                new BooleanProperty('单点编辑', '是否开启单点编辑', false, false, [this, 'pointEditing']),
                new BooleanProperty('允许拾取', '是否允许拾取', false, false, [this, 'allowPicking']),
                new JsonProperty('位置数组', '经度纬度高度，度为单位', true, false, [this, 'positions']),
                new BooleanProperty('是否贴地', '是否贴地表绘制', false, false, [this, 'ground']),
                new BooleanProperty('是否显示轮廓线', '是否显示轮廓线.', false, false, [this, 'outline']),
                new ColorProperty('轮廓线颜色', '轮廓线颜色', false, false, [this, 'outlineColor']),
                new NumberProperty('轮廓线宽度', '轮廓线宽度', false, false, [this, 'outlineWidth']),

                new BooleanProperty('是否填充', '是否填充', false, false, [this, 'fill']),
                new ColorProperty('填充颜色', '填充颜色', false, false, [this, 'color']),
                new NumberProperty('厚度', '厚度', false, false, [this, 'depth']),
                new BooleanProperty('是否开启深度检测', 'A boolean Property specifying the visibility.', false, false, [this, 'depthTest']),
                // new NumberProperty('z-index', 'z-index', true, false, [this, 'zIndex'], GeoPolygon.defaults.zIndex),
            ]),
        ]
    }
}

export namespace SMGeoPlottingBase {
    export const createDefaultProps = () => {
        return {
            show: true,
            fill: true,
            allowPicking: false,
            ground: false,
            outline: true,
            outlineColor: reactArray<[number, number, number, number]>([1, 1, 1, 1]),
            outlineWidth: 2,
            color: reactArray<[number, number, number, number]>([1, 1, 1, .5]),
            editing: false,
            pointEditing: false,
            positions: reactPositions(undefined),
            depth: 0,
            depthTest: false, //深度检测
            viewDistanceRange: reactArrayWithUndefined<[number, number, number, number]>(undefined),
            viewDistanceDebug: false,

            ...ESSceneObject.createDefaultProps(),
        }
    };
    export const propNames = Object.keys(createDefaultProps());
}
extendClassProps(SMGeoPlottingBase.prototype, SMGeoPlottingBase.createDefaultProps);
export interface SMGeoPlottingBase<T extends SuperMap.Geometry.GeoPlotting> extends ReactivePropsToNativePropsAndChanged<ReturnType<typeof SMGeoPlottingBase.createDefaultProps>> { }
type JsonType = PartialWithUndefinedReactivePropsToNativeProps<ReturnType<typeof SMGeoPlottingBase.createDefaultProps> & { type: string }>;
