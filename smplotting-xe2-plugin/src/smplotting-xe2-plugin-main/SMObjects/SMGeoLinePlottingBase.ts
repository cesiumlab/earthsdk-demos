import { BooleanProperty, ColorProperty, DashPatternProperty, ESSceneObject, EnumProperty, FunctionProperty, GeoPolyline, GroupProperty, NonreactiveJsonStringProperty, Number4Property, NumberProperty, PickedInfo, PointEditing, PositionsEditing, PositionsProperty, SceneObjectPickedInfo, getGeoJson, setGeoJson, uniquePositions } from "xbsj-xe2/dist-node/xe2-base-objects";
import { Event, JsonValue, Listener, PartialWithUndefinedReactivePropsToNativeProps, ReactivePropsToNativePropsAndChanged, bind, extendClassProps, reactArray, reactArrayWithUndefined, reactPositions, track } from "xbsj-xe2/dist-node/xe2-base-utils";
import { createNextAnimateFrame } from "xbsj-xe2/dist-node/xe2-utils";

export abstract class SMGeoLinePlottingBase<T extends SuperMap.Geometry.GeoLinePlotting> extends ESSceneObject {
    override get defaultProps() { return SMGeoLinePlottingBase.createDefaultProps(); }
    get json() { return this._innerGetJson() as JsonType; }
    set json(value: JsonType) { this._innerSetJson(value); }

    get geoJson() {
        return getGeoJson('MultiPoint', this.positions, this.name);
    }
    set geoJson(value: JsonValue) {
        const objValue = setGeoJson(value)
        this.name = objValue.name
        this.positions = objValue.position
    }

    get geoJsonStr() {
        try {
            return JSON.stringify(this.geoJson, undefined, '  ');
        } catch (error) {
            return '';
        }
    }
    set geoJsonStr(value: string) {
        if (!value) {
            return;
        }
        try {
            this.geoJson = JSON.parse(value);
        } catch (error) {
        }
    }

    private _pickedEvent = this.disposeVar(new Event<[PickedInfo]>());
    get pickedEvent() { return this._pickedEvent; }

    private _flyToEvent = this.disposeVar(new Event<[number | undefined]>());
    get flyToEvent(): Listener<[number | undefined]> { return this._flyToEvent; }
    flyTo(duration?: number) { this._flyToEvent.emit(duration); }

    private _finalPositions = reactPositions(undefined);
    get finalPositions() { return this._finalPositions.value; }
    get finalPositionsChanged() { return this._finalPositions.changed; }

    protected _smGeoLinePlotting: T;
    get maxPointsNum() { return this._maxPointsNum; }

    private _updateFinalPositionsProcessing = this.disposeVar(createNextAnimateFrame());
    updateFinalPositions() { this._updateFinalPositionsProcessing.restartIfNotRunning(); }

    private _sPositionsEditing = this.disposeVar(new PositionsEditing([this, 'positions'], false, [this, 'editing'], this.components, this.maxPointsNum));
    get sPositionsEditing() { return this._sPositionsEditing; }

    constructor(
        smGeoLinePlottingConstructor: new (points: SuperMap.Geometry.Point[]) => SuperMap.Geometry.GeoLinePlotting,
        private _maxPointsNum?: number,
        id?: string,
    ) {
        super(id);

        const smGeoLinePlotting = this.disposeVar(new smGeoLinePlottingConstructor([]) as T);
        this._smGeoLinePlotting = smGeoLinePlotting;

        this._updateFinalPositions();
        this.dispose(this._updateFinalPositionsProcessing.completeEvent.disposableOn(() => this._updateFinalPositions()));
        this.dispose(this.positionsChanged.disposableOn(() => this.updateFinalPositions()));

        {
            const sMGeoLineBase = this;
            const geoPolyline = this.disposeVar(new GeoPolyline());
            this.dispose(sMGeoLineBase.components.disposableAdd(geoPolyline));

            this.dispose(track([geoPolyline, 'allowPicking'], [sMGeoLineBase, 'allowPicking']));
            this.dispose(geoPolyline.pickedEvent.disposableOn(pickedInfo => {
                if (sMGeoLineBase.allowPicking ?? false) {
                    sMGeoLineBase.pickedEvent.emit(new SceneObjectPickedInfo(sMGeoLineBase, pickedInfo));
                }
            }));

            this.dispose(track([geoPolyline, 'show'], [sMGeoLineBase, 'show']));
            this.dispose(track([geoPolyline, 'width'], [sMGeoLineBase, 'width']));
            this.dispose(track([geoPolyline, 'ground'], [sMGeoLineBase, 'ground']));
            this.dispose(track([geoPolyline, 'color'], [sMGeoLineBase, 'color']));
            this.dispose(track([geoPolyline, 'hasDash'], [sMGeoLineBase, 'hasDash']));
            this.dispose(track([geoPolyline, 'gapColor'], [sMGeoLineBase, 'gapColor']));
            this.dispose(track([geoPolyline, 'dashLength'], [sMGeoLineBase, 'dashLength']));
            this.dispose(track([geoPolyline, 'dashPattern'], [sMGeoLineBase, 'dashPattern']));
            this.dispose(track([geoPolyline, 'hasArrow'], [sMGeoLineBase, 'hasArrow']));
            this.dispose(track([geoPolyline, 'arcType'], [sMGeoLineBase, 'arcType']));
            this.dispose(track([geoPolyline, 'viewDistanceRange'], [sMGeoLineBase, 'viewDistanceRange']));
            this.dispose(track([geoPolyline, 'viewDistanceDebug'], [sMGeoLineBase, 'viewDistanceDebug']));
            this.dispose(bind([geoPolyline, 'depthTest'], [sMGeoLineBase, 'depthTest']));


            this.dispose(track([geoPolyline, 'positions'], [sMGeoLineBase, 'finalPositions']));

            this.dispose(sMGeoLineBase.flyToEvent.disposableOn(duration => {
                geoPolyline.flyTo(duration);
            }));

            const points = this.disposeVar(reactArrayWithUndefined<[number, number, number][]>(undefined));
            this.disposeVar(new PointEditing(points, [this, 'pointEditing'], this.components));
            this.dispose(points.changed.disposableOn(() => { this.positions = points.value }));
            this.dispose(this.positionsChanged.disposableOn(() => { points.value = this.positions }));
        }
    }

    private _updateFinalPositions() {
        const smGeoPlotting = this._smGeoLinePlotting;
        let finalPositions: [number, number, number][] | undefined;
        do {
            if (!this.positions || this.positions.length < 2) {
                break;
            }
            const height = this.positions[0][2];
            const points = uniquePositions(this.positions).map(e => new SuperMap.Geometry.Point(e[0], e[1]));
            smGeoPlotting.setControlPoint(points);
            const finalPoints = smGeoPlotting.getVertices();
            finalPositions = finalPoints.map(e => [e.x, e.y, height] as [number, number, number]);
        } while (false);
        this._finalPositions.value = finalPositions;
    };

    get propNames() {
        return SMGeoLinePlottingBase.propNames;
    }
    static override defaults = {
        ...ESSceneObject.defaults,
        // show: true,
        // allowPicking: true,
        positions: [],
        // width: 2,
        // ground: false,
        // color: [1, 1, 1, 1] as [number, number, number, number],
        // hasDash: false,
        // gapColor: [0, 0, 0, 0] as [number, number, number, number],
        // dashLength: 16,
        // dashPattern: 255,
        // hasArrow: false,
        // arcType: 'GEODESIC' as 'NONE' | 'GEODESIC' | 'RHUMB',
        // editing: false,
        viewDistanceRange: [1000, 10000, 30000, 60000] as [number, number, number, number],
    }

    override getProperties(language?: string) {
        return [
            ...super.getProperties(language),
            new GroupProperty('通用', '通用', [
                new BooleanProperty('是否显示', 'A boolean Property specifying the visibility.', false, false, [this, 'show']),
                new BooleanProperty('允许拾取', '是否允许拾取', false, false, [this, 'allowPicking']),
                new Number4Property('可视范围控制', '可视范围控制[near0, near1, far1, far0]', true, false, [this, 'viewDistanceRange']),
                new BooleanProperty('可视距离调试', '可视距离调试', false, false, [this, 'viewDistanceDebug']),
                new PositionsProperty('位置数组', '经度纬度高度，度为单位', true, false, [this, 'positions'], SMGeoLinePlottingBase.defaults.positions),
                new NumberProperty('线宽', '线宽', false, false, [this, 'width']),
                new BooleanProperty('是否贴地', '是否贴地.', false, false, [this, 'ground']),
                new ColorProperty('颜色', ' A Property specifying the color.', false, false, [this, 'color']),
                new BooleanProperty('是否为虚线', '是否为虚线.', false, false, [this, 'hasDash']),
                new ColorProperty('间隔颜色', 'A Property specifying the color.', false, false, [this, 'gapColor']),
                new NumberProperty('虚线长度', '虚线长度', false, false, [this, 'dashLength']),
                new DashPatternProperty('虚线图案', '虚线图案', false, false, [this, 'dashPattern']),
                new BooleanProperty('是否带箭头', '是否带箭头.', false, false, [this, 'hasArrow']),
                new BooleanProperty('单点编辑', '是否开启单点编辑', false, false, [this, 'pointEditing']),
                new BooleanProperty('是否编辑', '是否编辑.', false, false, [this, 'editing']),
                new EnumProperty('弧线类型', '弧线类型', false, false, [this, 'arcType'], [['直线', 'NONE'], ['地理直线', 'GEODESIC'], ['地理恒向线', 'RHUMB']]),
                new FunctionProperty("飞入", "飞入", ['number'], (duration: number) => this.flyTo(duration), [1000]),
                new BooleanProperty('是否开启深度检测', 'A boolean Property specifying the visibility.', false, false, [this, 'depthTest']),
                new NonreactiveJsonStringProperty('geoJson', '生成GeoJSON数据。', false, false, () => this.geoJsonStr, (value: string | undefined) => value && (this.geoJsonStr = value)),
            ]),
        ]
    }
}

export namespace SMGeoLinePlottingBase {
    export const createDefaultProps = () => ({
        show: true, // boolean} [show=true] A boolean Property specifying the visibility
        allowPicking: false,
        positions: reactPositions(undefined), // A Property specifying the array of Cartesian3 positions that define the line strip.
        // loop: undefined as boolean | undefined,
        width: 2.0, // undfined时为1.0，A numeric Property specifying the width in pixels.
        ground: false,
        color: reactArray<[number, number, number, number]>([1, 1, 1, 1]), // default [1, 1, 1, 1]
        hasDash: false,
        gapColor: reactArray<[number, number, number, number]>([0, 0, 0, 0]), // default [0, 0, 0, 0]
        dashLength: 16, // default 16
        dashPattern: 255, // default 255
        hasArrow: false,
        arcType: 'GEODESIC' as 'NONE' | 'GEODESIC' | 'RHUMB' ,
        editing: false,
        pointEditing: false,
        depthTest: false, //深度检测

        viewDistanceRange: reactArrayWithUndefined<[number, number, number, number]>(undefined),
        viewDistanceDebug: false,

        ...ESSceneObject.createDefaultProps(),
    });
    export const propNames = Object.keys(createDefaultProps());
}
extendClassProps(SMGeoLinePlottingBase.prototype, SMGeoLinePlottingBase.createDefaultProps);
export interface SMGeoLinePlottingBase<T extends SuperMap.Geometry.GeoLinePlotting> extends ReactivePropsToNativePropsAndChanged<ReturnType<typeof SMGeoLinePlottingBase.createDefaultProps>> { }
type JsonType = PartialWithUndefinedReactivePropsToNativeProps<ReturnType<typeof SMGeoLinePlottingBase.createDefaultProps> & { type: string }>;

