import { BooleanProperty, ColorProperty, DashPatternProperty, ESSceneObject, EnumProperty, FunctionProperty, GeoPolylines, GroupProperty, NonreactiveJsonStringProperty, NumberProperty, PickedInfo, PointEditing, PositionsEditing, PositionsProperty, SceneObjectPickedInfo, getGeoJson, setGeoJson, uniquePositions } from "xbsj-xe2/dist-node/xe2-base-objects";
import { Event, JsonValue, Listener, PartialWithUndefinedReactivePropsToNativeProps, ReactivePropsToNativePropsAndChanged, bind, extendClassProps, reactArray, reactArrayWithUndefined, reactPositions, reactPositionsSet, track } from "xbsj-xe2/dist-node/xe2-base-utils";
import { createNextAnimateFrame } from "xbsj-xe2/dist-node/xe2-utils";

export abstract class SMGeoMultiLinePlottingBase<T extends SuperMap.Geometry.GeoMultiLinePlotting> extends ESSceneObject {
    override get defaultProps() { return SMGeoMultiLinePlottingBase.createDefaultProps(); }
    get json() { return this._innerGetJson() as JsonType; }
    set json(value: JsonType) { this._innerSetJson(value); }

    get geoJson() {
        return getGeoJson('MultiPoint', this.positions, this.name);
    }
    set geoJson(value: JsonValue) {
        const objValue = setGeoJson(value)
        this.name = objValue.name
        this.positions = objValue.position
    }

    get geoJsonStr() {
        try {
            return JSON.stringify(this.geoJson, undefined, '  ');
        } catch (error) {
            return '';
        }
    }
    set geoJsonStr(value: string) {
        if (!value) {
            return;
        }
        try {
            this.geoJson = JSON.parse(value);
        } catch (error) {
        }
    }

    private _pickedEvent = this.disposeVar(new Event<[PickedInfo]>());
    get pickedEvent() { return this._pickedEvent; }

    private _flyToEvent = this.disposeVar(new Event<[number | undefined]>());
    get flyToEvent(): Listener<[number | undefined]> { return this._flyToEvent; }
    flyTo(duration?: number) { this._flyToEvent.emit(duration); }

    private _finalPositionsSet = reactPositionsSet(undefined);
    get finalPositionsSet() { return this._finalPositionsSet.value; }
    get finalPositionsSetChanged() { return this._finalPositionsSet.changed; }

    protected _smGeoLinePlotting: T;
    get maxPointsNum() { return this._maxPointsNum; }

    private _updateFinalPositionsProcessing = this.disposeVar(createNextAnimateFrame());
    updateFinalPositions() { this._updateFinalPositionsProcessing.restartIfNotRunning(); }

    private _sPositionsEditing = this.disposeVar(new PositionsEditing([this, 'positions'], false, [this, 'editing'], this.components, this.maxPointsNum));
    get sPositionsEditing() { return this._sPositionsEditing; }

    constructor(
        smGeoLinePlottingConstructor: new (points: SuperMap.Geometry.Point[]) => SuperMap.Geometry.GeoMultiLinePlotting,
        private _maxPointsNum?: number,
        id?: string,
    ) {
        super(id);

        const smGeoLinePlotting = this.disposeVar(new smGeoLinePlottingConstructor([]) as T);
        this._smGeoLinePlotting = smGeoLinePlotting;

        this._updateFinalPositions();
        this.dispose(this._updateFinalPositionsProcessing.completeEvent.disposableOn(() => this._updateFinalPositions()));
        this.dispose(this.positionsChanged.disposableOn(() => this.updateFinalPositions()));

        {
            const sMGeoMultiLineBase = this;

            const geoPolylines = this.disposeVar(new GeoPolylines());
            this.dispose(sMGeoMultiLineBase.components.disposableAdd(geoPolylines));

            this.dispose(geoPolylines.pickedEvent.disposableOn(pickedInfo => {
                if (sMGeoMultiLineBase.allowPicking ?? false) {
                    sMGeoMultiLineBase.pickedEvent.emit(new SceneObjectPickedInfo(sMGeoMultiLineBase, pickedInfo));
                }
            }));

            this.dispose(track([geoPolylines, 'show'], [sMGeoMultiLineBase, 'show']));
            this.dispose(track([geoPolylines, 'width'], [sMGeoMultiLineBase, 'width']));
            this.dispose(track([geoPolylines, 'ground'], [sMGeoMultiLineBase, 'ground']));
            this.dispose(track([geoPolylines, 'color'], [sMGeoMultiLineBase, 'color']));
            this.dispose(track([geoPolylines, 'hasDash'], [sMGeoMultiLineBase, 'hasDash']));
            this.dispose(track([geoPolylines, 'gapColor'], [sMGeoMultiLineBase, 'gapColor']));
            this.dispose(track([geoPolylines, 'dashLength'], [sMGeoMultiLineBase, 'dashLength']));
            this.dispose(track([geoPolylines, 'dashPattern'], [sMGeoMultiLineBase, 'dashPattern']));
            // this.dispose(track([geoPolylines, 'hasArrow'], [sMGeoMultiLineBase, 'hasArrow']));
            this.dispose(track([geoPolylines, 'arcType'], [sMGeoMultiLineBase, 'arcType']));
            this.dispose(track([geoPolylines, 'allowPicking'], [sMGeoMultiLineBase, 'allowPicking']));

            this.dispose(track([geoPolylines, 'positions'], [sMGeoMultiLineBase, 'finalPositionsSet']));
            this.dispose(bind([geoPolylines, 'depthTest'], [sMGeoMultiLineBase, 'depthTest']));


            // const positionsEditingRef = this.disposeVar(createPositionsEditingRefForComponent([sMGeoMultiLineBase, 'positions'], false, sMGeoMultiLineBase.components, sMGeoMultiLineBase.maxPointsNum));
            // this.dispose(bind([sMGeoMultiLineBase, 'editing'], positionsEditingRef));

            this.dispose(sMGeoMultiLineBase.flyToEvent.disposableOn(duration => {
                geoPolylines.flyTo(duration);
            }));

            const points = this.disposeVar(reactArrayWithUndefined<[number, number, number][]>(undefined));
            this.disposeVar(new PointEditing(points, [this, 'pointEditing'], this.components));
            this.dispose(points.changed.disposableOn(() => { this.positions = points.value }));
            this.dispose(this.positionsChanged.disposableOn(() => { points.value = this.positions }));
        }
    }

    private _updateFinalPositions() {
        const smGeoPlotting = this._smGeoLinePlotting;
        let finalPositionsSet: [number, number, number][][] | undefined;
        do {
            if (!this.positions || this.positions.length < 2) {
                break;
            }
            const height = this.positions[0][2];
            const points = uniquePositions(this.positions).map(e => new SuperMap.Geometry.Point(e[0], e[1]));
            try {
                smGeoPlotting.setControlPoint(points);
            } catch (error) {
                console.error(`smGeoPlotting.setControlPoint(points) error! points: ${points}`);
                smGeoPlotting.components = [];
            }
            finalPositionsSet = (smGeoPlotting.components.filter(e => e instanceof SuperMap.Geometry.LineString) as SuperMap.Geometry.LineString[]).map(e =>
                (e.components.filter(e => e instanceof SuperMap.Geometry.Point) as SuperMap.Geometry.Point[]).map(e =>
                    [e.x, e.y, height] as [number, number, number]
                )
            );
        } while (false);
        this._finalPositionsSet.value = finalPositionsSet;
    };

    get propNames() {
        return SMGeoMultiLinePlottingBase.propNames;
    }

    static override defaults = {
        ...ESSceneObject.defaults,
        // show: true,
        // allowPicking: false,
        positions: [],
        // width: 2,
        // ground: false,
        // color: [1, 1, 1, 1] as [number, number, number, number],
        // hasDash: false,
        // gapColor: [0, 0, 0, 0] as [number, number, number, number],
        // dashLength: 16,
        // dashPattern: 255,
        // // hasArrow: false,
        // arcType: 'GEODESIC' as 'NONE' | 'GEODESIC' | 'RHUMB',
        // editing: false,
    }

    override getProperties(language?: string) {
        return [
            ...super.getProperties(language),
            new GroupProperty('通用', '通用', [
                new BooleanProperty('是否显示', 'A boolean Property specifying the visibility.', false, false, [this, 'show']),
                new BooleanProperty('允许拾取', '是否允许拾取', false, false, [this, 'allowPicking']),
                new PositionsProperty('位置数组', '经度纬度高度，度为单位', true, false, [this, 'positions'], SMGeoMultiLinePlottingBase.defaults.positions),
                new NumberProperty('线宽', '线宽', false, false, [this, 'width']),
                new BooleanProperty('是否贴地', '是否贴地.', false, false, [this, 'ground']),
                new ColorProperty('颜色', ' A Property specifying the color.', false, false, [this, 'color']),
                new BooleanProperty('是否为虚线', '是否为虚线.', false, false, [this, 'hasDash']),
                new ColorProperty('间隔颜色', 'A Property specifying the color.', false, false, [this, 'gapColor']),
                new NumberProperty('虚线长度', '虚线长度', false, false, [this, 'dashLength']),
                new DashPatternProperty('虚线图案', '虚线图案', false, false, [this, 'dashPattern']),
                // new BooleanProperty('是否带箭头', '是否带箭头.', true, false, [this, 'hasArrow'], SMGeoMultiLinePlottingBase.defaults.hasArrow),
                new BooleanProperty('单点编辑', '是否开启单点编辑', false, false, [this, 'pointEditing']),
                new BooleanProperty('是否编辑', '是否编辑.', false, false, [this, 'editing']),
                new EnumProperty('弧线类型', '弧线类型', false, false, [this, 'arcType'], [['直线', 'NONE'], ['地理直线', 'GEODESIC'], ['地理恒向线', 'RHUMB']]),
                new FunctionProperty("飞入", "飞入", ['number'], (duration: number) => this.flyTo(duration), [1000]),
                new BooleanProperty('是否开启深度检测', 'A boolean Property specifying the visibility.', false, false, [this, 'depthTest']),
                new NonreactiveJsonStringProperty('geoJson', '生成GeoJSON数据。', false, false, () => this.geoJsonStr, (value: string | undefined) => value && (this.geoJsonStr = value)),
            ]),
        ]
    }
}

export namespace SMGeoMultiLinePlottingBase {
    export const createDefaultProps = () => ({
        show: true, // boolean} [show=true] A boolean Property specifying the visibility
        allowPicking: false,
        positions: reactPositions(undefined), // A Property specifying the array of Cartesian3 positions that define the line strip.
        width: 1, // undfined时为1.0，A numeric Property specifying the width in pixels.
        ground: false,
        color: reactArray<[number, number, number, number]>([1, 1, 1, 1]), // default [1, 1, 1, 1]
        hasDash: false,
        gapColor: reactArray<[number, number, number, number]>([0, 0, 0, 0]), // default [0, 0, 0, 0]
        dashLength: 16, // default 16
        dashPattern: 255, // default 255
        // hasArrow: undefined as boolean | undefined,
        arcType: 'GEODESIC' as 'NONE' | 'GEODESIC' | 'RHUMB',
        editing: false,
        pointEditing: false,
        depthTest: false, //深度检测

        ...ESSceneObject.createDefaultProps(),
    });
    export const propNames = Object.keys(createDefaultProps());
}
extendClassProps(SMGeoMultiLinePlottingBase.prototype, SMGeoMultiLinePlottingBase.createDefaultProps);
export interface SMGeoMultiLinePlottingBase<T extends SuperMap.Geometry.GeoMultiLinePlotting> extends ReactivePropsToNativePropsAndChanged<ReturnType<typeof SMGeoMultiLinePlottingBase.createDefaultProps>> { }
type JsonType = PartialWithUndefinedReactivePropsToNativeProps<ReturnType<typeof SMGeoMultiLinePlottingBase.createDefaultProps> & { type: string }>;
