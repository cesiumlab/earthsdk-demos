import { ESNavigator, ESScale, ESViewerStatusBar } from 'esobjs-xe2-plugin/dist-node/esobjs-xe2-plugin-main';
import { Destroyable } from "xbsj-xe2/dist-node/xe2-base-utils";
import { XbsjEarthUi } from "../xbsjEarthUi";
import { react } from "xbsj-xe2/dist-node/xe2-base-utils";

export class ClassicNavigatorManager extends Destroyable {

    // private _navigator?: ESNavigator;//导航
    // get navigator() { return this._navigator; }

    // private _scale?: ESScale;//比例尺
    // get scale() { return this._scale; }

    // private _statusBar?: ESViewerStatusBar;//状态栏
    // get statusBar() { return this._statusBar; }

    private _timeLine = this.dv(react<boolean>(false));//时间线显影
    get timeLine() { return this._timeLine.value; }
    set timeLine(value: boolean) { this._timeLine.value = value; }

    private _timeLineWidth = this.dv(react<string>('calc(100% - 280px)'));
    get timeLineWidth() { return this._timeLineWidth.value; }
    get timeLineWidthChanged() { return this._timeLineWidth.changed; }
    set timeLineWidth(value: string) { this._timeLineWidth.value = value; }

    private _navigatorShow = this.dv(react<boolean>(true))//导航显隐
    get navigatorShow() { return this._navigatorShow.value; }
    get navigatorShowChanged() { return this._navigatorShow.changed; }
    set navigatorShow(value: boolean) { this._navigatorShow.value = value; }

    private _scaleShow = this.dv(react<boolean>(true))//比例尺显隐
    get scaleShow() { return this._scaleShow.value; }
    get scaleShowChanged() { return this._scaleShow.changed; }
    set scaleShow(value: boolean) { this._scaleShow.value = value; }

    private _statusBarShow = this.dv(react<boolean>(true))//显隐
    get statusBarShow() { return this._statusBarShow.value; }
    get statusBarShowChanged() { return this._statusBarShow.changed; }
    set statusBarShow(value: boolean) { this._statusBarShow.value = value; }

    private _navigatorScaleRight = this.dv(react<number>(290))//导航比例尺的right
    get navigatorScaleRight() { return this._navigatorScaleRight.value; }
    get navigatorScaleRightChanged() { return this._navigatorScaleRight.changed; }
    set navigatorScaleRight(value: number) { this._navigatorScaleRight.value = value; }


    

    constructor(private xbsjEarthUi: XbsjEarthUi) {
        super();

        // this._navigator = this.xbsjEarthUi.createSceneObject(ESNavigator);
        // this.d(() => this._navigator && this.xbsjEarthUi.destroySceneObject(this._navigator));

        // this._scale = this.xbsjEarthUi.createSceneObject(ESScale);
        // this.d(() => this._scale && this.xbsjEarthUi.destroySceneObject(this._scale));

        // this._statusBar = this.xbsjEarthUi.createSceneObject(ESViewerStatusBar);
        // if (this.statusBar) {
        //     this.statusBar.height = 30
        //     this.statusBar.bgColor = [37 / 255, 38 / 255, 42 / 255, 0.8]
        // }
        // this.d(() => this._statusBar && this.xbsjEarthUi.destroySceneObject(this._statusBar));

    }
}
