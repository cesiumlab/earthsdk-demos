// 声明文件中的 @/xr-utils必须替换掉，否则发布成npm包以后就读取不了了 vtxf 20220221

// 注意这个程序主要是为了处理声明文件的，不是处理js文件本身的。js文件中 @/xxx 都会被翻译成 ../xxx，这个是webpack自己操作的，externals中配置的。

import fs from 'fs';
import path from 'path';
import yargs from 'yargs'
const { config } = require('../libNames');
const libNames = config.libNames as string[];

/**
 * 文件遍历方法
 * @param fileDir 需要遍历的文件路径
 */
function* getFiles(fileDir: string) {
    const todoFileDirs: string[] = [fileDir];

    while (todoFileDirs.length > 0) {
        const currentFileDir = todoFileDirs.pop();
        if (!currentFileDir) {
            throw new Error(`!currentFileDir`);
        }

        const fileNames = fs.readdirSync(currentFileDir);

        for (let fileName of fileNames) {
            const filePath = path.join(currentFileDir, fileName);
            const stat = fs.statSync(filePath);
            if (stat.isFile()) {
                yield filePath;
            } else if (stat.isDirectory()) {
                todoFileDirs.push(filePath);
            }
        }
    }
}

function distReplace(fileDir: string) {
    for (let filePath of getFiles(fileDir)) {
        if (!filePath.endsWith('.d.ts')) {
            continue;
        }

        const text = fs.readFileSync(filePath, { encoding: 'utf-8', flag: 'r' });
    
        const replaceMap: [RegExp, string][] = [
            ...libNames.map(e => [new RegExp(`@/${e}`, 'g'), `${config.moduleName}/dist-node/${e}`] as [RegExp, string]),
        ];
    
        let text2 = text;
        for (let [r0, r1] of replaceMap) {
            text2 = text2.replace(r0, r1);
        }
    
        if (text !== text2) {
            fs.writeFileSync(filePath, text2, { encoding: 'utf-8' });
        }
    }
}

function processCommand() {
    const argv = yargs
    .option('i', {
      alias: 'input',
      demandOption: true,
      describe: '输入文件路径',
      type: 'string'
    })
    .parse(process.argv);

    if (argv instanceof Promise) {
        throw new Error(`argv instanceof Promise`);
    }

    const inputFileDir = argv.i;

    try {
        distReplace(inputFileDir);
    } catch (error) {
        console.error(`distReplace 发生错误！error: ${error}`);
    }
}

processCommand();
