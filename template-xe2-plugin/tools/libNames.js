const config = {
    namespace: 'XE2',
    moduleName: 'template-xe2-plugin',
    libNames: [
        "template-xe2-plugin-main",
        "template-xe2-plugin",
    ]
};

module.exports = { config };