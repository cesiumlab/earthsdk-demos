const path = require('path');
const webpack = require('webpack');
const { CleanWebpackPlugin } = require('clean-webpack-plugin');
const CopyWebpackPlugin = require('copy-webpack-plugin');
const { defines, info } = require('../base/defines');
const { config } = require('../libNames');
const libNames = config.libNames;
const namespace = config.namespace;

module.exports = {
  entry: {
    ...libNames.reduce((o, c) => (o[`${c}`] = `./src/${c}/index.ts`, o), {}),
  },
  module: {
    rules: [
      {
        test: /\.ts$/,
        use: "ts-loader",
        exclude: /node_modules/
      }
    ]
  },
  plugins: [
    new webpack.BannerPlugin(`${info.name}(${info.version}-${info.commitId.slice(0, 8)}-${info.date}) 版权所有@${info.owner}`),
    new webpack.DefinePlugin(defines),
    new CleanWebpackPlugin(),
    new CopyWebpackPlugin([
      'index.html',
      // {
      //   from: './static',
      //   to: 'static',
      //   toType: 'dir'
      // },
      // {
      //   from: './examples',
      //   to: 'examples',
      //   toType: 'dir'
      // },
      // {
      //   from: './**/xr-static/**/*',
      //   globOptions: { onlyFiles: false, },
      //   transformPath(targetPath, absolutePath) {
      //     // console.log('targetPath: ' + targetPath);
      //     var n = targetPath.indexOf('xr-static');
      //     return targetPath.substring(n);
      //   },
      // },
      // // @cesium
      // {
      //   // from: './node_modules/cesium/Build/Cesium', // CesiumUnminified
      //   from: process.env.NODE_ENV === 'production' ? './node_modules/cesium/Build/Cesium' : './node_modules/cesium/Build/CesiumUnminified', // CesiumUnminified
      //   to: 'js/cesium',
      //   toType: 'dir'
      // },
      {
        from: './xe2-assets',
        to: 'xe2-assets',
        toType: 'dir'
      },
    ]),
  ],
  externals: {
    ...libNames.reduce((o, c) => (o[`@/${c}`] = `${namespace}["${c}"]`, o), {}),
    
    // @cesium
    cesium: 'Cesium', // xr-utils中有cesium相关的函数

    'xbsj-xe2/dist-node/xe2': 'XE2["xe2"]',
    'xbsj-xe2/dist-node/xe2-base': 'XE2["xe2-base"]',
    'xbsj-xe2/dist-node/xe2-base-utils': 'XE2["xe2-base-utils"]',
    'xbsj-xe2/dist-node/xe2-utils': 'XE2["xe2-utils"]',
    'xbsj-xe2/dist-node/xe2-cesium': 'XE2["xe2-cesium"]',
    'xbsj-xe2/dist-node/xe2-mapbox': 'XE2["xe2-mapbox"]',
    'xbsj-xe2/dist-node/xe2-ue': 'XE2["xe2-ue"]',
    'xbsj-xe2/dist-node/utility-xe2-plugin': 'XE2["utility-xe2-plugin"]',
    'xbsj-xe2/dist-node/xe2-all-in-one': 'XE2["xe2-all-in-one"]',
    'xbsj-xe2/dist-node/xe2-base-objects': 'XE2["xe2-base-objects"]',
    'xbsj-xe2/dist-node/xe2-objects': 'XE2["xe2-objects"]',
    'xbsj-xe2/dist-node/xe2-cesium-objects': 'XE2["xe2-cesium-objects"]',
    'xbsj-xe2/dist-node/xe2-ue-objects': 'XE2["xe2-ue-objects"]',
    'xbsj-xe2/dist-node/xe2-openlayers': 'XE2["xe2-openlayers"]',
    'xbsj-xe2/dist-node/xe2-openlayers-objects': 'XE2["xe2-openlayers-objects"]',
    'smplotting-xe2-plugin/dist-node/smplotting-xe2-plugin': 'XE2["smplotting-xe2-plugin"]',
    'smplotting-xe2-plugin/dist-node/smplotting-xe2-plugin-main': 'XE2["smplotting-xe2-plugin-main"]',
    'template-xe2-plugin/dist-node/template-xe2-plugin': 'XE2["template-xe2-plugin"]',
    'template-xe2-plugin/dist-node/template-xe2-plugin-main': 'XE2["template-xe2-plugin-main"]',
  },
  // 编译ts文件时，需要resolve.extensions，否则会提示找不到模块！
  resolve: {
    extensions: ['.ts', '.js', '.json'],
    alias: {
      '@': path.resolve('./src/'),
      "vue": "vue/dist/vue.esm-bundler.js", // vue使用template时需要用到 vtxf 20201208
    },
    fallback: {
    },
  },
  output: {
    path: path.resolve(__dirname, '../../dist-web'),
    library: [namespace, "[name]"],
    libraryTarget: "var",
  }
};