
const { info } = require('../base/defines');
import fs from 'fs';
import yargs from 'yargs'

function writeFile(outputFileDir: string) {
    const commitId = info.commitId.slice(0, 8);
    fs.writeFileSync(`${outputFileDir}/VERSION.txt`, `${info.name}(${info.version}-${commitId}-${info.date}) 版权所有@${info.owner}`);
    const dateStr = info.date.replace(/:/g, '.')
    fs.writeFileSync(`${outputFileDir}/${info.version}.${commitId}.${dateStr}`, `${info.name}(${info.version}-${commitId}-${info.date}) 版权所有@${info.owner}`);
}

function processCommand() {
    const argv = yargs
        .option('o', {
            alias: 'output',
            demandOption: true,
            describe: '输出文件路径',
            type: 'string'
        })
        .parse(process.argv);

    if (argv instanceof Promise) {
        console.warn(`argv instanceof Promise`);
        return;
    }

    const outputFileDir = argv.o;

    writeFile(outputFileDir);
}

processCommand();
