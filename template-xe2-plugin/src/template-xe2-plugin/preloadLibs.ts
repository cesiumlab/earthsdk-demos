import { getCurrentScriptPath } from "./getCurrentScriptPath";

function preloadLibs() {
    const currentScriptPath = getCurrentScriptPath();
    const scriptName = 'template-xe2-plugin.js';
    const xe2RootPath = currentScriptPath.slice(0, -scriptName.length);

    // 发布时使用
    document.write(`\
        <script src="${xe2RootPath}template-xe2-plugin-main.js"></script>
    `);
}

preloadLibs();