function getCurrentScriptPathFromChromeFF() {
    // @ts-ignore
    return document.currentScript.src as string;
}

// function getCurrentScriptPathFromSafariOperaIE() {
//     const a = {};
//     let stack;
//     try {
//         a.b();
//     } catch(e) {
//         stack = e.stack || e.sourceURL || e.stacktrace;
//     }
//     const rExtractUri = /(?:http|https|file):\/\/.*?\/.+?.js/;
//     const absPath = rExtractUri.exec(stack);
//     return absPath[0] || '';
// }

export function getCurrentScriptPath() {
    return getCurrentScriptPathFromChromeFF();
}