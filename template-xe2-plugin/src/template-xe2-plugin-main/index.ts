import { registerScript } from 'xbsj-xe2/dist-node/xe2-utils';
registerScript();

import { copyright } from '../copyright';
copyright.print();
export { copyright };

export * from './objs';