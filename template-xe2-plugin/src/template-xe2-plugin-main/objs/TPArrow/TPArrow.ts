import { BooleanProperty, ESSceneObject, GeoArrow, GeoDivTextPoi, GroupProperty, StringProperty, PickedInfo, PositionsProperty, SceneObjectPickedInfo, PositionsEditing, PointEditing, PositionsCenter } from 'xbsj-xe2/dist-node/xe2-base-objects';
import { Event, Listener, PartialWithUndefinedReactivePropsToNativeProps, ReactivePropsToNativePropsAndChanged, bind, extendClassProps, reactPositions, track } from 'xbsj-xe2/dist-node/xe2-base-utils';
import { SceneObjectKey } from 'xbsj-xe2/dist-node/xe2-utils';
 
/**
 * 带文字提示的箭头
 */
export class TPArrow extends ESSceneObject {
    static readonly type = this.register('TPArrow', this, { chsName: 'TPArrow', tags: ['ESObjects', '_ES_Impl_Cesium'], description: 'TPArrow' });
    get typeName() { return 'TPArrow'; }
    override get defaultProps() { return TPArrow.createDefaultProps(); }
    get json() { return this._innerGetJson() as JsonType; }
    set json(value: JsonType) { this._innerSetJson(value); }

    private _pickedEvent = this.disposeVar(new Event<[PickedInfo]>());
    get pickedEvent() { return this._pickedEvent; }

    private _flyToEvent = this.disposeVar(new Event<[number | undefined]>());
    get flyToEvent(): Listener<[number | undefined]> { return this._flyToEvent; }
    flyTo(duration?: number) { this._flyToEvent.emit(duration); }
 
    static override defaults = {
        ...ESSceneObject.defaults,
        // 属性的类型若存在undefined的情况，这里配置为undefined时应该使用的默认值
        positions: [],
    }

    private _geoDivTextPoi = this.disposeVar(new GeoDivTextPoi());
    get geoDivTextPoi() { return this._geoDivTextPoi; }

    private _geoArrow = this.disposeVar(new GeoArrow());
    get geoArrow() { return this._geoArrow; }

    private _sPositionsEditing = this.disposeVar(new PositionsEditing([this, 'positions'], false, [this, 'editing'], this.components));
    get sPositionsEditing() { return this._sPositionsEditing; }

    private _sPointEditing = this.disposeVar(new PointEditing([this, 'positions'], [this, 'pointEditing'], this.components));
    get sPointEditing() { return this._sPointEditing; }

    private _positionsCenter = this.disposeVar(new PositionsCenter([this, 'positions']));
    get positionsCenter() { return this._positionsCenter; }
 
    constructor(id?: SceneObjectKey) {
        super(id);

        this.dispose(this.components.disposableAdd(this._geoArrow));
        this.dispose(track([this._geoArrow, 'positions'], [this, 'positions']));
        this.dispose(track([this._geoArrow, 'show'], [this, 'show']));
        this.dispose(track([this._geoArrow, 'ratio'], [this, 'ratio']));
        this.dispose(track([this._geoArrow, 'allowPicking'], [this, 'allowPicking']));

        this.dispose(this.components.disposableAdd(this.geoDivTextPoi));
        this.dispose(track([this.geoDivTextPoi, 'text'], [this, 'text']));
        this.dispose(track([this.geoDivTextPoi, 'position'], [this._geoArrow.positionsCenter, 'center']));
        this.dispose(track([this.geoDivTextPoi, 'show'], [this, 'show']));
        this.geoDivTextPoi.textEditingInteraction = true;

        this.dispose(this._geoArrow.pickedEvent.disposableOn(pickedInfo => {
            this._pickedEvent.emit(new SceneObjectPickedInfo(this, pickedInfo));
        }));

        this.dispose(this._flyToEvent.disposableOn(duration => {
            this._geoArrow.flyTo(duration);
        }));
    }
 
    override getProperties(language?: string) {
        return [
            ...super.getProperties(language),
            // 属性UI配置
            new GroupProperty('通用', '通用', [
                new BooleanProperty('显示', '显示(show)', false, false, [this, 'show']),
                new BooleanProperty('拾取', '拾取(allowPicking)', false, false, [this, 'allowPicking']),
                new StringProperty('文字', '文字', false, false, [this, 'text']),
                new BooleanProperty('编辑', '编辑(editing)', false, false, [this, 'editing']),
                new BooleanProperty('单点编辑', '单点编辑(editing)', false, false, [this, 'pointEditing']),
                new PositionsProperty('位置', '位置(positions)', true, false, [this, 'positions'], TPArrow.defaults.positions),
            ]),
        ];
    }
}
 
export namespace TPArrow {
    export const createDefaultProps = () => ({
        ...ESSceneObject.createDefaultProps(),
        show: true,
        allowPicking: false,
        text: '请输入文字',
        editing: false,
        pointEditing: false,
        positions: reactPositions(undefined),
        ratio: 0.02,
    });
}
extendClassProps(TPArrow.prototype, TPArrow.createDefaultProps);
export interface TPArrow extends ReactivePropsToNativePropsAndChanged<ReturnType<typeof TPArrow.createDefaultProps>> { }
type JsonType = PartialWithUndefinedReactivePropsToNativeProps<ReturnType<typeof TPArrow.createDefaultProps> & { type: string }>;
