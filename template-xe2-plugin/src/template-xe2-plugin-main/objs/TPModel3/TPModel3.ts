import { BooleanProperty, GroupProperty, PositionProperty, JsonProperty, NumberProperty, PositionEditing, StringProperty, PickedInfo, SceneObjectPickedInfo, Number4Property } from 'xbsj-xe2/dist-node/xe2-base-objects';
import { Event, Listener, PartialWithUndefinedReactivePropsToNativeProps, ReactivePropsToNativePropsAndChanged, extendClassProps, reactArrayWithUndefined, reactJsonWithUndefined, track } from 'xbsj-xe2/dist-node/xe2-base-utils';
import { ESSceneObject } from 'xbsj-xe2/dist-node/xe2-base-objects';
import { CzmModelPrimitive } from 'xbsj-xe2/dist-node/xe2-cesium-objects';
import { SceneObjectKey } from 'xbsj-xe2/dist-node/xe2-utils';
 
export class TPModel3 extends ESSceneObject {
    static readonly type = this.register('TPModel3', this, { chsName: 'TPModel3', tags: ['ESObjects', '_ES_Impl_Cesium'], description: 'TPModel3' });
    get typeName() { return 'TPModel3'; }
    override get defaultProps() { return TPModel3.createDefaultProps(); }
    get json() { return this._innerGetJson() as JsonType; }
    set json(value: JsonType) { this._innerSetJson(value); }

    private _pickedEvent = this.disposeVar(new Event<[PickedInfo]>());
    get pickedEvent() { return this._pickedEvent; }

    private _flyToEvent = this.disposeVar(new Event<[number | undefined]>());
    get flyToEvent(): Listener<[number | undefined]> { return this._flyToEvent; }
    flyTo(duration?: number) { this._flyToEvent.emit(duration); }
 
    static override defaults = {
        ...ESSceneObject.defaults,
        // 属性的类型若存在undefined的情况，这里配置为undefined时应该使用的默认值
        position: [116.39, 39.9, 100] as [number, number, number],
        viewDistanceRange: [1000, 10000, 30000, 60000] as [number, number, number, number],
    }
 
    // 位置编辑
    private _positionEditing = this.disposeVar(new PositionEditing([this, 'position'], [this, 'editing'], this.components));
    get positionEditing() { return this._positionEditing; }
 
    constructor(id?: SceneObjectKey) {
        super(id);
    }
 
    override getProperties(language?: string) {
        return [
            ...super.getProperties(language),
            // 属性UI配置
            new GroupProperty('通用', '通用', [
                new BooleanProperty('显示', '显示(show)', false, false, [this, 'show']),
                new BooleanProperty('编辑', '编辑(editing)', false, false, [this, 'editing']),
                new PositionProperty('位置', '位置(position)', true, false, [this, 'position'], TPModel3.defaults.position),
            ]),
            new GroupProperty('可视范围', '可视范围', [
                new Number4Property('可视范围控制', '可视范围控制[near0, near1, far1, far0]', true, false, [this, 'viewDistanceRange'], TPModel3.defaults.viewDistanceRange),
                new BooleanProperty('可视距离调试', '可视距离调试', false, false, [this, 'viewDistanceDebug']),
            ]),
        ];
    }
}
 
export namespace TPModel3 {
    export const createDefaultProps = () => ({
        ...ESSceneObject.createDefaultProps(),
        // 属性配置
        show: true,
        editing: false,
        position: reactArrayWithUndefined<[number, number, number]>(undefined),
        viewDistanceRange: reactArrayWithUndefined<[number, number, number, number]>(undefined),
        viewDistanceDebug: false,
    });
}
extendClassProps(TPModel3.prototype, TPModel3.createDefaultProps);
export interface TPModel3 extends ReactivePropsToNativePropsAndChanged<ReturnType<typeof TPModel3.createDefaultProps>> { }
type JsonType = PartialWithUndefinedReactivePropsToNativeProps<ReturnType<typeof TPModel3.createDefaultProps> & { type: string }>;