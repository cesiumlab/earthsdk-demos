import * as Cesium from 'cesium';
import { CzmModelPrimitive, CzmObject, CzmViewDistanceRangeControl, CzmViewer } from 'xbsj-xe2/dist-node/xe2-cesium-objects';
import { TPModel3 } from './TPModel3';
import { track } from 'xbsj-xe2/dist-node/xe2-base-utils';
import { SceneObjectPickedInfo } from 'xbsj-xe2/dist-node/xe2-base-objects';

/**
 * 模型，增加可见性控制
 */
export class CzmTPModel3 extends CzmObject<TPModel3> {
    static readonly type = this.register(TPModel3.type, this);

    private _czmModel = this.disposeVar(new CzmModelPrimitive());
    get czmModel() { return this._czmModel; }

    private _czmViewVisibleDistanceRangeControl = this.disposeVar(new CzmViewDistanceRangeControl(
        this.czmViewer,
        [this.sceneObject, 'viewDistanceRange'],
        [this.sceneObject, 'position'],
        // [this.sceneObject, 'radius'],
    ));
    get czmViewerVisibleDistanceRangeControl() { return this._czmViewVisibleDistanceRangeControl; }
    get visibleAlpha() { return this._czmViewVisibleDistanceRangeControl.visibleAlpha; }
    get visibleAlphaChanged() { return this._czmViewVisibleDistanceRangeControl.visibleAlphaChanged; }
    private _viewDistanceDebugBinding = (this.dispose(track([this._czmViewVisibleDistanceRangeControl, 'debug'], [this.sceneObject, 'viewDistanceDebug'])), 0);

    constructor(sceneObject: TPModel3, czmViewer: CzmViewer) {
        super(sceneObject, czmViewer);
        const viewer = czmViewer.viewer;
        if (!viewer) {
            console.warn(`viewer is undefined!`);
            return;
        }

        // 测试Cesium是否可用
        const cartesian = new Cesium.Cartesian3();
        console.log(cartesian);

        czmViewer.add(this._czmModel);
        this.dispose(() => czmViewer.delete(this._czmModel));

        this._czmModel.pixelSize = 10;

        this.dispose(track([this._czmModel, 'position'], [sceneObject, 'position']));
        this.dispose(track([this._czmModel, 'show'], [sceneObject, 'show']));

        this._czmModel.allowPicking = true;
        this.dispose(this._czmModel.pickedEvent.disposableOn(pickedInfo => {
            sceneObject.pickedEvent.emit(new SceneObjectPickedInfo(sceneObject, pickedInfo));
        }));

        this.dispose(sceneObject.flyToEvent.disposableOn(duration => {
            this._czmModel.flyTo(duration);
        }));

        {
            const update = () => {
                this._czmModel.show = (sceneObject.show ?? true) && (this.visibleAlpha > 0);
            };
            update();
            this.dispose(sceneObject.showChanged.disposableOn(update));
            this.dispose(this.visibleAlphaChanged.disposableOn(update));
        }

        {
            const update = () => {
                this._czmModel.color = [1, 1, 1, this.visibleAlpha];
            };
            update();
            this.dispose(this.visibleAlphaChanged.disposableOn(update));
        }
    }
}
