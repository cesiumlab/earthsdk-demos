import { PositionEditing } from 'xbsj-xe2/dist-node/xe2-base-objects';
import { BooleanProperty, GroupProperty, PositionProperty, PickedInfo } from 'xbsj-xe2/dist-node/xe2-base-objects';
import { Event, Listener, PartialWithUndefinedReactivePropsToNativeProps, ReactivePropsToNativePropsAndChanged, extendClassProps, reactArrayWithUndefined, reactJsonWithUndefined, track } from 'xbsj-xe2/dist-node/xe2-base-utils';
import { ESSceneObject } from 'xbsj-xe2/dist-node/xe2-base-objects';
import { CzmModelPrimitive } from 'xbsj-xe2/dist-node/xe2-cesium-objects';
import { SceneObjectKey } from 'xbsj-xe2/dist-node/xe2-utils';

/**
 * 模型，增加Cesium实现类
 */
export class TPModel2 extends ESSceneObject {
    static readonly type = this.register('TPModel2', this, { chsName: 'TPModel2', tags: ['ESObjects', '_ES_Impl_Cesium'], description: 'TPModel2' });
    get typeName() { return 'TPModel2'; }
    override get defaultProps() { return TPModel2.createDefaultProps(); }
    get json() { return this._innerGetJson() as JsonType; }
    set json(value: JsonType) { this._innerSetJson(value); }

    private _pickedEvent = this.disposeVar(new Event<[PickedInfo]>());
    get pickedEvent() { return this._pickedEvent; }

    private _flyToEvent = this.disposeVar(new Event<[number | undefined]>());
    get flyToEvent(): Listener<[number | undefined]> { return this._flyToEvent; }
    flyTo(duration?: number) { this._flyToEvent.emit(duration); }
 
    static override defaults = {
        ...ESSceneObject.defaults,
        // 属性的类型若存在undefined的情况，这里配置为undefined时应该使用的默认值
        position: [116.39, 39.9, 100] as [number, number, number],
    }
 
    // 位置编辑
    private _positionEditing = this.disposeVar(new PositionEditing([this, 'position'], [this, 'editing'], this.components));
    get positionEditing() { return this._positionEditing; }
 
    constructor(id?: SceneObjectKey) {
        super(id);
    }
 
    override getProperties(language?: string) {
        return [
            ...super.getProperties(language),
            // 属性UI配置
            new GroupProperty('通用', '通用', [
                new BooleanProperty('显示', '显示(show)', false, false, [this, 'show']),
                new BooleanProperty('编辑', '编辑(editing)', false, false, [this, 'editing']),
                new PositionProperty('位置', '位置(position)', true, false, [this, 'position'], TPModel2.defaults.position),
            ]),
        ];
    }
}
 
export namespace TPModel2 {
    export const createDefaultProps = () => ({
        ...ESSceneObject.createDefaultProps(),
        // 属性配置
        show: true,
        editing: false,
        position: reactArrayWithUndefined<[number, number, number]>(undefined),
    });
}
extendClassProps(TPModel2.prototype, TPModel2.createDefaultProps);
export interface TPModel2 extends ReactivePropsToNativePropsAndChanged<ReturnType<typeof TPModel2.createDefaultProps>> { }
type JsonType = PartialWithUndefinedReactivePropsToNativeProps<ReturnType<typeof TPModel2.createDefaultProps> & { type: string }>;