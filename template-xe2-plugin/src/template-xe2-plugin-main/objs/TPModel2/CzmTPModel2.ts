import * as Cesium from 'cesium';
import { CzmModelPrimitive, CzmObject, CzmViewer } from 'xbsj-xe2/dist-node/xe2-cesium-objects';
import { TPModel2 } from './TPModel2';
import { track } from 'xbsj-xe2/dist-node/xe2-base-utils';
import { SceneObjectPickedInfo } from 'xbsj-xe2/dist-node/xe2-base-objects';

export class CzmTPModel2 extends CzmObject<TPModel2> {
    static readonly type = this.register(TPModel2.type, this);

    private _czmModel = this.disposeVar(new CzmModelPrimitive());
    get czmModel() { return this._czmModel; }

    constructor(sceneObject: TPModel2, czmViewer: CzmViewer) {
        super(sceneObject, czmViewer);
        const viewer = czmViewer.viewer;
        if (!viewer) {
            console.warn(`viewer is undefined!`);
            return;
        }

        // 测试Cesium是否可用
        const cartesian = new Cesium.Cartesian3();
        console.log(cartesian);

        czmViewer.add(this._czmModel);
        this.dispose(() => czmViewer.delete(this._czmModel));

        this.dispose(track([this._czmModel, 'position'], [sceneObject, 'position']));
        this.dispose(track([this._czmModel, 'show'], [sceneObject, 'show']));

        this._czmModel.allowPicking = true;
        this.dispose(this._czmModel.pickedEvent.disposableOn(pickedInfo => {
            sceneObject.pickedEvent.emit(new SceneObjectPickedInfo(sceneObject, pickedInfo));
        }));

        this.dispose(sceneObject.flyToEvent.disposableOn(duration => {
            this._czmModel.flyTo(duration);
        }));
    }
}