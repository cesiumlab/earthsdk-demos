import { BooleanProperty, GroupProperty, PositionProperty, JsonProperty, NumberProperty, PositionEditing, StringProperty, PickedInfo, SceneObjectPickedInfo } from 'xbsj-xe2/dist-node/xe2-base-objects';
import { Event, Listener, PartialWithUndefinedReactivePropsToNativeProps, ReactivePropsToNativePropsAndChanged, bind, extendClassProps, reactArrayWithUndefined, reactJsonWithUndefined, track } from 'xbsj-xe2/dist-node/xe2-base-utils';
import { ESSceneObject } from 'xbsj-xe2/dist-node/xe2-base-objects';
import { CzmModelPrimitive,CzmToolbar } from 'xbsj-xe2/dist-node/xe2-cesium-objects';
import { SceneObjectKey } from 'xbsj-xe2/dist-node/xe2-utils';

/**
 * 模型，使用编辑工具栏来做编辑操作
 */
export class TPModel5 extends ESSceneObject {
    static readonly type = this.register('TPModel5', this, { chsName: 'TPModel5', tags: ['ESObjects', '_ES_Impl_Cesium'], description: 'TPModel5' });
    get typeName() { return 'TPModel5'; }
    override get defaultProps() { return TPModel5.createDefaultProps(); }
    get json() { return this._innerGetJson() as JsonType; }
    set json(value: JsonType) { this._innerSetJson(value); }

    private _pickedEvent = this.disposeVar(new Event<[PickedInfo]>());
    get pickedEvent() { return this._pickedEvent; }

    private _flyToEvent = this.disposeVar(new Event<[number | undefined]>());
    get flyToEvent(): Listener<[number | undefined]> { return this._flyToEvent; }
    flyTo(duration?: number) { this._flyToEvent.emit(duration); }
 
    static override defaults = {
        ...ESSceneObject.defaults,
        // 属性的类型若存在undefined的情况，这里配置为undefined时应该使用的默认值
        position: [116.39, 39.9, 100] as [number, number, number],
    }
 
    // 位置编辑
    // private _positionEditing = this.disposeVar(new PositionEditing([this, 'position'], [this, 'editing'], this.components));
    // get positionEditing() { return this._positionEditing; }

    private _toolBar = this.disposeVar(new CzmToolbar());
    get toolBar() { return this._toolBar; }

    private _czmModel = this.disposeVar(new CzmModelPrimitive());
    get czmModel() { return this._czmModel; }
 
    constructor(id?: SceneObjectKey) {
        super(id);

        this.dispose(this.components.disposableAdd(this._czmModel));
        this.dispose(track([this._czmModel, 'position'], [this, 'position']));
        this.dispose(track([this._czmModel, 'show'], [this, 'show']));

        this.dispose(this.components.disposableAdd(this.toolBar));
        this.dispose(bind([this.toolBar, 'position'], [this, 'position']));
        this.dispose(bind([this.toolBar, 'show'], [this, 'editing']));

        this.dispose(track([this._czmModel, 'scale'], [this.toolBar, 'scale'], s => [s, s, s]))

        this._czmModel.pixelSize = 10;
        this._czmModel.allowPicking = true;
        this.dispose(this._czmModel.pickedEvent.disposableOn(pickedInfo => {
            this._pickedEvent.emit(new SceneObjectPickedInfo(this, pickedInfo));
        }));

        this.dispose(this._flyToEvent.disposableOn(duration => {
            this._czmModel.flyTo(duration);
        }));

        {
            this._czmModel.silhouetteColor = [ 1, 1, 0, 1 ];
            const update = () => {
                this._czmModel.silhouetteSize = this.highLighted ? 2 : 0;
            };
            update();
            this.dispose(this.highLightedChanged.disposableOn(update));
        }
    }
 
    override getProperties(language?: string) {
        return [
            ...super.getProperties(language),
            // 属性UI配置
            new GroupProperty('通用', '通用', [
                new BooleanProperty('显示', '显示(show)', false, false, [this, 'show']),
                new BooleanProperty('编辑', '编辑(editing)', false, false, [this, 'editing']),
                new PositionProperty('位置', '位置(position)', true, false, [this, 'position'], TPModel5.defaults.position),
                new BooleanProperty('高亮', '高亮(highLighted)', false, false, [this, 'highLighted']),
            ]),
        ];
    }
}
 
export namespace TPModel5 {
    export const createDefaultProps = () => ({
        ...ESSceneObject.createDefaultProps(),
        // 属性配置
        show: true,
        editing: false,
        position: reactArrayWithUndefined<[number, number, number]>(undefined),
        highLighted: false,
    });
}
extendClassProps(TPModel5.prototype, TPModel5.createDefaultProps);
export interface TPModel5 extends ReactivePropsToNativePropsAndChanged<ReturnType<typeof TPModel5.createDefaultProps>> { }
type JsonType = PartialWithUndefinedReactivePropsToNativeProps<ReturnType<typeof TPModel5.createDefaultProps> & { type: string }>;