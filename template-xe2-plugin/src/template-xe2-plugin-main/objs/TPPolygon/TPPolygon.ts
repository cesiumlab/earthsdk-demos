import { BooleanProperty, ESSceneObject, GeoPolygon, GeoDivTextPoi, GroupProperty, StringProperty, PickedInfo, PositionsProperty, SceneObjectPickedInfo, PositionsEditing, PointEditing, PositionsCenter } from 'xbsj-xe2/dist-node/xe2-base-objects';
import { Event, Listener, PartialWithUndefinedReactivePropsToNativeProps, ReactivePropsToNativePropsAndChanged, bind, extendClassProps, reactPositions, track } from 'xbsj-xe2/dist-node/xe2-base-utils';
import { SceneObjectKey } from 'xbsj-xe2/dist-node/xe2-utils';
 
/**
 * 带文字提示的多边形
 */
export class TPPolygon extends ESSceneObject {
    static readonly type = this.register('TPPolygon', this, { chsName: 'TPPolygon', tags: ['ESObjects', '_ES_Impl_Cesium'], description: 'TPPolygon' });
    get typeName() { return 'TPPolygon'; }
    override get defaultProps() { return TPPolygon.createDefaultProps(); }
    get json() { return this._innerGetJson() as JsonType; }
    set json(value: JsonType) { this._innerSetJson(value); }

    private _pickedEvent = this.disposeVar(new Event<[PickedInfo]>());
    get pickedEvent() { return this._pickedEvent; }

    private _flyToEvent = this.disposeVar(new Event<[number | undefined]>());
    get flyToEvent(): Listener<[number | undefined]> { return this._flyToEvent; }
    flyTo(duration?: number) { this._flyToEvent.emit(duration); }
 
    static override defaults = {
        ...ESSceneObject.defaults,
        // 属性的类型若存在undefined的情况，这里配置为undefined时应该使用的默认值
        positions: [],
    }

    private _geoDivTextPoi = this.disposeVar(new GeoDivTextPoi());
    get geoDivTextPoi() { return this._geoDivTextPoi; }

    private _geoPolygon = this.disposeVar(new GeoPolygon());
    get geoPolygon() { return this._geoPolygon; }

    private _sPositionsEditing = this.disposeVar(new PositionsEditing([this, 'positions'], false, [this, 'editing'], this.components));
    get sPositionsEditing() { return this._sPositionsEditing; }

    private _sPointEditing = this.disposeVar(new PointEditing([this, 'positions'], [this, 'pointEditing'], this.components));
    get sPointEditing() { return this._sPointEditing; }

    private _positionsCenter = this.disposeVar(new PositionsCenter([this, 'positions']));
    get positionsCenter() { return this._positionsCenter; }
 
    constructor(id?: SceneObjectKey) {
        super(id);

        this.dispose(this.components.disposableAdd(this._geoPolygon));
        this.dispose(track([this._geoPolygon, 'positions'], [this, 'positions']));
        this.dispose(track([this._geoPolygon, 'show'], [this, 'show']));
        this.dispose(track([this._geoPolygon, 'allowPicking'], [this, 'allowPicking']));

        this.dispose(this.components.disposableAdd(this.geoDivTextPoi));
        this.geoDivTextPoi.textEditingInteraction = true;
        this.dispose(track([this.geoDivTextPoi, 'text'], [this, 'text']));
        this.dispose(track([this.geoDivTextPoi, 'position'], [this._geoPolygon.positionsCenter, 'center']));
        this.dispose(track([this.geoDivTextPoi, 'show'], [this, 'show']));

        this.dispose(this._geoPolygon.pickedEvent.disposableOn(pickedInfo => {
            this._pickedEvent.emit(new SceneObjectPickedInfo(this, pickedInfo));
        }));

        this.dispose(this._flyToEvent.disposableOn(duration => {
            this._geoPolygon.flyTo(duration);
        }));
    }
 
    override getProperties(language?: string) {
        return [
            ...super.getProperties(language),
            // 属性UI配置
            new GroupProperty('通用', '通用', [
                new BooleanProperty('显示', '显示(show)', false, false, [this, 'show']),
                new BooleanProperty('拾取', '拾取(allowPicking)', false, false, [this, 'allowPicking']),
                new StringProperty('文字', '文字', false, false, [this, 'text']),
                new BooleanProperty('编辑', '编辑(editing)', false, false, [this, 'editing']),
                new BooleanProperty('单点编辑', '单点编辑(editing)', false, false, [this, 'pointEditing']),
                new PositionsProperty('位置', '位置(positions)', true, false, [this, 'positions'], TPPolygon.defaults.positions),
            ]),
        ];
    }
}
 
export namespace TPPolygon {
    export const createDefaultProps = () => ({
        ...ESSceneObject.createDefaultProps(),
        allowPicking: false,
        show: true,
        text: '请输入文字',
        editing: false,
        pointEditing: false,
        positions: reactPositions(undefined),
    });
}
extendClassProps(TPPolygon.prototype, TPPolygon.createDefaultProps);
export interface TPPolygon extends ReactivePropsToNativePropsAndChanged<ReturnType<typeof TPPolygon.createDefaultProps>> { }
type JsonType = PartialWithUndefinedReactivePropsToNativeProps<ReturnType<typeof TPPolygon.createDefaultProps> & { type: string }>;
