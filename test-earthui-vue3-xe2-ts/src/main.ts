import { createApp } from 'vue';
import App from './App.vue';
import EarthSDKUI from 'earthsdk-ui';
//图标
import 'vue-xe2-plugin/dist-node/components/earthui/scripts/iconfont.js';
import 'earthsdk-ui/lib/style.css';
const app = createApp(App)
app.use(EarthSDKUI);
app.mount('#app');
