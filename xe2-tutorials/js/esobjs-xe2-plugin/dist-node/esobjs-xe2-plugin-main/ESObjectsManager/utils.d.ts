import { ESCzmViewer, ESUeViewer, ESViewer } from "../ESViewers";
import { ESObjectsManager } from "./ESObjectsManager";
export declare type SwitchToESCzmViewerOptions = {
    domid: string | HTMLElement;
    viewSync?: boolean;
    attributeSync?: boolean;
};
export declare type SwitchToESUEViewerOptions = {
    domid: string | HTMLElement;
    uri: string;
    app: string;
    viewSync?: boolean;
    attributeSync?: boolean;
};
export declare function parseSwitchToCesiumViewerOptions(...args: any[]): SwitchToESCzmViewerOptions | undefined;
export declare function getCameraInfoIfSyncNeeded(activeViewer: ESViewer | undefined, viewSync: boolean): {
    position: [number, number, number];
    rotation: [number, number, number];
} | undefined;
export declare function findOrCreateCesiumViewer(objectsManager: ESObjectsManager, options: SwitchToESCzmViewerOptions, cameraInfo?: {
    position: [number, number, number];
    rotation: [number, number, number];
}): ESCzmViewer;
export declare const syncOnceOtherViewer: (viewer: ESViewer, otherViewer: ESViewer) => void;
export declare function parseSwitchToUEViewerOptions(...args: any[]): SwitchToESUEViewerOptions | undefined;
export declare function findOrCreateUEViewer(objectsManager: ESObjectsManager, options: SwitchToESUEViewerOptions, cameraInfo?: {
    position: [number, number, number];
    rotation: [number, number, number];
}): ESUeViewer;
