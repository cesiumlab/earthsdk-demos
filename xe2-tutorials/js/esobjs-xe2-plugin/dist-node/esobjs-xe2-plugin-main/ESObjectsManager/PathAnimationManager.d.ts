import { Destroyable } from "xbsj-xe2/dist-node/xe2-base-utils";
import { ESPlayer } from '../esobjs';
import { ESObjectsManager } from "./ESObjectsManager";
declare type ChannelType = {
    pathId: string;
    sceneObjectIds: string[];
};
export declare class PathAnimationManager extends Destroyable {
    private _objectManager;
    private _player;
    get player(): ESPlayer;
    private _channels;
    get channels(): ChannelType[];
    get channelsChanged(): import("xbsj-xe2/dist-node/xe2-base-utils").Listener<[ChannelType[], ChannelType[]]>;
    set channels(value: ChannelType[]);
    private _channelsDispose;
    constructor(_objectManager: ESObjectsManager);
}
export {};
