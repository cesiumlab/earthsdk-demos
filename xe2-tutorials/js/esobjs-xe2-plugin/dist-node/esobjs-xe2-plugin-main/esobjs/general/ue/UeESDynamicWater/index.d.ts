import { UeViewer } from 'xbsj-xe2/dist-node/xe2-ue-objects';
import { ESDynamicWater } from './../../objs';
import { UeESLocalPolygon } from '../UeESLocalPolygon';
export declare class UeESDynamicWater extends UeESLocalPolygon<ESDynamicWater> {
    static readonly type: void;
    constructor(sceneObject: ESDynamicWater, ueViewer: UeViewer);
}
