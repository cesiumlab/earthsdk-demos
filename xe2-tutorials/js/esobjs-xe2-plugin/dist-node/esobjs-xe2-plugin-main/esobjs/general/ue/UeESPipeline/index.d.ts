import { ESPipeline } from "../../objs";
import { UeViewer } from "xbsj-xe2/dist-node/xe2-ue-objects";
import { UeESGeoLineString } from "../UeESGeoLineString";
export declare class UeESPipeline extends UeESGeoLineString<ESPipeline> {
    static readonly type: void;
    constructor(sceneObject: ESPipeline, ueViewer: UeViewer);
}
