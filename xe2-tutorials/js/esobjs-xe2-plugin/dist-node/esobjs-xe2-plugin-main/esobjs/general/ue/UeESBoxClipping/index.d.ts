import { ESBoxClipping } from '../../objs';
import { UeViewer } from 'xbsj-xe2/dist-node/xe2-ue-objects';
import { UeESObjectWithLocation } from '../../../base';
export declare class UeESBoxClipping extends UeESObjectWithLocation<ESBoxClipping> {
    static readonly type: void;
    constructor(sceneObject: ESBoxClipping, ueViewer: UeViewer);
}
