import { UeViewer } from 'xbsj-xe2/dist-node/xe2-ue-objects';
import { ESSunshineAnalysis } from './../../objs';
import { UeESGeoPolygon } from '../UeESGeoPolygon';
export declare class UeESSunshineAnalysis extends UeESGeoPolygon<ESSunshineAnalysis> {
    static readonly type: void;
    constructor(sceneObject: ESSunshineAnalysis, ueViewer: UeViewer);
}
