import { UeViewer } from "xbsj-xe2/dist-node/xe2-ue-objects";
import { UeESLocalVector2D } from "../../../base";
import { ESLocalPolygon } from '../../objs';
export declare class UeESLocalPolygon<T extends ESLocalPolygon = ESLocalPolygon> extends UeESLocalVector2D<T> {
    static readonly type: void;
    constructor(sceneObject: T, ueViewer: UeViewer);
}
