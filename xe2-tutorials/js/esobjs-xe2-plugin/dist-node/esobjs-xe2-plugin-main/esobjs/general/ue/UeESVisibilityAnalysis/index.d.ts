import { ESVisibilityAnalysis } from '../../objs';
import { UeViewer } from 'xbsj-xe2/dist-node/xe2-ue-objects';
import { UeESGeoVector } from '../../../base';
export declare class UeESVisibilityAnalysis extends UeESGeoVector<ESVisibilityAnalysis> {
    static readonly type: void;
    constructor(sceneObject: ESVisibilityAnalysis, ueViewer: UeViewer);
}
