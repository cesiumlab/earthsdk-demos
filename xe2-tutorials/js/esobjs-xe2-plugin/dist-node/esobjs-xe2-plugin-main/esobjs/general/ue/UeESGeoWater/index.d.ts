import { ESGeoWater } from './../../objs';
import { UeViewer } from 'xbsj-xe2/dist-node/xe2-ue-objects';
import { UeESGeoPolygon } from '../UeESGeoPolygon';
export declare class UeESGeoWater extends UeESGeoPolygon<ESGeoWater> {
    static readonly type: void;
    constructor(sceneObject: ESGeoWater, ueViewer: UeViewer);
}
