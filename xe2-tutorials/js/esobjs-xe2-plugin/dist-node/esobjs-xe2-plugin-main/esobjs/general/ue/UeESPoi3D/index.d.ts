import { UeViewer } from "xbsj-xe2/dist-node/xe2-ue-objects";
import { UeESObjectWithLocation } from "../../../base";
import { ESPoi3D } from '../../objs';
export declare class UeESPoi3D extends UeESObjectWithLocation<ESPoi3D> {
    static readonly type: void;
    constructor(sceneObject: ESPoi3D, ueViewer: UeViewer);
}
