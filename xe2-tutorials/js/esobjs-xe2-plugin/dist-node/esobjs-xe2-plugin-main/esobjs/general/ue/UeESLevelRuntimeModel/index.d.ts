import { ESLevelRuntimeModel } from '../../objs';
import { UeViewer } from "xbsj-xe2/dist-node/xe2-ue-objects";
import { UeESObjectWithLocation } from "../../../base";
export declare class UeESLevelRuntimeModel extends UeESObjectWithLocation<ESLevelRuntimeModel> {
    static readonly type: void;
    constructor(sceneObject: ESLevelRuntimeModel, ueViewer: UeViewer);
}
