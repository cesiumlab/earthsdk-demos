import { ESPit } from "../../objs";
import { UeViewer } from "xbsj-xe2/dist-node/xe2-ue-objects";
import { UeESGeoPolygon } from "../UeESGeoPolygon";
export declare class UeESPit extends UeESGeoPolygon<ESPit> {
    static readonly type: void;
    constructor(sceneObject: ESPit, ueViewer: UeViewer);
}
