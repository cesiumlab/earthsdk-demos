import { UeESObjectWithLocation, UeViewer } from "esobjs-xe2-plugin/dist-node/esobjs-xe2-plugin-main/esobjs/base";
import { ESBlastParticleSystem } from "../../objs";
export declare class UeESBlastParticleSystem extends UeESObjectWithLocation<ESBlastParticleSystem> {
    static readonly type: void;
    constructor(sceneObject: ESBlastParticleSystem, ueViewer: UeViewer);
}
