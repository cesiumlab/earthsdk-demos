import { ESDatasmithRuntimeModel } from '../../objs';
import { UeViewer } from "xbsj-xe2/dist-node/xe2-ue-objects";
import { UeESObjectWithLocation } from "../../../base";
export declare class UeESDatasmithRuntimeModel extends UeESObjectWithLocation<ESDatasmithRuntimeModel> {
    static readonly type: void;
    constructor(sceneObject: ESDatasmithRuntimeModel, ueViewer: UeViewer);
}
