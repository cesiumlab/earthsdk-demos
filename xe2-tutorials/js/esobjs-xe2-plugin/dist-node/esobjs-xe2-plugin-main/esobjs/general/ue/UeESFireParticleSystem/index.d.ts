import { UeESObjectWithLocation, UeViewer } from "esobjs-xe2-plugin/dist-node/esobjs-xe2-plugin-main/esobjs/base";
import { ESFireParticleSystem } from "../../objs";
export declare class UeESFireParticleSystem extends UeESObjectWithLocation<ESFireParticleSystem> {
    static readonly type: void;
    constructor(sceneObject: ESFireParticleSystem, ueViewer: UeViewer);
}
