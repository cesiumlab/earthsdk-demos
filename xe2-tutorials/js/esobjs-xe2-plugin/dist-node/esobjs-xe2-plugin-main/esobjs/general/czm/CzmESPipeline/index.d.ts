import { ESPipeline } from './../../objs/ESPipeline';
import { CzmCustomPrimitive, CzmTexture, CzmViewer } from 'xbsj-xe2/dist-node/xe2-cesium-objects';
import { CzmESGeoLineString } from '../CzmESGeoLineString';
export declare class CzmESPipeline extends CzmESGeoLineString<ESPipeline> {
    static readonly type: void;
    private _czmCustomPrimitive;
    get czmCustomPrimitive(): CzmCustomPrimitive;
    private _czmTexture;
    get czmTexture(): CzmTexture;
    constructor(sceneObject: ESPipeline, czmViewer: CzmViewer);
}
