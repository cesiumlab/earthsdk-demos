import { CzmESObjectWithLocation } from "esobjs-xe2-plugin/dist-node/esobjs-xe2-plugin-main/esobjs/base";
import { ESPoi3D } from "../../objs";
import { CzmViewer } from "xbsj-xe2/dist-node/xe2-cesium-objects";
export declare class CzmESPoi3D extends CzmESObjectWithLocation<ESPoi3D> {
    static readonly type: void;
    private _czmModelPrimitive;
    get czmModelPrimitive(): any;
    set czmModelPrimitive(val: any);
    private _textBase64;
    constructor(sceneObject: ESPoi3D, czmViewer: CzmViewer);
    flyTo(duration: number | undefined, id: number): boolean;
}
