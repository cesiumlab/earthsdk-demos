import { CzmESObjectWithLocation } from "esobjs-xe2-plugin/dist-node/esobjs-xe2-plugin-main/esobjs/base";
import { CzmBoxClippingPlanes, CzmViewer } from 'xbsj-xe2/dist-node/xe2-cesium-objects';
import { ESBoxClipping } from '../../objs';
import { SceneObjectWithId } from "xbsj-renderer/dist-node/xr-utils";
export declare class CzmESBoxClipping extends CzmESObjectWithLocation<ESBoxClipping> {
    static readonly type: void;
    private _czmBoxClippingPlanes;
    get czmBoxClippingPlanes(): CzmBoxClippingPlanes;
    private _tilesSceneObjectWithId;
    get tilesSceneObjectWithId(): SceneObjectWithId<import("xbsj-renderer/dist-node/xr-utils").SceneObject>;
    private _tilesSceneObjectWithIdInit;
    private _event;
    get event(): import("xbsj-xe2/dist-node/xe2-base-utils").NextAnimateFrameEvent;
    private _tilesIdResetting;
    constructor(sceneObject: ESBoxClipping, czmViewer: CzmViewer);
    flyTo(duration: number | undefined, id: number): boolean;
}
