/// <reference types="xbsj-renderer/dist-node/xr-cesium/__declares/__cesium" />
/// <reference types="xbsj-xe2/dist-node/xe2-cesium-objects/__declares/__cesium" />
import { CzmESGeoVector } from 'esobjs-xe2-plugin/dist-node/esobjs-xe2-plugin-main/esobjs/base';
import { GeoPolylines, PositionsEditing } from 'xbsj-xe2/dist-node/xe2-base-objects';
import { CzmViewer } from 'xbsj-xe2/dist-node/xe2-cesium-objects';
import { ESVisibilityAnalysis } from '../../objs';
import * as Cesium from 'cesium';
/**
 * 计算切割点
 * @param p1 起始点
 * @param p2 结束点
 * @param scene Cesium场景
 * @returns {[number, number, number]|undefined} 切割点
 */
export declare function computeCutPoint(p1: [number, number, number], p2: [number, number, number], scene: Cesium.Scene): [number, number, number] | undefined;
export declare class CzmESVisibilityAnalysis extends CzmESGeoVector<ESVisibilityAnalysis> {
    static readonly type: void;
    private _hideGeoPolylines;
    get hideGeoPolylines(): GeoPolylines;
    private _visibleGeoPolylines;
    get visibleGeoPolylines(): GeoPolylines;
    private _sPositionsEditing;
    get sPositionsEditing(): PositionsEditing;
    constructor(sceneObject: ESVisibilityAnalysis, czmViewer: CzmViewer);
    flyTo(duration: number | undefined, id: number): boolean;
    flyIn(duration: number | undefined, id: number): boolean;
}
