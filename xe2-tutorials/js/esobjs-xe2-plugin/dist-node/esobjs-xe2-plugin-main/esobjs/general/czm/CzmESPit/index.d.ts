import { CzmViewer } from "xbsj-xe2/dist-node/xe2-cesium-objects";
import { ESPit } from "../../objs";
import { CzmESGeoPolygon } from "../CzmESGeoPolygon";
declare type ESPointLLH = [number, number, number];
export declare class CzmESPit extends CzmESGeoPolygon<ESPit> {
    static readonly type: void;
    private _judgePoints;
    get judgePoints(): ESPointLLH[];
    set judgePoints(value: ESPointLLH[]);
    get judgePointsChanged(): import("xbsj-renderer/dist-node/xr-base-utils").Listener<[ESPointLLH[], ESPointLLH[]]>;
    constructor(sceneObject: ESPit, czmViewer: CzmViewer);
    flyTo(duration: number | undefined, id: number): boolean;
}
export {};
