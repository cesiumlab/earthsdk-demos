import { CzmESLocalVector } from 'esobjs-xe2-plugin/dist-node/esobjs-xe2-plugin-main/esobjs/base';
import { GeoPolyline } from 'xbsj-xe2/dist-node/xe2-base-objects';
import { CzmViewer, CzmWaterPrimitive } from 'xbsj-xe2/dist-node/xe2-cesium-objects';
import { ESDynamicWater } from '../../objs';
export declare class CzmESDynamicWater extends CzmESLocalVector<ESDynamicWater> {
    static readonly type: void;
    private _geoPolyline;
    get geoPolyline(): GeoPolyline;
    private _czmWaterPrimitive;
    get czmWaterPrimitive(): CzmWaterPrimitive;
    constructor(sceneObject: ESDynamicWater, czmViewer: CzmViewer);
    flyTo(duration: number | undefined, id: number): boolean;
}
