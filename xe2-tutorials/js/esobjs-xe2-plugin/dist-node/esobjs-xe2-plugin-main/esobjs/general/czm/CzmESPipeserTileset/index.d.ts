import { CzmViewer } from 'xbsj-xe2/dist-node/xe2-cesium-objects';
import { ESPipeserTileset } from '../../objs';
import { CzmES3DTileset } from '../CzmES3DTileset';
export declare class CzmESPipeserTileset extends CzmES3DTileset<ESPipeserTileset> {
    static readonly type: void;
    constructor(sceneObject: ESPipeserTileset, czmViewer: CzmViewer);
    flyTo(duration: number | undefined, id: number): boolean;
    flyIn(duration: number | undefined, id: number): boolean;
}
