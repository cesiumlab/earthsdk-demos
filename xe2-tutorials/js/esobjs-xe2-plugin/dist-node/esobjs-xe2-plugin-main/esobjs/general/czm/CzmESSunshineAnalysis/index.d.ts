import { CzmPointPrimitiveCollection, CzmViewer } from 'xbsj-xe2/dist-node/xe2-cesium-objects';
import { ESSunshineAnalysis } from '../../objs';
import { CzmESGeoPolygon } from "../CzmESGeoPolygon";
export declare class CzmESSunshineAnalysis extends CzmESGeoPolygon<ESSunshineAnalysis> {
    static readonly type: void;
    private _czmPointPrimitiveCollection;
    get czmPointPrimitiveCollection(): CzmPointPrimitiveCollection;
    constructor(sceneObject: ESSunshineAnalysis, czmViewer: CzmViewer);
    flyTo(duration: number | undefined, id: number): boolean;
    flyIn(duration: number | undefined, id: number): boolean;
}
