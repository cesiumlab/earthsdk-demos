import { Event, JsonValue, PartialWithUndefinedReactivePropsToNativeProps, ReactivePropsToNativePropsAndChanged } from "xbsj-xe2/dist-node/xe2-base-utils";
import { ESVisualObject } from "../../base/objs";
export declare function getFinalCzm3DTilesUrlString(czm3DTilesUrl: string): string;
export declare function getCzmCodeFromES3DTileset(eS3DTileset: ES3DTileset): string | undefined;
export declare type FeatureColorJsonType = {
    value?: string | number;
    minValue?: number;
    maxValue?: number;
    rgba: [number, number, number, number];
};
export declare type FeatureVisableJsonType = {
    value?: string | number;
    minValue?: number;
    maxValue?: number;
    visable: boolean;
};
/**
 * https://www.wolai.com/earthsdk/scb9Mm6X1zR4GypJQreRvK
 */
export declare class ES3DTileset extends ESVisualObject {
    static readonly type: string;
    get typeName(): string;
    get defaultProps(): {
        show: boolean;
        collision: boolean;
        allowPicking: boolean;
        flyToParam: import("xbsj-xe2/dist-node/xe2-base-utils").ReactiveVariable<import("../..").ESFlyToParam | undefined>;
        flyInParam: import("xbsj-xe2/dist-node/xe2-base-utils").ReactiveVariable<import("../..").ESFlyInParam | undefined>;
        execOnceFuncStr: string | undefined;
        updateFuncStr: string | undefined;
        toDestroyFuncStr: string | undefined;
        name: string;
        ref: string | undefined;
        devTags: import("xbsj-xe2/dist-node/xe2-base-utils").ReactiveVariable<string[] | undefined>;
        extras: import("xbsj-xe2/dist-node/xe2-base-utils").ReactiveVariable<JsonValue>;
        url: string;
        actorTag: string;
        materialMode: "normal" | "technology";
        highlight: boolean;
        maximumScreenSpaceError: number;
        highlightID: number;
        highlightColor: import("xbsj-xe2/dist-node/xe2-base-utils").ReactiveVariable<[number, number, number, number]>;
        offset: import("xbsj-xe2/dist-node/xe2-base-utils").ReactiveVariable<[number, number, number]>;
        rotation: import("xbsj-xe2/dist-node/xe2-base-utils").ReactiveVariable<[number, number, number]>;
        czmImageBasedLightingFactor: import("xbsj-xe2/dist-node/xe2-base-utils").ReactiveVariable<[number, number]>;
        czmLuminanceAtZenith: number;
        czmMaximumMemoryUsage: number;
        czmClassificationType: string;
        czmStyleJson: import("xbsj-xe2/dist-node/xe2-base-utils").ReactiveVariable<JsonValue>;
        czmBackFaceCulling: boolean;
        czmDebugShowBoundingVolume: boolean;
        czmDebugShowContentBoundingVolume: boolean;
        czmskipLevelOfDetail: boolean;
        clippingPlaneEnabled: boolean;
        unionClippingRegions: boolean;
        clippingPlaneEdgeColor: import("xbsj-xe2/dist-node/xe2-base-utils").ReactiveVariable<[number, number, number, number]>;
        clippingPlaneEdgeWidth: number;
    };
    get json(): JsonType;
    set json(value: JsonType);
    getCzmCode(): string | undefined;
    private _editing;
    get editing(): boolean;
    set editing(value: boolean);
    get editingChanged(): import("xbsj-xe2/dist-node/xe2-base-utils").Listener<[boolean, boolean]>;
    private _rotationEditing;
    get rotationEditing(): boolean;
    set rotationEditing(value: boolean);
    get rotationEditingChanged(): import("xbsj-xe2/dist-node/xe2-base-utils").Listener<[boolean, boolean]>;
    private _refreshTilesetEvent;
    get refreshTilesetEvent(): Event<[]>;
    refreshTileset(): void;
    private _czmTilesetReadyEvent;
    get czmTilesetReadyEvent(): Event<[tileset: any]>;
    private _highlightFeatureEvent;
    get highlightFeatureEvent(): Event<[id: string | number, color?: string | undefined]>;
    highlightFeature(id: string | number, color?: string): void;
    private _highlightFeatureAndFlyToEvent;
    get highlightFeatureAndFlyToEvent(): Event<[id: string | number, sphere: [number, number, number, number], color?: string | undefined, duration?: number | undefined]>;
    /**
    * @param id 节点id
    * @param sphere 笛卡尔坐标系[x, y, z, radius]
    * @param color 高亮颜色,不传就是默认颜色不高亮, 参数形式如 `rgba(255,0,0,1)`
    * @param duration 飞行时间，默认1s
    */
    highlightFeatureAndFlyTo(id: string | number, sphere: [number, number, number, number], color?: string, duration?: number): void;
    /**
     * 根据属性设置颜色
     */
    private _setFeatureColorEvent;
    get setFeatureColorEvent(): Event<[string, FeatureColorJsonType[]]>;
    setFeatureColor(featureName: string, json: FeatureColorJsonType[]): void;
    /**
     * 根据属性控制show
     */
    private _setFeatureVisableEvent;
    get setFeatureVisableEvent(): Event<[string, FeatureVisableJsonType[]]>;
    setFeatureVisable(featureName: string, json: FeatureVisableJsonType[]): void;
    /**
     * 还原样式设置
     */
    private _resetFeatureStyleEvent;
    get resetFeatureStyleEvent(): Event<[]>;
    resetFeatureStyle(): void;
    private _clippingPlanesId;
    get clippingPlanesId(): string;
    set clippingPlanesId(value: string);
    get clippingPlanesIdChanged(): import("xbsj-xe2/dist-node/xe2-base-utils").Listener<[string, string]>;
    /**
   * 面裁切 ESClippingPlane
   */
    private _clippingPlaneIds;
    get clippingPlaneIds(): string[];
    set clippingPlaneIds(value: string[]);
    get clippingPlaneIdsChanged(): import("xbsj-xe2/dist-node/xe2-base-utils").Listener<[string[], string[]]>;
    private _flattenedPlaneId;
    get flattenedPlaneId(): string;
    set flattenedPlaneId(value: string);
    get flattenedPlaneIdChanged(): import("xbsj-xe2/dist-node/xe2-base-utils").Listener<[string, string]>;
    private _flattenedPlaneEnabled;
    get flattenedPlaneEnabled(): boolean;
    set flattenedPlaneEnabled(value: boolean);
    get flattenedPlaneEnabledChanged(): import("xbsj-xe2/dist-node/xe2-base-utils").Listener<[boolean, boolean]>;
    /**
     * 体裁切 ESBoxClippingPlanes + 挖坑
     */
    private _excavateId;
    get excavateId(): string;
    set excavateId(value: string);
    get excavateIdChanged(): import("xbsj-xe2/dist-node/xe2-base-utils").Listener<[string, string]>;
    static defaults: {
        url: string;
        actorTag: string;
        materialMode: string;
        materialModes: [string, string][];
        offset: number[];
        show: boolean;
        collision: boolean;
        allowPicking: boolean;
        flyToParam: import("../..").ESFlyToParam;
        flyInParam: import("../..").ESFlyInParam;
        viewerTagsEnums: [string, string][];
    };
    getESProperties(): {
        basic: import("xbsj-xe2/dist-node/xe2-base-objects").Property[];
        dataSource: import("xbsj-xe2/dist-node/xe2-base-objects").Property[];
        general: import("xbsj-xe2/dist-node/xe2-base-objects").Property[];
        location: import("xbsj-xe2/dist-node/xe2-base-objects").Property[];
        coordinate: import("xbsj-xe2/dist-node/xe2-base-objects").Property[];
        style: import("xbsj-xe2/dist-node/xe2-base-objects").Property[];
    };
    getProperties(language?: string): import("xbsj-xe2/dist-node/xe2-base-objects").Property[];
}
export declare namespace ES3DTileset {
    const createDefaultProps: () => {
        show: boolean;
        collision: boolean;
        allowPicking: boolean;
        flyToParam: import("xbsj-xe2/dist-node/xe2-base-utils").ReactiveVariable<import("../..").ESFlyToParam | undefined>;
        flyInParam: import("xbsj-xe2/dist-node/xe2-base-utils").ReactiveVariable<import("../..").ESFlyInParam | undefined>;
        execOnceFuncStr: string | undefined;
        updateFuncStr: string | undefined;
        toDestroyFuncStr: string | undefined;
        name: string;
        ref: string | undefined;
        devTags: import("xbsj-xe2/dist-node/xe2-base-utils").ReactiveVariable<string[] | undefined>;
        extras: import("xbsj-xe2/dist-node/xe2-base-utils").ReactiveVariable<JsonValue>;
        url: string;
        actorTag: string;
        materialMode: "normal" | "technology";
        highlight: boolean;
        maximumScreenSpaceError: number;
        highlightID: number;
        highlightColor: import("xbsj-xe2/dist-node/xe2-base-utils").ReactiveVariable<[number, number, number, number]>;
        offset: import("xbsj-xe2/dist-node/xe2-base-utils").ReactiveVariable<[number, number, number]>;
        rotation: import("xbsj-xe2/dist-node/xe2-base-utils").ReactiveVariable<[number, number, number]>;
        czmImageBasedLightingFactor: import("xbsj-xe2/dist-node/xe2-base-utils").ReactiveVariable<[number, number]>;
        czmLuminanceAtZenith: number;
        czmMaximumMemoryUsage: number;
        czmClassificationType: string;
        czmStyleJson: import("xbsj-xe2/dist-node/xe2-base-utils").ReactiveVariable<JsonValue>;
        czmBackFaceCulling: boolean;
        czmDebugShowBoundingVolume: boolean;
        czmDebugShowContentBoundingVolume: boolean;
        czmskipLevelOfDetail: boolean;
        clippingPlaneEnabled: boolean;
        unionClippingRegions: boolean;
        clippingPlaneEdgeColor: import("xbsj-xe2/dist-node/xe2-base-utils").ReactiveVariable<[number, number, number, number]>;
        clippingPlaneEdgeWidth: number;
    };
}
export interface ES3DTileset extends ReactivePropsToNativePropsAndChanged<ReturnType<typeof ES3DTileset.createDefaultProps>> {
}
declare type JsonType = PartialWithUndefinedReactivePropsToNativeProps<ReturnType<typeof ES3DTileset.createDefaultProps> & {
    type: string;
}>;
export {};
