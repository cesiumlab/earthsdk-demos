import { ReactParamsType } from "xbsj-renderer/dist-node/xr-base-utils";
export declare function createEnvEvalStrReact(reactVar: ReactParamsType<string | undefined>, defaultValue?: string): import("xbsj-renderer/dist-node/xr-base-utils").ReactiveVariable<string | undefined>;
