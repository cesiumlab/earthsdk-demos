export declare function extrudePolyline(keyPosition2Ds: number[], width: number, loop?: boolean, targetPosition2Ds?: number[], targetSts?: number[], targetIndices?: number[]): void;
