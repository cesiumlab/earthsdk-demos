export declare function nearestPointFromLineSegment(a: [number, number], b: [number, number], c: [number, number]): [number, number] | undefined;
