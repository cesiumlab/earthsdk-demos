export declare function getCurrentScriptPath(): string;
export declare function getDirAndFileNameFromPath(path: string): [dir: string, fileName: string] | undefined;
export declare function getNameAndExtFromFileName(fileName: string): [name: string, ext: string] | undefined;
export declare function registerScript(): void;
