import { Destroyable } from 'xbsj-renderer/dist-node/xr-base-utils';
export declare class UnLoading extends Destroyable {
    private _unloadEvent;
    constructor();
    unload(func: () => void): void;
}
export declare const unLoading: UnLoading;
