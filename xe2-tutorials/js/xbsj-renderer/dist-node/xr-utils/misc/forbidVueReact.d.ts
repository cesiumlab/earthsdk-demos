/**
 * 注意这是一个类的装饰器
 *
 * @example
 * @forbidVueReact
 * class XXX {
 * }
 *
 * @param target 目标类
 */
export declare function forbidVueReact(target: Function): void;
