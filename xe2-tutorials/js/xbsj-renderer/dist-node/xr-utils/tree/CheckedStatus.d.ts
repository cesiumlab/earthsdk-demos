export declare type CheckedStatus = 'checked' | 'unchecked' | 'indeterminate';
