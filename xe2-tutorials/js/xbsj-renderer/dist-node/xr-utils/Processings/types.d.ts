export declare class CancelError extends Error {
}
export declare type CancelFuncType = (cancelError?: CancelError) => void;
