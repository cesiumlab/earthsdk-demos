export declare type ComplexImageType = 'img' | 'gif' | 'video' | 'flv' | 'hls';
export declare const complexImageTypes: string[];
