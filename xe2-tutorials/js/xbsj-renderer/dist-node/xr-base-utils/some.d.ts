export declare function some<T>(iteratorOrIterable: Iterable<T> | Iterator<T>, callbackfn: (currentValue: T, currentIndex: number) => boolean): boolean;
