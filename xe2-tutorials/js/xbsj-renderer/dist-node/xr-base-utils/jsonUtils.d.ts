import { JsonValue } from "./JsonValue";
export declare function toJsonStr(json: JsonValue): string;
export declare function printJson(json: JsonValue): void;
