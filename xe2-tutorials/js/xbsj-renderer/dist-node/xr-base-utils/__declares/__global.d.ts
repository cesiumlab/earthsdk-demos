// 如果是一个独立的声明文件，就不需要declare global 

declare global {
    export interface Window {
        [k: string]: any;
    }
}

export {};