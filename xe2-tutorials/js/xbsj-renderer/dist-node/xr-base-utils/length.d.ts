export declare function length<T>(iteratorOrIterable: Iterable<T> | Iterator<T>): number;
