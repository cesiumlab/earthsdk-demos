export declare function fetchImage(url: string, crossOrigin?: string | null): Promise<HTMLImageElement>;
