export declare function every<T>(iteratorOrIterable: Iterable<T> | Iterator<T>, callbackfn: (currentValue: T, currentIndex: number) => boolean): boolean;
