export declare const copyright: {
    readonly owner: any;
    readonly ownerlink: any;
    readonly info: string;
    readonly date: any;
    readonly author: any;
    readonly version: any;
    readonly name: any;
    readonly commitId: any;
    print(): void;
};
