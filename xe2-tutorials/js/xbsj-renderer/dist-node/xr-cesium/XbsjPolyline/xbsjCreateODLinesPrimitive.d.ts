/// <reference path="../../../../../../node_modules/xbsj-renderer/dist-node/xr-cesium/__declares/cesium.d.ts" />

import * as Cesium from 'cesium';


export type XbsjRoutePath = {
    positions: ([number, number] | [number, number, number])[];
    width: number;
    // color?: [number, number, number, number];
    // /**
    //  * 单位秒？
    //  */
    // startTime?: number;
    // /**
    //  * 单位秒
    //  */
    // duration?: number;
    [k: string]: any;
} | {
    startPos: [number, number, number];
    endPos: [number, number, number];
    heightRatio: number;
    width: number;
    // color: [number, number, number, number];
    // /**
    //  * 单位秒？
    //  */
    // startTime?: number;
    // /**
    //  * 单位秒
    //  */
    // duration?: number;
    [k: string]: any;
}

export type XbsjPositionCallbackResultType = {
    timeRatio: number;
    repeat: number;
    color: [number, number, number, number];
    bgColor: [number, number, number, number];
    bidirectional: 0|1|2|3; // 0/1表示单向，2表示双向, 3表示无信号
};

export type XbsjODLinesPostionCallback = (
    instanceIndex: number,
    frameState: Cesium.FrameState,
    result: XbsjPositionCallbackResultType,
) => XbsjPositionCallbackResultType;

export default function xbsjCreateODLinesPrimitive(
    routePaths: XbsjRoutePath[],
    color: [number, number, number, number],
    arcType: Cesium.ArcType,
    postionCallback: (instanceIndex: number, frameState: Cesium.FrameState | undefined, result: XbsjPositionCallbackResultType) => XbsjPositionCallbackResultType,
    brightening?: boolean,
    depthTest?: boolean,
    getTextureFunc?: () => Cesium.Texture,
): Cesium.Primitive;

