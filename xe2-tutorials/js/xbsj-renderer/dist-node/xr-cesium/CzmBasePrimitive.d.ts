import { Destroyable } from 'xbsj-renderer/dist-node/xr-base-utils';
import * as Cesium from 'cesium';
export declare abstract class CzmBasePrimitive extends Destroyable {
    _show: import("xbsj-renderer/dist-node/xr-base-utils").ReactiveVariable<boolean>;
    abstract update(frameState: Cesium.FrameState): void;
    get show(): boolean;
    set show(value: boolean);
    get showChanged(): import("xbsj-renderer/dist-node/xr-base-utils").Listener<[boolean, boolean]>;
}
