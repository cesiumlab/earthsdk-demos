import * as Cesium from 'cesium';
/**
 * 用来实现指定位置的物体偏移
 * @todo 目前没有考虑ReferenceFrame的问题，默认是Fixed模式
 */
export declare class OffsetPositionProperty {
    _definitionChanged: Cesium.Event<(...args: any[]) => void>;
    _getValue: (time: Cesium.JulianDate, result: Cesium.Cartesian3) => Cesium.Cartesian3 | undefined;
    constructor(offset: {
        direction: number;
        up: number;
        right: number;
    }, positionProp: Cesium.PositionProperty, orientationProp: Cesium.VelocityOrientationProperty);
    get isConstant(): boolean;
    get definitionChanged(): Cesium.Event<(...args: any[]) => void>;
    get referenceFrame(): Cesium.ReferenceFrame;
    getValue(time: Cesium.JulianDate, result: Cesium.Cartesian3): any;
    getValueInReferenceFrame(time: Cesium.JulianDate, referenceFrame: Cesium.ReferenceFrame, result: Cesium.Cartesian3): any;
    equals(other: OffsetPositionProperty): boolean;
}
