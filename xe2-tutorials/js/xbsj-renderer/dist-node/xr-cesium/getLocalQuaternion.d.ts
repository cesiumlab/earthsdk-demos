import * as Cesium from 'cesium';
/**
 * 假设本地模型+z为正向，+y为向上轴
 * @param rotation
 * @param result
 * @returns
 */
export declare function getLocalQuaternion(rotation: [number, number, number], result?: Cesium.Quaternion): Cesium.Quaternion;
