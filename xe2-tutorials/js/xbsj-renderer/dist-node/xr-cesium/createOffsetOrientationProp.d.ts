import * as Cesium from 'cesium';
export declare function createOffsetOrientationProp(offsetHpr: Cesium.HeadingPitchRoll, orientationProp: Cesium.VelocityOrientationProperty): Cesium.CallbackProperty;
