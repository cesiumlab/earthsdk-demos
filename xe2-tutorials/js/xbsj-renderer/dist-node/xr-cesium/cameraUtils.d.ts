import * as Cesium from 'cesium';
export declare function getCameraTargetPos(position: [number, number, number], rotation: [number, number, number], viewDistance: number, result?: [number, number, number]): [number, number, number] | undefined;
export declare function getCameraPosition(camera: Cesium.Camera, result?: [number, number, number]): [number, number, number];
export declare function getCameraRotation(camera: Cesium.Camera, result?: [number, number, number]): [number, number, number];
