import { Destroyable } from "xbsj-renderer/dist-node/xr-base-utils";
import { ComplexImage, CzmTextureCopyParams, ObjResettingWithEvent } from "xbsj-renderer/dist-node/xr-utils";
import * as Cesium from 'cesium';
export declare class CzmTexture extends Destroyable {
    private _czmViewer;
    private _comlexImage;
    get czmViewer(): Cesium.Viewer;
    get comlexImage(): ComplexImage;
    private _nativeTextureResetting;
    get nativeTextureResetting(): ObjResettingWithEvent<Cesium.Texture, import("xbsj-renderer/dist-node/xr-base-utils").NextAnimateFrameEvent>;
    get nativeTextureChanged(): import("xbsj-renderer/dist-node/xr-base-utils").Listener<[Cesium.Texture | undefined, Cesium.Texture | undefined]>;
    get nativeTexture(): Cesium.Texture | undefined;
    private _copyTexture;
    copyTexture(params: CzmTextureCopyParams): void;
    constructor(_czmViewer: Cesium.Viewer, _comlexImage: ComplexImage);
}
