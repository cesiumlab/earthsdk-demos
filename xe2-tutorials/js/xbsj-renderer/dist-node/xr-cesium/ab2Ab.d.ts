import * as Cesium from 'cesium';

/**
 * 指定模型坐标a和b，和对应在地球上的A和B，求模型的位置姿态信息
 * 
 * @param a 模型本地坐标点a，从3dmax中获取，数值应该都不大
 * @param b 模型本地坐标点b，从3dmax中获取，数值应该都不大
 * @param A 以地球球心为原点建立的笛卡尔坐标系下的坐标点A，数值应该会很大
 * @param B 以地球球心为原点建立的笛卡尔坐标系下的坐标点A，数值应该会很大
 * @returns {
 *      scale: number, // 缩放值  
 *      rotation: Cesium.Quaternion, // 四元数旋转量  
 *      positionInCartesian: Cesium.Cartesian3, // 位置 世界坐标系  
 *      positionInRadians: Cesium.HeadingPitchRoll, // 位置 经纬度弧度  
 *      positionInDegrees: {  
 *          longitude: number,  
 *          latitude: number,  
 *          height: number,  
 *      }, // 位置 经纬度度数  
 *      hpr: Cesium.HeadingPitchRoll, // 欧拉角，弧度单位  
 *      hprInDegrees: {  
 *          heading: number,  
 *          pitch: number,  
 *          roll: number,  
 *      }, // 欧拉角，度数单位  
 *      modelMatrix: Cesium.Matrix4, // 矩阵  
 * }  
 * 
 * @example 
 * 
 * const { modelMatrix } = ab2Ab(new Cesium.Cartesian3((0, 0, -1), new Cesium.Cartesian3(0, 0, 1), Cesium.Cartesian3.fromDegrees(116.39, 39.9, 100), Cesium.Cartesian3.fromDegrees(116.39, 39.9, 1000)));
 */
export function ab2Ab(a: Cesium.Cartesian3, b: Cesium.Cartesian3, A: Cesium.Cartesian3, B: Cesium.Cartesian3): {
    scale: number, // 缩放值  
    rotation: Cesium.Quaternion, // 四元数旋转量  
    positionInCartesian: Cesium.Cartesian3, // 位置 世界坐标系  
    positionInRadians: Cesium.HeadingPitchRoll, // 位置 经纬度弧度  
    positionInDegrees: {  
        longitude: number,  
        latitude: number,  
        height: number,  
    }, // 位置 经纬度度数  
    hpr: Cesium.HeadingPitchRoll, // 欧拉角，弧度单位  
    hprInDegrees: {  
        heading: number,  
        pitch: number,  
        roll: number,  
    }, // 欧拉角，度数单位  
    modelMatrix: Cesium.Matrix4, // 矩阵  
};

