import * as Cesium from 'cesium';
/**
 * ENU坐标系下，北向为前向，heading为0时朝向正北
 * @param rotation
 * @param result
 * @returns
 */
export declare function getGeoQuaternion(rotation: [number, number, number], result?: Cesium.Quaternion): Cesium.Quaternion;
