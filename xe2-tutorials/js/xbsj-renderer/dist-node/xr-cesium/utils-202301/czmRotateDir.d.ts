import * as Cesium from 'cesium';
export declare function czmRotateDir(dir: Cesium.Cartesian3, quat: Cesium.Quaternion, result: Cesium.Cartesian3): Cesium.Cartesian3;
