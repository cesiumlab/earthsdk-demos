import * as Cesium from 'cesium';
export declare function createTextureFromImage(context: Cesium.Context, image: HTMLImageElement): Cesium.Texture;
