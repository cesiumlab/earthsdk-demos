import * as Cesium from 'cesium';
export declare function czmGetQuatFromDirs(fromDir: Cesium.Cartesian3, toDir: Cesium.Cartesian3, result?: Cesium.Quaternion): Cesium.Quaternion;
