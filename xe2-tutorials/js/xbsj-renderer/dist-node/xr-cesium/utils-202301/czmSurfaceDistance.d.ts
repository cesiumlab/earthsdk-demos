import * as Cesium from 'cesium';
export declare function czmSurfaceDistance(c0: Cesium.Cartesian3, c1: Cesium.Cartesian3): number;
