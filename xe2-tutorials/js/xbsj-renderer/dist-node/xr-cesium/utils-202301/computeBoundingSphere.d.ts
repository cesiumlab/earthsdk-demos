export declare function computeBoundingSphere(positions: [number, number, number][]): [center: [number, number, number], radius: number] | undefined;
