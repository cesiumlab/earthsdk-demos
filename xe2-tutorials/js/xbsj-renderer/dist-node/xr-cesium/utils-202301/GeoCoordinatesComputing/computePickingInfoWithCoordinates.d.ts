import * as Cesium from 'cesium';
export declare type CoordinatesComputingPickingInfo = {
    constraintMode: 'x' | 'y' | 'z' | 'xy' | 'zAxis' | 'none';
    startDragPos: [number, number, number];
};
export declare type GeoCoordinatesComputingInfo = {
    readonly position: [number, number, number];
    readonly dimensions: [number, number, number];
    readonly heading: number;
};
export declare function computePickingInfoWithCoordinates(pointerEvent: PointerEvent, scene: Cesium.Scene, coordinates: GeoCoordinatesComputingInfo, axisSnapPixelSize: number, result: CoordinatesComputingPickingInfo, disabledOptions?: {
    x?: boolean;
    y?: boolean;
    xy?: boolean;
    z?: boolean;
    zAxis?: boolean;
}): true | undefined;
