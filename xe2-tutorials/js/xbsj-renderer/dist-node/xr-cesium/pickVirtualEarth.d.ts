import * as Cesium from 'cesium';
export declare function pickVirtualEarth(scene: Cesium.Scene, windowCoordinates: Cesium.Cartesian2, height: number, result?: Cesium.Cartographic): Cesium.Cartographic | undefined;
