import * as Cesium from 'cesium';
export declare function flyAround(camera: Cesium.Camera, position: [number, number, number], stopRadius: number | undefined, flyToDuration: number, // 单位毫秒 
aroundSpeed?: number, // 角速度 度/毫秒
placeCenter?: boolean, cancelCallback?: () => void): Promise<void>;
