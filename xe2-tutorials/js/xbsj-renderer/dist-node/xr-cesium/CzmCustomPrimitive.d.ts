import * as Cesium from 'cesium';
import { ReactivePropsToNativePropsAndChanged } from 'xbsj-renderer/dist-node/xr-base-utils';
import { CzmBasePrimitive } from "./CzmBasePrimitive";
export declare type AttributeType = {
    typedArray: Float32Array | Float64Array | Int8Array | Uint8Array | Int16Array | Uint16Array | Int32Array | Uint32Array;
    componentsPerAttribute: 1 | 2 | 3 | 4;
    usage?: Cesium.BufferUsage;
    normalize?: boolean;
};
export declare type AttributesType = {
    [k: string]: AttributeType;
};
export declare type IndexType = Uint8Array | Uint16Array | Uint32Array;
export declare type BoundingVolumeType = Cesium.BoundingSphere | Cesium.BoundingRectangle | Cesium.OrientedBoundingBox;
export declare class CzmCustomPrimitive extends CzmBasePrimitive {
    private _scene;
    private _updateEvent;
    private _drawCommand?;
    get drawCommand(): Cesium.DrawCommand | undefined;
    constructor(_scene: Cesium.Scene);
    update(frameState: Cesium.FrameState): void;
}
export declare namespace CzmCustomPrimitive {
    const createDefaultProps: () => {
        modelMatrix: import("xbsj-renderer/dist-node/xr-base-utils").ReactiveVariable<Cesium.Matrix4>;
        boundingVolume: BoundingVolumeType | undefined;
        pass: Cesium.Pass;
        attributes: AttributesType;
        cull: boolean;
        occlude: boolean;
        count: number | undefined;
        offset: number;
        indexTypedArray: IndexType | undefined;
        instanceCount: number;
        vertexShaderSource: string;
        fragmentShaderSource: string;
        castShadows: boolean;
        receiveShadows: boolean;
        uniformMap: Cesium.UniformMap;
        renderState: Cesium.RenderStateOptions;
        primitiveType: Cesium.PrimitiveType;
        executeInClosestFrustum: boolean;
        owner: Object | undefined;
        debugShowBoundingVolume: boolean;
        debugOverlappingFrustums: number;
        allowPicking: boolean | undefined;
        pickOnly: boolean;
        depthForTranslucentClassification: boolean;
    };
}
export interface CzmCustomPrimitive extends ReactivePropsToNativePropsAndChanged<ReturnType<typeof CzmCustomPrimitive.createDefaultProps>> {
}
