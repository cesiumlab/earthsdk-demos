import * as Cesium from 'cesium';
export declare function globePickPosition(scene: Cesium.Scene, windowPosition: Cesium.Cartesian2, result?: Cesium.Cartesian3): Cesium.Cartesian3 | undefined;
export declare function pickPosition(scene: Cesium.Scene, windowPosition: Cesium.Cartesian2, result?: Cesium.Cartesian3): Cesium.Cartesian3 | undefined;
