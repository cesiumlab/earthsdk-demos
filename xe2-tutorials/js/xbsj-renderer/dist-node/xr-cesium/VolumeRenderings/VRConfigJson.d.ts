declare type N16 = [number, number, number, number, number, number, number, number, number, number, number, number, number, number, number, number];
export declare type VRConfigJson = {
    "minCartesian": {
        "x": number;
        "y": number;
        "z": number;
    };
    "maxCartesian": {
        "x": number;
        "y": number;
        "z": number;
    };
    "stpProperty": string;
    "modelMatrix": N16;
    "inverseModelMatrix": N16;
    "origin": {
        "lon": number;
        "lat": number;
        "height": number;
    };
    "volumeTextureSize": [number, number, number, number];
    "sortedSizeInfo": [
        {
            "axisName": "x";
            "sizeInPixels": number;
            "sizeInMeters": number;
            "startInMeters": number;
            "endInMeters": number;
        },
        {
            "axisName": "y";
            "sizeInPixels": number;
            "sizeInMeters": number;
            "startInMeters": number;
            "endInMeters": number;
        },
        {
            "axisName": "z";
            "sizeInPixels": number;
            "sizeInMeters": number;
            "startInMeters": number;
            "endInMeters": number;
        }
    ];
    "minValue": number;
    "maxValue": number;
    "pngs": string[];
    "interval": number;
};
export {};
