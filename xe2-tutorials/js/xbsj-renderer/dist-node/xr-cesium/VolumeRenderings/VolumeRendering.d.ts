import * as Cesium from 'cesium';
import { Destroyable } from 'xbsj-renderer/dist-node/xr-base-utils';
import { VolumeRenderingPrimitive } from './VolumeRenderingPrimitive';
export declare class VolumeRendering extends Destroyable {
    private _scene;
    private _primitive;
    private _blendAlpha;
    constructor(_scene: Cesium.Scene, options: {
        volumeTextureSize: Cesium.Cartesian4;
    });
    get primitive(): VolumeRenderingPrimitive;
    get blendAlpha(): number;
    set blendAlpha(value: number);
    get blendAlphaChanged(): import("xbsj-renderer/dist-node/xr-base-utils").Listener<[number, number]>;
}
