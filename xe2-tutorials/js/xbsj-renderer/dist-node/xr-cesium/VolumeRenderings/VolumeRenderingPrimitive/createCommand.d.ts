import { CzmSmartCommand } from "../../CzmSmartCommand";
import * as Cesium from 'cesium';
import { VolumeRenderingPrimitive } from ".";
export declare function createCzmCommand(context: Cesium.Context, primitive: VolumeRenderingPrimitive, scene: Cesium.Scene): CzmSmartCommand;
