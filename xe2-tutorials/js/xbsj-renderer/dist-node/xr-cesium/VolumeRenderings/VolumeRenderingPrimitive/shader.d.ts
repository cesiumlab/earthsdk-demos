export declare const vertexShaderString = "\n    attribute vec3 position;\n    attribute vec3 normal;\n    attribute vec2 st;\n    attribute float batchId;\n    varying vec3 v_positionEC;\n    varying vec3 v_normalEC;\n    varying vec2 v_st;\n    void main() {\n        v_positionEC = (czm_modelView * vec4(position, 1.0)).xyz; \n        v_normalEC = czm_normal * normal; \n        v_st = st;\n        gl_Position = czm_modelViewProjection * vec4(position, 1.0);\n    }\n    ";
export declare function getFragmentShaderString(options: {
    /**
     * @type { 'max' | 'opaque' | 'accum' | 'accumColor'}
     */
    mode: 'max' | 'opaque' | 'accum' | 'accumColor';
    shaded: boolean;
    minIntensity: number;
    maxIntensity: number;
    fragColorFunctionString: string;
    stepSize: number;
    maxSteps: number;
    stpProperty: string;
    alphaRatio: number;
}): string;
