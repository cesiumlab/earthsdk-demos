import * as Cesium from 'cesium';
/**
 * 在地球表面寻找寻找距离某点最近的线上的一个点
 * 类似turf的nearestPointOnLine，但是不同之处在于：
 * 1. 不受线段的限制，可以超出线段
 * 2. 假设地球为正圆进行的计算，虽然也不准，但是比turf用runmb形式获取的最近点准确很多！
 *
 * 注意，高程不准确
 *
 * @param surfaceAxisStartCartesian 线段起点
 * @param surfaceAxisStopCartesian 线段终点
 * @param surfaceCartesian 地球上某点
 * @param result 求取的线段上的最近点，注意高程不准确！
 * @returns
 */
export declare function getSurfaceAxisClosestPoint(surfaceAxisStartCartesian: Cesium.Cartesian3, surfaceAxisStopCartesian: Cesium.Cartesian3, surfaceCartesian: Cesium.Cartesian3, result?: Cesium.Cartesian3): Cesium.Cartesian3;
