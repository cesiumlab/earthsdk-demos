import * as Cesium from 'cesium';
export declare function pickHeightPosition(scene: Cesium.Scene, pivot: Cesium.Cartesian3, windowCoordinates: Cesium.Cartesian2, result?: Cesium.Cartesian3): Cesium.Cartesian3 | undefined;
