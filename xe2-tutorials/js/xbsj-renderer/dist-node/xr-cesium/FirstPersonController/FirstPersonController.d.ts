/// <reference path="../fixCamera/fixCameraFlight.d.ts" />
/// <reference types="cesium" />
/// <reference types="xbsj-renderer/dist-node/xr-cesium/__declares/__cesium" />
import { Destroyable } from 'xbsj-renderer/dist-node/xr-base-utils';
import { KeyboardCameraController } from './KeyboardCameraController';
import { MouseCameraController } from './MouseCameraController';
export declare class FirstPersonController extends Destroyable {
    private _viewer;
    get viewer(): import("cesium").Viewer;
    private _mouseCameraController;
    get mouseCameraController(): MouseCameraController;
    get mouseEnabled(): boolean;
    set mouseEnabled(value: boolean);
    get mouseEnabledChanged(): import("xbsj-renderer/dist-node/xr-base-utils").Listener<[boolean, boolean]>;
    private _keyboardCameraController;
    get keyboardCameraController(): KeyboardCameraController;
    get keyboardEnabled(): boolean;
    set keyboardEnabled(value: boolean);
    get keyboardEnabledChanged(): import("xbsj-renderer/dist-node/xr-base-utils").Listener<[boolean, boolean]>;
    constructor(_viewer: Cesium.Viewer);
}
