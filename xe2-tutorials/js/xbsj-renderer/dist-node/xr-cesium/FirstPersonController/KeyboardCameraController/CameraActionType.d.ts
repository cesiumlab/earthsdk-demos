export declare type CameraActionType = 'WithCamera' | 'MoveForward' | 'MoveBackword' | 'MoveRight' | 'MoveLeft' | 'MoveUp' | 'MoveDown' | 'SpeedUp' | 'SpeedDown' | 'SwitchAlwaysWithCamera';
