import { Destroyable, Listener } from "xbsj-renderer/dist-node/xr-base-utils";
import { FirstPersonController } from "../FirstPersonController";
import { ObjResettingWithEvent } from "xbsj-renderer/dist-node/xr-utils";
import { KeyboardCameraControllerRunning } from "./KeyboardCameraControllerRunning";
import { CameraActionType } from "./CameraActionType";
export declare class KeyboardCameraController extends Destroyable {
    private _firstPersonController;
    get firstPersonController(): FirstPersonController;
    private _enabled;
    get enabled(): boolean;
    set enabled(value: boolean);
    get enabledChanged(): Listener<[boolean, boolean]>;
    static readonly defaultKeyStatusMap: {
        [k: string]: CameraActionType;
    };
    private _keyStatusMap;
    get keyStatusMap(): {
        [k: string]: CameraActionType;
    };
    set keyStatusMap(value: {
        [k: string]: CameraActionType;
    });
    get keyStatusMapChanged(): Listener<[{
        [k: string]: CameraActionType;
    }, {
        [k: string]: CameraActionType;
    }]>;
    private _speed;
    get speed(): number;
    set speed(value: number);
    get speedChanged(): Listener<[number, number]>;
    private _alwaysWithCamera;
    get alwaysWithCamera(): boolean;
    set alwaysWithCamera(value: boolean);
    get alwaysWithCameraChanged(): Listener<[boolean, boolean]>;
    private _keyDownEvent;
    get keyDownEvent(): Listener<[KeyboardEvent]>;
    keyDown(event: KeyboardEvent): void;
    private _keyUpEvent;
    get keyUpEvent(): Listener<[KeyboardEvent]>;
    keyUp(event: KeyboardEvent): void;
    private _abortEvent;
    get abortEvent(): Listener<[]>;
    abort(): void;
    private _keyboardResetting;
    get keyboardResetting(): ObjResettingWithEvent<KeyboardCameraControllerRunning, Listener<[boolean, boolean]>>;
    constructor(_firstPersonController: FirstPersonController);
}
