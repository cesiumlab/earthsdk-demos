import { Destroyable } from "xbsj-renderer/dist-node/xr-base-utils";
import { MouseMoving } from "./MouseMoving";
import { MouseCameraController } from "./MouseCameraController";
export declare class MouseCameraControllerRunning extends Destroyable {
    private _mouseCameraController;
    static origin_setPointerCapture: (pointerId: number) => void;
    get mouseCameraController(): MouseCameraController;
    get firstPersonController(): import("..").FirstPersonController;
    private _mouseMoving;
    get mouseMoving(): MouseMoving;
    constructor(_mouseCameraController: MouseCameraController);
}
