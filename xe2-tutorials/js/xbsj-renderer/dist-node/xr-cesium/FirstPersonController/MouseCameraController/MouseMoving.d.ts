import { Destroyable } from "xbsj-renderer/dist-node/xr-base-utils";
import { FirstPersonController } from "../FirstPersonController";
import { MouseCameraController } from "./MouseCameraController";
export declare class MouseMoving extends Destroyable {
    private _firstPersonController;
    private _mouseCameraController;
    constructor(_firstPersonController: FirstPersonController, _mouseCameraController: MouseCameraController);
}
