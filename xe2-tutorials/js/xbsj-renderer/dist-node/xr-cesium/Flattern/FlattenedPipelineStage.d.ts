/**
 * The tileset pipeline stage is responsible for updating the model with behavior
 * specific to 3D Tiles.
 *
 * @namespace FlattenedPipelineStage
 *
 * @private
 */
declare const FlattenedPipelineStage: {
    name: string;
};
export default FlattenedPipelineStage;
