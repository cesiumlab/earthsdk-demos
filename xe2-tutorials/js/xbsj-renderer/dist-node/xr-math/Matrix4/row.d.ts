import { Number16, Number4 } from '../Numbers';
export declare function _getRow(matrix: Readonly<Number16>, index: number, target?: Number4): Number4;
export declare function _setRow(matrix: Readonly<Number16>, index: number, cartesian: Readonly<Number4>, target?: Number16): Number16;
