import { TimePosRotType } from "../SceneObjects/GeoObjects/GeoPath";
export declare function getDistancesAndTimePosRotsFromPositions(positions: [number, number, number][], arcType: "NONE" | "GEODESIC" | "RHUMB" | undefined, granularity: number): {
    timePosRots: TimePosRotType[];
    distances: number[];
} | undefined;
