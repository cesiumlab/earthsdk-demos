import { Destroyable, Event, Listener, PartialWithUndefinedReactivePropsToNativeProps, ReactivePropsToNativePropsAndChanged } from 'xbsj-xe2/dist-node/xe2-base-utils';
import { SceneObjectKey, SceneObjectWithId } from 'xbsj-xe2/dist-node/xe2-utils';
import { ESSceneObject } from '../ESSceneObject';
import { PickedInfo } from '../../scene-manager';
import { GeoPolygon } from './GeoPolygon';
declare function saveCanvasToClipboard(canvas: HTMLCanvasElement): void;
declare class PolygonCanvasResetting extends Destroyable {
    private _geoPolygonCanvas;
    private _geoPolygon;
    static toMercatorPos: (lbh: [number, number, number]) => number[];
    constructor(_geoPolygonCanvas: GeoPolygonCanvas, _geoPolygon: GeoPolygon);
}
export declare type CanvasGeoInfoType = {
    rect: [number, number, number, number];
    height: number;
};
export declare class GeoPolygonCanvas extends ESSceneObject {
    static readonly type: string;
    get typeName(): string;
    get defaultProps(): {
        enabled: boolean;
        geoPolygonId: string;
        canvasWidth: number;
        execOnceFuncStr: string | undefined;
        updateFuncStr: string | undefined;
        toDestroyFuncStr: string | undefined;
        name: string;
        ref: string | undefined;
        devTags: import("xbsj-xe2/dist-node/xe2-base-utils").ReactiveVariable<string[] | undefined>;
        extras: import("xbsj-xe2/dist-node/xe2-base-utils").ReactiveVariable<import("xbsj-xe2/dist-node/xe2-base-utils").JsonValue>;
    };
    get json(): JsonType;
    set json(value: JsonType);
    private _pickedEvent;
    get pickedEvent(): Event<[PickedInfo]>;
    private _flyToEvent;
    get flyToEvent(): Listener<[number | undefined]>;
    flyTo(duration?: number): void;
    private _geoPolygonWithId;
    get geoPolygonWithId(): SceneObjectWithId<import("xbsj-xe2/dist-node/xe2-utils").SceneObject>;
    private _geoPolygonWithIdInit;
    private _canvas;
    get canvas(): HTMLCanvasElement;
    private _canvasCtx;
    get canvasCtx(): CanvasRenderingContext2D;
    private _canvasChanged;
    get canvasChanged(): Event<[]>;
    private _canvasGeoInfo;
    get canvasGeoInfo(): CanvasGeoInfoType | undefined;
    set canvasGeoInfo(value: CanvasGeoInfoType | undefined);
    get canvasGeoInfoChanged(): Listener<[CanvasGeoInfoType | undefined, CanvasGeoInfoType | undefined]>;
    static saveCanvasToClipboard: typeof saveCanvasToClipboard;
    static PolygonCanvasResetting: typeof PolygonCanvasResetting;
    private _polygonCanvasResetting;
    static defaults: {
        viewerTagsEnums: [string, string][];
    };
    constructor(id?: SceneObjectKey);
    getProperties(language?: string): import("../ESSceneObject").Property[];
}
export declare namespace GeoPolygonCanvas {
    const createDefaultProps: () => {
        enabled: boolean;
        geoPolygonId: string;
        canvasWidth: number;
        execOnceFuncStr: string | undefined;
        updateFuncStr: string | undefined;
        toDestroyFuncStr: string | undefined;
        name: string;
        ref: string | undefined;
        devTags: import("xbsj-xe2/dist-node/xe2-base-utils").ReactiveVariable<string[] | undefined>;
        extras: import("xbsj-xe2/dist-node/xe2-base-utils").ReactiveVariable<import("xbsj-xe2/dist-node/xe2-base-utils").JsonValue>;
    };
}
export interface GeoPolygonCanvas extends ReactivePropsToNativePropsAndChanged<ReturnType<typeof GeoPolygonCanvas.createDefaultProps>> {
}
declare type JsonType = PartialWithUndefinedReactivePropsToNativeProps<ReturnType<typeof GeoPolygonCanvas.createDefaultProps> & {
    type: string;
}>;
export {};
