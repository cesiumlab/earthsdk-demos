import { PickedInfo } from "../../../scene-manager";
import { Event, Listener, PartialWithUndefinedReactivePropsToNativeProps, ReactivePropsToNativePropsAndChanged } from "xbsj-xe2/dist-node/xe2-base-utils";
import { SceneObjectKey } from "xbsj-xe2/dist-node/xe2-utils";
import { ESSceneObject } from "../../ESSceneObject";
import { GeoCanvasPointPoi } from "../GeoCanvasPointPoi";
import { GeoPolyline, GeoPolylineArcType } from "../GeoPolyline";
import { Player } from "../../MiscObjects/Player";
import { CurrentInfoType } from "./CurrentInfoType";
import { RotLerpModeType } from "./RotLerpModeType";
import { GetCurrentFuncType } from "./GetCurrentFuncType";
import { getLeftRotation, getRightRotation } from "./getCurrent";
import { computeRotIfUndefinedUsingLerp, computeRotIfUndefinedUsingNextLine, computeRotIfUndefinedUsingPrevLine } from "./computeRotIfUndefined";
import { subdivide } from "./subdivide";
import { parseData } from "./parseData";
export { CurrentInfoType } from './CurrentInfoType';
export { RotLerpModeType };
export declare class GeoPath extends ESSceneObject {
    static readonly type: string;
    get typeName(): string;
    get defaultProps(): {
        show: boolean;
        currentPoiShow: boolean;
        timePosRots: import("xbsj-xe2/dist-node/xe2-base-utils").ReactiveVariable<TimePosRotType[] | undefined>;
        autoComputeTimeFromTimePosRots: boolean;
        leadTime: number;
        trailTime: number;
        startTime: number | undefined;
        stopTime: number | undefined;
        loop: boolean;
        currentTime: number;
        duration: number | undefined;
        speed: number;
        playing: boolean;
        polylineShow: boolean;
        width: number;
        ground: boolean;
        color: import("xbsj-xe2/dist-node/xe2-base-utils").ReactiveVariable<[number, number, number, number]>;
        hasDash: boolean;
        gapColor: import("xbsj-xe2/dist-node/xe2-base-utils").ReactiveVariable<[number, number, number, number]>;
        dashLength: number;
        dashPattern: number;
        hasArrow: boolean;
        depthTest: boolean;
        arcType: GeoPolylineArcType;
        allowPicking: boolean;
        dataUri: string;
        dataText: string | undefined;
        rotLerpMode: RotLerpModeType;
        debug: boolean;
        execOnceFuncStr: string | undefined;
        updateFuncStr: string | undefined;
        toDestroyFuncStr: string | undefined;
        name: string;
        ref: string | undefined;
        devTags: import("xbsj-xe2/dist-node/xe2-base-utils").ReactiveVariable<string[] | undefined>;
        extras: import("xbsj-xe2/dist-node/xe2-base-utils").ReactiveVariable<import("xbsj-xe2/dist-node/xe2-base-utils").JsonValue>;
    };
    get json(): JsonType;
    set json(value: JsonType);
    private _scratchCurrentInfo;
    private _currentInfo;
    get currentInfo(): CurrentInfoType | undefined;
    get currentInfoChanged(): Listener<[CurrentInfoType | undefined, CurrentInfoType | undefined]>;
    get currentIndex(): number | undefined;
    get currentIndexChanged(): Listener<[CurrentInfoType | undefined, CurrentInfoType | undefined]>;
    get currentPosition(): [number, number, number] | undefined;
    get currentPositionChanged(): Listener<[CurrentInfoType | undefined, CurrentInfoType | undefined]>;
    get currentRotation(): [number, number, number] | undefined;
    get currentRotationChanged(): Listener<[CurrentInfoType | undefined, CurrentInfoType | undefined]>;
    private _player;
    get player(): Player;
    private _geoPolyline;
    get geoPolyline(): GeoPolyline;
    private _geoCanvasPointPoi;
    get geoCanvasPointPoi(): GeoCanvasPointPoi;
    private _flyToEvent;
    get flyToEvent(): Listener<[number | undefined]>;
    flyTo(duration?: number): void;
    private _pickedEvent;
    get pickedEvent(): Event<[PickedInfo]>;
    private _accumDistancesChanged;
    private _accumDistancesChangedInit;
    get accumDistancesChanged(): Event<[]>;
    private _accumDistancesDirty;
    private _accumDistances;
    get accumDistances(): number[];
    get totalDistanceChanged(): Event<[]>;
    get totalDistance(): number;
    getCurrent(timeStamp: number): CurrentInfoType | undefined;
    subPath(startTimeStamp: number, stopTimeStamp: number): TimePosRotType[] | undefined;
    static computeRotIfUndefinedUsingPrevLine: typeof computeRotIfUndefinedUsingPrevLine;
    static computeRotIfUndefinedUsingNextLine: typeof computeRotIfUndefinedUsingNextLine;
    static computeRotIfUndefinedUsingLerp: typeof computeRotIfUndefinedUsingLerp;
    static getLeftRotation: typeof getLeftRotation;
    static getRightRotation: typeof getRightRotation;
    computeRotIfUndefinedUsingPrevLine(force?: boolean): void;
    computeRotIfUndefinedUsingNextLine(force?: boolean): void;
    computeRotIfUndefinedUsingLerp(force?: boolean): void;
    /**
     * @deprecated computeRotIfUndefined已弃用，请使用computeRotIfUndefinedUsingPrevLine
     * @param force 即使rotation不是undefined，也会被强制赋值
     */
    computeRotIfUndefined(force?: boolean): void;
    /**
     * 在控制点周边增加新的控制点
     * @param intervalDistance 间隔距离，单位是米
     * @param reserveOrigin 保留原控制点
     */
    addAroundPoints(intervalDistance: number, reserveOrigin: boolean): void;
    static subdivide: typeof subdivide;
    subdivide(arcType?: "GEODESIC" | "NONE" | "RHUMB", granularity?: number): void;
    private _getCurrentFunc;
    get getCurrentFunc(): GetCurrentFuncType | undefined;
    set getCurrentFunc(value: GetCurrentFuncType | undefined);
    get getCurrentFuncChanged(): Listener<[GetCurrentFuncType | undefined, GetCurrentFuncType | undefined]>;
    computeTimeFromTimePosRots(): void;
    static parseData: typeof parseData;
    static defaults: {
        timePosRots: TimePosRotType[];
        startTime: number;
        stopTime: number;
        loop: boolean;
        duration: number;
        playing: boolean;
        dataText: string;
        viewerTagsEnums: [string, string][];
    };
    constructor(id?: SceneObjectKey);
    get ratio(): number;
    set ratio(value: number);
    get ratioChanged(): Listener<[number, number]>;
    static timePosRotsMd: string;
    getProperties(language?: string): import("../../ESSceneObject").Property[];
}
export declare type TimePosRotType = [timeStamp: number, position: [longitude: number, latitude: number, height: number], rotation?: [heading: number, pitch: number, roll: number] | undefined];
export declare namespace GeoPath {
    const createDefaultProps: () => {
        show: boolean;
        currentPoiShow: boolean;
        timePosRots: import("xbsj-xe2/dist-node/xe2-base-utils").ReactiveVariable<TimePosRotType[] | undefined>;
        autoComputeTimeFromTimePosRots: boolean;
        leadTime: number;
        trailTime: number;
        startTime: number | undefined;
        stopTime: number | undefined;
        loop: boolean;
        currentTime: number;
        duration: number | undefined;
        speed: number;
        playing: boolean;
        polylineShow: boolean;
        width: number;
        ground: boolean;
        color: import("xbsj-xe2/dist-node/xe2-base-utils").ReactiveVariable<[number, number, number, number]>;
        hasDash: boolean;
        gapColor: import("xbsj-xe2/dist-node/xe2-base-utils").ReactiveVariable<[number, number, number, number]>;
        dashLength: number;
        dashPattern: number;
        hasArrow: boolean;
        depthTest: boolean;
        arcType: GeoPolylineArcType;
        allowPicking: boolean;
        dataUri: string;
        dataText: string | undefined;
        rotLerpMode: RotLerpModeType;
        debug: boolean;
        execOnceFuncStr: string | undefined;
        updateFuncStr: string | undefined;
        toDestroyFuncStr: string | undefined;
        name: string;
        ref: string | undefined;
        devTags: import("xbsj-xe2/dist-node/xe2-base-utils").ReactiveVariable<string[] | undefined>;
        extras: import("xbsj-xe2/dist-node/xe2-base-utils").ReactiveVariable<import("xbsj-xe2/dist-node/xe2-base-utils").JsonValue>;
    };
}
export interface GeoPath extends ReactivePropsToNativePropsAndChanged<ReturnType<typeof GeoPath.createDefaultProps>> {
}
declare type JsonType = PartialWithUndefinedReactivePropsToNativeProps<ReturnType<typeof GeoPath.createDefaultProps> & {
    type: string;
}>;
