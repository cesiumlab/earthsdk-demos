import { GeoPath, TimePosRotType } from ".";
export declare function subPath(geoPath: GeoPath, startTimeStamp: number, stopTimeStamp: number): TimePosRotType[] | undefined;
