import { CurrentInfoType } from "./CurrentInfoType";
import { GeoPath } from ".";
export declare type GetCurrentFuncType = (timeStamp: number, geoPath: GeoPath) => CurrentInfoType | undefined;
