import { Destroyable } from "xbsj-xe2/dist-node/xe2-base-utils";
import { GeoPath } from ".";
export declare class PolylineResetting extends Destroyable {
    constructor(geoPath: GeoPath);
}
