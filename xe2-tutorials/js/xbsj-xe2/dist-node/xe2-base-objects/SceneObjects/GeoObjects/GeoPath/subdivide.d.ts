import { TimePosRotType } from ".";
export declare function subdivide(timePosRots: TimePosRotType[], arcType?: "GEODESIC" | "NONE" | "RHUMB", granularity?: number): TimePosRotType[];
