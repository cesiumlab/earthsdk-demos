import { Destroyable } from "xbsj-xe2/dist-node/xe2-base-utils";
import { ViewCollection } from ".";
export declare const setFuncBtn: (sceneObject: ViewCollection, up: HTMLDivElement) => void;
export declare const setThumbnailList: (sceneObject: ViewCollection, bottom: HTMLDivElement) => void;
export declare class ViewCollectionToolUI extends Destroyable {
    private _tool;
    constructor(sceneObject: ViewCollection);
}
