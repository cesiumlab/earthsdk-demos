export declare function getFuncFromStr<T extends Function>(funcStr: string, args?: string[]): T | undefined;
