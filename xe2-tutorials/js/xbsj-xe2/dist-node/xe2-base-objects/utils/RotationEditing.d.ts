import { Destroyable, ReactParamsType } from "xbsj-xe2/dist-node/xe2-base-utils";
import { GeoRotator } from "../SceneObjects";
import { SceneObject } from '../scene-manager';
export declare class RotationEditing extends Destroyable {
    private _editingRef;
    get editingRef(): import("xbsj-xe2/dist-node/xe2-base-utils").ReactiveVariable<boolean>;
    private _geoRotator;
    get geoRotator(): GeoRotator;
    constructor(positionReactParam: ReactParamsType<[number, number, number] | undefined>, rotationReactParam: ReactParamsType<[number, number, number] | undefined>, rotationEditing: ReactParamsType<boolean> | undefined, components: {
        add: (sceneObject: SceneObject) => void;
        delete: (sceneObject: SceneObject) => void;
    }, options?: {
        initialRotation?: [number, number, number];
        showHelper?: boolean;
    });
}
