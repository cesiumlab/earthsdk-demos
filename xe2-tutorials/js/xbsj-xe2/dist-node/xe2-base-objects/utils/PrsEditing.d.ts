import { Destroyable, ReactParamsType } from "xbsj-xe2/dist-node/xe2-base-utils";
import { ESSceneObject, Viewer } from '../scene-manager';
import { PositionEditing } from "./PositionEditing";
import { RotationEditing } from "./RotationEditing";
export declare type PrsEditingMode = 'Position' | 'Rotation' | 'None';
export declare class PrsEditing extends Destroyable {
    private _positionReactParam;
    private _rotationReactParam;
    private _prsEditingReact;
    private _sceneObjectOrViewer;
    private _options?;
    private _editingMode;
    private _editingChanging;
    private _editingModeInit;
    get editingMode(): PrsEditingMode;
    set editingMode(value: PrsEditingMode);
    get editingModeChanged(): import("xbsj-xe2/dist-node/xe2-base-utils").Listener<[PrsEditingMode, PrsEditingMode]>;
    switch(): void;
    private _positionEditingReact;
    private _rotationEditingReact;
    private _componentsOrViewer;
    private _sPositionEditing;
    get sPositionEditing(): PositionEditing;
    private _sRotationEditing;
    get sRotationEditing(): RotationEditing;
    constructor(_positionReactParam: ReactParamsType<[number, number, number] | undefined>, _rotationReactParam: ReactParamsType<[number, number, number] | undefined>, _prsEditingReact: ReactParamsType<boolean> | undefined, _sceneObjectOrViewer: ESSceneObject | Viewer, _options?: {
        rotation?: {
            initialRotation?: [number, number, number] | undefined;
            showHelper?: boolean | undefined;
        } | undefined;
    } | undefined);
}
