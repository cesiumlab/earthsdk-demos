export declare function clamp0_360(angle: number): number;
export declare function clampN180_180(angle: number): number;
export declare function lerpAngle(leftAngle: number, rightAngle: number, ratio: number): number;
export declare function lerpRotation(leftRotation: [number, number, number], rightRotation: [number, number, number], ratio: number, targetRotation?: [number, number, number]): [number, number, number];
