export declare function map<T, R>(collection: Iterable<T>, func: (element: T) => R): R[];
export declare function equalsN3(left: [number, number, number] | undefined, right: [number, number, number] | undefined): boolean;
