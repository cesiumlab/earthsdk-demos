import { PixelStreaming } from '@epicgames-ps/lib-pixelstreamingfrontend-ue5.3';
import { Application } from '@epicgames-ps/lib-pixelstreamingfrontend-ui-ue5.3';
import { ESMsgWebSocketWrapper } from './ESMsgWebSocketWrapper';
export declare class ESUEViewer {
    private _domOrDomId;
    private _options;
    private _eventTarget;
    get eventTarget(): EventTarget;
    addEventListener(type: string, callback: EventListenerOrEventListenerObject | null, options?: AddEventListenerOptions | boolean): void;
    removeEventListener(type: string, callback: EventListenerOrEventListenerObject | null, options?: EventListenerOptions | boolean): void;
    dispatchEvent(event: Event): boolean;
    private _disposeFns;
    dispose(fn: Function): void;
    disposeVar<T extends {
        destroy(): undefined;
    }>(obj: T): T;
    destroy(): void;
    debug: boolean;
    forceNotUseEsMsgWs: boolean;
    private uiInteractionCallbacks;
    private _url;
    private _container;
    get container(): HTMLElement;
    private _pixelStream;
    get pixelStream(): PixelStreaming;
    private _application;
    get application(): Application;
    private _esMsgWsWrapper?;
    private _esMsgWsWrapperInit;
    get esMsgWsWrapper(): ESMsgWebSocketWrapper | undefined;
    constructor(_domOrDomId: HTMLDivElement | string, _options: {
        url: string;
        esmsgWsUri?: string | undefined;
    });
    resolveCallback(text: string): boolean;
    processResponse(text: string): void;
    sendInputData(data: ArrayBuffer): void;
    emitUIInteraction(descriptor: any, callback: any): void;
    private get webRtcController();
    private _pixelStreamEmitUIInteractionAsString;
    emitUIInteractionForBigData(descriptor: any, callback: any): void;
    emitCommand(descriptor: object): void;
    getVideoSize(): {
        width: number;
        height: number;
    };
    resizeUEVideo(x: number, y: number): void;
}
