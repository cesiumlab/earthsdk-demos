import { HasOwner } from "../base/HasOwner";
import { ESUEViewer } from "./ESUEViewer";
export declare class ESMsgWebSocketWrapper extends HasOwner<ESUEViewer> {
    private _uri;
    get esUeViewer(): ESUEViewer;
    private _processResponseFn;
    get uri(): string;
    private _inner;
    private _innerInit;
    get inner(): WebSocket;
    constructor(owner: ESUEViewer, _uri: string);
    sendInputData(data: ArrayBuffer): boolean | undefined;
    emitDescriptorWithString(messageType: number, descriptorAsString: string): void;
}
