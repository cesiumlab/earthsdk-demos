import { HasOwner, Event as XEvent } from "xbsj-renderer/dist-node/xr-base-utils";
import { ESUEViewer } from "./ESUEViewer";
export declare class ESMsgWebSocketWrapper extends HasOwner<ESUEViewer> {
    private _uri;
    get esUeViewer(): ESUEViewer;
    private _responseEvent;
    get responseEvent(): XEvent<[string]>;
    get uri(): string;
    private _inner;
    private _innerInit;
    get inner(): WebSocket;
    constructor(owner: ESUEViewer, _uri: string);
    setInputData(data: ArrayBuffer): void;
}
