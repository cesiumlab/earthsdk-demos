export declare type NormalizeAndQuantizeFuncsType = {
    normalizeAndQuantizeUnsigned: (x: number, y: number) => {
        inRange: boolean;
        x: number;
        y: number;
    };
    unquantizeAndDenormalizeUnsigned: (x: number, y: number) => {
        x: number;
        y: number;
    };
    normalizeAndQuantizeSigned: (x: number, y: number) => {
        x: number;
        y: number;
    };
};
/**
 * 把网页的鼠标位置 映射为归一化的鼠标位置
 */
export declare function setupNormalizeAndQuantize(playerElement: HTMLDivElement): {
    normalizeAndQuantizeUnsigned: (x: number, y: number) => {
        inRange: boolean;
        x: number;
        y: number;
    };
    unquantizeAndDenormalizeUnsigned: (x: number, y: number) => {
        x: number;
        y: number;
    };
    normalizeAndQuantizeSigned: (x: number, y: number) => {
        x: number;
        y: number;
    };
} | undefined;
