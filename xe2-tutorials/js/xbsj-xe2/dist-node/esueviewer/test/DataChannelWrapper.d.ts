import { HasOwner, Event as XEvent } from "xbsj-renderer/dist-node/xr-base-utils";
import { RTCPeerConnectionWrapper } from "./RTCPeerConnectionWrapper";
export declare class DataChannelWrapper extends HasOwner<RTCPeerConnectionWrapper> {
    private _label;
    private _options;
    get peerConnectionWrapper(): RTCPeerConnectionWrapper;
    get webSocketWrapper(): import("./WebSocketWrapper").WebSocketWrapper;
    get esUeViewer(): import("./ESUEViewer").ESUEViewer;
    get videoElement(): import("./VideoElement").VideoElement;
    private _openEvent;
    get openEvent(): XEvent<[Event]>;
    private _closeEvent;
    get closeEvent(): XEvent<[Event]>;
    private _messageEvent;
    get messageEvent(): XEvent<[MessageEvent<any>]>;
    private _responseEvent;
    get responseEvent(): XEvent<[string]>;
    private _inner;
    private _innerInit;
    get inner(): RTCDataChannel;
    constructor(owner: RTCPeerConnectionWrapper, _label: string, _options: RTCDataChannelInit);
    send(data: any): void;
    private __dataQueue;
    sendBigData(data: any): void;
    private _onDataChannelMessage;
}
