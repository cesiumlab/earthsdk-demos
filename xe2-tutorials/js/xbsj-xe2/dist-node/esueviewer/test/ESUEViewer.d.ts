import { Destroyable, Event as XEvent } from "xbsj-renderer/dist-node/xr-base-utils";
import { ESMsgWebSocketWrapper } from "./ESMsgWebSocketWrapper";
import { VideoElement } from "./VideoElement";
import { WebSocketWrapper } from "./WebSocketWrapper";
export declare class ESUEViewer extends Destroyable {
    private _domOrDomId;
    private _options;
    private _ueevent;
    get ueevent(): XEvent<[{
        detail: string;
    }]>;
    debug: boolean;
    forceNotUseEsMsgWs: boolean;
    private uiInteractionCallbacks;
    private _container;
    get container(): HTMLElement;
    private _videoElement;
    private _videoElementInit;
    get videoElement(): VideoElement;
    get videoinitedEvent(): XEvent<[{
        detail: {
            width: number;
            height: number;
        };
    }]>;
    private _esMsgWsWrapper?;
    private _esMsgWsWrapperInit;
    get esMsgWsWrapper(): ESMsgWebSocketWrapper | undefined;
    private _wsWrapper;
    get wsWrapper(): WebSocketWrapper;
    get closeEvent(): XEvent<[CloseEvent]>;
    get errorEvent(): XEvent<[Event]>;
    constructor(_domOrDomId: HTMLDivElement | string, _options: {
        url: string;
        esmsgWsUri?: string | undefined;
    });
    resolveCallback(text: string): boolean;
    processResponse(text: string): void;
    sendInputData(data: ArrayBuffer): void;
    private emitDescriptor;
    private _emitDescriptorWithString;
    private _emitDescriptorWithString2;
    private emitDescriptorForBigData;
    emitUIInteraction(descriptor: any, callback: any): void;
    emitUIInteractionForBigData(descriptor: any, callback: any): void;
    emitCommand(descriptor: any): void;
    getVideoSize(): {
        width: number;
        height: number;
    };
    resizeUEVideo(x: number, y: number): void;
}
