import { HasOwner, Event as XEvent } from "xbsj-renderer/dist-node/xr-base-utils";
import { DataChannelWrapper } from "./DataChannelWrapper";
import { WebSocketWrapper } from "./WebSocketWrapper";
export declare class RTCPeerConnectionWrapper extends HasOwner<WebSocketWrapper> {
    private _peerConnectionOptions;
    private _offerMsg;
    get webSocketWrapper(): WebSocketWrapper;
    get esUeViewer(): import("./ESUEViewer").ESUEViewer;
    get videoElement(): import("./VideoElement").VideoElement;
    private _trackEvent;
    get trackEvent(): XEvent<[RTCTrackEvent]>;
    private _inner;
    private _innerInit;
    get inner(): RTCPeerConnection;
    private _dataChannelWrapper;
    get dataChannelWrapper(): DataChannelWrapper;
    private _onoffer;
    constructor(owner: WebSocketWrapper, _peerConnectionOptions: {
        [k: string]: any;
    }, _offerMsg: any);
}
