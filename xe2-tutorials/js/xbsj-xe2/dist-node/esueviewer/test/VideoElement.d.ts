import { Destroyable, Event as XEvent } from "xbsj-renderer/dist-node/xr-base-utils";
export declare class VideoElement extends Destroyable {
    normalizeAndQuantizeUnsigned: (x: number, y: number) => {
        inRange: boolean;
        x: number;
        y: number;
    };
    unquantizeAndDenormalizeUnsigned: (x: number, y: number) => {
        x: number;
        y: number;
    };
    normalizeAndQuantizeSigned: (x: number, y: number) => {
        x: number;
        y: number;
    };
    private _videoinitedEvent;
    get videoinitedEvent(): XEvent<[{
        detail: {
            width: number;
            height: number;
        };
    }]>;
    private _inputDataEvent;
    get inputDataEvent(): XEvent<[ArrayBuffer]>;
    sendInputData(data: ArrayBuffer): void;
    private _element;
    private _elementInit;
    get element(): HTMLDivElement;
    private _video;
    get video(): HTMLVideoElement;
    private _videoInit;
    constructor();
    onVideoInitialised(): void;
    getVideoSize(): {
        width: number;
        height: number;
    };
    playVideoStream(): void;
    requestInitialSettings(): void;
    requestQualityControl(): void;
    emitMouseMove(x: number, y: number, deltaX: number, deltaY: number): void;
    emitMouseDown(button: number, x: number, y: number): void;
    emitMouseUp(button: number, x: number, y: number): void;
    emitMouseWheel(delta: number, x: number, y: number): void;
    releaseMouseButtons(buttons: number, x: number, y: number): void;
    pressMouseButtons(buttons: number, x: number, y: number): void;
    registerHoveringMouseEvents(playerElement: HTMLElement): void;
    registerMouseEnterAndLeaveEvents(playerElement: HTMLDivElement): void;
    registerKeyboardEvents(): void;
}
