import { HasOwner, Event as XEvent } from "xbsj-renderer/dist-node/xr-base-utils";
import { ESUEViewer } from "./ESUEViewer";
import { RTCPeerConnectionWrapper } from "./RTCPeerConnectionWrapper";
export declare class WebSocketWrapper extends HasOwner<ESUEViewer> {
    private _url;
    private _messageEvent;
    get messageEvent(): XEvent<[MessageEvent<any>]>;
    private _errorEvent;
    get errorEvent(): XEvent<[Event]>;
    private _closeEvent;
    get closeEvent(): XEvent<[CloseEvent]>;
    private _inner;
    private _innerInit;
    get inner(): WebSocket;
    send(data: string | ArrayBufferLike | Blob | ArrayBufferView): void;
    private _lastPeerConnectionOptions?;
    private _peerConnectionWrapperResetting;
    private _peerConnectionWrapperResettingInit;
    get peerConnectionWrapper(): RTCPeerConnectionWrapper | undefined;
    constructor(owner: ESUEViewer, _url: string);
}
