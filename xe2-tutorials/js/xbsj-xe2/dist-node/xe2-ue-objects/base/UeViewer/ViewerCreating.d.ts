import { PickedInfo } from 'xbsj-xe2/dist-node/xe2-base-objects';
import { Destroyable } from "xbsj-xe2/dist-node/xe2-base-utils";
import { UeViewer } from "./index";
export declare class UePickedInfo extends PickedInfo {
    uePickResult?: {
        [key: string]: any;
        id?: string | undefined;
        position?: [number, number, number] | undefined;
        screenPosition?: [number, number] | undefined;
        actorTags?: string[] | undefined;
        className?: string | undefined;
        parentInfo?: {
            name?: string | undefined;
            className?: string | undefined;
            actorTags?: string[] | undefined;
        }[] | undefined;
        features?: {
            [k: string]: any;
        } | undefined;
        hitItem?: number | undefined;
        componentName?: string | undefined;
    } | undefined;
    constructor(uePickResult?: {
        [key: string]: any;
        id?: string | undefined;
        position?: [number, number, number] | undefined;
        screenPosition?: [number, number] | undefined;
        actorTags?: string[] | undefined;
        className?: string | undefined;
        parentInfo?: {
            name?: string | undefined;
            className?: string | undefined;
            actorTags?: string[] | undefined;
        }[] | undefined;
        features?: {
            [k: string]: any;
        } | undefined;
        hitItem?: number | undefined;
        componentName?: string | undefined;
    } | undefined, childPickedInfo?: PickedInfo);
}
export declare class ViewerCreating extends Destroyable {
    constructor(container: HTMLDivElement, uri: string | undefined, ueViewer: UeViewer);
}
