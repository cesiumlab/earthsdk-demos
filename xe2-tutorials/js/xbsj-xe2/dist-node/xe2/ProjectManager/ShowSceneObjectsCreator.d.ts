import { Destroyable, Event } from "xbsj-xe2/dist-node/xe2-base-utils";
import { ProjectManager } from ".";
export declare class ShowSceneObjectsCreator extends Destroyable {
    private _projectManager;
    private _showCreatorEvent;
    get showCreatorEvent(): Event<[]>;
    showCreator(): void;
    private _selectTypeOkEvent;
    get selectTypeOkEvent(): Event<[string]>;
    selectTypeOk(type: string): void;
    private _isEnable;
    get isEnable(): boolean;
    set isEnable(value: boolean);
    get isEnableChanged(): import("xbsj-xe2/dist-node/xe2-base-utils").Listener<[boolean, boolean]>;
    private _selectType;
    get selectType(): string | undefined;
    set selectType(value: string | undefined);
    get selectTypeChanged(): import("xbsj-xe2/dist-node/xe2-base-utils").Listener<[string | undefined, string | undefined]>;
    private _processing;
    get processing(): import("xbsj-xe2/dist-node/xe2-utils").Processing<void, []>;
    constructor(_projectManager: ProjectManager);
}
