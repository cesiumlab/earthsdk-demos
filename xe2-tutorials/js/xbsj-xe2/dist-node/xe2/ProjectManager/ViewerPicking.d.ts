import { HasOwner } from "xbsj-renderer/dist-node/xr-base-utils";
import { ProjectManager } from "./ProjectManager";
import { PickedInfo, Viewer } from "xbsj-xe2/dist-node/xe2-base-objects";
export declare class ViewerPicking extends HasOwner<ProjectManager> {
    debug: boolean;
    constructor(owner: ProjectManager);
    viewerPick(viewer: Viewer, windowPos: [number, number], attachedInfo: any): Promise<PickedInfo | undefined>;
}
