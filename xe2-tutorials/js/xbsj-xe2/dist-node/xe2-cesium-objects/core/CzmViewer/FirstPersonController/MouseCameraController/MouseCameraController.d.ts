import { Destroyable } from "xbsj-renderer/dist-node/xr-base-utils";
import { FirstPersonController } from "../FirstPersonController";
import { MouseCameraControllerRunning } from "./MouseCameraControllerRunning";
import { ObjResettingWithEvent } from "xbsj-renderer/dist-node/xr-utils";
export declare class MouseCameraController extends Destroyable {
    private _firstPersonController;
    get firstPersonController(): FirstPersonController;
    private _enabled;
    get enabled(): boolean;
    set enabled(value: boolean);
    get enabledChanged(): import("xbsj-renderer/dist-node/xr-base-utils").Listener<[boolean, boolean]>;
    enableViewerOriginInputs: (value: boolean) => void;
    private _mouseResetting;
    get mouseResetting(): ObjResettingWithEvent<MouseCameraControllerRunning, import("xbsj-renderer/dist-node/xr-base-utils").Listener<[boolean, boolean]>>;
    constructor(_firstPersonController: FirstPersonController);
}
