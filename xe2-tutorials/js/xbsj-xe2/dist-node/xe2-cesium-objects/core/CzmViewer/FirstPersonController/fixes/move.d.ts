import * as Cesium from 'cesium';
export declare function moveForward(camera: Cesium.Camera, amount: number): void;
export declare function moveRight(camera: Cesium.Camera, amount: number): void;
export declare function moveUp(camera: Cesium.Camera, amount: number): void;
