import { Event } from "xbsj-xe2/dist-node/xe2-base-utils";
import { CzmClippingPlaneCollectionJsonType } from "../base-objects/utils";
export interface CzmClippingPlanesType {
    computedClippingPlanes: CzmClippingPlaneCollectionJsonType | undefined;
    computedClippingPlanesChanged: Event<[CzmClippingPlaneCollectionJsonType | undefined, CzmClippingPlaneCollectionJsonType | undefined]>;
}
