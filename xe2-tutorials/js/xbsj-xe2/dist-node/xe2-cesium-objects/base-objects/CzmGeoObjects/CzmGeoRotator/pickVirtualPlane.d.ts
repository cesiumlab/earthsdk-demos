import * as Cesium from 'cesium';
export declare function pickVirtualPlane(scene: Cesium.Scene, planePivot: Cesium.Cartesian3, planeNormal: Cesium.Cartesian3, windowCoordinates: Cesium.Cartesian2, result?: Cesium.Cartesian3): Cesium.Cartesian3 | undefined;
