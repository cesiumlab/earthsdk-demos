export declare type RotatorPlaneType = "none" | "heading" | "pitch" | "roll";
