import { HasOwner, Listener } from 'xbsj-xe2/dist-node/xe2-base-utils';
import { ObjResetting } from 'xbsj-xe2/dist-node/xe2-utils';
import * as Cesium from 'cesium';
import { CzmGeoRotator } from './index';
import { Rotating } from './Rotating';
import { RotatorPlaneType } from './RotatorPlaneType';
import { RotatorStartInfoType } from './RotatorStartInfoType';
export declare class RotatorRunning extends HasOwner<CzmGeoRotator> {
    private _rotatingResetting;
    get rotatingResetting(): ObjResetting<Rotating, Listener<[startInfo: RotatorStartInfoType | undefined]>>;
    constructor(owner: CzmGeoRotator);
    pickPlane(startInfo: RotatorStartInfoType, scene: Cesium.Scene, type: Exclude<RotatorPlaneType, 'none'>, windowPos: [number, number], sceneScale: number): void;
    getStartInfo(pointerEvent: PointerEvent): RotatorStartInfoType | undefined;
}
