import { HasOwner } from 'xbsj-xe2/dist-node/xe2-base-utils';
import * as Cesium from 'cesium';
import { CzmGeoRotator } from '.';
export declare class BasePlane extends HasOwner<CzmGeoRotator> {
    get viewer(): Cesium.Viewer;
    get scene(): Cesium.Scene;
    get sceneObject(): import("../../../../xe2-base-objects").GeoRotator;
    protected _valid: boolean;
    get valid(): boolean;
    protected _normal: Cesium.Cartesian3;
    get normal(): Cesium.Cartesian3 | undefined;
    protected _origin: Cesium.Cartesian3;
    get origin(): false | Cesium.Cartesian3;
    constructor(czmGeoRotator: CzmGeoRotator);
    pick(windowPos: [number, number]): Cesium.Cartesian3 | undefined;
}
