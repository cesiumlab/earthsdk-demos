import { BasePlane } from './BasePlane';
import { CzmGeoRotator } from '.';
export declare class HeadingPlane extends BasePlane {
    constructor(czmGeoRotator: CzmGeoRotator);
}
