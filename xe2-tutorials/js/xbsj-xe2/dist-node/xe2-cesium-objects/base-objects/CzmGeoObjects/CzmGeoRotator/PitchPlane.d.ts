import { BasePlane } from './BasePlane';
import { CzmGeoRotator } from '.';
export declare class PitchPlane extends BasePlane {
    constructor(czmGeoRotator: CzmGeoRotator);
}
