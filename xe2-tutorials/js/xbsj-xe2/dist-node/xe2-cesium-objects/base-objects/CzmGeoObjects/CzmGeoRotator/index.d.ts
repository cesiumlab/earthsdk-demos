import { GeoPoint, GeoRotator, ObjResettingWithEvent } from 'xbsj-xe2/dist-node/xe2-base-objects';
import * as Cesium from 'cesium';
import { CzmObject, CzmViewer } from '../../../core';
import { CzmModelPrimitive } from '../../CzmObjects';
import { BasePlane } from './BasePlane';
import { GeoRotatorCircleWrapper } from './GeoRotatorCircleWrapper';
import { RotatorPlaneType } from './RotatorPlaneType';
import { RotatorRunning } from './RotatorRunning';
export declare class CzmGeoRotator extends CzmObject<GeoRotator> {
    static readonly type: void;
    private _pointer;
    get pointer(): GeoPoint;
    private _pointerInit;
    private _cameraModel;
    get cameraModel(): CzmModelPrimitive;
    private _cameraModelInit;
    private _hoveredPlaneType;
    get hoveredPlaneType(): RotatorPlaneType;
    set hoveredPlaneType(value: RotatorPlaneType);
    get hoveredPlaneTypeChanged(): import("xbsj-xe2/dist-node/xe2-base-utils").Listener<[RotatorPlaneType, RotatorPlaneType]>;
    private _movingPlaneType;
    get movingPlaneType(): RotatorPlaneType;
    set movingPlaneType(value: RotatorPlaneType);
    get movingPlaneTypeChanged(): import("xbsj-xe2/dist-node/xe2-base-utils").Listener<[RotatorPlaneType, RotatorPlaneType]>;
    static rotationNum: {
        heading: number;
        pitch: number;
        roll: number;
    };
    static rotationFuncs: {
        heading: ([heading, pitch, roll]: [number, number, number]) => [number, number, number];
        pitch: ([heading, pitch, roll]: [number, number, number]) => [number, number, number];
        roll: ([heading, pitch, roll]: [number, number, number]) => [number, number, number];
    };
    private _circles;
    get circles(): {
        heading: GeoRotatorCircleWrapper;
        pitch: GeoRotatorCircleWrapper;
        roll: GeoRotatorCircleWrapper;
    };
    private _cartesian;
    get cartesian(): Cesium.Cartesian3 | undefined;
    private _cartesianInit;
    private _planes;
    getPlane(type: Exclude<RotatorPlaneType, 'none'>): BasePlane;
    private _rotatorRunningResetting;
    get rotatorRunningResetting(): ObjResettingWithEvent<RotatorRunning, import("xbsj-xe2/dist-node/xe2-base-utils").Listener<[boolean, boolean]>>;
    constructor(sceneObject: GeoRotator, czmViewer: CzmViewer);
}
