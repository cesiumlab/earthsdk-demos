import { BasePlane } from './BasePlane';
import { CzmGeoRotator } from '.';
export declare class RollPlane extends BasePlane {
    constructor(czmGeoRotator: CzmGeoRotator);
}
