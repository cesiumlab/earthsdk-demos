import { GeoRotatorCircle } from 'xbsj-xe2/dist-node/xe2-base-objects';
import { HasOwner } from 'xbsj-xe2/dist-node/xe2-base-utils';
import { RotatorPlaneType } from './RotatorPlaneType';
import { CzmGeoRotator } from '.';
export declare class GeoRotatorCircleWrapper extends HasOwner<CzmGeoRotator> {
    private _type;
    get sceneObject(): import("xbsj-xe2/dist-node/xe2-base-objects").GeoRotator;
    get type(): "heading" | "pitch" | "roll";
    private _circle;
    get circle(): GeoRotatorCircle;
    private _circleInit;
    constructor(owner: CzmGeoRotator, _type: Exclude<RotatorPlaneType, 'none'>);
}
