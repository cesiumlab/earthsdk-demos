import { HasOwner } from 'xbsj-xe2/dist-node/xe2-base-utils';
import { RotatorStartInfoType } from './RotatorStartInfoType';
import { CzmGeoRotator } from '.';
import { RotatorRunning } from './RotatorRunning';
export declare class Rotating extends HasOwner<RotatorRunning> {
    private _startInfo;
    get startInfo(): RotatorStartInfoType;
    get planeType(): "heading" | "pitch" | "roll";
    get czmGeoRotator(): CzmGeoRotator;
    get plane(): import("./BasePlane").BasePlane;
    get circle(): import("../../../../xe2-base-objects").GeoRotatorCircle;
    constructor(owner: RotatorRunning, _startInfo: RotatorStartInfoType);
    move(pointerEvent: PointerEvent, startInfo: RotatorStartInfoType): void;
}
