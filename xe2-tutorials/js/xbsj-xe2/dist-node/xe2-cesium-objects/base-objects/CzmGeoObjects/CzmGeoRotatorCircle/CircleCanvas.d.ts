import { HasOwner } from 'xbsj-xe2/dist-node/xe2-base-utils';
import { CzmGeoRotatorCircle } from '.';
export declare class CircleCanvas extends HasOwner<CzmGeoRotatorCircle> {
    private _canvas;
    get canvas(): HTMLCanvasElement;
    private _ctx;
    get ctx(): CanvasRenderingContext2D;
    private _flipText;
    get flipText(): boolean;
    set flipText(value: boolean);
    get flipTextChanged(): import("xbsj-xe2/dist-node/xe2-base-utils").Listener<[boolean, boolean]>;
    constructor(owner: CzmGeoRotatorCircle);
}
