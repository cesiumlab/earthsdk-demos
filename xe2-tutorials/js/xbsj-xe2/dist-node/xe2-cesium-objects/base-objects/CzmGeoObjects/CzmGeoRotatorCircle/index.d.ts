import { GeoRotatorCircle } from 'xbsj-xe2/dist-node/xe2-base-objects';
import { CzmObject, CzmViewer } from '../../../core';
import { CzmTexture } from '../../CzmObjects';
import { CircleCanvas } from './CircleCanvas';
import { CustomPrimitiveForCircle } from './CustomPrimitiveForCicle';
import { DebugAxis } from './DebugAxis';
export declare class CzmGeoRotatorCircle extends CzmObject<GeoRotatorCircle> {
    static readonly type: void;
    private _czmTexture;
    get czmTexture(): CzmTexture;
    private _czmTextureInit;
    private _customPrimitiveForCircle;
    get customPrimitiveForCircle(): CustomPrimitiveForCircle;
    private _circleCanvas;
    get circleCanvas(): CircleCanvas;
    private _debugAxis;
    get debugAxis(): DebugAxis;
    constructor(sceneObject: GeoRotatorCircle, czmViewer: CzmViewer);
}
