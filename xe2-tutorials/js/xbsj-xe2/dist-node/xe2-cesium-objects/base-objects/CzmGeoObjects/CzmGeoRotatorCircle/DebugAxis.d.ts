import { HasOwner } from "xbsj-xe2/dist-node/xe2-base-utils";
import { CzmGeoRotatorCircle } from ".";
import { CzmCustomPrimitive } from "../../CzmObjects";
export declare class DebugAxis extends HasOwner<CzmGeoRotatorCircle> {
    private _axis;
    get axis(): CzmCustomPrimitive;
    private _axisInit;
    constructor(owner: CzmGeoRotatorCircle);
}
