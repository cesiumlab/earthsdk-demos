import { SceneObject } from 'xbsj-xe2/dist-node/xe2-base-objects';
import { HasOwner } from 'xbsj-renderer/dist-node/xr-base-utils';
import { CzmCustomPrimitive } from '../../CzmObjects';
import { CzmGeoRotatorCircle } from '.';
export declare function createCustomPrimitive(): SceneObject | undefined;
export declare class CustomPrimitiveForCircle extends HasOwner<CzmGeoRotatorCircle> {
    get sceneObject(): import("xbsj-xe2/dist-node/xe2-base-objects").GeoRotatorCircle;
    get czmTexture(): import("../../CzmObjects").CzmTexture;
    get czmViewer(): import("../../..").CzmViewer;
    private _customPrimitive;
    get customPrimitive(): CzmCustomPrimitive;
    private _customPrimitiveInit;
    constructor(owner: CzmGeoRotatorCircle);
}
