import { GeoImageModel } from 'xbsj-xe2/dist-node/xe2-base-objects';
import { CzmObject, CzmViewer } from '../../../core';
import { CzmImageModel } from '../../CzmObjects';
export declare class CzmGeoImageModel extends CzmObject<GeoImageModel> {
    static readonly type: void;
    private _czmImageModel;
    get czmImageModel(): CzmImageModel;
    constructor(sceneObject: GeoImageModel, czmViewer: CzmViewer);
}
