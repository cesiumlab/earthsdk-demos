import { GeoCoplanarPolygon } from 'xbsj-xe2/dist-node/xe2-base-objects';
import { Destroyable } from 'xbsj-xe2/dist-node/xe2-base-utils';
import { CzmViewer } from '../../../core';
import { CzmGeoCoplanarPolygon } from '.';
export declare class GroundCoplanarPolygon extends Destroyable {
    constructor(sceneObject: GeoCoplanarPolygon, czmViewer: CzmViewer, czmGeoCoplanarPolygon: CzmGeoCoplanarPolygon);
}
