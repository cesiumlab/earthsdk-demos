import { GeoCoplanarPolygon, ObjResettingWithEvent } from 'xbsj-xe2/dist-node/xe2-base-objects';
import { Listener } from 'xbsj-xe2/dist-node/xe2-base-utils';
import { CzmViewer } from '../../../core';
import { CzmObject } from '../../../core';
import { CoplanarPolygon } from './CoplanarPolygon';
import { GroundCoplanarPolygon } from './GroundCoplanarPolygon';
import { CzmViewDistanceRangeControl } from '../../utils2';
export declare class CzmGeoCoplanarPolygon extends CzmObject<GeoCoplanarPolygon> {
    static readonly type: void;
    private _resetting;
    get resetting(): ObjResettingWithEvent<CoplanarPolygon | GroundCoplanarPolygon, Listener<[boolean | undefined, boolean | undefined]>>;
    private _czmViewVisibleDistanceRangeControl;
    get czmViewVisibleDistanceRangeControl(): CzmViewDistanceRangeControl;
    get visibleAlpha(): number;
    get visibleAlphaChanged(): Listener<[number, number]>;
    private _viewDistanceDebugBinding;
    constructor(sceneObject: GeoCoplanarPolygon, czmViewer: CzmViewer);
}
