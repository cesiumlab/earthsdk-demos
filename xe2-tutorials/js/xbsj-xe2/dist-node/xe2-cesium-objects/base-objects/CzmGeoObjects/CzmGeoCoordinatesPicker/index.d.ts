import { GeoCoordinatesPicker } from 'xbsj-xe2/dist-node/xe2-base-objects';
import { CzmObject, CzmViewer } from '../../../core';
export declare class CzmGeoCoordinatesPicker extends CzmObject<GeoCoordinatesPicker> {
    static readonly type: void;
    private _mainCoordinates;
    get coordinates(): import("xbsj-xe2/dist-node/xe2-base-objects").GeoCoordinates;
    private _czmPointWrapper;
    private _pickingProcessingResetting;
    constructor(coordinatesPicker: GeoCoordinatesPicker, czmViewer: CzmViewer);
}
