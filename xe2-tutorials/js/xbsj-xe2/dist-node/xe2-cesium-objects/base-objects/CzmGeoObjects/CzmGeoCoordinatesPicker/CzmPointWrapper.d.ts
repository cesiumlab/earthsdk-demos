import { GeoCoordinatesPicker } from 'xbsj-xe2/dist-node/xe2-base-objects';
import { CzmViewer } from '../../../core';
import { Destroyable } from 'xbsj-renderer/dist-node/xr-base-utils';
export declare class CzmPointWrapper extends Destroyable {
    constructor(czmViewer: CzmViewer, coordinatesPicker: GeoCoordinatesPicker);
}
