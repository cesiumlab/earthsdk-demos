import { Destroyable } from 'xbsj-xe2/dist-node/xe2-base-utils';
import { CzmGeoCoordinatesPicker } from '.';
export declare class PickingProcessing extends Destroyable {
    private _owner;
    private _pickingPosition;
    get owner(): CzmGeoCoordinatesPicker;
    constructor(_owner: CzmGeoCoordinatesPicker);
}
