import { GeoCoordinates, GeoCoordinatesPicker } from 'xbsj-xe2/dist-node/xe2-base-objects';
import { CzmViewer } from '../../../core';
import { Destroyable } from 'xbsj-renderer/dist-node/xr-base-utils';
export declare class MainCoordinates extends Destroyable {
    private _czmViewer;
    private _coordinatesPicker;
    private _coordinates;
    get coordinates(): GeoCoordinates;
    private _coordinatesInit;
    constructor(_czmViewer: CzmViewer, _coordinatesPicker: GeoCoordinatesPicker);
}
