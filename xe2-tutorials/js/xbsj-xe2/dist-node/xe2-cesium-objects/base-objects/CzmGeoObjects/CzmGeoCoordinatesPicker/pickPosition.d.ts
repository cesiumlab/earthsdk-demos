import { CzmViewer } from '../../../core';
export declare function pickPosition(czmViewer: CzmViewer, pointerEvent: PointerEvent, virtualHeight: number | undefined, result?: [number, number, number]): Promise<[number, number, number] | undefined>;
