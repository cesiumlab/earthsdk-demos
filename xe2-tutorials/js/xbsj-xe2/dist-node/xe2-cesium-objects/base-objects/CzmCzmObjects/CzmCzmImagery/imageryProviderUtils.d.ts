import { CzmImageryProviderJsonType } from 'xbsj-xe2/dist-node/xe2-base-objects';
import * as Cesium from 'cesium';
import { CzmViewer } from '../../../core';
export declare function needRecreate(oldImageryProviderJson: CzmImageryProviderJsonType | undefined, newImageryProviderJson: CzmImageryProviderJsonType | undefined): boolean;
export declare function createImageryProviderFromJson(imageryProviderJson: CzmImageryProviderJsonType, czmViewer: CzmViewer): Promise<Cesium.ImageryProvider | undefined>;
export declare function updateImageryProviderFromJson(imageryProvider: Cesium.ImageryProvider, imageryProviderJson: CzmImageryProviderJsonType): void;
