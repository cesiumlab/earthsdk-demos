import { Destroyable } from 'xbsj-xe2/dist-node/xe2-base-utils';
import { ObjResettingWithEvent } from 'xbsj-xe2/dist-node/xe2-utils';
import * as Cesium from 'cesium';
import { CzmViewer } from '../../../core';
import { Czm3DTiles, Czm3DTilesCustomShaderClassType } from '../../CzmObjects';
import { AbsoluteClippingPlaneCollectionUpdating } from './AbsoluteClippingPlaneCollectionUpdating';
import { CzmCzm3DTiles } from './CzmCzm3DTiles';
import { RelativeClippingPlaneCollectionUpdating } from './RelativeClippingPlaneCollectionUpdating';
export declare class NativeTilesetReadyResetting extends Destroyable {
    private _tileset;
    private _czmCzm3DTiles;
    private _czmNativeViewer;
    private _czmViewer;
    get tileset(): Cesium.Cesium3DTileset;
    get czmCzm3DTiles(): CzmCzm3DTiles;
    get sceneObject(): Czm3DTiles;
    private _updateMatrixEvent;
    private _updateMatrix;
    private _clippingPlanesCollectionResetting;
    get clippingPlanesCollectionResetting(): ObjResettingWithEvent<AbsoluteClippingPlaneCollectionUpdating | RelativeClippingPlaneCollectionUpdating, import("xbsj-xe2/dist-node/xe2-base-utils").Listener<[boolean, boolean]>>;
    private _flattenedCustomShader;
    get flattenedCustomShader(): Czm3DTilesCustomShaderClassType | undefined;
    set flattenedCustomShader(value: Czm3DTilesCustomShaderClassType | undefined);
    get flattenedCustomShaderChanged(): import("xbsj-xe2/dist-node/xe2-base-utils").Listener<[Czm3DTilesCustomShaderClassType | undefined, Czm3DTilesCustomShaderClassType | undefined]>;
    private _czmFlattenedPlaneIdResetting;
    constructor(_tileset: Cesium.Cesium3DTileset, _czmCzm3DTiles: CzmCzm3DTiles, _czmNativeViewer: Cesium.Viewer, _czmViewer: CzmViewer);
}
