import * as Cesium from 'cesium';
import { CzmCzm3DTiles } from './CzmCzm3DTiles';
export declare function updateModelMatrix(tileset: Cesium.Cesium3DTileset, czmCzm3DTiles: CzmCzm3DTiles): void;
