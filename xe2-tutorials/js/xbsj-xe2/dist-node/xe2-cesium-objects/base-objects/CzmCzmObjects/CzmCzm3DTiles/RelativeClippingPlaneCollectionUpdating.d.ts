import { Destroyable } from 'xbsj-xe2/dist-node/xe2-base-utils';
import * as Cesium from 'cesium';
import { CzmCzm3DTiles } from './CzmCzm3DTiles';
export declare class RelativeClippingPlaneCollectionUpdating extends Destroyable {
    private _tileset;
    private _czmCzm3DTiles;
    get tileset(): Cesium.Cesium3DTileset;
    get czmCzm3DTiles(): CzmCzm3DTiles;
    constructor(_tileset: Cesium.Cesium3DTileset, _czmCzm3DTiles: CzmCzm3DTiles);
}
