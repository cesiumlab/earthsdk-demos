import { NativeNumber16Type } from 'xbsj-xe2/dist-node/xe2-base-objects';
import { Destroyable } from 'xbsj-xe2/dist-node/xe2-base-utils';
import * as Cesium from 'cesium';
import { CzmFlattenedPlane } from '../../CzmObjects';
import { NativeTilesetReadyResetting } from './NativeTilesetReadyResetting';
export declare class Czm3DTilesFlattenedPlaneResetting extends Destroyable {
    private _nativeTilesetReadyResetting;
    private _czmFlattenedPlane;
    get czm3DTiles(): import("../../CzmObjects").Czm3DTiles;
    get czmCzm3DTiles(): import("./CzmCzm3DTiles").CzmCzm3DTiles;
    get tileset(): Cesium.Cesium3DTileset;
    get czmFlattenedPlane(): CzmFlattenedPlane;
    setFlattened(value: boolean): void;
    setFlattenedBound(value: [number, number, number, number]): void;
    setElevationMatrix(value: NativeNumber16Type): void;
    constructor(_nativeTilesetReadyResetting: NativeTilesetReadyResetting, _czmFlattenedPlane: CzmFlattenedPlane);
}
