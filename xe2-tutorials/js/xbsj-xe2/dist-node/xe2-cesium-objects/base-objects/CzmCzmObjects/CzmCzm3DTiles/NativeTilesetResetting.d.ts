import { Destroyable } from 'xbsj-xe2/dist-node/xe2-base-utils';
import { ObjResettingWithEvent } from 'xbsj-xe2/dist-node/xe2-utils';
import * as Cesium from 'cesium';
import { CzmViewer } from '../../../core';
import { NativeTilesetReadyResetting } from './NativeTilesetReadyResetting';
import { CzmCzm3DTiles } from './CzmCzm3DTiles';
export declare class NativeTilesetResetting extends Destroyable {
    private _url;
    private _czmCzm3DTiles;
    private _czmNativeViewer;
    private _czmViewer;
    get url(): string;
    get czmCzm3DTiles(): CzmCzm3DTiles;
    get sceneObject(): import("../..").Czm3DTiles;
    get czmViewer(): CzmViewer;
    private _tileset;
    get tileset(): Cesium.Cesium3DTileset | undefined;
    set tileset(value: Cesium.Cesium3DTileset | undefined);
    get tilesetChanged(): import("xbsj-xe2/dist-node/xe2-base-utils").Listener<[Cesium.Cesium3DTileset | undefined, Cesium.Cesium3DTileset | undefined]>;
    private _readyResetting;
    get readyResetting(): ObjResettingWithEvent<NativeTilesetReadyResetting, import("xbsj-xe2/dist-node/xe2-base-utils").Listener<[Cesium.Cesium3DTileset | undefined, Cesium.Cesium3DTileset | undefined]>>;
    constructor(_url: string, _czmCzm3DTiles: CzmCzm3DTiles, _czmNativeViewer: Cesium.Viewer, _czmViewer: CzmViewer);
}
