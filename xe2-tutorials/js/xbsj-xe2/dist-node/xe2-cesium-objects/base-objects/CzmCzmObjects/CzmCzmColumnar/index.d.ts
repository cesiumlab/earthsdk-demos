import { CzmObject, CzmViewer } from 'xbsj-xe2/dist-node/xe2-cesium-objects/core';
import { CzmColumnar } from '../../CzmObjects';
import { GeoPolyline } from 'xbsj-xe2/dist-node/xe2-base-objects';
export declare class CzmCzmColumnar extends CzmObject<CzmColumnar> {
    static readonly type: void;
    private _geoPolyline;
    get geoPolyline(): GeoPolyline;
    constructor(sceneObject: CzmColumnar, czmViewer: CzmViewer);
}
