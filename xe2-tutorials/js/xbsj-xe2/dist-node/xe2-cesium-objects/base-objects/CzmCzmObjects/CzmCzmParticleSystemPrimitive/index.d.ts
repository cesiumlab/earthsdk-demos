import * as Cesium from 'cesium';
import { CzmObject, CzmViewer } from '../../../core';
import { CzmParticleSystemPrimitive } from '../../CzmObjects';
export declare class CzmCzmParticleSystemPrimitive extends CzmObject<CzmParticleSystemPrimitive> {
    static readonly type: void;
    private _primitive?;
    get primitive(): Cesium.ParticleSystem | undefined;
    constructor(sceneObject: CzmParticleSystemPrimitive, czmViewer: CzmViewer);
}
