import { CzmObject, CzmViewer } from '../../../core';
import { CzmArcgisHistoryImagery, CzmImagery } from '../../CzmObjects';
import { CustomDiv } from 'xbsj-xe2/dist-node/xe2-base-objects';
export declare class CzmCzmArcgisHistoryImagery extends CzmObject<CzmArcgisHistoryImagery> {
    static readonly type: void;
    private _czmImageery;
    get czmImageery(): undefined | CzmImagery;
    set czmImageery(value: undefined | CzmImagery);
    private _customDiv;
    get customDiv(): CustomDiv<{
        destroy(): undefined;
    }>;
    constructor(sceneObject: CzmArcgisHistoryImagery, czmViewer: CzmViewer);
}
