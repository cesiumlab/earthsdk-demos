import { CzmObject, CzmViewer } from '../../../core';
import { CzmCustomPrimitive, CzmImageModel } from '../../CzmObjects';
export declare class CzmCzmImageModel extends CzmObject<CzmImageModel> {
    static readonly type: void;
    private _customPrimitive;
    get customPrimitive(): CzmCustomPrimitive;
    constructor(sceneObject: CzmImageModel, czmViewer: CzmViewer);
}
