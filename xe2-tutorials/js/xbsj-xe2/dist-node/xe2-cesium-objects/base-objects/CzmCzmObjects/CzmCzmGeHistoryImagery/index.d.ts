import { CzmObject, CzmViewer } from '../../../core';
import { CzmGeHistoryImagery, CzmImagery } from '../../CzmObjects';
import { CustomDiv } from 'xbsj-xe2/dist-node/xe2-base-objects';
export declare class CzmCzmGeHistoryImagery extends CzmObject<CzmGeHistoryImagery> {
    static readonly type: void;
    private _czmImagery;
    get czmImagery(): undefined | CzmImagery;
    set czmImagery(value: undefined | CzmImagery);
    private _customDiv;
    get customDiv(): CustomDiv<{
        destroy(): undefined;
    }>;
    constructor(sceneObject: CzmGeHistoryImagery, czmViewer: CzmViewer);
}
