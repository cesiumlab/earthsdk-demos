import { Destroyable } from 'xbsj-xe2/dist-node/xe2-base-utils';
import * as Cesium from 'cesium';
import { NativePrimitive } from './NativePrimitive';
export declare class NativePrimitiveReady extends Destroyable {
    private _owner;
    get owner(): NativePrimitive;
    updateNodeMatrix(primitive: Cesium.Model): void;
    constructor(_owner: NativePrimitive);
    update(): void;
}
