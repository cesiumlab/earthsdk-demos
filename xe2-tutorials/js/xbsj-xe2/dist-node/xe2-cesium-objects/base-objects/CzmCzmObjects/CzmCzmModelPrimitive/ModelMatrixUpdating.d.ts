import { Destroyable } from 'xbsj-xe2/dist-node/xe2-base-utils';
import * as Cesium from 'cesium';
import { NativePrimitive } from './NativePrimitive';
export declare class ModelMatrixUpdating extends Destroyable {
    private _nativePrimitive;
    get nativePrimitve(): NativePrimitive;
    get czmCzmModelPrimitive(): import(".").CzmCzmModelPrimitive;
    get sceneObject(): import("../..").CzmModelPrimitive;
    get sceneScaleFromPixelSize(): import("xbsj-xe2/dist-node/xe2-base-utils").ReactiveVariable<number | undefined>;
    get viewer(): Cesium.Viewer;
    get finalShow(): boolean;
    get primtive(): Cesium.Model;
    constructor(_nativePrimitive: NativePrimitive);
}
