import { Destroyable } from 'xbsj-xe2/dist-node/xe2-base-utils';
import { CzmCzmModelPrimitive } from '.';
export declare class NativePrimitiveResetting extends Destroyable {
    private _owner;
    get owner(): CzmCzmModelPrimitive;
    private _resetting;
    constructor(_owner: CzmCzmModelPrimitive);
}
