import { Destroyable } from 'xbsj-xe2/dist-node/xe2-base-utils';
import { NativePrimitive } from './NativePrimitive';
import { NativePrimitiveResetting } from './NativePrimitiveResetting';
export declare class NativePrimitiveCreating extends Destroyable {
    private _owner;
    get owner(): NativePrimitiveResetting;
    private _nativePrimitive;
    get nativePrimitive(): NativePrimitive | undefined;
    constructor(_owner: NativePrimitiveResetting);
}
