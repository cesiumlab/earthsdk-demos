import { Destroyable } from 'xbsj-xe2/dist-node/xe2-base-utils';
import * as Cesium from 'cesium';
import { PickingManager } from '../../../core';
import { NativePrimitiveCreating } from './NativePrimitiveCreating';
import { NativePrimitiveReady } from './NativePrimitiveReady';
export declare class NativePrimitive extends Destroyable {
    private _gltf;
    private _primitive;
    private _owner;
    get owner(): NativePrimitiveCreating;
    get czmCzmModelPrimitive(): import(".").CzmCzmModelPrimitive;
    get czmViewer(): import("../../../core").CzmViewer;
    get viewer(): Cesium.Viewer;
    get scene(): Cesium.Scene;
    get primitive(): Cesium.Model;
    get sceneObject(): import("../..").CzmModelPrimitive;
    get gltf(): any;
    get pickingManager(): PickingManager;
    private _modelMatrixUpdating;
    private _nativePrimitiveReady?;
    get nativePrimitiveReady(): NativePrimitiveReady | undefined;
    update(): void;
    constructor(_gltf: any, _primitive: Cesium.Model, _owner: NativePrimitiveCreating);
}
