import { CzmObject, CzmViewer } from '../../../core';
import { CzmWaterPrimitive } from '../../CzmObjects';
export declare class CzmCzmWaterPrimitive extends CzmObject<CzmWaterPrimitive> {
    static readonly type: void;
    private _primitive?;
    constructor(sceneObject: CzmWaterPrimitive, czmViewer: CzmViewer);
}
