import { ESSceneObject, GeoPolygon, GeoPolygonCanvas, PickedInfo, PointEditing, PositionsCenter, PositionsEditing } from 'xbsj-xe2/dist-node/xe2-base-objects';
import { Destroyable, Event, Listener, PartialWithUndefinedReactivePropsToNativeProps, ReactivePropsToNativePropsAndChanged } from 'xbsj-xe2/dist-node/xe2-base-utils';
import { ObjResettingWithEvent, SceneObject, SceneObjectKey, SceneObjectWithId } from 'xbsj-xe2/dist-node/xe2-utils';
import { CzmFlattenedPlane } from './CzmFlattenedPlane';
import { CzmTexture } from './CzmTexture';
declare class CzmFlattenedPlaneWithIdResetting extends Destroyable {
    private _czmPolygonFlattenedPlane;
    private _czmFlattenedPlane;
    constructor(_czmPolygonFlattenedPlane: CzmPolygonFlattenedPlane, _czmFlattenedPlane: CzmFlattenedPlane);
}
export declare class CzmPolygonFlattenedPlane extends ESSceneObject {
    static readonly type: string;
    get typeName(): string;
    get defaultProps(): {
        enabled: boolean;
        show: boolean;
        editing: boolean;
        pointEditing: boolean;
        positions: import("xbsj-xe2/dist-node/xe2-base-utils").ReactiveVariable<[number, number, number][] | undefined>;
        allowPicking: boolean;
        czmFlattenedPlaneId: string;
        execOnceFuncStr: string | undefined;
        updateFuncStr: string | undefined;
        toDestroyFuncStr: string | undefined;
        name: string;
        ref: string | undefined;
        devTags: import("xbsj-xe2/dist-node/xe2-base-utils").ReactiveVariable<string[] | undefined>;
        extras: import("xbsj-xe2/dist-node/xe2-base-utils").ReactiveVariable<import("xbsj-xe2/dist-node/xe2-base-utils").JsonValue>;
    };
    get json(): JsonType;
    set json(value: JsonType);
    private _pickedEvent;
    get pickedEvent(): Event<[PickedInfo]>;
    private _flyToEvent;
    get flyToEvent(): Listener<[number | undefined]>;
    flyTo(duration?: number): void;
    static defaults: {
        viewerTagsEnums: [string, string][];
    };
    private _sPositionsEditing;
    get sPositionsEditing(): PositionsEditing;
    private _sPointEditing;
    get sPointEditing(): PointEditing;
    private _positionsCenter;
    get positionsCenter(): PositionsCenter;
    private _czmTexture;
    get czmTexture(): CzmTexture;
    private _czmTextureInit;
    private _polygon;
    get polygon(): GeoPolygon;
    private _polygonInit;
    private _polygonCanvas;
    get polygonCanvas(): GeoPolygonCanvas;
    private _polygonCanvasInit;
    private _czmFlattenedPlaneWithId;
    get czmFlattenedPlaneWithId(): SceneObjectWithId<SceneObject>;
    private _czmFlattenedPlaneWithIdInit;
    private _czmFlattendPlaneWithIdResetting;
    get czmFlattendPlaneWithIdResetting(): ObjResettingWithEvent<CzmFlattenedPlaneWithIdResetting, Listener<[SceneObject | undefined, SceneObject | undefined]>>;
    constructor(id?: SceneObjectKey);
    getProperties(language?: string): import("xbsj-xe2/dist-node/xe2-base-objects").Property[];
}
export declare namespace CzmPolygonFlattenedPlane {
    const createDefaultProps: () => {
        enabled: boolean;
        show: boolean;
        editing: boolean;
        pointEditing: boolean;
        positions: import("xbsj-xe2/dist-node/xe2-base-utils").ReactiveVariable<[number, number, number][] | undefined>;
        allowPicking: boolean;
        czmFlattenedPlaneId: string;
        execOnceFuncStr: string | undefined;
        updateFuncStr: string | undefined;
        toDestroyFuncStr: string | undefined;
        name: string;
        ref: string | undefined;
        devTags: import("xbsj-xe2/dist-node/xe2-base-utils").ReactiveVariable<string[] | undefined>;
        extras: import("xbsj-xe2/dist-node/xe2-base-utils").ReactiveVariable<import("xbsj-xe2/dist-node/xe2-base-utils").JsonValue>;
    };
}
export interface CzmPolygonFlattenedPlane extends ReactivePropsToNativePropsAndChanged<ReturnType<typeof CzmPolygonFlattenedPlane.createDefaultProps>> {
}
declare type JsonType = PartialWithUndefinedReactivePropsToNativeProps<ReturnType<typeof CzmPolygonFlattenedPlane.createDefaultProps> & {
    type: string;
}>;
export {};
