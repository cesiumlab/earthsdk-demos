import { ESSceneObject } from 'xbsj-xe2/dist-node/xe2-base-objects';
import { Listener, PartialWithUndefinedReactivePropsToNativeProps, ReactivePropsToNativePropsAndChanged } from 'xbsj-xe2/dist-node/xe2-base-utils';
import { SceneObjectKey } from 'xbsj-xe2/dist-node/xe2-utils';
import { CzmClippingPlaneCollectionJsonType } from '../utils';
export declare class CzmClippingPlanes extends ESSceneObject {
    static readonly type: string;
    get typeName(): string;
    get defaultProps(): {
        enabled: boolean;
        planeIds: import("xbsj-xe2/dist-node/xe2-base-utils").ReactiveVariable<string[]>;
        reverseNormal: boolean;
        unionClippingRegions: boolean;
        edgeColor: import("xbsj-xe2/dist-node/xe2-base-utils").ReactiveVariable<[number, number, number, number]>;
        edgeWidth: number;
        execOnceFuncStr: string | undefined;
        updateFuncStr: string | undefined;
        toDestroyFuncStr: string | undefined;
        name: string;
        ref: string | undefined;
        devTags: import("xbsj-xe2/dist-node/xe2-base-utils").ReactiveVariable<string[] | undefined>;
        extras: import("xbsj-xe2/dist-node/xe2-base-utils").ReactiveVariable<import("xbsj-xe2/dist-node/xe2-base-utils").JsonValue>;
    };
    get json(): JsonType;
    set json(value: JsonType);
    private _flyToEvent;
    get flyToEvent(): Listener<[number | undefined]>;
    flyTo(duration?: number): void;
    private _computedClippingPlanes;
    get computedClippingPlanes(): CzmClippingPlaneCollectionJsonType | undefined;
    set computedClippingPlanes(value: CzmClippingPlaneCollectionJsonType | undefined);
    get computedClippingPlanesChanged(): Listener<[CzmClippingPlaneCollectionJsonType | undefined, CzmClippingPlaneCollectionJsonType | undefined]>;
    private _planesResetting;
    static defaults: {
        viewerTagsEnums: [string, string][];
    };
    constructor(id?: SceneObjectKey);
    getProperties(language?: string): import("xbsj-xe2/dist-node/xe2-base-objects").Property[];
}
export declare namespace CzmClippingPlanes {
    const createDefaultProps: () => {
        enabled: boolean;
        planeIds: import("xbsj-xe2/dist-node/xe2-base-utils").ReactiveVariable<string[]>;
        reverseNormal: boolean;
        unionClippingRegions: boolean;
        edgeColor: import("xbsj-xe2/dist-node/xe2-base-utils").ReactiveVariable<[number, number, number, number]>;
        edgeWidth: number;
        execOnceFuncStr: string | undefined;
        updateFuncStr: string | undefined;
        toDestroyFuncStr: string | undefined;
        name: string;
        ref: string | undefined;
        devTags: import("xbsj-xe2/dist-node/xe2-base-utils").ReactiveVariable<string[] | undefined>;
        extras: import("xbsj-xe2/dist-node/xe2-base-utils").ReactiveVariable<import("xbsj-xe2/dist-node/xe2-base-utils").JsonValue>;
    };
}
export interface CzmClippingPlanes extends ReactivePropsToNativePropsAndChanged<ReturnType<typeof CzmClippingPlanes.createDefaultProps>> {
}
declare type JsonType = PartialWithUndefinedReactivePropsToNativeProps<ReturnType<typeof CzmClippingPlanes.createDefaultProps> & {
    type: string;
}>;
export {};
