import { ESSceneObject, GeoPolygon, PickedInfo, PositionsEditing } from 'xbsj-xe2/dist-node/xe2-base-objects';
import { Destroyable, Event, Listener, PartialWithUndefinedReactivePropsToNativeProps, ReactivePropsToNativePropsAndChanged } from 'xbsj-xe2/dist-node/xe2-base-utils';
import { SceneObjectKey } from 'xbsj-xe2/dist-node/xe2-utils';
import { CzmClippingPlanes } from './CzmClippingPlanes';
export declare function positionsToClippingPlaneInfos(positions: [number, number, number][]): {
    pos: [number, number, number];
    dir: number;
}[] | undefined;
export declare class ClippingPlanesFromPositions extends Destroyable {
    private _sceneObject;
    private _czmClippingPlanes;
    private _positions;
    get czmClippingPlanes(): CzmClippingPlanes;
    static positionsToClippingPlaneInfos: typeof positionsToClippingPlaneInfos;
    private _czmPlanes;
    constructor(_sceneObject: CzmPolygonClippingPlanes, _czmClippingPlanes: CzmClippingPlanes, _positions: [number, number, number][]);
}
export declare class CzmPolygonClippingPlanes extends ESSceneObject {
    static readonly type: string;
    get typeName(): string;
    get defaultProps(): {
        enabled: boolean;
        showHelper: boolean;
        editing: boolean;
        allowPicking: boolean;
        positions: import("xbsj-xe2/dist-node/xe2-base-utils").ReactiveVariable<[number, number, number][] | undefined>;
        reverse: boolean;
        edgeColor: import("xbsj-xe2/dist-node/xe2-base-utils").ReactiveVariable<[number, number, number, number]>;
        edgeWidth: number;
        execOnceFuncStr: string | undefined;
        updateFuncStr: string | undefined;
        toDestroyFuncStr: string | undefined;
        name: string;
        ref: string | undefined;
        devTags: import("xbsj-xe2/dist-node/xe2-base-utils").ReactiveVariable<string[] | undefined>;
        extras: import("xbsj-xe2/dist-node/xe2-base-utils").ReactiveVariable<import("xbsj-xe2/dist-node/xe2-base-utils").JsonValue>;
    };
    get json(): JsonType;
    set json(value: JsonType);
    private _pickedEvent;
    get pickedEvent(): Event<[PickedInfo]>;
    private _flyToEvent;
    get flyToEvent(): Listener<[number | undefined]>;
    flyTo(duration?: number): void;
    private _sPositionsEditing;
    get sPositionsEditing(): PositionsEditing;
    static defaults: {
        viewerTagsEnums: [string, string][];
    };
    private _polygon;
    get polygon(): GeoPolygon;
    private _polygonInit;
    private _clippingPlanes;
    get clippingPlanes(): CzmClippingPlanes;
    private _clippingPlanesInit;
    get computedClippingPlanes(): import("../utils").CzmClippingPlaneCollectionJsonType | undefined;
    get computedClippingPlanesChanged(): Listener<[import("../utils").CzmClippingPlaneCollectionJsonType | undefined, import("../utils").CzmClippingPlaneCollectionJsonType | undefined]>;
    private _positionsResetting;
    constructor(id?: SceneObjectKey);
    getProperties(language?: string): import("xbsj-xe2/dist-node/xe2-base-objects").Property[];
}
export declare namespace CzmPolygonClippingPlanes {
    const createDefaultProps: () => {
        enabled: boolean;
        showHelper: boolean;
        editing: boolean;
        allowPicking: boolean;
        positions: import("xbsj-xe2/dist-node/xe2-base-utils").ReactiveVariable<[number, number, number][] | undefined>;
        reverse: boolean;
        edgeColor: import("xbsj-xe2/dist-node/xe2-base-utils").ReactiveVariable<[number, number, number, number]>;
        edgeWidth: number;
        execOnceFuncStr: string | undefined;
        updateFuncStr: string | undefined;
        toDestroyFuncStr: string | undefined;
        name: string;
        ref: string | undefined;
        devTags: import("xbsj-xe2/dist-node/xe2-base-utils").ReactiveVariable<string[] | undefined>;
        extras: import("xbsj-xe2/dist-node/xe2-base-utils").ReactiveVariable<import("xbsj-xe2/dist-node/xe2-base-utils").JsonValue>;
    };
}
export interface CzmPolygonClippingPlanes extends ReactivePropsToNativePropsAndChanged<ReturnType<typeof CzmPolygonClippingPlanes.createDefaultProps>> {
}
declare type JsonType = PartialWithUndefinedReactivePropsToNativeProps<ReturnType<typeof CzmPolygonClippingPlanes.createDefaultProps> & {
    type: string;
}>;
export {};
