/// <reference types="cesium" />
/// <reference types="xbsj-renderer/node_modules/cesium" />
/// <reference types="xbsj-renderer/dist-node/xr-cesium/__declares/__cesium" />
/// <reference types="xbsj-xe2/dist-node/xe2-cesium-objects/__declares/__cesium" />
/// <reference types="xbsj-renderer/dist-node/xr-cesium/fixcamera/fixcameraflight" />
import { Listener, PartialWithUndefinedReactivePropsToNativeProps, ReactivePropsToNativePropsAndChanged, Event } from "xbsj-xe2/dist-node/xe2-base-utils";
import { SceneObjectKey } from "xbsj-xe2/dist-node/xe2-utils";
import { ESSceneObject } from "xbsj-xe2/dist-node/xe2-base-objects";
export declare type DatesType = {
    time: string[];
    timeID: number[];
};
export declare class CzmArcgisHistoryImagery extends ESSceneObject {
    static readonly type: string;
    get typeName(): string;
    get defaultProps(): {
        execOnceFuncStr: string | undefined;
        updateFuncStr: string | undefined;
        toDestroyFuncStr: string | undefined;
        name: string;
        ref: string | undefined;
        devTags: import("xbsj-xe2/dist-node/xe2-base-utils").ReactiveVariable<string[] | undefined>;
        extras: import("xbsj-xe2/dist-node/xe2-base-utils").ReactiveVariable<import("xbsj-xe2/dist-node/xe2-base-utils").JsonValue>;
        show: boolean;
        showUI: boolean;
        currentDate: string;
        containerId: string | undefined;
        cssText: string;
    };
    get json(): JsonType;
    set json(value: JsonType);
    private _datesEvent;
    get datesEvent(): Event<[DatesType, import("cesium").Viewer]>;
    private _container;
    get container(): HTMLDivElement | undefined;
    get containerChanged(): Listener<[HTMLDivElement | undefined, HTMLDivElement | undefined]>;
    set container(value: HTMLDivElement | undefined);
    private _dates;
    get dates(): DatesType | undefined;
    get datesChanged(): Listener<[DatesType | undefined, DatesType | undefined]>;
    set dates(value: DatesType | undefined);
    constructor(id?: SceneObjectKey);
    static defaults: {
        containerId: string;
        cssText: string;
        viewerTagsEnums: [string, string][];
    };
    getProperties(language?: string): import("xbsj-xe2/dist-node/xe2-base-objects").Property[];
}
export declare namespace CzmArcgisHistoryImagery {
    const createDefaultProps: () => {
        execOnceFuncStr: string | undefined;
        updateFuncStr: string | undefined;
        toDestroyFuncStr: string | undefined;
        name: string;
        ref: string | undefined;
        devTags: import("xbsj-xe2/dist-node/xe2-base-utils").ReactiveVariable<string[] | undefined>;
        extras: import("xbsj-xe2/dist-node/xe2-base-utils").ReactiveVariable<import("xbsj-xe2/dist-node/xe2-base-utils").JsonValue>;
        show: boolean;
        showUI: boolean;
        currentDate: string;
        containerId: string | undefined;
        cssText: string;
    };
}
export interface CzmArcgisHistoryImagery extends ReactivePropsToNativePropsAndChanged<ReturnType<typeof CzmArcgisHistoryImagery.createDefaultProps>> {
}
declare type JsonType = PartialWithUndefinedReactivePropsToNativeProps<ReturnType<typeof CzmArcgisHistoryImagery.createDefaultProps> & {
    type: string;
}>;
export {};
