/// <reference types="cesium" />
/// <reference types="xbsj-renderer/node_modules/cesium" />
/// <reference types="xbsj-renderer/dist-node/xr-cesium/__declares/__cesium" />
/// <reference types="xbsj-xe2/dist-node/xe2-cesium-objects/__declares/__cesium" />
/// <reference types="xbsj-renderer/dist-node/xr-cesium/fixcamera/fixcameraflight" />
import { Listener, PartialWithUndefinedReactivePropsToNativeProps, ReactivePropsToNativePropsAndChanged, Event } from "xbsj-xe2/dist-node/xe2-base-utils";
import { SceneObjectKey } from "xbsj-xe2/dist-node/xe2-utils";
import { ESSceneObject } from "xbsj-xe2/dist-node/xe2-base-objects";
export declare class CzmGeHistoryImagery extends ESSceneObject {
    static readonly type: string;
    get typeName(): string;
    get defaultProps(): {
        execOnceFuncStr: string | undefined;
        updateFuncStr: string | undefined;
        toDestroyFuncStr: string | undefined;
        name: string;
        ref: string | undefined;
        devTags: import("xbsj-xe2/dist-node/xe2-base-utils").ReactiveVariable<string[] | undefined>;
        extras: import("xbsj-xe2/dist-node/xe2-base-utils").ReactiveVariable<import("xbsj-xe2/dist-node/xe2-base-utils").JsonValue>;
        show: boolean;
        showUI: boolean;
        currentDate: string;
        containerId: string | undefined;
        cssText: string;
    };
    get json(): JsonType;
    set json(value: JsonType);
    private _datesEvent;
    get datesEvent(): Event<[string[], import("cesium").Viewer]>;
    private _container;
    get container(): HTMLDivElement | undefined;
    get containerChanged(): Listener<[HTMLDivElement | undefined, HTMLDivElement | undefined]>;
    set container(value: HTMLDivElement | undefined);
    private _dates;
    get dates(): string[];
    get datesChanged(): Listener<[string[], string[]]>;
    set dates(value: string[]);
    constructor(id?: SceneObjectKey);
    static defaults: {
        containerId: string;
        cssText: string;
        viewerTagsEnums: [string, string][];
    };
    getProperties(language?: string): import("xbsj-xe2/dist-node/xe2-base-objects").Property[];
}
export declare namespace CzmGeHistoryImagery {
    const createDefaultProps: () => {
        execOnceFuncStr: string | undefined;
        updateFuncStr: string | undefined;
        toDestroyFuncStr: string | undefined;
        name: string;
        ref: string | undefined;
        devTags: import("xbsj-xe2/dist-node/xe2-base-utils").ReactiveVariable<string[] | undefined>;
        extras: import("xbsj-xe2/dist-node/xe2-base-utils").ReactiveVariable<import("xbsj-xe2/dist-node/xe2-base-utils").JsonValue>;
        show: boolean;
        showUI: boolean;
        currentDate: string;
        containerId: string | undefined;
        cssText: string;
    };
}
export interface CzmGeHistoryImagery extends ReactivePropsToNativePropsAndChanged<ReturnType<typeof CzmGeHistoryImagery.createDefaultProps>> {
}
declare type JsonType = PartialWithUndefinedReactivePropsToNativeProps<ReturnType<typeof CzmGeHistoryImagery.createDefaultProps> & {
    type: string;
}>;
export {};
