# EarthSDK2

## 概览

EarthSDK2

1. 安装准备
命令：npm i

2. 调试
命令：npm run dev

3. 打包
执行命令：npm run build
打包目录: ./dist-web 和 ./dist-node 

## 开发文档  
https://www.wolai.com/earthsdk/nrp963KZyrXAtmGgUeuLRo  

## 开发规范

_ES_开头为内置标签  

### 场景对象类型的内置标签规范

_ES_Misc_NoEditingOnCreating 创建时不进行自动编辑操作！  

_ES_Impl_Cesium 具有Cesium实现  
_ES_Impl_Mapbox 具有Mapbox实现  
_ES_Impl_OpenLayers 具有OpenLayers实现  
_ES_Impl_UE 具有UE实现  
_ES_Impl_Any 可能被任意渲染器的实现  
_ES_Impl_None 不需要被任意渲染器实现
_ES_Built_In 内置标签，用来构建其他场景对象的场景对象，不能独立使用！不适合通过JSON存储！

### 场景对象的内置标签规范

视口标签规范  
_ES_Viewer_开头为视口标签  
_ES_Viewer_0  
_ES_Viewer_1  
_ES_Viewer_2  
用户也可以自定义视口标签 _ES_Viewer_XXX  


