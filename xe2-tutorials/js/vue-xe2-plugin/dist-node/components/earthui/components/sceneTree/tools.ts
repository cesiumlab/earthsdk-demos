import { XbsjEarthUi } from "../../scripts/xbsjEarthUi"
function createSceneObjByJson(obj: { [key: string]: any }, xbsjEarthUi: XbsjEarthUi) {
    const sceneTree = xbsjEarthUi.getSceneTree()
    if (!sceneTree) return
    if (obj['sceneObj']) {
        const a = obj['sceneObj']
        const treeItem = sceneTree.createSceneObjectTreeItemFromJson(a)
        if (treeItem) {
            sceneTree.uiTree.clearAllSelectedItems()
            treeItem.uiTreeObject.selected = true
        }
    }
    if (obj['children']) {
        for (let i = 0; i < obj['children'].length; i++) {
            createSceneObjByJson(obj['children'][i], xbsjEarthUi);
        }
    }
}
export function meargeObj(root: string, xbsjEarthUi: XbsjEarthUi) {
    const a = JSON.parse(root)
    const obj = a.sceneTree.root
    if (obj) {
        createSceneObjByJson(obj, xbsjEarthUi);
    }
}