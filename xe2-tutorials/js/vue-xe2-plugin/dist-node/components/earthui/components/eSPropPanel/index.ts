import { Component } from "vue";
import BooleanProperty from './propertiesMenu/properties/BooleanProperty.vue';
import BooleansProperty from './propertiesMenu/properties/BooleansProperty.vue';
import Boolean2Property from './propertiesMenu/properties/Boolean2Property.vue';
import Boolean2sProperty from './propertiesMenu/properties/Boolean2sProperty.vue';
import Boolean3Property from './propertiesMenu/properties/Boolean3Property.vue';
import Boolean3sProperty from './propertiesMenu/properties/Boolean3sProperty.vue';
import Boolean4Property from './propertiesMenu/properties/Boolean4Property.vue';
import Boolean4sProperty from './propertiesMenu/properties/Boolean4sProperty.vue';
import NumberProperty from './propertiesMenu/properties/NumberProperty.vue';
import NumbersProperty from './propertiesMenu/properties/NumbersProperty.vue';
import Number2Property from './propertiesMenu/properties/Number2Property.vue';
import Number2sProperty from './propertiesMenu/properties/Number2sProperty.vue';
import Number3Property from './propertiesMenu/properties/Number3Property.vue';
import Number3sProperty from './propertiesMenu/properties/Number3sProperty.vue';
import Number4Property from './propertiesMenu/properties/Number4Property.vue';
import Number4sProperty from './propertiesMenu/properties/Number4sProperty.vue';
import StringProperty from './propertiesMenu/properties/StringProperty.vue';
import StringsProperty from './propertiesMenu/properties/StringsProperty.vue';
import String2Property from './propertiesMenu/properties/String2Property.vue';
import String2sProperty from './propertiesMenu/properties/String2sProperty.vue';
import String3Property from './propertiesMenu/properties/String3Property.vue';
import String3sProperty from './propertiesMenu/properties/String3sProperty.vue';
import String4Property from './propertiesMenu/properties/String4Property.vue';
import String4sProperty from './propertiesMenu/properties/String4sProperty.vue';
import ColorProperty from './propertiesMenu/properties/ColorProperty.vue';
import ColorRgbProperty from './propertiesMenu/properties/ColorRgbProperty.vue';
import DashPatternProperty from './propertiesMenu/properties/DashPatternProperty.vue';
import DateProperty from './propertiesMenu/properties/DateProperty.vue';
import DatesProperty from './propertiesMenu/properties/DatesProperty.vue';
import EnumProperty from './propertiesMenu/properties/EnumProperty.vue';
import EnumStringsProperty from './propertiesMenu/properties/EnumStringsProperty.vue';
import FunctionProperty from './propertiesMenu/properties/FunctionProperty.vue';
import JsonProperty from './propertiesMenu/properties/JsonProperty.vue';
import MethodProperty from './propertiesMenu/properties/MethodProperty.vue';
import MinmaxProperty from './propertiesMenu/properties/MinmaxProperty.vue';
import PositionProperty from './propertiesMenu/properties/PositionProperty.vue';
import Number4WithUndefinedProperty from './propertiesMenu/properties/Number4WithUndefinedProperty.vue';
import NumberSliderProperty from './propertiesMenu/properties/NumberSliderProperty.vue';
import EvalStringProperty from './propertiesMenu/properties/EvalStringProperty.vue';
import LongStringProperty from './propertiesMenu/properties/LongStringProperty.vue';
import NearFarScalerProperty from './propertiesMenu/properties/NearFarScalerProperty.vue';
import NonreactiveJsonStringProperty from './propertiesMenu/properties/NonreactiveJsonStringProperty.vue';
import PlayerProperty from './propertiesMenu/properties/PlayerProperty.vue';
import PositionsProperty from './propertiesMenu/properties/PositionsProperty.vue';
import PositionsSetPropety from './propertiesMenu/properties/PositionsSetPropety.vue';
import RotationProperty from './propertiesMenu/properties/RotationProperty.vue';
import UriProperty from './propertiesMenu/properties/UriProperty.vue';
import ViewPlayerProperty from './propertiesMenu/properties/ViewPlayerProperty.vue';


export const propComps: { [k: string]: Component } = {
    BooleanProperty: BooleanProperty,
    // BooleansProperty: BooleansProperty,
    // Boolean2Property: Boolean2Property,
    // Boolean2sProperty: Boolean2sProperty,
    // Boolean3Property: Boolean3Property,
    // Boolean3sProperty: Boolean3sProperty,
    // Boolean4Property: Boolean4Property,
    // Boolean4sProperty: Boolean4sProperty,
    NumberProperty: NumberProperty,
    // NumbersProperty: NumbersProperty,
    Number2Property: Number2Property,
    Number2sProperty: Number2sProperty,
    Number3Property: Number3Property,
    Number3sProperty: Number3sProperty,
    Number4Property: Number4Property,
    // Number4sProperty: Number4sProperty,
    StringProperty: StringProperty,
    // StringsProperty: StringsProperty,
    // String2Property: String2Property,
    // String2sProperty: String2sProperty,
    // String3Property: String3Property,
    // String3sProperty: String3sProperty,
    // String4Property: String4Property,
    // String4sProperty: String4sProperty,
    ColorProperty: ColorProperty,
    // ColorRgbProperty: ColorRgbProperty,
    DashPatternProperty: DashPatternProperty,
    DateProperty: DateProperty,
    DatesProperty: DatesProperty,
    EnumProperty: EnumProperty,
    // EnumStringsProperty: EnumStringsProperty,
    FunctionProperty: FunctionProperty,
    JsonProperty: JsonProperty,
    // MethodProperty: MethodProperty,
    // MinmaxProperty: MinmaxProperty,
    PositionProperty: PositionProperty,
    // Number4WithUndefinedProperty: Number4WithUndefinedProperty,
    NumberSliderProperty: NumberSliderProperty,
    EvalStringProperty: EvalStringProperty,
    LongStringProperty: LongStringProperty,
    // NearFarScalerProperty: NearFarScalerProperty,
    NonreactiveJsonStringProperty: NonreactiveJsonStringProperty,
    PlayerProperty: PlayerProperty,
    PositionsProperty: PositionsProperty,
    PositionsSetPropety: PositionsSetPropety,
    RotationProperty: RotationProperty,
    UriProperty: UriProperty,
    ViewPlayerProperty: ViewPlayerProperty,
};
