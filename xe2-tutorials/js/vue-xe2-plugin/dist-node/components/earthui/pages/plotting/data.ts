//文字
export const textTypeList: { name: string, img: string, type: { textSize: number, textColor: [number, number, number, number] } }[] = [
    {
        name: '文本',
        img: require('../../assets/plotting/text/l-white.png'),
        type: {
            textSize: 14,
            textColor: [1, 1, 1, 1]
        }
    },
    // {
    //     name: '白中文本',
    //     img: require('../../assets/plotting/text/m-white.png'),
    //     type: {
    //         textSize: 18,
    //         textColor: [1, 1, 1, 1]
    //     }
    // },
    // {
    //     name: '白大文本',
    //     img: require('../../assets/plotting/text/b-white.png'),
    //     type: {
    //         textSize: 22,
    //         textColor: [1, 1, 1, 1]
    //     }
    // },
    // {
    //     name: '红小文本',
    //     img: require('../../assets/plotting/text/l-red.png'),
    //     type: {
    //         textSize: 14,
    //         textColor: [1, 0, 0, 1]
    //     }
    // },
    // {
    //     name: '红中文本',
    //     img: require('../../assets/plotting/text/m-red.png'),
    //     type: {
    //         textSize: 18,
    //         textColor: [1, 0, 0, 1]
    //     }
    // },
    // {
    //     name: '红大文本',
    //     img: require('../../assets/plotting/text/b-red.png'),
    //     type: {
    //         textSize: 22,
    //         textColor: [1, 0, 0, 1]
    //     }
    // },
    // {
    //     name: '绿小文本',
    //     img: require('../../assets/plotting/text/l-green.png'),
    //     type: {
    //         textSize: 14,
    //         textColor: [0, 1, 0, 1]
    //     }
    // },
    // {
    //     name: '绿中文本',
    //     img: require('../../assets/plotting/text/m-green.png'),
    //     type: {
    //         textSize: 18,
    //         textColor: [0, 1, 0, 1]
    //     }
    // },
    // {
    //     name: '绿大文本',
    //     img: require('../../assets/plotting/text/b-green.png'),
    //     type: {
    //         textSize: 22,
    //         textColor: [0, 1, 0, 1]
    //     }
    // },
    // {
    //     name: '蓝小文本',
    //     img: require('../../assets/plotting/text/l-blue.png'),
    //     type: {
    //         textSize: 14,
    //         textColor: [0, 0, 1, 1]
    //     }
    // },
    // {
    //     name: '蓝中文本',
    //     img: require('../../assets/plotting/text/m-blue.png'),
    //     type: {
    //         textSize: 18,
    //         textColor: [0, 0, 1, 1]
    //     }
    // },
    // {
    //     name: '蓝大文本',
    //     img: require('../../assets/plotting/text/b-blue.png'),
    //     type: {
    //         textSize: 22,
    //         textColor: [0, 0, 1, 1]
    //     }
    // }
]
//图标点
export const imageTypeList: { name: string, img: string, type: string }[] = [
    {
        name: '蓝色相机',
        img: require('../../assets/plotting/points/CameraBlue.png'),
        type: ('inner://CameraBlue.png')
    }, {
        name: '绿色相机',
        img: require('../../assets/plotting/points/CameraGreen.png'),
        type: ('inner://CameraGreen.png')
    }, {
        name: '蓝色车辆',
        img: require('../../assets/plotting/points/CarBlue.png'),
        type: ('inner://CarBlue.png')
    }, {
        name: '红色车辆',
        img: require('../../assets/plotting/points/CarRed.png'),
        type: ('inner://CarRed.png')
    }, {
        name: '渡口',
        img: require('../../assets/plotting/points/Dukou.png'),
        type: ('inner://Dukou.png')
    }, {
        name: '界限',
        img: require('../../assets/plotting/points/Jiexian.png'),
        type: ('inner://Jiexian.png')
    }, {
        name: '警示',
        img: require('../../assets/plotting/points/Jingshi.png'),
        type: ('inner://Jingshi.png')
    }, {
        name: '蓝色料斗',
        img: require('../../assets/plotting/points/LiaodouBlue.png'),
        type: ('inner://LiaodouBlue.png')
    }, {
        name: '红色料斗',
        img: require('../../assets/plotting/points/LiaodouRed.png'),
        type: ('inner://LiaodouRed.png')
    }, {
        name: '蓝色龙门吊',
        img: require('../../assets/plotting/points/LongmendiaoBlue.png'),
        type: ('inner://LongmendiaoBlue.png')
    }, {
        name: '红色龙门吊',
        img: require('../../assets/plotting/points/LongmendiaoRed.png'),
        type: ('inner://LongmendiaoRed.png')
    }, {
        name: '锚',
        img: require('../../assets/plotting/points/Mao.png'),
        type: ('inner://Mao.png')
    }, {
        name: '蓝色门机',
        img: require('../../assets/plotting/points/MenjiBlue.png'),
        type: ('inner://MenjiBlue.png')
    }, {
        name: '红色门机',
        img: require('../../assets/plotting/points/MenjiRed.png'),
        type: ('inner://MenjiRed.png')
    }, {
        name: '鸣笛',
        img: require('../../assets/plotting/points/Mingdi.png'),
        type: ('inner://Mingdi.png')
    }, {
        name: '气象站',
        img: require('../../assets/plotting/points/Qixiang.png'),
        type: ('inner://Qixiang.png')
    }, {
        name: '蓝色人员',
        img: require('../../assets/plotting/points/RenyuanBlue.png'),
        type: ('inner://RenyuanBlue.png')
    }, {
        name: '红色人员',
        img: require('../../assets/plotting/points/RenyuanRed.png'),
        type: ('inner://RenyuanRed.png')
    }, {
        name: '圆形蓝色人员',
        img: require('../../assets/plotting/points/RenyuanCircleBlue.png'),
        type: ('inner://RenyuanCircleBlue.png')
    }, {
        name: '圆形红色人员',
        img: require('../../assets/plotting/points/RenyuanCircleRed.png'),
        type: ('inner://RenyuanCircleRed.png')
    }
]
//矢量
export const vectorObjectList: { type: string, zh: string, icon: string,leftButton:boolean }[] = [
    {
        type: 'esGeoLineString',
        zh: '折线',
        icon: 'zhexian',
        leftButton:true
    },
    {
        type: 'esGeoPolygon',
        zh: '多边形',
        icon: 'duobianxing',
        leftButton:false
    }, {
        type: 'esLocalCircle',
        zh: '圆',
        icon: 'yuan',
        leftButton:true
    },
    {
        type: 'esGeoRectangle',
        zh: '矩形',
        icon: 'juxing',
        leftButton:false
    }
]

//注记
export const annotationObjectList: { type: string, zh: string, icon: string,leftButton:boolean }[] = [
    {
        type: 'esTextLabel',
        zh: '文字标注',
        icon: 'wenzibiaozhu',
        leftButton:true
    },
    {
        type: 'esImageLabel',
        zh: '图标点',
        icon: 'tubiaodian',
        leftButton:false
    },
    {
        type: 'esWidget',
        zh: '部件',
        icon: 'bujian',
        leftButton:true
    },
    {
        type: 'esGeoDiv',
        zh: '自定义DIV',
        icon: 'zidingyi2',
        leftButton:false
    },
]
//园区
export const parkObjectList: { type: string, zh: string, icon: string,leftButton:boolean }[] = [
    {
        type: 'esHuman',
        zh: '人员',
        icon: 'renyuan',
        leftButton:true
    },
    {
        type: 'esCar',
        zh: '车辆',
        icon: 'cheliang',
        leftButton:false
    },
    {
        type: 'esGltfModel',
        zh: '建筑',
        icon: 'jianzhu',
        leftButton:true
    }
]
//特效
export const effectObjectList: { type: string, zh: string, icon: string,leftButton:boolean }[] = [
    {
        type: 'esPath',
        zh: '路径',
        icon: 'lujingdonghua',
        leftButton:true
    },
    {
        type: 'esApertureEffect',
        zh: '光圈特效',
        icon: 'guangquantexiao',
        leftButton:false
    },
    {
        type: 'esPolygonFence',
        zh: '电子围栏',
        icon: 'dianziweilan',
        leftButton:true
    },
    {
        type: 'esPipeFence',
        zh: '管道围栏',
        icon: 'weilan',
        leftButton:false
    },
    {
        type: 'esAlarm',
        zh: '报警',
        icon: 'baojing',
        leftButton:true
    },
    {
        type: 'esCameraVisibleRange',
        zh: '摄像头',
        icon: 'shexiangtou',
        leftButton:false
    },
    {
        type: 'esVideoFusion',
        zh: '视频融合',
        icon: 'shipinronghe',
        leftButton:true
    },
    {
        type: 'esLocalSkyBox',
        zh: '天空盒',
        icon: 'tiankonghe',
        leftButton:false
    },

]
//actor
export const ueObjectList: { type: string, zh: string, icon: string,leftButton:boolean }[] = [

    {
        type: 'esUnrealActor',
        zh: 'actor',
        icon: 'actor',
        leftButton:true
    }
]

