//封装 get 请求
export function get<T>(url: string, token?: string): Promise<T> {
    let header: any;
    if (token) {
        header = {
            'Content-Type': 'application/json',
            'Authorization': 'Bearer ' + token
        }
    } else {
        header = {
            'Content-Type': 'application/json',
            'Authorization': localStorage.getItem('token')
        }
    }

    return fetch(url, { headers: header })
        .then(response => {
            if (!response.ok) {
                throw new Error(response.statusText);
            }
            return response.json();
        });
}
//封装 get 请求
export function getNoToken<T>(url: string): Promise<T> {
    let header: any;
    return fetch(url, { headers: header })
        .then(response => {
            if (!response.ok) {
                throw new Error(response.statusText);
            }
            return response.json();
        });
}
//封装 deleteApi 请求
export function deleteApi<T>(url: string): Promise<T> {
    return fetch(url, { method: 'DELETE' })
        .then(response => {
            if (!response.ok) {
                throw new Error(response.statusText);
            }
            return response.json();
        });
}
// 封装 POST 请求
export function post<T>(url: string, data: any): Promise<T> {
    const header = {
        'Content-Type': 'application/json',
        'Authorization': localStorage.getItem('token') as string
    }
    return fetch(url, {
        method: 'POST',
        headers: header,
        body: JSON.stringify(data)
    })
        .then(response => {
            if (!response.ok) {
                throw new Error(response.statusText);
            }
            return response.json();
        });
}

// 封装 PUT 请求
export function put<T>(url: string, data: any, token?: string): Promise<T> {
    let header: any
    if (token) {
        header = {
            'Content-Type': 'application/json',
            'Authorization': 'Bearer ' + token
        }
    } else {
        header = {
            'Content-Type': 'application/json',
            'Authorization': localStorage.getItem('token')
        }
    }
    return fetch(url, {
        method: 'PUT',
        headers: header,
        body: JSON.stringify(data)
    })
        .then(response => {
            if (!response.ok) {
                throw new Error(response.statusText);
            }
            return response.json();
        });
}
