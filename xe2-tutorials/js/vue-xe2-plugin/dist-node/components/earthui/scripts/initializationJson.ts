
import { get } from '../api/service'
import { parse } from 'search-params'
import { ES3DTileset, ESImageryLayer, ESTerrainLayer } from "esobjs-xe2-plugin/dist-node/esobjs-xe2-plugin-main";
import { Message } from "earthsdk-ui"
import { XbsjEarthUi } from './xbsjEarthUi';
const search = window.location.search.substring(1)
const parseSearch = parse(search)
//scene
function initSceneId(xbsjEarthUi: XbsjEarthUi) {
    const id = parseSearch.scene
    get(`${xbsjEarthUi.cesiumLabUrl}/tile/scene/?id=${id}`).then((res: any) => {
        if (res.code === 1000) {
            const resJson = JSON.parse(res.data.content)
            if (!resJson) return
            xbsjEarthUi.json = resJson
            Message.success(`加载场景成功：${id}`)
        } else {
            initSceneFetch(xbsjEarthUi)
        }
    }).catch(error => {
        console.log(error);
        initSceneFetch(xbsjEarthUi)
    })
}
//esss
function initESSSsceneidId(xbsjEarthUi: XbsjEarthUi) {
    const sceneid = parseSearch.ESSSsceneid as string
    const appid = parseSearch.appid as string
    const token = parseSearch.token as string
    if (!token) {
        Message.error(`无token值`)
    } else if (!sceneid) {
        Message.error(`无id值`)
    } else if (!appid) {
        Message.error(`无场景id`)
    } else {
        // const origin = window.location.origin
        // const origin = 'http://localhost:8086';
        get(`${xbsjEarthUi.esssUrl}/staticscene/${sceneid}`, token).then((res: any) => {
            if (res.status === 'ok') {
                Message.success(`加载场景成功：${appid}`)
                const options = {
                    domid: 'viewersContainer',
                    uri: origin,
                    app: appid
                }
                const viewer = xbsjEarthUi.switchToUEViewer(options)
                viewer.innerViewer.sunSkyControlled = true
                const resJson = res.data.content
                if (!resJson) {
                    initSceneFetch(xbsjEarthUi)
                } else {
                    xbsjEarthUi.json = resJson
                }
            } else {
                initSceneFetch(xbsjEarthUi)
            }
        }).catch(error => {
            console.log(error);
            initSceneFetch(xbsjEarthUi)
        })
    }
}

// function initSceneDefault(xbsjEarthUi: XbsjEarthUi) {
//     get(`${xbsjEarthUi.cesiumLabUrl}/tile/scene/default`).then((res: any) => {
//         if (res) {
//             xbsjEarthUi.json = res
//             Message.success('加载场景成功')
//         } else {
//             initSceneFetch(xbsjEarthUi)
//         }
//     }).catch(error => {
//         console.log(error);
//         initSceneFetch(xbsjEarthUi)
//     })
// }
//json文件
function initSceneFetch(xbsjEarthUi: XbsjEarthUi) {
    fetch('./data/scene.json').then(response => response.json()).then((data) => {
        if (data) {
            xbsjEarthUi.json = data
            Message.success('加载默认场景成功')
        }
    });
}


//初始化
export function initSceneJson(xbsjEarthUi: XbsjEarthUi) {
    if (search) {
        if (parseSearch.scene) {//存在scene
            initSceneId(xbsjEarthUi)
        } else if (parseSearch.ESSSsceneid) {//存在scene
            initESSSsceneidId(xbsjEarthUi)
        } else {
            initSceneFetch(xbsjEarthUi)
        }
    } else {
        initSceneFetch(xbsjEarthUi)
    }
}
function a(v: any) {
    if (v === undefined || v === null) return undefined
    return +v
}
export function initSceneWithType(xbsjEarthUi: XbsjEarthUi) {
    const sceneTree = xbsjEarthUi.getSceneTree()
    if (!sceneTree) return
    if (parseSearch.type) {
        if (parseSearch.type === 'images') {//存在images
            const rectangle = [+(parseSearch.west ?? -180), +(parseSearch.south ?? -90), +(parseSearch.east ?? 180), +(parseSearch.north ?? 90)]
            // console.log('images', parseSearch);
            const czmTilingScheme = parseSearch.proj === "4326" ? "GeographicTilingScheme" : "WebMercatorTilingScheme"
            const url = parseSearch.tiletrans === "tms" ? `${parseSearch.url}/tilemapresource.xml` : `${parseSearch.url}`
            const imagesJson =
            {
                "type": "ESImageryLayer",
                "url": url,
                "rectangle": rectangle,
                "czmTilingScheme": czmTilingScheme,
                "allowPicking": true,
                "name": `${parseSearch.name}`,
                "minimumLevel": a(parseSearch.minzoom),
                "maximumLevel": a(parseSearch.maxzoom),
                "czmTileWidth": a(parseSearch.tilesize),
                "czmTileHeight": a(parseSearch.tilesize),
                "zIndex": 1
            }
            const treeItem = sceneTree.createSceneObjectTreeItemFromJson<ESImageryLayer>(imagesJson);
            if (!treeItem) return
            sceneTree.uiTree.clearAllSelectedItems()
            treeItem.uiTreeObject.selected = true
            const { sceneObject } = treeItem
            if (!(sceneObject instanceof ESImageryLayer)) return
            setTimeout(() => {
                sceneObject.flyTo()
            }, 1000);
        } else if (parseSearch.type === "terrains") {//存在terrains
            const rectangle = [+(parseSearch.west ?? -180), +(parseSearch.south ?? -90), +(parseSearch.east ?? 180), +(parseSearch.north ?? 90)]
            const terrainsJson = {
                "type": "ESTerrainLayer",
                "url": `${parseSearch.url}`,
                "rectangle": rectangle,
                "allowPicking": true,
                "name": `${parseSearch.name}`,
            }
            const treeItem = sceneTree.createSceneObjectTreeItemFromJson<ESTerrainLayer>(terrainsJson);
            if (!treeItem) return
            sceneTree.uiTree.clearAllSelectedItems()
            treeItem.uiTreeObject.selected = true
            const { sceneObject } = treeItem
            if (!(sceneObject instanceof ESTerrainLayer)) return
            setTimeout(() => {
                sceneObject.flyTo()
            }, 1000);
        } else if (parseSearch.type === "models") {//存在models
            const tilesetJson = {
                "type": "ES3DTileset",
                "url": `${parseSearch.url}`,
                "allowPicking": true,
                "name": `${parseSearch.name}`,
            }
            const treeItem = sceneTree.createSceneObjectTreeItemFromJson<ES3DTileset>(tilesetJson);
            if (!treeItem) return
            sceneTree.uiTree.clearAllSelectedItems()
            treeItem.uiTreeObject.selected = true
            const { sceneObject } = treeItem
            if (!(sceneObject instanceof ES3DTileset)) return
            setTimeout(() => {
                sceneObject.flyTo()
            }, 1000);
        }
    }
}
//初始化地址
export function initurl(xbsjEarthUi: XbsjEarthUi) {
    if (parseSearch.czmlabPath) {
        xbsjEarthUi.czmlabPath = parseSearch.czmlabPath as string
        console.log('czmlab-path:', xbsjEarthUi.czmlabPath);
    }
    if (parseSearch.cesiumLabUrl) {
        xbsjEarthUi.cesiumLabUrl = parseSearch.cesiumLabUrl as string
        console.log('cesiumLabUrl:', xbsjEarthUi.czmlabPath);
    }

}


