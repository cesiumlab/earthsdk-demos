import { JsonValue } from "xbsj-xe2/dist-node/xe2-base-utils";

export type ViewJson = {
    collection: JsonValue[];
    cnView: JsonValue;
    globalView: JsonValue;
    defaultIndex: number;
};
