import { ESCameraView, ESCameraViewCollection } from "esobjs-xe2-plugin/dist-node/esobjs-xe2-plugin-main";
import { Destroyable } from "xbsj-xe2/dist-node/xe2-base-utils";
import { XbsjEarthUi } from "../xbsjEarthUi";

export class ViewsManager extends Destroyable {
    private _cnView: ESCameraView; // 中国视角
    get cnView() { return this._cnView; }
    private _globalView: ESCameraView; // 全球视角
    get globalView() { return this._globalView; }

    constructor(private _xbsjEarthUi: XbsjEarthUi) {
        super();
        const xbsjEarthUi = this._xbsjEarthUi;
        // cn
        {
            const view = xbsjEarthUi.createSceneObject(ESCameraView);
            if (!view) throw new Error(` cn projectManager.createSceneObject error!`);
            this.d(() => xbsjEarthUi.destroySceneObject(view));
            view.position = [106.24551319129831, 37.20974503372304, 9812382.238207458];
            view.rotation = [0.1, -90, 0];
            view.show = false
            this._cnView = view;
        }
        // global
        {
            const view = xbsjEarthUi.createSceneObject(ESCameraView);
            if (!view) throw new Error(` global projectManager.createSceneObject error!`);
            this.d(() => xbsjEarthUi.destroySceneObject(view));
            view.position = [120.36355117699637, 28.909792305870496, 22191655.672192514];
            view.rotation = [0.1, -90, 0];
            view.show = false
            this._globalView = view;
        }
    }
}
