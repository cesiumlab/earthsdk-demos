import { Destroyable, react } from "xbsj-xe2/dist-node/xe2-base-utils";
import { CzmDepthOfFieldPostProcess, CzmViewer } from "xbsj-xe2/dist-node/xe2-cesium-objects";

import { XbsjEarthUi } from "../xbsjEarthUi";
import { UeViewer } from "xbsj-xe2/dist-node/xe2-ue-objects";
export class Reprocess extends Destroyable {
    //阴影
    private _shadowsShow = this.dv(react<boolean>(false));
    get shadowsShow() { return this._shadowsShow.value; }
    get shadowsShowChanged() { return this._shadowsShow.changed; }
    set shadowsShow(value: boolean) { this._shadowsShow.value = value; }

    //环境遮蔽
    private _ambientOcclusionShow = this.dv(react<boolean>(false));
    get ambientOcclusionShow() { return this._ambientOcclusionShow.value; }
    get ambientOcclusionShowChanged() { return this._ambientOcclusionShow.changed; }
    set ambientOcclusionShow(value: boolean) { this._ambientOcclusionShow.value = value; }

    //泛光
    private _floodlightShow = this.dv(react<boolean>(false));
    get floodlightShow() { return this._floodlightShow.value; }
    get floodlightShowChanged() { return this._floodlightShow.changed; }
    set floodlightShow(value: boolean) { this._floodlightShow.value = value; }

     //自动曝光
     private _autoExposureShow = this.dv(react<boolean>(false));
     get autoExposureShow() { return this._autoExposureShow.value; }
     get autoExposureShowChanged() { return this._autoExposureShow.changed; }
     set autoExposureShow(value: boolean) { this._autoExposureShow.value = value; }


    constructor(xbsjEarthUi: XbsjEarthUi) {
        super();

        {//阴影
            const update = () => {
                const viewer = xbsjEarthUi.activeViewer?.innerViewer as unknown as CzmViewer
                if (!viewer) {
                    return
                }
                viewer.shadows = this.shadowsShow
            }
            update();
            this.d(this.shadowsShowChanged.don(update));
        }
        {//环境遮蔽
            const update = () => {
                const viewer = xbsjEarthUi.activeViewer?.innerViewer as unknown as CzmViewer
                if (!viewer) {
                    return
                }
                viewer.scenePpsAmbientOcclusionEnabled = this.ambientOcclusionShow
            }
            update();
            this.d(this.ambientOcclusionShowChanged.don(update));
        }
        {//泛光
            const update = () => {
                const viewer = xbsjEarthUi.activeViewer?.innerViewer as unknown as CzmViewer
                if (!viewer) {
                    return
                }
                viewer.scenePpsBloomEnabled = this.floodlightShow
            }
            update();
            this.d(this.floodlightShowChanged.don(update));
        }
         {//自动曝光
            const update = () => {
                const viewer = xbsjEarthUi.activeViewer?.innerViewer as unknown as UeViewer
                if (!viewer) {
                    return
                }
                viewer.autoExposure = this.autoExposureShow
            }
            update();
            this.d(this.autoExposureShowChanged.don(update));
        }
    }
}