import { ESNavigator, ESScale, ESViewerStatusBar } from 'esobjs-xe2-plugin/dist-node/esobjs-xe2-plugin-main';
import { Destroyable } from "xbsj-xe2/dist-node/xe2-base-utils";
import { XbsjEarthUi } from "../xbsjEarthUi";
export class ClassicNavigatorManager extends Destroyable {

    private _navigator?: ESNavigator;//导航
    get navigator() { return this._navigator; }

    private _scale?: ESScale;//比例尺
    get scale() { return this._scale; }

    private _statusBar?: ESViewerStatusBar;//状态栏
    get statusBar() { return this._statusBar; }

    constructor(private xbsjEarthUi: XbsjEarthUi) {
        super();

        this._navigator = this.xbsjEarthUi.createSceneObject(ESNavigator);
        this.d(() => this._navigator && this.xbsjEarthUi.destroySceneObject(this._navigator));

        this._scale = this.xbsjEarthUi.createSceneObject(ESScale);
        this.d(() => this._scale && this.xbsjEarthUi.destroySceneObject(this._scale));

        this._statusBar = this.xbsjEarthUi.createSceneObject(ESViewerStatusBar);
        if (this.statusBar) {
            this.statusBar.height = 40
            this.statusBar.bgColor = [37 / 255, 38 / 255, 42 / 255, 0.8]
        }
        this.d(() => this._statusBar && this.xbsjEarthUi.destroySceneObject(this._statusBar));

    }
}
