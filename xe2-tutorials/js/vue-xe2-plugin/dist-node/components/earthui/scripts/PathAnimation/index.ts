import { ESPath, ESPlayer } from 'esobjs-xe2-plugin/dist-node/esobjs-xe2-plugin-main';
import { ObjResettingWithEvent, geoDistance } from "xbsj-xe2/dist-node/xe2-base-objects";
import { Destroyable, createNextAnimateFrameEvent, react } from "xbsj-xe2/dist-node/xe2-base-utils";
import { XbsjEarthUi } from "../xbsjEarthUi";

class PathEvent extends Destroyable {
    constructor(pathAnimation: PathAnimation, xbsjEarthUi: XbsjEarthUi) {
        super();
        const esPath = xbsjEarthUi.getSceneObjectById(pathAnimation.esPathId) as ESPath
        const esModel = xbsjEarthUi.getSceneObjectById(pathAnimation.esModelId)
        if (esPath && esModel) {
            this.d(esPath.currentPositionChanged.don(() => {
                if (!esPath.currentPosition) return;
                if (esModel && esPath) {
                    //@ts-ignore
                    esModel.position = esPath.currentPosition as [number, number, number]
                    if (!esPath.currentRotation) return;
                    //@ts-ignore
                    esModel.rotation = esPath.currentRotation as [number, number, number]
                }
            }))
            const points = esPath.points
            if (!points || points.length < 2) {
                return
            }
            let timeStamps = [0]
            for (var i = 0; i < points.length - 1; i++) {
                const distance = geoDistance(points[i], points[i + 1])
                const time = (distance / pathAnimation.speed * 1000) + timeStamps[i]
                timeStamps.push(Number((time).toFixed(0)))
            }
            pathAnimation.esPlayer.startTime = timeStamps[0]
            pathAnimation.esPlayer.stopTime = timeStamps[timeStamps.length - 1]
            esPath.timeStamps = timeStamps
        }
    }
}
export class PathAnimation extends Destroyable {

    private _esPlayer = this.xbsjEarthUi.createSceneObject(ESPlayer) as ESPlayer;//播放器
    get esPlayer() { return this._esPlayer; }

    private _esPathId = this.dv(react<string>(''));//路径id
    get esPathId() { return this._esPathId.value; }
    get esPathIdChanged() { return this._esPathId.changed; }
    set esPathId(value: string) { this._esPathId.value = value; }

    private _esModelId = this.dv(react<string>(''));//模型id
    get esModelId() { return this._esModelId.value; }
    get esModelIdChanged() { return this._esModelId.changed; }
    set esModelId(value: string) { this._esModelId.value = value; }

    private _speed = this.dv(react<number>(1));//速度
    get speed() { return this._speed.value; }
    get speedChanged() { return this._speed.changed; }
    set speed(value: number) { this._speed.value = value; }

    public readonly syncEvent = this.disposeVar(createNextAnimateFrameEvent(
        this.esPathIdChanged,
        this.esModelIdChanged,
        this.speedChanged
    ))

    private _Event = this.disposeVar(new ObjResettingWithEvent(this.syncEvent, () => {
        if (!this.esPathId || !this.esModelId) return undefined;
        return new PathEvent(this, this.xbsjEarthUi);
    }));

    constructor(private xbsjEarthUi: XbsjEarthUi) {
        super();
    }

}
