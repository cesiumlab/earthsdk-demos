import { SMGeoPlottingBase } from "../SMGeoPlottingBase";
/**
 * 手绘面
 * 由鼠标移动轨迹而形成的手绘面
 */
export declare class SMGeoFreePolygon extends SMGeoPlottingBase<SuperMap.Geometry.GeoFreePolygon> {
    static readonly type: string;
    get typeName(): string;
    constructor(id?: string);
}
