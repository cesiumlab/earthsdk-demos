import { SMGeoPlottingBase } from "../SMGeoPlottingBase";
/**
 * 闭合曲线
 * 使用三个或三个以上控制点直接创建闭合曲线
*/
export declare class SMGeoCloseCurve extends SMGeoPlottingBase<SuperMap.Geometry.GeoCloseCurve> {
    static readonly type: string;
    get typeName(): string;
    constructor(id?: string);
}
