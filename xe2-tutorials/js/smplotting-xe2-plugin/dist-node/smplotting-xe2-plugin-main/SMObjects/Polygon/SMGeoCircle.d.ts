import { SMGeoPlottingBase } from "../SMGeoPlottingBase";
/**
 * 圆
 * 使用圆心和圆上一点绘制出一个圆
 */
export declare class SMGeoCircle extends SMGeoPlottingBase<SuperMap.Geometry.GeoCircle> {
    static readonly type: string;
    get typeName(): string;
    constructor(id?: string);
}
