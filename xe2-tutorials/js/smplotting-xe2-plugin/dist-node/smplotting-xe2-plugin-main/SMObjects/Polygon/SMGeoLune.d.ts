import { ReactivePropsToNativePropsAndChanged } from "xbsj-xe2/dist-node/xe2-base-utils";
import { SMGeoPlottingBase } from "../SMGeoPlottingBase";
/**
 * 弓形符号
 * 使用三点绘制弓形符号
*/
export declare class SMGeoLune extends SMGeoPlottingBase<SuperMap.Geometry.GeoLune> {
    static readonly type: string;
    get typeName(): string;
    static defaults: {
        sides: number;
        viewDistanceRange: [number, number, number, number];
        viewerTagsEnums: [string, string][];
    };
    constructor(id?: string);
    getProperties(language?: string): import("xbsj-xe2/dist-node/xe2-base-objects").Property[];
}
export declare namespace SMGeoLune {
    const createDefaultProps: () => {
        execOnceFuncStr: string | undefined;
        updateFuncStr: string | undefined;
        toDestroyFuncStr: string | undefined;
        name: string;
        ref: string | undefined;
        devTags: import("xbsj-xe2/dist-node/xe2-base-utils").ReactiveVariable<string[] | undefined>;
        extras: import("xbsj-xe2/dist-node/xe2-base-utils").ReactiveVariable<import("xbsj-xe2/dist-node/xe2-base-utils").JsonValue>;
        show: boolean;
        fill: boolean;
        allowPicking: boolean;
        ground: boolean;
        outline: boolean;
        outlineColor: import("xbsj-xe2/dist-node/xe2-base-utils").ReactiveVariable<[number, number, number, number]>;
        outlineWidth: number;
        color: import("xbsj-xe2/dist-node/xe2-base-utils").ReactiveVariable<[number, number, number, number]>;
        editing: boolean;
        pointEditing: boolean;
        positions: import("xbsj-xe2/dist-node/xe2-base-utils").ReactiveVariable<[number, number, number][] | undefined>;
        depth: number;
        depthTest: boolean;
        viewDistanceRange: import("xbsj-xe2/dist-node/xe2-base-utils").ReactiveVariable<[number, number, number, number] | undefined>;
        viewDistanceDebug: boolean;
        sides: number | undefined;
    };
}
export interface SMGeoLune extends ReactivePropsToNativePropsAndChanged<ReturnType<typeof SMGeoLune.createDefaultProps>> {
}
