import { SMGeoPlottingBase } from "../SMGeoPlottingBase";
/**
 * 矩形
 * 使用两个控制点直接创建矩形
 */
export declare class SMGeoRoundedRect extends SMGeoPlottingBase<SuperMap.Geometry.GeoRoundedRect> {
    static readonly type: string;
    get typeName(): string;
    constructor(id?: string);
}
