import { SMGeoPlottingBase } from "../SMGeoPlottingBase";
/**
 * 多边形
 * 使用三个或三个以上控制点直接创建多边形
 */
export declare class SMGeoPolygonEx extends SMGeoPlottingBase<SuperMap.Geometry.GeoPolygonEx> {
    static readonly type: string;
    get typeName(): string;
    constructor(id?: string);
}
