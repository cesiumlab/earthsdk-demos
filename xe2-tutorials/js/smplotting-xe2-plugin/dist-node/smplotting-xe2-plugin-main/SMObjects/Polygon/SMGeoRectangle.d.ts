import { SMGeoPlottingBase } from "../SMGeoPlottingBase";
/**
 * 圆角矩形
 * 使用两个控制点直接创建圆角矩形
 */
export declare class SMGeoRectangle extends SMGeoPlottingBase<SuperMap.Geometry.GeoRectangle> {
    static readonly type: string;
    get typeName(): string;
    constructor(id?: string);
}
