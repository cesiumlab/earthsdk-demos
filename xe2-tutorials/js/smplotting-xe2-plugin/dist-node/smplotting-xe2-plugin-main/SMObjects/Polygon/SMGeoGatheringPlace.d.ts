import { SMGeoPlottingBase } from "../SMGeoPlottingBase";
/**
 * 聚集地符号
 * 使用两个控制点直接创建聚集地符号
 */
export declare class SMGeoGatheringPlace extends SMGeoPlottingBase<SuperMap.Geometry.GeoGatheringPlace> {
    static readonly type: string;
    get typeName(): string;
    constructor(id?: string);
}
