import { ESSceneObject, PickedInfo, PositionsEditing } from "xbsj-xe2/dist-node/xe2-base-objects";
import { Event, JsonValue, Listener, PartialWithUndefinedReactivePropsToNativeProps, ReactivePropsToNativePropsAndChanged } from "xbsj-xe2/dist-node/xe2-base-utils";
export declare abstract class SMGeoMultiLinePlottingBase<T extends SuperMap.Geometry.GeoMultiLinePlotting> extends ESSceneObject {
    private _maxPointsNum?;
    get defaultProps(): {
        execOnceFuncStr: string | undefined;
        updateFuncStr: string | undefined;
        toDestroyFuncStr: string | undefined;
        name: string;
        ref: string | undefined;
        devTags: import("xbsj-xe2/dist-node/xe2-base-utils").ReactiveVariable<string[] | undefined>;
        extras: import("xbsj-xe2/dist-node/xe2-base-utils").ReactiveVariable<JsonValue>;
        show: boolean;
        allowPicking: boolean;
        positions: import("xbsj-xe2/dist-node/xe2-base-utils").ReactiveVariable<[number, number, number][] | undefined>;
        width: number;
        ground: boolean;
        color: import("xbsj-xe2/dist-node/xe2-base-utils").ReactiveVariable<[number, number, number, number]>;
        hasDash: boolean;
        gapColor: import("xbsj-xe2/dist-node/xe2-base-utils").ReactiveVariable<[number, number, number, number]>;
        dashLength: number;
        dashPattern: number;
        arcType: "GEODESIC" | "NONE" | "RHUMB";
        editing: boolean;
        pointEditing: boolean;
        depthTest: boolean;
    };
    get json(): JsonType;
    set json(value: JsonType);
    get geoJson(): JsonValue;
    set geoJson(value: JsonValue);
    get geoJsonStr(): string;
    set geoJsonStr(value: string);
    private _pickedEvent;
    get pickedEvent(): Event<[PickedInfo]>;
    private _flyToEvent;
    get flyToEvent(): Listener<[number | undefined]>;
    flyTo(duration?: number): void;
    private _finalPositionsSet;
    get finalPositionsSet(): [number, number, number][][] | undefined;
    get finalPositionsSetChanged(): Listener<[[number, number, number][][] | undefined, [number, number, number][][] | undefined]>;
    protected _smGeoLinePlotting: T;
    get maxPointsNum(): number | undefined;
    private _updateFinalPositionsProcessing;
    updateFinalPositions(): void;
    private _sPositionsEditing;
    get sPositionsEditing(): PositionsEditing;
    constructor(smGeoLinePlottingConstructor: new (points: SuperMap.Geometry.Point[]) => SuperMap.Geometry.GeoMultiLinePlotting, _maxPointsNum?: number | undefined, id?: string);
    private _updateFinalPositions;
    get propNames(): string[];
    static defaults: {
        positions: never[];
        viewerTagsEnums: [string, string][];
    };
    getProperties(language?: string): import("xbsj-xe2/dist-node/xe2-base-objects").Property[];
}
export declare namespace SMGeoMultiLinePlottingBase {
    const createDefaultProps: () => {
        execOnceFuncStr: string | undefined;
        updateFuncStr: string | undefined;
        toDestroyFuncStr: string | undefined;
        name: string;
        ref: string | undefined;
        devTags: import("xbsj-xe2/dist-node/xe2-base-utils").ReactiveVariable<string[] | undefined>;
        extras: import("xbsj-xe2/dist-node/xe2-base-utils").ReactiveVariable<JsonValue>;
        show: boolean;
        allowPicking: boolean;
        positions: import("xbsj-xe2/dist-node/xe2-base-utils").ReactiveVariable<[number, number, number][] | undefined>;
        width: number;
        ground: boolean;
        color: import("xbsj-xe2/dist-node/xe2-base-utils").ReactiveVariable<[number, number, number, number]>;
        hasDash: boolean;
        gapColor: import("xbsj-xe2/dist-node/xe2-base-utils").ReactiveVariable<[number, number, number, number]>;
        dashLength: number;
        dashPattern: number;
        arcType: "GEODESIC" | "NONE" | "RHUMB";
        editing: boolean;
        pointEditing: boolean;
        depthTest: boolean;
    };
    const propNames: string[];
}
export interface SMGeoMultiLinePlottingBase<T extends SuperMap.Geometry.GeoMultiLinePlotting> extends ReactivePropsToNativePropsAndChanged<ReturnType<typeof SMGeoMultiLinePlottingBase.createDefaultProps>> {
}
declare type JsonType = PartialWithUndefinedReactivePropsToNativeProps<ReturnType<typeof SMGeoMultiLinePlottingBase.createDefaultProps> & {
    type: string;
}>;
export {};
