import { SMGeoLinePlottingBase } from "../SMGeoLinePlottingBase";
export declare class SMGeoArc extends SMGeoLinePlottingBase<SuperMap.Geometry.GeoArc> {
    static readonly type: string;
    get typeName(): string;
    constructor(id?: string);
}
export declare class SMGeoBezierCurve2 extends SMGeoLinePlottingBase<SuperMap.Geometry.GeoBezierCurve2> {
    static readonly type: string;
    get typeName(): string;
    constructor(id?: string);
}
export declare class SMGeoBezierCurve3 extends SMGeoLinePlottingBase<SuperMap.Geometry.GeoBezierCurve3> {
    static readonly type: string;
    get typeName(): string;
    constructor(id?: string);
}
export declare class SMGeoBezierCurveN extends SMGeoLinePlottingBase<SuperMap.Geometry.GeoBezierCurveN> {
    static readonly type: string;
    get typeName(): string;
    constructor(id?: string);
}
export declare class SMGeoCardinalCurve extends SMGeoLinePlottingBase<SuperMap.Geometry.GeoCardinalCurve> {
    static readonly type: string;
    get typeName(): string;
    constructor(id?: string);
}
export declare class SMGeoFreeline extends SMGeoLinePlottingBase<SuperMap.Geometry.GeoFreeline> {
    static readonly type: string;
    get typeName(): string;
    constructor(id?: string);
}
export declare class SMGeoPolyline extends SMGeoLinePlottingBase<SuperMap.Geometry.GeoPolyline> {
    static readonly type: string;
    get typeName(): string;
    constructor(id?: string);
}
