import { ESSceneObject, PickedInfo, PositionsEditing } from "xbsj-xe2/dist-node/xe2-base-objects";
import { Event, JsonValue, Listener, PartialWithUndefinedReactivePropsToNativeProps, ReactivePropsToNativePropsAndChanged } from "xbsj-xe2/dist-node/xe2-base-utils";
export declare abstract class SMGeoLinePlottingBase<T extends SuperMap.Geometry.GeoLinePlotting> extends ESSceneObject {
    private _maxPointsNum?;
    get defaultProps(): {
        execOnceFuncStr: string | undefined;
        updateFuncStr: string | undefined;
        toDestroyFuncStr: string | undefined;
        name: string;
        ref: string | undefined;
        devTags: import("xbsj-xe2/dist-node/xe2-base-utils").ReactiveVariable<string[] | undefined>;
        extras: import("xbsj-xe2/dist-node/xe2-base-utils").ReactiveVariable<JsonValue>;
        show: boolean;
        allowPicking: boolean;
        positions: import("xbsj-xe2/dist-node/xe2-base-utils").ReactiveVariable<[number, number, number][] | undefined>;
        width: number;
        ground: boolean;
        color: import("xbsj-xe2/dist-node/xe2-base-utils").ReactiveVariable<[number, number, number, number]>;
        hasDash: boolean;
        gapColor: import("xbsj-xe2/dist-node/xe2-base-utils").ReactiveVariable<[number, number, number, number]>;
        dashLength: number;
        dashPattern: number;
        hasArrow: boolean;
        arcType: "GEODESIC" | "NONE" | "RHUMB";
        editing: boolean;
        pointEditing: boolean;
        depthTest: boolean;
        viewDistanceRange: import("xbsj-xe2/dist-node/xe2-base-utils").ReactiveVariable<[number, number, number, number] | undefined>;
        viewDistanceDebug: boolean;
    };
    get json(): JsonType;
    set json(value: JsonType);
    get geoJson(): JsonValue;
    set geoJson(value: JsonValue);
    get geoJsonStr(): string;
    set geoJsonStr(value: string);
    private _pickedEvent;
    get pickedEvent(): Event<[PickedInfo]>;
    private _flyToEvent;
    get flyToEvent(): Listener<[number | undefined]>;
    flyTo(duration?: number): void;
    private _finalPositions;
    get finalPositions(): [number, number, number][] | undefined;
    get finalPositionsChanged(): Listener<[[number, number, number][] | undefined, [number, number, number][] | undefined]>;
    protected _smGeoLinePlotting: T;
    get maxPointsNum(): number | undefined;
    private _updateFinalPositionsProcessing;
    updateFinalPositions(): void;
    private _sPositionsEditing;
    get sPositionsEditing(): PositionsEditing;
    constructor(smGeoLinePlottingConstructor: new (points: SuperMap.Geometry.Point[]) => SuperMap.Geometry.GeoLinePlotting, _maxPointsNum?: number | undefined, id?: string);
    private _updateFinalPositions;
    get propNames(): string[];
    static defaults: {
        positions: never[];
        viewDistanceRange: [number, number, number, number];
        viewerTagsEnums: [string, string][];
    };
    getProperties(language?: string): import("xbsj-xe2/dist-node/xe2-base-objects").Property[];
}
export declare namespace SMGeoLinePlottingBase {
    const createDefaultProps: () => {
        execOnceFuncStr: string | undefined;
        updateFuncStr: string | undefined;
        toDestroyFuncStr: string | undefined;
        name: string;
        ref: string | undefined;
        devTags: import("xbsj-xe2/dist-node/xe2-base-utils").ReactiveVariable<string[] | undefined>;
        extras: import("xbsj-xe2/dist-node/xe2-base-utils").ReactiveVariable<JsonValue>;
        show: boolean;
        allowPicking: boolean;
        positions: import("xbsj-xe2/dist-node/xe2-base-utils").ReactiveVariable<[number, number, number][] | undefined>;
        width: number;
        ground: boolean;
        color: import("xbsj-xe2/dist-node/xe2-base-utils").ReactiveVariable<[number, number, number, number]>;
        hasDash: boolean;
        gapColor: import("xbsj-xe2/dist-node/xe2-base-utils").ReactiveVariable<[number, number, number, number]>;
        dashLength: number;
        dashPattern: number;
        hasArrow: boolean;
        arcType: "GEODESIC" | "NONE" | "RHUMB";
        editing: boolean;
        pointEditing: boolean;
        depthTest: boolean;
        viewDistanceRange: import("xbsj-xe2/dist-node/xe2-base-utils").ReactiveVariable<[number, number, number, number] | undefined>;
        viewDistanceDebug: boolean;
    };
    const propNames: string[];
}
export interface SMGeoLinePlottingBase<T extends SuperMap.Geometry.GeoLinePlotting> extends ReactivePropsToNativePropsAndChanged<ReturnType<typeof SMGeoLinePlottingBase.createDefaultProps>> {
}
declare type JsonType = PartialWithUndefinedReactivePropsToNativeProps<ReturnType<typeof SMGeoLinePlottingBase.createDefaultProps> & {
    type: string;
}>;
export {};
