import { SMGeoMultiLinePlottingBase } from "../SMGeoMultiLinePlottingBase";
import { SMGeoPlottingBase } from "../SMGeoPlottingBase";
export declare class SMGeoBezierCurveArrow extends SMGeoMultiLinePlottingBase<SuperMap.Geometry.GeoCardinalCurveArrow> {
    static readonly type: string;
    get typeName(): string;
    constructor(id?: string);
}
export declare class SMGeoCardinalCurveArrow extends SMGeoMultiLinePlottingBase<SuperMap.Geometry.GeoCardinalCurveArrow> {
    static readonly type: string;
    get typeName(): string;
    constructor(id?: string);
}
export declare class SMGeoCurveFlag extends SMGeoPlottingBase<SuperMap.Geometry.GeoCurveFlag> {
    static readonly type: string;
    get typeName(): string;
    constructor(id?: string);
    protected _updateFinalPositions(): void;
}
export declare class SMGeoDiagonalArrow extends SMGeoPlottingBase<SuperMap.Geometry.GeoDiagonalArrow> {
    static readonly type: string;
    get typeName(): string;
    constructor(id?: string);
}
export declare class SMGeoDoubleArrow extends SMGeoPlottingBase<SuperMap.Geometry.GeoDoubleArrow> {
    static readonly type: string;
    get typeName(): string;
    constructor(id?: string);
}
export declare class SMGeoDoveTailDiagonalArrow extends SMGeoPlottingBase<SuperMap.Geometry.GeoDoveTailDiagonalArrow> {
    static readonly type: string;
    get typeName(): string;
    constructor(id?: string);
}
export declare class SMGeoDoveTailStraightArrow extends SMGeoPlottingBase<SuperMap.Geometry.GeoDoveTailStraightArrow> {
    static readonly type: string;
    get typeName(): string;
    constructor(id?: string);
}
export declare class SMGeoParallelSearch extends SMGeoMultiLinePlottingBase<SuperMap.Geometry.GeoParallelSearch> {
    static readonly type: string;
    get typeName(): string;
    constructor(id?: string);
}
export declare class SMGeoPolylineArrow extends SMGeoMultiLinePlottingBase<SuperMap.Geometry.GeoPolylineArrow> {
    static readonly type: string;
    get typeName(): string;
    constructor(id?: string);
}
export declare class SMGeoRectFlag extends SMGeoPlottingBase<SuperMap.Geometry.GeoRectFlag> {
    static readonly type: string;
    get typeName(): string;
    constructor(id?: string);
    protected _updateFinalPositions(): void;
}
export declare class SMGeoSectorSearch extends SMGeoMultiLinePlottingBase<SuperMap.Geometry.GeoSectorSearch> {
    static readonly type: string;
    get typeName(): string;
    constructor(id?: string);
}
export declare class SMGeoStraightArrow extends SMGeoPlottingBase<SuperMap.Geometry.GeoStraightArrow> {
    static readonly type: string;
    get typeName(): string;
    constructor(id?: string);
}
export declare class SMGeoTriangleFlag extends SMGeoPlottingBase<SuperMap.Geometry.GeoTriangleFlag> {
    static readonly type: string;
    get typeName(): string;
    constructor(id?: string);
    protected _updateFinalPositions(): void;
}
