import { MyProjectManager } from './MyProjectManager';

const myProjectManager = new MyProjectManager();
// @ts-ignore
window.g_myProjectManager = myProjectManager;

/**
 * 
 * @returns { MyProjectManager }
 */
export function getMyProjectManager() {
    // @ts-ignore
    return window.g_myProjectManager;
}
