import { ESObjectsManager } from 'esobjs-xe2-plugin/dist-node/esobjs-xe2-plugin-main';
import { CzmBoxEntity } from 'xbsj-xe2/dist-node/xe2-cesium-objects';

export class MyProjectManager extends ESObjectsManager {

    constructor() {
        super();

        this.json = {
            "asset": {
                "version": "0.1.0",
                "type": "ESObjectsManager",
                "createdTime": "2022-06-17T05:54:41.744Z",
                "modifiedTime": "2023-12-14T08:28:04.290Z",
                "name": "基础场景"
            },
            "viewers": [],
            "viewCollection": [],
            "lastView": null,
            "sceneTree": {
                "root": {
                    "children": [
                        {
                            "name": "Cesium基础场景",
                            "children": [
                                {
                                    "name": "谷歌影像",
                                    "sceneObj": {
                                        "id": "e211f45f-bed9-4898-8ae4-8f4ba7cba447",
                                        "type": "ESImageryLayer",
                                        "url": "http://0414.gggis.com/maps/vt?lyrs=s&x={x}&y={y}&z={z}",
                                        "name": "谷歌影像"
                                    },
                                    "children": []
                                }
                            ]
                        }
                    ]
                }
            }
        };

        const box = this._box;
        if (box) {
            box.position = [116.39, 39.9, 100];
            box.dimensions = [2000000, 2000000, 2000000];
            box.fill = false;
            box.outline = true;
            box.outlineColor = [1, 1, 0, 1];
        }
    }
}