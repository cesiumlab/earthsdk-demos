import { createApp } from 'vue';
import App from './App.vue';
import './css/index.css';
import { ESUeViewer } from "earthsdk3-ue";
import { ESObjectsManager } from 'earthsdk3';
const objm = new ESObjectsManager(ESUeViewer);
//@ts-ignore
window.g_objm = objm;

createApp(App, { objm }).mount('#app');
