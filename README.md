# 说明

## app1-es-object-editor 
场景对象编辑器  
主要用处：  
1. 可以在此APP中配置好场景，然后输出JSON，在具体项目中使用；
2. 可以测试新创建的场景对象，因为属性可以很容易的修改，修改后所见即所得；
3. vue-xe2-plugin比较特殊，源码也在本APP中，需要通过yarn publishVue发布到misc/node_modules/vue-xe2-plugin中；

## app2-es-earthS-app
是一个三维可视化项目，深度集成和使用XE2

## app4-earth-ui
CesiumLab的三维可视UI，新版earth-ui.

## esobjs-xe2-plugin
XE2的ES对象插件，ES类对象是在cesium和UE中都有实现效果的对象。这个插件也是XE2的主要组成部分。

## esobjs-xe2-plugin-assets
在构建 esobjs-xe2-plugin 插件的时候会使用一些静态资源，这个库存放这些静态资源。

## misc
一些app1中可用的数据

## smplotting-xe2-plugin
XE2的标绘插件，内部使用超图的二维矢量库的算法来绘制图形

## template-vite-vue3-xe2-ts
项目模版，使用vue3、xe2、typescript来创建整个三维项目，构建工具使用vite

## template-vue2-xe2-js
项目模版，使用vue2、xe2、javascript来创建整个三维项目,构建工具使用webpack

## template-vue2-xe2-ts
项目模版，使用vue2、xe2、typescript来创建整个三维项目,构建工具使用webpack

## template-vue3-xe2-js
项目模版，使用vue3、xe2、javascript来创建整个三维项目,构建工具使用webpack

## template-vue3-xe2-ts
项目模版，使用vue3、xe2、typescript来创建整个三维项目,构建工具使用webpack

## template-xe2-plugin
这是创建XE插件的模板，内有自定义对象的范例，可自行集成自定义对象到XE2中。

## test-xxx
这是一个测试项目，用来测试一些功能,请勿用于实际生产中！！！

## vue-xe2-plugin
这是一个vue3+ts的插件，只能在vue3+ts项目中使用，主要作用是在vue中更加方便丝滑的使用XE2.


