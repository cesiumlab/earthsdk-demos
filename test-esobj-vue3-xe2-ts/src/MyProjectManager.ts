import { ESObjectsManager } from 'esobjs-xe2-plugin/dist-node/esobjs-xe2-plugin-main';
import { water } from './water';
export class MyProjectManager extends ESObjectsManager {
    constructor() {
        super();

        const json = {
            "asset": {
                "type": "ESObjectsManager",
            },
            "viewers": [],
            "viewCollection": [],
            "lastView": null,
            "sceneTree": {
                "root": {
                    "name": "ROOT",
                    "children": [
                        {
                            "name": "基础场景",
                            "children": [
                                {
                                    "name": "离线影像",
                                    "sceneObj": {
                                        "name": "离线影像",
                                        "type": "ESImageryLayer",
                                        "url": "http://0414.gggis.com/maps/vt?lyrs=s&x={x}&y={y}&z={z}"
                                    }
                                },
                                {
                                    "name": "ESGltfModel_4408",
                                    "sceneObj": {
                                        "id": "44084161-9e9c-46f7-9a8c-406ab1f0ea66",
                                        "type": "ESGltfModel",
                                        "position": [
                                            116.38971463675509,
                                            39.90633304843587,
                                            0.000005881136678406031
                                        ],
                                        "name": "ESGltfModel_4408",
                                        "allowPicking": true
                                    },
                                    "children": []
                                },
                                {
                                    "name": "水面",
                                    "sceneObj": water
                                }
                            ]
                        }
                    ]
                }
            }
        };

        // @ts-ignore
        this.json = json;
    }
}
