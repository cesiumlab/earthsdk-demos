import { ESGeoDiv, ESHuman, ESSceneObject } from 'esobjs-xe2-plugin/dist-node/esobjs-xe2-plugin-main';
import { react, reactArray, track, bind } from "xbsj-xe2/dist-node/xe2-base-utils";

//利用现有对象组合成的对象 ，继承自谁可以自行选择，ESSceneObject是基础类
export class ESHumanDiv extends ESSceneObject {
    static type = this.register('ESHumanDiv', this, { chsName: '人员提示', tags: ['ESObjects'], description: "人员对象+提示对象" });
    get typeName() { return 'ESHumanDiv'; }
    get defaultProps() { return ESHumanDiv.createDefaultProps(); }
    get json() { return this._innerGetJson(); }
    set json(value) { this._innerSetJson(value); }

    // 显隐控制
    private _show = this.disposeVar(react(true));
    get show() { return this._show.value; }
    set show(value) { this._show.value = value }
    get showChanged() { return this._show.changed; }

    // 位置
    private _location = this.disposeVar(reactArray<[number, number, number]>([116.39, 39.9, 0]));
    get location() { return this._location.value; }
    set location(value) { this._location.value = value }
    get locationChanged() { return this._location.changed; }

    constructor(id?: string) {
        super(id);
        {
            const human = this.dv(new ESHuman());
            const geoDiv = this.dv(new ESGeoDiv());
            geoDiv.innerHTML = `<div style="width: 70px; height: 20px; background: white; color: #000;">人员测试</div>`
            //重要！！！
            this.dispose(this.components.disposableAdd(human));
            this.dispose(this.components.disposableAdd(geoDiv));

            //属性同步
            this.d(track([human, 'show'], [this, 'show']));//human对象show受this.show影响，
            this.d(bind([geoDiv, 'show'], [this, 'show']));//互相绑定，互相影响


            //另一种属性同步
            const updateLocation = () => {
                const location = this.location;
                human.position = [...location];
                geoDiv.position = [location[0], location[1], location[2] + 3];
            };
            updateLocation();//初始化
            this.dispose(this.locationChanged.don(updateLocation));
        }
    }
}


