import { CzmObject, CzmViewer } from "xbsj-xe2/dist-node/xe2-cesium-objects";
import { ESDePolygon } from "./ESDePolygon";
import * as Cesium from 'cesium';
//ESDePolygon对象的实现类
export class CzmESDePolygon extends CzmObject<ESDePolygon> {
    //重要！！！
    static readonly type = this.register(ESDePolygon.type, this);

    constructor(sceneObject: ESDePolygon, czmViewer: CzmViewer) {
        super(sceneObject, czmViewer);
        const viewer = czmViewer.viewer;
        if (!viewer) {
            console.warn(`viewer is undefined!`);
            return;
        }
        const { entities } = viewer;

        const polygon = entities.add({
            polygon: {
                hierarchy: new Cesium.PolygonHierarchy(
                    Cesium.Cartesian3.fromDegreesArray([-107.0, 27.0, -107.0, 22.0, -102.0, 23.0, -97.0, 21.0, -97.0, 25.0])
                ),
                outline: true,
                outlineColor: Cesium.Color.WHITE,
                outlineWidth: 4,
            },
        });

        {//显影控制
            const updateShow = () => { polygon.show = sceneObject.show; }
            updateShow();
            this.d(sceneObject.showChanged.don(updateShow))
        }

        //对象销毁时移除对象
        this.d(() => { polygon && entities.remove(polygon); })
    }
}
