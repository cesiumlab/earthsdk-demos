import { ESSceneObject } from 'esobjs-xe2-plugin/dist-node/esobjs-xe2-plugin-main';
import { react } from "xbsj-xe2/dist-node/xe2-base-utils";
//ESDePolygon类的定义，继承自谁可以自行选择，ESSceneObject是基础类
export class ESDePolygon extends ESSceneObject {
    //tags: ['ESObjects', '_ES_Impl_Cesium', '_ES_Impl_UE']; _ES_Impl_Cesium代表有Cesium实现
    static type = this.register('ESDePolygon', this, { chsName: 'entities多边形', tags: ['ESObjects', '_ES_Impl_Cesium'], description: "entities多边形" });
    get typeName() { return 'ESDePolygon'; }
    get defaultProps() { return ESDePolygon.createDefaultProps(); }
    get json() { return this._innerGetJson(); }
    set json(value) { this._innerSetJson(value); }

    // 显隐控制
    private _show = this.disposeVar(react(true));
    get show() { return this._show.value; }
    set show(value) { this._show.value = value }
    get showChanged() { return this._show.changed; }

    constructor(id?: string) {
        super(id);
    }
}

// export namespace ESDePolygon {
//     export const createDefaultProps = () => ({
//         ...ESSceneObject.createDefaultProps(),
//         mode: "cylinder",
//         radius: 10,
//     });
// }
// extendClassProps(ESAlarm.prototype, ESAlarm.createDefaultProps);
// export interface ESAlarm extends ReactivePropsToNativePropsAndChanged<ReturnType<typeof ESAlarm.createDefaultProps>> { }
// type JsonType = PartialWithUndefinedReactivePropsToNativeProps<ReturnType<typeof ESAlarm.createDefaultProps> & { type: string }>;



