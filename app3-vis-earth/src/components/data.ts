
export const searchData: any = [
    {
        "parent": [],
        "address": "东城区",
        "distance": [],
        "pname": "北京市",
        "importance": [],
        "biz_ext": {
            "cost": [],
            "rating": []
        },
        "biz_type": [],
        "cityname": "北京市",
        "type": "地名地址信息;普通地名;省级地名",
        "photos": [
            {
                "title": "圆明园遗址公园",
                "url": "http://store.is.autonavi.com/showpic/25fb9cbadca2a322c4fb8d80e8b51488"
            },
            {
                "title": "颐和园",
                "url": "http://store.is.autonavi.com/showpic/26e0d532a775aa39c4b03bdc6de83be4"
            },
            {
                "title": "八达岭长城",
                "url": "http://store.is.autonavi.com/showpic/cbfe5cef98a9487a8e9ddd14152591e7"
            }
        ],
        "typecode": "190102",
        "shopinfo": "2",
        "poiweight": [],
        "childtype": [],
        "adname": "东城区",
        "name": "北京市",
        "location": "116.407387,39.904179",
        "tel": [],
        "shopid": [],
        "id": "B000ABCR66"
    },
    {
        "parent": [],
        "address": "西直门北大街北滨河路1号",
        "distance": [],
        "pname": "北京市",
        "importance": [],
        "biz_ext": {
            "cost": [],
            "rating": []
        },
        "biz_type": [],
        "cityname": "北京市",
        "type": "交通设施服务;火车站;火车站",
        "photos": [
            {
                "title": [],
                "url": "http://store.is.autonavi.com/showpic/7090a78338fda603dd26d069920fa992"
            },
            {
                "title": [],
                "url": "http://store.is.autonavi.com/showpic/b71fd3fbc082badb9debe289c69eac52"
            },
            {
                "title": [],
                "url": "https://aos-comment.amap.com/B000A833V8/comment/114da6e4bc758a8c3653529beacc37f4_2048_2048_80.jpg"
            }
        ],
        "typecode": "150200",
        "shopinfo": "2",
        "poiweight": [],
        "childtype": [],
        "adname": "西城区",
        "name": "北京北站",
        "location": "116.353464,39.944699",
        "tel": "010-51866852",
        "shopid": [],
        "id": "B000A833V8"
    },
    {
        "parent": [],
        "address": "中关村南大街甲18号(魏公村地铁站D西南口步行480米)",
        "distance": [],
        "pname": "北京市",
        "importance": [],
        "biz_ext": {
            "cost": [],
            "rating": []
        },
        "biz_type": [],
        "cityname": "北京市",
        "type": "商务住宅;楼宇;商务写字楼",
        "photos": [
            {
                "title": [],
                "url": "http://store.is.autonavi.com/showpic/973bf8dfe22e505752e05c74c4f57730"
            },
            {
                "title": "交通图",
                "url": "http://store.is.autonavi.com/showpic/61478ce15f63a171f45d66f2db676757"
            },
            {
                "title": "交通图",
                "url": "http://store.is.autonavi.com/showpic/34df82b54f2ce11f2f8877cdebbc0588"
            }
        ],
        "typecode": "120201",
        "shopinfo": "0",
        "poiweight": [],
        "childtype": [],
        "adname": "海淀区",
        "name": "北京·国际",
        "location": "116.324812,39.953426",
        "tel": [],
        "shopid": [],
        "id": "B000A81IED"
    },
    {
        "parent": [],
        "address": "颐和园路5号",
        "distance": [],
        "pname": "北京市",
        "importance": [],
        "biz_ext": {
            "cost": [],
            "rating": []
        },
        "biz_type": [],
        "cityname": "北京市",
        "type": "科教文化服务;学校;高等院校",
        "photos": [
            {
                "title": [],
                "url": "http://store.is.autonavi.com/showpic/179e616f7547d931de33dec835224fa3"
            },
            {
                "title": [],
                "url": "http://store.is.autonavi.com/showpic/f741699c2d5ff06d1f20f371ee327914"
            },
            {
                "title": [],
                "url": "http://store.is.autonavi.com/showpic/5abc2ce7af6b83443cc6252203c33ec5"
            }
        ],
        "typecode": "141201",
        "shopinfo": "0",
        "poiweight": [],
        "childtype": [],
        "adname": "海淀区",
        "name": "北京大学",
        "location": "116.310918,39.992873",
        "tel": [],
        "shopid": [],
        "id": "B000A816R6"
    },
    {
        "parent": [],
        "address": "毛家湾胡同甲13号",
        "distance": [],
        "pname": "北京市",
        "importance": [],
        "biz_ext": {
            "cost": [],
            "rating": []
        },
        "biz_type": [],
        "cityname": "北京市",
        "type": "交通设施服务;火车站;火车站",
        "photos": [
            {
                "title": [],
                "url": "http://store.is.autonavi.com/showpic/3b3195adc4fd9a07c14fbf5d1d1b0230"
            },
            {
                "title": [],
                "url": "https://aos-comment.amap.com/B000A83C36/comment/FFFA891C_A77B_4F30_92CD_5DE5EB8BD243_L0_001_1500_2000_1731645158492_05358454.jpg"
            },
            {
                "title": [],
                "url": "https://aos-comment.amap.com/B000A83C36/comment/content_media_external_images_media_100022432_1736255988900_45688241.jpg"
            }
        ],
        "typecode": "150200",
        "shopinfo": "2",
        "poiweight": [],
        "childtype": [],
        "adname": "东城区",
        "name": "北京站",
        "location": "116.427354,39.902830",
        "tel": "010-51831812",
        "shopid": [],
        "id": "B000A83C36"
    },
    {
        "parent": [],
        "address": "东单大华路1号",
        "distance": [],
        "pname": "北京市",
        "importance": [],
        "biz_ext": {
            "cost": [],
            "rating": []
        },
        "biz_type": [],
        "cityname": "北京市",
        "type": "医疗保健服务;综合医院;三级甲等医院",
        "photos": [
            {
                "title": [],
                "url": "http://store.is.autonavi.com/showpic/7d035c34bc70930def56dd6526ece43a"
            },
            {
                "title": [],
                "url": "http://store.is.autonavi.com/showpic/2554022f7a28554296879ffa4d456b34"
            },
            {
                "title": [],
                "url": "http://store.is.autonavi.com/showpic/59d90610564fca919371f56b44748037"
            }
        ],
        "typecode": "090101",
        "shopinfo": "2",
        "poiweight": [],
        "childtype": [],
        "adname": "东城区",
        "name": "北京医院",
        "location": "116.415057,39.903772",
        "tel": "010-85132114;010-85132266",
        "shopid": [],
        "id": "B000A52E0B"
    },
    {
        "parent": [],
        "address": "东长安街33号",
        "distance": [],
        "pname": "北京市",
        "importance": [],
        "biz_ext": {
            "cost": [],
            "star": "5",
            "opentime2": [],
            "rating": "4.4",
            "lowest_price": [],
            "hotel_ordering": "1",
            "open_time": [],
            "meal_ordering": "0"
        },
        "biz_type": "hotel",
        "cityname": "北京市",
        "type": "住宿服务;宾馆酒店;五星级宾馆|餐饮服务;餐饮相关场所;餐饮相关",
        "photos": [
            {
                "provider": [],
                "title": "外观（夜景）",
                "url": "http://store.is.autonavi.com/showpic/8454488c7b13e24f8e79a35dc8116814"
            },
            {
                "provider": [],
                "title": "外观",
                "url": "http://store.is.autonavi.com/showpic/9747ed9b30953f1a3dbe2cd1f6bda5ac"
            },
            {
                "provider": [],
                "title": "客房",
                "url": "http://store.is.autonavi.com/showpic/1e1b362d6f9f72cb8611084a1c15cb8d"
            }
        ],
        "typecode": "100102|050000",
        "shopinfo": "1",
        "poiweight": [],
        "childtype": [],
        "adname": "东城区",
        "name": "北京饭店",
        "location": "116.409385,39.908798",
        "tel": "010-65137766",
        "shopid": [],
        "id": "B000A10FBB"
    },
    {
        "parent": [],
        "address": "2号线",
        "distance": [],
        "pname": "北京市",
        "importance": [],
        "biz_ext": {
            "cost": [],
            "rating": []
        },
        "biz_type": [],
        "cityname": "北京市",
        "type": "交通设施服务;地铁站;地铁站",
        "photos": [
            {
                "title": [],
                "url": "https://aos-comment.amap.com/BV10006813/comment/a884fc09e45a3c56224d1d79b697fce4_2048_2048_80.jpg"
            },
            {
                "title": [],
                "url": "https://aos-comment.amap.com/BV10006813/comment/178c7292e3fa9a65b4557c2ebe698365_2048_2048_80.jpg"
            },
            {
                "title": [],
                "url": "https://aos-comment.amap.com/BV10006813/comment/6797dea290a5f0ec891a6014255d81e6_2048_2048_80.jpg"
            }
        ],
        "typecode": "150500",
        "shopinfo": "2",
        "poiweight": [],
        "childtype": [],
        "adname": "东城区",
        "name": "北京站(地铁站)",
        "location": "116.427287,39.904983",
        "tel": [],
        "shopid": [],
        "id": "BV10006813"
    },
    {
        "parent": [],
        "address": "学院路30号",
        "distance": [],
        "pname": "北京市",
        "importance": [],
        "biz_ext": {
            "cost": [],
            "rating": []
        },
        "biz_type": [],
        "cityname": "北京市",
        "type": "科教文化服务;学校;高等院校",
        "photos": [
            {
                "title": [],
                "url": "http://store.is.autonavi.com/showpic/2d326224f00d3042aa791701d5edf707"
            },
            {
                "title": "内景图",
                "url": "http://store.is.autonavi.com/showpic/0e0c535bd917b8314801cc92ff24a99d"
            },
            {
                "title": "外景图",
                "url": "http://store.is.autonavi.com/showpic/e8a720056c102b7296dfccb880007932"
            }
        ],
        "typecode": "141201",
        "shopinfo": "0",
        "poiweight": [],
        "childtype": [],
        "adname": "海淀区",
        "name": "北京科技大学",
        "location": "116.359265,39.990384",
        "tel": "010-62332312",
        "shopid": [],
        "id": "B000A7I3OJ"
    },
    {
        "parent": [],
        "address": "车公庄大街6号(车公庄西地铁站C东南口步行220米)",
        "distance": [],
        "pname": "北京市",
        "importance": [],
        "biz_ext": {
            "cost": [],
            "rating": []
        },
        "biz_type": [],
        "cityname": "北京市",
        "type": "科教文化服务;学校;高等院校",
        "photos": [
            {
                "title": [],
                "url": "http://store.is.autonavi.com/showpic/91fcca6048752256ca2e1447edc58d84"
            },
            {
                "title": [],
                "url": "http://store.is.autonavi.com/showpic/414e4a302becfc698d227cb4ac1ba921"
            },
            {
                "title": [],
                "url": "http://store.is.autonavi.com/showpic/7362a23665a58060dbc926753df56f23"
            }
        ],
        "typecode": "141201",
        "shopinfo": "0",
        "poiweight": [],
        "childtype": [],
        "adname": "西城区",
        "name": "北京行政学院",
        "location": "116.347155,39.930935",
        "tel": "010-68007014;010-68007200",
        "shopid": [],
        "id": "B000A6C3A5"
    },
    {
        "parent": [],
        "address": "盔甲厂胡同6号(北京站地铁站C东南出口步行490米)",
        "distance": [],
        "pname": "北京市",
        "importance": [],
        "biz_ext": {
            "cost": [],
            "star": [],
            "opentime2": [],
            "rating": "4.7",
            "lowest_price": [],
            "hotel_ordering": "1",
            "open_time": []
        },
        "biz_type": "hotel",
        "cityname": "北京市",
        "type": "住宿服务;宾馆酒店;经济型连锁酒店",
        "photos": [
            {
                "provider": [],
                "title": "Logo",
                "url": "http://store.is.autonavi.com/showpic/23ce160c3a67b79cebd1ef2f00517fac"
            },
            {
                "provider": [],
                "title": [],
                "url": "http://store.is.autonavi.com/showpic/087d3ab585cee86317021bd6ac4d08e2"
            },
            {
                "provider": [],
                "title": "安逸商务三人间",
                "url": "http://store.is.autonavi.com/showpic/84d48db3f45a7a6c58e62c53d9fd2b6c"
            }
        ],
        "typecode": "100105",
        "shopinfo": "0",
        "poiweight": [],
        "childtype": [],
        "adname": "东城区",
        "name": "北京中安宾馆(建国门北京站店)",
        "location": "116.432092,39.903075",
        "tel": "010-65221122;15801341927",
        "shopid": [],
        "id": "B0FFFRHMQ9"
    },
    {
        "parent": "B0FFGR9PCE",
        "address": "北京城区靛厂路3号楼",
        "distance": [],
        "pname": "北京市",
        "importance": [],
        "biz_ext": {
            "cost": "562.00",
            "opentime2": "【正常】全年10:00-14:00；17:00-21:00",
            "rating": "4.7",
            "open_time": "10:00-14:00 17:00-21:00",
            "meal_ordering": "0"
        },
        "biz_type": "diner",
        "cityname": "北京市",
        "type": "餐饮服务;中餐厅;综合酒楼",
        "photos": [
            {
                "title": [],
                "url": "http://store.is.autonavi.com/showpic/e336846cf3b3e6ea80fcdf695bd68377"
            },
            {
                "title": [],
                "url": "http://store.is.autonavi.com/showpic/c6431176bf01db3e2c11b2916347a096"
            },
            {
                "title": [],
                "url": "https://aos-comment.amap.com/B000AAASEM/comment/a2684de16d4c53f38af5b3c1542067aa_2048_2048_80.jpg"
            }
        ],
        "typecode": "050101",
        "shopinfo": "1",
        "poiweight": [],
        "childtype": "324",
        "adname": "丰台区",
        "name": "北京宴(丰台总店)",
        "location": "116.285498,39.889887",
        "tel": "13661227904",
        "shopid": [],
        "id": "B000AAASEM"
    },
    {
        "parent": [],
        "address": "学院路38号",
        "distance": [],
        "pname": "北京市",
        "importance": [],
        "biz_ext": {
            "cost": [],
            "rating": []
        },
        "biz_type": [],
        "cityname": "北京市",
        "type": "科教文化服务;学校;高等院校",
        "photos": [
            {
                "title": [],
                "url": "http://store.is.autonavi.com/showpic/91cafaf144e7da1fd670fe9f7b2a38bf"
            },
            {
                "title": [],
                "url": "https://aos-comment.amap.com/B000A7CDQ4/comment/88095fd9c373ef3f544901fd82eb1907_2048_2048_80.jpg"
            },
            {
                "title": [],
                "url": "https://aos-comment.amap.com/B000A7CDQ4/comment/f53e54e8b045a10312e0ef18d968b2e7_2048_2048_80.jpg"
            }
        ],
        "typecode": "141201",
        "shopinfo": "0",
        "poiweight": [],
        "childtype": [],
        "adname": "海淀区",
        "name": "北京大学(学院路校区)",
        "location": "116.357828,39.984810",
        "tel": "010-62091114;010-82802191",
        "shopid": [],
        "id": "B000A7CDQ4"
    },
    {
        "parent": [],
        "address": "北三环东路2号",
        "distance": [],
        "pname": "北京市",
        "importance": [],
        "biz_ext": {
            "cost": [],
            "star": [],
            "opentime2": [],
            "rating": "4.7",
            "lowest_price": [],
            "hotel_ordering": "1",
            "open_time": []
        },
        "biz_type": "hotel",
        "cityname": "北京市",
        "type": "住宿服务;宾馆酒店;宾馆酒店",
        "photos": [
            {
                "provider": [],
                "title": "客房",
                "url": "http://store.is.autonavi.com/showpic/a0568b3ad7666b50af772adbf1e1d725"
            },
            {
                "provider": [],
                "title": "客房",
                "url": "http://store.is.autonavi.com/showpic/4833e9a6be11bf3d3f41897b06850d2e"
            },
            {
                "provider": [],
                "title": "餐饮",
                "url": "http://store.is.autonavi.com/showpic/b2fd83e756ce53e25f53d6f9f56a9504"
            }
        ],
        "typecode": "100100",
        "shopinfo": "0",
        "poiweight": [],
        "childtype": [],
        "adname": "朝阳区",
        "name": "北京维景国际大酒店(中旅大厦店)",
        "location": "116.451465,39.958587",
        "tel": "010-64622288",
        "shopid": [],
        "id": "B0FFF5A9C5"
    },
    {
        "parent": [],
        "address": "西黄城根北街甲2号(近北大医院)",
        "distance": [],
        "pname": "北京市",
        "importance": [],
        "biz_ext": {
            "cost": [],
            "rating": []
        },
        "biz_type": [],
        "cityname": "北京市",
        "type": "科教文化服务;学校;中学",
        "photos": [
            {
                "title": [],
                "url": "http://store.is.autonavi.com/showpic/6e8a82840d9275b942df220b2ad13e74"
            },
            {
                "title": [],
                "url": "http://store.is.autonavi.com/showpic/feac44a933ee306b618757ce4965496b"
            },
            {
                "title": [],
                "url": "http://store.is.autonavi.com/showpic/682f30b6b8953447b4b7675255ea6dd0"
            }
        ],
        "typecode": "141202",
        "shopinfo": "0",
        "poiweight": [],
        "childtype": [],
        "adname": "西城区",
        "name": "北京市第四中学(高中部)",
        "location": "116.378173,39.931466",
        "tel": "010-66175566",
        "shopid": [],
        "id": "B000A8UTV5"
    },
    {
        "parent": [],
        "address": "建国门内大街9号",
        "distance": [],
        "pname": "北京市",
        "importance": [],
        "biz_ext": {
            "cost": [],
            "star": "5",
            "opentime2": [],
            "rating": "4.8",
            "lowest_price": [],
            "hotel_ordering": "1",
            "open_time": [],
            "meal_ordering": "0"
        },
        "biz_type": "hotel",
        "cityname": "北京市",
        "type": "住宿服务;宾馆酒店;五星级宾馆|餐饮服务;餐饮相关场所;餐饮相关",
        "photos": [
            {
                "provider": [],
                "title": [],
                "url": "http://store.is.autonavi.com/showpic/cc647881ccc8cfeea39a2b27e27975b9"
            },
            {
                "provider": [],
                "title": "客房",
                "url": "http://store.is.autonavi.com/showpic/3ead7205f0489231a914c1009ca97a44"
            },
            {
                "provider": [],
                "title": "客房",
                "url": "http://store.is.autonavi.com/showpic/bb1c16b33a6d29f7e8683c583b62fc01"
            }
        ],
        "typecode": "100102|050000",
        "shopinfo": "0",
        "poiweight": [],
        "childtype": [],
        "adname": "东城区",
        "name": "北京国际饭店",
        "location": "116.428434,39.909665",
        "tel": "010-65126688",
        "shopid": [],
        "id": "B000A82FYT"
    },
    {
        "parent": [],
        "address": "美术馆后街23号(南锣鼓巷地铁站F东北口步行480米)",
        "distance": [],
        "pname": "北京市",
        "importance": [],
        "biz_ext": {
            "cost": [],
            "rating": []
        },
        "biz_type": [],
        "cityname": "北京市",
        "type": "医疗保健服务;综合医院;三级甲等医院",
        "photos": [
            {
                "title": [],
                "url": "http://store.is.autonavi.com/showpic/3af8b5e59f6d2336e67d1a97cea772b8"
            },
            {
                "title": [],
                "url": "http://store.is.autonavi.com/showpic/8c9c6f39baab3cfed35b69d01028aada"
            },
            {
                "title": "其他",
                "url": "http://store.is.autonavi.com/showpic/e5d002eae1b14d4832b6b810348f8172"
            }
        ],
        "typecode": "090101",
        "shopinfo": "2",
        "poiweight": [],
        "childtype": [],
        "adname": "东城区",
        "name": "首都医科大学附属北京中医医院",
        "location": "116.407913,39.931876",
        "tel": "010-84712345",
        "shopid": [],
        "id": "B000A24092"
    },
    {
        "parent": [],
        "address": "莲花池东路118号",
        "distance": [],
        "pname": "北京市",
        "importance": [],
        "biz_ext": {
            "cost": [],
            "rating": []
        },
        "biz_type": [],
        "cityname": "北京市",
        "type": "交通设施服务;火车站;火车站",
        "photos": [
            {
                "title": [],
                "url": "http://store.is.autonavi.com/showpic/778d4f452762efe0e80ccf924cb6340c"
            },
            {
                "title": [],
                "url": "http://store.is.autonavi.com/showpic/71c814eb3f8bbf1a38b574ddf8806b6e"
            },
            {
                "title": [],
                "url": "https://aos-comment.amap.com/B000A83M61/comment/447C7CE3_84FC_47CC_B19E_95F4A0646048_L0_001_2047_2168_1728179435702_71226452.jpg"
            }
        ],
        "typecode": "150200",
        "shopinfo": "2",
        "poiweight": [],
        "childtype": [],
        "adname": "丰台区",
        "name": "北京西站",
        "location": "116.322033,39.894912",
        "tel": "010-51824233",
        "shopid": [],
        "id": "B000A83M61"
    },
    {
        "parent": [],
        "address": "西黄城根北街6号",
        "distance": [],
        "pname": "北京市",
        "importance": [],
        "biz_ext": {
            "cost": [],
            "rating": []
        },
        "biz_type": [],
        "cityname": "北京市",
        "type": "科教文化服务;学校;中学",
        "photos": [
            {
                "title": [],
                "url": "http://store.is.autonavi.com/showpic/0b40d41d7cee7578e0ac8c3be5f3fea3"
            },
            {
                "title": [],
                "url": "http://store.is.autonavi.com/showpic/21a4f6de1a8558866087bab730de9ae1"
            },
            {
                "title": [],
                "url": "http://store.is.autonavi.com/showpic/00c34bca6312829fd350cc10b6740b95"
            }
        ],
        "typecode": "141202",
        "shopinfo": "0",
        "poiweight": [],
        "childtype": [],
        "adname": "西城区",
        "name": "北京市第三十九中学",
        "location": "116.378097,39.926654",
        "tel": "010-66175798;010-66175807",
        "shopid": [],
        "id": "B000A2DAAB"
    },
    {
        "parent": [],
        "address": "王府井大街138号(地铁金鱼胡同)",
        "distance": [],
        "pname": "北京市",
        "importance": [],
        "biz_ext": {
            "cost": [],
            "rating": "4.8"
        },
        "biz_type": [],
        "cityname": "北京市",
        "type": "购物服务;商场;普通商场",
        "photos": [
            {
                "title": [],
                "url": "http://store.is.autonavi.com/showpic/e3868321399c84ae0c547f0050f70ba6"
            },
            {
                "title": [],
                "url": "http://store.is.autonavi.com/showpic/61683276a2f15c8382e6977da6442318"
            },
            {
                "title": [],
                "url": "https://aos-comment.amap.com/B000A842O2/comment/content_media_external_images_media_1000140011_ss__1736576277999_03297105.jpg"
            }
        ],
        "typecode": "060102",
        "shopinfo": "1",
        "poiweight": [],
        "childtype": [],
        "adname": "东城区",
        "name": "北京apm",
        "location": "116.411923,39.914339",
        "tel": "010-58176688;010-58176995",
        "shopid": [],
        "id": "B000A842O2"
    }
]
