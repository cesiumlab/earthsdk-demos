import { defineConfig } from 'vite'
import vue from '@vitejs/plugin-vue'
import cesium from 'vite-plugin-cesium';

// https://vite.dev/config/
export default defineConfig({
  base: './',
  plugins: [vue(), cesium()],
})
