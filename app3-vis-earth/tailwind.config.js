module.exports = {
  content: ["./index.html", "./src/**/*.{vue,js,ts}"],
  theme: {
    colors: {
      'bg_c': '#FFFF',
      'text_c': '#333333',
      'text_sz1': '12px',
      'text_sz2': '14px',
    },
    extend: {
      spacing: {
        "2px": "2px",
        "5px": "5px",
        "10px": "10px",
        "15px": "15px",
        "20px": "20px",
        '25px': '25px',
        '30px': '30px',
        "300px": "300px",
      },
    },
  },
  plugins: [],
}
