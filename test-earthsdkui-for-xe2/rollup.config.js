/* eslint-disable */
const name = 'earthsdkui-for-xe2'
const typescript = require('rollup-plugin-typescript2')
const vuePlugin = require('rollup-plugin-vue')
const autoprefixer = require('autoprefixer');
const postcss = require('rollup-plugin-postcss');
// 如果依赖模块中存在 es 模块，需要使用 @rollup/plugin-node-resolve 插件进行转换
const nodeResolve = require('@rollup/plugin-node-resolve')

const file = (type) => `lib/${name}.${type}.js`

module.exports = { // 这里将 file 方法 和 name 导出
    file,
    name
}

const overrides = {
    compilerOptions: { declaration: true }, // 是否创建 typescript 声明文件
    exclude: [ // 排除项
        'node_modules',
        'tools',
        'src/App.vue',
        'src/main.ts'
    ]
}

module.exports = {
    input: './packages/index.ts',
    output: {
        name: 'earthsdkui-for-xe2',
        file: file('umd'),
        format: 'umd' // 编译模式
    },
    plugins: [
        nodeResolve(),
        typescript({ tsconfigOverride: overrides }),
        vuePlugin(),
        postcss({
            plugins: [autoprefixer()],
            extract: 'style.css'
        })
    ],
    external: ['vue'] // 依赖模块
}

