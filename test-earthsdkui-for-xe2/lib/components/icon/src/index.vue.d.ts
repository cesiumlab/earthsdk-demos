import './font/iconfont.js';
import './index.css';
declare const _default: import("vue").DefineComponent<{
    name: {
        type: StringConstructor;
        required: true;
    };
    color: {
        type: StringConstructor;
        required: true;
    };
    size: {
        type: NumberConstructor;
        required: false;
        default: number;
    };
}, (_ctx: any, _cache: any) => import("vue").VNode<import("vue").RendererNode, import("vue").RendererElement, {
    [key: string]: any;
}>, unknown, {}, {}, import("vue").ComponentOptionsMixin, import("vue").ComponentOptionsMixin, {}, string, import("vue").VNodeProps & import("vue").AllowedComponentProps & import("vue").ComponentCustomProps, Readonly<import("vue").ExtractPropTypes<{
    name: {
        type: StringConstructor;
        required: true;
    };
    color: {
        type: StringConstructor;
        required: true;
    };
    size: {
        type: NumberConstructor;
        required: false;
        default: number;
    };
}>>, {
    size: number;
}, {}>;
export default _default;
