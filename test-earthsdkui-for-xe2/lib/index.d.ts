import { App } from 'vue';
export * from './module';
declare const _default: {
    install: (app: App<any>) => void;
};
export default _default;
