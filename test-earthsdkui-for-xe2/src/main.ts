import { createApp } from 'vue'
import App from './App.vue'

import 'earthsdkui-for-xe2/lib/style.css';
// import EarthSDKUI from 'earthsdkui-for-xe2';

const app = createApp(App)
// app.use(EarthSDKUI)
app.mount('#app')
