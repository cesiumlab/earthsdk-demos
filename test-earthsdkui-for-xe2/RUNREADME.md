# earthsdk-for-xe2

## Project setup
```
yarn install
```
### 使用rollup打包编写好的组件(需全局安装rollup)
```
rollup -c
```
### Compiles and hot-reloads for development
```
yarn serve
```

### Compiles and minifies for production
```
yarn build
```

### Customize configuration
See [Configuration Reference](https://cli.vuejs.org/config/).

