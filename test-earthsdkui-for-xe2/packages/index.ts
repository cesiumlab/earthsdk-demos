import { App } from 'vue'
export * from './module'
import components from './module'

// 通过Vue.use安装 Vue.js 插件。如果插件是一个对象，必须提供 install 方法。
//如果插件是一个函数，它会被作为 install 方法，install 方法调用时，会将 Vue 作为参数传入。
// 需要注意的是：通过Vue.use安装 Vue.js 插件，如果插件是一个对象，那么对象当Vue.use(插件)之后，
//插件的install方法会立即执行；如果插件是一个函数，当Vue.use(插件)之后，函数会被立即执行
// https://cn.vuejs.org/api/application.html#app-use

// 完整引入组件
const install = function (app: App) {
    components.forEach(component => {
        app.use(component as unknown as { install: () => any })
    })
}

export default { install }

