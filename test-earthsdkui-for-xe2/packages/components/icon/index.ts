import { App } from 'vue'
import ESIcon from './src/index.vue'
ESIcon.install = function (app: App) {
    //https://cn.vuejs.org/api/application.html#app-component
    //组件注册
    app.component(ESIcon.name, ESIcon)
    return app
}

export default ESIcon
