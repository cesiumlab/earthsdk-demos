## 安装

### yarn

```sh

yarn add earthsdkui-for-xe2 --save

```

### npm

```sh

npm i earthsdkui-for-xe2 --save

```

### cdn

#### unpkg

```html
<link
    rel="stylesheet"
    href="//unpkg.com/earthsdkui-for-xe2/lib/style.css"
/>
<script src="//unpkg.com/earthsdkui-for-xe2"></script>
```

#### jsdelivr

```html
<link
    rel="stylesheet"
    href="//cdn.jsdelivr.net/npm/earthsdkui-for-xe2/lib/style.css"
/>
<script src="//cdn.jsdelivr.net/npm/earthsdkui-for-xe2"></script>
```

## 使用

### 全局引入

在`main.js`中

```js
import { createApp } from 'vue';
import App from './App.vue';
// 完整引入组件库
import 'earthsdkui-for-xe2/lib/style.css';
import ESUIForXe2 from 'earthsdkui-for-xe2';

const app = createApp(App);
// 全局安装
app.use(ESUIForXe2);
app.mount('#app');
```

组件当中

```vue
<es-icon name="shouye" color="red"></es-icon>
```

### 按需引入

组件当中

```vue
<script setup>

import { ESIcon } from 'earthsdkui-for-xe2';

</script>

<template>
 
<es-icon name="shouye" color="red"></es-icon>

</template>

<style scoped>
</style>
```
