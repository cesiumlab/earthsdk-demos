import { createApp } from 'vue';
import App from './App.vue';

// @ts-ignore
window.g_app = app;

const app = createApp(App)
app.mount('#app');
