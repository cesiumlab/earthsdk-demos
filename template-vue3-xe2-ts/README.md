# template2-vue3-xe2

使用xe2创建一个cesium版地球，并且演示响应式属性如何使用

## 准备工作
推荐使用node 16版本，其他版本不保证没问题。  
推荐使用SSD硬盘存在项目源码，因涉及大量碎文件拷贝，SSD可以加速。

## Project setup
```
yarn
```

### Compiles and hot-reloads for development
```
yarn serve
```

### Compiles and minifies for production
```
yarn build
```

### 问题
*. 如果yarn执行以后没反应，有可能是资源下载过慢，可以使用淘宝金镜像，执行以下这一句就好。  
npm config set registry https://registry.npm.taobao.org  
