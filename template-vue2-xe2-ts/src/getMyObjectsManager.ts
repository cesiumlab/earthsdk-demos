import { MyObjectsManager } from './myObjectsManager';

const objm = new MyObjectsManager();
// eslint-disable-next-line @typescript-eslint/ban-ts-comment
// @ts-ignore
window.g_objm = objm;

/**
 * @returns {MyObjectsManager}
 */
export function getMyObjectsManager(): MyObjectsManager {
// eslint-disable-next-line @typescript-eslint/ban-ts-comment
// @ts-ignore
    return window.g_objm as MyObjectsManager;
}
