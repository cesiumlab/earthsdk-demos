import Vue from 'vue'
import App from './App.vue'
import { getMyObjectsManager } from './getMyObjectsManager'
import { MyObjectsManager } from './myObjectsManager'
Vue.config.productionTip = false
Vue.prototype.$objm = getMyObjectsManager() as MyObjectsManager

declare module 'vue/types/vue' {
  interface Vue {
    $objm: MyObjectsManager;
  }
}
new Vue({
  render: h => h(App),
}).$mount('#app')
