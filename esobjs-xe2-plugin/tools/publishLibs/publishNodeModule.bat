REM publish node module
call set SOURCE=%1
call set DEST=%2
call mkdirp %DEST%
call rimraf %DEST%\dist-node
call rimraf %DEST%\dist-web
call ncp %SOURCE%dist-node %DEST%\dist-node
call ncp %SOURCE%dist-web %DEST%\dist-web
call echo SUCCESS
