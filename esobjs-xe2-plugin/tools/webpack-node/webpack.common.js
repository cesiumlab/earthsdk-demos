const path = require('path');
const webpack = require('webpack')
const { CleanWebpackPlugin } = require('clean-webpack-plugin');
const CopyWebpackPlugin = require('copy-webpack-plugin');
const { defines, info } = require('../base/defines');
const ReplaceBundleStringPlugin = require('replace-bundle-webpack-plugin-webpack-5');
const { config } = require('../libNames');
const libNames = config.libNames;

module.exports = {
  entry: {
    ...libNames.reduce((o, c) => (o[`${c}`] = `./src/${c}/index.ts`, o), {}),
  },
  module: {
    rules: [
      {
        test: /\.ts$/,
        use: [{
          loader: "ts-loader",
          options: {
            configFile: path.resolve(__dirname, '../../tsconfig-node.json'),
          }
        }],
        exclude: /node_modules/
      },
    ]
  },
  plugins: [
    new webpack.BannerPlugin(`${info.name}(${info.version}-${info.commitId.slice(0, 8)}-${info.date}) 版权所有@${info.owner}`),
    new webpack.DefinePlugin(defines),
    new CleanWebpackPlugin(),
    new CopyWebpackPlugin([
      // {
      //   from: './src/**/xr-static/**/*',
      //   globOptions: { onlyFiles: false, },
      //   transformPath(targetPath, absolutePath) {
      //     // console.log('targetPath: ' + targetPath);
      //     var n = targetPath.indexOf('xr-static');
      //     return targetPath.substring(n);
      //   },
      // },
      {
        context: './src', // 使用context，拷贝时就不会带着src文件夹了！
        from: '**/*.d.ts',
      },
      {
        from: './xe2-assets',
        to: 'xe2-assets',
        toType: 'dir'
      },
      // {
      //   from: './static',
      //   to: 'static',
      //   toType: 'dir'
      // },
    ]),
    new ReplaceBundleStringPlugin([{
      partten: /__webpack_require__/g,
      replacement: function () {
        return '__webpack_require__node__';
      }
    }]),
  ],
  // 编译ts文件时，需要resolve.extensions，否则会提示找不到模块！
  resolve: {
    extensions: ['.ts', '.js', '.json'],
    alias: {
      '@': path.resolve('./src/'),
    },
  },
  externals: {
    ...libNames.reduce((o, c) => (o[`@/${c}`] = `../${c}`, o), {}),

    // @cesium
    cesium: 'cesium', // xr-utils中有cesium相关的函数
    // @xbsj-renderer
    'xbsj-renderer/dist-node/xr-base-utils': 'xbsj-renderer/dist-node/xr-base-utils',
    'xbsj-renderer/dist-node/xr-math': 'xbsj-renderer/dist-node/xr-math',
    'xbsj-renderer/dist-node/xr-utils': 'xbsj-renderer/dist-node/xr-utils',
    'xbsj-renderer/dist-node/xr-cesium': 'xbsj-renderer/dist-node/xr-cesium',
    // @xbsj-xe2
    'xbsj-xe2/dist-node/xe2': 'xbsj-xe2/dist-node/xe2',
    'xbsj-xe2/dist-node/xe2-base': 'xbsj-xe2/dist-node/xe2-base',
    'xbsj-xe2/dist-node/xe2-base-utils': 'xbsj-xe2/dist-node/xe2-base-utils',
    'xbsj-xe2/dist-node/xe2-utils': 'xbsj-xe2/dist-node/xe2-utils',
    'xbsj-xe2/dist-node/xe2-cesium': 'xbsj-xe2/dist-node/xe2-cesium',
    'xbsj-xe2/dist-node/xe2-mapbox': 'xbsj-xe2/dist-node/xe2-mapbox',
    'xbsj-xe2/dist-node/xe2-ue': 'xbsj-xe2/dist-node/xe2-ue',
    'xbsj-xe2/dist-node/utility-xe2-plugin': 'xbsj-xe2/dist-node/utility-xe2-plugin',
    'xbsj-xe2/dist-node/xe2-all-in-one': 'xbsj-xe2/dist-node/xe2-all-in-one',
    'xbsj-xe2/dist-node/xe2-base-objects': 'xbsj-xe2/dist-node/xe2-base-objects',
    'xbsj-xe2/dist-node/xe2-objects': 'xbsj-xe2/dist-node/xe2-objects',
    'xbsj-xe2/dist-node/xe2-cesium-objects': 'xbsj-xe2/dist-node/xe2-cesium-objects',
    'xbsj-xe2/dist-node/xe2-ue-objects': 'xbsj-xe2/dist-node/xe2-ue-objects',
    'xbsj-xe2/dist-node/xe2-openlayers': 'xbsj-xe2/dist-node/xe2-openlayers',
    'xbsj-xe2/dist-node/xe2-openlayers-objects': 'xbsj-xe2/dist-node/xe2-openlayers-objects',
    // plugins
    // 'smplotting-xe2-plugin/dist-node/smplotting-xe2-plugin': 'smplotting-xe2-plugin/dist-node/smplotting-xe2-plugin',
    // 'smplotting-xe2-plugin/dist-node/smplotting-xe2-plugin-main': 'smplotting-xe2-plugin/dist-node/smplotting-xe2-plugin-main',
    'esobjs-xe2-plugin/dist-node/esobjs-xe2-plugin': 'esobjs-xe2-plugin/dist-node/esobjs-xe2-plugin',
    'esobjs-xe2-plugin/dist-node/esobjs-xe2-plugin-main': 'esobjs-xe2-plugin/dist-node/esobjs-xe2-plugin-main',
  },
  target: 'node', // 默认是web 以后可以放到单独的选项中进行编译
};
