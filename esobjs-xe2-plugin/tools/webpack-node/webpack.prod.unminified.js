const path = require('path');
const merge = require('webpack-merge');
const common = require('./webpack.common.js');

/**
 * 主要用于调试
 */
const r = merge(common, {
  output: {
    filename: '[name]/index.js',
    path: path.resolve(__dirname, '../../dist-node-unminified'),
    libraryTarget: "commonjs2",
  },
  optimization: {
    minimize: false,
  },
  mode: "production"
});

// r.module.rules.unshift({
//   test: /\.ts$/,
//   loader: './tools/webpack-node/shader-minify-loader',
// });

module.exports = r;