const config = {
    namespace: 'XE2',
    moduleName: 'esobjs-xe2-plugin',
    libNames: [
        "esobjs-xe2-plugin-main",
        "esobjs-xe2-plugin",
    ]
};

module.exports = { config };