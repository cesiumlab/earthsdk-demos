import { registerScript } from 'xbsj-xe2/dist-node/xe2-utils';
registerScript();

import { copyright } from '../copyright';

//@ts-ignore
window.g_xe2_copyright_print = window.g_xe2_copyright_print ?? true;
//@ts-ignore
window.g_xe2_copyright_print && copyright.print();

export { copyright };
export * from './esobjs';
export * from './ESObjectsManager';
export * from './ESViewers';
