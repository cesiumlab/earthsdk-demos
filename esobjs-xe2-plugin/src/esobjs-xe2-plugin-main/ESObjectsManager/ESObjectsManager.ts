import { GroupPropTreeItem, LeafPropTreeItem, ProjectManager, PropTree, SceneTree, SceneTreeItem, SceneTreeJsonValue, TreeItemInsertFlag } from 'xbsj-xe2/dist-node/xe2';
import { SceneObject } from 'xbsj-xe2/dist-node/xe2-base-objects';
import { Destroyable, Event, JsonValue, react } from 'xbsj-xe2/dist-node/xe2-base-utils';
import { PropertyCompCallbackFuncParamsType } from 'xbsj-xe2/dist-node/xe2/ProjectManager/PropertyCompCallbackFuncParamsType';
import { ESCzmViewer } from '../ESViewers/ESCzmViewer';
import { ESUeViewer } from '../ESViewers/ESUeViewer';
import { ESViewer } from '../ESViewers/ESViewer';
import { ESCameraViewCollection } from '../esobjs';
import { ViewInfo } from '../esobjs/general/objs/ESCameraViewCollection/ViewWrapper';
import { ESPlyarAndPathTime } from "./ESPlyarAndPathTime";
import { PathAnimationManager } from './PathAnimationManager';
import { ESCzmViewerOptions, ESUEViewerOptions, SwitchToESCzmViewerOptions, SwitchToESUEViewerOptions, findOrCreateCesiumViewer, findOrCreateUEViewer, getCameraInfoIfSyncNeeded, getSyncPropsValue, handleCameraInfo, parseSwitchToCesiumViewerOptions, parseSwitchToUEViewerOptions } from './utils';
import { sleep } from 'xbsj-renderer/dist-node/xr-utils';
type ESViewerJsonValue = JsonValue
export type ESObjectsManagerJson = {
    asset: {
        type: string;
        version: string;
        createdTime: string;
        modifiedTime: string;
        name: string;
    };
    viewers: ESViewerJsonValue[],
    sceneTree: SceneTreeJsonValue;
    viewCollection: ViewInfo[];
    lastView?: ViewInfo;
};


/**
 * https://www.wolai.com/earthsdk/gRF9WpHdF9Cx1afsTHp9gt
 * https://c0yh9tnn0na.feishu.cn/docx/KRsPdxBcjoW1J1xC3fhcdPUgnIb
 */
export class ESObjectsManager extends Destroyable {
    private _projectManager = this.dv(new ProjectManager());
    get projectManager() { return this._projectManager; }

    private _cameraViewsManager = this._projectManager.createSceneObjectFromClass(ESCameraViewCollection) as ESCameraViewCollection;
    private _cvcDispose = this.d(() => this._cameraViewsManager && this._projectManager.destroySceneObject(this._cameraViewsManager));
    get cameraViewsManager() { return this._cameraViewsManager; }

    private _asset = {
        type: 'ESObjectsManager',
        version: '0.1.0',
        createdTime: '',
        modifiedTime: '',
        name: '未命名项目'
    }
    get json() {
        const createdTime = this._asset.createdTime || new Date().toISOString();
        const modifiedTime = new Date().toISOString();
        const name = this._asset.name || '未命名项目';
        const version = this._asset.version || '0.1.0';
        const asset = { type: 'ESObjectsManager', version, createdTime, modifiedTime, name }
        const sceneTree = this.sceneTree.json
        const viewers: ESViewerJsonValue[] = [...this._viewers].map(viewer => viewer.json)
        const viewCollection = (this._cameraViewsManager?.views) ?? []
        const lastView = this.activeViewer?.getCurrentCameraInfo()
        return { asset, viewers, sceneTree, viewCollection, lastView };
    }
    set json(value: ESObjectsManagerJson) {
        try {
            if (!value.asset) return;
            if (!value.asset.type || value.asset.type !== "ESObjectsManager") {
                console.warn("json装配失败! asset.type 不存在或者不是'ESObjectsManager'");
                return;
            }
            this._asset.createdTime = value.asset && value.asset.createdTime || new Date().toISOString();
            this._asset.modifiedTime = value.asset && value.asset.modifiedTime || '';
            this._asset.name = value.asset && value.asset.name || '未命名项目';
            this._asset.version = value.asset && value.asset.version || '0.1.0';
            value.sceneTree && (this.sceneTree.json = value.sceneTree);
            value.viewCollection && this._cameraViewsManager && (this._cameraViewsManager.views = value.viewCollection);
            if (!value.viewers || !Array.isArray(value.viewers)) {
                console.warn('viewers is not an array or does not exist !');
                return;
            };
            if (this._viewers.size === 0 || value.viewers.length === 0) {
                console.warn('viewers is empty !');
                return;
            };
            this._viewers.forEach(v => {
                //@ts-ignore
                const viewer = value.viewers.find(v2 => v2.id === v.id);
                if (viewer) {
                    //@ts-ignore
                    v.json = viewer;
                }
            })
        } catch (e) {
            console.error(`ESObjectsManager解析json数据时发生错误! error: ${e}`);
        }
    }
    get jsonStr() {
        return JSON.stringify(this.json, undefined, '    ');
    }
    set jsonStr(value: string) {
        try {
            this.json = value && JSON.parse(value) || { asset: { version: '0.1.0' } };
        } catch (error) {
            console.error(`ESObjectsManager解析json数据时发生错误! error: ${error}`);
        }
    }

    private _viewerCreatedEvent = this.dv(new Event<[ESViewer]>());
    get viewerCreatedEvent() { return this._viewerCreatedEvent; }

    private _viewers = new Set<ESViewer>();
    private _viewersDispose = this.d(() => {
        for (const viewer of this._viewers) { viewer.destroy(); }
        this._viewers.clear();
    });
    get viewers() { return this._viewers; }
    getViewers() { return this._viewers; }

    private _activeViewer = this.dv(react<ESViewer | undefined>(undefined));
    get activeViewer() { return this._activeViewer.value; }
    set activeViewer(value: ESViewer | undefined) { this._activeViewer.value = value; }
    get activeViewerChanged() { return this._activeViewer.changed; }

    createCesiumViewer(options: ESCzmViewerOptions): ESCzmViewer;
    createCesiumViewer(domid: string | HTMLElement, id?: string, czmOptions?: { [k: string]: any }): ESCzmViewer;
    createCesiumViewer(...args: any[]) {
        let options: ESCzmViewerOptions | undefined = undefined;
        if (typeof args[0] === 'object' && !(args[0] instanceof HTMLElement)) {
            options = args[0];
        } else if (typeof args[0] === 'string' || args[0] instanceof HTMLElement) {
            options = { domid: args[0], id: args[1] ?? undefined, czmOptions: args[2] ?? undefined };
        }
        if (!options) return;
        const viewer = new ESCzmViewer(this, options);
        this._viewers.add(viewer);
        //判断是否存在激活的视口
        if (!this.activeViewer) { this.activeViewer = viewer; }
        //触发viewerCreatedEvent
        const disposse = viewer.viewerChanged.don((e) => {
            if (!e) return;
            this._viewerCreatedEvent.emit(viewer);
            disposse()
        })
        return viewer;
    }

    createUEViewer(options: ESUEViewerOptions): ESUeViewer;
    createUEViewer(domid: string | HTMLElement, uri: string, app: string, id?: string): ESUeViewer;
    createUEViewer(...args: any[]) {
        let options: ESUEViewerOptions | undefined = undefined;
        if (typeof args[0] === 'object' && !(args[0] instanceof HTMLElement)) {
            options = args[0];
        } else if (typeof args[0] === 'string' || args[0] instanceof HTMLElement) {
            options = { domid: args[0], uri: args[1], app: args[2], id: args[3] ?? undefined };
        }
        if (!options) return;
        const viewer = new ESUeViewer(this, options);
        this._viewers.add(viewer);
        //判断是否存在激活的视口
        if (!this.activeViewer) { this.activeViewer = viewer; }
        //触发viewerCreatedEvent
        const disposse = this.d(viewer.viewerChanged.don((e) => {
            if (!e) return;
            this._viewerCreatedEvent.emit(viewer);
            disposse()
        }))
        return viewer;
    }

    destroyViewer(viewer: ESViewer) {
        if (viewer.isDestroyed()) return true;
        (this._activeViewer.value === viewer) && (this._activeViewer.value = undefined);
        this._viewers.delete(viewer);
        viewer.innerViewer.container = undefined;
        viewer.destroy();
        return viewer.isDestroyed()
    }


    /**
     * 内部同步视口相机信息，外部勿用
     */
    _lastCameraInfo: { position: [number, number, number]; rotation: [number, number, number]; } | undefined

    switchToCesiumViewer(options: SwitchToESCzmViewerOptions): ESCzmViewer;
    switchToCesiumViewer(domid: string | HTMLElement, viewSync?: boolean, attributeSync?: boolean, destroy?: boolean): ESCzmViewer;
    switchToCesiumViewer(...args: any[]) {
        const parsedOptions = parseSwitchToCesiumViewerOptions(...args);
        if (!parsedOptions) throw new Error('Invalid arguments provided to switchToCesiumViewer.');
        this._activeViewer.value && (!parsedOptions.destroy) && (this._activeViewer.value.getNavigationMode() !== 'Map') && this._activeViewer.value.changeToMap();

        if (!this._lastCameraInfo) {
            this._lastCameraInfo = getCameraInfoIfSyncNeeded(this.activeViewer, parsedOptions.viewSync ?? true);
        }
        const viewer = findOrCreateCesiumViewer(this, parsedOptions);
        handleCameraInfo(this, viewer);
        parsedOptions.destroy && this._activeViewer.value && this.destroyViewer(this._activeViewer.value);
        this._activeViewer.value = viewer;
        return viewer;
    }

    switchToUEViewer(options: SwitchToESUEViewerOptions): ESUeViewer;
    switchToUEViewer(domid: string | HTMLElement, uri: string, app: string, viewSync?: boolean, attributeSync?: boolean, destroy?: boolean): ESUeViewer;
    switchToUEViewer(...args: any[]) {
        const parsedOptions = parseSwitchToUEViewerOptions(...args);
        if (!parsedOptions) throw new Error('Invalid arguments provided to switchToUEViewer.');
        this._activeViewer.value && (!parsedOptions.destroy) && (this._activeViewer.value.getNavigationMode() !== 'Map') && this._activeViewer.value.changeToMap();

        if (!this._lastCameraInfo) {
            this._lastCameraInfo = getCameraInfoIfSyncNeeded(this.activeViewer, parsedOptions.viewSync ?? true);
        }
        const viewer = findOrCreateUEViewer(this, parsedOptions);
        handleCameraInfo(this, viewer);
        parsedOptions.destroy && this._activeViewer.value && this.destroyViewer(this._activeViewer.value);
        this._activeViewer.value = viewer;
        return viewer;
    }

    //开启调用每个Viewer的syncOtherViewer(Viewer)把当前的activedViewer传进去
    private _syncOtherViewersToActived = this.dv(react<boolean>(false));
    get syncOtherViewersToActived() { return this._syncOtherViewersToActived.value; }
    set syncOtherViewersToActived(value: boolean) { this._syncOtherViewersToActived.value = value; }
    get syncOtherViewersToActivedChanged() { return this._syncOtherViewersToActived.changed; }

    private _sceneTrees: string[] = [];
    get sceneTree() { return this._projectManager.defaultSceneTree }
    getSceneTree(name?: string) {
        if (name !== undefined) {
            if (!this._sceneTrees.includes(name)) {
                console.warn('SceneTree not found:', name);
                return undefined
            }
            return this._projectManager.getSceneTree(name);
        } else {
            return this._projectManager.defaultSceneTree;
        }
    }

    /**
     * 默认场景树jsonLoadingEvent
     */
    get jsonLoadingEvent() { return this._projectManager.defaultSceneTree.jsonLoadingEvent; }

    getSceneTrees() {
        const list = [];
        this._sceneTrees.forEach(name => {
            list.push(this._projectManager.getSceneTree(name));
        })
        list.push(this.sceneTree)
        return list
    }
    createSceneTree(options: { name: string, height?: number }): SceneTree | undefined;
    createSceneTree(name: string, height?: number): SceneTree | undefined;
    createSceneTree(...args: any[]) {
        let options: { name: string, height?: number } | undefined = undefined;
        if (typeof args[0] === 'object') {
            options = args[0];
        } else if (typeof args[0] === 'string') {
            options = { name: args[0], height: args[1] };
        }
        if (!options) return;
        const { name, height } = options
        //判断创建名称是否重复，或者和默认名称冲突
        if (this._sceneTrees.includes(name) || (name === 'default')) {
            console.warn('SceneTree already exists:', name);
            const treeName = (name === 'default') ? undefined : name;
            return this.getSceneTree(treeName)
        }
        const sceneTree = this._projectManager.getSceneTree(name, height);
        //记录创建的树名称
        this._sceneTrees.push(name);
        return sceneTree;
    }

    createSceneObject<T extends SceneObject = SceneObject>(sceneObjectType: string | (new (id?: string | undefined) => T), id?: string): T | undefined {
        return this._projectManager.createSceneObject(sceneObjectType, id);
    }
    createSceneObjectFromClass<T extends SceneObject>(sceneObjConstructor: new (id?: string | undefined) => T, id?: string): T | undefined {
        return this._projectManager.createSceneObjectFromClass(sceneObjConstructor, id);
    }
    createSceneObjectFromJson<T extends SceneObject>(sceneObjectJson: JsonValue & {
        [k: string]: any;
        type: string;
    }): T | undefined {
        return this._projectManager.createSceneObjectFromJson(sceneObjectJson);
    }
    getSceneObjectById(id: string): SceneObject | undefined {
        return this._projectManager.getSceneObjectById(id);
    }
    destroySceneObject(sceneObject: SceneObject): void {
        return this._projectManager.destroySceneObject(sceneObject);
    }
    destroyAllSceneObjects(): void {
        return this._projectManager.destroyAllSceneObjects();
    }
    createSceneObjectTreeItem<T extends SceneObject>(sceneObjectType: string | (new (id?: string | undefined) => T), id?: string, currentTreeItem?: SceneTreeItem, flag?: TreeItemInsertFlag): SceneTreeItem | undefined {
        return this._projectManager.createSceneObjectTreeItem(sceneObjectType, id, currentTreeItem, flag);
    }

    createGroupTreeItem(name?: string, id?: string, currentTreeItem?: SceneTreeItem, flag?: TreeItemInsertFlag): SceneTreeItem | undefined {
        return this._projectManager.createGroupTreeItem(name, id, currentTreeItem, flag);
    }

    createSceneObjectTreeItemFromClass<T extends SceneObject>(sceneObjConstructor: new (id?: string | undefined) => T, id?: string, currentTreeItem?: SceneTreeItem, flag?: TreeItemInsertFlag): SceneTreeItem | undefined {
        return this._projectManager.createSceneObjectTreeItemFromClass(sceneObjConstructor, id, currentTreeItem, flag);
    }

    createSceneObjectTreeItemFromJson(sceneObjectJson: JsonValue & { [k: string]: any; type: string; }, currentTreeItem?: SceneTreeItem, flag?: TreeItemInsertFlag): SceneTreeItem | undefined {
        return this._projectManager.createSceneObjectTreeItemFromJson(sceneObjectJson, currentTreeItem, flag);
    }

    destroySceneObjectTreeItem(sceneTreeItem: SceneTreeItem): void {
        return this._projectManager.destroySceneObjectTreeItem(sceneTreeItem);
    }

    destroyAllSceneObjectTreeItems(): void {
        return this._projectManager.destroyAllSceneObjectTreeItems();
    }
    get propUiTreeManager() {
        return this._projectManager.propUiTreeManager;
    }
    propTreeCallback(params: PropertyCompCallbackFuncParamsType & {
        treeItem: GroupPropTreeItem | LeafPropTreeItem;
        propTree: PropTree;
    }): void {
        this._projectManager.propTreeCallback(params);
    }
    getTreeItemFromSceneObject(sceneObject: SceneObject): SceneTreeItem | undefined {
        return this._projectManager.getTreeItemFromSceneObject(sceneObject);
    }
    selectSingleTreeItemWithSceneObject(sceneObject: SceneObject | undefined): void {
        this._projectManager.selectSingleTreeItemWithSceneObject(sceneObject);
    }
    get sceneObjectsManager() {
        return this._projectManager.sceneObjectsManager;
    }

    private _esPlyarAndPathTime = this.dv(new ESPlyarAndPathTime(this));

    //路径动画管理器
    private _pathAnimationManager = this.dv(new PathAnimationManager(this));
    /**
     * 路径动画管理器
     * 1.channels : { pathId: string, sceneObjectIds: string[] }[]；
     * 2.player : ESPlayer；
     * 3.指定的id的sceneObject必须拥有position和rotation属性,path类型为 ESPath；
     */
    get pathAnimationManager() { return this._pathAnimationManager };

    constructor() {
        super();
        //默认拾取不进入编辑状态
        this._projectManager.editingOnPicking = false;
        this._projectManager.allowPickingOnCreating = false;
    }
}
