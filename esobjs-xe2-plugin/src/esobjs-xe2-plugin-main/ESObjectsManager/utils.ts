import { ESCzmViewer, ESUeViewer, ESViewer } from "../ESViewers";
import { ESObjectsManager } from "./ESObjectsManager";

export type ESUEViewerOptions = { domid: string | HTMLElement, uri: string, app: string, id?: string }
export type ESCzmViewerOptions = { domid: string | HTMLElement, id?: string, czmOptions?: { [k: string]: any } }

export type SwitchToESCzmViewerOptions = {
    domid: string | HTMLElement;
    viewSync?: boolean;
    attributeSync?: boolean;
    destroy?: boolean
};

export type SwitchToESUEViewerOptions = {
    domid: string | HTMLElement;
    uri: string;
    app: string;
    viewSync?: boolean;
    attributeSync?: boolean;
    destroy?: boolean
};
export function parseSwitchToCesiumViewerOptions(...args: any[]): SwitchToESCzmViewerOptions | undefined {
    let options: SwitchToESCzmViewerOptions | undefined = undefined;
    if (typeof args[0] === 'object' && !(args[0] instanceof HTMLElement)) {
        const { domid, viewSync, attributeSync, destroy } = args[0];
        options = { domid, viewSync: viewSync ?? true, attributeSync: attributeSync ?? true, destroy: destroy ?? true };
    } else if (typeof args[0] === 'string' || args[0] instanceof HTMLElement) {
        options = { domid: args[0], viewSync: args[1] ?? true, attributeSync: args[2] ?? true, destroy: args[3] ?? true };
    }
    return options;
}

export function getCameraInfoIfSyncNeeded(activeViewer: ESViewer | undefined, viewSync: boolean) {
    if (activeViewer && viewSync) {
        const info = activeViewer.getCurrentCameraInfo();
        return info;
    } else {
        console.warn('No activeViewer exists or viewSync is false');
        return undefined;
    }
}

export function handleCameraInfo(objm: ESObjectsManager, viewer: ESViewer) {
    const cameraInfo = objm._lastCameraInfo;
    if (cameraInfo) {
        const dispos = viewer.d(viewer.innerViewer.viewerChanged.donce((e) => {
            if (!e || !cameraInfo) return;
            const { position, rotation } = cameraInfo;
            viewer.flyIn(position, rotation, 0);
            objm._lastCameraInfo = undefined;
            dispos();
        }));
    }
}
export function findOrCreateCesiumViewer(
    objectsManager: ESObjectsManager,
    options: SwitchToESCzmViewerOptions) {
    const existingViewer = [...objectsManager.viewers].find((v) => v instanceof ESCzmViewer);
    if (existingViewer && existingViewer instanceof ESCzmViewer && !(options.destroy ?? true)) {

        objectsManager.activeViewer && (options.attributeSync ?? true) && syncOnceOtherViewer(existingViewer, objectsManager.activeViewer)
        existingViewer.changeContainer(options);
        return existingViewer;
    } else {
        console.warn('No CesiumViewer exists or destroy is true, will be created');
        const newViewer = objectsManager.createCesiumViewer(options);
        if (newViewer) {
            objectsManager.activeViewer && (options.attributeSync ?? true) && syncOnceOtherViewer(newViewer, objectsManager.activeViewer)
        }
        return newViewer;
    }
}


export const getSyncPropsValue = (otherViewer?: ESViewer) => {
    if (!otherViewer) return;
    const reactProps = ESViewer.createReactProps();
    const otherProps = { ...reactProps };
    Object.keys(reactProps).forEach(item => {
        //@ts-ignore
        otherProps[item] = otherViewer[item];
    })
    return otherProps
}


export const syncOnceOtherViewer = (viewer: ESViewer, otherViewer: ESViewer) => {
    const reactProps = ESViewer.createReactProps();
    try {
        Object.keys(reactProps).forEach(item => {
            //@ts-ignore
            viewer[item] = otherViewer[item]
        })
    } catch (error) {
        console.warn(error)
    }
}


export function parseSwitchToUEViewerOptions(...args: any[]): SwitchToESUEViewerOptions | undefined {
    let options: SwitchToESUEViewerOptions | undefined = undefined;
    if (typeof args[0] === 'object' && !(args[0] instanceof HTMLElement)) {
        const { domid, uri, app, viewSync, attributeSync, destroy } = args[0];
        options = { domid, uri, app, viewSync: viewSync ?? true, attributeSync: attributeSync ?? true, destroy: destroy ?? true };
    } else if (typeof args[0] === 'string' || args[0] instanceof HTMLElement) {
        options = { domid: args[0], uri: args[1], app: args[2], viewSync: args[3] ?? true, attributeSync: args[4] ?? true, destroy: args[5] ?? true };
    }
    return options;
}


export function findOrCreateUEViewer(
    objectsManager: ESObjectsManager,
    options: SwitchToESUEViewerOptions,
): ESUeViewer {
    const existingViewer = [...objectsManager.viewers].find((v) => v instanceof ESUeViewer);
    if (existingViewer && existingViewer instanceof ESUeViewer && !(options.destroy ?? true)) {

        objectsManager.activeViewer && (options.attributeSync ?? true) && syncOnceOtherViewer(existingViewer, objectsManager.activeViewer)
        existingViewer.changeContainer(options);
        return existingViewer;
    } else {
        console.warn('No UEViewer exists or destroy is true, will be created');
        const newViewer = objectsManager.createUEViewer(options);
        if (newViewer) {
            objectsManager.activeViewer && (options.attributeSync ?? true) && syncOnceOtherViewer(newViewer, objectsManager.activeViewer)
        }
        return newViewer;
    }
}


