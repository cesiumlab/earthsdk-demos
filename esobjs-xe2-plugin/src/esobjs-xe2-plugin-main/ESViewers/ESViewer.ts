import { toCartesian3 } from 'xbsj-xe2/dist-node/xe2-base-cesium';
import { ESSceneObject, ObjResettingWithEvent, PickedInfo, SceneObjectPickedInfo, getGeoBoundingSphereFromPositions } from 'xbsj-xe2/dist-node/xe2-base-objects';
import { Event, PartialWithUndefinedReactivePropsToNativeProps, ReactivePropsToNativePropsAndChanged, createGuid, createNextAnimateFrameEvent, extendClassProps, react, reactArrayWithUndefined } from 'xbsj-xe2/dist-node/xe2-base-utils';
import { CzmViewer } from 'xbsj-xe2/dist-node/xe2-cesium-objects';
import { UeViewer } from 'xbsj-xe2/dist-node/xe2-ue-objects';
import { ESObjectsManager } from '../ESObjectsManager';
import * as Cesium from 'cesium';
import { SyncEventResetting, TimeSyncEventResetting } from './utils';
export type ESViewerType = 'ESViewer' | 'ESUeViewer' | 'ESCzmViewer';
export type ESNavigationMode = "Map" | "Walk" | "Line" | "UserDefined" | "RotateGlobe" | "RotatePoint" | "Follow";
type FlyToParamType = { distance: number, heading: number, pitch: number, flyDuration: number, hDelta: number, pDelta: number };
type StatusInfoType = {
    fps: number;
    position: [number, number, number];
    rotation: [number, number, number];
    length: number;
}

/**
 * https://www.wolai.com/earthsdk/pHn9b4jQ8ArXz8U29bjrAb
 * https://c0yh9tnn0na.feishu.cn/docx/K7VwdBYTMoG0BtxMX6McvOVDn4d
 */
export abstract class ESViewer extends ESSceneObject {
    abstract get innerViewer(): CzmViewer | UeViewer;
    _statusInfo: StatusInfoType = {
        fps: 0,
        position: [0, 0, 0],
        rotation: [0, 0, 0],
        length: 0,
    }

    get typeName() { return this._typeName }
    override get defaultProps() { return ESViewer.createDefaultProps(); }
    get json() {
        const sceneObj = this._innerGetJson(false) as JsonType;
        if (this.innerViewer instanceof UeViewer) {
            const { uri, signallingServerId } = this.innerViewer
            return { ...sceneObj, uri, app: signallingServerId, } as JsonType;
        } else {
            return { ...sceneObj } as JsonType;
        }
    }
    set json(value: JsonType) {
        this._innerSetJson(value);
        if (this.innerViewer instanceof UeViewer) {
            //@ts-ignore
            this.innerViewer.uri = value.uri ?? "";
            //@ts-ignore
            this.innerViewer.signallingServerId = value.app ?? "";
        }
    }

    //////////////////////////////////////////////////////////////////////////////
    // 内部使用的属性 begin
    get objectManager() { return this._objectManager; }
    // 内部使用的属性 end
    //////////////////////////////////////////////////////////////////////////////

    //只读属性
    get status() { return this.innerViewer.status; }
    get statusChanged() { return this.innerViewer.statusChanged; }
    get viewerChanged() { return this.innerViewer.viewerChanged; }

    //////////////////////////////////////////////////////////////////////////////
    // actived 相关属性和逻辑操作  只读属性，不能通过设置actived来变为activeViewer
    get actived() { return this === this._objectManager.activeViewer; }
    private _activedDispose = (() => {
        this.d(this._objectManager.activeViewerChanged.don(() => { this.innerViewer.actived = this.actived; }));
    })();
    // actived 相关属性和逻辑操作 end
    //////////////////////////////////////////////////////////////////////////////

    private _syncViewer = this.dv(react<ESViewer | undefined>(undefined));
    get syncViewer() { return this._syncViewer.value; }
    set syncViewer(value) { this._syncViewer.value = value; }
    get syncViewerChanged() { return this._syncViewer.changed; }

    public readonly syncEvent = this.dv(createNextAnimateFrameEvent(
        this.objectManager.activeViewerChanged,
        this.objectManager.syncOtherViewersToActivedChanged,
    ))
    private _syncEventDispose = this.d(this.syncEvent.don(() => {
        const enable = this.objectManager.syncOtherViewersToActived;
        //全局不同步或者自身是activeViewer，则不需要同步视口,新创建的视口需要同步
        if (!enable || this.actived) {
            this.syncOtherViewer(undefined);
            return;
        };
        this.syncOtherViewer(this.objectManager.activeViewer);
    }))

    private _syncEventResetting = this.dv(new ObjResettingWithEvent(this.syncViewerChanged, () => {
        const syncViewer = this.syncViewer;
        if (!syncViewer) return undefined;
        return new SyncEventResetting(this, syncViewer);
    }));

    //绑定同步两个视口的属性,undefined时解绑
    syncOtherViewer(viewer: ESViewer | undefined) {
        //被同步的视口不能同步其他视口
        if (viewer && viewer.syncViewer) viewer.syncViewer = undefined;
        if (viewer === this) {
            this._syncViewer.value = undefined;
        } else {
            this._syncViewer.value = viewer;
        }
    }

    private _timeSyncEventDispose = this.dv(new ObjResettingWithEvent(this.timeSyncChanged, () => {
        if (!this.timeSync) return undefined;
        return new TimeSyncEventResetting(this);
    }));

    /**
     * 设置当前时间
     * @param value 时间戳(毫秒)数值或者时间格式字符串
     * 字符串格式 2024 06 26 12:34:56 或者 2023-09-29或者2023/09/29 12:34:56
     */
    setCurrentTime(value: number | string) {
        const time = (typeof value === 'string') ? Date.parse(value) : value;
        if (!time) {
            console.warn('setCurrentTime 参数格式有误', value);
            return;
        }
        this.currentTime = time;
    }

    //获取视口的引擎类型 UE/Cesium
    getEngineType() {
        if (this.innerViewer instanceof CzmViewer) {
            return 'Cesium';
        } else {
            return 'UE';
        }
    }
    /**
     * 两种引擎返回值不一致
     * @param screenPosition 屏幕坐标
     * @param attachedInfo 自定义标识
     * @param parentInfo 是否拾取父节点 仅仅UE支持
     * @return PickedInfo | UePickedInfo | undefined
     */
    async pick(screenPosition: [number, number], attachedInfo?: any, parentInfo?: boolean) {
        const res = await this.objectManager.projectManager.viewerPick(this.innerViewer, screenPosition, attachedInfo, parentInfo)
        if (res instanceof SceneObjectPickedInfo) {
            return res
        } else {
            return { childPickedInfo: res, sceneObject: undefined }
        }
    }

    async pickPosition(screenPosition: [number, number]) {
        const res = await this.innerViewer.pickPosition(screenPosition)
        return res
    }
    //duration单位为秒
    flyIn(position: [number, number, number], rotation?: [number, number, number], duration?: number) {
        if (this.innerViewer instanceof CzmViewer) {
            this.innerViewer.flyTo(position, undefined, rotation, duration && duration * 1000);
        } else {
            this.innerViewer.flyIn(position, rotation, duration)
        }
    }
    //flyDuration单位为秒
    flyTo(flyToParam: FlyToParamType, position: [number, number, number]) {
        if (this.innerViewer instanceof CzmViewer) {
            const { distance, heading, pitch, flyDuration, hDelta, pDelta } = flyToParam;
            this.innerViewer.flyTo(position, distance, [heading, pitch, 0], flyDuration * 1000, hDelta, pDelta);
        } else {
            this.innerViewer.flyTo(flyToParam, position)
        }
    }
    flyToBoundingSphere(rectangle: [number, number, number, number], distance?: number, duration?: number | undefined) {
        const positions: [number, number, number][] = [
            [rectangle[0], rectangle[1], 0],
            [rectangle[0], rectangle[3], 0],
            [rectangle[2], rectangle[3], 0],
            [rectangle[2], rectangle[1], 0]
        ]
        const options = getGeoBoundingSphereFromPositions(positions)
        if (!options) return;
        const { center, radius } = options
        if (this.innerViewer instanceof CzmViewer) {
            this.innerViewer.flyTo(center, distance ?? radius, [0, -90, 0], (duration ?? 1) * 1000);
        } else {
            const flyToParam = {
                distance: distance ?? radius,
                heading: 0,
                pitch: -90,
                flyDuration: duration ?? 1,
                hDelta: 0,
                pDelta: 0
            } as const;
            this.innerViewer.flyTo(flyToParam, center)
        }
    }
    getCurrentCameraInfo() {
        if (this.innerViewer instanceof CzmViewer) {
            const info = this.innerViewer.getCameraInfo()
            return info
        } else {
            const { position, rotation } = this._statusInfo
            return { position, rotation }
        }
    }
    getLengthInPixel() {
        if (this.innerViewer instanceof CzmViewer) {
            //@ts-ignore
            const length = this.innerViewer.viewerLegend.legend.computedLengthInMeters / this.innerViewer.viewerLegend.legend.computedLengthInPixels as number
            return length
        } else {
            const { length } = this._statusInfo
            return length
        }
    }

    protected _navigationMode = this.dv(react<ESNavigationMode>('Map'));
    get navigationModeChanged() { return this._navigationMode.changed; }
    getNavigationMode() { return this._navigationMode.value; }

    /**
     * 切换导航模式为“行走”（第一人称）模式。
     * 此模式允许用户在行走的模式下导航，键盘 W A S D控制行走，鼠标控制方向。
     *
     * @param position - 行走导航模式的初始位置 [number, number, number]。
     * @param jumpZVelocity - 跳跃时的 Z 轴速度。默认为 4.2m/s。
     *
     * @returns {void}
     */
    changeToWalk(position: [number, number, number], jumpZVelocity: number = 4.2) {
        this._navigationMode.value = 'Walk';
        const opt = { mode: 'Walk', position: position, jumpZVelocity } as const
        if (this.innerViewer instanceof CzmViewer) return;
        this.innerViewer.changeNavigationMode(opt);
    }

    /**
     * 切换导航模式为“地图”模式。
     * 此模式允许用户在地图上自由导航。
     *
     * @returns {void}
     */
    changeToMap() {
        console.log('changeToMap');
        this._navigationMode.value = 'Map';
        const opt = { mode: 'Map' } as const
        if (this.innerViewer instanceof CzmViewer) return;
        this.innerViewer?.changeNavigationMode(opt);
    }

    /**
     * 切换到“旋转地球”导航模式。
     * 此模式允许用户在连续循环中旋转地球。
     *
     * @param latitude - 地球旋转的纬度。默认为 38 度。
     * @param height - 地球旋转的高度。默认为 10,000,000 米。
     * @param cycleTime - 一个完整循环所需的秒数。默认为 60 秒。
     *
     * @returns {void}
     */
    changeToRotateGlobe(latitude?: number, height?: number, cycleTime?: number) {
        this._navigationMode.value = 'RotateGlobe';
        const opt = {
            mode: 'RotateGlobe',
            latitude: latitude ?? 38,
            height: height ?? 10000000,
            cycleTime: cycleTime ?? 60
        } as const
        if (this.innerViewer instanceof CzmViewer) return;
        this.innerViewer.changeNavigationMode(opt);
    }

    /**
     * 切换导航模式为“Line”（线路）沿线漫游。
     * 此模式允许用户沿指定的地理线路导航。
     * @param geoLineStringId - 要导航的地理线路的唯一标识符。
     * @param speed - 用户在线路上导航的速度 默认10m/s。
     * @param heightOffset - 用户在线路上导航的垂直偏移量 默认 10米。
     * @param loop - 一个布尔值，表示在到达线路末尾后是否导航回到起点循环。默认开启
     * @param turnRateDPS - 用户在线路上导航的转弯速度 度/秒。
     * @param lineMode - 路径漫游模式 auto/manual 默认 auto。
     *
     * @returns {void} 
     */
    changeToLine(geoLineStringId: string, speed: number = 10, heightOffset: number = 10, loop: boolean = true, turnRateDPS: number = 10, lineMode: "auto" | "manual" = "auto") {
        this._navigationMode.value = 'Line';
        const opt = {
            mode: 'Line',
            geoLineStringId: geoLineStringId,
            speed: speed,
            heightOffset: heightOffset,
            loop: loop,
            turnRateDPS: turnRateDPS,
            lineMode: lineMode
        } as const
        if (this.innerViewer instanceof CzmViewer) return;
        this.innerViewer.changeNavigationMode(opt);
    }

    /**
     * 切换导航模式为“UserDefined” 仅UE支持。
     * 此模式允许用户自定义交互方式。
     *
     * @param userDefinedPawn ue支持，用户自定义交互方式
     * @returns 
     */
    changeToUserDefined(userDefinedPawn: string) {
        this._navigationMode.value = 'UserDefined';
        const opt = { mode: 'UserDefined', userDefinedPawn: userDefinedPawn } as const
        if (this.innerViewer instanceof UeViewer) {
            this.innerViewer.changeNavigationMode(opt);
        }
    }

    /**
     * 切换导航模式为“RotatePoint”（绕点）绕点旋转。
     * 此模式允许用户绕点旋转。
     * @param position - 要环绕的点位置（经纬度）。
     * @param heading - 初始的环绕角度，默认0。
     * @param pitch - 初始的环绕俯仰角，默认-30。
     * @param distance - 距离点的距离，默认50000米 ，单位米
     * @param orbitPeriod - 默认环绕一周的时间 单位S,默认60S。
     *
     * @returns {void} 
     */
    changeToRotatePoint(position: [number, number, number], distance: number = 50000, orbitPeriod: number = 60, heading: number = 0, pitch: number = -30) {
        this._navigationMode.value = 'RotatePoint';
        const opt = {
            mode: 'RotatePoint',
            position: position,
            distance: distance,
            orbitPeriod: orbitPeriod,
            heading: heading,
            pitch: pitch,
        } as const
        console.log('绕点模式：', opt);
        if (this.innerViewer instanceof CzmViewer) return;
        this.innerViewer.changeNavigationMode(opt);
    }

    /**
     * 切换导航模式为“Follow”（跟随）跟随模式。
     * 此模式允许视角跟随ES对象。
     * @param objectId - 要跟随的ES对象Id。
     * @param distance - 距离点的距离，默认0米 ，单位米 传入0会自行计算距离为包围盒半径*3
     * @param heading - 初始的环绕角度，默认0。
     * @param pitch - 初始的环绕俯仰角，默认-30。
     * @param relativeRotation - 相机姿态保持相对，默认true。默认情况下，相机会跟随人物视角转动，如果设置为false，相机会保持一个方向不动。
     *
     * @returns {void} 
     */
    changeToFollow(objectId: string, distance: number = 0, heading: number = 0, pitch: number = -30, relativeRotation: boolean = true) {
        this._navigationMode.value = 'Follow';
        const opt = {
            mode: 'Follow',
            objectId,
            distance,
            heading,
            pitch,
            relativeRotation
        } as const
        console.log('跟随模式：', opt);
        if (this.innerViewer instanceof CzmViewer) return;
        this.innerViewer.changeNavigationMode(opt);
    }

    abstract computeVisibility(center: [number, number, number], radius: number): Promise<"outside" | "intersecting" | "inside" | undefined>

    getFPS() {
        if (this.innerViewer instanceof CzmViewer) {
            return this.innerViewer.fps;
        } else {
            return this._statusInfo.fps
        }
    }
    //版本信息初始化时调用一次后存下来
    _versions: string[] = [];
    getVersion() { return this._versions };
    private _setVersions = (() => {
        const versionList: string[] = [];
        const names: string[] = [];
        //@ts-ignore
        const info = window.g_xe2CopyRightsMd && window.g_xe2CopyRightsMd() || '' as string
        if (info) {
            const spans = info.match(/<span.*?>(.*?)<\/span>/g);
            const spans2 = spans.map((span: any) => span.match(/<span.*?>(.*?)<\/span>/)[1]);
            const result = spans2.map((span: any) => span.replace(/<a.*?>|<\/a>/g, ''));
            result.forEach((el: string) => {
                const index = el.indexOf('/');
                if (index === -1) {
                    (!names.includes(el)) && names.push(el) && versionList.push(el);
                } else {
                    const name = el.substring(0, index);
                    if (names.includes(name)) return;
                    names.push(name);
                    versionList.push(el);
                }
            })
        }
        this._versions.push(...versionList);
    })();

    async getHeightByLonLat(lon: number, lat: number, channel: string = "ECC_GameTraceChannel1"): Promise<number | null> {
        if (this.innerViewer instanceof CzmViewer) {
            // const res = this.sampleHeight([lon, lat])
            // return (res !== undefined) ? res : null;
            const viewer = this.innerViewer.viewer;
            if (!viewer) return null;
            const carto = Cesium.Cartographic.fromDegrees(lon, lat, undefined, CzmViewer.getHeightsScartchCarto);
            const height = viewer.scene.sampleHeight(carto);
            return height;
        } else {
            if (!channel) return null;
            const res = await this.innerViewer.getHeightByLonLat(lon, lat, channel)
            const height = res ? (res.height !== undefined ? res.height : null) : null
            return height;
        }
    }
    async getHeightsByLonLats(lonLats: [number, number][], channel: string = "ECC_GameTraceChannel1") {
        if (this.innerViewer instanceof CzmViewer) {
            const heights = lonLats.map(lonLat => this.getHeightByLonLat(...lonLat))
            const resPromise = await Promise.all(heights)
            return resPromise;
        } else {
            if (!channel) return;
            const res = await this.innerViewer.getHeightsByLonLats(lonLats, channel)
            return res;
        }
    }
    async capture(resx: number = 64, resy: number = 64) {
        const res = await this.innerViewer.capture(resx, resy)
        return res;
    }
    async lonLatAltToScreenPosition(position: [number, number, number]) {
        if (this.innerViewer instanceof CzmViewer) {
            const res = this.innerViewer.viewer?.scene.cartesianToCanvasCoordinates(toCartesian3(position))
            return res && [res.x, res.y] as [number, number];
        } else {
            const res = await this.innerViewer.lonLatAltToScreenPosition(position)
            return res && [res.Y, res.Y] as [number, number];
        }
    }

    /**事件 */

    private _hoverEvent = this.dv(new Event<[{ screenPosition: [number, number], mouseEvent?: MouseEvent }]>());
    get hoverEvent() { return this._hoverEvent; }

    private _hoverOutEvent = this.dv(new Event());
    get hoverOutEvent() { return this._hoverOutEvent; }

    private _mouseMoveEvent = this.dv(new Event<[{ screenPosition: [number, number], mouseEvent?: MouseEvent }]>());
    get mouseMoveEvent() { return this._mouseMoveEvent; }

    private _clickEvent = this.dv(new Event<[{ screenPosition?: [number, number], mouseEvent?: MouseEvent }]>());
    get clickEvent() { return this._clickEvent; }

    private _dbclickEvent = this.dv(new Event<[{ screenPosition?: [number, number], mouseEvent?: MouseEvent }]>());
    get dbclickEvent() { return this._dbclickEvent; }

    private _pickedEvent = this.dv(new Event<[pickedInfo: PickedInfo]>());
    get pickedEvent() { return this._pickedEvent; }

    override get id() {
        return this._vid;
    }

    constructor(private _objectManager: ESObjectsManager, private _vid: string = createGuid(), private _typeName: ESViewerType) {
        super();

    }
}

export namespace ESViewer {
    export const createReactProps = () => ({
        //基础
        show: true,
        globeShow: true,
        ionAccessToken: '' as string,
        apiKey: '' as string,
        apiUrl: '' as string,
        secretKey: '' as string,
        speechVoiceMode: '' as string,
        hoverTime: 2,
        debug: false,
        // DECIMAL_DEGREE   度格式，dd.ddddd°
        // DEGREES_DECIMAL_MINUTES  度分格式，dd°mm.mmm'
        // SEXAGESIMAL_DEGREE  度分秒格式，dd°mm'ss"
        lonLatFormat: 'DECIMAL_DEGREE',
        fov: 60,
        //环境
        currentTime: Date.now(),//当前时间，控制光照
        simulationTime: Date.now(),//仿真时间，控制场景动画
        timeSync: false,//时间同步，false 禁用，true 启用,控制两个时间的同步

        rain: 0,//雨
        snow: 0,//雪
        cloud: 0,//云
        fog: 0,//雾
        depthOfField: 0,//景深
        atmosphere: true,//大气
        sun: true,//太阳
        moon: true,//月亮
        //--------------------------UE特有的属性放到ESUeViewer中！
        // sunIntensity:111000,//光照强度
        // brightness:50,//像素亮度
        // ev100Ratio:0.2,//EV100比值
        // skybox: reactJsonWithUndefined<SkyboxType>(undefined),//使用对象，此处删除

        // 编辑状态
        // editingPointSize: 12,
        // editingPointColor: reactArray<[number, number, number, number]>([0, 1, 0, 1]),
        // editingAuxiliaryPointColor: reactArray<[number, number, number, number]>([1, 1, 0, 1]),
        // editingLineWidth: 1,
        // editingLineColor: reactArray<[number, number, number, number]>([1, 1, 1, 1]),
        // editingAxisSize: 1,
        // editingAuxiliaryPointSize: 8
        editingPointSize: undefined as number | undefined,
        editingPointColor: reactArrayWithUndefined<[number, number, number, number] | undefined>(undefined),
        editingAuxiliaryPointColor: reactArrayWithUndefined<[number, number, number, number] | undefined>(undefined),
        editingLineWidth: undefined as number | undefined,
        editingLineColor: reactArrayWithUndefined<[number, number, number, number] | undefined>(undefined),
        editingAxisSize: undefined as number | undefined,
        editingAuxiliaryPointSize: undefined as number | undefined,
        textAvoidance: undefined as boolean | undefined,//是否开启避让
        flyToBoundingSize: 256 as number | undefined,//默认飞行定位时对象包围球所占的屏幕像素大小
        editingHeightOffset: 0 as number | undefined,//编辑高度偏移
        terrainShader: {
            slope: {
                show: false
            },
            aspect: {
                show: false,
            },
            elevationRamp: {
                show: false,
            },
            elevationContour: {
                show: false,
                color: [1, 0, 0, 1],
                spacing: 150,
                width: 2,
            }
        }
    });
}
export namespace ESViewer {
    export const createDefaultProps = () => ({
        ...ESViewer.createReactProps(),
        ...ESSceneObject.createDefaultProps(),
    });
}
extendClassProps(ESViewer.prototype, ESViewer.createDefaultProps);
export interface ESViewer extends ReactivePropsToNativePropsAndChanged<ReturnType<typeof ESViewer.createDefaultProps>> { }
type JsonType = PartialWithUndefinedReactivePropsToNativeProps<ReturnType<typeof ESViewer.createDefaultProps> & { type: string }>;
