import { Event, bind, createNextAnimateFrameEvent, react, track } from 'xbsj-xe2/dist-node/xe2-base-utils';
import { UeViewer } from 'xbsj-xe2/dist-node/xe2-ue-objects';
import { ESObjectsManager } from "../ESObjectsManager";
import { ESUEViewerOptions } from '../ESObjectsManager/utils';
import { ESViewer } from "./ESViewer";
import { computeVisibilityCallFunc } from './ueutils';


/**
 * https://www.wolai.com/earthsdk/igCH4kASqsnDLfAXZbn7zM
 * https://c0yh9tnn0na.feishu.cn/docx/IOIgdN3mJo4bmOxqMCzca6wEnCb
 */
export class ESUeViewer extends ESViewer {
    private _ESFORUE_VERSION = `ESForUE_25.03.03`;

    private _innerViewer = this.objectManager.createSceneObjectFromClass(UeViewer) as UeViewer;
    private _innerViewerDispose = this.d(() => this.objectManager.destroySceneObject(this._innerViewer));
    get innerViewer() { return this._innerViewer; }

    //属性/////////////////////////////////////
    get geoReferenceOrigin() { return this._innerViewer.geoReferenceOrigin; }
    set geoReferenceOrigin(value: [number, number, number] | undefined) { this._innerViewer.geoReferenceOrigin = value; }
    get geoReferenceOriginChanged() { return this._innerViewer.geoReferenceOriginChanged; }

    get keepWorldOriginNearCamera() { return this._innerViewer.keepWorldOriginNearCamera ?? false; }
    set keepWorldOriginNearCamera(value: boolean) { this._innerViewer.keepWorldOriginNearCamera = value; }
    get keepWorldOriginNearCameraChanged() { return this._innerViewer.keepWorldOriginNearCameraChanged; }

    get widgetInteractionDistance() { return this._innerViewer.widgetInteractionDistance ?? 200; }
    set widgetInteractionDistance(value: number) { this._innerViewer.widgetInteractionDistance = value; }
    get widgetInteractionDistanceChanged() { return this._innerViewer.widgetInteractionDistanceChanged; }

    get sceneControlled() { return this._innerViewer.sceneControlled ?? true; }
    set sceneControlled(value: boolean) { this._innerViewer.sceneControlled = value; }
    get sceneControlledChanged() { return this._innerViewer.sceneControlledChanged; }

    get brightness() { return this._innerViewer.brightness ?? 0; }
    set brightness(value: number) { this._innerViewer.brightness = value; }
    get brightnessChanged() { return this._innerViewer.brightnessChanged; }

    /**
     * @deprecated 避免使用该属性,将于两个版本后移除
     */
    get sunIntensity() { return this._innerViewer.sunIntensity ?? 111000; }
    set sunIntensity(value: number) { this._innerViewer.sunIntensity = value; }
    get sunIntensityChanged() { return this._innerViewer.sunIntensityChanged; }

    /**
     * @deprecated 避免使用该属性,将于两个版本后移除
     */
    get ev100Ratio() { return this._innerViewer.ev100Ratio ?? 0.01; }
    set ev100Ratio(value: number) { this._innerViewer.ev100Ratio = value; }
    get ev100RatioChanged() { return this._innerViewer.ev100RatioChanged; }

    /**
     * @deprecated 请使用sceneControlled,该属性将于两个版本后移除
     */
    get sunskyControlled() { return this._innerViewer.sunskyControlled ?? true; }
    set sunskyControlled(value: boolean) { this._innerViewer.sunskyControlled = value; }
    get sunskyControlledChanged() { return this._innerViewer.sunskyControlledChanged; }

    get emissiveIntensity() { return this._innerViewer.emissiveIntensity ?? 1; }
    set emissiveIntensity(value: number) { this._innerViewer.emissiveIntensity = value; }
    get emissiveIntensityChanged() { return this._innerViewer.emissiveIntensityChanged; }

    get memReportInterval() { return this._innerViewer.memReportInterval ?? 10; }
    set memReportInterval(value: number) { this._innerViewer.memReportInterval = value; }
    get memReportIntervalChanged() { return this._innerViewer.memReportIntervalChanged; }

    get autoReconnect() { return this._innerViewer.autoReconnect ?? true; }
    set autoReconnect(value: boolean) { this._innerViewer.autoReconnect = value; }
    get autoReconnectChanged() { return this._innerViewer.autoReconnectChanged; }

    get statusUpdateInterval() { return this._innerViewer.statusUpdateInterval ?? 0.5; }
    set statusUpdateInterval(value: number) { this._innerViewer.statusUpdateInterval = value; }
    get statusUpdateIntervalChanged() { return this._innerViewer.statusUpdateIntervalChanged; }

    get baseUrl() { return this._innerViewer.baseUrl ?? window.location.href ?? ''; }
    set baseUrl(value: string) { this._innerViewer.baseUrl = value; }
    get baseUrlChanged() { return this._innerViewer.baseUrlChanged; }

    //////////////////////////////////////////
    //事件////////////////////////////////////////
    private _syncEventDis = this.d(this.viewerChanged.don(() => { this.syncEvent.emit() }))
    private _hoverEventDispose = (() => {
        this.d(this._innerViewer.hoverEvent.don((el) => { this.hoverEvent.emit({ screenPosition: el.screenPosition }) })
        )
    })()
    private _hoverOutEventDispose = (() => {
        this.d(this._innerViewer.hoverOutEvent.don(() => { this.hoverOutEvent.emit() }))
    })()
    private _mouseMoveEventDispose = (() => {
        this.d(this._innerViewer.mouseMoveEvent.don((el) => { this.mouseMoveEvent.emit({ screenPosition: el.screenPosition }) }))
    })()
    private _clickEventDispose = (() => {
        this.d(this._innerViewer.click.don((el) => {
            this.clickEvent.emit({ screenPosition: el.screenPosition })
        }))
    })()
    private _dbclickEventDispose = (() => {
        this.d(this._innerViewer.dbclick.don((el) => {
            this.dbclickEvent.emit({ screenPosition: el.screenPosition })
        }))
    })()

    private _pickedEventDispose = (() => {
        this.d(this._innerViewer.uePickedEvent.don((el) => { el && this.pickedEvent.emit(el) }))
    })()

    private _statusUpdateEvent = (() => {
        this.d(this._innerViewer.statusUpdateEvent.don((data) => {
            //@ts-ignore
            this._statusInfo.fps = data.FPS ?? 0;
            //@ts-ignore
            this._statusInfo.length = data.length ?? 0;
            //@ts-ignore
            this._statusInfo.position = data.position ?? [0, 0, 0];
            //@ts-ignore
            this._statusInfo.rotation = data.rotation ?? [0, 0, 0];
        }))
    })()


    private _viewerChangedEvent = (() => {
        const update = async () => {
            const v = await this._innerViewer.getVersion();
            if (!v) return;
            //根据符号";"将version切分
            const versionList = v.version.split(";");
            this._versions.push(...versionList);
            const index = versionList.findIndex(ve => ve.includes('ESForUE'));
            if (index === -1) return;
            const version = versionList[index];
            if (version.startsWith(this._ESFORUE_VERSION)) {
                console.warn(`当前版本EarthSDK对应的ESForUE版本为: ${this._ESFORUE_VERSION};而已连接实例的ESForUE版本为: ${version}; Version Error! `)
            }
        };
        this.d(this._innerViewer.viewerChanged.donce(update));
    })();
    //用户监听自定义事件
    get customMessage() { return this._innerViewer.customMessage; }
    //语音识别事件
    get speechRecognition() { return this._innerViewer.speechRecognition; }
    ////////////////////////////////////////////
    //方法//////////////////////////////////////////
    /**
     * UE位置信息转经纬度
     */
    async UEPositionToLonLatAlt(pos: [number, number, number]) {
        const position = await this._innerViewer.uePositionToLonLatAlt(pos);
        return position;
    }
    defaultCameraFlyIn(duration?: number) { this._innerViewer.defaultCameraFlyIn(duration); }
    restoreOriginalScene() { this._innerViewer.restoreOriginalScene(); }
    quit() { this._innerViewer.quit(); }
    async getAllSocketNamesByActorTag(actorTag: string) {
        const res = await this._innerViewer.getAllSocketNamesByActorTag(actorTag);
        return res;
    }
    // 废弃
    // setSpeechRecognitionKeys(apiKey: string, secretKey: string) { this._innerViewer.setSpeechRecognitionKeys(apiKey, secretKey); }
    async getCameraRelativeHeight(channel: string) {
        const res = await this._innerViewer.getCameraRelativeHeight(channel);
        return res && res.re.height;
    }

    async sendCustomMessage(message: string) {
        const res = await this._innerViewer.sendCustomMessage(message);
        return res;
    }

    async getObjectByInfo(info: { actorTag: string, componentTag: string }) {
        const res = await this._innerViewer.getObjectByInfo(info);
        return res && res.object;
    }

    async saveStringToFile(str: string, path?: string, file?: string) {
        const res = await this._innerViewer.saveStringToFile(str, path, file);
        return res;
    }
    ////////////////////////////////////////////
    changeContainer(options: ESUEViewerOptions) {
        this._innerViewer.uri = options.uri;
        this._innerViewer.signallingServerId = options.app;

        let divBox: HTMLElement;
        if (typeof options.domid === 'string') {
            const box = document.getElementById(options.domid);
            if (!box) {
                console.error('未找到id为' + options.domid + '的元素');
                return;
            };
            divBox = box;
        } else {
            divBox = options.domid;
        }
        divBox.innerHTML = '';
        // 创建一个空的div容器
        const container = document.createElement('div');
        // container.style.width = '100%';
        // container.style.height = '100%';
        // container.style.margin = '0px';
        // container.style.padding = '0px';
        // container.style.border = 'none';
        // container.style.overflow = 'hidden';
        // container.style.background = 'rgba(0,0,0,0)';
        container.style.cssText = `width: 100%; height: 100%; margin: 0px; padding: 0px; border: none; overflow: hidden; position: relative; z-index: 0; background: rgba(0,0,0,0);`
        divBox.appendChild(container);
        this._innerViewer.containerOrId = container;
    }

    async computeVisibility(center: [number, number, number], radius: number) {
        if (!this._innerViewer.viewer) return undefined;
        return await computeVisibilityCallFunc(this._innerViewer.viewer, { center, radius });
    }

    constructor(objectManager: ESObjectsManager, options: ESUEViewerOptions) {
        super(objectManager, options.id, 'ESUeViewer');

        this.changeContainer(options);

        //ESViewer属性绑定TODO
        //基础
        this.d(track([this._innerViewer, 'show'], [this, 'show']));
        this.d(track([this._innerViewer, 'globeShow'], [this, 'globeShow']));
        this.d(track([this._innerViewer, 'hoverTime'], [this, 'hoverTime']));
        this.d(track([this._innerViewer, 'debug'], [this, 'debug']));
        // 先注释，等xbsj-xe2发版，在取消注释
        this.d(track([this._innerViewer, 'fov'], [this, 'fov']));
        this.d(track([this._innerViewer, 'ionAccessToken'], [this, 'ionAccessToken']));

        // 语音识别
        this.d(track([this._innerViewer, 'apiKey'], [this, 'apiKey']));
        this.d(track([this._innerViewer, 'apiUrl'], [this, 'apiUrl']));
        this.d(track([this._innerViewer, 'secretKey'], [this, 'secretKey']));
        this.d(track([this._innerViewer, 'speechVoiceMode'], [this, 'speechVoiceMode']));
        //环境TODO
        this.d(track([this._innerViewer, 'currentTime'], [this, 'currentTime']));
        this.d(track([this._innerViewer, 'rain'], [this, 'rain']));
        this.d(track([this._innerViewer, 'snow'], [this, 'snow']));
        this.d(track([this._innerViewer, 'cloud'], [this, 'cloud']));
        this.d(track([this._innerViewer, 'atmosphere'], [this, 'atmosphere']));
        this.d(track([this._innerViewer, 'depthOfField'], [this, 'depthOfField']));
        this.d(track([this._innerViewer, 'fog'], [this, 'fog']));

        //光照强度
        this.d(track([this._innerViewer, 'brightness'], [this, 'brightness']));
        //编辑
        this.d(track([this._innerViewer, 'editingPointSize'], [this, 'editingPointSize']));
        this.d(track([this._innerViewer, 'editingPointColor'], [this, 'editingPointColor']));
        this.d(track([this._innerViewer, 'editingAuxiliaryPointColor'], [this, 'editingAuxiliaryPointColor']));
        this.d(track([this._innerViewer, 'editingLineWidth'], [this, 'editingLineWidth']));
        this.d(track([this._innerViewer, 'editingLineColor'], [this, 'editingLineColor']));
        this.d(track([this._innerViewer, 'editingAxisSize'], [this, 'editingAxisSize']));
        this.d(track([this._innerViewer, 'editingAuxiliaryPointSize'], [this, 'editingAuxiliaryPointSize']));

        this.d(track([this.innerViewer, 'lonLatFormat'], [this, 'lonLatFormat']));
        this.d(track([this._innerViewer, 'textAvoidance'], [this, 'textAvoidance']));
        this.d(track([this._innerViewer, 'flyToBoundingSize'], [this, 'flyToBoundingSize']));
        this.d(track([this._innerViewer, 'editingHeightOffset'], [this, 'editingHeightOffset']));
        this.d(track([this._innerViewer, 'terrainShader'], [this, 'terrainShader']));
        // this.d(bind([this, 'sun'], [this._innerViewer, 'sun']));
        // this.d(bind([this, 'moon'], [this._innerViewer, 'moon']));

        //基础属性
        this.d(bind([this._innerViewer, 'execOnceFuncStr'], [this, 'execOnceFuncStr']));
        this.d(bind([this._innerViewer, 'updateFuncStr'], [this, 'updateFuncStr']));
        this.d(bind([this._innerViewer, 'toDestroyFuncStr'], [this, 'toDestroyFuncStr']));
        this.d(bind([this._innerViewer, 'name'], [this, 'name']));
        this.d(bind([this._innerViewer, 'ref'], [this, 'ref']));
        this.d(bind([this._innerViewer, 'devTags'], [this, 'devTags']));
    }
}
