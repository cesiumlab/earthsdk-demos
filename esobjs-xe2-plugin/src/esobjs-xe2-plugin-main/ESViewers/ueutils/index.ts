import { UeCloudViewerBase } from "xbsj-xe2/dist-node/xe2-ue-objects/base/UeViewer/UeCloudViewerBase";

export type ComputeVisibilityResult = {
    params: { boundingVolume: { center: [number, number, number], radius: number } },
    result: {
        re: {
            result: 1 | 2 | 3
        },
        error: string | undefined;
    }
}
export const computeVisibilityCallFunc = async (viewer: UeCloudViewerBase, boundingVolume: ComputeVisibilityResult['params']['boundingVolume']) => {
    const res = await viewer.callUeFunc<ComputeVisibilityResult['result']>({
        f: 'computeVisibility',
        p: { boundingVolume }
    })
    if (res.error || !res.re) {
        console.error(`computeVisibility:`, res.error)
        return undefined;
    }
    const list = ['outside', 'intersecting', 'inside'] as const;
    return list[res.re.result - 1];
}
