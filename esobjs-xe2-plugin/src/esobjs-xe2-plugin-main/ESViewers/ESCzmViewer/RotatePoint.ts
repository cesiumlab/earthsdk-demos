import { Destroyable, react, reactArrayWithUndefined, track } from "xbsj-renderer/dist-node/xr-base-utils";
import { ESCzmViewer } from ".";
import { ObjResettingWithEvent } from "xbsj-renderer/dist-node/xr-utils";
import { InnerRotatePoint } from "./InnerRotatePoint";
import * as Cesium from 'cesium';

class InnerRotatePointWrapper extends Destroyable {
    get owner() { return this._owner; }
    private _inner = this.dv(new InnerRotatePoint(this.owner.eSCzmViewer.innerViewer.viewer as Cesium.Viewer));
    get inner() { return this._inner; }

    constructor(private _owner: RotatePoint) {
        super();

        {
            const update = () => {
                this.inner.distance = this.owner.distance;
                this.inner.cycle = this.owner.cycle;
                this.inner.position = this.owner.position;
                this.inner.heading = this.owner.heading;
                this.inner.pitch = this.owner.pitch;
            };
            update();
            this.d(this.owner.distanceChanged.disposableOn(update));
            this.d(this.owner.cycleChanged.disposableOn(update));
            this.d(this.owner.positionChanged.disposableOn(update));
            this.d(this.owner.headingChanged.don(update));
            this.d(this.owner.pitchChanged.don(update));
        }
    }
}

export class RotatePoint extends Destroyable {
    get eSCzmViewer() { return this._eSCzmViewer; }
    /**
     * 要环绕的点
     * @type {[number,number,number]|undefined}
     * @instance
     * @default undefined
     * @memberof RotatePoint
     */
    private _position = this.dv(reactArrayWithUndefined<[number, number, number]>(undefined));
    get position() { return this._position.value; }
    set position(value: [number, number, number] | undefined) { this._position.value = value; }
    get positionChanged() { return this._position.changed; }
    /**
     * 相机距离点的距离
     * @type {number}
     * @instance
     * @default 50000
     * @memberof RotatePoint
     */
    private _distance = this.dv(react<number>(50000));
    get distance() { return this._distance.value; }
    set distance(value: number) { this._distance.value = value; }
    get distanceChanged() { return this._distance.changed; }

    /**
     * 飞行一圈的周期，单位为秒
     * @type {number}
     * @instance
     * @default 60
     * @memberof RotatePoint
     */
    private _cycle = this.dv(react<number>(60));
    get cycle() { return this._cycle.value; }
    set cycle(value: number) { this._cycle.value = value; }
    get cycleChanged() { return this._cycle.changed; }

    /**
     * 相机偏航角
     * @type {number}
     * @instance
     * @default 0°
     * @memberof RotatePoint
     */
    private _heading = this.dv(react<number>(0));
    get heading() { return this._heading.value; }
    set heading(value: number) { this._heading.value = value; }
    get headingChanged() { return this._heading.changed; }

    /**
     * 相机俯仰角
     * @type {number}
     * @instance
     * @default -30°
     * @memberof RotatePoint
     */
    private _pitch = this.dv(react<number>(-30));
    get pitch() { return this._pitch.value; }
    set pitch(value: number) { this._pitch.value = value; }
    get pitchChanged() { return this._pitch.changed; }

    private _inner = this.dv(new ObjResettingWithEvent(this._eSCzmViewer.innerViewer.viewerChanged, () => {
        if (!this._eSCzmViewer.innerViewer.viewer) return undefined;
        return new InnerRotatePointWrapper(this);
    }));

    get inner() { return this._inner; }

    /**
     * 启动环绕地球
     * @returns 
     */
    start() {
        return this._inner.obj?.inner.start();
    }

    /**
     * 取消环绕地球
     * @returns 
     */
    cancel() {
        return this._inner.obj?.inner.cancel();
    }

    constructor(private _eSCzmViewer: ESCzmViewer) {
        super();
    }
}
