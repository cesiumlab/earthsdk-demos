import { ESSceneObject, GeoCameraController, GeoPolylinePathCameraController, GeoSceneObjectCameraController, getDistancesFromPositions, getIncludedAngleFromPositions, SceneObject } from "xbsj-xe2/dist-node/xe2-base-objects";
import { bind, track } from "xbsj-xe2/dist-node/xe2-base-utils";
import { CzmBackLayerCloudCollection, CzmCzmCustomPrimitive, CzmCzmModelPrimitive, CzmDepthOfFieldPostProcess, CzmRainPostProcess, CzmSnowPostProcess, CzmViewer } from 'xbsj-xe2/dist-node/xe2-cesium-objects';
import { ESObjectsManager } from '../../ESObjectsManager';
import { ESNavigationMode, ESViewer } from '../ESViewer';
import { cloudCollection } from '../data';
import { RotateGlobe } from "./RotateGlobe";
import * as Cesium from 'cesium';
import { toCartesian3, toColor } from "xbsj-renderer/dist-node/xr-cesium";
import { ESCzmViewerOptions } from "../../ESObjectsManager/utils";
import { RotatePoint } from "./RotatePoint";
import { CzmESObjectWithLocation } from "@/esobjs-xe2-plugin-main/esobjs";
import { getCav } from "@/esobjs-xe2-plugin-main/esobjs/general/czm/base";


/**
 * https://www.wolai.com/earthsdk/vsNUdU6avwV6wiVeSDH7nC
 * https://c0yh9tnn0na.feishu.cn/docx/I1KidukHko5HKtxO76bcAS1inKO
 */
export class ESCzmViewer extends ESViewer {
    private _handler?: Cesium.ScreenSpaceEventHandler;
    private _innerViewer = this.objectManager.createSceneObjectFromClass(CzmViewer) as CzmViewer;
    private _innerViewerInit = (() => {
        // @ts-ignore
        this._innerViewer.__esCzmViewer__ = this;
    })();
    static getESCzmViewer(viewer: CzmViewer): ESCzmViewer {
        // @ts-ignore
        return viewer.__esCzmViewer__;
    }
    private _createCesiumViewerFuncDispose = (() => {
        const czmOptions = this._options?.czmOptions;
        if (!czmOptions) return;
        const defaultOptions = {
            animation: false,
            baseLayerPicker: false,
            fullscreenButton: false,
            geocoder: false,
            homeButton: false,
            infoBox: false,
            sceneModePicker: false,
            selectionIndicator: false,
            timeline: false,
            navigationHelpButton: false,
            navigationInstructionsInitiallyVisible: false,
            scene3DOnly: true,
            //@ts-ignore
            msaaSamples: this._innerViewer?.viewer?.msaaSamples,
        }
        const funcStr = `\
async function initCesiumViewer(container, czmViewer) {
    // 创建viewer
    const viewer = new Cesium.Viewer(container, ${JSON.stringify({ ...defaultOptions, ...czmOptions }, null, 4)});
    // 不知道从哪个版本开始,Cesium默认会增加Ion影像!
    viewer.imageryLayers.removeAll();
    viewer.extend(Cesium.viewerCesiumInspectorMixin);
    viewer.cesiumInspector.container.style.display = 'none';
    viewer.extend(Cesium.viewerCesium3DTilesInspectorMixin);
    viewer.cesium3DTilesInspector.container.style.display = 'none';
     // 避免初始就能拾取瓦片 
    viewer.cesium3DTilesInspector.viewModel.picking = false;
    viewer.clock.currentTime = Cesium.JulianDate.fromDate(new Date('2022-04-19T20:00:53.10067292911116965Z'));
    // 原始设置
    viewer.scene.screenSpaceCameraController.lookEventTypes = [{ eventType: Cesium.CameraEventType.RIGHT_DRAG }, { eventType: Cesium.CameraEventType.RIGHT_DRAG, modifier: Cesium.KeyboardEventModifier.SHIFT }];
    viewer.scene.screenSpaceCameraController.zoomEventTypes = [Cesium.CameraEventType.WHEEL, Cesium.CameraEventType.PINCH]
    // 去除系统默认事件
    viewer.screenSpaceEventHandler.removeInputAction(Cesium.ScreenSpaceEventType.LEFT_CLICK);
    viewer.screenSpaceEventHandler.removeInputAction(Cesium.ScreenSpaceEventType.LEFT_DOUBLE_CLICK);
    return viewer;
}
`
        this.debug && console.log('createCesiumViewerFuncStr', funcStr)
        this._innerViewer.createCesiumViewerFuncStr = funcStr;
    })()
    private _innerViewerDispose = this.d(() => this.objectManager.destroySceneObject(this._innerViewer));
    get innerViewer() { return this._innerViewer; }

    get depthTestAgainstTerrain() { return this._innerViewer.sceneGlobeDepthTestAgainstTerrain ?? false; }
    set depthTestAgainstTerrain(value: boolean) { this._innerViewer.sceneGlobeDepthTestAgainstTerrain = value; }
    get depthTestAgainstTerrainChanged() { return this._innerViewer.sceneGlobeDepthTestAgainstTerrainChanged; }

    //事件////////////////////////////////////////
    private _syncEventDis = this.d(this.viewerChanged.don((v) => {
        this.syncEvent.emit()
        this.innerViewer.viewer && (this.innerViewer.viewer.scene.globe.baseColor = toColor([1, 1, 1, 1]));
    }))

    private _hoverEventDispose = (() => {
        this.d(this.innerViewer.interaction.pointerHover.hoverEvent.don((el) => {
            this.hoverEvent.emit({
                screenPosition: [el.offsetX, el.offsetY],
                mouseEvent: el
            })
        }))
    })()
    private _hoverOutEventDispose = (() => {
        this.d(this.innerViewer.interaction.pointerHover.hoverOutEvent.don(() => {
            this.hoverOutEvent.emit()
        }))
    })()
    private _mouseMoveEventDispose = (() => {
        this.d(this.innerViewer.interaction.mouseMoveEvent.don((el) => {
            this.mouseMoveEvent.emit({
                screenPosition: [el.offsetX, el.offsetY],
                mouseEvent: el
            })
        }))
    })()
    private _clickEventDispose = (() => {
        this.d(this.innerViewer.interaction.pointerClick.clickEvent.don((el) => {
            this.clickEvent.emit({
                screenPosition: [el.offsetX, el.offsetY],
                mouseEvent: el
            })
        }))
    })()
    private _dbclickEventDispose = (() => {
        this.d(this.innerViewer.interaction.pointerClick.dbclickEvent.don((el) => {
            this.dbclickEvent.emit({
                screenPosition: [el.offsetX, el.offsetY],
                mouseEvent: el
            })
        }))
    })()
    //TODO
    private _pickedEventDispose = (() => {
        this.d(this.innerViewer.czmPickedEvent.don((el) => {
            this.debug && console.log('pickedEvent', el)
            el && this.pickedEvent.emit(el)
        }))
    })()

    private _viewerChanged = (() => {
        const update = () => {
            //@ts-ignore
            const cesiumv = `Cesium  ${Cesium.VERSION ?? '未知版本'} `;
            this._versions.push(cesiumv)
        };
        this.d(this.innerViewer.viewerChanged.donce(update));
    })();
    ////////////////////////////////////////////

    changeContainer(options: ESCzmViewerOptions) {
        let divBox: HTMLElement;
        if (typeof options.domid === 'string') {
            const box = document.getElementById(options.domid);
            if (!box) {
                console.error('未找到id为' + options.domid + '的元素');
                return;
            };
            divBox = box;
        } else {
            divBox = options.domid;
        }
        divBox.innerHTML = '';
        // 创建一个空的div容器
        const container = document.createElement('div');
        // container.style.width = '100%';
        // container.style.height = '100%';
        // container.style.margin = '0px';
        // container.style.padding = '0px';
        // container.style.border = 'none';
        // container.style.overflow = 'hidden';
        // container.style.position = 'relative';
        // container.style.zIndex = '0';
        // container.style.background = 'rgba(0,0,0,0)';
        container.style.cssText = `width: 100%; height: 100%; margin: 0px; padding: 0px; border: none; overflow: hidden; position: relative; z-index: 0; background: rgba(0,0,0,0);`
        divBox.appendChild(container);
        this._innerViewer.containerOrId = container;
    }

    // private changeNavigationMode() {

    // }
    // 清空已有漫游
    private _clearRoaming(currentMode: ESNavigationMode) {
        this._navigationMode.value = currentMode;
        // 沿线漫游
        this._geoPolylinePathCameraController.enabled = false;
        this._geoPolylinePathCameraController.playing = false;
        this._geoPolylinePathCameraController.show = false;
        this._geoPolylinePathCameraController.currentTime = 0;
        // 绕球旋转
        this._rotateGlobe.cancel();
        // 绕点旋转
        this._rotatePoint.cancel();
        // 第一人称漫游
        this.innerViewer.firstPersonKeyboardEnabled = false;
        //跟踪模式
        this._followMode.enabled = false;
        this._followMode.sceneObjectId = undefined;
        this._followMode.offsetRotation = [0, 0, 0];
        this._followMode.viewDistance = 0;
    }
    //沿线漫游
    private _geoPolylinePathCameraController = this.dv(new GeoPolylinePathCameraController());
    //环绕旋转
    private _rotateGlobe = this.dv(new RotateGlobe(this));
    // 绕点旋转
    private _rotatePoint = this.dv(new RotatePoint(this));
    // 跟随模式
    private _followMode = this.dv(new GeoSceneObjectCameraController());

    override changeToWalk(position: [number, number, number]) {
        this._clearRoaming('Walk');
        this._changedMouseEvent("Walk");
        // 飞行到坐标位置
        const res = this.innerViewer.getCameraInfo()
        if (!res) return
        const ro = [res.rotation[0], 0, 0] as [number, number, number]
        this.innerViewer.flyTo(position, undefined, ro)
        // 开启第一人称漫游
        this.innerViewer.firstPersonKeyboardEnabled = true
        this.innerViewer.firstPersonWalkingSpeed = 0.006 //6m/s
        this.innerViewer.firstPersonKeyStatusMap = {
            "ShiftLeft": "WithCamera",
            "ShiftRight": "WithCamera",
            "KeyW": "MoveForward",
            "KeyS": "MoveBackword",
            "KeyA": "MoveLeft",
            "KeyD": "MoveRight",
            "ArrowUp": "MoveForward",
            "ArrowDown": "MoveBackword",
            "ArrowLeft": "RotateLeft",
            "ArrowRight": "RotateRight",
            "KeyR": "SpeedUp",
            "KeyF": "SpeedDown",
            "KeyQ": "SwitchAlwaysWithCamera"
        }
    }
    override changeToMap() {
        this._clearRoaming('Map');
        this._changedMouseEvent('Map');
    }
    override changeToRotateGlobe(latitude?: number, height?: number, cycleTime?: number) {
        this._clearRoaming('RotateGlobe');
        this._changedMouseEvent('RotateGlobe');
        this._rotateGlobe.latitude = latitude ?? 38.0;
        this._rotateGlobe.height = height ?? 10000000;
        this._rotateGlobe.cycle = cycleTime ?? 60;
        this._rotateGlobe.start()
        // 清除类内部绑定的左键落下事件
        this._rotateGlobe.inner.obj?.inner.sseh.removeInputAction(Cesium.ScreenSpaceEventType.LEFT_DOWN);
    }
    override changeToLine(geoLineStringId: string, speed: number = 10, heightOffset: number = 10, loop: boolean = true, turnRateDPS: number = 10, lineMode: "auto" | "manual" = "auto") {
        this._clearRoaming('Line');
        this._changedMouseEvent('Line');

        const _this = this;
        const lineModeIsAuto = lineMode === "auto";
        const sceneObject = ESSceneObject.getSceneObjById(geoLineStringId) as ESSceneObject | undefined;
        if (!sceneObject) return;
        // @ts-ignore
        const { points } = sceneObject;
        if (!points) return;
        // 获取线段长度
        const distances = getDistancesFromPositions(points, 'NONE');
        const totalDistance = distances[distances.length - 1];
        // 修改默认视距
        this._geoPolylinePathCameraController.viewDistance = 0;
        this._geoPolylinePathCameraController.positions = [...points]
        this._geoPolylinePathCameraController.geoPolylinePath.duration = totalDistance / speed * 1000;
        this._geoPolylinePathCameraController.loop = loop;
        this._geoPolylinePathCameraController.offsetHeight = heightOffset;
        this._geoPolylinePathCameraController.show = false;
        // 计算每个拐弯角总的拐弯距离
        const includedAngles = getIncludedAngleFromPositions(points);
        this._geoPolylinePathCameraController.rotationRadius = !lineModeIsAuto || includedAngles == false ? [0] : includedAngles.map((item: number) => (item / turnRateDPS) * speed);
        // 鼠标控制
        this._geoPolylinePathCameraController.enabledRotationInput = !lineModeIsAuto;
        this._geoPolylinePathCameraController.enabledScaleInput = false;
        this._geoPolylinePathCameraController.playing = lineModeIsAuto;
        if (lineModeIsAuto) {
            this._geoPolylinePathCameraController.offsetRotation = [-90, 0, 0];
            document.removeEventListener('keydown', (e) => {
                lineKeyEvent(e);
            })
            document.removeEventListener('keyup', (e) => {
                lineKeyEvent(e);
            })
        } else {
            document.addEventListener('keydown', (e) => {
                lineKeyEvent(e);
            })
            document.addEventListener('keyup', (e) => {
                lineKeyEvent(e);
            })
        }
        // 鼠标事件
        function lineKeyEvent(e: KeyboardEvent) {
            if (e.key === 'w' || e.key === 's') {
                _this._geoPolylinePathCameraController.playing = e.type === 'keydown';
                _this._geoPolylinePathCameraController.speed = e.key === 'w' || e.type === "keyup" ? 1 : -1;
            }
        }
        if (!this._geoPolylinePathCameraController.enabled) {
            this._geoPolylinePathCameraController.enabled = true;
        }
    }
    override changeToUserDefined(userDefinedPawn: string) {
        this._clearRoaming("UserDefined");
        this._changedMouseEvent("UserDefined");
        this.changeToMap()
        console.warn('Cesium引擎暂不支持自定义漫游,已切换为Map模式', userDefinedPawn)
    }

    override changeToRotatePoint(position: [number, number, number], distance: number = 50000, orbitPeriod: number = 60, heading: number = 0, pitch: number = -30) {
        this._clearRoaming("RotatePoint");
        this._changedMouseEvent("RotatePoint");
        this._rotatePoint.position = position;
        this._rotatePoint.distance = distance;
        this._rotatePoint.cycle = orbitPeriod;
        this._rotatePoint.heading = heading;
        this._rotatePoint.pitch = pitch;
        this._rotatePoint.start()
        // 清除类内部绑定的左键落下事件
        this._rotatePoint.inner.obj?.inner.sseh.removeInputAction(Cesium.ScreenSpaceEventType.LEFT_DOWN);
    }
    override changeToFollow(objectId: string, distance: number = 0, heading: number = 0, pitch: number = -30, relativeRotation: boolean = true) {
        this._clearRoaming("Follow");
        this._changedMouseEvent("Follow");

        if (distance != 0) {
            this._followMode.viewDistance = distance;
        } else {
            const esObject = SceneObject.getSceneObjById(objectId); // 获取ES对象
            if (!esObject) {
                console.warn("未获取到ES对象，请检查输入的ES对象ID是否正常");
                return;
            }
            let czmObject = this.innerViewer.getCzmObject(esObject);
            //@ts-ignore
            if (!czmObject || !(czmObject instanceof CzmESObjectWithLocation) || !(czmObject.czmModelPrimitive || czmObject.czmCustomPrimitive)) {
                console.warn("无法获取内部对象包围盒，请检查输入的ES对象ID是否正常");
                return;
            }
            //@ts-ignore
            let czmPrimitive = this.innerViewer.getCzmObject(czmObject.czmModelPrimitive ?? czmObject.czmCustomPrimitive) as CzmCzmCustomPrimitive | CzmCzmModelPrimitive;
            const cav = getCav(this.innerViewer, czmPrimitive);
            if (cav == undefined) return;
            this._followMode.viewDistance = cav[1] * 3;
        }
        this._followMode.enabled = true;
        this._followMode.sceneObjectId = objectId;
        this._followMode.offsetRotation = [heading, pitch, 0];
        this._followMode.relativeRotation = relativeRotation;
    }
    // 更改鼠标事件
    private _changedMouseEvent(currentMode?: ESNavigationMode) {
        if (!this._handler && this.innerViewer.viewer) {
            this._handler = new Cesium.ScreenSpaceEventHandler(this.innerViewer.viewer.scene.canvas);
        }
        const handler = this._handler;
        if (this.innerViewer.viewer?.scene && this.innerViewer.viewer.scene.screenSpaceCameraController) {
            let screenSpaceCameraController = this.innerViewer.viewer.scene.screenSpaceCameraController;
            this.d(() => {
                handler && clearHandler(handler)
            })
            if (currentMode == 'Walk' || currentMode == 'RotateGlobe') {
                screenSpaceCameraController.lookEventTypes = undefined;
                screenSpaceCameraController.rotateEventTypes = undefined;
                screenSpaceCameraController.tiltEventTypes = undefined;
                screenSpaceCameraController.zoomEventTypes = undefined;
                screenSpaceCameraController.translateEventTypes = undefined;
                if (handler && currentMode == 'Walk') {
                    handler.setInputAction((event: any) => {
                        handler.setInputAction((eventMove: any) => {
                            if (this.innerViewer.viewer)
                                look3D(this.innerViewer.viewer.scene, eventMove, screenSpaceCameraController);
                        }, Cesium.ScreenSpaceEventType.MOUSE_MOVE);
                        // 清除事件
                        handler.setInputAction((event: any) => {
                            handler.removeInputAction(Cesium.ScreenSpaceEventType.MOUSE_MOVE);
                            handler.removeInputAction(Cesium.ScreenSpaceEventType.LEFT_UP);
                        }, Cesium.ScreenSpaceEventType.LEFT_UP);
                    }, Cesium.ScreenSpaceEventType.LEFT_DOWN);
                    handler.setInputAction((event: any) => {
                        handler.setInputAction((eventMove: any) => {
                            if (this.innerViewer.viewer)
                                look3D(this.innerViewer.viewer.scene, eventMove, screenSpaceCameraController);
                        }, Cesium.ScreenSpaceEventType.MOUSE_MOVE);
                        // 清除事件
                        handler.setInputAction((event: any) => {
                            handler.removeInputAction(Cesium.ScreenSpaceEventType.MOUSE_MOVE);
                            handler.removeInputAction(Cesium.ScreenSpaceEventType.RIGHT_UP);
                        }, Cesium.ScreenSpaceEventType.RIGHT_UP);
                    }, Cesium.ScreenSpaceEventType.RIGHT_DOWN);
                    handler.setInputAction((event: any) => {
                        handler.setInputAction((eventMove: any) => {
                            if (this.innerViewer.viewer)
                                look3D(this.innerViewer.viewer.scene, eventMove, screenSpaceCameraController);
                        }, Cesium.ScreenSpaceEventType.MOUSE_MOVE);
                        // 清除事件
                        handler.setInputAction((event: any) => {
                            handler.removeInputAction(Cesium.ScreenSpaceEventType.MOUSE_MOVE);
                            handler.removeInputAction(Cesium.ScreenSpaceEventType.MIDDLE_UP);
                        }, Cesium.ScreenSpaceEventType.MIDDLE_UP);
                    }, Cesium.ScreenSpaceEventType.MIDDLE_DOWN);
                }
            } else {
                handler && clearHandler(handler);
                screenSpaceCameraController.lookEventTypes = Cesium.CameraEventType.RIGHT_DRAG;
                screenSpaceCameraController.rotateEventTypes = Cesium.CameraEventType.LEFT_DRAG;
                screenSpaceCameraController.tiltEventTypes = [
                    Cesium.CameraEventType.MIDDLE_DRAG, Cesium.CameraEventType.PINCH,
                    { eventType: Cesium.CameraEventType.LEFT_DRAG, modifier: Cesium.KeyboardEventModifier.CTRL },
                    { eventType: Cesium.CameraEventType.RIGHT_DRAG, modifier: Cesium.KeyboardEventModifier.CTRL }
                ]
                screenSpaceCameraController.translateEventTypes = Cesium.CameraEventType.LEFT_DRAG;
                screenSpaceCameraController.zoomEventTypes = [
                    Cesium.CameraEventType.WHEEL, Cesium.CameraEventType.PINCH,
                    { eventType: Cesium.CameraEventType.RIGHT_DRAG, modifier: Cesium.KeyboardEventModifier.SHIFT }
                ]
            }
        }
    }

    async computeVisibility(center: [number, number, number], radius: number) {
        if (!this._innerViewer.viewer) return undefined;
        const { scene } = this._innerViewer.viewer;
        const centerCartesian = toCartesian3(center);
        const boundingSphere = new Cesium.BoundingSphere(centerCartesian, radius);
        const frustum = scene.camera.frustum;
        const cullingVolume = frustum.computeCullingVolume(scene.camera.position, scene.camera.direction, scene.camera.up);
        const visibility = cullingVolume.computeVisibility(boundingSphere);
        if (visibility === Cesium.Intersect.OUTSIDE) {
            return "outside"
        } else if (visibility === Cesium.Intersect.INTERSECTING) {
            return "intersecting"
        } else if (visibility === Cesium.Intersect.INSIDE) {
            return "inside"
        } else {
            return undefined
        }
    }

    constructor(objectManager: ESObjectsManager, private _options: ESCzmViewerOptions) {
        super(objectManager, _options.id, 'ESCzmViewer');
        //div挂载
        this.changeContainer(this._options)

        this.innerViewer.add(this._geoPolylinePathCameraController);
        this.d(() => this.innerViewer.delete(this._geoPolylinePathCameraController));

        this.innerViewer.add(this._followMode);
        this.d(() => this.innerViewer.delete(this._followMode));

        //基础TODO
        this.d(track([this._innerViewer, 'show'], [this, 'show']));
        this.d(track([this._innerViewer, 'ionAccessToken'], [this, 'ionAccessToken']));
        this.d(track([this._innerViewer, 'sceneGlobeShow'], [this, 'globeShow']));

        //环境TODO
        this.d(track([this._innerViewer, 'clockCurrentTime'], [this, 'currentTime']));
        this.d(track([this._innerViewer, 'sceneSkyAtmosphereShow'], [this, 'atmosphere']));
        this.d(track([this._innerViewer, 'sceneMoonShow'], [this, 'moon']));

        {//太阳
            const update = () => {
                this.innerViewer.sceneSunShow = this.sun;
                this._innerViewer.sceneGlobeEnableLighting = this.sun;//全球光照
            }
            update();
            this.d(this.sunChanged.don(update));
        }

        {//雾fog 
            const update = () => {
                if (this.fog <= 0) {
                    this.innerViewer.sceneFogEnabled = false
                } else
                    this.innerViewer.sceneFogEnabled = true
            }
            update();
            this.d(this.fogChanged.don(update));
        }

        {//云cloud
            const czmBackLayerCloudCollection = this.dv(new CzmBackLayerCloudCollection())
            this.innerViewer.add(czmBackLayerCloudCollection)
            this.d(() => this.innerViewer.delete(czmBackLayerCloudCollection))

            czmBackLayerCloudCollection.option = cloudCollection;
            czmBackLayerCloudCollection.show = false

            const update = () => {
                if (this.cloud <= 0) {
                    czmBackLayerCloudCollection.show = false
                } else {
                    czmBackLayerCloudCollection.show = true
                }
            }
            update();
            this.d(this.cloudChanged.don(update));
        }

        {//雨 rain
            const czmRainPostProcess = this.dv(new CzmRainPostProcess())
            this.innerViewer.add(czmRainPostProcess)
            this.d(() => this.innerViewer.delete(czmRainPostProcess))
            czmRainPostProcess.show = false
            const update = () => {
                if (this.rain <= 0) {
                    czmRainPostProcess.show = false
                } else {
                    czmRainPostProcess.show = true
                }
            }
            update();
            this.d(this.rainChanged.don(update));
        }
        {//雪 snow
            const czmSnowPostProcess = this.dv(new CzmSnowPostProcess())
            this.innerViewer.add(czmSnowPostProcess)
            this.d(() => this.innerViewer.delete(czmSnowPostProcess))
            czmSnowPostProcess.show = false
            const update = () => {
                if (this.snow <= 0) {
                    czmSnowPostProcess.show = false
                } else {
                    czmSnowPostProcess.show = true
                }
            }
            update();
            this.d(this.snowChanged.don(update));
        }

        {//景深
            const czmDepthOfField = this.dv(new CzmDepthOfFieldPostProcess())
            this.innerViewer.add(czmDepthOfField)
            this.d(() => this.innerViewer.delete(czmDepthOfField))
            czmDepthOfField.show = false
            const update = () => {
                if (this.depthOfField <= 0) {
                    czmDepthOfField.show = false
                } else {
                    czmDepthOfField.show = true
                }
            }
            update();
            this.d(this.depthOfFieldChanged.don(update));
        }

        this.d(track([this._innerViewer, 'lonLatFormat'], [this, 'lonLatFormat']));
        this.d(track([this._innerViewer, 'textAvoidance'], [this, 'textAvoidance']));
        this.d(track([this._innerViewer, 'sceneCameraFrustumFov'], [this, 'fov']));
        this.d(track([this._innerViewer, 'flyToBoundingSize'], [this, 'flyToBoundingSize']));
        this.d(track([this._innerViewer, 'editingHeightOffset'], [this, 'editingHeightOffset']));
        this.d(track([this._innerViewer, 'terrainShader'], [this, 'terrainShader']));

        //基础属性
        this.d(bind([this._innerViewer, 'execOnceFuncStr'], [this, 'execOnceFuncStr']));
        this.d(bind([this._innerViewer, 'updateFuncStr'], [this, 'updateFuncStr']));
        this.d(bind([this._innerViewer, 'toDestroyFuncStr'], [this, 'toDestroyFuncStr']));
        this.d(bind([this._innerViewer, 'name'], [this, 'name']));
        this.d(bind([this._innerViewer, 'ref'], [this, 'ref']));
        this.d(bind([this._innerViewer, 'devTags'], [this, 'devTags']));

        //编辑TODO
        // this.d(bind([this, 'editingPointSize'], [this._innerViewer, 'editingPointSize']));
        // this.d(bind([this, 'editingPointColor'], [this._innerViewer, 'editingPointColor']));
        // this.d(bind([this, 'editingAuxiliaryPointColor'], [this._innerViewer, 'editingAuxiliaryPointColor']));
        // this.d(bind([this, 'editingLineWidth'], [this._innerViewer, 'editingLineWidth']));
        // this.d(bind([this, 'editingLineColor'], [this._innerViewer, 'editingLineColor']));
        // this.d(bind([this, 'editingAxisSize'], [this._innerViewer, 'editingAxisSize']));
        // this.d(bind([this, 'editingAuxiliaryPointSize'], [this._innerViewer, 'editingAuxiliaryPointSize']));

        // 自动曝光
        // this.d(track([this._innerViewer,'autoExposure'],[this,'autoExposure']));
        // this.d(track([this._innerViewer, 'xbsjLocalBoxSources'], [this, 'skybox']));

        // this.d(bind([this, 'hoverTime'], [this._innerViewer, 'hoverTime']));
        // this.d(track([this._innerViewer, 'debug'],[this, 'debug']));

    }
}

function look3D(_scene: Cesium.Scene, movement: any, screenSpaceCameraController: Cesium.ScreenSpaceCameraController, rotationAxis: Cesium.Cartesian3 | undefined = undefined) {
    const scene = _scene;
    const camera = scene.camera;

    const startPos = new Cesium.Cartesian2();
    startPos.x = movement.startPosition.x;
    startPos.y = 0.0;
    const endPos = new Cesium.Cartesian2();
    endPos.x = movement.endPosition.x;
    endPos.y = 0.0;

    let startRay = camera.getPickRay(startPos, new Cesium.Ray());
    let endRay = camera.getPickRay(endPos, new Cesium.Ray());
    let angle = 0.0;
    let start;
    let end;
    if (!startRay || !endRay) return;

    if (camera.frustum instanceof Cesium.OrthographicFrustum) {
        start = startRay.origin;
        end = endRay.origin;

        Cesium.Cartesian3.add(camera.direction, start, start);
        Cesium.Cartesian3.add(camera.direction, end, end);

        Cesium.Cartesian3.subtract(start, camera.position, start);
        Cesium.Cartesian3.subtract(end, camera.position, end);

        Cesium.Cartesian3.normalize(start, start);
        Cesium.Cartesian3.normalize(end, end);
    } else {
        start = startRay.direction;
        end = endRay.direction;
    }

    let dot = Cesium.Cartesian3.dot(start, end);
    if (dot < 1.0) {
        // dot is in [0, 1]
        angle = Math.acos(dot);
    }

    angle = movement.startPosition.x > movement.endPosition.x ? angle : -angle;

    //@ts-ignore
    const horizontalRotationAxis = screenSpaceCameraController._horizontalRotationAxis;
    if (Cesium.defined(rotationAxis)) {
        camera.look(rotationAxis, -angle);
    } else if (Cesium.defined(horizontalRotationAxis)) {
        camera.look(horizontalRotationAxis, -angle);
    } else {
        camera.lookLeft(angle);
    }

    startPos.x = 0.0;
    startPos.y = movement.startPosition.y;
    endPos.x = 0.0;
    endPos.y = movement.endPosition.y;

    startRay = camera.getPickRay(startPos, new Cesium.Ray());
    endRay = camera.getPickRay(endPos, new Cesium.Ray());
    angle = 0.0;
    if (!startRay || !endRay) return;

    if (camera.frustum instanceof Cesium.OrthographicFrustum) {
        start = startRay.origin;
        end = endRay.origin;

        Cesium.Cartesian3.add(camera.direction, start, start);
        Cesium.Cartesian3.add(camera.direction, end, end);

        Cesium.Cartesian3.subtract(start, camera.position, start);
        Cesium.Cartesian3.subtract(end, camera.position, end);

        Cesium.Cartesian3.normalize(start, start);
        Cesium.Cartesian3.normalize(end, end);
    } else {
        start = startRay.direction;
        end = endRay.direction;
    }

    dot = Cesium.Cartesian3.dot(start, end);
    if (dot < 1.0) {
        // dot is in [0, 1]
        angle = Math.acos(dot);
    }
    angle = movement.startPosition.y > movement.endPosition.y ? angle : -angle;

    rotationAxis = Cesium.defaultValue(rotationAxis, horizontalRotationAxis);
    if (Cesium.defined(rotationAxis)) {
        const direction = camera.direction;
        const negativeRotationAxis = Cesium.Cartesian3.negate(
            rotationAxis,
            new Cesium.Cartesian3()
        );
        const northParallel = Cesium.Cartesian3.equalsEpsilon(
            direction,
            rotationAxis,
            Cesium.Math.EPSILON2
        );
        const southParallel = Cesium.Cartesian3.equalsEpsilon(
            direction,
            negativeRotationAxis,
            Cesium.Math.EPSILON2
        );
        if (!northParallel && !southParallel) {
            dot = Cesium.Cartesian3.dot(direction, rotationAxis);
            let angleToAxis = Cesium.Math.acosClamped(dot);
            if (angle > 0 && angle > angleToAxis) {
                angle = angleToAxis - Cesium.Math.EPSILON4;
            }

            dot = Cesium.Cartesian3.dot(direction, negativeRotationAxis);
            angleToAxis = Cesium.Math.acosClamped(dot);
            if (angle < 0 && -angle > angleToAxis) {
                angle = -angleToAxis + Cesium.Math.EPSILON4;
            }

            const tangent = Cesium.Cartesian3.cross(rotationAxis, direction, new Cesium.Cartesian3());
            camera.look(tangent, angle);
        } else if ((northParallel && angle < 0) || (southParallel && angle > 0)) {
            camera.look(camera.right, -angle);
        }
    } else {
        camera.lookUp(angle);
    }
}

function clearHandler(handler: Cesium.ScreenSpaceEventHandler) {
    handler.removeInputAction(Cesium.ScreenSpaceEventType.LEFT_DOWN);
    handler.removeInputAction(Cesium.ScreenSpaceEventType.RIGHT_DOWN);
    handler.removeInputAction(Cesium.ScreenSpaceEventType.MIDDLE_DOWN);
    handler.removeInputAction(Cesium.ScreenSpaceEventType.MOUSE_MOVE);
    handler.removeInputAction(Cesium.ScreenSpaceEventType.LEFT_UP);
    handler.removeInputAction(Cesium.ScreenSpaceEventType.RIGHT_UP);
    handler.removeInputAction(Cesium.ScreenSpaceEventType.MIDDLE_UP);
}

