import { Destroyable, react, track } from "xbsj-renderer/dist-node/xr-base-utils";
import { ESCzmViewer } from ".";
import { ObjResettingWithEvent } from "xbsj-renderer/dist-node/xr-utils";
import { InnerRotateGlobe } from "./InnerRotateGlobe";
import * as Cesium from 'cesium';

class InnerRotateGlobeWrapper extends Destroyable {
    get owner() { return this._owner; }
    private _inner = this.dv(new InnerRotateGlobe(this.owner.eSCzmViewer.innerViewer.viewer as Cesium.Viewer));
    get inner() { return this._inner; }

    constructor(private _owner: RotateGlobe) {
        super();

        {
            const update = () => {
                this._inner.height = this._owner.height;
                this._inner.cycle = this._owner.cycle;
                this._inner.latitude = this._owner.latitude * Math.PI / 180;
            };
            update();
            this.d(this._owner.heightChanged.disposableOn(update));
            this.d(this._owner.cycleChanged.disposableOn(update));
            this.d(this._owner.latitudeChanged.disposableOn(update));
        }
    }
}

export class RotateGlobe extends Destroyable {
    get eSCzmViewer() { return this._eSCzmViewer; }

    /**
     * 相机高度
     * @type {number}
     * @instance
     * @default 10000000
     * @memberof RotateGlobe
     */
    private _height = this.dv(react<number>(10000000));
    get height() { return this._height.value; }
    set height(value: number) { this._height.value = value; }
    get heightChanged() { return this._height.changed; }

    /**
     * 飞行一圈的周期，单位为秒
     * @type {number}
     * @instance
     * @default 60
     * @memberof RotateGlobe
     */
    private _cycle = this.dv(react<number>(60));
    get cycle() { return this._cycle.value; }
    set cycle(value: number) { this._cycle.value = value; }
    get cycleChanged() { return this._cycle.changed; }

    /**
     * 相机所在纬线高度 单位弧度
     * @type {number}
     * @instance
     * @default 北纬38°
     * @memberof RotateGlobe
     */
    private _latitude = this.dv(react<number>(38.0));
    get latitude() { return this._latitude.value; }
    set latitude(value: number) { this._latitude.value = value; }
    get latitudeChanged() { return this._latitude.changed; }

    private _inner = this.dv(new ObjResettingWithEvent(this._eSCzmViewer.innerViewer.viewerChanged, () => {
        if (!this._eSCzmViewer.innerViewer.viewer) return undefined;
        return new InnerRotateGlobeWrapper(this);
    }));

    get inner() { return this._inner; }

    /**
     * 启动环绕地球
     * @returns 
     */
    start() {
        return this._inner.obj?.inner.start();
    }

    /**
     * 取消环绕地球
     * @returns 
     */
    cancel() {
        return this._inner.obj?.inner.cancel();
    }

    constructor(private _eSCzmViewer: ESCzmViewer) {
        super();
    }
}
