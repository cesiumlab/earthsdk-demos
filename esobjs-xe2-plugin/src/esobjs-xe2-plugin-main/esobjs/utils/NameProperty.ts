import { ReactVarProperty } from "xbsj-xe2/dist-node/xe2-base-objects";
import { Property } from "xbsj-xe2/dist-node/xe2-base-objects";
export class NameProperty extends Property {
    constructor(name: string, description: string) {
        super(name, description);
    }
    get type() { return 'NameProperty'; }
}