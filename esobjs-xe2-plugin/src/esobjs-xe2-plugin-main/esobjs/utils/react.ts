import { reactDeepArrayWithUndefined } from "xbsj-xe2/dist-node/xe2-base-utils";
import { ESCameraViewCollection } from "../general";

export function reactPosition2Ds(defaultValue: [number, number][] | undefined) {
    return reactDeepArrayWithUndefined<[number, number]>(defaultValue, (a, b) => (a[0] === b[0] && a[1] === b[1]), s => [...s]);
}
export function map<T, R>(collection: Iterable<T>, func: (element: T) => R) {
    const a = new Array<R>();
    for (let element of collection) {
        a.push(func(element));
    }
    return a;
}
export function equalsN3(left: [number, number, number] | undefined, right: [number, number, number] | undefined) {
    if (left === undefined && right === undefined) return true;
    if (!left || !right) return false;
    return (left[0] === right[0] && left[1] === right[1] && left[2] === right[2]);
}

type ViewCollectionFunc = {
    playing: boolean,
    loop: boolean,
    appendView: () => void,
    insertView: (index: number) => void,
    stop: () => void,
    flyToPrevView: () => boolean,
    flyToNextView: () => boolean,
    deleteCurrentView: () => boolean
}

const defaultCssText = `\
position: absolute;
left: 50%;
bottom: 50px;
transform: translate(-50% , 0);
z-index: 100;
width:560px;
height:auto;
background-color:rgba(0, 33, 43,0.6);
padding: 10px;
border-radius: 8px;
\
`;

export const setFuncBtn = (sceneObject: ESCameraViewCollection, up: HTMLDivElement) => {
    // 功能按钮数组
    const vfc = [
        {
            title: '添加视角',
            content: '+',
            func: () => {
                sceneObject.addView()
            }
        },
        {
            title: '当前位置插入视角',
            content: '↓+',
            func: () => {
                sceneObject.insertView(sceneObject.currentViewIndex + 1)
            }
        },
        {
            title: '是否播放',
            content: (sceneObject.playing ?? false) ? '▼' : '〓',
            func: () => {
                sceneObject.playing = !sceneObject.playing
            }
        },
        {
            title: '停止',
            content: '■',
            func: () => {
                sceneObject.stop()
            }
        },
        {
            title: '是否循环',
            content: (sceneObject.loop ?? true) ? ESCameraViewCollection.defaults.loopOpen : ESCameraViewCollection.defaults.loopClose,
            func: () => {
                sceneObject.loop = !sceneObject.loop
            }
        },
        {
            title: '上一个视角',
            content: '↑',
            func: () => {
                sceneObject.flyToPrevView()
            }
        },
        {
            title: '下一个视角',
            content: '↓',
            func: () => {
                sceneObject.flyToNextView()
            }
        },
        {
            title: '删除当前视角',
            content: '-',
            func: () => {
                sceneObject.deleteCurrentView()
            }
        },
    ]
    for (let e of vfc) {

        // 每个按钮的默认样式
        const span = document.createElement('span')
        up.appendChild(span)
        span.style.cssText += `;
        width: 30px;
        height: 30px;
        line-height: 30px;
        font-size: 18px;
        cursor: pointer;
        border-radius: 3px;
        text-align: center;
        background-color: #383838;
        user-select: none;
        color:#ffffff;
        `

        // 部分按钮的样式
        if (e.title === '是否循环') {
            const loopImg = document.createElement('span')
            span.appendChild(loopImg)
            loopImg.style.cssText += `;
            display: block;
            width: 20px;
            height: 20px;
            margin: 5px auto;
            background: url(${sceneObject.transitionImageUrl(e.content)}) no-repeat;
            background-size: 100% 100%;
            `
            {
                const update = () => {
                    const url = (sceneObject.loop ?? true) ? ESCameraViewCollection.defaults.loopOpen : ESCameraViewCollection.defaults.loopClose
                    loopImg.style.background = `url(${sceneObject.transitionImageUrl(url)}) no-repeat`
                    loopImg.style.backgroundSize = '100% 100%'
                }
                update()

                sceneObject.dispose(sceneObject.loopChanged.disposableOn(update))
            }

        } else {
            span.innerText = e.content
        }
        if (e.title === '是否播放') {
            span.style.transform = 'rotate(-90deg)'
            {
                const update = () => {
                    const oc = (sceneObject.playing ?? false) ? '〓' : '▼'
                    span.innerText = oc
                }
                update()
                sceneObject.dispose(sceneObject.playingChanged.disposableOn(update))
            }
        }
        if (e.title === '停止') {
            span.style.cssText += `;
            line-height: 26px;
            font-size: 24px;
            `
        }

        // 按钮的title
        span.title = e.title

        // 给按钮绑定事件
        span.addEventListener('click', () => {
            if (typeof e.func === 'function') {
                e.func()
            }
        })
        span.addEventListener('mouseover', () => {
            span.style.backgroundColor = '#242424'
        })
        span.addEventListener('mouseout', () => {
            span.style.backgroundColor = '#383838'
        })
    }
}

export const setThumbnailList = (sceneObject: ESCameraViewCollection, bottom: HTMLDivElement) => {
    const update = () => {
        // 清空容器中的元素，重新赋值
        bottom.innerHTML = ''

        sceneObject.views.forEach((e, i) => {
            const box = document.createElement('div')
            bottom.appendChild(box)
            box.style.margin = '0 20px 5px 20px'
            box.style.width = '64px'
            box.style.height = '64px'
            const img = document.createElement('img')
            box.appendChild(img)

            // 选中的缩略图给边框
            const images = bottom.querySelectorAll('div')
            if (sceneObject.currentViewIndex === i) {
                images.forEach((g, i) => {
                    g.style.border = 'none'
                })
                box.style.border = '2px solid red'
            }

            // 绑定事件
            box.addEventListener('click', () => {
                sceneObject.setCurrentView(i)
                images.forEach((g, i) => {
                    g.style.border = 'none'
                })
                if (i === sceneObject.currentViewIndex) {
                    box.style.border = '2px solid red'
                }
            })
            box.addEventListener('dblclick', () => {
                sceneObject.flyToView(i)
            })

            // 如果缩略图被删除设置背景颜色
            if (!!e.thumbnail) {
                img.src = `${e.thumbnail}`
            } else {
                box.style.backgroundColor = '#e6e6e673'
            }
        })
    }
    update()
    sceneObject.dispose(sceneObject.viewsChanged.disposableOn(update))
    sceneObject.dispose(sceneObject.currentViewIndexChanged.disposableOn(update))
}
