
import { PositionEditing } from "xbsj-xe2/dist-node/xe2-base-objects";
import { Destroyable, ReactParamsType, getReactFuncs, reactArrayWithUndefined } from "xbsj-xe2/dist-node/xe2-base-utils";
import { CzmViewer } from 'xbsj-xe2/dist-node/xe2-cesium-objects';
import { Vector } from 'xbsj-xe2/dist-node/xe2-math';

export class CzmESEditing extends Destroyable {
    private _innerPositionReact = this.disposeVar(reactArrayWithUndefined<[number, number, number]>(undefined));
    private _sPositionEditing = this.disposeVar(new PositionEditing(this._innerPositionReact, this._editing, this._czmViewer));
    get sPositionEditing() { return this._sPositionEditing; }

    constructor(
        private _czmViewer: CzmViewer,
        private _editing: ReactParamsType<boolean> | undefined,
        private _positionReactParam: ReactParamsType<[number, number, number] | undefined>
    ) {
        super();

        const [getPosition, setPosition, positionChanged] = getReactFuncs<[number, number, number]>(this._positionReactParam);

        // 为了使编辑生效，需要监听sceneObject的position和_innerPositionReact,
        // 如果是在[0,0,0]点的话，就把_innerPositionReact设置为undefined,就能编辑了
        {
            const updated = () => {
                if (Vector.equals(getPosition(), [0, 0, 0])) {
                    this._innerPositionReact.value = undefined;
                } else {
                    this._innerPositionReact.value = getPosition();
                }
            };
            updated();
            this.dispose(positionChanged.disposableOn(updated));
        }
        {
            const updated = () => {
                if (this._innerPositionReact.value == undefined) {
                    setPosition([0, 0, 0]);
                }
                else {
                    setPosition(this._innerPositionReact.value);
                }
            }
            this.dispose(this._innerPositionReact.changed.disposableOn(updated));
        }
    }
}