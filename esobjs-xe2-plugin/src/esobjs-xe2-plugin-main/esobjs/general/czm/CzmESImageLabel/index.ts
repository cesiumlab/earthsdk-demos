import { CzmPickedInfo, ESSceneObject, GeoCanvasImagePoi, GeoImageModel, ObjResettingWithEvent, PickedInfo, SceneObject, SceneObjectPickedInfo } from 'xbsj-xe2/dist-node/xe2-base-objects';
import { Destroyable, bind, createNextAnimateFrameEvent, track } from 'xbsj-xe2/dist-node/xe2-base-utils';
import { CzmViewer } from 'xbsj-xe2/dist-node/xe2-cesium-objects';
import { CzmESLabel, WidgetEventInfo, bindNorthRotation, rpToap } from '../../../base';
import { ESImageLabel } from '../../objs';
import { getCzmPickedInfoFromPickedInfo } from '../base/utils';
import { defaultFlyToRotation, flyWithPosition, getObjectProperties } from '../base';

// export class CzmESImageLabel<T extends ESImageLabel = ESImageLabel> extends CzmESLabel<T>{
//     static readonly type = this.register<ESImageLabel>(ESImageLabel.type, this);
//     private _czmGeoCanvasImagePoi = this.disposeVar(new GeoCanvasImagePoi());
//     get czmGeoCanvasImagePoi() { return this._czmGeoCanvasImagePoi; }

//     constructor(sceneObject: T, czmViewer: CzmViewer) {
//         super(sceneObject, czmViewer);
//         const viewer = czmViewer.viewer;
//         if (!viewer) {
//             console.warn(`viewer is undefined!`);
//             return;
//         }
//         const czmGeoCanvasImagePoi = this._czmGeoCanvasImagePoi;
//         czmViewer.add(czmGeoCanvasImagePoi);
//         this.dispose(() => czmViewer.delete(czmGeoCanvasImagePoi))

//         {
//             const getStrFromEnvUrl = (url: string) => {
//                 let strUrl = url;
//                 const flag = url.includes('inner://');
//                 if (flag) {
//                     const imgName = url.split('inner://')[1];
//                     strUrl = '${esobjs-xe2-plugin-assets-script-dir}/xe2-assets/esobjs-xe2-plugin/images/points/' + imgName;
//                 }
//                 return SceneObject.context.getStrFromEnv(strUrl);
//             }

//             const urlReact = this.disposeVar(ESSceneObject.context.createEvnStrReact([sceneObject, 'url'], ESImageLabel.defaults.url));

//             const update = () => {
//                 const url = getStrFromEnvUrl(urlReact.value ?? ESImageLabel.defaults.url); 
//                 czmGeoCanvasImagePoi.imageUri = url
//                 const sizeByContent = sceneObject.sizeByContent;
//                 if (sizeByContent) {
//                     const image = new Image()
//                     image.src = url;
//                     image.onload = () => {
//                         czmGeoCanvasImagePoi.size = [image.width, image.height];
//                     }
//                 } else {
//                     const size = sceneObject.size;
//                     czmGeoCanvasImagePoi.size = [...size];
//                 }
//             }
//             const updateEvent = this.disposeVar(createNextAnimateFrameEvent(
//                 urlReact.changed,
//                 sceneObject.sizeByContentChanged,
//                 sceneObject.sizeChanged,
//             ));
//             this.dispose(updateEvent.disposableOn(() => update()));
//             update()

//             this.dispose(track([czmGeoCanvasImagePoi, 'show'], [sceneObject, 'show']));
//             this.dispose(bind([czmGeoCanvasImagePoi, 'position'], [sceneObject, 'position']));

//             const updateAnchor = () => {
//                 const anchor = sceneObject.anchor;
//                 if (anchor) {
//                     czmGeoCanvasImagePoi.originRatioAndOffset = [...anchor, 0, 0];
//                 } else {
//                     czmGeoCanvasImagePoi.originRatioAndOffset = [0, 0, 0, 0];
//                 }
//             }
//             this.dispose(sceneObject.anchorChanged.disposableOn(() => updateAnchor()));
//             updateAnchor();

//             this.dispose(czmGeoCanvasImagePoi.clickEvent.disposableOn(e => {
//                 const { offsetX, offsetY } = e;
//                 const type = e.button === 0 ? "leftClick" : e.button === 2 ? "rightClick" : undefined;
//                 const eventInfo = {
//                     type,
//                     add: { mousePos: [offsetX, offsetY] }
//                 };
//                 sceneObject.widgetEvent.emit(eventInfo as WidgetEventInfo);
//             }));

//             this.dispose(czmGeoCanvasImagePoi.hoveredChanged.disposableOn(bol => {
//                 if (bol === undefined) return;
//                 const eventInfo = {
//                     type: bol ? "mouseEnter" : "mouseLeave" as WidgetEventInfo['type'],
//                 }
//                 sceneObject.widgetEvent.emit(eventInfo);
//             }));
//         }
//     }
//     override flyTo(duration: number | undefined, id: number): boolean {
//         const { sceneObject, czmViewer, czmGeoCanvasImagePoi } = this;
//         if (!czmViewer.actived) return false;
//         if (sceneObject.flyToParam || sceneObject.flyInParam) {
//             super.flyTo(duration, id);
//             return true;
//         } else {
//             czmGeoCanvasImagePoi.flyTo(duration && duration * 1000);
//             sceneObject.flyOverEvent.emit(id, 'over', czmViewer);
//             return true;
//         }
//     }
// }

export class ImageLabel2D extends Destroyable {
    private _czmGeoCanvasImagePoi = this.disposeVar(new GeoCanvasImagePoi());
    get czmGeoCanvasImagePoi() { return this._czmGeoCanvasImagePoi; }

    public flyTo = (duration: number | undefined, id: number) => {
        if (this.czmGeoCanvasImagePoi.position) {
            this.czmViewer.flyTo(this.czmGeoCanvasImagePoi.position, 1000, defaultFlyToRotation, duration && duration * 1000);
            return;
        }
        this.czmGeoCanvasImagePoi.flyTo(duration && duration * 1000);
    }

    constructor(private sceneObject: ESImageLabel, private czmViewer: CzmViewer) {
        super();

        const czmGeoCanvasImagePoi = this._czmGeoCanvasImagePoi;
        czmViewer.add(czmGeoCanvasImagePoi);
        this.dispose(() => czmViewer.delete(czmGeoCanvasImagePoi))

        czmGeoCanvasImagePoi.pickOnClick = true;
        this.d(czmGeoCanvasImagePoi.pickedEvent.don(pickedInfo => {
            if (sceneObject.allowPicking ?? false) {
                const pickInfo = getCzmPickedInfoFromPickedInfo(pickedInfo)
                sceneObject.pickedEvent.emit(new SceneObjectPickedInfo(sceneObject, pickInfo));
            }
        }));

        {
            const getStrFromEnvUrl = (url: string) => {
                let strUrl = url;
                const flag = url.includes('inner://');
                if (flag) {
                    const imgName = url.split('inner://')[1];
                    strUrl = '${esobjs-xe2-plugin-assets-script-dir}/xe2-assets/esobjs-xe2-plugin/images/points/' + imgName;
                }
                return SceneObject.context.getStrFromEnv(rpToap(strUrl));
            }

            const update = () => {
                const url = getStrFromEnvUrl(typeof sceneObject.url == 'string' ? sceneObject.url : sceneObject.url.url);
                czmGeoCanvasImagePoi.imageUri = url
                const sizeByContent = sceneObject.sizeByContent;
                if (sizeByContent) {
                    const image = new Image()
                    image.src = url;
                    image.onload = () => {
                        czmGeoCanvasImagePoi.size = [image.width, image.height];
                    }
                } else {
                    const size = sceneObject.size;
                    czmGeoCanvasImagePoi.size = [...size];
                }
            }
            const updateEvent = this.disposeVar(createNextAnimateFrameEvent(
                sceneObject.urlChanged,
                sceneObject.sizeByContentChanged,
                sceneObject.sizeChanged,
            ));
            this.dispose(updateEvent.disposableOn(() => update()));
            update()

            this.d(track([czmGeoCanvasImagePoi, 'zOrder'], [sceneObject, 'zOrder']));
            this.dispose(track([czmGeoCanvasImagePoi, 'show'], [sceneObject, 'show']));
            this.dispose(bind([czmGeoCanvasImagePoi, 'position'], [sceneObject, 'position']));

            {
                const update = () => {
                    czmGeoCanvasImagePoi.scale = [sceneObject.scale[1], sceneObject.scale[2]];
                }
                update();
                this.dispose(sceneObject.scaleChanged.don(update));
            }
            const updateAnchor = () => {
                const anchor = sceneObject.anchor;
                const offset = sceneObject.offset;
                if (anchor) {
                    czmGeoCanvasImagePoi.originRatioAndOffset = [...anchor, -offset[0], -offset[1]];
                } else {
                    czmGeoCanvasImagePoi.originRatioAndOffset = [0, 0, 0, 0];
                }
            }
            const event = this.ad(createNextAnimateFrameEvent(sceneObject.anchorChanged, sceneObject.offsetChanged));
            this.dispose(event.disposableOn(() => updateAnchor()));
            updateAnchor();

            this.dispose(czmGeoCanvasImagePoi.clickEvent.disposableOn(e => {
                const { offsetX, offsetY } = e;
                const type = e.button === 0 ? "leftClick" : e.button === 2 ? "rightClick" : undefined;
                const eventInfo = {
                    type,
                    add: { mousePos: [offsetX, offsetY] }
                };
                sceneObject.widgetEvent.emit(eventInfo as WidgetEventInfo);
            }));

            this.dispose(czmGeoCanvasImagePoi.hoveredChanged.disposableOn(bol => {
                if (bol === undefined) return;
                const eventInfo = {
                    type: bol ? "mouseEnter" : "mouseLeave" as WidgetEventInfo['type'],
                }
                sceneObject.widgetEvent.emit(eventInfo);
            }));
        }
    }
}

export class ImageLabel3D extends Destroyable {
    private _czmGeoImageModel = this.disposeVar(new GeoImageModel());
    get czmGeoImageModel() { return this._czmGeoImageModel; }

    public flyTo = (duration: number | undefined, id: number) => {
        if (this.czmGeoImageModel.position)
            return flyWithPosition(this.czmViewer, this.sceneObject, id, this.sceneObject.position, Math.max(...this.czmGeoImageModel.size), duration, true);
        this.czmGeoImageModel.flyTo(duration && duration * 1000);
    }

    constructor(private sceneObject: ESImageLabel, private czmViewer: CzmViewer, czmESImageLabel: CzmESImageLabel) {
        super();

        const czmGeoImageModel = this._czmGeoImageModel;
        czmViewer.add(czmGeoImageModel);
        this.dispose(() => czmViewer.delete(czmGeoImageModel))

        this.d(czmGeoImageModel.pickedEvent.don(pickedInfo => {
            const pointerEvent = getObjectProperties(pickedInfo, "attachedInfo")?.pointerEvent;
            const czmPickResult = getObjectProperties(pickedInfo, "czmPickResult");
            if (!pointerEvent) return;
            // 响应widgetEvent事件
            // 鼠标点击事件
            const eventInfo = {
                type: pointerEvent.button == 0 ? "leftClick" : pointerEvent.button == 2 ? "rightClick" : undefined,
                add: { mousePos: [pointerEvent.offsetX, pointerEvent.offsetY] as [number, number] }
            }
            if (eventInfo.type == undefined) {
                eventInfo.type = czmPickResult == undefined ? "mouseLeave" : "mouseEnter";
            }
            sceneObject.widgetEvent.emit(eventInfo as WidgetEventInfo)
            // 左键事件，额外进行响应pickedEvent事件
            if (pointerEvent.button == 0 && (sceneObject.allowPicking ?? false)) {
                const pickInfo = getCzmPickedInfoFromPickedInfo(pickedInfo)
                sceneObject.pickedEvent.emit(new SceneObjectPickedInfo(sceneObject, pickInfo));
            }
        }));

        const sizeByContent = sceneObject.sizeByContent;

        const getStrFromEnvUrl = (url: string) => {
            let strUrl = url;
            const flag = url.includes('inner://');
            if (flag) {
                const imgName = url.split('inner://')[1];
                strUrl = '${esobjs-xe2-plugin-assets-script-dir}/xe2-assets/esobjs-xe2-plugin/images/points/' + imgName;
            }
            // return SceneObject.context.getStrFromEnv(strUrl);
            return SceneObject.context.getStrFromEnv(rpToap(strUrl));
        }

        // const url = getStrFromEnvUrl(czmESImageLabel.urlReact ?? ESImageLabel.defaults.url);
        const url = getStrFromEnvUrl(typeof czmESImageLabel.sceneObject.url == 'string' ? czmESImageLabel.sceneObject.url : czmESImageLabel.sceneObject.url.url);

        const image = new Image()
        image.src = url;
        image.onload = () => {
            if (sizeByContent) {
                czmGeoImageModel.size = [image.width / 100, image.height / 100];
            } else {
                const size = sceneObject.size;
                czmGeoImageModel.size = [size[0] / 100, size[1] / 100];
            }
        }
        czmGeoImageModel.uri = url

        this.dispose(bind([czmGeoImageModel, 'positionEditing'], [sceneObject, 'editing']));
        this.dispose(track([czmGeoImageModel, 'show'], [sceneObject, 'show']));
        this.dispose(bind([czmGeoImageModel, 'position'], [sceneObject, 'position']));
        this.dispose(track([czmGeoImageModel, 'allowPicking'], [sceneObject, 'allowPicking']));
        czmGeoImageModel.pixelSize = undefined
        czmGeoImageModel.useAxis = "XZ";
        {
            const update = () => {
                czmGeoImageModel.scale = [sceneObject.scale[1], sceneObject.scale[2]];
            }
            update();
            this.d(sceneObject.scaleChanged.don(update))
        }
        {
            const update = () => {
                if (sceneObject.rotationType === 0) {
                    czmGeoImageModel.rotationMode = "WithProp"
                    this.d(bindNorthRotation([czmGeoImageModel, 'rotation'], [sceneObject, 'rotation']));
                }
                if (sceneObject.rotationType === 1) {
                    czmGeoImageModel.rotationMode = "WithCamera"
                }
                if (sceneObject.rotationType === 2) {
                    czmGeoImageModel.rotationMode = 'WithCameraOnlyZ'
                }
            }
            update()
            this.dispose(sceneObject.rotationTypeChanged.disposableOn(update))
        }
        const updateAnchor = () => {
            const anchor = sceneObject.anchor;
            const offset = sceneObject.offset;
            if (anchor) {
                czmGeoImageModel.originRatioAndOffset = [...anchor, -offset[0] / 100, -offset[1] / 100];
            } else {
                czmGeoImageModel.originRatioAndOffset = [0, 0, 0, 0];
            }
        }
        const event = this.ad(createNextAnimateFrameEvent(sceneObject.anchorChanged, sceneObject.offsetChanged));
        this.dispose(event.disposableOn(() => updateAnchor()));
        updateAnchor();
    }
}

export class CzmESImageLabel<T extends ESImageLabel = ESImageLabel> extends CzmESLabel<T> {
    static readonly type = this.register<ESImageLabel>(ESImageLabel.type, this);
    // private _urlReact = this.disposeVar(ESSceneObject.context.createEvnStrReact([this.sceneObject, 'url'], ESImageLabel.defaults.url));
    // get urlReact() { return this._urlReact.value; }
    // set urlReact(value: any) { this._urlReact.value = value; }
    // get urlReactChanged() { return this._urlReact.changed; }

    private _event = this.disposeVar(createNextAnimateFrameEvent(this.sceneObject.urlChanged, this.sceneObject.screenRenderChanged, this.sceneObject.sizeByContentChanged, this.sceneObject.sizeChanged));
    get event() { return this._event; }

    // @ts-ignore
    private _resetting = this.disposeVar(new ObjResettingWithEvent(this._event, () => {
        if (this.sceneObject.screenRender) {
            return new ImageLabel2D(this.sceneObject, this.czmViewer);
        }
        else {
            return new ImageLabel3D(this.sceneObject, this.czmViewer, this);
        }
    }))
    get resetting() { return this._resetting; }

    constructor(sceneObject: T, czmViewer: CzmViewer) {
        super(sceneObject, czmViewer);
        const viewer = czmViewer.viewer;
        if (!viewer) {
            console.warn(`viewer is undefined!`);
            return;
        }

    }
    override flyTo(duration: number | undefined, id: number): boolean {
        const { sceneObject, czmViewer, resetting } = this;
        if (!czmViewer.actived) return false;
        if (sceneObject.flyToParam || sceneObject.flyInParam) {
            return super.flyTo(duration, id);
        } else {
            if (resetting && resetting.obj) {
                resetting.obj.flyTo(duration, id);
            }
            sceneObject.flyOverEvent.emit(id, 'over', czmViewer);
            return true;
        }
    }
}

