import { CzmESObjectWithLocation } from '@/esobjs-xe2-plugin-main/esobjs/base';
import { GeoDivTextPoi } from 'xbsj-xe2/dist-node/xe2-base-objects';
import { bind, track } from "xbsj-xe2/dist-node/xe2-base-utils";
import { CzmViewer } from 'xbsj-xe2/dist-node/xe2-cesium-objects';
import { ESGeoDivTextPoi } from '../../objs';
import { CzmESEditing } from '@/esobjs-xe2-plugin-main/esobjs/utils';

export class CzmESGeoDivTextPoi<T extends ESGeoDivTextPoi = ESGeoDivTextPoi> extends CzmESObjectWithLocation<T> {
    static readonly type = this.register<ESGeoDivTextPoi>(ESGeoDivTextPoi.type, this);
    private _czmDivTextPoi = this.disposeVar(new GeoDivTextPoi());
    get czmDivTextPoi() { return this._czmDivTextPoi; }

    private _sEditing = this.disposeVar(new CzmESEditing(this.czmViewer, [this.sceneObject, 'editing'], [this.sceneObject, 'position']));
    get sEditing() { return this._sEditing; }

    constructor(sceneObject: T, czmViewer: CzmViewer) {
        super(sceneObject, czmViewer);
        this.sPrsEditing.enabled = false;

        const viewer = czmViewer.viewer;
        if (!viewer) {
            console.warn(`viewer is undefined!`);
            return;
        }

        const czmDivTextPoi = this._czmDivTextPoi;
        czmViewer.add(czmDivTextPoi);
        this.dispose(() => czmViewer.delete(czmDivTextPoi))
        this.dispose(track([czmDivTextPoi, 'show'], [sceneObject, 'show']));
        this.dispose(bind([czmDivTextPoi, 'position'], [sceneObject, 'position']));
        this.dispose(bind([czmDivTextPoi, 'textEditingInteraction'], [sceneObject, 'textEditingInteraction']));
        this.dispose(bind([czmDivTextPoi, 'textEditing'], [sceneObject, 'textEditing']));
        this.dispose(bind([czmDivTextPoi, 'width'], [sceneObject, 'width']));
        this.dispose(bind([czmDivTextPoi, 'text'], [sceneObject, 'text']));
        this.dispose(track([czmDivTextPoi, 'originRatioAndOffset'], [sceneObject, 'originRatioAndOffset']));
        this.dispose(track([czmDivTextPoi, 'opacity'], [sceneObject, 'opacity']));
        this.dispose(bind([czmDivTextPoi, 'fontSize'], [sceneObject, 'fontSize']));
        this.dispose(track([czmDivTextPoi, 'color'], [sceneObject, 'color']));
        this.dispose(track([czmDivTextPoi, 'backgroundColor'], [sceneObject, 'backgroundColor']));
        this.dispose(track([czmDivTextPoi, 'borderRadius'], [sceneObject, 'borderRadius']));
        this.dispose(track([czmDivTextPoi, 'borderColor'], [sceneObject, 'borderColor']));
        this.dispose(track([czmDivTextPoi, 'borderWidth'], [sceneObject, 'borderWidth']));
        this.dispose(track([czmDivTextPoi, 'textAlign'], [sceneObject, 'textAlign']));
        this.dispose(track([czmDivTextPoi, 'borderStyle'], [sceneObject, 'borderStyle']));
    }
    override flyTo(duration: number | undefined, id: number): boolean {
        const { sceneObject, czmViewer, czmDivTextPoi } = this;
        if (!czmViewer.actived) return false;
        if (sceneObject.flyToParam || sceneObject.flyInParam) {
            return super.flyTo(duration, id);
        } else {
            czmDivTextPoi.flyTo(duration && duration * 1000);
            sceneObject.flyOverEvent.emit(id, 'over', czmViewer);
            return true;
        }
    }
}
