import { bind, track } from "xbsj-xe2/dist-node/xe2-base-utils";
import { ESCzml } from '../../objs';
import { CzmObject, CzmViewer } from 'xbsj-xe2/dist-node/xe2-cesium-objects';
import { CzmCzml } from "xbsj-xe2/dist-node/xe2-cesium-objects";

export class CzmESCzml extends CzmObject<ESCzml> {
    static readonly type = this.register(ESCzml.type, this);
    constructor(sceneObject: ESCzml, czmViewer: CzmViewer) {
        super(sceneObject, czmViewer);
        const viewer = czmViewer.viewer;
        if (!viewer) {
            console.warn(`viewer is undefined!`);
            return;
        }
        const czmCzml = this.disposeVar(new CzmCzml());
        czmViewer.add(czmCzml);
        this.dispose(() => czmViewer.delete(czmCzml))

        this.dispose(track([czmCzml, 'show'], [sceneObject, 'show']));
        this.dispose(track([czmCzml, 'allowPicking'], [sceneObject, 'allowPicking']));
        this.dispose(bind([czmCzml, 'data'], [sceneObject, 'data']));
        this.dispose(bind([czmCzml, 'uri'], [sceneObject, 'uri']));
        this.dispose(bind([czmCzml, 'loadFuncStr'], [sceneObject, 'loadFuncStr']));
        this.dispose(bind([czmCzml, 'autoResetClock'], [sceneObject, 'autoResetClock']));
        this.dispose(bind([czmCzml, 'clockEnabled'], [sceneObject, 'clockEnabled']));
        this.dispose(bind([czmCzml, 'startTime'], [sceneObject, 'startTime']));
        this.dispose(bind([czmCzml, 'stopTime'], [sceneObject, 'stopTime']));
        this.dispose(bind([czmCzml, 'currentTime'], [sceneObject, 'currentTime']));
        this.dispose(bind([czmCzml, 'multiplier'], [sceneObject, 'multiplier']));
        this.dispose(bind([czmCzml, 'clockStep'], [sceneObject, 'clockStep']));
        this.dispose(bind([czmCzml, 'clockRange'], [sceneObject, 'clockRange']));
        this.dispose(bind([czmCzml, 'shouldAnimate'], [sceneObject, 'shouldAnimate']));
        this.dispose(sceneObject.flyToEvent.disposableOn(duration => {
            if (!czmViewer.actived) return;
            czmCzml.flyTo(duration && duration * 1000);
        }));

    }
}
