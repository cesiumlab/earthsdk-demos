import { bind, track } from "xbsj-xe2/dist-node/xe2-base-utils";

import { CzmESVisualObject } from "@/esobjs-xe2-plugin-main/esobjs/base";
import { SceneObjectPickedInfo } from "xbsj-xe2/dist-node/xe2-base-objects";
import { CzmHeatMap, CzmViewer } from 'xbsj-xe2/dist-node/xe2-cesium-objects';
import { ESHeatMap } from '../../objs';
import { getCzmPickedInfoFromPickedInfo } from "../base/utils";
import { flyWithPositions, getPointerEventButton } from "../base";

export class CzmESHeatMap extends CzmESVisualObject<ESHeatMap> {
    static readonly type = this.register(ESHeatMap.type, this);
    private _czmHeatMap = this.disposeVar(new CzmHeatMap());
    get czmHeatMap() { return this._czmHeatMap; }

    constructor(sceneObject: ESHeatMap, czmViewer: CzmViewer) {
        super(sceneObject, czmViewer);
        const viewer = czmViewer.viewer;
        if (!viewer) {
            console.warn(`viewer is undefined!`);
            return;
        }
        const czmHeatMap = this._czmHeatMap;
        czmViewer.add(czmHeatMap);
        this.dispose(() => czmViewer.delete(czmHeatMap));

        this.dispose(track([czmHeatMap, 'show'], [sceneObject, 'show']));
        this.dispose(track([czmHeatMap, 'allowPicking'], [sceneObject, 'allowPicking']));
        this.dispose(track([czmHeatMap, 'rotationAngle'], [sceneObject, 'rotationAngle']));
        this.dispose(track([czmHeatMap, 'color'], [sceneObject, 'color']));
        this.dispose(track([czmHeatMap, 'classificationType'], [sceneObject, 'classificationType']));
        this.dispose(track([czmHeatMap, 'data'], [sceneObject, 'data']));
        this.dispose(track([czmHeatMap, 'canvasWidth'], [sceneObject, 'canvasWidth']));
        this.dispose(track([czmHeatMap, 'max'], [sceneObject, 'max']));
        this.dispose(track([czmHeatMap, 'min'], [sceneObject, 'min']));
        this.dispose(track([czmHeatMap, 'radius'], [sceneObject, 'radius']));
        this.dispose(track([czmHeatMap, 'maxOpacity'], [sceneObject, 'maxOpacity']));
        this.dispose(track([czmHeatMap, 'minOpacity'], [sceneObject, 'minOpacity']));
        this.dispose(track([czmHeatMap, 'blur'], [sceneObject, 'blur']));
        this.dispose(track([czmHeatMap, 'gradient'], [sceneObject, 'gradient']));
        this.dispose(bind([czmHeatMap, 'rectangle'], [sceneObject, 'rectangle']));
        this.dispose(bind([czmHeatMap, 'editing'], [sceneObject, 'editing']));
        this.dispose(bind([czmHeatMap, 'pointEditing'], [sceneObject, 'pointEditing']));
        this.dispose(track([czmHeatMap, 'debug'], [sceneObject, 'debug']));

        this.dispose(czmHeatMap.pickedEvent.disposableOn(pickedInfo => {
            if (getPointerEventButton(pickedInfo) === 0 && (sceneObject.allowPicking ?? false)) {
                const pickInfo = getCzmPickedInfoFromPickedInfo(pickedInfo)
                sceneObject.pickedEvent.emit(new SceneObjectPickedInfo(sceneObject, pickInfo));
            }
        }));
    }
    override flyTo(duration: number | undefined, id: number): boolean {
        const { sceneObject, czmViewer, czmHeatMap } = this;
        if (!czmViewer.actived) return false;
        if (sceneObject.flyToParam || sceneObject.flyInParam) {
            return super.flyTo(duration, id);
        } else {
            if (czmHeatMap.rectangle) {
                flyWithPositions(czmViewer, sceneObject, id,
                    [[sceneObject.rectangle[0], sceneObject.rectangle[1], 0], [sceneObject.rectangle[2], sceneObject.rectangle[3], 0]],
                    duration);
                return true
            }
            return false;
        }
    }
    override flyIn(duration: number | undefined, id: number): boolean {
        const { sceneObject, czmViewer, czmHeatMap } = this;
        if (!czmViewer.actived) return false;
        if (sceneObject.flyInParam) {
            return super.flyIn(duration, id);
        } else {
            if (czmHeatMap.rectangle) {
                flyWithPositions(czmViewer, sceneObject, id,
                    [[sceneObject.rectangle[0], sceneObject.rectangle[1], 0], [sceneObject.rectangle[2], sceneObject.rectangle[3], 0]],
                    duration);
                return true
            }
            return false;
        }
    }
}
