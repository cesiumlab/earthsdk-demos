import { CzmESVisualObject } from '@/esobjs-xe2-plugin-main/esobjs/base';
import { GeoLocationMeasurement, SceneObjectPickedInfo } from 'xbsj-xe2/dist-node/xe2-base-objects';
import { bind, track } from "xbsj-xe2/dist-node/xe2-base-utils";
import { CzmViewer } from 'xbsj-xe2/dist-node/xe2-cesium-objects';
import { ESLocationMeasurement } from '../../objs';
import { CzmESEditing } from '@/esobjs-xe2-plugin-main/esobjs/utils';
import { getCzmPickedInfoFromPickedInfo } from '../base/utils';
import { getPointerEventButton } from '../base';

export class CzmESLocationMeasurement extends CzmESVisualObject<ESLocationMeasurement> {
    static readonly type = this.register(ESLocationMeasurement.type, this);
    private _czmLocationMeasurement = this.disposeVar(new GeoLocationMeasurement());
    get czmLocationMeasurement() { return this._czmLocationMeasurement; }

    private _sEditing = this.disposeVar(new CzmESEditing(this.czmViewer, [this.sceneObject, 'editing'], [this.sceneObject, 'position']));
    get sEditing() { return this._sEditing; }

    constructor(sceneObject: ESLocationMeasurement, czmViewer: CzmViewer) {
        super(sceneObject, czmViewer);
        const viewer = czmViewer.viewer;
        if (!viewer) {
            console.warn(`viewer is undefined!`);
            return;
        }

        const czmLocationMeasurement = this._czmLocationMeasurement;
        czmViewer.add(czmLocationMeasurement);
        this.dispose(() => czmViewer.delete(czmLocationMeasurement))
        this.dispose(track([czmLocationMeasurement, 'show'], [sceneObject, 'show']));
        this.dispose(bind([czmLocationMeasurement, 'position'], [sceneObject, 'position']));
        {
            const update = () => {
                // [["度", "DECIMAL_DEGREE"], ["度分", "DEGREES_DECIMAL_MINUTES"], ["度分秒", "SEXAGESIMAL_DEGREE"]]
                czmViewer.lonLatFormat ?? (czmViewer.lonLatFormat = "DECIMAL_DEGREE")
                czmLocationMeasurement.textFunc = czmViewer.lonLatFormat == "DECIMAL_DEGREE"
                    ? GeoLocationMeasurement.defaultTextFunc_度格式
                    : czmViewer.lonLatFormat == "DEGREES_DECIMAL_MINUTES"
                        ? GeoLocationMeasurement.defaultTextFunc_度分格式
                        : GeoLocationMeasurement.defaultTextFunc_度分秒格式;
            }
            update();
            this.d(czmViewer.lonLatFormatChanged.don(() => {
                update();
            }))
        }

        this.dispose(czmLocationMeasurement.pickedEvent.disposableOn(pickedInfo => {
            if (getPointerEventButton(pickedInfo) === 0 && (sceneObject.allowPicking ?? false)) {
                const pickInfo = getCzmPickedInfoFromPickedInfo(pickedInfo)
                sceneObject.pickedEvent.emit(new SceneObjectPickedInfo(sceneObject, pickInfo));
            }
        }));
    }
    override flyTo(duration: number | undefined, id: number): boolean {
        const { sceneObject, czmViewer, czmLocationMeasurement } = this;
        if (!czmViewer.actived) return false;
        if (sceneObject.flyToParam || sceneObject.flyInParam) {
            return super.flyTo(duration, id);
        } else {
            czmLocationMeasurement.flyTo(duration && duration * 1000);
            sceneObject.flyOverEvent.emit(id, 'over', czmViewer);
            return true;
        }
    }
    override flyIn(duration: number | undefined, id: number): boolean {
        const { sceneObject, czmViewer, czmLocationMeasurement } = this;
        if (!czmViewer.actived) return false;
        if (sceneObject.flyInParam) {
            return super.flyIn(duration, id);
        } else {
            czmLocationMeasurement.flyTo(duration && duration * 1000);
            sceneObject.flyOverEvent.emit(id, 'over', czmViewer);
            return true;
        }
    }
}
