import { CzmESObjectWithLocation, ESSceneObject } from "@/esobjs-xe2-plugin-main/esobjs/base";
import { Destroyable, bind, track } from "xbsj-xe2/dist-node/xe2-base-utils";
import { CzmBoxClippingPlanes, CzmViewer } from 'xbsj-xe2/dist-node/xe2-cesium-objects';
import { ES3DTileset, ESBoxClipping } from '../../objs';
import { ObjResettingWithEvent, SceneObjectWithId, createNextAnimateFrameEvent } from "xbsj-renderer/dist-node/xr-utils";
import { defaultFlyToRotation, flyWithPosition } from "../base";
import * as Cesium from 'cesium';
class TilesIdResetting extends Destroyable {
    constructor(private _czmESBoxClippingPlanes: CzmESBoxClipping, private _eS3DTileset: ES3DTileset) {
        super();
        const id = this._czmESBoxClippingPlanes.czmBoxClippingPlanes.id
        this._eS3DTileset.clippingPlaneId = id;
        this.dispose(() => { this._eS3DTileset.clippingPlaneId = ''; });
    }
}
export class CzmESBoxClipping extends CzmESObjectWithLocation<ESBoxClipping> {
    static readonly type = this.register(ESBoxClipping.type, this);
    // private _czmBoxClippingPlanes = this.disposeVar(new CzmBoxClippingPlanes());
    private _czmBoxClippingPlanes = this.disposeVar(ESSceneObject.createFromClass(CzmBoxClippingPlanes))
    get czmBoxClippingPlanes() { return this._czmBoxClippingPlanes; }

    private _tilesSceneObjectWithId = this.disposeVar(new SceneObjectWithId());
    get tilesSceneObjectWithId() { return this._tilesSceneObjectWithId; }
    private _tilesSceneObjectWithIdInit = this.dispose(track([this._tilesSceneObjectWithId, 'id'], [this.sceneObject, 'targetID']));

    private _event = this.disposeVar(createNextAnimateFrameEvent(this.tilesSceneObjectWithId.sceneObjectChanged, this.sceneObject.showChanged, this.czmBoxClippingPlanes.enabledChanged))
    get event() { return this._event; }

    private _tilesIdResetting = this.disposeVar(new ObjResettingWithEvent(this.event, () => {
        const { sceneObject } = this.tilesSceneObjectWithId;
        if (!sceneObject) return undefined;
        if (!(sceneObject instanceof ES3DTileset)) return undefined;
        if (!this.sceneObject.show) return;
        return new TilesIdResetting(this, sceneObject as ES3DTileset);
    }));

    constructor(sceneObject: ESBoxClipping, czmViewer: CzmViewer) {
        super(sceneObject, czmViewer);
        const viewer = czmViewer.viewer;
        if (!viewer) {
            console.warn(`viewer is undefined!`);
            return;
        }
        const { czmBoxClippingPlanes } = this;
        czmViewer.add(czmBoxClippingPlanes);
        this.dispose(() => czmViewer.delete(czmBoxClippingPlanes))

        {//show
            const updateShow = () => {
                czmBoxClippingPlanes.enabled = sceneObject.show;
                czmBoxClippingPlanes.showHelper = sceneObject.show;
            }
            updateShow()
            this.dispose(sceneObject.showChanged.don(updateShow))
        }
        {//反转
            const update = () => {
                czmBoxClippingPlanes.reverse = !sceneObject.reverse;
            }
            update()
            this.dispose(sceneObject.reverseChanged.don(update))
        }
        {//applyOnTerrain根据targetID判断
            const update = () => {
                if (sceneObject.targetID) {
                    czmBoxClippingPlanes.applyOnTerrain = false;
                } else {
                    czmBoxClippingPlanes.applyOnTerrain = true;
                }
            }
            update()
            this.dispose(sceneObject.targetIDChanged.don(update))
        }
        {//size
            const update = () => {
                const [x, y, z] = sceneObject.size;
                czmBoxClippingPlanes.minSize = [-0.5 * x, -0.5 * y, -0.5 * z];
                czmBoxClippingPlanes.maxSize = [0.5 * x, 0.5 * y, 0.5 * z];
            }
            update()
            this.dispose(sceneObject.sizeChanged.don(update))
        }

        this.dispose(bind([czmBoxClippingPlanes, 'position'], [sceneObject, 'position']));
        this.dispose(bind([czmBoxClippingPlanes, 'rotation'], [sceneObject, 'rotation']));
        this.dispose(track([czmBoxClippingPlanes, 'edgeColor'], [sceneObject, 'edgeColor']));
        this.dispose(track([czmBoxClippingPlanes, 'edgeWidth'], [sceneObject, 'edgeWidth']));
    }
    override flyTo(duration: number | undefined, id: number): boolean {
        const { sceneObject, czmViewer, czmBoxClippingPlanes } = this;
        if (!czmViewer.actived || !czmViewer.viewer) return false;
        if (sceneObject.flyToParam || sceneObject.flyInParam) {
            return super.flyTo(duration, id);
        } else {
            if (czmBoxClippingPlanes.position) {
                flyWithPosition(czmViewer, sceneObject, id, czmBoxClippingPlanes.position, Math.max(...sceneObject.size), duration, true);
                return true;
            }
            return false;
        }
    }
}
