import { getMinMaxCorner } from 'xbsj-xe2/dist-node/xe2-base-objects';
import { CzmESGeoVector } from '@/esobjs-xe2-plugin-main/esobjs/base';
import { GeoAreaMeasurement, SceneObjectPickedInfo } from 'xbsj-xe2/dist-node/xe2-base-objects';
import { JsonValue, bind, createNextAnimateFrameEvent, track } from "xbsj-xe2/dist-node/xe2-base-utils";
import { CzmViewer } from 'xbsj-xe2/dist-node/xe2-cesium-objects';
import { ESAreaMeasurement } from '../../objs';
import { getCzmPickedInfoFromPickedInfo } from '../base/utils';
import { flyWithPositions, getPointerEventButton } from '../base';

export class CzmESAreaMeasurement<T extends ESAreaMeasurement = ESAreaMeasurement> extends CzmESGeoVector<T> {
    static readonly type = this.register<ESAreaMeasurement>(ESAreaMeasurement.type, this);
    private _czmAreaMeasurement = this.disposeVar(new GeoAreaMeasurement());
    get czmAreaMeasurement() { return this._czmAreaMeasurement; }

    constructor(sceneObject: T, czmViewer: CzmViewer) {
        super(sceneObject, czmViewer);
        const viewer = czmViewer.viewer;
        if (!viewer) {
            console.warn(`viewer is undefined!`);
            return;
        }
        sceneObject.strokeGround = false;
        sceneObject.fillGround = false;

        const czmAreaMeasurement = this._czmAreaMeasurement;
        czmViewer.add(czmAreaMeasurement);
        this.dispose(() => czmViewer.delete(czmAreaMeasurement))
        this.dispose(track([czmAreaMeasurement, 'show'], [sceneObject, 'show']));
        this.dispose(track([czmAreaMeasurement, 'allowPicking'], [sceneObject, 'allowPicking']));
        this.dispose(bind([czmAreaMeasurement, 'positions'], [sceneObject, 'points']));
        this.dispose(bind([czmAreaMeasurement, 'editing'], [sceneObject, 'editing']));
        this.dispose(bind([czmAreaMeasurement, 'strokeGround'], [sceneObject, 'strokeGround']));
        this.d(bind([czmAreaMeasurement, 'ground'], [sceneObject, 'fillGround']));

        // this.dispose(track([czmAreaMeasurement, 'hasDash'], [sceneObject, 'hasDash']));
        // this.dispose(track([czmAreaMeasurement, 'gapColor'], [sceneObject, 'gapColor']));
        // this.dispose(track([czmAreaMeasurement, 'dashLength'], [sceneObject, 'dashLength']));
        // this.dispose(track([czmAreaMeasurement, 'dashPattern'], [sceneObject, 'dashPattern']));
        // this.dispose(track([czmAreaMeasurement, 'width'], [sceneObject, 'strokeWidth']));
        // this.dispose(track([czmAreaMeasurement, 'color'], [sceneObject, 'fillColor']));


        {
            const updateProp = () => {
                const stroked = sceneObject.stroked
                if (!stroked) {
                    czmAreaMeasurement.width = 0;
                    return
                } else {
                    czmAreaMeasurement.width = sceneObject.strokeWidth;
                }
                czmAreaMeasurement.color = sceneObject.strokeColor
                const strokeStyle = sceneObject.strokeStyle;
                if (strokeStyle.material === 'hasDash' && strokeStyle.materialParams) {
                    try {
                        const params = strokeStyle.materialParams as ({ [x: string]: JsonValue })
                        if (Reflect.has(params, 'gapColor')) {
                            czmAreaMeasurement.gapColor = params.gapColor as [number, number, number, number] | undefined ?? [0, 0, 0, 0];
                        }
                        if (Reflect.has(params, 'dashLength')) {
                            czmAreaMeasurement.dashLength = params.dashLength as number
                        }
                        if (Reflect.has(params, 'dashPattern')) {
                            czmAreaMeasurement.dashPattern = params.dashPattern as number
                        }
                    } catch (error) {
                        console.error(error)
                    }
                } else {
                    czmAreaMeasurement.gapColor = [0, 0, 0, 0];
                    czmAreaMeasurement.dashLength = 0
                    czmAreaMeasurement.dashPattern = 0
                }

                const strokeMaterial = sceneObject.strokeMaterial ?? 'normal'
                if (strokeMaterial === 'hasDash') {
                    czmAreaMeasurement.hasDash = true
                    czmAreaMeasurement.hasArrow = false
                } else if (strokeMaterial === 'hasArrow') {
                    czmAreaMeasurement.hasDash = false
                    czmAreaMeasurement.hasArrow = true
                } else if (strokeMaterial === 'normal') {
                    czmAreaMeasurement.hasDash = false
                    czmAreaMeasurement.hasArrow = false
                } else {
                    czmAreaMeasurement.hasDash = false
                    czmAreaMeasurement.hasArrow = false
                }
            }
            updateProp();
            const updateEvent = this.disposeVar(createNextAnimateFrameEvent(
                sceneObject.strokeStyleChanged,
                sceneObject.strokedChanged,
            ));
            this.dispose(updateEvent.disposableOn(updateProp));
        }


        {
            const updateProp = () => {
                const filled = sceneObject.filled
                if (!filled) {
                    czmAreaMeasurement.fillColor = [1, 1, 1, 0];
                    return
                } else {
                    czmAreaMeasurement.fillColor = sceneObject.fillColor;
                }
            }
            updateProp();
            const updateEvent = this.disposeVar(createNextAnimateFrameEvent(
                sceneObject.fillStyleChanged,
                sceneObject.filledChanged
            ));
            this.dispose(updateEvent.disposableOn(updateProp));
        }

        this.dispose(czmAreaMeasurement.pickedEvent.disposableOn(pickedInfo => {
            if (getPointerEventButton(pickedInfo) === 0 && (sceneObject.allowPicking ?? false)) {
                const pickInfo = getCzmPickedInfoFromPickedInfo(pickedInfo)
                sceneObject.pickedEvent.emit(new SceneObjectPickedInfo(sceneObject, pickInfo));
            }
        }));
    }
    override flyTo(duration: number | undefined, id: number): boolean {
        const { sceneObject, czmViewer, czmAreaMeasurement } = this;
        if (!czmViewer.actived) return false;
        if (sceneObject.flyToParam || sceneObject.flyInParam) {
            return super.flyTo(duration, id);
        } else {
            if (czmAreaMeasurement.positions) {
                flyWithPositions(czmViewer, sceneObject, id, czmAreaMeasurement.positions, duration);
                return true;
            }
            return false;
        }
    }
    override flyIn(duration: number | undefined, id: number): boolean {
        const { sceneObject, czmViewer, czmAreaMeasurement } = this;
        if (!czmViewer.actived) return false;
        if (sceneObject.flyInParam) {
            return super.flyIn(duration, id);
        } else {
            if (czmAreaMeasurement.positions) {
                flyWithPositions(czmViewer, sceneObject, id, czmAreaMeasurement.positions, duration);
                return true;
            }
            return false;
        }
    }
}
