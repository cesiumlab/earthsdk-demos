import { CzmESGeoVector, ESSceneObject } from "@/esobjs-xe2-plugin-main/esobjs/base";
import { Destroyable, bind, track } from "xbsj-xe2/dist-node/xe2-base-utils";
import { CzmViewer, CzmPolygonFlattenedPlane } from 'xbsj-xe2/dist-node/xe2-cesium-objects';
import { ObjResettingWithEvent, SceneObjectWithId, createNextAnimateFrameEvent } from "xbsj-xe2/dist-node/xe2-utils";
import { ES3DTileset, ESPolygonFlattenedPlane } from '../../objs';
import { flyWithPositions } from "../base";

class TilesIdResetting extends Destroyable {
    constructor(private _czmESPolygonFlattenedPlane: CzmESPolygonFlattenedPlane, private _eS3DTileset: ES3DTileset) {
        super();

        this._eS3DTileset.flattenedPlaneEnabled = true
        this._czmESPolygonFlattenedPlane.polygonFlattenedPlane.czmFlattenedPlaneId = this._eS3DTileset.flattenedPlaneId

        this.dispose(() => this._eS3DTileset.flattenedPlaneEnabled = false)
    }
}

export class CzmESPolygonFlattenedPlane<T extends ESPolygonFlattenedPlane = ESPolygonFlattenedPlane> extends CzmESGeoVector<T> {
    static readonly type = this.register<ESPolygonFlattenedPlane>(ESPolygonFlattenedPlane.type, this);

    private _polygonFlattenedPlane = this.disposeVar(ESSceneObject.createFromClass(CzmPolygonFlattenedPlane))
    get polygonFlattenedPlane() { return this._polygonFlattenedPlane; }

    private _tilesSceneObjectWithId = this.disposeVar(new SceneObjectWithId());
    get tilesSceneObjectWithId() { return this._tilesSceneObjectWithId; }
    private _tilesSceneObjectWithIdInit = this.dispose(track([this._tilesSceneObjectWithId, 'id'], [this.sceneObject, 'targetID']));

    private _event = this.disposeVar(createNextAnimateFrameEvent(this.tilesSceneObjectWithId.sceneObjectChanged, this.sceneObject.showChanged))
    get event() { return this._event; }

    // @ts-ignore
    // private _tilesIdResetting = this.disposeVar(new ObjResettingWithEvent(this.event, () => {

    //     const { sceneObject } = this.tilesSceneObjectWithId;
    //     if (!sceneObject) return undefined;
    //     if (!(sceneObject instanceof ES3DTileset)) return undefined;
    //     if (!this.sceneObject.show) return;
    //     console.log("ggg",sceneObject);

    //     return new TilesIdResetting(this, sceneObject as ES3DTileset);
    // }));

    constructor(sceneObject: T, czmViewer: CzmViewer) {
        super(sceneObject, czmViewer);
        // this.viewer = czmViewer
        const viewer = czmViewer.viewer;
        if (!viewer) {
            console.warn(`viewer is undefined!`);
            return;
        }

        const polygonFlattenedPlane = this._polygonFlattenedPlane;
        czmViewer.add(polygonFlattenedPlane);
        this.dispose(() => czmViewer.delete(polygonFlattenedPlane))

        polygonFlattenedPlane.enabled = true
        this.dispose(track([polygonFlattenedPlane, 'show'], [sceneObject, 'show']));
        this.dispose(bind([polygonFlattenedPlane, 'positions'], [sceneObject, 'points']));
        this.dispose(track([polygonFlattenedPlane, 'allowPicking'], [sceneObject, 'allowPicking']));
        this.dispose(bind([polygonFlattenedPlane, 'editing'], [sceneObject, 'editing']));
        this.dispose(bind([polygonFlattenedPlane, 'czmFlattenedPlaneId'], [sceneObject, 'czmFlattenedPlaneId']));
        this.dispose(track([polygonFlattenedPlane.polygon, 'outline'], [sceneObject, 'stroked']));
        this.dispose(track([polygonFlattenedPlane.polygon, 'outlineColor'], [sceneObject, 'strokeColor']));
        this.dispose(track([polygonFlattenedPlane.polygon, 'outlineWidth'], [sceneObject, 'strokeWidth']));
        this.dispose(track([polygonFlattenedPlane.polygon, 'fill'], [sceneObject, 'filled']));
        this.dispose(track([polygonFlattenedPlane.polygon, 'color'], [sceneObject, 'fillColor']));
        this.dispose(track([polygonFlattenedPlane.polygon, 'strokeGround'], [sceneObject, 'strokeGround']));
        this.dispose(track([polygonFlattenedPlane.polygon, 'ground'], [sceneObject, 'fillGround']));

        this.disposeVar(new ObjResettingWithEvent(this.event, () => {

            const { sceneObject } = this.tilesSceneObjectWithId;
            if (!sceneObject) return undefined;
            if (!(sceneObject instanceof ES3DTileset)) return undefined;
            if (!this.sceneObject.show) return;

            return new TilesIdResetting(this, sceneObject as ES3DTileset);
        }));

    }

    override flyTo(duration: number | undefined, id: number): boolean {
        const { sceneObject, czmViewer, polygonFlattenedPlane } = this;
        if (!czmViewer.actived) return false;
        if (sceneObject.flyToParam || sceneObject.flyInParam) {
            return super.flyTo(duration, id);
        } else {
            if (polygonFlattenedPlane.positions) {
                flyWithPositions(czmViewer, sceneObject, id, polygonFlattenedPlane.positions, duration);
                return true;
            }
            return false;
        }
    }
}
