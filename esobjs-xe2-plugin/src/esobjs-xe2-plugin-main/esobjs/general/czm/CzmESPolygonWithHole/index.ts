import { CzmESGeoPolygon } from "../CzmESGeoPolygon";
import { GeoPolylines, PolygonHierarchyType, PositionsEditing, SceneObjectPickedInfo, getMinMaxCorner } from 'xbsj-xe2/dist-node/xe2-base-objects';
import { bind, createNextAnimateFrameEvent, track } from "xbsj-xe2/dist-node/xe2-base-utils";
import { CzmPolygonPrimitive, CzmViewer } from 'xbsj-xe2/dist-node/xe2-cesium-objects';
import { ESPolygonWithHole } from '../../objs';
import { getCzmPickedInfoFromPickedInfo } from "../base/utils";
import { flyWithPositions, getPointerEventButton } from "../base";

export class CzmESPolygonWithHole extends CzmESGeoPolygon<ESPolygonWithHole> {
    static override readonly type = this.register(ESPolygonWithHole.type, this);
    private _czmPolygonPrimitive = this.disposeVar(new CzmPolygonPrimitive());
    get czmPolygonPrimitive() { return this._czmPolygonPrimitive; }

    private _geoPolylines = this.disposeVar(new GeoPolylines());

    private _sPositionsEditing = this.disposeVar(new PositionsEditing([this.sceneObject, 'points'], true, [this.sceneObject, 'editing'], this.czmViewer));
    get sPositionsEditing() { return this._sPositionsEditing; }

    constructor(sceneObject: ESPolygonWithHole, czmViewer: CzmViewer) {
        super(sceneObject, czmViewer);
        const viewer = czmViewer.viewer;
        if (!viewer) {
            console.warn(`viewer is undefined!`);
            return;
        }
        this.geoPolygon.show = false;

        const geoPolylines = this._geoPolylines;
        geoPolylines.arcType = 'RHUMB'
        czmViewer.add(geoPolylines);
        this.dispose(() => czmViewer.delete(geoPolylines))

        const czmPolygonPrimitive = this._czmPolygonPrimitive;
        czmViewer.add(czmPolygonPrimitive);
        this.dispose(() => czmViewer.delete(czmPolygonPrimitive))

        this.dispose(track([czmPolygonPrimitive, 'allowPicking'], [sceneObject, 'allowPicking']));

        this.dispose(track([geoPolylines, 'allowPicking'], [sceneObject, 'allowPicking']));
        this.dispose(track([geoPolylines, 'color'], [sceneObject, 'strokeColor']));
        this.dispose(track([geoPolylines, 'width'], [sceneObject, 'strokeWidth']));

        {
            czmPolygonPrimitive.perPositionHeight = true;

            const updateProp = () => {
                geoPolylines.show = sceneObject.show && sceneObject.stroked;
                czmPolygonPrimitive.show = sceneObject.show && sceneObject.filled;
                czmPolygonPrimitive.material = {
                    type: 'Color',
                    color: sceneObject.fillColor
                };
            }
            updateProp()
            const updateEvent = this.disposeVar(createNextAnimateFrameEvent(
                sceneObject.showChanged,
                sceneObject.strokedChanged,
                sceneObject.filledChanged,
                sceneObject.fillColorChanged,
            ));
            this.dispose(updateEvent.don(updateProp));
        }

        this.dispose(czmPolygonPrimitive.pickedEvent.disposableOn(pickedInfo => {
            if (getPointerEventButton(pickedInfo) === 0 && (sceneObject.allowPicking ?? false)) {
                const pickInfo = getCzmPickedInfoFromPickedInfo(pickedInfo)
                sceneObject.pickedEvent.emit(new SceneObjectPickedInfo(sceneObject, pickInfo));
            }
        }));
        {
            const update = () => {
                const polygonHierarchy = {
                    positions: [] as [number, number, number][],
                    holes: [] as PolygonHierarchyType[]
                }
                if (sceneObject.points && sceneObject.points.length >= 3) {
                    geoPolylines.positions = [[...sceneObject.points, sceneObject.points[0]]]
                    polygonHierarchy.positions = sceneObject.points;
                    if (sceneObject.innerRings && sceneObject.innerRings.length > 0) {
                        for (let i = 0; i < sceneObject.innerRings.length; i++) {
                            const item = sceneObject.innerRings[i];
                            polygonHierarchy.holes.push({ positions: item });
                            geoPolylines.positions.push([...item, item[0]])
                        }
                    }
                    czmPolygonPrimitive.polygonHierarchy = polygonHierarchy;
                } else {
                    czmPolygonPrimitive.polygonHierarchy = polygonHierarchy
                }
            }
            update()
            const event = this.disposeVar(createNextAnimateFrameEvent(
                sceneObject.pointsChanged,
                sceneObject.innerRingsChanged));
            this.dispose(event.disposableOn(() => update()));
        }

    }
    override flyTo(duration: number | undefined, id: number): boolean {
        const { sceneObject, czmViewer, czmPolygonPrimitive } = this;
        if (!czmViewer.actived) return false;
        if (sceneObject.flyToParam || sceneObject.flyInParam) {
            return super.flyTo(duration, id);
        } else {
            if (czmPolygonPrimitive.positions) {
                flyWithPositions(czmViewer, sceneObject, id, czmPolygonPrimitive.positions, duration);
                return true;
            }
            return false;
        }
    }
    override flyIn(duration: number | undefined, id: number): boolean {
        const { sceneObject, czmViewer, czmPolygonPrimitive } = this;
        if (!czmViewer.actived) return false;
        if (sceneObject.flyInParam) {
            return super.flyIn(duration, id);
        } else {
            if (czmPolygonPrimitive.positions) {
                flyWithPositions(czmViewer, sceneObject, id, czmPolygonPrimitive.positions, duration);
                return true;
            }
            return false;
        }
    }
}
