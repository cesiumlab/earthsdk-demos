import { SceneObject } from 'xbsj-xe2/dist-node/xe2-base-objects';
import { ESWidget } from '../../objs';
import { ObjResettingWithEvent, createNextAnimateFrameEvent } from 'xbsj-xe2/dist-node/xe2-utils';
import { CzmViewer } from 'xbsj-xe2/dist-node/xe2-cesium-objects';
import { CzmESObjectWithLocation, } from '@/esobjs-xe2-plugin-main/esobjs/base';
import { getWidgetDiv, imgUrl, Widget3D, Widget2D } from '../base';

export class CzmESWidget extends CzmESObjectWithLocation<ESWidget> {
    static readonly type = this.register(ESWidget.type, this);

    // 存储widget组件
    private _widgetTemp: any;
    get widgetTemp() { return this._widgetTemp; }
    set widgetTemp(val) { this._widgetTemp = val }

    constructor(sceneObject: ESWidget, czmViewer: CzmViewer) {
        super(sceneObject, czmViewer);
        const viewer = czmViewer.viewer;
        if (!viewer) {
            console.warn(`viewer is undefined!`);
            return;
        }
        (async () => {
            const basePath = "${esobjs-xe2-plugin-assets-script-dir}/xe2-assets/esobjs-xe2-plugin/images/Info/";
            const InfoBackGround = await imgUrl(SceneObject.context.getStrFromEnv(basePath + '/InfoBackGround.png'));
            const InfoItemRowBackGround = await imgUrl(SceneObject.context.getStrFromEnv(basePath + '/InfoItemRowBackGround.png'))
            // 屏幕世界模式切换
            {
                const event = this.dv(createNextAnimateFrameEvent(sceneObject.screenRenderChanged, sceneObject.infoChanged));
                this.widgetTemp = this.disposeVar(new ObjResettingWithEvent(event, () => {
                    const div = getWidgetDiv(sceneObject, InfoItemRowBackGround, InfoBackGround);
                    if (sceneObject.screenRender) {
                        return new Widget2D(sceneObject, czmViewer, div, Object.keys(sceneObject.info).length != 0);
                    } else {
                        return new Widget3D(sceneObject, czmViewer, div, Object.keys(sceneObject.info).length != 0);
                    }
                }))
            }
        })()
    }
    override flyTo(duration: number | undefined, id: number): boolean {
        const { sceneObject, czmViewer, widgetTemp } = this;
        if (!czmViewer.actived) return false;
        if (sceneObject.flyToParam || sceneObject.flyInParam) {
            return super.flyTo(duration, id);
        } else {
            if (widgetTemp) {
                widgetTemp.obj.flyTo(duration, id);
            } else {
                const time = setTimeout(() => {
                    clearTimeout(time);
                    this.flyTo(duration, id);
                }, 200)
            }
            sceneObject.flyOverEvent.emit(id, 'over', czmViewer);
            return true;
        }
    }
}