import { CzmESObjectWithLocation, bindNorthRotation } from "@/esobjs-xe2-plugin-main/esobjs/base";
import { SceneObject, SceneObjectPickedInfo } from "xbsj-xe2/dist-node/xe2-base-objects";
import { bind, createNextAnimateFrameEvent, track } from "xbsj-xe2/dist-node/xe2-base-utils";
import { CzmModelPrimitive, CzmViewer } from 'xbsj-xe2/dist-node/xe2-cesium-objects';
import { ESCar } from '../../objs';
import { getCzmPickedInfoFromPickedInfo } from "../base/utils";
import { flyWithPrimitive, getPointerEventButton } from "../base";

export class CzmESCar extends CzmESObjectWithLocation<ESCar> {
    static readonly type = this.register(ESCar.type, this);
    private _czmModelPrimitive = this.disposeVar(new CzmModelPrimitive());
    get czmModelPrimitive() { return this._czmModelPrimitive; }

    constructor(sceneObject: ESCar, czmViewer: CzmViewer) {
        super(sceneObject, czmViewer);
        const viewer = czmViewer.viewer;
        if (!viewer) {
            console.warn(`viewer is undefined!`);
            return;
        }
        const czmModelPrimitive = this._czmModelPrimitive;
        czmViewer.add(czmModelPrimitive);
        this.dispose(() => czmViewer.delete(czmModelPrimitive))

        this.dispose(track([czmModelPrimitive, 'show'], [sceneObject, 'show']));
        this.dispose(bind([czmModelPrimitive, 'position'], [sceneObject, 'position']));
        this.dispose(bindNorthRotation([czmModelPrimitive, 'rotation'], [sceneObject, 'rotation']));
        this.dispose(bind([czmModelPrimitive, 'scale'], [sceneObject, 'scale']));
        {
            const event = this.dv(createNextAnimateFrameEvent(sceneObject.allowPickingChanged, sceneObject.editingChanged))
            const update = () => {
                if (sceneObject.allowPicking && !sceneObject.editing) {
                    czmModelPrimitive.allowPicking = true;
                } else {
                    czmModelPrimitive.allowPicking = false;
                }
            }
            update();
            this.d(event.don(update));
        }

        const policeCarUrl = SceneObject.context.getStrFromEnv('${esobjs-xe2-plugin-assets-script-dir}/xe2-assets/esobjs-xe2-plugin/glbs/PoliceCar.glb')

        const update = () => {
            const mode = sceneObject.mode;
            switch (mode) {
                case 'policeCar':
                    czmModelPrimitive.url = policeCarUrl;
                    break;
                default:
                    czmModelPrimitive.url = policeCarUrl;
                    break;
            }
        };
        this.dispose(sceneObject.modeChanged.disposableOn(() => update()));
        update();

        this.dispose(czmModelPrimitive.pickedEvent.disposableOn(pickedInfo => {
            if (getPointerEventButton(pickedInfo) === 0 && (sceneObject.allowPicking ?? false)) {
                const pickInfo = getCzmPickedInfoFromPickedInfo(pickedInfo)
                sceneObject.pickedEvent.emit(new SceneObjectPickedInfo(sceneObject, pickInfo));
            }
        }));

        // this.dispose(sceneObject.smoothMoveEvent.disposableOn((Destination, Time) => {
        //     const time = Time * 1000;
        //     czmModelPrimitive.smoothMove(Destination, time);
        // }));
    }
    override flyTo(duration: number | undefined, id: number): boolean {
        const { sceneObject, czmViewer, czmModelPrimitive } = this;
        if (!czmViewer.actived) return false;
        if (sceneObject.flyToParam || sceneObject.flyInParam) {
            return super.flyTo(duration, id);
        } else {
            flyWithPrimitive(czmViewer, sceneObject, id, duration, czmModelPrimitive, true);
            return true;
        }
    }
}
