
import { CzmESObjectWithLocation, bindNorthRotation } from "@/esobjs-xe2-plugin-main/esobjs/base";
import { SceneObject, SceneObjectPickedInfo } from "xbsj-xe2/dist-node/xe2-base-objects";
import { bind, createNextAnimateFrameEvent, track } from "xbsj-xe2/dist-node/xe2-base-utils";
import { CzmCzmModelPrimitive, CzmModelAnimationJsonType, CzmModelPrimitive, CzmViewer } from 'xbsj-xe2/dist-node/xe2-cesium-objects';
import { ESHuman } from '../../objs';
import { getCzmPickedInfoFromPickedInfo } from "../base/utils";
import { flyWithPrimitive, getPointerEventButton } from "../base";
import * as Cesium from 'cesium';

export class CzmESHuman extends CzmESObjectWithLocation<ESHuman> {
    static readonly type = this.register(ESHuman.type, this);
    private _czmModelPrimitive = this.disposeVar(new CzmModelPrimitive());
    get czmModelPrimitive() { return this._czmModelPrimitive; }

    constructor(sceneObject: ESHuman, czmViewer: CzmViewer) {
        super(sceneObject, czmViewer);
        const viewer = czmViewer.viewer;
        if (!viewer) {
            console.warn(`viewer is undefined!`);
            return;
        }
        const czmModelPrimitive = this._czmModelPrimitive;
        czmViewer.add(czmModelPrimitive);
        this.dispose(() => czmViewer.delete(czmModelPrimitive))
        this.dispose(track([czmModelPrimitive, 'show'], [sceneObject, 'show']));
        this.dispose(bind([czmModelPrimitive, 'position'], [sceneObject, 'position']));
        this.dispose(bindNorthRotation([czmModelPrimitive, 'rotation'], [sceneObject, 'rotation']));
        this.dispose(bind([czmModelPrimitive, 'scale'], [sceneObject, 'scale']));
        {
            const event = this.dv(createNextAnimateFrameEvent(sceneObject.allowPickingChanged, sceneObject.editingChanged))
            const update = () => {
                if (sceneObject.allowPicking && !sceneObject.editing) {
                    czmModelPrimitive.allowPicking = true;
                } else {
                    czmModelPrimitive.allowPicking = false;
                }
            }
            update();
            this.d(event.don(update));
        }
        const workerUrl = SceneObject.context.getStrFromEnv('${esobjs-xe2-plugin-assets-script-dir}/xe2-assets/esobjs-xe2-plugin/glbs/build_worker/worker.glb')
        const policeUrl = SceneObject.context.getStrFromEnv('${esobjs-xe2-plugin-assets-script-dir}/xe2-assets/esobjs-xe2-plugin/glbs/build_worker/police.glb')
        const pedestrianUrl = SceneObject.context.getStrFromEnv('${esobjs-xe2-plugin-assets-script-dir}/xe2-assets/esobjs-xe2-plugin/glbs/build_worker/pedestrian.glb')
        const strangerUrl = SceneObject.context.getStrFromEnv('${esobjs-xe2-plugin-assets-script-dir}/xe2-assets/esobjs-xe2-plugin/glbs/build_worker/Stranger.glb')
        const suitManUrl = SceneObject.context.getStrFromEnv('${esobjs-xe2-plugin-assets-script-dir}/xe2-assets/esobjs-xe2-plugin/glbs/build_worker/SuitsMan.glb')
        const suitWomanUrl = SceneObject.context.getStrFromEnv('${esobjs-xe2-plugin-assets-script-dir}/xe2-assets/esobjs-xe2-plugin/glbs/build_worker/SuitsWoman.glb')

        const update = () => {
            const mode = sceneObject.mode;
            switch (mode) {
                case 'worker':
                    czmModelPrimitive.url = workerUrl;
                    break;
                case 'police':
                    czmModelPrimitive.url = policeUrl;
                    break;
                case 'pedestrian':
                    czmModelPrimitive.url = pedestrianUrl;
                    break;
                case 'stranger':
                    czmModelPrimitive.url = strangerUrl;
                    break;
                case 'suitMan':
                    czmModelPrimitive.url = suitManUrl;
                    break;
                case 'suitWoman':
                    czmModelPrimitive.url = suitWomanUrl;
                    break;
                default:
                    czmModelPrimitive.url = workerUrl;
                    break;
            }
        };

        this.dispose(sceneObject.modeChanged.disposableOn(() => {
            update()
        }));
        update();

        this.dispose(czmModelPrimitive.pickedEvent.disposableOn(pickedInfo => {
            if (getPointerEventButton(pickedInfo) === 0 && (sceneObject.allowPicking ?? false)) {
                const pickInfo = getCzmPickedInfoFromPickedInfo(pickedInfo)
                sceneObject.pickedEvent.emit(new SceneObjectPickedInfo(sceneObject, pickInfo));
            }
        }));

        const updateAnimation = () => {
            const animation = sceneObject.animation ?? ESHuman.defaults.animation;
            if (!sceneObject.show) {
                czmModelPrimitive.activeAnimationsJson = undefined;
                return;
            }
            if (animation === 'walking') {
                czmModelPrimitive.activeAnimationsJson = ESHuman.defaults.czmAnimationsWalk as CzmModelAnimationJsonType[]
            } else if (animation === 'standing') {
                czmModelPrimitive.activeAnimationsJson = ESHuman.defaults.czmAnimationsStand as CzmModelAnimationJsonType[]
            } else if (animation === 'running') {
                czmModelPrimitive.activeAnimationsJson = ESHuman.defaults.czmAnimationsRun as CzmModelAnimationJsonType[]
            }
        }
        updateAnimation()
        const event = this.ad(createNextAnimateFrameEvent(
            sceneObject.showChanged,
            sceneObject.animationChanged
        ))
        this.dispose(event.disposableOn(() => updateAnimation()));
        this.dispose(viewer.scene.preUpdate.addEventListener(() => {
            const czmCzmModelPrimitive = czmViewer.getCzmObject(czmModelPrimitive) as CzmCzmModelPrimitive;
            const primitive = czmCzmModelPrimitive.primitive;
            const { scene, camera } = viewer;
            if (primitive && primitive.ready) {
                // 看看包围球占几个像素
                const pixelSize = primitive.boundingSphere.radius * 2 / camera.getPixelSize(
                    primitive.boundingSphere,
                    scene.drawingBufferWidth,
                    scene.drawingBufferHeight
                );
                const cullingVolume = viewer.camera.frustum.computeCullingVolume(camera.position, camera.direction, camera.up);
                const intersect = cullingVolume.computeVisibility(primitive.boundingSphere);
                czmModelPrimitive.activeAnimationsAnimateWhilePaused = !(intersect === Cesium.Intersect.OUTSIDE) && pixelSize > 5;
            }
        }));
    }
    override flyTo(duration: number | undefined, id: number): boolean {
        const { sceneObject, czmViewer, czmModelPrimitive } = this;
        if (!czmViewer.actived || !czmViewer.viewer) return false;
        if (sceneObject.flyToParam || sceneObject.flyInParam) {
            return super.flyTo(duration, id);
        } else {
            flyWithPrimitive(czmViewer, sceneObject, id, duration, czmModelPrimitive, true);
            return true;
        }
    }
}
