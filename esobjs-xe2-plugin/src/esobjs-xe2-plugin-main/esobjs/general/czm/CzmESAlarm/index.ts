
import { bind, createNextAnimateFrameEvent, track } from "xbsj-xe2/dist-node/xe2-base-utils";
import { CzmViewer } from 'xbsj-xe2/dist-node/xe2-cesium-objects';
import { SceneObject, SceneObjectPickedInfo } from "xbsj-xe2/dist-node/xe2-base-objects";
import { CzmModelAnimationJsonType, CzmModelPrimitive } from "xbsj-xe2/dist-node/xe2-cesium-objects";
import { ESAlarm } from "../../objs";
import { CzmESObjectWithLocation, bindNorthRotation } from "@/esobjs-xe2-plugin-main/esobjs/base";
import { getCzmPickedInfoFromPickedInfo } from "../base/utils";
import { ESCzmViewer } from "../../../../ESViewers";
import { flyWithPrimitive, getPointerEventButton } from "../base";

export class CzmESAlarm extends CzmESObjectWithLocation<ESAlarm> {
    static readonly type = this.register(ESAlarm.type, this);
    private _czmModelPrimitive = this.disposeVar(new CzmModelPrimitive());
    get czmModelPrimitive() { return this._czmModelPrimitive; }

    constructor(sceneObject: ESAlarm, czmViewer: CzmViewer) {
        super(sceneObject, czmViewer);
        const viewer = czmViewer.viewer;
        if (!viewer) {
            console.warn(`viewer is undefined!`);
            return;
        }
        const esCzmViewer = ESCzmViewer.getESCzmViewer(czmViewer);
        console.log(esCzmViewer);

        const czmModelPrimitive = this._czmModelPrimitive;
        czmModelPrimitive.activeAnimationsAnimateWhilePaused = true
        czmModelPrimitive.activeAnimationsJson = ESAlarm.defaults.czmAnimations as CzmModelAnimationJsonType[]

        czmViewer.add(czmModelPrimitive);
        this.dispose(() => czmViewer.delete(czmModelPrimitive))
        this.dispose(track([czmModelPrimitive, 'show'], [sceneObject, 'show']));
        this.dispose(bind([czmModelPrimitive, 'position'], [sceneObject, 'position']));
        this.dispose(bindNorthRotation([czmModelPrimitive, 'rotation'], [sceneObject, 'rotation']));
        // this.d(track([czmModelPrimitive, 'scale'], [sceneObject, 'scale']));
        {
            const event = this.dv(createNextAnimateFrameEvent(sceneObject.allowPickingChanged, sceneObject.editingChanged))
            const update = () => {
                if (sceneObject.allowPicking && !sceneObject.editing) {
                    czmModelPrimitive.allowPicking = true;
                } else {
                    czmModelPrimitive.allowPicking = false;
                }
            }
            update();
            this.d(event.don(update));
        }
        //圆形警告
        const circleUrl = SceneObject.context.getStrFromEnv('${esobjs-xe2-plugin-assets-script-dir}/xe2-assets/esobjs-xe2-plugin/glbs/warning/warning_b.glb')
        //柱状警告
        const cylinderUrl = SceneObject.context.getStrFromEnv('${esobjs-xe2-plugin-assets-script-dir}/xe2-assets/esobjs-xe2-plugin/glbs/warning/warning_a.glb')

        {
            const updateMode = () => {
                const mode = sceneObject.mode;
                if (mode === 'circle') {
                    czmModelPrimitive.url = circleUrl;
                } else if (mode === 'cylinder' || mode === undefined) {
                    czmModelPrimitive.url = cylinderUrl;
                }
            };
            this.dispose(sceneObject.modeChanged.disposableOn(() => {
                updateMode()
            }));
            updateMode();
        }


        {
            const updateRadius = () => {
                const radius = sceneObject.radius ?? ESAlarm.defaults.radius;
                czmModelPrimitive.scale = [radius * sceneObject.scale[0], radius * sceneObject.scale[1], radius * sceneObject.scale[2]];
            };
            const event = this.dv(createNextAnimateFrameEvent(sceneObject.radiusChanged, sceneObject.scaleChanged))
            this.dispose(event.disposableOn(() => {
                updateRadius()
            }));
            updateRadius();
        }

        this.dispose(czmModelPrimitive.pickedEvent.disposableOn(pickedInfo => {
            if (getPointerEventButton(pickedInfo) === 0 && (sceneObject.allowPicking ?? false)) {
                const pickInfo = getCzmPickedInfoFromPickedInfo(pickedInfo)
                sceneObject.pickedEvent.emit(new SceneObjectPickedInfo(sceneObject, pickInfo));
            }
        }));
        //触发创建完成事件
        sceneObject.ueCreatedEvent.emit();
    }
    // 重写flyTo方法,基类监听flyToEvent后飞行执行此方法
    override flyTo(duration: number | undefined, id: number) {
        const { sceneObject, czmViewer, czmModelPrimitive } = this;
        if (!czmViewer.actived || !czmViewer.viewer) return false;
        if (sceneObject.flyToParam || sceneObject.flyInParam) {
            return super.flyTo(duration, id);
        } else {
            flyWithPrimitive(czmViewer, sceneObject, id, duration, czmModelPrimitive, true);
            return true;
        }
    }
}
