
import { CzmESObjectWithLocation, bindNorthRotation } from "@/esobjs-xe2-plugin-main/esobjs/base";
import { SceneObjectPickedInfo } from "xbsj-xe2/dist-node/xe2-base-objects";
import { track } from "xbsj-xe2/dist-node/xe2-base-utils";
import { CzmCustomPrimitive, CzmViewer } from 'xbsj-xe2/dist-node/xe2-cesium-objects';
import { ESCustomPrimitive } from "../../objs";
import { getCzmPickedInfoFromPickedInfo } from "../base/utils";
import { flyWithPrimitive, getPointerEventButton } from "../base";

export class CzmESCustomPrimitive extends CzmESObjectWithLocation<ESCustomPrimitive> {
    static readonly type = this.register(ESCustomPrimitive.type, this);

    private _czmCustomPrimitive = this.disposeVar(new CzmCustomPrimitive());
    get czmCustomPrimitive() { return this._czmCustomPrimitive; }

    constructor(sceneObject: ESCustomPrimitive, czmViewer: CzmViewer) {
        super(sceneObject, czmViewer);
        const viewer = czmViewer.viewer;
        if (!viewer) {
            console.warn(`viewer is undefined!`);
            return;
        }

        const czmCustomPrimitive = this._czmCustomPrimitive;
        czmViewer.add(czmCustomPrimitive);
        this.dispose(() => czmViewer.delete(czmCustomPrimitive));

        this.dispose(track([czmCustomPrimitive, 'show'], [sceneObject, 'show']));
        this.dispose(track([czmCustomPrimitive, 'allowPicking'], [sceneObject, 'allowPicking']));
        this.dispose(bindNorthRotation([czmCustomPrimitive, 'rotation'], [sceneObject, 'rotation']));
        this.dispose(track([czmCustomPrimitive, 'position'], [sceneObject, 'position']));
        this.dispose(track([czmCustomPrimitive, 'scale'], [sceneObject, 'scale']));
        this.dispose(czmCustomPrimitive.pickedEvent.disposableOn(pickedInfo => {
            if (getPointerEventButton(pickedInfo) === 0 && (sceneObject.allowPicking ?? false)) {
                const pickInfo = getCzmPickedInfoFromPickedInfo(pickedInfo)
                sceneObject.pickedEvent.emit(new SceneObjectPickedInfo(sceneObject, pickInfo));
            }
        }));

        this.dispose(track([czmCustomPrimitive, 'viewDistanceRange'], [sceneObject, 'viewDistanceRange']));
        this.dispose(track([czmCustomPrimitive, 'viewDistanceDebug'], [sceneObject, 'viewDistanceDebug']));
        this.dispose(track([czmCustomPrimitive, 'maximumScale'], [sceneObject, 'maximumScale']));
        this.dispose(track([czmCustomPrimitive, 'minimumScale'], [sceneObject, 'minimumScale']));
        this.dispose(track([czmCustomPrimitive, 'pixelSize'], [sceneObject, 'pixelSize']));
        this.dispose(track([czmCustomPrimitive, 'showSceneScale'], [sceneObject, 'showSceneScale']));

        this.dispose(track([czmCustomPrimitive, 'modelMatrix'], [sceneObject, 'modelMatrix']));
        this.dispose(track([czmCustomPrimitive, 'cull'], [sceneObject, 'cull']));
        this.dispose(track([czmCustomPrimitive, 'boundingVolume'], [sceneObject, 'boundingVolume']));
        this.dispose(track([czmCustomPrimitive, 'pass'], [sceneObject, 'pass']));
        this.dispose(track([czmCustomPrimitive, 'primitiveType'], [sceneObject, 'primitiveType']));
        this.dispose(track([czmCustomPrimitive, 'renderState'], [sceneObject, 'renderState']));
        this.dispose(track([czmCustomPrimitive, 'vertexShaderSource'], [sceneObject, 'vertexShaderSource']));
        this.dispose(track([czmCustomPrimitive, 'fragmentShaderSource'], [sceneObject, 'fragmentShaderSource']));
        this.dispose(track([czmCustomPrimitive, 'uniformMap'], [sceneObject, 'uniformMap']));
        this.dispose(track([czmCustomPrimitive, 'attributes'], [sceneObject, 'attributes']));
        this.dispose(track([czmCustomPrimitive, 'indexTypedArray'], [sceneObject, 'indexTypedArray']));
        this.dispose(track([czmCustomPrimitive, 'attributesJson'], [sceneObject, 'attributesJson']));
        this.dispose(track([czmCustomPrimitive, 'indexTypedArrayJson'], [sceneObject, 'indexTypedArrayJson']));
        this.dispose(track([czmCustomPrimitive, 'count'], [sceneObject, 'count']));
        this.dispose(track([czmCustomPrimitive, 'offset'], [sceneObject, 'offset']));
        this.dispose(track([czmCustomPrimitive, 'instanceCount'], [sceneObject, 'instanceCount']));

        this.dispose(track([czmCustomPrimitive, 'localPosition'], [sceneObject, 'localPosition']));
        this.dispose(track([czmCustomPrimitive, 'localScale'], [sceneObject, 'localScale']));
        this.dispose(track([czmCustomPrimitive, 'localModelMatrix'], [sceneObject, 'localModelMatrix']));
        this.dispose(track([czmCustomPrimitive, 'debugShowBoundingVolume'], [sceneObject, 'debugShowBoundingVolume']));
        this.dispose(track([czmCustomPrimitive, 'debugOverlappingFrustums'], [sceneObject, 'debugOverlappingFrustums']));
        this.dispose(track([czmCustomPrimitive, 'occlude'], [sceneObject, 'occlude']));
        this.dispose(track([czmCustomPrimitive, 'castShadows'], [sceneObject, 'castShadows']));
        this.dispose(track([czmCustomPrimitive, 'receiveShadows'], [sceneObject, 'receiveShadows']));
        this.dispose(track([czmCustomPrimitive, 'executeInClosestFrustum'], [sceneObject, 'executeInClosestFrustum']));
        this.dispose(track([czmCustomPrimitive, 'pickOnly'], [sceneObject, 'pickOnly']));
        this.dispose(track([czmCustomPrimitive, 'depthForTranslucentClassification'], [sceneObject, 'depthForTranslucentClassification']));

        {
            // 单向监听,旋转90度
            const updated = () => {
                if (sceneObject.localRotation) {
                    // 复制本地偏移
                    const localRotation: [number, number, number] = [...sceneObject.localRotation];
                    localRotation[0] -= 90;
                    czmCustomPrimitive.localRotation = localRotation;
                } else {
                    czmCustomPrimitive.localRotation = [-90, 0, 0];
                }
            }
            updated();
            this.dispose(sceneObject.localRotationChanged.disposableOn(updated));
        }
    }
    override flyTo(duration: number | undefined, id: number): boolean {
        const { sceneObject, czmViewer, czmCustomPrimitive } = this;
        if (!czmViewer.actived) return false;
        if (sceneObject.flyToParam || sceneObject.flyInParam) {
            return super.flyTo(duration, id);
        } else {
            flyWithPrimitive(czmViewer, sceneObject, id, duration, czmCustomPrimitive, true);
            return true;
        }
    }
}
