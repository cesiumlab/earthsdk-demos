import { CzmESObjectWithLocation } from '@/esobjs-xe2-plugin-main/esobjs/base';
import { GeoPoint } from 'xbsj-xe2/dist-node/xe2-base-objects';
import { bind, track } from 'xbsj-xe2/dist-node/xe2-base-utils';
import { CzmViewer } from 'xbsj-xe2/dist-node/xe2-cesium-objects';
import { ESUnrealActor } from '../../objs';

export class CzmESUnrealActor extends CzmESObjectWithLocation<ESUnrealActor> {
    static readonly type = this.register(ESUnrealActor.type, this);
    private _czmGeoPoint = this.disposeVar(new GeoPoint());
    get czmGeoPoint() { return this._czmGeoPoint; }

    constructor(sceneObject: ESUnrealActor, czmViewer: CzmViewer) {
        super(sceneObject, czmViewer);
        const viewer = czmViewer.viewer;
        if (!viewer) {
            console.warn(`viewer is undefined!`);
            return;
        }
        const czmGeoPoint = this._czmGeoPoint;
        czmViewer.add(czmGeoPoint);
        this.dispose(() => czmViewer.delete(czmGeoPoint))

        this.dispose(track([czmGeoPoint, 'show'], [sceneObject, 'show']));
        this.dispose(bind([czmGeoPoint, 'position'], [sceneObject, 'position']));
    }
    override flyTo(duration: number | undefined, id: number): boolean {
        const { sceneObject, czmViewer, czmGeoPoint } = this;
        if (!czmViewer.actived) return false;
        if (sceneObject.flyToParam || sceneObject.flyInParam) {
            return super.flyTo(duration, id);
        } else {
            czmGeoPoint.flyTo(duration && duration * 1000);
            sceneObject.flyOverEvent.emit(id, 'over', czmViewer);
            return true;
        }
    }
}
