import { CzmESGeoVector, WaterAttribute } from '@/esobjs-xe2-plugin-main/esobjs/base';
import { createNextAnimateFrameEvent, track } from 'xbsj-renderer/dist-node/xr-base-utils';
import { GeoPolyline, PositionsEditing, SceneObjectPickedInfo } from 'xbsj-xe2/dist-node/xe2-base-objects';
import { CzmViewer, CzmWater } from 'xbsj-xe2/dist-node/xe2-cesium-objects';
import { ESGeoWater } from '../../objs';
import { getCzmPickedInfoFromPickedInfo } from '../base/utils';
import { flyWithPositions, getPointerEventButton, waterType } from '../base';
export class CzmESGeoWater extends CzmESGeoVector<ESGeoWater> {
    static readonly type = this.register(ESGeoWater.type, this);

    private _geoPolyline = this.disposeVar(new GeoPolyline());
    get geoPolyline() { return this._geoPolyline; }

    private _czmWater = this.disposeVar(new CzmWater());
    get czmWater() { return this._czmWater; }

    private _sPositionsEditing = this.disposeVar(new PositionsEditing([this.sceneObject, 'points'], true, [this.sceneObject, 'editing'], this.czmViewer));
    get sPositionsEditing() { return this._sPositionsEditing; }

    constructor(sceneObject: ESGeoWater, czmViewer: CzmViewer) {
        super(sceneObject, czmViewer);
        const viewer = czmViewer.viewer;
        if (!viewer) {
            console.warn(`viewer is undefined!`);
            return;
        }

        const { geoPolyline, czmWater } = this
        czmViewer.add(geoPolyline);
        this.d(() => czmViewer.delete(geoPolyline));

        czmViewer.add(czmWater);
        this.d(() => czmViewer.delete(czmWater));


        this.d(czmWater.pickedEvent.don((pickedInfo) => {
            if (getPointerEventButton(pickedInfo) === 0 && (sceneObject.allowPicking ?? false)) {
                const pickInfo = getCzmPickedInfoFromPickedInfo(pickedInfo)
                sceneObject.pickedEvent.emit(new SceneObjectPickedInfo(sceneObject, pickInfo));
            }
        }));

        this.d(track([czmWater, 'allowPicking'], [sceneObject, 'allowPicking']));
        this.d(track([czmWater, 'ground'], [sceneObject, 'fillGround']));
        this.d(track([geoPolyline, 'allowPicking'], [sceneObject, 'allowPicking']));
        this.d(track([geoPolyline, 'color'], [sceneObject, 'strokeColor']));
        this.d(track([geoPolyline, 'width'], [sceneObject, 'strokeWidth']));
        this.d(track([geoPolyline, 'ground'], [sceneObject, 'strokeGround']));

        {
            const event = this.dv(createNextAnimateFrameEvent(
                sceneObject.waterColorChanged,
                sceneObject.waterImageChanged,
                sceneObject.frequencyChanged,
                sceneObject.waveVelocityChanged,
                sceneObject.amplitudeChanged,
                sceneObject.specularIntensityChanged,
                sceneObject.waterTypeChanged,
                sceneObject.flowDirectionChanged,
                sceneObject.flowSpeedChanged,
            ))
            const update = () => {
                if (sceneObject.waterType === 'custom') {
                    this.updateWater({
                        waterColor: sceneObject.waterColor ?? ESGeoWater.defaults.waterColor,
                        frequency: (sceneObject.frequency ?? ESGeoWater.defaults.frequency) / 10,
                        waveVelocity: (sceneObject.waveVelocity ?? ESGeoWater.defaults.waveVelocity) / 100,
                        amplitude: (sceneObject.amplitude ?? ESGeoWater.defaults.amplitude) * 100,
                        specularIntensity: sceneObject.specularIntensity ?? ESGeoWater.defaults.specularIntensity,
                        flowDirection: sceneObject.flowDirection ?? ESGeoWater.defaults.flowDirection,
                        flowSpeed: sceneObject.flowSpeed ?? ESGeoWater.defaults.flowSpeed,
                        waterImage: sceneObject.waterImage ?? ESGeoWater.defaults.waterImage,
                    });
                } else {
                    const waterAttribute = Object.assign({}, waterType[sceneObject.waterType]);
                    waterAttribute.frequency && (waterAttribute.frequency /= 10);
                    waterAttribute.waveVelocity && (waterAttribute.waveVelocity /= 100);
                    waterAttribute.amplitude && (waterAttribute.amplitude *= 100);
                    this.updateWater(waterAttribute)
                }
            }
            update();
            this.d(event.don(update));
        }
        {//line show
            const update = () => {
                geoPolyline.show = (sceneObject.show && sceneObject.stroked) ? true : false;
            };
            const updateEvent = this.disposeVar(createNextAnimateFrameEvent(
                sceneObject.showChanged,
                sceneObject.strokedChanged,
            ));
            this.d(updateEvent.don(update));
            update();
        }
        {//Water show  ground filled
            const update = () => {
                czmWater.show = sceneObject.show && sceneObject.filled;
            };
            const updateEvent = this.disposeVar(createNextAnimateFrameEvent(
                sceneObject.showChanged,
                sceneObject.filledChanged,
            ));
            this.d(updateEvent.don(update));
            update();
        }

        {
            // update positions
            const update = () => {
                if (sceneObject.points && sceneObject.points.length >= 3) {
                    geoPolyline.positions = [...sceneObject.points, sceneObject.points[0]];
                    czmWater.positions = [...sceneObject.points];
                } else {
                    geoPolyline.positions = sceneObject.points
                        && sceneObject.points.length >= 2
                        ? [...sceneObject.points, sceneObject.points[0]]
                        : [];
                    czmWater.positions = [];
                }
            };
            update();
            const updateEvent = this.disposeVar(createNextAnimateFrameEvent(
                sceneObject.pointsChanged,
            ));
            this.d(updateEvent.don(update));
        }
        // 通过positions传递editing状态
        this.ad(sceneObject.editingChanged.don((newVal) => {
            czmWater.updateBoundingSphere(newVal);
        }))
    }
    override flyTo(duration: number | undefined, id: number): boolean {
        const { sceneObject, czmViewer } = this;
        if (!czmViewer.actived) return false;
        if (sceneObject.flyToParam || sceneObject.flyInParam) {
            return super.flyTo(duration, id);
        } else {
            if (sceneObject.points) {
                flyWithPositions(czmViewer, sceneObject, id, sceneObject.points, duration);
                return true;
            }
            return false;
        }
    }
    private updateWater(updateAttribute: WaterAttribute) {
        const { czmWater } = this;
        if (updateAttribute.waterColor && (czmWater.baseWaterColor != updateAttribute.waterColor)) {
            czmWater.baseWaterColor = updateAttribute.waterColor
        }
        if (updateAttribute.specularIntensity && (czmWater.specularIntensity != updateAttribute.specularIntensity)) {
            czmWater.specularIntensity = updateAttribute.specularIntensity
        }
        if (updateAttribute.frequency && (czmWater.frequency != updateAttribute.frequency)) {
            czmWater.frequency = updateAttribute.frequency
        }
        if (updateAttribute.waveVelocity && (czmWater.animationSpeed != updateAttribute.waveVelocity)) {
            czmWater.animationSpeed = updateAttribute.waveVelocity
        }
        if (updateAttribute.amplitude && (czmWater.amplitude != updateAttribute.amplitude)) {
            czmWater.amplitude = updateAttribute.amplitude
        }
        if (updateAttribute.flowSpeed != undefined && (czmWater.flowSpeed != updateAttribute.flowSpeed)) {
            czmWater.flowSpeed = updateAttribute.flowSpeed
        }
        if (updateAttribute.waterImage && (czmWater.baseWaterImage != updateAttribute.waterImage)) {
            czmWater.baseWaterImage = updateAttribute.waterImage
        }
        if (updateAttribute.flowDirection) {
            czmWater.stRotation = updateAttribute.flowDirection;
        }
    }
}
