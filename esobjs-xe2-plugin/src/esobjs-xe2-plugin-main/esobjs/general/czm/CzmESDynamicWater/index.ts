import { CzmESLocalVector, WaterAttribute } from '@/esobjs-xe2-plugin-main/esobjs/base';
import { createNextAnimateFrameEvent, track } from 'xbsj-renderer/dist-node/xr-base-utils';
import { GeoPolyline, SceneObjectPickedInfo, getDistancesFromPositions, localPositionsToPositions } from 'xbsj-xe2/dist-node/xe2-base-objects';
import { CzmViewer, CzmWater, getCameraPosition, getViewerExtensions } from 'xbsj-xe2/dist-node/xe2-cesium-objects';
import { ESDynamicWater } from '../../objs';
import { getCzmPickedInfoFromPickedInfo } from '../base/utils';
import { flyWithPositions, getPointerEventButton, waterType } from '../base';

/**
 * https://www.wolai.com/earthsdk/f9Kycrmp1srzt2dJyzgUxr
 */
export class CzmESDynamicWater extends CzmESLocalVector<ESDynamicWater> {
    static readonly type = this.register(ESDynamicWater.type, this);

    private _czmGeoPolyline = this.disposeVar(new GeoPolyline());
    get czmGeoPolyline() { return this._czmGeoPolyline; }

    private _czmWater = this.disposeVar(new CzmWater());
    get czmWater() { return this._czmWater; }

    constructor(sceneObject: ESDynamicWater, czmViewer: CzmViewer) {
        super(sceneObject, czmViewer);
        const viewer = czmViewer.viewer;
        if (!viewer) {
            console.warn(`viewer is undefined!`);
            return;
        }

        const extensions = getViewerExtensions(czmViewer.viewer);
        if (!extensions) return;

        const { czmGeoPolyline, czmWater } = this
        czmViewer.add(czmGeoPolyline);
        this.d(() => czmViewer.delete(czmGeoPolyline));

        czmViewer.add(czmWater);
        this.d(() => czmViewer.delete(czmWater));

        this.d(czmWater.pickedEvent.don((pickedInfo) => {
            if (getPointerEventButton(pickedInfo) === 0 && (sceneObject.allowPicking ?? false)) {
                const pickInfo = getCzmPickedInfoFromPickedInfo(pickedInfo)
                sceneObject.pickedEvent.emit(new SceneObjectPickedInfo(sceneObject, pickInfo));
            }
        }));

        {
            this.d(track([czmWater, 'allowPicking'], [sceneObject, 'allowPicking']));
            this.d(track([czmWater, 'ground'], [sceneObject, 'fillGround']));
            this.d(track([czmGeoPolyline, 'allowPicking'], [sceneObject, 'allowPicking']));
            this.d(track([czmGeoPolyline, 'color'], [sceneObject, 'strokeColor']));
            this.d(track([czmGeoPolyline, 'width'], [sceneObject, 'strokeWidth']));
            this.d(track([czmGeoPolyline, 'ground'], [sceneObject, 'strokeGround']));
        }
        {
            const event = this.dv(createNextAnimateFrameEvent(
                sceneObject.waterColorChanged,
                sceneObject.waterImageChanged,
                sceneObject.frequencyChanged,
                sceneObject.waveVelocityChanged,
                sceneObject.amplitudeChanged,
                sceneObject.specularIntensityChanged,
                sceneObject.waterTypeChanged,
                sceneObject.flowDirectionChanged,
                sceneObject.flowSpeedChanged,
            ))
            const update = () => {
                if (sceneObject.waterType === 'custom') {
                    this.updateWater({
                        waterColor: sceneObject.waterColor ?? ESDynamicWater.defaults.waterColor,
                        frequency: (sceneObject.frequency ?? ESDynamicWater.defaults.frequency) / 10,
                        waveVelocity: (sceneObject.waveVelocity ?? ESDynamicWater.defaults.waveVelocity) / 100,
                        amplitude: (sceneObject.amplitude ?? ESDynamicWater.defaults.amplitude) * 100,
                        specularIntensity: sceneObject.specularIntensity ?? ESDynamicWater.defaults.specularIntensity,
                        flowDirection: sceneObject.flowDirection ?? ESDynamicWater.defaults.flowDirection,
                        flowSpeed: sceneObject.flowSpeed ?? ESDynamicWater.defaults.flowSpeed,
                        waterImage: sceneObject.waterImage ?? ESDynamicWater.defaults.waterImage,
                    });
                } else {
                    const waterAttribute = Object.assign({}, waterType[sceneObject.waterType]);
                    waterAttribute.frequency && (waterAttribute.frequency /= 10);
                    waterAttribute.waveVelocity && (waterAttribute.waveVelocity /= 100);
                    waterAttribute.amplitude && (waterAttribute.amplitude *= 100);
                    this.updateWater(waterAttribute)
                }
            }
            update();
            this.d(event.don(update));
        }
        {//line show
            const update = () => {
                czmGeoPolyline.show = sceneObject.show && sceneObject.stroked;
            };
            const updateEvent = this.disposeVar(createNextAnimateFrameEvent(
                sceneObject.showChanged,
                sceneObject.strokedChanged,
            ));
            this.d(updateEvent.don(update));
            update();
        }
        {//Water show
            const update = () => {
                czmWater.show = (sceneObject.show && sceneObject.filled) ? true : false;
            };
            const updateEvent = this.disposeVar(createNextAnimateFrameEvent(
                sceneObject.showChanged,
                sceneObject.filledChanged,
            ));
            this.d(updateEvent.don(update));
            update();
        }

        {
            // update positions
            const update = () => {
                if (!sceneObject.points || (sceneObject.points && sceneObject.points.length <= 2)) {
                    czmGeoPolyline.positions = [];
                    czmWater.positions = [];
                    return
                };
                if (sceneObject.scale && sceneObject.scale.some(e => e === 0)) {
                    console.warn(`缩放属性(scale)不能设置值为0！`);
                    return;
                }
                const points = sceneObject.points.map(e => [e[0], e[1], 0] as [number, number, number])
                const [positions] = localPositionsToPositions({
                    originPosition: sceneObject.position,
                    originRotation: sceneObject.rotation,
                    originScale: sceneObject.scale,
                    // @ts-ignore
                    initialRotationMode: 'XForwardZUp',
                }, points);
                czmGeoPolyline.positions = [...positions, positions[0]];
                czmWater.positions = [...positions];
            };
            update();
            const updateEvent = this.disposeVar(createNextAnimateFrameEvent(
                sceneObject.pointsChanged,
                sceneObject.positionChanged,
                sceneObject.rotationChanged,
                sceneObject.scaleChanged,
            ));
            this.d(updateEvent.don(update));
        }


    }
    override visibleDistance(sceneObject: ESDynamicWater, czmViewer: CzmViewer): void {
        if (czmViewer.viewer?.camera && sceneObject.show) {
            const dis = getDistancesFromPositions([sceneObject.position, getCameraPosition(czmViewer.viewer.camera)], 'NONE')[0];
            let show = false;
            if (sceneObject.minVisibleDistance < sceneObject.maxVisibleDistance) {
                show = sceneObject.minVisibleDistance < dis && dis < sceneObject.maxVisibleDistance;
            } else if (sceneObject.maxVisibleDistance == 0) {
                show = dis > sceneObject.minVisibleDistance;
            }
            this._czmGeoPolyline.show = sceneObject.show && sceneObject.stroked && show;
            this._czmWater.show = sceneObject.show && sceneObject.filled && show;
        }
    }
    override flyTo(duration: number | undefined, id: number): boolean {
        const { sceneObject, czmViewer, czmWater } = this;
        if (!czmViewer.actived) return false;
        if (sceneObject.flyToParam || sceneObject.flyInParam) {
            return super.flyTo(duration, id);
        } else {
            if (czmWater.positions) {
                flyWithPositions(czmViewer, sceneObject, id, czmWater.positions, duration, true);
                return true;
            }
            return false;
        }
    }
    private updateWater(updateAttribute: WaterAttribute) {
        const { czmWater } = this;
        if (updateAttribute.waterColor && czmWater.baseWaterColor != updateAttribute.waterColor) {
            czmWater.baseWaterColor = updateAttribute.waterColor
        }
        if (updateAttribute.specularIntensity && czmWater.specularIntensity != updateAttribute.specularIntensity) {
            czmWater.specularIntensity = updateAttribute.specularIntensity
        }
        if (updateAttribute.frequency && czmWater.frequency != updateAttribute.frequency) {
            czmWater.frequency = updateAttribute.frequency
        }
        if (updateAttribute.waveVelocity && czmWater.animationSpeed != updateAttribute.waveVelocity) {
            czmWater.animationSpeed = updateAttribute.waveVelocity
        }
        if (updateAttribute.amplitude && czmWater.amplitude != updateAttribute.amplitude) {
            czmWater.amplitude = updateAttribute.amplitude
        }
        if (updateAttribute.flowSpeed != undefined && (czmWater.flowSpeed != updateAttribute.flowSpeed)) {
            czmWater.flowSpeed = updateAttribute.flowSpeed
        }
        if (updateAttribute.waterImage && (czmWater.baseWaterImage != updateAttribute.waterImage)) {
            czmWater.baseWaterImage = updateAttribute.waterImage
        }
        if (updateAttribute.flowDirection) {
            czmWater.stRotation = updateAttribute.flowDirection;
        }
    }
}
