import { CzmESLocalVector } from '@/esobjs-xe2-plugin-main/esobjs/base';
import { GeoCoplanarPolygon, SceneObjectPickedInfo, localPositionsToPositions } from 'xbsj-xe2/dist-node/xe2-base-objects';
import { bind, createNextAnimateFrameEvent, track } from 'xbsj-xe2/dist-node/xe2-base-utils';
import { CzmViewer, getViewerExtensions } from 'xbsj-xe2/dist-node/xe2-cesium-objects';
import { ESLocalPolygonZ } from '../../objs';
import { getCzmPickedInfoFromPickedInfo } from '../base/utils';
import { flyWithPositions, getPointerEventButton } from '../base';

export class CzmESLocalPolygonZ extends CzmESLocalVector<ESLocalPolygonZ> {
    static readonly type = this.register(ESLocalPolygonZ.type, this);
    private _czmGeoPolygon = this.disposeVar(new GeoCoplanarPolygon());
    get czmGeoPolygon() { return this._czmGeoPolygon; }

    constructor(sceneObject: ESLocalPolygonZ, czmViewer: CzmViewer) {
        super(sceneObject, czmViewer);
        const viewer = czmViewer.viewer;
        if (!viewer) {
            console.warn(`viewer is undefined!`);
            return;
        }

        const extensions = getViewerExtensions(czmViewer.viewer);
        if (!extensions) {
            return;
        }

        const czmGeoPolygon = this._czmGeoPolygon;

        czmViewer.add(czmGeoPolygon);
        this.dispose(() => czmViewer.delete(czmGeoPolygon));

        {
            this.dispose(track([czmGeoPolygon, 'show'], [sceneObject, 'show']));
            this.dispose(track([czmGeoPolygon, 'allowPicking'], [sceneObject, 'allowPicking']));
            this.dispose(track([czmGeoPolygon, 'outline'], [sceneObject, 'stroked']));
            this.dispose(track([czmGeoPolygon, 'outlineColor'], [sceneObject, 'strokeColor']));
            this.dispose(track([czmGeoPolygon, 'outlineWidth'], [sceneObject, 'strokeWidth']));
            this.dispose(track([czmGeoPolygon, 'color'], [sceneObject, 'fillColor']));
            this.dispose(track([czmGeoPolygon, 'fill'], [sceneObject, 'filled']));
            this.dispose(track([czmGeoPolygon, 'ground'], [sceneObject, 'fillGround']));
            this.dispose(track([czmGeoPolygon, 'strokeGround'], [sceneObject, 'strokeGround']));
        }

        this.dispose(czmGeoPolygon.pickedEvent.disposableOn((pickedInfo) => {
            if (getPointerEventButton(pickedInfo) === 0 && (sceneObject.allowPicking ?? false)) {
                const pickInfo = getCzmPickedInfoFromPickedInfo(pickedInfo)
                sceneObject.pickedEvent.emit(new SceneObjectPickedInfo(sceneObject, pickInfo));
            }
        }));

        {
            // update positions
            const update = () => {
                if (!sceneObject.points) {
                    return;
                }

                if (sceneObject.scale && sceneObject.scale.some(e => e === 0)) {
                    console.warn(`缩放属性(scale)不能设置值为0！`);
                    return;
                }

                const [positions] = localPositionsToPositions({
                    originPosition: sceneObject.position,
                    originRotation: sceneObject.rotation,
                    originScale: sceneObject.scale,
                    // @ts-ignore
                    initialRotationMode: 'XForwardZUp',
                }, sceneObject.points);

                czmGeoPolygon.positions = positions;
            };
            update();
            const updateEvent = this.disposeVar(createNextAnimateFrameEvent(
                sceneObject.pointsChanged,
                sceneObject.positionChanged,
                sceneObject.rotationChanged,
                sceneObject.scaleChanged,
            ));
            this.dispose(updateEvent.disposableOn(update));
        }
    }
    override flyTo(duration: number | undefined, id: number): boolean {
        const { sceneObject, czmViewer, czmGeoPolygon } = this;
        if (!czmViewer.actived) return false;
        if (sceneObject.flyToParam || sceneObject.flyInParam) {
            return super.flyTo(duration, id);
        } else {
            if (czmGeoPolygon.positions) {
                flyWithPositions(czmViewer, sceneObject, id, czmGeoPolygon.positions, duration);
                return true;
            }
            return false;
        }
    }
}
