import { ESSceneObject, SceneObjectPickedInfo } from 'xbsj-xe2/dist-node/xe2-base-objects';
import { CzmPolygonClipping, CzmViewer } from 'xbsj-xe2/dist-node/xe2-cesium-objects';
import { ES3DTileset, ESExcavate } from '../../objs';
import { Destroyable, bind, track } from "xbsj-xe2/dist-node/xe2-base-utils";
import { ObjResettingWithEvent, SceneObject, SceneObjectWithId, createNextAnimateFrameEvent } from "xbsj-xe2/dist-node/xe2-utils";
import { getCzmPickedInfoFromPickedInfo } from "../base/utils";
import { flyWithPositions, getPointerEventButton } from "../base";
import { CzmESGeoPolygon } from "../CzmESGeoPolygon";

class TilesIdResetting extends Destroyable {
    constructor(private _czmESExcavate: CzmESExcavate, private _eS3DTileset: ES3DTileset) {
        super();
        // 清除找不到的ES对象
        for (let i = 0; i < this._eS3DTileset.excavateId.length; i++) {
            const element = this._eS3DTileset.excavateId[i];
            if (!SceneObject.getSceneObjById(element))
                this._eS3DTileset.excavateId.splice(i, 1)
        }
        this._eS3DTileset.excavateId = [
            ...this._eS3DTileset.excavateId,
            this._czmESExcavate.czmPolygonClipping.id
        ]
        this.dispose(() => {
            this._eS3DTileset.excavateId = [
                ...this._eS3DTileset.excavateId.filter(id => id !== this._czmESExcavate.czmPolygonClipping.id
                )
            ]
        })

    }
}

class CzmViewerResetting extends Destroyable {
    constructor(private _czmESExcavate: CzmESExcavate) {
        super();
        if (!this._czmESExcavate.viewer) return;
        // 清除找不到的ES对象
        for (let i = 0; i < this._czmESExcavate.viewer.sceneGlobeClippingPolygonsId.length; i++) {
            const element = this._czmESExcavate.viewer.sceneGlobeClippingPolygonsId[i];
            if (!SceneObject.getSceneObjById(element))
                this._czmESExcavate.viewer.sceneGlobeClippingPolygonsId.splice(i, 1)
        }
        this._czmESExcavate.viewer.sceneGlobeClippingPolygonsId = [
            ...this._czmESExcavate.viewer.sceneGlobeClippingPolygonsId,
            this._czmESExcavate.czmPolygonClipping.id
        ]
        this.dispose(() => {
            if (!this._czmESExcavate.viewer) return;
            this._czmESExcavate.viewer.sceneGlobeClippingPolygonsId = [
                ...this._czmESExcavate.viewer.sceneGlobeClippingPolygonsId.filter(id => id !== this._czmESExcavate.czmPolygonClipping.id
                )
            ]
        })
    }
}

export class CzmESExcavate extends CzmESGeoPolygon<ESExcavate> {
    static override readonly type = this.register(ESExcavate.type, this);

    private _czmPolygonClipping = this.disposeVar(ESSceneObject.createFromClass(CzmPolygonClipping));
    get czmPolygonClipping() { return this._czmPolygonClipping; }

    private _viewer: CzmViewer | undefined = undefined
    get viewer() { return this._viewer }
    set viewer(value: CzmViewer | undefined) { this._viewer = value }

    private _tilesSceneObjectWithId = this.disposeVar(new SceneObjectWithId());
    get tilesSceneObjectWithId() { return this._tilesSceneObjectWithId; }
    private _tilesSceneObjectWithIdInit = this.dispose(track([this._tilesSceneObjectWithId, 'id'], [this.sceneObject, 'targetID']));

    private _event = this.disposeVar(createNextAnimateFrameEvent(this.tilesSceneObjectWithId.sceneObjectChanged, this.sceneObject.showChanged))
    get event() { return this._event; }

    constructor(sceneObject: ESExcavate, czmViewer: CzmViewer) {
        super(sceneObject, czmViewer);

        this.viewer = czmViewer
        const viewer = czmViewer.viewer;

        if (!viewer) {
            console.warn(`viewer is undefined!`);
            return;
        }

        const czmPolygonClipping = this._czmPolygonClipping;
        czmViewer.add(czmPolygonClipping);
        this.dispose(() => czmViewer.delete(czmPolygonClipping))

        {
            this.disposeVar(new ObjResettingWithEvent(this.event, () => {
                const { sceneObject, id } = this.tilesSceneObjectWithId;
                if (!this.sceneObject.show) return undefined;
                if (id === "") {
                    return new CzmViewerResetting(this);
                } else {
                    if (!sceneObject) return undefined;
                    if (!(sceneObject instanceof ES3DTileset)) return undefined;
                    return new TilesIdResetting(this, sceneObject as ES3DTileset);
                }
            }));
        }
        czmPolygonClipping.showHelper = false;
        this.dispose(bind([czmPolygonClipping, 'positions'], [sceneObject, 'points']));
        this.dispose(track([czmPolygonClipping, 'allowPicking'], [sceneObject, 'allowPicking']));
        {
            const update = () => {
                czmPolygonClipping.reverse = !(sceneObject.mode === "in");
            }
            update()
            this.dispose(sceneObject.modeChanged.disposableOn(update))
        }
        this.dispose(czmPolygonClipping.pickedEvent.disposableOn(pickedInfo => {
            if (getPointerEventButton(pickedInfo) === 0 && (sceneObject.allowPicking ?? false)) {
                const pickInfo = getCzmPickedInfoFromPickedInfo(pickedInfo)
                sceneObject.pickedEvent.emit(new SceneObjectPickedInfo(sceneObject, pickInfo));
            }
        }));
    }
    override flyTo(duration: number | undefined, id: number): boolean {
        const { sceneObject, czmViewer, czmPolygonClipping } = this;
        if (!czmViewer.actived) return false;
        if (sceneObject.flyToParam || sceneObject.flyInParam) {
            return super.flyTo(duration, id);
        } else {
            if (czmPolygonClipping.positions) {
                flyWithPositions(czmViewer, sceneObject, id, czmPolygonClipping.positions, duration);
                return true;
            }
            return false;
        }
    }

}
