import { CzmESObjectWithLocation } from '@/esobjs-xe2-plugin-main/esobjs/base';
import { flyTo } from 'xbsj-xe2/dist-node/xe2-base-cesium';
import { SceneObjectPickedInfo } from "xbsj-xe2/dist-node/xe2-base-objects";
import { bind, track } from "xbsj-xe2/dist-node/xe2-base-utils";
import { CzmCustomPrimitive, CzmViewer } from 'xbsj-xe2/dist-node/xe2-cesium-objects';
import { ESSection } from '../../objs';
import { CzmESEditing } from '@/esobjs-xe2-plugin-main/esobjs/utils';
import { getCzmPickedInfoFromPickedInfo } from '../base/utils';
import { defaultFlyToRotation, getPointerEventButton } from '../base';

export class CzmESSection extends CzmESObjectWithLocation<ESSection> {
    static readonly type = this.register(ESSection.type, this);
    private _czmCustom = this.disposeVar(new CzmCustomPrimitive());
    get czmCustom() { return this._czmCustom; }

    constructor(sceneObject: ESSection, czmViewer: CzmViewer) {
        super(sceneObject, czmViewer);
        const viewer = czmViewer.viewer;
        if (!viewer) {
            console.warn(`viewer is undefined!`);
            return;
        }
        const czmCustom = this._czmCustom;
        czmViewer.add(czmCustom);
        this.dispose(() => czmViewer.delete(czmCustom))

        this.dispose(track([czmCustom, 'allowPicking'], [sceneObject, 'allowPicking']));
        this.dispose(track([czmCustom, 'show'], [sceneObject, 'show']));
        this.dispose(track([czmCustom, 'pixelSize'], [sceneObject, 'pixelSize']));
        this.dispose(bind([czmCustom, 'position'], [sceneObject, 'position']));
        this.dispose(bind([czmCustom, 'rotation'], [sceneObject, 'rotation']));
        this.dispose(bind([czmCustom, 'localPosition'], [sceneObject, 'localPosition']));

        this.dispose(czmCustom.pickedEvent.disposableOn(pickedInfo => {
            if (getPointerEventButton(pickedInfo) === 0 && (sceneObject.allowPicking ?? false)) {
                const pickInfo = getCzmPickedInfoFromPickedInfo(pickedInfo)
                sceneObject.pickedEvent.emit(new SceneObjectPickedInfo(sceneObject, pickInfo));
            }
        }));

    }
    override flyTo(duration: number | undefined, id: number): boolean {
        const { sceneObject, czmViewer } = this;
        if (!czmViewer.actived) return false;
        if (sceneObject.flyToParam || sceneObject.flyInParam) {
            return super.flyTo(duration, id);
        } else {
            if (czmViewer.viewer) {
                flyTo(czmViewer.viewer.scene.camera, sceneObject.position, 5, defaultFlyToRotation, duration && duration * 1000);
                sceneObject.flyOverEvent.emit(id, 'over', czmViewer);
                return true;
            }
            return false;
        }
    }
}
