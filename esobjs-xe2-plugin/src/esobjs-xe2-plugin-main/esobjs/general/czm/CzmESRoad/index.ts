import { bind, createNextAnimateFrameEvent, track } from "xbsj-xe2/dist-node/xe2-base-utils";
import { CzmESVisualObject } from '@/esobjs-xe2-plugin-main/esobjs/base';
import { CzmRoad, CzmViewer } from 'xbsj-xe2/dist-node/xe2-cesium-objects';
import { ESRoad } from '../../objs';
import { flyWithPositions } from '../base';

export class CzmESRoad extends CzmESVisualObject<ESRoad> {
    static readonly type = this.register(ESRoad.type, this);
    private _czmESRoad = this.disposeVar(new CzmRoad());
    get czmESRoad() { return this._czmESRoad; }

    constructor(sceneObject: ESRoad, czmViewer: CzmViewer) {
        super(sceneObject, czmViewer);
        const viewer = czmViewer.viewer;
        if (!viewer) {
            console.warn(`viewer is undefined!`);
            return;
        }

        const czmESRoad = this._czmESRoad;
        czmViewer.add(czmESRoad);
        this.dispose(() => czmViewer.delete(czmESRoad))

        // this.dispose(track([czmESRoad, 'show'], [sceneObject, 'stroked']));
        // this.dispose(track([czmESRoad, 'show'], [sceneObject, 'show']));
        {
            const update = () => {
                czmESRoad.show = sceneObject.show && sceneObject.stroked;
            }
            update();
            const event = this.dv(createNextAnimateFrameEvent(sceneObject.showChanged, sceneObject.strokedChanged));
            this.dispose(event.don(update));
        }
        this.dispose(bind([czmESRoad, 'positions'], [sceneObject, 'points']));
        this.dispose(track([czmESRoad, 'width'], [sceneObject, 'width']));
        this.dispose(track([czmESRoad, 'arcType'], [sceneObject, 'arcType']));
        this.dispose(track([czmESRoad, 'imageUrl'], [sceneObject, 'imageUrl']));
        this.dispose(track([czmESRoad, 'repeat'], [sceneObject, 'repeat']));
        this.dispose(track([czmESRoad, 'editing'], [sceneObject, 'editing']));
        this.dispose(track([czmESRoad, 'allowPicking'], [sceneObject, 'allowPicking']));
    }
    override flyTo(duration: number | undefined, id: number): boolean {
        const { sceneObject, czmViewer, czmESRoad } = this;
        if (!czmViewer.actived) return false;
        if (sceneObject.flyToParam || sceneObject.flyInParam) {
            return super.flyTo(duration, id);
        } else {
            if (czmESRoad.positions) {
                flyWithPositions(czmViewer, sceneObject, id, czmESRoad.positions, duration);
                return true;
            }
            return false;
        }
    }
    override flyIn(duration: number | undefined, id: number): boolean {
        const { sceneObject, czmViewer, czmESRoad } = this;
        if (!czmViewer.actived) return false;
        if (sceneObject.flyInParam) {
            return super.flyIn(duration, id);
        } else {
            if (czmESRoad.positions) {
                flyWithPositions(czmViewer, sceneObject, id, czmESRoad.positions, duration);
                return true;
            }
            return false;
        }
    }
}
