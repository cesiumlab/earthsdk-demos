import { CzmESGeoVector } from "@/esobjs-xe2-plugin-main/esobjs/base";
import { bind, track } from "xbsj-xe2/dist-node/xe2-base-utils";
import { CzmViewer } from 'xbsj-xe2/dist-node/xe2-cesium-objects';
import { ESVolumeMeasurement } from '../../objs';
import { GeoVolumeMeasurement, SceneObjectPickedInfo } from "xbsj-xe2/dist-node/xe2-base-objects";
import { getCzmPickedInfoFromPickedInfo } from "../base/utils";
import { flyWithPositions, getPointerEventButton } from "../base";

export class CzmESVolumeMeasurement<T extends ESVolumeMeasurement = ESVolumeMeasurement> extends CzmESGeoVector<T> {
    static readonly type = this.register<ESVolumeMeasurement>(ESVolumeMeasurement.type, this);

    private _geoVolumeMeasurement = this.disposeVar(new GeoVolumeMeasurement());
    get geoVolumeMeasurement() { return this._geoVolumeMeasurement; }

    constructor(sceneObject: T, czmViewer: CzmViewer) {
        super(sceneObject, czmViewer);

        const viewer = czmViewer.viewer;
        if (!viewer) {
            console.warn(`viewer is undefined!`);
            return;
        }

        const geoVolumeMeasurement = this._geoVolumeMeasurement;
        czmViewer.add(geoVolumeMeasurement);
        this.dispose(() => czmViewer.delete(geoVolumeMeasurement))

        this.dispose(track([geoVolumeMeasurement, 'show'], [sceneObject, 'show']));
        this.dispose(bind([geoVolumeMeasurement, 'positions'], [sceneObject, 'points']));
        this.dispose(track([geoVolumeMeasurement, 'allowPicking'], [sceneObject, 'allowPicking']));
        this.dispose(bind([geoVolumeMeasurement, 'editing'], [sceneObject, 'editing']));

        this.dispose(bind([geoVolumeMeasurement, 'planeHeight'], [sceneObject, 'planeHeight']));
        this.dispose(track([geoVolumeMeasurement, 'gridWidth'], [sceneObject, 'gridWidth']));
        this.dispose(track([sceneObject, 'cutVolume'], [geoVolumeMeasurement, 'cutVolume']));
        this.dispose(track([sceneObject, 'fillVolume'], [geoVolumeMeasurement, 'fillVolume']));
        this.dispose(track([sceneObject, 'cutAndFillVolume'], [geoVolumeMeasurement, 'cutAndFillVolume']));
        this.dispose(bind([sceneObject, 'progress'], [geoVolumeMeasurement, 'progress']));
        this.dispose(track([geoVolumeMeasurement, 'depthTest'], [sceneObject, 'depthTest']));

        this.d(track([geoVolumeMeasurement, 'outline'], [sceneObject, 'stroked']));
        this.d(track([geoVolumeMeasurement, 'outlineWidth'], [sceneObject, 'strokeWidth']));
        this.d(track([geoVolumeMeasurement, 'outlineColor'], [sceneObject, 'strokeColor']));
        this.d(track([geoVolumeMeasurement, 'filled'], [sceneObject, 'filled']));
        this.d(track([geoVolumeMeasurement, 'fillColor'], [sceneObject, 'fillColor']));
        this.d(track([geoVolumeMeasurement, 'fillGround'], [sceneObject, 'fillGround']));
        this.d(track([geoVolumeMeasurement, 'strokeGround'], [sceneObject, 'strokeGround']));
        // {
        //     const updateProp = () => {
        //         const stroked = sceneObject.stroked
        //         if (!stroked) {
        //             geoVolumeMeasurement.outlineWidth = 0;
        //             return
        //         } else {
        //             geoVolumeMeasurement.outlineWidth = sceneObject.strokeWidth;
        //         }

        //         geoVolumeMeasurement.outlineWidth = sceneObject.strokeWidth;
        //         geoVolumeMeasurement.outlineColor = sceneObject.strokeColor;
        //     }
        //     updateProp();
        //     const updateEvent = this.disposeVar(createNextAnimateFrameEvent(
        //         sceneObject.strokeStyleChanged,
        //         sceneObject.strokedChanged,
        //     ));
        //     this.dispose(updateEvent.disposableOn(updateProp));
        // }
        this.dispose(sceneObject.startEvent.disposableOn(() => {
            geoVolumeMeasurement.enableEmit()
        }))

        this.dispose(sceneObject.clearEvent.disposableOn(() => {
            geoVolumeMeasurement.clearEmit()
        }))
        this.dispose(geoVolumeMeasurement.pickedEvent.disposableOn(pickedInfo => {
            if (getPointerEventButton(pickedInfo) === 0 && (sceneObject.allowPicking ?? false)) {
                const pickInfo = getCzmPickedInfoFromPickedInfo(pickedInfo)
                sceneObject.pickedEvent.emit(new SceneObjectPickedInfo(sceneObject, pickInfo));
            }
        }));
    }

    override flyTo(duration: number | undefined, id: number): boolean {
        const { sceneObject, czmViewer, geoVolumeMeasurement } = this;
        if (!czmViewer.actived) return false;
        if (sceneObject.flyToParam || sceneObject.flyInParam) {
            return super.flyTo(duration, id);
        } else {
            if (geoVolumeMeasurement.positions) {
                flyWithPositions(czmViewer, sceneObject, id, geoVolumeMeasurement.positions, duration);
                return true;
            }
            return false;
        }
    }
}
