import * as Cesium from 'cesium';

export function createShader(alpha: number, alphaBlend: boolean) {
    // const mix = viewModel.alphaBlendMode === "透明相乘";
    const mix = alphaBlend;

    let shader;
    if (alpha >= 1) {
      if (!mix) {
        shader = `void fragmentMain(FragmentInput fsInput, inout czm_modelMaterial material)
          {
              material.diffuse = fsInput.metadata.color.rgb;
              material.alpha = 1.0;
          }`;
      }
      else {
        shader = `void fragmentMain(FragmentInput fsInput, inout czm_modelMaterial material)
          {
              material.diffuse = fsInput.metadata.color.rgb;
              material.alpha = fsInput.metadata.color.a;
          }`;
      }
    }
    else {
      if (!mix) {
        shader = `void fragmentMain(FragmentInput fsInput, inout czm_modelMaterial material)
          {
              material.diffuse = fsInput.metadata.color.rgb;
              material.alpha = float(${alpha});
          }`;
      }
      else {
        shader = `void fragmentMain(FragmentInput fsInput, inout czm_modelMaterial material)
          {
              material.diffuse = fsInput.metadata.color.rgb;
              material.alpha = fsInput.metadata.color.a * float(${alpha});
          }`;
      }
    }

    const customShaderColor = new Cesium.CustomShader({
      fragmentShaderText: shader,
    });

    return customShaderColor;
  }