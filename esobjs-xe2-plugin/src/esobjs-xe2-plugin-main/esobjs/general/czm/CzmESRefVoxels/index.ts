import { CzmESObjectWithLocation } from '@/esobjs-xe2-plugin-main/esobjs/base';
import * as Cesium from 'cesium';
import { Destroyable, react } from "xbsj-xe2/dist-node/xe2-base-utils";
import { CzmViewer } from 'xbsj-xe2/dist-node/xe2-cesium-objects';
import { ObjResettingWithEvent, createNextAnimateFrameEvent, createProcessingFromAsyncFunc } from "xbsj-xe2/dist-node/xe2-utils";
import { ESRefVoxels } from '../../objs';
import { RefSingleTileVoxelProvider } from './RefSingleTileVoxelProvider';
import { createShader } from './createShader';

class CzmRefVoxels extends Destroyable {
    constructor(sceneObject: ESRefVoxels, czmESRefVoxels: CzmESRefVoxels, values: [number, number, number, number][]) {
        super();

        // if (!sceneObject.position) {
        //     throw new Error(`!sceneObject.position`);
        // }

        const position = sceneObject.position ?? ESRefVoxels.defaults.position;
        if (!position) {
            throw new Error(`!position`);
        }

        const [l, b, h] = position;
        const [dx, dy, dz] = sceneObject.dimensions ?? ESRefVoxels.defaults.dimensions;
        const resolution = sceneObject.resolution ?? ESRefVoxels.defaults.resolution;

        // const position = Cesium.Cartesian3.fromDegrees(...sceneObject.position);

        // const heading = Cesium.Math.toRadians(0);
        // const pitch = 0;
        // const roll = 0;
        // const hpr = new Cesium.HeadingPitchRoll(heading, pitch, roll);
        // const orientation = Cesium.Transforms.headingPitchRollQuaternion(
        //     position,
        //     hpr
        // );

        function toColor(value: number, color: Cesium.Color) {
            const ColorConfig = sceneObject.colorLegend ?? ESRefVoxels.defaults.colorLegend;
            if (!ColorConfig) {
                throw new Error(`!ColorConfig`);
            }

            for (let i = 0; i < ColorConfig.length; i++) {
                if (value >= ColorConfig[i].v) {

                    color.red = ColorConfig[i].c[0] / 255;
                    color.green = ColorConfig[i].c[1] / 255;
                    color.blue = ColorConfig[i].c[2] / 255;
                    color.alpha = ColorConfig[i].c[3] / 255;
                    return;
                }
            }

            color.red = 1;
            color.green = 1;
            color.blue = 1;
            color.alpha = 0.0;

            return;
        }

        const center = { x: l, y: b, z: h };
        //构造体渲染的数据构造
        const provider = new RefSingleTileVoxelProvider(
            {
                center: center,  //中心点
                x: { dimension: dx, resolution }, // 经线方向上的个数，以及分辨率
                y: { dimension: dy, resolution }, // 纬线方向上的个数，以及分辨率
                z: { dimension: dz, resolution },  // 高度方向上的个数，以及分辨率
                values: values, // 经纬度数据
                toColor,
            }
        );

        const { czmViewer } = czmESRefVoxels;
        if (!czmViewer) throw new Error(`!czmViewer`);
        const { viewer } = czmViewer;
        if (!viewer) throw new Error('!viewer');

        const d = ESRefVoxels.defaults;
        const voxelPrimitive = viewer.scene.primitives.add(
            new Cesium.VoxelPrimitive({
                // @ts-ignore
                provider: provider,
                customShader: createShader(sceneObject.alpha ?? d.alpha, sceneObject.alphaBlend ?? d.alphaBlend),
                modelMatrix: provider.caculModelMatrix(1),
            })
        );

        this.dispose(() => viewer.scene.primitives.remove(voxelPrimitive));

        {
            const update = () => {
                voxelPrimitive.customShader = createShader(sceneObject.alpha ?? d.alpha, sceneObject.alphaBlend ?? d.alphaBlend)
            };
            update();
            const event = this.disposeVar(createNextAnimateFrameEvent(sceneObject.alphaChanged, sceneObject.alphaBlendChanged));
            this.dispose(event.disposableOn(update));
        }

        {
            const update = () => {
                voxelPrimitive.minClippingBounds.x = sceneObject.clipMinX ?? ESRefVoxels.defaults.clipMinX;
                voxelPrimitive.minClippingBounds.y = sceneObject.clipMinY ?? ESRefVoxels.defaults.clipMinY;
                voxelPrimitive.minClippingBounds.z = sceneObject.clipMinZ ?? ESRefVoxels.defaults.clipMinZ;

                voxelPrimitive.maxClippingBounds.x = sceneObject.clipMaxX ?? ESRefVoxels.defaults.clipMaxX;
                voxelPrimitive.maxClippingBounds.y = sceneObject.clipMaxY ?? ESRefVoxels.defaults.clipMaxY;
                voxelPrimitive.maxClippingBounds.z = sceneObject.clipMaxZ ?? ESRefVoxels.defaults.clipMaxZ;
            };
            update();
            const event = this.disposeVar(createNextAnimateFrameEvent(
                sceneObject.clipMinXChanged,
                sceneObject.clipMinYChanged,
                sceneObject.clipMinZChanged,
                sceneObject.clipMaxXChanged,
                sceneObject.clipMaxYChanged,
                sceneObject.clipMaxZChanged,
            ));
            this.dispose(event.disposableOn(update));
        }

        {
            const update = () => voxelPrimitive.nearestSample = sceneObject.nearestSample ?? ESRefVoxels.defaults.nearestSample;
            update();
            this.dispose(sceneObject.nearestSampleChanged.disposableOn(update));
        }
    }
}

function fetchData(url: string) {
    //先获取数据，获取之后，回调里进行创建数据
    const promise = Cesium.Resource.fetchText({ url });
    if (!promise) return undefined;
    return promise.then(text => {
        const datas = text.split('\r\n');
        console.log("text lines:", datas.length);
        const values = [] as [number, number, number, number][];

        let west = 180;
        let east = -180;
        let south = 90;
        let north = -90;
        let minH = 10000000;
        let maxH = 0;

        //解析所有数据，并且统计最大最小范围
        for (let i = 1; i < datas.length; i++) {
            const d2 = datas[i].split('	');
            if (d2.length < 4) {
                continue;
            }
            const d = [parseFloat(d2[0]), parseFloat(d2[1]), parseFloat(d2[2]) * 1000, parseFloat(d2[3])] as [number, number, number, number];
            if (d[3] < 0) {
                continue;
            }
            west = Math.min(west, d[0]);
            east = Math.max(east, d[0]);

            south = Math.min(south, d[1]);
            north = Math.max(north, d[1]);

            minH = Math.min(minH, d[2]);
            maxH = Math.max(maxH, d[2]);
            values.push(d);
        }

        console.log("valid count:", values.length);
        console.log("bouding ", west, east, south, north, minH, maxH);

        return values;
    });
}

export class CzmESRefVoxels extends CzmESObjectWithLocation<ESRefVoxels> {
    static readonly type = this.register(ESRefVoxels.type, this);

    private _values = this.disposeVar(react<[number, number, number, number][] | undefined>(undefined));
    get values() { return this._values.value; }
    set values(value: [number, number, number, number][] | undefined) { this._values.value = value; }
    get valuesChanged() { return this._values.changed; }

    private _recreateEvent = this.disposeVar(createNextAnimateFrameEvent(
        this.valuesChanged,
        this.sceneObject.positionChanged,
        this.sceneObject.dimensionsChanged,
        this.sceneObject.resolutionChanged,
        this.sceneObject.colorLegendChanged,
    ));
    get recreateEvent() { return this._recreateEvent; }

    private _resetting = this.disposeVar(new ObjResettingWithEvent(this._recreateEvent, () => {
        if (!this.values) return undefined;
        return new CzmRefVoxels(this.sceneObject, this, this.values);
    }));
    get resetting() { return this._resetting; }

    constructor(sceneObject: ESRefVoxels, czmViewer: CzmViewer) {
        super(sceneObject, czmViewer);
        const viewer = czmViewer.viewer;
        if (!viewer) {
            console.warn(`viewer is undefined!`);
            return;
        }

        {
            const fetchProcessing = this.disposeVar(createProcessingFromAsyncFunc(async cancelsManager => {
                this.values = undefined;
                const url = sceneObject.url ?? ESRefVoxels.defaults.url;
                if (!url) return;
                const p = fetchData(url);
                if (!p) return;
                const values = await cancelsManager.promise(p);
                this.values = values;
            }));

            const update = () => {
                const url = sceneObject.url ?? ESRefVoxels.defaults.url;
                if (!url) return;
                fetchProcessing.restart();
            }
            update();
            this.dispose(sceneObject.urlChanged.disposableOn(update));
        }
    }
    override flyTo(duration: number | undefined, id: number): boolean {
        const { sceneObject, czmViewer } = this;
        if (!czmViewer.actived) return false;
        if (sceneObject.flyToParam || sceneObject.flyInParam) {
            return super.flyTo(duration, id);
        } else {
            const d = ESRefVoxels.defaults;
            const { position = d.position, dimensions = d.dimensions, resolution = d.resolution } = sceneObject;
            if (!position) return false;
            const d2s = dimensions.map(e => e * resolution);
            const viewDistance = Math.max(...d2s);
            czmViewer.flyTo(position, viewDistance, undefined, duration && duration * 1000);
            sceneObject.flyOverEvent.emit(id, 'over', czmViewer);
            return true;
        }
    }
}
