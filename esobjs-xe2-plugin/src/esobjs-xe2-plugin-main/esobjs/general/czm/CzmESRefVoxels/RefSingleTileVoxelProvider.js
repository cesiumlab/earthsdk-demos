import * as Cesium from 'cesium';

function getMinBounds(shape) {
  if (shape === Cesium.VoxelShapeType.ELLIPSOID) {
      const minBounds = Cesium.Cartesian3.clone(
          Cesium.VoxelShapeType.getMinBounds(shape)
      );
      minBounds.z = 0.0;
      return minBounds;
  }

  return undefined;
}

function getMaxBounds(shape) {
  if (shape === Cesium.VoxelShapeType.ELLIPSOID) {
      const maxBounds = Cesium.Cartesian3.clone(
          Cesium.VoxelShapeType.getMaxBounds(shape)
      );
      maxBounds.z = 1000000.0;
      return maxBounds;
  }

  return undefined;
}

export function RefSingleTileVoxelProvider(options) {
    this.toColor = options.toColor;

    //创建一个enu 矩阵 
    this.center = options.center;
    this.enuMatrix = Cesium.Transforms.eastNorthUpToFixedFrame(Cesium.Cartesian3.fromDegrees(this.center.x, this.center.y,
      this.center.z));


    this.shape = Cesium.VoxelShapeType.BOX;
    //这个都是相对单位形状来说的，box只能是0-1范围，球是经纬度
    this.minBounds = getMinBounds(this.shape);
    this.maxBounds = getMaxBounds(this.shape);

    //计算x  这里单位是米
    this.xRes = options.x.resolution;
    this.yRes = options.y.resolution;
    this.zRes = options.z.resolution;
    //计算x的范围
    this.minX = - options.x.dimension * 0.5 * this.xRes;
    this.maxX = + options.x.dimension * 0.5 * this.xRes;
    //计算y的范围
    this.minY = - options.y.dimension * 0.5 * this.yRes;
    this.maxY = + options.y.dimension * 0.5 * this.yRes;
    //计算z的范围 注意z是从下底中心起算
    this.minZ = 0;
    this.maxZ = options.z.dimension * this.zRes;




    this.dimensions = new Cesium.Cartesian3(options.x.dimension, options.y.dimension, options.z.dimension);
    this.names = ["color"];
    this.types = [Cesium.MetadataType.VEC4];
    this.componentTypes = [Cesium.MetadataComponentType.FLOAT32];

    this._values = options.values;
  }
  RefSingleTileVoxelProvider.prototype.caculModelMatrix = function(scale) {
    //因为这个box的中心点在内部，所以对z值做一个偏移
    const globelMatrix = Cesium.Transforms.eastNorthUpToFixedFrame(
      Cesium.Cartesian3.fromDegrees(this.center.x, this.center.y,
        this.center.z + (this.maxZ + this.zRes) * scale)
    );
    //由于box的单位形状，这里必须对图元进行缩放 才能匹配到合适的地理大小

    const scaleMatrix = Cesium.Matrix4.fromScale(
      new Cesium.Cartesian3((this.maxX - this.minX) * scale,
        (this.maxY - this.minY) * scale,
        (this.maxZ - this.minZ) * scale));
    return Cesium.Matrix4.multiply(globelMatrix, scaleMatrix, new Cesium.Matrix4());
  }
  const scratchColor = new Cesium.Color();

  RefSingleTileVoxelProvider.prototype.requestData = function(
    options
  ) {
    const tileLevel = options.tileLevel;
    const tileX = options.tileX;
    const tileY = options.tileY;
    const tileZ = options.tileZ;

    if (tileLevel >= 1) {
      return undefined;
    }

    const dimensions = this.dimensions;
    const voxelCount = dimensions.x * dimensions.y * dimensions.z;
    const type = this.types[0];
    const channelCount = Cesium.MetadataType.getComponentCount(type);
    const dataColor = new Float32Array(voxelCount * channelCount);


    //先进行数据的填充
    const dataValues = new Float32Array(voxelCount);
    const dataCount = new Int32Array(voxelCount);

    const invEnu = Cesium.Matrix4.inverse(this.enuMatrix, new Cesium.Matrix4());
    const ccc = new Cesium.Cartesian3();
    const color = new Cesium.Color(1, 1, 1, 1);

    for (let i = 0; i < this._values.length; i++) {
      //把经纬度转
      const v = this._values[i];
      const ecef = Cesium.Cartesian3.fromDegrees(v[0], v[1], v[2]);
      const enu = Cesium.Matrix4.multiplyByPoint(invEnu, ecef, ccc);
      //计算得到了相对坐标，那么根据这个坐标来进行尺寸着色
      let x = Math.floor((enu.x - this.minX) / this.xRes);
      if (x < 0) { x = 0; } if (x >= dimensions.x - 1) { x = this.dimensions.x - 1; }

      let y = Math.floor((enu.y - this.minY) / this.yRes);
      if (y < 0) { y = 0; } if (y >= dimensions.y - 1) { y = this.dimensions.y - 1; }

      let z = Math.floor((enu.z - this.minZ) / this.zRes);
      if (z < 0) { z = 0; } if (z >= dimensions.z - 1) { z = this.dimensions.z - 1; }
      //console.log(x,y,z);
      const index =
        z * dimensions.y * dimensions.x + y * dimensions.x + x;

      // dataValues[index] += v[3];
      dataValues[index] = Math.max(dataValues[index], v[3]);
      dataCount[index]++;
    }
    //周边至少有两个有值
    function findNeiborValue(x, y, z) {
      let count = 0;
      let valueSum = 0;
      for (let i = -1; i < 2; i++) {
        //边界跳过
        if (i + x < 0 || i + x >= dimensions.x) {
          continue;
        }
        for (let j = -1; j < 2; j++) {
          //边界跳过
          if (j + y < 0 || j + y >= dimensions.y) {
            continue;
          }
          for (let k = -1; k < 2; k++) {
            //边界跳过
            if (k + z < 0 || k + z >= dimensions.z) {
              continue;
            }
            const index = (k + z) * dimensions.y * dimensions.x + (j + y) * dimensions.x + (i + x);
            if (dataCount[index] > 0) {
              count++;
              valueSum += dataValues[index];
            }
          }
        }
      }
      if (count <= 1) { return undefined; }

      return valueSum / count;

    }
    //填洞，寻找周边元素，如果周边元素 至少有一个有值，那么给这个地方填充平均值
    for (let x = 0; x < dimensions.x; x++) {
      for (let y = 0; y < dimensions.y; y++) {
        for (let z = 0; z < dimensions.z; z++) {
          const index = z * dimensions.y * dimensions.x + y * dimensions.x + x;
          //有数据的跳过
          if (dataCount[index] > 0) { continue; }
          //寻找周边值
          const v = findNeiborValue(x, y, z);
          if (v !== undefined) {

            dataValues[index] = v;
          }
        }
      }
    }
    
    //根据值着色
    for (let i = 0; i < voxelCount; i++) {

      /*
       let v= dataValues[i];
      const c = dataCount[i];
      //求个平均值
      if(c>0){
        v = v/c;
      }
       toColor(v, color);
      */
      this.toColor(dataValues[i], color);
      dataColor[i * channelCount + 0] = color.red;
      dataColor[i * channelCount + 1] = color.green;
      dataColor[i * channelCount + 2] = color.blue;
      dataColor[i * channelCount + 3] = color.alpha;
    }

    return Promise.resolve([dataColor]);
  };