import { CzmESObjectWithLocation } from '@/esobjs-xe2-plugin-main/esobjs/base';
import { GeoDivSwitchPoi } from 'xbsj-xe2/dist-node/xe2-base-objects';
import { bind, createNextAnimateFrameEvent, track } from 'xbsj-xe2/dist-node/xe2-base-utils';
import { CzmViewer } from 'xbsj-xe2/dist-node/xe2-cesium-objects';
import { ESUEWidget } from '../../objs';
import { defaultFlyToRotation } from '../base';

export class CzmESUEWidget extends CzmESObjectWithLocation<ESUEWidget> {
    static readonly type = this.register(ESUEWidget.type, this);
    private _czmGeoDivSwitchPoi = this.disposeVar(new GeoDivSwitchPoi());
    get czmGeoDivSwitchPoi() { return this._czmGeoDivSwitchPoi; }

    constructor(sceneObject: ESUEWidget, czmViewer: CzmViewer) {
        super(sceneObject, czmViewer);
        const viewer = czmViewer.viewer;
        if (!viewer) {
            console.warn(`viewer is undefined!`);
            return;
        }
        const czmGeoDivSwitchPoi = this._czmGeoDivSwitchPoi;
        czmGeoDivSwitchPoi.showIcon = false;
        czmViewer.add(czmGeoDivSwitchPoi);
        this.dispose(() => czmViewer.delete(czmGeoDivSwitchPoi))

        this.d(track([czmGeoDivSwitchPoi, 'zOrder'], [sceneObject, 'zOrder']));
        this.dispose(track([czmGeoDivSwitchPoi, 'show'], [sceneObject, 'show']));
        this.dispose(track([czmGeoDivSwitchPoi, 'text'], [sceneObject, 'info'], (val: Object | undefined) => Object.values(val ?? { title: '请输入内容' }).join()));
        this.dispose(bind([czmGeoDivSwitchPoi, 'position'], [sceneObject, 'position']));
        const updateAnchor = () => {
            const anchor = sceneObject.anchor;
            const offset = sceneObject.offset;
            if (anchor) {
                czmGeoDivSwitchPoi.originRatioAndOffset = [...anchor, -offset[0], -offset[1]];
            } else {
                czmGeoDivSwitchPoi.originRatioAndOffset = [0, 0, 0, 0];
            }
        }
        const event = this.ad(createNextAnimateFrameEvent(sceneObject.anchorChanged, sceneObject.offsetChanged));
        this.dispose(event.disposableOn(() => updateAnchor()));
        updateAnchor();
    }
    override flyTo(duration: number | undefined, id: number): boolean {
        const { sceneObject, czmViewer, czmGeoDivSwitchPoi } = this;
        if (!czmViewer.actived) return false;
        if (sceneObject.flyToParam || sceneObject.flyInParam) {
            return super.flyTo(duration, id);
        } else {
            if (czmGeoDivSwitchPoi.position) {
                this.czmViewer.flyTo(czmGeoDivSwitchPoi.position, 1000, defaultFlyToRotation, duration && duration * 1000);
                return true;
            }
            czmGeoDivSwitchPoi.flyTo(duration && duration * 1000);
            sceneObject.flyOverEvent.emit(id, 'over', czmViewer);
            return true;
        }
    }
}
