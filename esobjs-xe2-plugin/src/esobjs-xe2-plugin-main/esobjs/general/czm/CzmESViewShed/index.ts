import { CzmViewShed } from "xbsj-xe2/dist-node/xe2-cesium-objects";
import { CzmViewer } from 'xbsj-xe2/dist-node/xe2-cesium-objects';
import { ESViewShed } from '../../objs';
import { bind, createNextAnimateFrameEvent } from 'xbsj-xe2/dist-node/xe2-base-utils';
import { CzmESObjectWithLocation, bindNorthRotation } from "@/esobjs-xe2-plugin-main/esobjs/base";
import { Vector } from 'xbsj-xe2/dist-node/xe2-math';
import { flyWithPosition } from "../base";

export class CzmESViewShed extends CzmESObjectWithLocation<ESViewShed> {
    static readonly type = this.register(ESViewShed.type, this);

    private _czmViewShed = this.disposeVar(new CzmViewShed());
    get czmViewShed() { return this._czmViewShed; }

    constructor(sceneObject: ESViewShed, czmViewer: CzmViewer) {
        super(sceneObject, czmViewer);

        const viewer = czmViewer.viewer;
        if (!viewer) {
            console.warn(`viewer is undefined!`);
            return;
        }
        const czmViewShed = this._czmViewShed;
        czmViewer.add(czmViewShed);
        this.dispose(() => czmViewer.delete(czmViewShed));

        this.dispose(bindNorthRotation([czmViewShed, 'rotation'], [sceneObject, 'rotation']));
        this.dispose(bind([czmViewShed, 'show'], [sceneObject, 'show']));
        this.dispose(bind([czmViewShed, 'showHelper'], [sceneObject, 'showFrustum']));
        this.dispose(bind([czmViewShed, 'near'], [sceneObject, 'near']));
        this.dispose(bind([czmViewShed, 'far'], [sceneObject, 'far']));
        {
            this.sPrsEditing.enabled = false; // 禁用基类中的编辑
            this.d(bind([czmViewShed, 'editing'], [sceneObject, 'editing']));
        }
        const update = () => {
            czmViewShed.fovH = sceneObject.fov ?? ESViewShed.defaults.fov;
            if (sceneObject.fov != 0 && sceneObject.aspectRatio != 0) {
                czmViewShed.fovV = sceneObject.fov / sceneObject.aspectRatio;
            }
        }
        update();
        const updateEvent = this.disposeVar(createNextAnimateFrameEvent(
            sceneObject.fovChanged,
            sceneObject.aspectRatioChanged,
        ));
        this.dispose(updateEvent.disposableOn(update));
        // 为了使双点编辑生效，需要监听sceneObject和czmCustomPrimitive的position,
        // 如果是在[0,0,0]点的话，就把czm对象位置设置为undefined,就能双点编辑了
        {
            const updated = () => {
                if (Vector.equals(sceneObject.position, [0, 0, 0])) {
                    czmViewShed.position = undefined;
                } else {
                    czmViewShed.position = sceneObject.position;
                }
            }
            updated();
            this.dispose(this.sceneObject.positionChanged.disposableOn(updated));
        }
        {
            const updated = () => {
                if (czmViewShed.position == undefined) {
                    sceneObject.position = [0, 0, 0];
                }
                else {
                    sceneObject.position = czmViewShed.position;
                }
            }
            this.dispose(czmViewShed.positionChanged.disposableOn(updated));
        }
    }
    override flyTo(duration: number | undefined, id: number): boolean {
        const { sceneObject, czmViewer } = this;
        if (!czmViewer.actived) return false;
        if (sceneObject.flyToParam || sceneObject.flyInParam) {
            return super.flyTo(duration, id);
        } else {
            const viewDistance = (sceneObject.far ?? ESViewShed.defaults.far);
            if (sceneObject.position) {
                flyWithPosition(czmViewer, sceneObject, id, sceneObject.position, viewDistance, duration);
                return true;
            }
            return false;
        }
    }
}
