// import { ESObjectWithLocation } from '../../objs';
// import { CzmObject, CzmViewer, getViewerExtensions } from 'xbsj-xe2/dist-node/xe2-cesium-objects';

// export class CzmESObjectWithLocation<T extends ESObjectWithLocation> extends CzmObject<T> {
//     constructor(sceneObject: T, czmViewer: CzmViewer) {
//         super(sceneObject, czmViewer);
//         const viewer = czmViewer.viewer;
//         if (!viewer) {
//             console.warn(`viewer is undefined!`);
//             return;
//         }

//         const extensions = getViewerExtensions(czmViewer.viewer);
//         if (!extensions) {
//             return;
//         }

//         this.dispose(sceneObject.flyToEvent.disposableOn(duration => {
//             if (!czmViewer.actived) return;

//             if (!sceneObject.position) {
//                 console.warn(`flyInEvent error! !sceneObject.position`);
//                 return;
//             }

//             if (!sceneObject.flyToParam) {
//                 console.warn(`flyInEvent error! !sceneObject.flyToParam`);
//                 return;
//             }

//             const { heading, pitch, hDelta, pDelta, distance, flyDuration } = sceneObject.flyToParam;

//             czmViewer.flyTo(
//                 sceneObject.position,
//                 distance,
//                 [heading, pitch, 0],
//                 flyDuration ?? duration,
//             );
//         }));

//         this.dispose(sceneObject.calcFlyToParamEvent.disposableOn(() => {
//             if (!czmViewer.actived) return;

//             if (!sceneObject.position) {
//                 console.warn(`calcFlyToParamEvent error! !sceneObject.position`);
//                 return;
//             }
//             sceneObject.flyToParam = czmViewer.calcFlyToParam(sceneObject.position);
//         }));
//     }
// }
