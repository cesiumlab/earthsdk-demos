import { GeoCustomDivPoi, SceneObjectPickedInfo, Viewer } from 'xbsj-xe2/dist-node/xe2-base-objects';
import { CzmViewer } from 'xbsj-xe2/dist-node/xe2-cesium-objects';
import { ESPoi2D, ESWidget } from '../../objs';
import { bind, track, Destroyable, createNextAnimateFrameEvent, react } from 'xbsj-xe2/dist-node/xe2-base-utils';
import { getCzmPickedInfoFromPickedInfo } from './utils';
import { getObjectProperties, getPoi2DDefaultAnchor } from './fun';
import { WidgetEventInfo } from '@/esobjs-xe2-plugin-main/esobjs/base';
import { defaultFlyToRotation } from './publicConfig';

export class Widget2D extends Destroyable {
    // 自定义div内容
    public widgetInfo: HTMLElement;
    // 创建2D面板
    private _czmGeoCustomDivPoi = this.disposeVar(new GeoCustomDivPoi());
    get czmGeoCustomDivPoi() { return this._czmGeoCustomDivPoi; }
    public sceneObject;
    public domSize: { [xx: string]: any } | undefined = undefined;

    private _defaultAnchor = this.disposeVar(react<[number, number]>([0, 0]));
    get defaultAnchor() { return this._defaultAnchor.value; }
    set defaultAnchor(value: [number, number]) { this._defaultAnchor.value = value; }
    get defaultAnchorChanged() { return this._defaultAnchor.changed; }
    constructor(private _sceneObject: ESWidget | ESPoi2D, private _czmViewer: CzmViewer, private _widgetInfo: HTMLElement, private _drawWidget: boolean, listenAnchor: boolean = true) {
        super();
        let sceneObject = this.sceneObject = this._sceneObject;
        let czmViewer = this._czmViewer;
        this.widgetInfo = this._widgetInfo;
        {
            if (sceneObject instanceof ESPoi2D) {
                this.defaultAnchor = getPoi2DDefaultAnchor(this.widgetInfo, sceneObject.mode);
            }
        }

        const { czmGeoCustomDivPoi } = this;
        czmViewer.add(czmGeoCustomDivPoi);
        this.dispose(() => czmViewer.delete(czmGeoCustomDivPoi));

        this.d(track([czmGeoCustomDivPoi, 'zOrder'], [sceneObject, 'zOrder']));
        this.dispose(track([czmGeoCustomDivPoi, 'show'], [sceneObject, 'show']));
        this.dispose(bind([czmGeoCustomDivPoi, 'position'], [sceneObject, 'position']));
        this.dispose(bind([czmGeoCustomDivPoi, 'editing'], [sceneObject, 'editing']));
        if (sceneObject instanceof ESWidget)
            this.dispose(track([czmGeoCustomDivPoi, 'opacity'], [sceneObject, 'opacity']));
        {
            const event = this.dv(createNextAnimateFrameEvent(sceneObject.allowPickingChanged, sceneObject.editingChanged))
            const update = () => {
                if (sceneObject.allowPicking && !sceneObject.editing) {
                    czmGeoCustomDivPoi.allowPicking = true;
                } else {
                    czmGeoCustomDivPoi.allowPicking = false;
                }
            }
            update();
            this.d(event.don(update));
        }
        {
            this.d(czmGeoCustomDivPoi.pickedEvent.don(pickedInfo => {
                const pointerEvent = getObjectProperties(pickedInfo, "attachedInfo")?.pointerEvent;
                if (!pointerEvent) return;
                // 响应widgetEvent事件
                // 鼠标点击事件
                const eventInfo = {
                    type: pointerEvent.buttons != 0 && pointerEvent.button == 0 ? "leftClick" : pointerEvent.button == 2 ? "rightClick" : undefined,
                    add: { mousePos: [pointerEvent.offsetX, pointerEvent.offsetY] as [number, number] }
                }
                if (eventInfo.type == undefined) {
                    do {
                        if (pointerEvent.type == 'mouseleave') {
                            eventInfo.type = "mouseLeave";
                            break;
                        }
                        if (pointerEvent.type == 'mouseenter') {
                            eventInfo.type = "mouseEnter";
                            break;
                        }
                    } while (false);
                }
                sceneObject.widgetEvent.emit(eventInfo as WidgetEventInfo)
                // 左键事件，额外进行响应pickedEvent事件
                if (pointerEvent.buttons != 0 && pointerEvent.button == 0 && (sceneObject.allowPicking ?? false)) {
                    const pickInfo = getCzmPickedInfoFromPickedInfo(pickedInfo)
                    sceneObject.pickedEvent.emit(new SceneObjectPickedInfo(sceneObject, pickInfo));
                }
            }))
        }
        // info信息变更
        {
            const update = () => {
                try {
                    if (_drawWidget) {
                        czmGeoCustomDivPoi.instanceClass = this.createInstanceClass();
                    } else {
                        czmGeoCustomDivPoi.instanceClass = undefined;
                    }
                } catch (error) {
                    console.log(error);
                }
            }
            update();
        }
        // 偏移锚点更改
        {
            if (listenAnchor) {
                const update = () => {
                    const anchor = sceneObject.anchor;
                    const offset = sceneObject.offset;
                    if (anchor) {
                        // 锚点需要通过缩放比进行计算
                        czmGeoCustomDivPoi.originRatioAndOffset = [...anchor, -offset[0], -offset[1]];
                        // 修改锚点后需要修改div基准点
                        // this.widgetInfo.style.transformOrigin = `${sceneObject.anchor[0] * 100}% ${sceneObject.anchor[1] * 100}%`
                    } else {
                        czmGeoCustomDivPoi.originRatioAndOffset = [0, 0, 0, 0];
                    }
                }
                update();
                const event = this.ad(createNextAnimateFrameEvent(sceneObject.anchorChanged, sceneObject.offsetChanged));
                this.dispose(event.disposableOn(update))
            }
        }
        // 尺寸切换
        {
            const update = async () => {
                let width, height;
                if (!this.domSize || this.domSize.width == 0 || this.domSize.height == 0) {
                    this.domSize = this.widgetInfo.getBoundingClientRect();
                    if (this.domSize.width == 0 || this.domSize.height == 0) {
                        const time = setTimeout(() => {
                            update();
                            clearTimeout(time);
                        }, 200);
                        return;
                    }
                }
                if (sceneObject.sizeByContent) {
                    width = this.domSize.width;
                    height = this.domSize.height;
                } else {
                    width = sceneObject.size[0];
                    height = sceneObject.size[1];
                }
                // 设置屏幕div尺寸
                this.widgetInfo.style.transform = `scale(${width / this.domSize.width * sceneObject.scale[1]},${height / this.domSize.height * sceneObject.scale[2]})`;
                this.widgetInfo.style.transformOrigin = `${sceneObject.anchor[0] * 100}% ${sceneObject.anchor[1] * 100}%`
            }
            update();
            const updateEvent = this.disposeVar(createNextAnimateFrameEvent(
                sceneObject.sizeChanged,
                sceneObject.sizeByContentChanged,
                sceneObject.scaleChanged,
            ))
            this.dispose(updateEvent.disposableOn(update))
        }
    }
    public flyTo = (duration: number | undefined, id: number) => {
        if (this.czmGeoCustomDivPoi.position) {
            this._czmViewer.flyTo(this.czmGeoCustomDivPoi.position, 1000, defaultFlyToRotation, duration && duration * 1000);
            return;
        }
        this.czmGeoCustomDivPoi.flyTo(duration && duration * 1000);
    }
    private createInstanceClass = () => {
        let _this = this;
        return class MyDiv extends Destroyable {
            constructor(private _subContainer: HTMLDivElement, czmGeoCustomDivPoi: GeoCustomDivPoi<{ destroy(): undefined }>, viewer?: Viewer | undefined) {
                super();
                if (!viewer) return;
                if (!(viewer instanceof CzmViewer)) return;
                const div = _this.widgetInfo;
                this._subContainer.appendChild(div);
                this.dispose(() => {
                    this._subContainer.removeChild(div);
                });
                {
                    const update = () => {
                        div.style.pointerEvents = _this.sceneObject.editing ? "none" : "all";
                    }
                    update();
                    this.d(_this.sceneObject.editingChanged.don(update));
                }

            }
        }
    }
}
