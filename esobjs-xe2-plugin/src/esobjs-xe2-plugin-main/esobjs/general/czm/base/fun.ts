import { SceneObject } from "xbsj-xe2/dist-node/xe2-utils";
import { ESWidget } from "../../objs";
import { ESImageBaseInfo, ESVisualObject } from "@/esobjs-xe2-plugin-main/esobjs/base";
import { BoundingRectangle, BoundingSphere, OrientedBoundingBox, Rectangle } from "cesium";
import { positionFromCartesian } from "xbsj-xe2/dist-node/xe2-base-cesium";
import { clamp, Vector } from "xbsj-xe2/dist-node/xe2-math";
import { getGeoBoundingSphereFromPositions, PickedInfo } from "xbsj-xe2/dist-node/xe2-base-objects";
import { CzmCustomPrimitive, CzmCzmCustomPrimitive, CzmCzmModelPrimitive, CzmModelPrimitive, CzmViewer } from "xbsj-xe2/dist-node/xe2-cesium-objects";
import { defaultFlyToRotation } from "./publicConfig";
import * as Cesium from 'cesium';
// 创建ESWidget中div面板
export function getWidgetDiv(sceneObject: ESWidget, InfoItemRowBackGround: any, InfoBackGround: any) {
    const div = document.createElement('div');
    let title = undefined as undefined | string;
    let bodyDiv = ``;
    const infoKeys = Object.keys(sceneObject.info)
    for (let i = 0; i < infoKeys.length; i++) {
        if (infoKeys[i].toLowerCase() == 'title') {
            title = sceneObject.info[infoKeys[i]];
            continue;
        }
        bodyDiv += `<div style="
                display: flex;
                font-size: 10px;
                width: 100%;">
                    <div style="flex:1;position: relative;">
                        <img src="${InfoItemRowBackGround.url}" style="width:100%;height:100%;position:absolute;z-index:-1"/>
                        <span style="
                        display: inline-block;
                        box-sizing: border-box;
                        width: 100%;
                        padding: 5px;
                        ">${infoKeys[i]}</span>
                    </div>
                    <div style="flex:1;position: relative;">
                        <img src="${InfoItemRowBackGround.url}" style="width:100%;height:100%;position:absolute;z-index:-1"/>
                        <span style="
                        display: inline-block;
                        box-sizing: border-box;
                        width: 100%;
                        padding: 5px;
                        ">${sceneObject.info[infoKeys[i]]}</span>
                    </div>
                </div>`
    }
    div.innerHTML = `<div class="info" style=" 
            color: #fff;
            position: relative;
            box-sizing: border-box;
            width: ${InfoBackGround.width}px;
            height: ${InfoBackGround.height}px;
            background-repeat: round;
            background-size: cover;">
                <img src="${InfoBackGround.url}" style="width:100%;height:100%;position:absolute;z-index:-1"/>
                <div style="padding: 10px;height: 100%;width: 100%;box-sizing: border-box;">
                    <div class="top" style="
                    text-align: center;
                    font-size: 16px;
                    height: 30px;">
                        <span>${title}</span>
                    </div>
                    <div class="body" style="
                    overflow-y: auto;
                    height: calc(100% - 30px);">
                        ${bodyDiv}
                    </div>
                </div>
            </div>`;
    let styleNode = document.createElement("style");
    styleNode.setAttribute("type", "text/css");
    styleNode.innerHTML = style;
    div.appendChild(styleNode);
    return div;
}
// 图片转base64
export function imgUrl(imgUrl: string): Promise<ESImageBaseInfo | undefined> {
    return new Promise((resolve, reject) => {
        let image = new Image();
        image.src = imgUrl + "?v=" + Math.random(); // 处理缓存
        image.crossOrigin = "*"; // 支持跨域图片
        image.onload = function () {
            resolve({
                width: image.width,
                height: image.height,
                base64: getBase64Image(image),
                url: imgUrl
            }); //调用函数并将其转为base64图片
        };
        image.onerror = function () {
            resolve(undefined);
        }
    })
    function getBase64Image(img: HTMLImageElement) {
        let canvas = document.createElement("canvas");
        canvas.width = img.width;
        canvas.height = img.height;
        let ctx = canvas.getContext("2d") as CanvasRenderingContext2D;
        ctx.drawImage(img, 0, 0, img.width, img.height);
        let ext = img.src.substring(img.src.lastIndexOf(".") + 1).toLowerCase();
        let dataURL = canvas.toDataURL("image/" + ext);
        return dataURL;
    }
}
// 创建横向标注
export function getTransverseDiv(className: string, Images: any, contentName: any) {
    const { iconBox, icon, anchor } = Images;
    let iconDiv = ``;
    if (iconBox && icon) {
        iconDiv += `<img src="${iconBox.url}" class="${className}-iconBox" style="text-align:center !important"/>
                    <img src="${icon.url}" class="${className}-icon"
                        style="{{icon}} position:absolute !important;
                               top: ${iconBox.height * 0.5}px !important;
                               left: ${iconBox.width * 0.5}px !important;
                               transform: translate(-50%, -50%); !important"
                    />`;
    }
    if (anchor) {
        iconDiv += `<img src="${anchor.url}" class="${className}-anchor" 
                        style="{{anchor}} width:${anchor.width}px !important;
                                height:${anchor.height}px !important;
                                position:absolute !important;"
                    />`
    }
    return `
    <div style="{{textBoxTransverseParent}}
        color: #fff !important;
        display: flex !important;">
            <div style="width: ${iconBox?.width ?? anchor?.width ?? 0}px !important;position: relative !important;">
                ${iconDiv}
            </div>
            <div class="${className}-textBoxTransverse" style="{{textBoxTransverse}}
                    text-align:center !important;
                    position: relative !important;
                    min-width: 100px !important;
                    ">
                <div class="textDiv" style="position:absolute !important;left:0 !important;width:100% !important;z-index:-1 !important;">
                </div>
                <div style = "padding:0 5px !important;">
                    <span style="text-wrap:nowrap !important">
                        ${contentName}
                    </span>
                </div>
            </div>
    </div>`
}
//创建竖向标注
export function getVerticalDiv(className: string, images: any, contentName: any) {
    const { iconBox, icon, anchor } = images;
    let iconDiv = ``;
    if (iconBox != undefined && icon != undefined) {
        iconDiv += `<img src="${iconBox.url}" class="${className}-iconBox" style="width:${iconBox.width}px !important;height:${iconBox.height}px !important;text-align:center !important"/>
                    <img class="${className}-icon" src="${icon.url}"
                        style="{{icon}} width:${icon.width}px !important;
                                height:${icon.height}px !important;
                                position:absolute !important;
                                left: 50% !important;
                                transform: translate(-50%, -50%) !important;"
                    />`;
    }
    if (anchor != undefined) {
        iconDiv += `<img src="${anchor.url}" class="${className}-anchor" 
                        style="{{anchor}} width:${anchor.width}px !important;
                            height:${anchor.height}px !important;
                            position:absolute !important;"
                    />`
    }
    return `
    <div style=" 
        color: #fff !important;
        text-align: center !important;">
            <div class="${className}-textBoxVertical" style="{{textBoxVertical}}
            min-width: 100px !important;
            text-align:center !important;
            position: relative !important;">
                <div class="textDiv" style="position:absolute !important;left:0 !important;width:100% !important;z-index:-1 !important;">
                </div>
                <div style="padding:0 5px !important;">
                    <span style="text-wrap:nowrap !important">
                    ${contentName}
                    </span>
                </div>
            </div>
            <div style="{{textBoxVerticalBrother}} position: relative !important;">
               ${iconDiv}
            </div>
    </div>`
}
// 创建文本框
export function createTextDiv(textBox: ESImageBaseInfo[]) {
    let textDiv: string = ``;
    let height = 0;
    let tempWidth = 0;
    // 找到需要固定的宽度总计
    for (let i = 0; i < textBox.length; i += 2) {
        tempWidth += textBox[i].width;
    }
    // 一张图直接拉伸，多张图奇数固定宽度，偶数拉伸宽度
    for (let i = 0; i < textBox.length; i++) {
        if (textBox.length == 1) {
            textDiv += `<img src="${textBox[i].url}" style="{{img}} width: 100% !important;height: ${textBox[i].height}px !important;vertical-align: top !important"/>`
        } else if (i % 2 == 1) {
            textDiv += `<img src="${textBox[i].url}" style="{{img}}
                            width: calc(${100 / Math.floor(textBox.length / 2)}% - ${tempWidth / Math.floor(textBox.length / 2)}px) !important;
                            height: ${textBox[i].height}px !important;
                            vertical-align: top !important"
                        />`
        } else {
            textDiv += `<img src="${textBox[i].url}" style="{{img}} width: ${textBox[i].width}px !important;height: ${textBox[i].height}px !important;vertical-align: top !important"/>`
        }
        height = height > textBox[i].height ? height : textBox[i].height;
    }
    return { textDiv, height };
}
// 计算标注锚点
export function getPoi2DDefaultAnchor(widgetInfo: HTMLElement, mode: string) {
    // 通过createInstanceClass加载div后,获取不到尺寸，克隆个节点手动添加后获取尺寸后再删除
    let defaultAnchor: [number, number] = [0, 0];
    if (!widgetInfo || !mode) return defaultAnchor;
    const tempDiv = widgetInfo.cloneNode(true) as HTMLDivElement;
    tempDiv.style.position = "absolute";
    tempDiv.style.top = "0";
    tempDiv.style.zIndex = '-1';
    document.body.appendChild(tempDiv);
    const anchorImg = (tempDiv.querySelector(`.${mode}-anchor`) ?? tempDiv.querySelector(`.${mode}-iconBox`)) as HTMLImageElement;
    if (anchorImg && anchorImg.parentElement) {
        // ["P3D02", "P3D04", "Flag01"]锚点需要特殊处理，锚点根据锚点元素在整个标注的位置进行确定
        const anchorY =
            (anchorImg.parentElement.offsetTop + anchorImg.parentElement.offsetHeight - (["Flag01"].includes(mode) ? 10 : ["P3D02", "P3D04"].includes(mode) ? 15 : 0)) / tempDiv.offsetHeight;
        defaultAnchor = [
            (anchorImg.offsetLeft + anchorImg.width / 2) / tempDiv.offsetWidth,
            anchorY < 1 ? anchorY : 1
        ] as [number, number];
    }
    document.body.removeChild(tempDiv);
    return defaultAnchor;
}
// 获取ESPoi2D中的图片
export async function getModeImage(mode: string) {
    const excludeIcon = ["Flag01", "Flag02", 'Linear02', 'ManAbnormal', 'ManNormal', 'P3D02', 'P3D03', 'P3D04', 'P3D05', 'P3D06', 'P3D07', 'Stranger', 'WomanAbnormal', 'WomanNormal']
    const excludeAnchor = ['CircularV05', 'Diamond01'];
    const excludeIconBox = ['Flag01', 'Flag02', 'Linear02', 'Linear03', 'ManAbnormal', 'ManNormal', 'P3D02', 'P3D03', 'P3D04', 'P3D05', 'P3D06', 'P3D07', 'Stranger', 'WomanAbnormal', 'WomanNormal'];
    let TempTextBox: (ESImageBaseInfo | undefined)[] = [];
    const basePath = "${esobjs-xe2-plugin-assets-script-dir}/xe2-assets/esobjs-xe2-plugin/images/ESPoi2D/";
    const icon = excludeIcon.includes(mode) ? undefined : await imgUrl(SceneObject.context.getStrFromEnv(basePath + `/${mode}/icon.png`));
    const anchor = excludeAnchor.includes(mode) ? undefined : await imgUrl(SceneObject.context.getStrFromEnv(basePath + `/${mode}/anchor.png`));
    for (let i = 1; i <= (modeTextBoxCount[mode] ?? 3); i++) {
        TempTextBox.push(await imgUrl(SceneObject.context.getStrFromEnv(basePath + `/${mode}/textBox_0${i}.png`)));
    }
    const iconBox = excludeIconBox.includes(mode) ? undefined : await imgUrl(SceneObject.context.getStrFromEnv(basePath + `/${mode}/iconBox.png`));
    const textBox = TempTextBox.filter(item => item != undefined) as ESImageBaseInfo[];
    return { icon, anchor, iconBox, textBox };
}
// 创建ESPoi2D中的div面板
export function getESPoi2DDiv(mode: string, contentName: any, textBox: ESImageBaseInfo[], iconBox: ESImageBaseInfo | undefined, icon: ESImageBaseInfo | undefined, anchor: ESImageBaseInfo | undefined) {
    const Images = { iconBox, icon, anchor };
    const div = document.createElement("div");
    div.classList.add("czmPoi2dDiv");
    const isTransverseArr = ["SquareH", "CircularH", "Flag", "Linear01"];
    if (isTransverseArr.some((item) => {
        return mode.includes(item)
    })) {
        div.innerHTML = getTransverseDiv(mode, Images, contentName);
    } else {
        div.innerHTML = getVerticalDiv(mode, Images, contentName);
    }
    const { textDiv, height } = createTextDiv(textBox)
    let iconDiv: HTMLImageElement | undefined = undefined;
    if (mode == "Linear03" && icon) {
        iconDiv = document.createElement("img");
        iconDiv.classList.add(`${mode}-icon`);
        iconDiv.src = icon.url;
        iconDiv.style.width = `${icon.width}px`;
        iconDiv.style.height = `${icon.height}px`;
        iconDiv.style.position = "absolute";
        iconDiv.style.top = `2px`;
    }
    const textDivBox = div.querySelector(".textDiv");
    if (textDivBox && textDivBox.nextElementSibling) {
        do {
            const style = textDivBox.nextElementSibling.lastElementChild?.getAttribute('style');
            if (iconDiv) {
                textDivBox.nextElementSibling.insertAdjacentElement('afterbegin', iconDiv)
                textDivBox.nextElementSibling.lastElementChild?.setAttribute("style", style + `;margin-left: ${icon?.width}px;`);
                break;
            }
            if (['Stranger', 'Man', 'Woman'].some((item) => {
                return mode.includes(item)
            })) {
                textDivBox.nextElementSibling.lastElementChild?.setAttribute("style", style + `;margin-left: 50px;`);
                break;
            }
        } while (false);
    };
    if (textDivBox) textDivBox.innerHTML = textDiv;
    const { poi2DCss, poi2DCssObj } = getESPoi2DCss(mode, anchor, iconBox, height);
    //@ts-ignore
    // if (window.Poi2DCreateCounter && window.Poi2DCreateCounter[mode] == 1) {
    //     const styleNode = document.createElement("style");
    //     styleNode.classList.add(mode + "-style");
    //     styleNode.setAttribute("type", "text/css");
    //     styleNode.innerHTML = poi2DCss;
    //     document.body.appendChild(styleNode);
    // }
    return { div, styleObj: poi2DCssObj };
}
// 创建ESPoi2D中的CSS样式
export function getESPoi2DCss(mode: string, _anchor: ESImageBaseInfo | undefined, _iconBox: ESImageBaseInfo | undefined, height: number) {
    const divHeight = getESPoi2DDivHeight(mode, _anchor, _iconBox);
    const poi2DCssObj = {
        img: `display:inline-block !important;`,
        icon: `top: ${(_iconBox?.height ?? 0) * (mode == "CircularV05" ? 0.4 : 0.5)}px !important;`,
        //太恶心了，不知道为什么bottom和transform用在anchor上面使用html2canvas转换后，布局会乱
        anchor: `top:${_iconBox?.height ? _iconBox.height + (["Diamond02", "Linear01"].includes(mode) ? 0 : (_anchor?.height ?? 0)) : 0}px !important;
                left: calc(50% - ${(_anchor?.width ?? 0) / 2}px)!important;`,
        textBoxTransverseParent: `height:${divHeight}px !important;`,
        textBoxTransverse: `
                            margin-left:${_anchor && !["Flag02", "Linear01", "CircularH02", "CircularH01"].includes(mode)
                ? mode.includes("Flag01")
                    ? -_anchor.width / 2 + 2
                    : _anchor.height
                : (mode == "CircularH01"
                    ? 15
                    : mode == "CircularH02"
                        ? -2
                        : 2)}px !important;
                            line-height: ${mode == "CircularH01"
                ? height + 5
                : mode == "Linear01"
                    ? _iconBox?.height
                    : height}px !important;
                            margin-top:${_anchor && !mode.includes("Flag") && !mode.includes("SquareH") && !mode.includes("Linear01")
                ? _iconBox != undefined
                    ? (_iconBox.height - height) * 0.5 - (mode == "CircularH01"
                        ? 5
                        : 0)
                    : _anchor.height
                : 0}px !important;`,
        textBoxVertical: `height:${height}px !important;
                                    line-height: ${["SquareV01", "SquareV02", "SquareV03"].includes(mode)
                ? height - 5
                : height}px !important;
                                    margin-bottom: ${mode.includes("SquareV")
                ? 8
                : mode == "P3D03"
                    ? 5
                    : mode == "P3D02"
                        ? 24
                        : ["Diamond01", "P3D01"].includes(mode)
                            ? 11
                            : mode.includes("CircularV") || mode.includes("P3D") || ["Diamond02"].includes(mode)
                                ? 15
                                : 0}px !important;`,
        textBoxVerticalBrother: `height: ${divHeight}px !important;`

    }
    const poi2DCss = `
        .czmPoi2dDiv .textDiv img {${poi2DCssObj.img}}
        .czmPoi2dDiv .${mode}-icon {${poi2DCssObj.icon}}
        .czmPoi2dDiv .${mode}-anchor {${poi2DCssObj.anchor}}
        .czmPoi2dDiv > div:has(.${mode}-textBoxTransverse) {${poi2DCssObj.textBoxTransverseParent}}
        .czmPoi2dDiv .${mode}-textBoxTransverse {${poi2DCssObj.textBoxTransverse}}
        .czmPoi2dDiv .${mode}-textBoxVertical {${poi2DCssObj.textBoxVertical}}
        .czmPoi2dDiv .${mode}-textBoxVertical+div {${poi2DCssObj.textBoxVerticalBrother}}
        `;
    return { poi2DCss, poi2DCssObj }
}
export function getESPoi2DDivHeight(mode: string, _anchor: ESImageBaseInfo | undefined, _iconBox: ESImageBaseInfo | undefined) {
    // 高度是整个图片高度，还要加上边距，边距之前设置的是anchor的50%
    if (_iconBox == undefined) {
        return _anchor?.height;
    } else if (_anchor == undefined) {
        return _iconBox.height;
    } else {
        return _iconBox.height + _anchor.height * (["Diamond02", "Linear01", "Linear02", "Linear03"].includes(mode) ? 1 : 2);
    }
}
// 替换Poi2D中预留样式
export function replaceStr(template: string, poi2DCssObj: { [xx: string]: any }, noReplace?: boolean) {
    for (let key in poi2DCssObj) {
        const reg = new RegExp("\\{\\{" + key + "\\}\\}", "g")
        template = template.replace(reg, noReplace ? poi2DCssObj[key] : poi2DCssObj[key]);
        // template = template.replace(reg, noReplace ? "" : poi2DCssObj[key]);
    }
    return template;
}
// 查找对象属性
export function getObjectProperties(pickedInfo: any, searchKey: string): any {
    if (!pickedInfo) return undefined;
    if (!Object.prototype.hasOwnProperty.call(pickedInfo, searchKey)) {
        return getObjectProperties(pickedInfo.childPickedInfo, searchKey);
    } else {
        return pickedInfo[searchKey] != undefined ? Object.assign({}, pickedInfo[searchKey]) : undefined;
    }
}
// 获取包围盒中心点和视距，包围盒半径默认占屏幕128像素
export function getCenterAndViewDistance(czmViewer: CzmViewer, boundingVolume: BoundingSphere | BoundingRectangle | OrientedBoundingBox) {
    let viewDistance: number | undefined;
    let center: [number, number, number] | undefined;
    if (boundingVolume instanceof BoundingSphere) {
        center = positionFromCartesian(boundingVolume.center);
        viewDistance = boundingVolume.radius;
    } else if (boundingVolume instanceof BoundingRectangle) {
        const { x, y, width, height } = boundingVolume;
        const rectangle = Rectangle.fromRadians(x, y, x + width, y + height);
        const bs = BoundingSphere.fromRectangle2D(rectangle);
        center = positionFromCartesian(bs.center);
        viewDistance = bs.radius;
    } else if (boundingVolume instanceof OrientedBoundingBox) {
        center = positionFromCartesian(boundingVolume.center);
        const h = boundingVolume.halfAxes;
        const d0 = Vector.magnitude([h[0], h[1], h[2]]);
        const d1 = Vector.magnitude([h[3], h[4], h[5]]);
        const d2 = Vector.magnitude([h[6], h[7], h[8]]);
        viewDistance = Math.max(d0, d1, d2);
    }

    if (!center || viewDistance === undefined || !czmViewer.viewer) {
        return undefined;
    }
    viewDistance = getViewDistance(czmViewer, viewDistance);
    return [center, viewDistance] as [center: [number, number, number], viewDistance: number];
}
export async function flyWithPosition(czmViewer: CzmViewer, sceneObject: ESVisualObject, id: number, position: [number, number, number], radius: number, duration: number | undefined, useSceneObjectHeading: boolean = false) {
    if (!czmViewer.viewer) return;
    const viewDistance = getViewDistance(czmViewer, radius);
    const rotation = [...defaultFlyToRotation] as [number, number, number];
    if (useSceneObjectHeading) {
        //@ts-ignore
        rotation[0] = sceneObject.rotation[0] - 90;
    }
    const { position: flyPosition, rotation: flyRotation } = getFlyToCenterAndRotation(position, rotation, viewDistance);
    let r = await czmViewer.flyTo(flyPosition, 0, flyRotation, duration && duration * 1000);
    sceneObject.flyOverEvent.emit(id, r ? 'over' : r == undefined ? 'error' : 'cancelled', czmViewer);
    return r ?? false;
}
export async function flyWithPositions(czmViewer: CzmViewer, sceneObject: ESVisualObject, id: number, positions: [number, number, number][], duration: number | undefined, useSceneObjectHeading: boolean = false) {
    const centerAndRadius = getGeoBoundingSphereFromPositions(positions);
    if (centerAndRadius && centerAndRadius.center && centerAndRadius.radius && czmViewer.viewer) {
        const viewDistance = getViewDistance(czmViewer, centerAndRadius.radius);
        const rotation = [...defaultFlyToRotation] as [number, number, number];
        if (useSceneObjectHeading) {
            //@ts-ignore
            rotation[0] = sceneObject.rotation[0] - 90;
        }
        const { position: flyPosition, rotation: flyRotation } = getFlyToCenterAndRotation(centerAndRadius.center, rotation, viewDistance);
        let r = await czmViewer.flyTo(flyPosition, 0, flyRotation, duration && duration * 1000);
        sceneObject.flyOverEvent.emit(id, r ? 'over' : r == undefined ? 'error' : 'cancelled', czmViewer);
        return r ?? false;
    }
    return false;
}
export async function flyWithPrimitive(czmViewer: CzmViewer, sceneObject: ESVisualObject, id: number, duration: number | undefined, czmPrimitive: CzmCustomPrimitive | CzmModelPrimitive, useSceneObjectHeading: boolean = false) {
    let czmCustomPrimitive = czmViewer.getCzmObject(czmPrimitive) as CzmCzmCustomPrimitive | CzmCzmModelPrimitive;
    const cav = getCav(czmViewer, czmCustomPrimitive);
    if (cav != undefined) {
        const [center, viewDistance] = cav
        const rotation = [...defaultFlyToRotation] as [number, number, number];
        if (useSceneObjectHeading) {
            //@ts-ignore
            rotation[0] = sceneObject.rotation[0] - 90;
        }
        const { position: flyPosition, rotation: flyRotation } = getFlyToCenterAndRotation(center, rotation, viewDistance);
        let r = await czmViewer.flyTo(flyPosition, 0, flyRotation, duration && duration * 1000);
        sceneObject.flyOverEvent.emit(id, r ? 'over' : r == undefined ? 'error' : 'cancelled', czmViewer);
        return r ?? false;
    }
    return false;
}
export function getCav(czmViewer: CzmViewer, czmCustomPrimitiveTemp: CzmCzmCustomPrimitive | CzmCzmModelPrimitive) {
    if (czmCustomPrimitiveTemp instanceof CzmCzmCustomPrimitive
        && czmCustomPrimitiveTemp.nativePrimitive
        && czmCustomPrimitiveTemp.nativePrimitive.boundingVolume
    ) {
        return getCenterAndViewDistance(czmViewer, czmCustomPrimitiveTemp.nativePrimitive.boundingVolume);
    } else if (czmCustomPrimitiveTemp instanceof CzmCzmModelPrimitive
        && czmCustomPrimitiveTemp.primitive) {
        return getCenterAndViewDistance(czmViewer, czmCustomPrimitiveTemp.primitive.boundingSphere);
    }
}
function getViewDistance(czmViewer: CzmViewer, viewDistance: number) {
    viewDistance = Math.abs(viewDistance);
    if (viewDistance == 0) viewDistance = 1;
    if (!czmViewer.viewer) return viewDistance;
    // 获取切线长度 核心公式为夹角tan比值等于像素比值，从而通过像素比值和视口fov可以算出夹角大小，进而算出切线长度
    const tempTangent = viewDistance / (czmViewer.flyToBoundingSize * .5 / (czmViewer.viewer.canvas.width / 2) * Math.tan(Cesium.Math.toRadians(czmViewer.sceneCameraFrustumFov / 2)));
    // 勾股定理求视距
    return Math.sqrt(viewDistance * viewDistance + tempTangent * tempTangent)
}
export function getFlyToCenterAndRotation(targetPoint: [number, number, number], targetRotation: [number, number, number], viewDistance: number) {
    // 获取局部坐标点
    const localPosition = new Cesium.Cartesian3(
        -viewDistance * Math.cos(Cesium.Math.toRadians(targetRotation[1])) * Math.cos(Cesium.Math.toRadians(targetRotation[0] - 90)),
        viewDistance * Math.cos(Cesium.Math.toRadians(targetRotation[1])) * Math.sin(Cesium.Math.toRadians(targetRotation[0] - 90)),
        -viewDistance * Math.sin(Cesium.Math.toRadians(targetRotation[1]))
    );
    // 获取局部坐标矩阵
    const localMatrix = Cesium.Transforms.eastNorthUpToFixedFrame(Cesium.Cartesian3.fromDegrees(...targetPoint));
    // 本地坐标转世界坐标
    let worldPoint = Cesium.Matrix4.multiplyByPoint(localMatrix, localPosition, new Cesium.Cartesian3())

    const targetPointCar3 = Cesium.Cartesian3.fromDegrees(...targetPoint);

    //向量
    const normal = Cesium.Cartesian3.subtract(
        targetPointCar3,
        worldPoint,
        new Cesium.Cartesian3()
    );
    //归一化
    Cesium.Cartesian3.normalize(normal, normal);
    //旋转矩阵 rotationMatrixFromPositionVelocity源码中有，并未出现在cesiumAPI中
    const rotationMatrix3 = Cesium.Transforms.rotationMatrixFromPositionVelocity(
        worldPoint,
        normal,
        Cesium.Ellipsoid.WGS84
    );
    const modelMatrix4 = Cesium.Matrix4.fromRotationTranslation(
        rotationMatrix3,
        worldPoint
    );
    var m1 = Cesium.Transforms.eastNorthUpToFixedFrame(
        Cesium.Matrix4.getTranslation(modelMatrix4, new Cesium.Cartesian3()),
        Cesium.Ellipsoid.WGS84,
        new Cesium.Matrix4()
    );
    // 矩阵相除
    var m3 = Cesium.Matrix4.multiply(
        Cesium.Matrix4.inverse(m1, new Cesium.Matrix4()),
        modelMatrix4,
        new Cesium.Matrix4()
    );
    // 得到旋转矩阵
    var mat3 = Cesium.Matrix4.getMatrix3(m3, new Cesium.Matrix3());
    // 计算四元数
    var q = Cesium.Quaternion.fromRotationMatrix(mat3);
    // 计算旋转角(弧度)
    var hpr = Cesium.HeadingPitchRoll.fromQuaternion(q);
    const flyPosition = Cesium.Cartographic.fromCartesian(worldPoint)
    return {
        position: [
            Cesium.Math.toDegrees(flyPosition.longitude),
            Cesium.Math.toDegrees(flyPosition.latitude),
            flyPosition.height
        ] as [number, number, number],
        rotation: [
            Cesium.Math.toDegrees(hpr.heading) + 90,
            Cesium.Math.toDegrees(hpr.pitch),
            Cesium.Math.toDegrees(hpr.roll)
        ] as [number, number, number]
    }
}
// 旋转角转方向向量
export function getDirectionVector(rotation: [number, number, number]) {
    const heading = Cesium.Math.toRadians(rotation[0]);
    const pitch = Cesium.Math.toRadians(rotation[1]); // 将角度转换为弧度
    const roll = Cesium.Math.toRadians(rotation[2]);
    // 创建一个Matrix3来表示俯仰、偏航和滚转角
    const rotationMat3 = Cesium.Matrix3.fromHeadingPitchRoll(new Cesium.HeadingPitchRoll(heading, pitch, roll));
    // 获取方向向量
    const direction = new Cesium.Cartesian3();
    Cesium.Matrix3.multiplyByVector(rotationMat3, Cesium.Cartesian3.UNIT_X, direction);
    return [direction.x, direction.y, direction.z] as [number, number, number];
}
// 获取pointerEvent鼠标button数字
export function getPointerEventButton(pickedInfo: PickedInfo) {
    const pointerEvent = getObjectProperties(pickedInfo, "attachedInfo")?.pointerEvent as PointerEvent;
    // 默认值修改为0,找不到鼠标事件，一律按自定义调用处理
    return pointerEvent?.button ?? 0;
}
// 滚动条样式
const style = `
        /*定义滚动条高宽及背景
        高宽分别对应横竖滚动条的尺寸*/
        .info ::-webkit-scrollbar,.body ::-webkit-scrollbar {
            width: 5px;
            height:5px;
            background-color: #F5F5F5;
            border-radius: 8px;
        }
    
        /*定义滚动条轨道
        内阴影+圆角*/
        .info ::-webkit-scrollbar-track,.body ::-webkit-scrollbar-track{
            -webkit-box-shadow: inset 0 0 6px rgba(0, 0, 0, 0.3);
            border-radius: 8px;
            background-color: #F5F5F5;
        }
    
        /*定义滑块
        内阴影+圆角*/
        .info ::-webkit-scrollbar-thumb,.body ::-webkit-scrollbar-thumb{
            border-radius: 8px;
            -webkit-box-shadow: inset 0 0 6px rgba(0, 0, 0, .3);
            background-color: green;
        }`;
// 每个ESPoi2D类型文本框组成数量
const modeTextBoxCount = {
    "SquareH01": 1,
    "SquareH02": 3,
    "SquareV01": 5,
    "SquareV02": 5,
    "SquareV03": 5,
    "SquareV04": 3,
    "Flag01": 3,
    "Flag02": 3,
    "Linear01": 0,
    "Linear02": 3,
    "Linear03": 3,
    "CircularH01": 3,
    "CircularH02": 3,
    "CircularV01": 1,
    "CircularV02": 3,
    "CircularV03": 3,
    "CircularV04": 3,
    "CircularV05": 2,
    "P3D01": 3,
    "P3D02": 3,
    "P3D03": 1,
    "P3D04": 3,
    "P3D05": 1,
    "P3D06": 1,
    "P3D07": 1,
    "Diamond01": 3,
    "Diamond02": 3
} as { [xx: string]: number }
export function isJSONString(str: string) {
    try {
        JSON.parse(str);
        return true;
    } catch (e) {
        return false;
    }
}