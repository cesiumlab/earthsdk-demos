import { CzmViewer } from "xbsj-xe2/dist-node/xe2-cesium-objects";
import { ESFlyToParam, ESObjectWithLocation } from "../../..";
import { CzmPickedInfo, PickedInfo } from "xbsj-xe2/dist-node/xe2-base-objects";

export function flyToUsingFlyToParam(czmViewer: CzmViewer, position: [number, number, number], flyToParam: ESFlyToParam, duration?: number) {
    if (!czmViewer.actived) return;

    const { heading, pitch, hDelta, pDelta, distance, flyDuration } = flyToParam;

    czmViewer.flyTo(
        position,
        distance,
        [heading, pitch, 0],
        duration ?? flyDuration,
    );
}

export function flyToESObjectWithLocation(czmViewer: CzmViewer, sceneObject: ESObjectWithLocation, duration?: number) {
    if (!sceneObject.position) {
        console.warn(`flyInEvent error! !sceneObject.position`);
        return;
    }

    if (!sceneObject.flyToParam) {
        console.warn(`flyInEvent error! !sceneObject.flyToParam`);
        return;
    }

    flyToUsingFlyToParam(czmViewer, sceneObject.position, sceneObject.flyToParam);
}

export function calcFlyToParamWithESObjectWithLocation(czmViewer: CzmViewer, sceneObject: ESObjectWithLocation) {
    if (!czmViewer.actived) return;

    if (!sceneObject.position) {
        console.warn(`calcFlyToParamEvent error! !sceneObject.position`);
        return;
    }
    sceneObject.flyToParam = czmViewer.calcFlyToParam(sceneObject.position);
}


export function getCzmPickedInfoFromPickedInfo(pickedInfo?: PickedInfo): CzmPickedInfo | undefined {
    if (!pickedInfo) return undefined;
    // Check if pickedInfo is already of type CzmPickedInfo
    if (pickedInfo instanceof CzmPickedInfo) {
        return pickedInfo;
    }
    // If not, try to find it in childPickedInfo (assuming it exists)
    const childCzmPickedInfo = pickedInfo.childPickedInfo;
    if (childCzmPickedInfo) {
        return getCzmPickedInfoFromPickedInfo(childCzmPickedInfo);
    }
    // If childPickedInfo is also undefined, return undefined
    return undefined;
}
