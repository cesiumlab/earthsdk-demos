// import { CzmESVisualObject } from "@/esobjs-xe2-plugin-main/esobjs/base";
// import { SMGeoDiagonalArrow } from 'smplotting-xe2-plugin/dist-node/smplotting-xe2-plugin-main';
// import { SceneObjectPickedInfo, getMinMaxCorner } from 'xbsj-xe2/dist-node/xe2-base-objects';
// import { CzmViewer } from 'xbsj-xe2/dist-node/xe2-cesium-objects';
// import { ESDiagonalArrow } from '../../objs';
// import { bind, track } from "xbsj-xe2/dist-node/xe2-base-utils";
// import { getCzmPickedInfoFromPickedInfo } from "../base/utils";
// import { flyWithPositions } from "../base";
// export class CzmESDiagonalArrow extends CzmESVisualObject<ESDiagonalArrow> {
//     static readonly type = this.register(ESDiagonalArrow.type, this);
//     private _czmESDiagonalArrow = this.disposeVar(new SMGeoDiagonalArrow());
//     get czmESDiagonalArrow() { return this._czmESDiagonalArrow; }

//     constructor(sceneObject: ESDiagonalArrow, czmViewer: CzmViewer) {
//         super(sceneObject, czmViewer);
//         const viewer = czmViewer.viewer;
//         if (!viewer) {
//             console.warn(`viewer is undefined!`);
//             return;
//         }

//         const czmESDiagonalArrow = this._czmESDiagonalArrow;
//         // @ts-ignore
//         czmViewer.add(czmESDiagonalArrow);
//         // @ts-ignore

//         this.dispose(() => czmViewer.delete(czmESDiagonalArrow))
//         this.dispose(track([czmESDiagonalArrow, 'show'], [sceneObject, 'show']));
//         this.dispose(bind([czmESDiagonalArrow, 'editing'], [sceneObject, 'editing']));
//         this.dispose(bind([czmESDiagonalArrow, 'pointEditing'], [sceneObject, 'pointEditing']));
//         this.dispose(track([czmESDiagonalArrow, 'allowPicking'], [sceneObject, 'allowPicking']));
//         this.dispose(bind([czmESDiagonalArrow, 'positions'], [sceneObject, 'points']));
//         this.dispose(track([czmESDiagonalArrow, 'depth'], [sceneObject, 'depth']));
//         this.dispose(track([czmESDiagonalArrow, 'ground'], [sceneObject, 'ground']));

//         // this.dispose(track([czmESDiagonalArrow, 'outline'], [sceneObject, 'outline']));
//         // this.dispose(track([czmESDiagonalArrow, 'outlineColor'], [sceneObject, 'outlineColor']));
//         // this.dispose(track([czmESDiagonalArrow, 'outlineWidth'], [sceneObject, 'outlineWidth']));
//         // this.dispose(track([czmESDiagonalArrow, 'fill'], [sceneObject, 'fill']));
//         // this.dispose(track([czmESDiagonalArrow, 'color'], [sceneObject, 'color']));

//         this.dispose(track([czmESDiagonalArrow, 'outline'], [sceneObject, 'stroked']));
//         this.dispose(track([czmESDiagonalArrow, 'outlineColor'], [sceneObject, 'strokeColor']));
//         this.dispose(track([czmESDiagonalArrow, 'outlineWidth'], [sceneObject, 'strokeWidth']));
//         this.dispose(track([czmESDiagonalArrow, 'fill'], [sceneObject, 'filled']));
//         this.dispose(track([czmESDiagonalArrow, 'color'], [sceneObject, 'fillColor']));

//         this.dispose(czmESDiagonalArrow.pickedEvent.disposableOn(pickedInfo => {
//             if (sceneObject.allowPicking ?? false) {
//                 const pickInfo = getCzmPickedInfoFromPickedInfo(pickedInfo)
//                 sceneObject.pickedEvent.emit(new SceneObjectPickedInfo(sceneObject, pickInfo));
//             }
//         }));
//     }
//     override flyTo(duration: number | undefined, id: number): boolean {
//         const { sceneObject, czmViewer, czmESDiagonalArrow } = this;
//         if (!czmViewer.actived) return false;
//         if (sceneObject.flyToParam || sceneObject.flyInParam) {
//             return super.flyTo(duration, id);
//         } else {
//             if (czmESDiagonalArrow.positions) {
//                 flyWithPositions(czmViewer, sceneObject, id, czmESDiagonalArrow.positions, duration);
//                 return true;
//             }
//             return false;
//         }
//     }
//     override flyIn(duration: number | undefined, id: number): boolean {
//         const { sceneObject, czmViewer, czmESDiagonalArrow } = this;
//         if (!czmViewer.actived) return false;
//         if (sceneObject.flyInParam) {
//             return super.flyIn(duration, id);
//         } else {
//             const d = duration ?? 1
//             const { center } = getMinMaxCorner(czmESDiagonalArrow.positions as [number, number, number][]);
//             this.flyToWithPromise(id, center, undefined, undefined, d)
//             return true;
//         }
//     }
// }
