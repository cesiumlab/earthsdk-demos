// import { SMGeoCircle } from 'smplotting-xe2-plugin/dist-node/smplotting-xe2-plugin-main'
// import { CzmObject, CzmViewer } from 'xbsj-xe2/dist-node/xe2-cesium-objects';
// import { ESGeoCircle } from 'esobjs-xe2-plugin/dist-node/esobjs-xe2-plugin-main';
// import { SceneObjectPickedInfo } from 'xbsj-xe2/dist-node/xe2-base-objects';

// import { bind, track } from "xbsj-xe2/dist-node/xe2-base-utils";
// export class CzmESGeoCircle extends CzmObject<ESGeoCircle> {
//     static readonly type = this.register(ESGeoCircle.type, this);

//     constructor(sceneObject: ESGeoCircle, czmViewer: CzmViewer) {
//         super(sceneObject, czmViewer);
//         const viewer = czmViewer.viewer;
//         if (!viewer) {
//             console.warn(`viewer is undefined!`);
//             return;
//         }

//         const czmESCircle = this.disposeVar(new SMGeoCircle());
//         czmViewer.add(czmESCircle);
//         this.dispose(() => czmViewer.delete(czmESCircle))
//         this.dispose(track([czmESCircle, 'show'], [sceneObject, 'show']));
//         this.dispose(bind([czmESCircle, 'editing'], [sceneObject, 'editing']));
//         this.dispose(bind([czmESCircle, 'pointEditing'], [sceneObject, 'pointEditing']));
//         this.dispose(track([czmESCircle, 'allowPicking'], [sceneObject, 'allowPicking']));
//         this.dispose(bind([czmESCircle, 'positions'], [sceneObject, 'points']));
//         this.dispose(track([czmESCircle, 'outline'], [sceneObject, 'outline']));
//         this.dispose(track([czmESCircle, 'outlineColor'], [sceneObject, 'outlineColor']));
//         this.dispose(track([czmESCircle, 'outlineWidth'], [sceneObject, 'outlineWidth']));
//         this.dispose(track([czmESCircle, 'fill'], [sceneObject, 'fill']));
//         this.dispose(track([czmESCircle, 'color'], [sceneObject, 'color']));
//         this.dispose(track([czmESCircle, 'depth'], [sceneObject, 'depth']));
//         this.dispose(track([czmESCircle, 'ground'], [sceneObject, 'ground']));
//         this.dispose(czmESCircle.pickedEvent.disposableOn(pickedInfo => {
//             if (sceneObject.allowPicking ?? false) {
//                 sceneObject.pickedEvent.emit(new SceneObjectPickedInfo(sceneObject, pickedInfo));
//             }
//         }));

//         this.dispose(sceneObject.flyToEvent.disposableOn(duration => {
//             czmESCircle.flyTo(duration);
//         }));
//     }
// }
