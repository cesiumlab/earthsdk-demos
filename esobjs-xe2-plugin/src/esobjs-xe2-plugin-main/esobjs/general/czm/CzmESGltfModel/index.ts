import { CzmESObjectWithLocation, rpToap } from '@/esobjs-xe2-plugin-main/esobjs/base';
import { SceneObject, SceneObjectPickedInfo } from 'xbsj-xe2/dist-node/xe2-base-objects';
import { bind, createNextAnimateFrameEvent, track } from 'xbsj-xe2/dist-node/xe2-base-utils';
import { CzmModelPrimitive, CzmViewer, getViewerExtensions } from 'xbsj-xe2/dist-node/xe2-cesium-objects';
import { ESGltfModel } from '../../objs';
import { getCzmPickedInfoFromPickedInfo } from '../base/utils';
import { flyWithPrimitive, getPointerEventButton } from '../base';

export class CzmESGltfModel<T extends ESGltfModel = ESGltfModel> extends CzmESObjectWithLocation<T> {
    static readonly type = this.register<ESGltfModel>(ESGltfModel.type, this);
    private _czmModel = this.dv(new CzmModelPrimitive());
    get model() { return this._czmModel };

    constructor(sceneObject: T, czmViewer: CzmViewer) {
        super(sceneObject, czmViewer);

        const viewer = czmViewer.viewer;
        if (!viewer) {
            console.warn(`viewer is undefined!`);
            return;
        }
        const extensions = getViewerExtensions(czmViewer.viewer);
        if (!extensions) {
            return;
        }

        const model = this._czmModel;
        czmViewer.add(model);
        this.d(() => czmViewer.delete(model))

        this.d(model.readyEvent.don((m) => { sceneObject.czmModelReadyEvent.emit(m) }))
        this.d(track([model, 'show'], [sceneObject, 'show']));
        this.d(track([model, 'color'], [sceneObject, 'czmColor']));
        this.d(track([model, 'nativeMaximumScale'], [sceneObject, 'czmMaximumScale']));
        this.d(track([model, 'nativeMinimumPixelSize'], [sceneObject, 'czmMinimumPixelSize']));
        this.d(bind([model, 'position'], [sceneObject, 'position']));
        // this.d(track([model, 'nativeScale'], [sceneObject, 'scale'], (s: [number, number, number]) => s[0]));
        this.d(track([model, 'nativeScale'], [sceneObject, 'czmNativeScale']));
        this.d(track([model, 'scale'], [sceneObject, 'scale']));
        // CzmModelPrimitive是northRotation，需要使用bindNorthRotation vtxf 20230925
        // this.d(bindNorthRotation([model, 'rotation'], [sceneObject, 'rotation']));
        // this.d(bind([model, 'positionEditing'], [sceneObject, 'editing']))
        {
            const updateRotation = () => {
                const temp = [...(sceneObject.rotation ?? [0, 0, 0])] as [number, number, number];
                temp[0] += sceneObject.instances ? 0 : 90;
                model.rotation = temp;
            }
            updateRotation();
            this.d(sceneObject.rotationChanged.don(updateRotation));
            {
                const update = () => {
                    updateRotation();
                    if (!sceneObject.instances) {
                        model.instances = sceneObject.instances;
                        return;
                    }
                    model.instances = sceneObject.instances.map(instance => {
                        const temp = JSON.parse(JSON.stringify(instance));
                        !temp.rotation && (temp.rotation = [0, 0, 0]);
                        temp.rotation[0] += 90;
                        return temp;
                    });
                }
                update();
                this.ad(sceneObject.instancesChanged.don(update));
            }
        }
        {
            const update = () => {
                const temp = [...model.rotation] as [number, number, number];
                temp[0] -= sceneObject.instances ? 0 : 90;
                sceneObject.rotation = temp;
            }
            update();
            this.d(model.rotationChanged.don(update));
        }

        {
            const event = this.dv(createNextAnimateFrameEvent(sceneObject.allowPickingChanged, sceneObject.editingChanged))
            const update = () => {
                if (sceneObject.allowPicking && !sceneObject.editing) {
                    model.allowPicking = true;
                } else {
                    model.allowPicking = false;
                }
            }
            update();
            this.d(event.don(update));
        }

        const updateUrl = () => { model.url = SceneObject.context.getStrFromEnv(rpToap(typeof sceneObject.url == 'string' ? sceneObject.url : sceneObject.url.url)); }
        updateUrl()
        this.d(sceneObject.urlChanged.don(updateUrl));

        this.d(sceneObject.setNodePositionEvent.don((NodeName, NodePosition) => {
            model.setNodeTranslation(NodeName, NodePosition)
        }));

        this.d(sceneObject.setNodeRotationEvent.don((NodeName, NodeRotation) => {
            model.setNodeRotation(NodeName, NodeRotation)
        }));
        this.d(sceneObject.setNodeScaleEvent.don((NodeName, NodeScale) => {
            model.setNodeScale(NodeName, NodeScale)
        }));
        this.d(sceneObject.printDebugInfoEvent.don(() => {
            model.printDebugInfo();
        }));

        this.d(model.pickedEvent.don(pickedInfo => {
            if (getPointerEventButton(pickedInfo) === 0 && (sceneObject.allowPicking ?? false)) {
                const pickInfo = getCzmPickedInfoFromPickedInfo(pickedInfo)
                sceneObject.pickedEvent.emit(new SceneObjectPickedInfo(sceneObject, pickInfo));
            }
        }));
    }
    override flyTo(duration: number | undefined, id: number): boolean {
        const { sceneObject, czmViewer, model } = this;
        if (!czmViewer.actived) return false;
        if (sceneObject.flyToParam || sceneObject.flyInParam) {
            return super.flyTo(duration, id);
        } else {
            flyWithPrimitive(czmViewer, sceneObject, id, duration, model, true);
            return true;
        }
    }
}
