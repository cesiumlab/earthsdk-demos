import { CzmESObjectWithLocation } from "@/esobjs-xe2-plugin-main/esobjs/base";
import { View } from 'xbsj-xe2/dist-node/xe2-base-objects';
import { bind } from 'xbsj-xe2/dist-node/xe2-base-utils';
import { CzmViewer, getViewerExtensions } from 'xbsj-xe2/dist-node/xe2-cesium-objects';
import { ESCameraView } from '../../objs';

export class CzmESCameraView extends CzmESObjectWithLocation<ESCameraView> {
    static readonly type = this.register(ESCameraView.type, this);
    private _view = this.disposeVar(new View());
    get view() { return this._view; }

    constructor(sceneObject: ESCameraView, czmViewer: CzmViewer) {
        super(sceneObject, czmViewer);

        // ESCameraView旋转编辑时，需要设置初始heading角度朝北
        if (this.sPrsEditing.prsEditing) {
            this.sPrsEditing.prsEditing.sRotationEditing.geoRotator.rotation = [0, 0, 0];
        }

        const viewer = czmViewer.viewer;
        if (!viewer) {
            console.warn(`viewer is undefined!`);
            return;
        }
        const extensions = getViewerExtensions(czmViewer.viewer);
        if (!extensions) {
            return;
        }

        const { view } = this;
        czmViewer.add(view)
        this.dispose(() => { czmViewer.delete(view) })
        this.dispose(bind([view, 'position'], [sceneObject, 'position']));
        // CzmESCameraView不能用bindNorthRotation，因为，它就是需要0为正北朝向
        // this.dispose(bindNorthRotation([view, 'rotation'], [sceneObject, 'rotation']));
        this.dispose(bind([view, 'rotation'], [sceneObject, 'rotation']));
        this.dispose(bind([view, 'thumbnail'], [sceneObject, 'thumbnail']));
        view.show = false

        this.dispose(sceneObject.flyInEvent.disposableOn(duration => {
            view.flyTo((sceneObject.duration ?? (duration ?? 1)) * 1000)
        }));

        this.dispose(sceneObject.resetWithCurrentCameraEvent.disposableOn(() => {
            view.resetWithCurrentCamera(View.ResetFlag.Position | View.ResetFlag.Rotation | View.ResetFlag.ViewDistance);
        }));

        this.dispose(sceneObject.captureEvent.disposableOn((x, y) => {
            const { thumbnailWidth, thumbnailHeight } = ESCameraView.defaults;
            view.capture(x ?? thumbnailWidth, y ?? thumbnailHeight);
        }));
    }
    override flyTo(duration: number | undefined, id: number): boolean {
        const { sceneObject, czmViewer, view } = this;
        if (!czmViewer.actived) return false;
        if (sceneObject.flyToParam || sceneObject.flyInParam) {
            return super.flyTo(duration, id);
        } else {
            view.flyTo(duration && duration * 1000);
            sceneObject.flyOverEvent.emit(id, 'over', czmViewer);
            return true;
        }
    }
}
