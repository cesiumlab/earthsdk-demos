import { CzmESVisualObject } from '@/esobjs-xe2-plugin-main/esobjs/base';
import { CzmImageryProviderJsonType, ESSceneObject } from 'xbsj-xe2/dist-node/xe2-base-objects';
import { createNextAnimateFrameEvent, track } from "xbsj-xe2/dist-node/xe2-base-utils";
import { CzmImagery, CzmViewer } from 'xbsj-xe2/dist-node/xe2-cesium-objects';
import { ESGeHistoryImagery } from '../../objs';

export class CzmESGeHistoryImagery extends CzmESVisualObject<ESGeHistoryImagery> {
    static readonly type = this.register(ESGeHistoryImagery.type, this);

    constructor(sceneObject: ESGeHistoryImagery, czmViewer: CzmViewer) {
        super(sceneObject, czmViewer);
        const viewer = czmViewer.viewer;
        if (!viewer) {
            console.warn(`viewer is undefined!`);
            return;
        }

    }
    override flyTo(duration: number | undefined, id: number): boolean {
        const { sceneObject, czmViewer } = this;
        if (!czmViewer.actived) return false;
        if (sceneObject.flyToParam || sceneObject.flyInParam) {
            return super.flyTo(duration, id);
        } else {
            sceneObject.esImageryLayer.flyTo(duration && duration);
            sceneObject.flyOverEvent.emit(id, 'over', czmViewer);
            return true;
        }
    }
    override flyIn(duration: number | undefined, id: number): boolean {
        const { sceneObject, czmViewer } = this;

        if (!czmViewer.actived) return false;
        if (sceneObject.flyInParam) {
            return super.flyIn(duration, id);
        } else {
            sceneObject.esImageryLayer.flyTo(duration && duration);
            sceneObject.flyOverEvent.emit(id, 'over', czmViewer);
            return true;
        }
    }
}
