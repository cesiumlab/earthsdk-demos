import { CzmESObjectWithLocation } from "@/esobjs-xe2-plugin-main/esobjs/base";
import { SceneObjectPickedInfo } from 'xbsj-xe2/dist-node/xe2-base-objects';
import { bind, track } from 'xbsj-xe2/dist-node/xe2-base-utils';
import { CzmCityBasePoint, CzmViewer, getViewerExtensions } from 'xbsj-xe2/dist-node/xe2-cesium-objects';
import { ESCityBasePoint } from '../../general/objs';
import { getCzmPickedInfoFromPickedInfo } from "./base/utils";
import { flyWithPrimitive, getPointerEventButton } from "./base";

export class CzmESCityBasePoint extends CzmESObjectWithLocation<ESCityBasePoint> {
    static readonly type = this.register(ESCityBasePoint.type, this);
    private _czmCityBasePoint = this.disposeVar(new CzmCityBasePoint());
    get czmCityBasePoint() { return this._czmCityBasePoint; }

    constructor(sceneObject: ESCityBasePoint, czmViewer: CzmViewer) {
        super(sceneObject, czmViewer);
        const viewer = czmViewer.viewer;
        if (!viewer) {
            console.warn(`viewer is undefined!`);
            return;
        }

        const extensions = getViewerExtensions(czmViewer.viewer);
        if (!extensions) {
            return;
        }

        const czmCityBasePoint = this._czmCityBasePoint;
        czmViewer.add(czmCityBasePoint);
        this.dispose(() => czmViewer.delete(czmCityBasePoint));

        this.dispose(track([czmCityBasePoint, 'show'], [sceneObject, 'show']));
        this.dispose(track([czmCityBasePoint, 'scale'], [sceneObject, 'scale']));
        this.dispose(track([czmCityBasePoint, 'allowPicking'], [sceneObject, 'allowPicking']));
        this.dispose(track([czmCityBasePoint, 'color'], [sceneObject, 'color']));
        this.dispose(track([czmCityBasePoint, 'color'], [sceneObject, 'color']));
        this.dispose(bind([czmCityBasePoint, 'position'], [sceneObject, 'position']));

        this.dispose(czmCityBasePoint.pickedEvent.disposableOn(pickedInfo => {
            if (getPointerEventButton(pickedInfo) === 0 && (sceneObject.allowPicking ?? false)) {
                const pickInfo = getCzmPickedInfoFromPickedInfo(pickedInfo)
                sceneObject.pickedEvent.emit(new SceneObjectPickedInfo(sceneObject, pickInfo));
            }
        }));
    }
    override flyTo(duration: number | undefined, id: number): boolean {
        const { sceneObject, czmViewer, czmCityBasePoint } = this;
        if (!czmViewer.actived) return false;
        if (sceneObject.flyToParam || sceneObject.flyInParam) {
            return super.flyTo(duration, id);
        } else {
            flyWithPrimitive(czmViewer, sceneObject, id, duration, czmCityBasePoint.cylinderCustomPrimitive, true);
            return true;
        }
    }
}
