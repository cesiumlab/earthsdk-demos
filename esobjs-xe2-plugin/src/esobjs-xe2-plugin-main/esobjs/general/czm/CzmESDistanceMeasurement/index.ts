import { getMinMaxCorner } from 'xbsj-xe2/dist-node/xe2-base-objects';
import { CzmESVisualObject } from '@/esobjs-xe2-plugin-main/esobjs/base';
import { GeoDistanceMeasurement, SceneObjectPickedInfo } from 'xbsj-xe2/dist-node/xe2-base-objects';
import { JsonValue, bind, createNextAnimateFrameEvent, track } from "xbsj-xe2/dist-node/xe2-base-utils";
import { CzmViewer } from 'xbsj-xe2/dist-node/xe2-cesium-objects';
import { ESDistanceMeasurement } from '../../objs';
import { getCzmPickedInfoFromPickedInfo } from '../base/utils';
import { flyWithPositions, getPointerEventButton } from '../base';

export class CzmESDistanceMeasurement extends CzmESVisualObject<ESDistanceMeasurement> {
    static readonly type = this.register(ESDistanceMeasurement.type, this);

    private _czmDistanceMeasurement = this.disposeVar(new GeoDistanceMeasurement());
    get czmDistanceMeasurement() { return this._czmDistanceMeasurement; }

    constructor(sceneObject: ESDistanceMeasurement, czmViewer: CzmViewer) {
        super(sceneObject, czmViewer);
        const viewer = czmViewer.viewer;
        if (!viewer) {
            console.warn(`viewer is undefined!`);
            return;
        }

        const { czmDistanceMeasurement } = this;
        czmViewer.add(czmDistanceMeasurement);
        this.dispose(() => czmViewer.delete(czmDistanceMeasurement))
        this.dispose(track([czmDistanceMeasurement, 'show'], [sceneObject, 'show']));
        this.dispose(track([czmDistanceMeasurement, 'allowPicking'], [sceneObject, 'allowPicking']));
        this.dispose(bind([czmDistanceMeasurement, 'positions'], [sceneObject, 'points']));
        this.dispose(bind([czmDistanceMeasurement, 'editing'], [sceneObject, 'editing']));
        this.dispose(bind([czmDistanceMeasurement, 'strokeGround'], [sceneObject, 'strokeGround']));
        {
            const updateProp = () => {
                const stroked = sceneObject.stroked
                if (!stroked) {
                    czmDistanceMeasurement.width = 0;
                    return
                } else {
                    czmDistanceMeasurement.width = sceneObject.strokeWidth;
                }

                czmDistanceMeasurement.width = sceneObject.strokeWidth;
                czmDistanceMeasurement.color = sceneObject.strokeColor;

                const strokeMaterial = sceneObject.strokeMaterial ?? 'normal'
                if (strokeMaterial === 'hasDash') {
                    czmDistanceMeasurement.hasDash = true
                    czmDistanceMeasurement.hasArrow = false
                } else if (strokeMaterial === 'hasArrow') {
                    czmDistanceMeasurement.hasDash = false
                    czmDistanceMeasurement.hasArrow = true
                } else if (strokeMaterial === 'normal') {
                    czmDistanceMeasurement.hasDash = false
                    czmDistanceMeasurement.hasArrow = false
                } else {
                    czmDistanceMeasurement.hasDash = false
                    czmDistanceMeasurement.hasArrow = false
                }

                const strokeStyle = sceneObject.strokeStyle
                if (strokeStyle.material === 'hasDash' && strokeStyle.materialParams) {
                    try {
                        const params = strokeStyle.materialParams as ({ [x: string]: JsonValue })
                        if (Reflect.has(params, 'gapColor')) {
                            czmDistanceMeasurement.gapColor = params.gapColor as [number, number, number, number] ?? [0, 0, 0, 0];
                        }
                        if (Reflect.has(params, 'dashLength')) {
                            czmDistanceMeasurement.dashLength = params.dashLength as number
                        }
                        if (Reflect.has(params, 'dashPattern')) {
                            czmDistanceMeasurement.dashPattern = params.dashPattern as number
                        }
                    } catch (error) {
                        console.error(error)
                    }
                } else {
                    czmDistanceMeasurement.gapColor = [0, 0, 0, 0];
                    czmDistanceMeasurement.dashLength = 0;
                    czmDistanceMeasurement.dashPattern = 0;
                }
            }
            updateProp();
            const updateEvent = this.disposeVar(createNextAnimateFrameEvent(
                sceneObject.strokeStyleChanged,
                sceneObject.strokedChanged,
            ));
            this.dispose(updateEvent.disposableOn(updateProp));
        }

        this.dispose(czmDistanceMeasurement.pickedEvent.disposableOn(pickedInfo => {
            if (getPointerEventButton(pickedInfo) === 0 && (sceneObject.allowPicking ?? false)) {
                const pickInfo = getCzmPickedInfoFromPickedInfo(pickedInfo)

                sceneObject.pickedEvent.emit(new SceneObjectPickedInfo(sceneObject, pickInfo));
            }
        }));
    }
    override flyTo(duration: number | undefined, id: number): boolean {
        const { sceneObject, czmViewer, czmDistanceMeasurement } = this;
        if (!czmViewer.actived) return false;
        if (sceneObject.flyToParam || sceneObject.flyInParam) {
            return super.flyTo(duration, id);
        } else {
            if (czmDistanceMeasurement.positions) {
                flyWithPositions(czmViewer, sceneObject, id, czmDistanceMeasurement.positions, duration);
                return true;
            }
            return false;
        }
    }
    override flyIn(duration: number | undefined, id: number): boolean {
        const { sceneObject, czmViewer, czmDistanceMeasurement } = this;
        if (!czmViewer.actived) return false;
        if (sceneObject.flyInParam) {
            return super.flyIn(duration, id);
        } else {
            if (czmDistanceMeasurement.positions) {
                flyWithPositions(czmViewer, sceneObject, id, czmDistanceMeasurement.positions, duration);
                return true;
            }
            return false;
        }
    }
}
