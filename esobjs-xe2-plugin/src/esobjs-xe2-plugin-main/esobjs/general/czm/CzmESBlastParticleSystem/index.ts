
import { CzmESObjectWithLocation, bindNorthRotation } from "@/esobjs-xe2-plugin-main/esobjs/base";
import { bind, track } from "xbsj-xe2/dist-node/xe2-base-utils";
import { CzmBlastParticleSystem, CzmViewer } from 'xbsj-xe2/dist-node/xe2-cesium-objects';
import { ESBlastParticleSystem } from '../../objs';
import { defaultFlyToRotation } from "../base";

export class CzmESBlastParticleSystem extends CzmESObjectWithLocation<ESBlastParticleSystem> {
    static readonly type = this.register(ESBlastParticleSystem.type, this);
    private _czmBlastParticeSystem = this.disposeVar(new CzmBlastParticleSystem());
    get czmBlastParticeSystem() { return this._czmBlastParticeSystem; }

    constructor(sceneObject: ESBlastParticleSystem, czmViewer: CzmViewer) {
        super(sceneObject, czmViewer);
        const viewer = czmViewer.viewer;
        if (!viewer) {
            console.warn(`viewer is undefined!`);
            return;
        }

        const { czmBlastParticeSystem } = this;
        czmViewer.add(czmBlastParticeSystem);
        this.dispose(() => czmViewer.delete(czmBlastParticeSystem))
        this.dispose(track([czmBlastParticeSystem, 'show'], [sceneObject, 'show']));
        this.dispose(bind([czmBlastParticeSystem, 'position'], [sceneObject, 'position']));
        this.dispose(track([czmBlastParticeSystem, 'translation'], [sceneObject, 'translation']));
        this.dispose(bindNorthRotation([czmBlastParticeSystem, 'rotation'], [sceneObject, 'rotation']));
        this.dispose(track([czmBlastParticeSystem, 'image'], [sceneObject, 'image']));
    }
    override flyTo(duration: number | undefined, id: number): boolean {
        const { sceneObject, czmViewer, czmBlastParticeSystem } = this;
        if (!czmViewer.actived) return false;
        if (sceneObject.flyToParam || sceneObject.flyInParam) {
            return super.flyTo(duration, id);
        } else {
            const position = czmBlastParticeSystem.position as [number, number, number];
            czmViewer.flyTo(position, 100, defaultFlyToRotation, duration && duration * 1000);
            sceneObject.flyOverEvent.emit(id, 'over', czmViewer);
            return true;
        }
    }
}
