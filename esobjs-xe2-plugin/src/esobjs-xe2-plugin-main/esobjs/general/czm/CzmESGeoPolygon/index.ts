import { CzmESGeoVector } from '@/esobjs-xe2-plugin-main/esobjs/base';
import { GeoCoplanarPolygon, SceneObjectPickedInfo, getMinMaxCorner } from 'xbsj-xe2/dist-node/xe2-base-objects';
import { bind, track } from 'xbsj-xe2/dist-node/xe2-base-utils';
import { CzmViewer, getViewerExtensions } from 'xbsj-xe2/dist-node/xe2-cesium-objects';
import { ESGeoPolygon } from '../../objs';
import { getCzmPickedInfoFromPickedInfo } from '../base/utils';
import { flyWithPositions, getPointerEventButton } from '../base';


export class CzmESGeoPolygon<T extends ESGeoPolygon = ESGeoPolygon> extends CzmESGeoVector<T> {
    static readonly type = this.register<ESGeoPolygon>(ESGeoPolygon.type, this);

    // private _sPositionsEditing = this.disposeVar(new PositionsEditing([this.sceneObject, 'points'], true, [this.sceneObject, 'editing'], this.czmViewer));
    // get sPositionsEditing() { return this._sPositionsEditing; }

    private _geoPolygon = this.disposeVar(new GeoCoplanarPolygon());
    get geoPolygon() { return this._geoPolygon; }

    constructor(sceneObject: T, czmViewer: CzmViewer) {
        super(sceneObject, czmViewer);
        const viewer = czmViewer.viewer;
        if (!viewer) {
            console.warn(`viewer is undefined!`);
            return;
        }

        const extensions = getViewerExtensions(czmViewer.viewer);
        if (!extensions) {
            return;
        }

        const geoPolygon = this._geoPolygon;
        czmViewer.add(geoPolygon);
        this.dispose(() => czmViewer.delete(geoPolygon))

        this.dispose(track([geoPolygon, 'show'], [sceneObject, 'show']));
        this.dispose(track([geoPolygon, 'allowPicking'], [sceneObject, 'allowPicking']));
        this.dispose(bind([geoPolygon, 'editing'], [sceneObject, 'editing']));
        this.dispose(bind([geoPolygon, 'positions'], [sceneObject, 'points']));

        this.dispose(track([geoPolygon, 'strokeGround'], [sceneObject, 'strokeGround']));
        this.dispose(track([geoPolygon, 'ground'], [sceneObject, 'fillGround']));

        this.dispose(track([geoPolygon, 'outline'], [sceneObject, 'stroked']));
        this.dispose(track([geoPolygon, 'outlineColor'], [sceneObject, 'strokeColor']));
        this.dispose(track([geoPolygon, 'outlineWidth'], [sceneObject, 'strokeWidth']));

        this.dispose(track([geoPolygon, 'fill'], [sceneObject, 'filled']));
        this.dispose(track([geoPolygon, 'color'], [sceneObject, 'fillColor']));

        // const entity = viewer.entities.add({ polygon: {} });
        // this.dispose(() => viewer.entities.remove(entity));
        // const boundingSphere = new Cesium.BoundingSphere();
        // {
        //     const updateProp = () => { entity.show = sceneObject.show ?? true; }
        //     updateProp();
        //     this.dispose(sceneObject.showChanged.disposableOn(updateProp));
        // }

        // {
        //     const updateProp = () => {
        //         // @ts-ignore
        //         entity.polygon.height = sceneObject.ground ? undefined : 0.0;
        //     }
        //     updateProp();
        //     this.dispose(sceneObject.groundChanged.disposableOn(updateProp));
        // }

        // {
        //     const updateProp = () => {
        //         // @ts-ignore
        //         entity.polygon.outline = sceneObject.stroked;
        //         // @ts-ignore
        //         entity.polygon.outlineWidth = sceneObject.stroked && sceneObject.strokeWidth
        //         // @ts-ignore
        //         entity.polygon.outlineColor = sceneObject.stroked && toColor(sceneObject.strokeColor);
        //     }
        //     updateProp();
        //     const updateEvent = this.disposeVar(createNextAnimateFrameEvent(
        //         sceneObject.strokeStyleChanged,
        //         sceneObject.strokedChanged,
        //     ));
        //     this.dispose(updateEvent.disposableOn(updateProp));
        // }


        // {
        //     const updateProp = () => {
        //         // @ts-ignore
        //         entity.polygon.fill = sceneObject.filled;
        //         // @ts-ignore
        //         entity.polygon.material = sceneObject.filled && toColor(sceneObject.fillColor);
        //     }
        //     updateProp();
        //     const updateEvent = this.disposeVar(createNextAnimateFrameEvent(
        //         sceneObject.fillStyleChanged,
        //         sceneObject.filledChanged,
        //     ));
        //     this.dispose(updateEvent.disposableOn(updateProp));
        // }

        // {
        //     const updateProp = () => {
        //         if (!sceneObject.points) {
        //             return;
        //         }
        //         const cartesians = positionsToUniqueCartesians(sceneObject.points);
        //         if (cartesians.length < 2) {
        //             boundingSphere.radius = -1;
        //             return;
        //         }
        //         Cesium.BoundingSphere.fromPoints(cartesians, boundingSphere);
        //         const hierarchy = new Cesium.PolygonHierarchy(cartesians)

        //         // @ts-ignore
        //         entity.polygon.hierarchy = hierarchy;
        //         // entity.polygon.hierarchy =  new Cesium.CallbackProperty(()=>{
        //         //     return hierarchy;
        //         // }, false); //使用回调函数,防止闪烁。warning：用高度值undefind设置贴地时，回调函数会导致内存不释放，polygon叠加
        //     }
        //     updateProp();
        //     this.dispose(sceneObject.pointsChanged.disposableOn(updateProp));
        // }

        {//事件
            this.dispose(geoPolygon.pickedEvent.disposableOn(pickedInfo => {
                if (getPointerEventButton(pickedInfo) === 0 && (sceneObject.allowPicking ?? false)) {
                    const pickInfo = getCzmPickedInfoFromPickedInfo(pickedInfo)
                    sceneObject.pickedEvent.emit(new SceneObjectPickedInfo(sceneObject, pickInfo));
                }
            }));

            // this.dispose(czmViewer.czmPickedEvent.disposableOn(pickedInfo => {
            //     if (!pickedInfo) return; const pickedResult = pickedInfo.czmPickResult;
            //     if (pickedResult && (sceneObject.allowPicking ?? false) && pickedResult.id === entity) {
            //         sceneObject.pickedEvent.emit(new SceneObjectPickedInfo(sceneObject, pickedInfo));
            //     }
            // }));
        }
    }
    override flyTo(duration: number | undefined, id: number): boolean {
        const { sceneObject, czmViewer, geoPolygon } = this;
        if (!czmViewer.actived) return false;
        if (sceneObject.flyToParam || sceneObject.flyInParam) {
            return super.flyTo(duration, id);
        } else {
            if (geoPolygon.positions) {
                flyWithPositions(czmViewer, sceneObject, id, geoPolygon.positions, duration);
                return true;
            }
            return false;
        }
    }
    override flyIn(duration: number | undefined, id: number): boolean {
        const { sceneObject, czmViewer, geoPolygon } = this;
        if (!czmViewer.actived) return false;
        if (sceneObject.flyInParam) {
            return super.flyIn(duration, id);
        } else {
            if (geoPolygon.positions) {
                flyWithPositions(czmViewer, sceneObject, id, geoPolygon.positions, duration);
                return true;
            }
            return false;
        }
    }
}
