import { CzmObject, CzmViewer } from "xbsj-xe2/dist-node/xe2-cesium-objects";
import { ESHumanPoi } from "../../objs";
import { bind, createNextAnimateFrameEvent, track } from "xbsj-renderer/dist-node/xr-base-utils";
export class CzmESHumanPoi extends CzmObject<ESHumanPoi> {
    static readonly type = this.register(ESHumanPoi.type, this);
    constructor(sceneObject: ESHumanPoi, czmViewer: CzmViewer) {
        super(sceneObject, czmViewer);
        const viewer = czmViewer.viewer;
        if (!viewer) {
            console.warn(`viewer is undefined!`);
            return;
        }

        const { poi } = sceneObject;
        this.dispose(track([poi, 'rotation'], [sceneObject, 'rotation']));

        const updatePos = () => {
            const pos = sceneObject.position;
            poi.position = [pos[0], pos[1], pos[2] + sceneObject.poiOffsetHeight];
        }
        updatePos();
        const posEvent = this.dv(createNextAnimateFrameEvent(sceneObject.positionChanged, sceneObject.poiOffsetHeightChanged))
        this.d(posEvent.don(updatePos));

    }
}
