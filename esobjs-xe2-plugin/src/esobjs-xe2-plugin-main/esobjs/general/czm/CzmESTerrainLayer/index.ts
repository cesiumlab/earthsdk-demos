import { CzmESVisualObject, ESJResource } from '@/esobjs-xe2-plugin-main/esobjs/base';
import { CzmTerrainProviderJsonType, ESSceneObject } from 'xbsj-xe2/dist-node/xe2-base-objects';
import { reactJson, track } from "xbsj-xe2/dist-node/xe2-base-utils";
import { CzmTerrain, CzmViewer } from 'xbsj-xe2/dist-node/xe2-cesium-objects';
import { ESTerrainLayer } from '../../objs';

function getParamsFromUrl(url: string) {
    const index = url.indexOf("?");
    const b = url.slice(index + 1);
    const c = b.split("&");
    const data: { [k: string]: any } = {};
    for (var i = 0; i < c.length; i++) {
        var d = c[i].split("=");
        data[d[0]] = d[1];
    }
    return data;
}

export class CzmESTerrainLayer extends CzmESVisualObject<ESTerrainLayer> {
    static readonly type = this.register(ESTerrainLayer.type, this);
    private _czmTerrain = this.dv(new CzmTerrain());
    get czmTerrain() { return this._czmTerrain; }

    constructor(sceneObject: ESTerrainLayer, czmViewer: CzmViewer) {
        super(sceneObject, czmViewer);
        const viewer = czmViewer.viewer;
        if (!viewer) {
            console.warn(`viewer is undefined!`);
            return;
        }

        const czmTerrain = this._czmTerrain;
        czmViewer.add(czmTerrain);
        this.d(() => czmViewer.delete(czmTerrain));

        {
            const ellipsoid = {
                x: 6378137,
                y: 6378137,
                z: 6356752.314245179
            }

            const urlReact = this.ad(reactJson<string | ESJResource>(""))
            {
                const update = () => {
                    if (typeof sceneObject.url === 'string') {
                        urlReact.value = ESSceneObject.context.getStrFromEnv(sceneObject.url ?? ESTerrainLayer.defaults.url);
                    } else {
                        const temp = sceneObject.url;
                        temp.url = ESSceneObject.context.getStrFromEnv(temp.url ?? ESTerrainLayer.defaults.url);
                        urlReact.value = temp;
                    }
                }
                update();
                this.ad(sceneObject.urlChanged.don(update))
            }
            const update = () => {
                if (!urlReact.value) return;
                let url = urlReact.value;
                let urlURL = typeof url === 'string' ? url : url.url;
                //判断是否为平球
                if (urlURL.includes(ESTerrainLayer.defaults.url)) {
                    const params = getParamsFromUrl(urlURL)
                    //判断是否为有高程参数
                    if (params && params.h) {
                        const h = Number(params.h)
                        const provider = {
                            "type": "EllipsoidTerrainProvider",
                            "ellipsoid": [ellipsoid.x + h, ellipsoid.y + h, ellipsoid.z + h]
                        } as CzmTerrainProviderJsonType
                        czmTerrain.provider = provider;
                    } else {
                        const provider = {
                            "type": "CesiumTerrainProvider",
                            "url": '',
                            "requestMetadata": true,
                            "requestWaterMask": true,
                            "requestVertexNormals": true
                        } as CzmTerrainProviderJsonType
                        czmTerrain.provider = provider;
                    }
                } else {
                    if (urlURL.includes('/layer.json')) {
                        // url = urlReact.value.substring(0, urlReact.value.indexOf('/layer.json'))
                        if (typeof url === 'string') {
                            url = urlURL.split("/layer.json").join("")
                        } else {
                            url.url = urlURL.split("/layer.json").join("")
                        };
                    }
                    const provider = {
                        "type": "CesiumTerrainProvider",
                        url,
                        "requestMetadata": true,
                        "requestWaterMask": true,
                        "requestVertexNormals": true
                    } as CzmTerrainProviderJsonType
                    czmTerrain.provider = provider;
                }
            };
            update();
            this.d(urlReact.changed.don(update));
        }
        console.log('czmTerrain', czmTerrain)

        this.d(track([czmTerrain, 'show'], [sceneObject, 'show']));
        this.d(track([czmTerrain, 'rectangle'], [sceneObject, 'rectangle']));
        this.d(track([czmTerrain, 'zIndex'], [sceneObject, 'zIndex']));
    }

    //czmTerrain.flyTo 的duration是特殊的，单位是秒
    override flyTo(duration: number | undefined, id: number): boolean {
        const { sceneObject, czmViewer, czmTerrain } = this;
        if (!czmViewer.actived) return false;
        if (sceneObject.flyToParam || sceneObject.flyInParam) {
            return super.flyTo(duration, id);
        } else {
            czmTerrain.flyTo(duration);
            sceneObject.flyOverEvent.emit(id, 'over', czmViewer);
            return true;
        }
    }
    override flyIn(duration: number | undefined, id: number): boolean {
        const { sceneObject, czmViewer, czmTerrain } = this;
        if (!czmViewer.actived) return false;
        if (sceneObject.flyInParam) {
            return super.flyIn(duration, id);
        } else {
            czmTerrain.flyTo(duration);
            sceneObject.flyOverEvent.emit(id, 'over', czmViewer);
            return true;
        }
    }
}

