import { CzmImagery, CzmViewer } from 'xbsj-xe2/dist-node/xe2-cesium-objects';
import { CzmESVisualObject } from '@/esobjs-xe2-plugin-main/esobjs/base';
import { ESMVTLayer } from '../../objs';
import { createNextAnimateFrameEvent, track } from 'xbsj-renderer/dist-node/xr-base-utils';
import { getCzmPickedInfoFromPickedInfo, getPointerEventButton } from '../base';
import { SceneObjectPickedInfo } from 'xbsj-xe2/dist-node/xe2-base-objects';

export class CzmESMVTLayer extends CzmESVisualObject<ESMVTLayer> {
    static readonly type = this.register(ESMVTLayer.type, this);

    private _czmImagery = this.dv(new CzmImagery());
    get czmImagery() { return this._czmImagery; }

    constructor(sceneObject: ESMVTLayer, czmViewer: CzmViewer) {
        super(sceneObject, czmViewer);
        const viewer = czmViewer.viewer;
        if (!viewer) {
            console.warn(`viewer is undefined!`);
            return;
        }

        const czmImagery = this._czmImagery;
        czmViewer.add(czmImagery);
        this.d(() => czmViewer.delete(czmImagery));

        this.d(track([czmImagery, 'show'], [sceneObject, 'show']));
        this.d(track([czmImagery, 'zIndex'], [sceneObject, 'zIndex']))
        this.d(track([czmImagery, 'rectangle'], [sceneObject, 'rectangle']));

        this.dispose(czmImagery.pickedEvent.disposableOn(pickedInfo => {
            if (getPointerEventButton(pickedInfo) === 0 && (sceneObject.allowPicking ?? false)) {
                const pickInfo = getCzmPickedInfoFromPickedInfo(pickedInfo)
                sceneObject.pickedEvent.emit(new SceneObjectPickedInfo(sceneObject, pickInfo));
            }
        }));

        {
            const event = this.dv(createNextAnimateFrameEvent(
                sceneObject.urlChanged,
                sceneObject.accessTokenChanged,
                sceneObject.maximumLevelChanged,
                sceneObject.minimumLevelChanged,
                sceneObject.tileSizeChanged,
                sceneObject.allowPickingChanged,
                sceneObject.rectangleChanged,
                sceneObject.styleChanged,
            ));
            const update = () => {
                czmImagery.imageryProvider = {
                    //@ts-ignore
                    type: "MVTImageryProvider",
                    url: sceneObject.url,
                    accessToken: sceneObject.accessToken,
                    maximumLevel: sceneObject.maximumLevel,
                    minimumLevel: sceneObject.minimumLevel,
                    tileSize: sceneObject.tileSize,
                    enablePickFeatures: sceneObject.allowPicking,
                    rectangle: sceneObject.rectangle,
                    style: sceneObject.style,
                };
            }
            update();
            this.d(event.don(update));
        }
    }

    override flyTo(duration: number | undefined, id: number): boolean {
        const { sceneObject, czmViewer, czmImagery } = this;
        if (!czmViewer.actived) return false;
        if (sceneObject.flyToParam || sceneObject.flyInParam) {
            return super.flyTo(duration, id);
        } else {
            czmImagery.flyTo(duration && duration);
            sceneObject.flyOverEvent.emit(id, 'over', czmViewer);
            return true;
        }
    }
    override flyIn(duration: number | undefined, id: number): boolean {
        const { sceneObject, czmViewer, czmImagery } = this;
        if (!czmViewer.actived) return false;
        if (sceneObject.flyInParam) {
            return super.flyIn(duration, id);
        } else {
            czmImagery.flyTo(duration && duration);
            sceneObject.flyOverEvent.emit(id, 'over', czmViewer);
            return true;
        }
    }
}