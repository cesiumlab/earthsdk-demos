
import { CzmESObjectWithLocation, bindNorthRotation } from "@/esobjs-xe2-plugin-main/esobjs/base";
import { SceneObjectPickedInfo } from "xbsj-xe2/dist-node/xe2-base-objects";
import { bind, track } from "xbsj-xe2/dist-node/xe2-base-utils";
import { CzmCustomPrimitive, CzmViewer } from 'xbsj-xe2/dist-node/xe2-cesium-objects';
import { ESApertureEffect } from "../../objs";
import { getCzmPickedInfoFromPickedInfo } from "../base/utils";
import { defaultFlyToRotation, flyWithPrimitive, getPointerEventButton } from "../base";
export class CzmESApertureEffect extends CzmESObjectWithLocation<ESApertureEffect> {
    static readonly type = this.register(ESApertureEffect.type, this);
    private _czmCustomPrimitive = this.disposeVar(new CzmCustomPrimitive());
    get czmCustomPrimitive() { return this._czmCustomPrimitive };

    constructor(sceneObject: ESApertureEffect, czmViewer: CzmViewer) {
        super(sceneObject, czmViewer);
        const viewer = czmViewer.viewer;
        if (!viewer) {
            console.warn(`viewer is undefined!`);
            return;
        }
        const { czmCustomPrimitive } = this;
        czmViewer.add(czmCustomPrimitive);
        this.dispose(() => czmViewer.delete(czmCustomPrimitive))
        this.dispose(track([czmCustomPrimitive, 'show'], [sceneObject, 'show']));
        this.dispose(track([czmCustomPrimitive, 'allowPicking'], [sceneObject, 'allowPicking']));
        this.dispose(bindNorthRotation([czmCustomPrimitive, 'rotation'], [sceneObject, 'rotation']));
        this.dispose(bind([czmCustomPrimitive, 'position'], [sceneObject, 'position']));
        this.dispose(track([czmCustomPrimitive, 'scale'], [sceneObject, 'scale']));


        czmCustomPrimitive.indexTypedArray = new Uint16Array([0, 1, 2, 0, 2, 3]);
        czmCustomPrimitive.uniformMap = {
            u_color: [0, 0.7, 1, 1],
        };
        czmCustomPrimitive.fragmentShaderSource = `
                in vec2 v_st;
                uniform vec4 u_color;
                void main()
                {
                    float d = length(v_st);
                    float movingCircleD = fract(czm_frameNumber / 130.0);
                    float alpha = step(.3, d) * step(d, .35) + step(.98, d) * step(d, 1.) + d * d * step(d, 1.);
                    alpha += step(movingCircleD, d) * step(d, movingCircleD + .015);
                    out_FragColor = vec4(u_color.rgb, alpha);
                }
            `;


        const update = () => {
            const width = sceneObject.radius * 2
            const height = sceneObject.radius * 2

            czmCustomPrimitive.boundingVolume = {
                type: 'LocalAxisedBoundingBox',
                data: {
                    min: [-width * .5, -height * .5, 0],
                    max: [width * .5, height * .5, 0],
                }
            };

            czmCustomPrimitive.attributes = {
                position: {
                    componentsPerAttribute: 3,
                    typedArray: new Float32Array([
                        -0.5 * width, -0.5 * height, 0,
                        0.5 * width, -0.5 * height, 0,
                        0.5 * width, 0.5 * height, 0,
                        -0.5 * width, 0.5 * height, 0,
                    ]),
                },
                normal: {
                    componentsPerAttribute: 3,
                    typedArray: new Float32Array([
                        0, 0, 1,
                        0, 0, 1,
                        0, 0, 1,
                        0, 0, 1,
                    ]),
                },
                textureCoordinates: {
                    componentsPerAttribute: 2,
                    typedArray: new Float32Array([
                        -1, -1,
                        1, -1,
                        1, 1,
                        -1, 1,
                    ]),
                },
            };
        }
        update()
        this.dispose(sceneObject.radiusChanged.don(() => update()));
        this.dispose(czmCustomPrimitive.pickedEvent.don(pickedInfo => {
            if (getPointerEventButton(pickedInfo) === 0 && (sceneObject.allowPicking ?? false)) {
                const pickInfo = getCzmPickedInfoFromPickedInfo(pickedInfo)
                sceneObject.pickedEvent.emit(new SceneObjectPickedInfo(sceneObject, pickInfo));
            }
        }));
    }
    override flyTo(duration: number | undefined, id: number): boolean {
        const { sceneObject, czmViewer, czmCustomPrimitive } = this;
        if (!czmViewer.actived || !czmViewer.viewer) return false;
        if (sceneObject.flyToParam || sceneObject.flyInParam) {
            return super.flyTo(duration, id);
        } else {
            flyWithPrimitive(czmViewer, sceneObject, id, duration, czmCustomPrimitive, true);
            return true;
        }
    }
}
