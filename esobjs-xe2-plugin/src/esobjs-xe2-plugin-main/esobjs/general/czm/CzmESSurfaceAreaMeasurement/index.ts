import { getMinMaxCorner } from 'xbsj-xe2/dist-node/xe2-base-objects';
import { CzmESGeoPolygon } from '../CzmESGeoPolygon';
import { SceneObjectPickedInfo } from 'xbsj-xe2/dist-node/xe2-base-objects';
import { bind, track, createNextAnimateFrameEvent } from "xbsj-xe2/dist-node/xe2-base-utils";
import { CzmSpaceAreaMeasurement, CzmViewer } from 'xbsj-xe2/dist-node/xe2-cesium-objects';
import { ESSurfaceAreaMeasurement } from '../../objs';
import { getCzmPickedInfoFromPickedInfo } from '../base/utils';
import { flyWithPositions, getPointerEventButton } from '../base';

export class CzmESSurfaceAreaMeasurement extends CzmESGeoPolygon<ESSurfaceAreaMeasurement> {
    static override readonly type = this.register(ESSurfaceAreaMeasurement.type, this);
    private _czmAreaMeasurement = this.disposeVar(new CzmSpaceAreaMeasurement());
    get czmAreaMeasurement() { return this._czmAreaMeasurement; }

    constructor(sceneObject: ESSurfaceAreaMeasurement, czmViewer: CzmViewer) {
        super(sceneObject, czmViewer);
        const viewer = czmViewer.viewer;
        if (!viewer) {
            console.warn(`viewer is undefined!`);
            return;
        }

        const czmAreaMeasurement = this._czmAreaMeasurement;
        czmViewer.add(czmAreaMeasurement);
        this.dispose(() => czmViewer.delete(czmAreaMeasurement))

        this.dispose(track([czmAreaMeasurement, 'show'], [sceneObject, 'show']));
        this.dispose(bind([czmAreaMeasurement, 'positions'], [sceneObject, 'points']));
        this.dispose(bind([czmAreaMeasurement, 'editing'], [sceneObject, 'editing']));
        this.dispose(track([czmAreaMeasurement, 'interpolationDistance'], [sceneObject, 'interpolation']));
        this.dispose(track([czmAreaMeasurement, 'offsetHeight'], [sceneObject, 'offsetHeight']));
        {
            const updateProp = () => {
                this.geoPolygon.outline = false;
                const stroked = sceneObject.stroked
                if (!stroked) {
                    czmAreaMeasurement.outlineWidth = 0;
                    return
                } else {
                    czmAreaMeasurement.outlineWidth = sceneObject.strokeWidth;
                }

                czmAreaMeasurement.outlineWidth = sceneObject.strokeWidth;
                czmAreaMeasurement.outlineColor = sceneObject.strokeColor;
            }
            updateProp();
            const updateEvent = this.disposeVar(createNextAnimateFrameEvent(
                sceneObject.strokeStyleChanged,
                sceneObject.strokedChanged,
            ));
            this.dispose(updateEvent.disposableOn(updateProp));
        }
        this.dispose(czmAreaMeasurement.pickedEvent.disposableOn(pickedInfo => {
            if (getPointerEventButton(pickedInfo) === 0 && (sceneObject.allowPicking ?? false)) {
                const pickInfo = getCzmPickedInfoFromPickedInfo(pickedInfo)
                sceneObject.pickedEvent.emit(new SceneObjectPickedInfo(sceneObject, pickInfo));
            }
        }));
        this.dispose(sceneObject.startEvent.don(() => { this._czmAreaMeasurement.start() }))
    }
    override flyTo(duration: number | undefined, id: number): boolean {
        const { sceneObject, czmViewer, czmAreaMeasurement } = this;
        if (!czmViewer.actived) return false;
        if (sceneObject.flyToParam || sceneObject.flyInParam) {
            return super.flyTo(duration, id);
        } else {
            if (czmAreaMeasurement.positions) {
                flyWithPositions(czmViewer, sceneObject, id, czmAreaMeasurement.positions, duration);
                return true;
            }
            return true;
        }
    }
    override flyIn(duration: number | undefined, id: number): boolean {
        const { sceneObject, czmViewer, czmAreaMeasurement } = this;
        if (!czmViewer.actived) return false;
        if (sceneObject.flyInParam) {
            return super.flyIn(duration, id);
        } else {
            if (czmAreaMeasurement.positions) {
                flyWithPositions(czmViewer, sceneObject, id, czmAreaMeasurement.positions, duration);
                return true;
            }
            return true;
        }
    }
}
