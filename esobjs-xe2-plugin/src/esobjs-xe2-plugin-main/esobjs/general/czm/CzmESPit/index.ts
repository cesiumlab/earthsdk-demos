import { CzmTexture, CzmViewer } from "xbsj-xe2/dist-node/xe2-cesium-objects";
import { ESCustomPrimitive, ESPit } from "../../objs";
import { CzmESGeoPolygon } from "../CzmESGeoPolygon";
import { Destroyable, createNextAnimateFrameEvent, reactArray, track } from "xbsj-renderer/dist-node/xr-base-utils";
import { ESSceneObject, ObjResettingWithEvent, SceneObjectPickedInfo, getGeoBoundingSphereFromPositions, getMinMaxCorner, lbhToXyz, positionsToLocalPositions } from "xbsj-xe2/dist-node/xe2-base-objects";
import * as Cesium from 'cesium'
import { getCzmPickedInfoFromPickedInfo } from "../base/utils";
import { flyWithPositions, getPointerEventButton } from "../base";
const earcut = require('earcut')
type ESPointLLH = [number, number, number];

export class CzmESPit extends CzmESGeoPolygon<ESPit> {
    static override readonly type = this.register(ESPit.type, this);

    // 操作说明：1 输入变量名 2 输入类型信息 3 输入默认值  tab进入下一个输入项
    // import { react } from "xbsj-xe2/dist-node/xe2-base-utils";
    private _judgePoints = this.disposeVar(reactArray<ESPointLLH[]>([]));
    get judgePoints() { return this._judgePoints.value; }
    set judgePoints(value: ESPointLLH[]) { this._judgePoints.value = value; }
    get judgePointsChanged() { return this._judgePoints.changed; }

    constructor(sceneObject: ESPit, czmViewer: CzmViewer) {
        super(sceneObject, czmViewer);
        const viewer = czmViewer.viewer;
        if (!viewer) {
            console.log('viewer is undefined!');
            return;
        }
        {
            // 覆盖父级绑定
            const update = () => {
                this.geoPolygon.fill = false;
            }
            update()
            this.d(sceneObject.filledChanged.don(update))
        }
        // 点变化、深度变化、图像变化都重绘
        const event = this.dv(createNextAnimateFrameEvent(
            sceneObject.depthChanged,
            sceneObject.sideImageChanged,
            sceneObject.bottomImageChanged,
            sceneObject.interpolationChanged,
            this.judgePointsChanged,
        ))
        this.dv(new ObjResettingWithEvent(event, () => {
            if (sceneObject.points && sceneObject.points.length >= 3) {
                return new PitComponents(sceneObject, czmViewer);
            }
            return undefined;
        }))
        {
            let debounce = (fn: Function, delay: number) => {
                var timer: NodeJS.Timeout; // 维护一个 timer
                var _this = this; // 取debounce执行作用域的this
                return function () {
                    var args = arguments;
                    if (timer) {
                        clearTimeout(timer);
                    }
                    timer = setTimeout(function () {
                        fn.apply(_this, args); // 用apply指向调用debounce的对象，相当于_this.fn(args);
                    }, delay);
                };
            }
            let update = debounce(() => {
                if (sceneObject.points) {
                    this.judgePoints = sceneObject.points;
                }
            }, 200);
            // 通过点判断是否为编辑状态
            sceneObject.pointsChanged.don(() => {
                update()
            })
        }
        {
            sceneObject.editingChanged.don((newVal) => {
                sceneObject.stroked = newVal;
            })
        }
    }
    override flyTo(duration: number | undefined, id: number): boolean {
        const { sceneObject, czmViewer, } = this;
        if (!czmViewer.actived) return false;
        if (sceneObject.flyToParam || sceneObject.flyInParam) {
            return super.flyTo(duration, id);
        } else {
            if (sceneObject.points) {
                flyWithPositions(czmViewer, sceneObject, id, sceneObject.points, duration)
                return true;
            }
            return false;
        }
    }
}
// 计算需要创建的自定义图元
class PitComponents extends Destroyable {

    constructor(sceneObject: ESPit, czmViewer: CzmViewer) {
        super();
        // 获取中心点和最低点用于计算最低点高程
        const { minPos, center } = getMinMaxCorner(sceneObject.points as ESPointLLH[]);
        const bottomDepth = minPos[2] - sceneObject.depth;
        // 画底面
        if (sceneObject.points) {
            const localPoints = positionsToLocalPositions({ originPosition: center }, sceneObject.points.map((item, index) => {
                let TempArr = [...item] as [number, number, number];
                TempArr[2] = bottomDepth
                return TempArr
            }));
            this.dv(new PitComponent(sceneObject, czmViewer, center, localPoints[0], "bottom", true));
            // 画侧面
            // 插值
            const interpolationPositions = this._interpolationAlongPolygon(sceneObject.points.map((item, index) => {
                return Cesium.Cartesian3.fromArray(lbhToXyz(item));
            }), sceneObject.interpolation);
            // 获取高度
            this._getPositionByLonLat(czmViewer, interpolationPositions).then(res => {
                let TempPoint = [] as ESPointLLH[];
                for (let i = 0; i < res.length; i++) {
                    const element = res[i];
                    TempPoint.push(element, [element[0], element[1], bottomDepth]);
                }
                const localPoints = positionsToLocalPositions({ originPosition: center }, TempPoint);
                // 判断是顺时针还是逆时针
                let isClockwise = this._isClockwise(sceneObject.points as [number, number, number][], czmViewer);
                this.dv(new PitComponent(sceneObject, czmViewer, center, localPoints[0], "side", isClockwise));
            })

        }
    }
    private _interpolationAlongPolygon(positions: Cesium.Cartesian3[], interpolationDistance: number) {
        var result = [];
        for (let i = 0; i < positions.length; ++i) {
            var a = positions[i];
            var b = positions[(i + 1) % positions.length];
            var points = this._interpolationAlongLine(a, b, interpolationDistance);
            result.push(...points);
        }
        result.push(positions[0]);
        return result;
    }
    // 插值点
    private _interpolationAlongLine(p1: Cesium.Cartesian3, p2: Cesium.Cartesian3, interpolationDistance: number) {
        var result = [];
        var totalLength = Cesium.Cartesian3.distance(p1, p2);
        var v = Cesium.Cartesian3.subtract(p2, p1, new Cesium.Cartesian3());
        v = Cesium.Cartesian3.normalize(v, v);
        var length = 0;
        while (length < totalLength) {
            var project = new Cesium.Cartesian3(v.x * length, v.y * length, v.z * length);
            var interpolation = Cesium.Cartesian3.add(p1, project, new Cesium.Cartesian3());
            result.push(interpolation);
            length += interpolationDistance;
        }
        return result;
    }
    private _getPositionByLonLat(czmViewer: CzmViewer, posArr: Cesium.Cartesian3[]) {
        return new Promise<ESPointLLH[]>(function (resolve) {
            let lonLat: Cesium.Cartographic[] = []
            posArr.forEach(item => {
                lonLat.push(Cesium.Cartographic.fromCartesian(item));
            })
            let finalArr: ESPointLLH[] = []
            //模型上取点
            const viewer = czmViewer.viewer as Cesium.Viewer
            const promiseModel = viewer.scene.sampleHeightMostDetailed(lonLat);//这里可以传多个点
            //地形上取点
            const promiseTerrain = Cesium.sampleTerrainMostDetailed(viewer.terrainProvider, lonLat);
            Promise.allSettled([promiseModel, promiseTerrain]).then(res => {
                if (res[0].status == 'fulfilled') {
                    res[0].value.forEach((obj, index) => {
                        if (obj) {
                            finalArr[index] = [
                                Cesium.Math.toDegrees(lonLat[index].longitude),
                                Cesium.Math.toDegrees(lonLat[index].latitude),
                                obj.height > 0 ? obj.height : 0];
                        }
                    })
                }
                if (res[1].status == 'fulfilled') {
                    res[1].value.forEach((obj, index) => {
                        if (finalArr[index] && finalArr[index][2]) {
                            //这里就是判断是不是已经在模型上取点
                        } else {
                            finalArr[index] = [
                                Cesium.Math.toDegrees(lonLat[index].longitude),
                                Cesium.Math.toDegrees(lonLat[index].latitude),
                                obj.height > 0 ? obj.height : 0];
                        }
                    })
                }
                resolve(finalArr);
            })
        })
    }
    private _isClockwise(points: [number, number, number][], czmViewer: CzmViewer) {
        // 判断是相对顺时针还是逆时针，根据相机视角作为辅助向量进行计算
        if (!czmViewer.viewer) {
            return;
        }
        for (let i = 0; i < points.length; i++) {
            let one = Cesium.Cartesian3.fromDegrees(...points[i]);
            let two = Cesium.Cartesian3.fromDegrees(...points[(i + 1) % points.length]);
            let three = Cesium.Cartesian3.fromDegrees(...points[(i + 2) % points.length]);
            let start = Cesium.Cartesian3.subtract(two, one, new Cesium.Cartesian3());
            let end = Cesium.Cartesian3.subtract(three, two, new Cesium.Cartesian3());
            Cesium.Cartesian3.normalize(start, start);
            Cesium.Cartesian3.normalize(end, end);
            if (Math.abs(Cesium.Cartesian3.dot(start, end)) == 1) {
                if (i == points.length - 1) {
                    return true;
                } else {
                    continue;
                }
            }
            let normalVector = Cesium.Cartesian3.cross(start, end, new Cesium.Cartesian3());
            let isClockwise = Cesium.Cartesian3.dot(normalVector, czmViewer.viewer.camera.directionWC);
            return isClockwise > 0;
        }
    }
}
class PitComponent extends Destroyable {
    // 自定义图元
    private _esCustomPrimitive = this.dv(new ESCustomPrimitive());
    get esCustomPrimitive() { return this._esCustomPrimitive; }
    // 自定义纹理
    private _czmTexture = this.dv(ESSceneObject.createFromClass(CzmTexture));
    get czmTexture() { return this._czmTexture; }

    private _width: number = 0;
    private _height: number = 0;

    constructor(sceneObject: ESPit, czmViewer: CzmViewer, private _center: [number, number, number], localPoints: ESPointLLH[], type: string, isClockwise?: boolean) {
        super();
        // 自定义图元
        const esCustomPrimitive = this._esCustomPrimitive;
        czmViewer.add(esCustomPrimitive);
        this.dispose(() => czmViewer.delete(esCustomPrimitive));
        // 纹理
        const czmTexture = this._czmTexture;
        czmViewer.add(czmTexture);
        this.dispose(() => czmViewer.delete(czmTexture));
        // 绑定监听
        // this.dispose(track([esCustomPrimitive, 'show'], [sceneObject, 'show']));
        {
            const update = () => {
                esCustomPrimitive.show = sceneObject.show && sceneObject.filled;
            }
            update()
            const event = this.dv(createNextAnimateFrameEvent(sceneObject.showChanged, sceneObject.filledChanged));
            this.dispose(event.don(update));
        }
        this.dispose(track([esCustomPrimitive, 'allowPicking'], [sceneObject, 'allowPicking']));
        this.d(esCustomPrimitive.pickedEvent.don((pickedInfo) => {
            if (getPointerEventButton(pickedInfo) === 0 && (sceneObject.allowPicking ?? false)) {
                const pickInfo = getCzmPickedInfoFromPickedInfo(pickedInfo)
                sceneObject.pickedEvent.emit(new SceneObjectPickedInfo(sceneObject, pickInfo));
            }
        }));
        esCustomPrimitive.position = this._center;
        // 创建
        {
            const update = () => {
                // 展开数组
                const position = localPoints.flat();
                let indexes: Uint32Array = type == "bottom" ? earcut(position, null, 3) : this._triangleIndices(localPoints.length);
                // uv对应图片
                let uv = <number[]>[];
                let maxHeight = -Infinity, minHeight = Infinity, maxWidth = -Infinity, minWidth = Infinity, currentWidth = 0;
                if (type == "bottom") {
                    for (let i = 0; i < localPoints.length; i++) {
                        maxWidth = maxWidth > localPoints[i][0] ? maxWidth : localPoints[i][0];
                        minWidth = minWidth < localPoints[i][0] ? minWidth : localPoints[i][0];
                        maxHeight = maxHeight > localPoints[i][1] ? maxHeight : localPoints[i][1];
                        minHeight = minHeight < localPoints[i][1] ? minHeight : localPoints[i][1];
                    }
                    this._width = maxWidth - minWidth;
                    this._height = maxHeight - minHeight;
                    for (let i = 0; i < localPoints.length; i++) {
                        uv.push((localPoints[i][0] - this._width / 2) / this._width, (localPoints[i][1] - this._height / 2) / this._height)
                    }
                } else {
                    for (let i = 0; i < localPoints.length; i++) {
                        this._width += this._getDistance(localPoints[i], localPoints[i - 1 < 0 ? 0 : i - 1]);
                        maxHeight = maxHeight > localPoints[i][2] ? maxHeight : localPoints[i][2];
                        minHeight = minHeight < localPoints[i][2] ? minHeight : localPoints[i][2];
                    }
                    this._height = maxHeight - minHeight;
                    for (let i = 0; i < localPoints.length; i++) {
                        currentWidth += this._getDistance(localPoints[i], localPoints[i - 1 < 0 ? 0 : i - 1]);
                        uv.push(currentWidth / this._width, (localPoints[i][2] - minHeight) / this._height)
                    }
                }
                esCustomPrimitive.indexTypedArray = new Uint16Array(isClockwise ? indexes : indexes.reverse());
                esCustomPrimitive.attributes = {
                    position: {
                        typedArray: new Float32Array(position),
                        componentsPerAttribute: 3,
                    },
                    st: {
                        typedArray: new Float32Array(isClockwise ? uv : uv.reverse()),
                        componentsPerAttribute: 2
                    }
                };
                const uDis = type == "bottom" ? sceneObject.bottomImage.uDis ?? 50 : sceneObject.sideImage.uDis ?? 50;
                const vDis = type == "bottom" ? sceneObject.bottomImage.vDis ?? 50 : sceneObject.sideImage.vDis ?? 50;

                esCustomPrimitive.fragmentShaderSource = `in vec2 v_st;
                                                        uniform sampler2D u_image;
                                                        uniform vec4 u_color;
                                                        void main()
                                                        {
                                                            vec2 st = v_st;
                                                            st.s = fract(st.s * ${(this._width / uDis).toFixed(1)});
                                                            st.t = fract(st.t * ${(this._height / vDis).toFixed(1)});
                                                            vec4 imageColor = texture(u_image, st);
                                                            out_FragColor = imageColor * u_color;
                                                        }
                                                        `
                czmTexture.uri = type == "bottom" ? sceneObject.bottomImage.url : sceneObject.sideImage.url;
                esCustomPrimitive.uniformMap = {
                    "u_image": {
                        "type": "texture",
                        "id": czmTexture.id
                    },
                    "u_color": [
                        1,
                        1,
                        1,
                        1
                    ]
                }
                // 自动计算包围盒
                const minMax = esCustomPrimitive.computeLocalAxisedBoundingBoxFromAttribute("position");
                if (!minMax) return;
                const { min, max } = minMax;
                esCustomPrimitive.setLocalAxisedBoundingBox(min, max);
            }
            update();
        }
        {
            const update = () => {
                if (this._esCustomPrimitive.uniformMap) {
                    this._esCustomPrimitive.uniformMap = {
                        "u_image": {
                            "type": "texture",
                            "id": this._czmTexture.id
                        },
                        "u_color": [
                            1,
                            1,
                            1,
                            sceneObject.opacity
                        ]
                    }
                }
            }
            update();
            this.d(sceneObject.opacityChanged.don(update));
        }
    }
    // 构建侧边三角网
    private _triangleIndices(length: number) {
        let lineIndices = new Uint32Array(length * 3);
        for (let i = 0; i < length; ++i) {
            lineIndices[i * 3 + 0] = i;
            lineIndices[i * 3 + 1] = i % 2 ? (i + 2) % length : (i + 1) % length;
            lineIndices[i * 3 + 2] = i % 2 ? (i + 1) % length : (i + 2) % length;
        };
        return lineIndices;
    }
    private _getDistance(start: ESPointLLH, end: ESPointLLH) {
        return Math.sqrt(Math.pow(start[0] - end[0], 2) + Math.pow(start[1] - end[1], 2));
    }
}