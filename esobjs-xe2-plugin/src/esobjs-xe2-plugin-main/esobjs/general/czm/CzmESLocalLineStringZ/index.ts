import { CzmESObjectWithLocation } from '@/esobjs-xe2-plugin-main/esobjs/base';
import { GeoPolyline, SceneObjectPickedInfo, getDistancesFromPositions, localPositionsToPositions } from 'xbsj-xe2/dist-node/xe2-base-objects';
import { createNextAnimateFrameEvent, track } from 'xbsj-xe2/dist-node/xe2-base-utils';
import { CzmViewer, getCameraPosition, getViewerExtensions } from 'xbsj-xe2/dist-node/xe2-cesium-objects';
import { ESLocalLineStringZ } from '../../objs';
import { getCzmPickedInfoFromPickedInfo } from '../base/utils';
import { flyWithPositions, getPointerEventButton } from '../base';

export class CzmESLocalLineStringZ extends CzmESObjectWithLocation<ESLocalLineStringZ> {
    static readonly type = this.register(ESLocalLineStringZ.type, this);
    private _czmGeoPolyline = this.disposeVar(new GeoPolyline());
    get czmGeoPolyline() { return this._czmGeoPolyline; }

    constructor(sceneObject: ESLocalLineStringZ, czmViewer: CzmViewer) {
        super(sceneObject, czmViewer);
        const viewer = czmViewer.viewer;
        if (!viewer) {
            console.warn(`viewer is undefined!`);
            return;
        }

        const extensions = getViewerExtensions(czmViewer.viewer);
        if (!extensions) {
            return;
        }

        const czmGeoPolyline = this._czmGeoPolyline;
        czmViewer.add(czmGeoPolyline);
        this.dispose(() => czmViewer.delete(czmGeoPolyline));

        czmGeoPolyline.arcType = 'NONE';

        {
            this.dispose(track([czmGeoPolyline, 'width'], [sceneObject, 'strokeWidth']));
            this.dispose(track([czmGeoPolyline, 'color'], [sceneObject, 'strokeColor']));
            const update = () => {
                czmGeoPolyline.show = sceneObject.show && sceneObject.stroked;
            }
            update()
            const event = this.dv(createNextAnimateFrameEvent(sceneObject.showChanged, sceneObject.strokedChanged));
            this.dispose(event.don(update));
        }

        {
            this.dispose(track([czmGeoPolyline, 'loop'], [sceneObject, 'loop']));
            this.dispose(track([czmGeoPolyline, 'hasDash'], [sceneObject, 'hasDash']));
            this.dispose(track([czmGeoPolyline, 'gapColor'], [sceneObject, 'gapColor']));
            this.dispose(track([czmGeoPolyline, 'dashLength'], [sceneObject, 'dashLength']));
            this.dispose(track([czmGeoPolyline, 'dashPattern'], [sceneObject, 'dashPattern']));
            this.dispose(track([czmGeoPolyline, 'hasArrow'], [sceneObject, 'hasArrow']));
            this.dispose(track([czmGeoPolyline, 'depthTest'], [sceneObject, 'depthTest']));
            this.dispose(track([czmGeoPolyline, 'allowPicking'], [sceneObject, 'allowPicking']));
            this.dispose(track([czmGeoPolyline, 'ground'], [sceneObject, 'strokeGround']));
        }

        this.dispose(czmGeoPolyline.pickedEvent.disposableOn((pickedInfo) => {
            if (getPointerEventButton(pickedInfo) === 0 && (sceneObject.allowPicking ?? false)) {
                const pickInfo = getCzmPickedInfoFromPickedInfo(pickedInfo)
                sceneObject.pickedEvent.emit(new SceneObjectPickedInfo(sceneObject, pickInfo));
            }
        }));
        {
            // update positions
            const update = () => {
                if (!sceneObject.points) {
                    return;
                }

                if (sceneObject.scale && sceneObject.scale.some(e => e === 0)) {
                    console.warn(`缩放属性(scale)不能设置值为0！`);
                    return;
                }

                const [positions] = localPositionsToPositions({
                    originPosition: sceneObject.position,
                    originRotation: sceneObject.rotation,
                    originScale: sceneObject.scale,
                }, sceneObject.points);

                czmGeoPolyline.positions = positions;
            };
            update();
            const updateEvent = this.disposeVar(createNextAnimateFrameEvent(
                sceneObject.pointsChanged,
                sceneObject.positionChanged,
                sceneObject.rotationChanged,
                sceneObject.scaleChanged,
            ));
            this.dispose(updateEvent.disposableOn(update));
        }
    }
    override visibleDistance(sceneObject: ESLocalLineStringZ, czmViewer: CzmViewer): void {
        if (czmViewer.viewer?.camera && sceneObject.show) {
            const dis = getDistancesFromPositions([sceneObject.position, getCameraPosition(czmViewer.viewer.camera)], 'NONE')[0];
            let show = false;
            if (sceneObject.minVisibleDistance < sceneObject.maxVisibleDistance) {
                show = sceneObject.minVisibleDistance < dis && dis < sceneObject.maxVisibleDistance;
            } else if (sceneObject.maxVisibleDistance == 0) {
                show = dis > sceneObject.minVisibleDistance;
            }
            this._czmGeoPolyline.show = sceneObject.show && sceneObject.stroked && show;
        }
    }
    override flyTo(duration: number | undefined, id: number): boolean {
        const { sceneObject, czmViewer, czmGeoPolyline } = this;
        if (!czmViewer.actived) return false;
        if (sceneObject.flyToParam || sceneObject.flyInParam) {
            return super.flyTo(duration, id);
        } else {
            if (czmGeoPolyline.positions) {
                flyWithPositions(czmViewer, sceneObject, id, czmGeoPolyline.positions, duration)
                return true;
            }
            return false;
        }
    }
}
