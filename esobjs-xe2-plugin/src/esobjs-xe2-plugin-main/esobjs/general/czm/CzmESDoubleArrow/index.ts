// import { CzmESVisualObject } from "@/esobjs-xe2-plugin-main/esobjs/base";
// import { SMGeoDoubleArrow } from 'smplotting-xe2-plugin/dist-node/smplotting-xe2-plugin-main';
// import { SceneObjectPickedInfo, getMinMaxCorner } from 'xbsj-xe2/dist-node/xe2-base-objects';
// import { bind, track } from "xbsj-xe2/dist-node/xe2-base-utils";
// import { CzmViewer } from 'xbsj-xe2/dist-node/xe2-cesium-objects';
// import { ESDoubleArrow } from '../../objs';
// import { getCzmPickedInfoFromPickedInfo } from "../base/utils";
// import { flyWithPositions } from "../base";
// export class CzmESDoubleArrow extends CzmESVisualObject<ESDoubleArrow> {
//     static readonly type = this.register(ESDoubleArrow.type, this);
//     private _czmESDoubleArrow = this.disposeVar(new SMGeoDoubleArrow());
//     get czmESDoubleArrow() { return this._czmESDoubleArrow; }

//     constructor(sceneObject: ESDoubleArrow, czmViewer: CzmViewer) {
//         super(sceneObject, czmViewer);
//         const viewer = czmViewer.viewer;
//         if (!viewer) {
//             console.warn(`viewer is undefined!`);
//             return;
//         }

//         const czmESDoubleArrow = this._czmESDoubleArrow;
//         // @ts-ignore
//         czmViewer.add(czmESDoubleArrow);
//         // @ts-ignore

//         this.dispose(() => czmViewer.delete(czmESDoubleArrow))
//         this.dispose(track([czmESDoubleArrow, 'show'], [sceneObject, 'show']));
//         this.dispose(bind([czmESDoubleArrow, 'editing'], [sceneObject, 'editing']));
//         this.dispose(bind([czmESDoubleArrow, 'pointEditing'], [sceneObject, 'pointEditing']));
//         this.dispose(track([czmESDoubleArrow, 'allowPicking'], [sceneObject, 'allowPicking']));
//         this.dispose(bind([czmESDoubleArrow, 'positions'], [sceneObject, 'points']));
//         this.dispose(track([czmESDoubleArrow, 'depth'], [sceneObject, 'depth']));
//         this.dispose(track([czmESDoubleArrow, 'ground'], [sceneObject, 'ground']));


//         this.dispose(track([czmESDoubleArrow, 'outline'], [sceneObject, 'stroked']));
//         this.dispose(track([czmESDoubleArrow, 'outlineColor'], [sceneObject, 'strokeColor']));
//         this.dispose(track([czmESDoubleArrow, 'outlineWidth'], [sceneObject, 'strokeWidth']));
//         this.dispose(track([czmESDoubleArrow, 'fill'], [sceneObject, 'filled']));
//         this.dispose(track([czmESDoubleArrow, 'color'], [sceneObject, 'fillColor']));

//         this.dispose(czmESDoubleArrow.pickedEvent.disposableOn(pickedInfo => {
//             if (sceneObject.allowPicking ?? false) {
//                 const pickInfo = getCzmPickedInfoFromPickedInfo(pickedInfo)

//                 sceneObject.pickedEvent.emit(new SceneObjectPickedInfo(sceneObject, pickInfo));
//             }
//         }));
//     }
//     override flyTo(duration: number | undefined, id: number): boolean {
//         const { sceneObject, czmViewer, czmESDoubleArrow } = this;
//         if (!czmViewer.actived) return false;
//         if (sceneObject.flyToParam || sceneObject.flyInParam) {
//             return super.flyTo(duration, id);
//         } else {
//             if (czmESDoubleArrow.positions) {
//                 flyWithPositions(czmViewer, sceneObject, id, czmESDoubleArrow.positions, duration);
//                 return true;
//             }
//             return false;
//         }
//     }
//     override flyIn(duration: number | undefined, id: number): boolean {
//         const { sceneObject, czmViewer, czmESDoubleArrow } = this;
//         if (!czmViewer.actived) return false;
//         if (sceneObject.flyInParam) {
//             return super.flyIn(duration, id);
//         } else {
//             const d = duration ?? 1
//             const { center } = getMinMaxCorner(czmESDoubleArrow.positions as [number, number, number][]);
//             this.flyToWithPromise(id, center, undefined, undefined, d)
//             return true;
//         }
//     }
// }
