import { getMinMaxCorner } from 'xbsj-xe2/dist-node/xe2-base-objects';
import { CzmESGeoVector, CzmESVisualObject } from '@/esobjs-xe2-plugin-main/esobjs/base';
import * as Cesium from 'cesium';
import { positionFromCartesian, positionsToUniqueCartesians, toColor } from 'xbsj-xe2/dist-node/xe2-base-cesium';
import { GeoPolyline, PositionsEditing, SceneObjectPickedInfo } from 'xbsj-xe2/dist-node/xe2-base-objects';
import { createNextAnimateFrameEvent, track, bind } from 'xbsj-xe2/dist-node/xe2-base-utils';
import { CzmViewer, flyTo, getViewerExtensions } from 'xbsj-xe2/dist-node/xe2-cesium-objects';
import { ESGeoLineString } from '../../objs';
import { getCzmPickedInfoFromPickedInfo } from '../base/utils';
import { flyWithPositions, getPointerEventButton } from '../base';

export class CzmESGeoLineString<T extends ESGeoLineString = ESGeoLineString> extends CzmESGeoVector<T> {
    static readonly type = this.register<ESGeoLineString>(ESGeoLineString.type, this);
    // private _sPositionsEditing = this.disposeVar(new PositionsEditing([this.sceneObject, 'points'], false, [this.sceneObject, 'editing'], this.czmViewer));
    // get sPositionsEditing() { return this._sPositionsEditing; }

    private _geoPolyline = this.disposeVar(new GeoPolyline());
    get geoPolyline() { return this._geoPolyline; }

    constructor(sceneObject: T, czmViewer: CzmViewer) {
        super(sceneObject, czmViewer);
        const viewer = czmViewer.viewer;
        if (!viewer) {
            console.warn(`viewer is undefined!`);
            return;
        }
        const extensions = getViewerExtensions(czmViewer.viewer);
        if (!extensions) return;

        const geoPolyline = this._geoPolyline;
        czmViewer.add(geoPolyline);
        this.dispose(() => czmViewer.delete(geoPolyline))

        // this.dispose(track([geoPolyline, 'show'], [sceneObject, 'show']));
        this.dispose(track([geoPolyline, 'allowPicking'], [sceneObject, 'allowPicking']));
        this.dispose(bind([geoPolyline, 'editing'], [sceneObject, 'editing']));
        this.dispose(bind([geoPolyline, 'positions'], [sceneObject, 'points']));

        // this.dispose(track([geoPolyline, 'show'], [sceneObject, 'stroked']));
        this.dispose(track([geoPolyline, 'width'], [sceneObject, 'strokeWidth']));
        this.dispose(track([geoPolyline, 'color'], [sceneObject, 'strokeColor']));
        this.dispose(track([geoPolyline, 'ground'], [sceneObject, 'strokeGround']));

        {
            const updateProp = () => {
                geoPolyline.show = sceneObject.show && sceneObject.stroked;
            }
            updateProp();
            const updateEvent = this.disposeVar(createNextAnimateFrameEvent(
                sceneObject.showChanged,
                sceneObject.strokedChanged,
            ));
            this.dispose(updateEvent.disposableOn(updateProp));
        }


        {//事件
            // this.dispose(sceneObject.flyToEvent.disposableOn((duration, id) => {
            //     if (!czmViewer.actived) return;
            //     if (sceneObject.flyToParam || sceneObject.flyInParam) {
            //         this.flyTo(duration, id);
            //     } else {
            //         geoPolyline.flyTo(duration && duration * 1000);
            //         sceneObject.flyOverEvent.emit(id, 'over', czmViewer);
            //     }
            // }));

            this.dispose(geoPolyline.pickedEvent.disposableOn(pickedInfo => {
                if (getPointerEventButton(pickedInfo) === 0 && (sceneObject.allowPicking ?? false)) {
                    const pickInfo = getCzmPickedInfoFromPickedInfo(pickedInfo)

                    sceneObject.pickedEvent.emit(new SceneObjectPickedInfo(sceneObject, pickInfo));
                }
            }));
        }



        // this.dispose(track([geoPolyline, 'ground'], [sceneObject, 'ground']));

        // const entity = viewer.entities.add({ polyline: {} });

        // this.dispose(() => viewer.entities.remove(entity));

        // const boundingSphere = new Cesium.BoundingSphere();

        // {
        //     const updateProp = () => { entity.show = sceneObject.show ?? true; }
        //     updateProp();
        //     this.dispose(sceneObject.showChanged.disposableOn(updateProp));
        // }

        // {
        //     const updateProp = () => {
        //         // @ts-ignore
        //         entity.polyline.width = sceneObject.stroked && sceneObject.strokeWidth
        //         // @ts-ignore
        //         entity.polyline.material = sceneObject.stroked && toColor(sceneObject.strokeColor);
        //     }
        //     updateProp();
        //     const updateEvent = this.disposeVar(createNextAnimateFrameEvent(
        //         sceneObject.strokeStyleChanged,
        //         sceneObject.strokedChanged,
        //     ));
        //     this.dispose(updateEvent.disposableOn(updateProp));
        // }

        // {
        //     const updateProp = () => {
        //         if (!sceneObject.points) {
        //             // @ts-ignore
        //             entity.polyline.positions = undefined;
        //             return;
        //         }
        //         const cartesians = positionsToUniqueCartesians(sceneObject.points);
        //         if (cartesians.length < 2) {
        //             boundingSphere.radius = -1;
        //             return;
        //         }
        //         Cesium.BoundingSphere.fromPoints(cartesians, boundingSphere);
        //         // @ts-ignore
        //         entity.polyline.positions = cartesians;
        //     }
        //     updateProp();
        //     this.dispose(sceneObject.pointsChanged.disposableOn(updateProp));
        // }


        // {//事件
        //     this.dispose(czmViewer.czmPickedEvent.disposableOn(pickedInfo => {
        //         if (!pickedInfo) return; const pickedResult = pickedInfo.czmPickResult;
        //         if (pickedResult && (sceneObject.allowPicking ?? false) && pickedResult.id === entity) {
        //             sceneObject.pickedEvent.emit(new SceneObjectPickedInfo(sceneObject, pickedInfo));
        //         }
        //     }));
        // }
    }
    override flyTo(duration: number | undefined, id: number): boolean {
        const { sceneObject, czmViewer, geoPolyline } = this;
        if (!czmViewer.actived) return false;
        if (sceneObject.flyToParam || sceneObject.flyInParam) {
            return super.flyTo(duration, id);
        } else {
            if (geoPolyline.positions) {
                flyWithPositions(czmViewer, sceneObject, id, geoPolyline.positions, duration);
                return true;
            }
            return false;
        }
    }
    override flyIn(duration: number | undefined, id: number): boolean {
        const { sceneObject, czmViewer, geoPolyline } = this;
        if (!czmViewer.actived) return false;
        if (sceneObject.flyInParam) {
            return super.flyIn(duration, id);
        } else {
            if (geoPolyline.positions) {
                flyWithPositions(czmViewer, sceneObject, id, geoPolyline.positions, duration);
                return true;
            }
            return false;
        }
    }
}
