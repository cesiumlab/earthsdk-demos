
import { CzmESObjectWithLocation, bindNorthRotation } from "@/esobjs-xe2-plugin-main/esobjs/base";
import { bind, track } from "xbsj-xe2/dist-node/xe2-base-utils";
import { CzmFireParticleSystem, CzmViewer } from "xbsj-xe2/dist-node/xe2-cesium-objects";
import { ESFireParticleSystem } from '../../objs';
import { CzmESEditing } from "@/esobjs-xe2-plugin-main/esobjs/utils";
import { defaultFlyToRotation } from "../base";

export class CzmESFireParticleSystem extends CzmESObjectWithLocation<ESFireParticleSystem> {
    static readonly type = this.register(ESFireParticleSystem.type, this);
    private _czmFireParticleSystem = this.disposeVar(new CzmFireParticleSystem());
    get czmFireParticleSystem() { return this._czmFireParticleSystem; }

    constructor(sceneObject: ESFireParticleSystem, czmViewer: CzmViewer) {
        super(sceneObject, czmViewer);
        const viewer = czmViewer.viewer;
        if (!viewer) {
            console.warn(`viewer is undefined!`);
            return;
        }
        const czmFireParticleSystem = this._czmFireParticleSystem;
        czmViewer.add(czmFireParticleSystem);
        this.dispose(() => czmViewer.delete(czmFireParticleSystem))
        this.dispose(track([czmFireParticleSystem, 'show'], [sceneObject, 'show']));
        this.dispose(bind([czmFireParticleSystem, 'position'], [sceneObject, 'position']));
        this.dispose(track([czmFireParticleSystem, 'translation'], [sceneObject, 'translation']));
        this.dispose(bindNorthRotation([czmFireParticleSystem, 'rotation'], [sceneObject, 'rotation']));
        this.dispose(track([czmFireParticleSystem, 'image'], [sceneObject, 'image']));
    }
    override flyTo(duration: number | undefined, id: number): boolean {
        const { sceneObject, czmViewer, czmFireParticleSystem } = this;
        if (!czmViewer.actived) return false;
        if (sceneObject.flyToParam || sceneObject.flyInParam) {
            return super.flyTo(duration, id);
        } else {
            const position = czmFireParticleSystem.position as [number, number, number];
            czmViewer.flyTo(position, 100, defaultFlyToRotation, duration && duration * 1000);
            sceneObject.flyOverEvent.emit(id, 'over', czmViewer);
            return true;
        }
    }
}
