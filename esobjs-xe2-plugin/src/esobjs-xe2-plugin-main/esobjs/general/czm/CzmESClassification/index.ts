import { CzmESVisualObject } from "@/esobjs-xe2-plugin-main/esobjs/base";
import { GeoClassificationPolygon, SceneObjectPickedInfo, getMinMaxCorner } from 'xbsj-xe2/dist-node/xe2-base-objects';
import { bind, track } from 'xbsj-xe2/dist-node/xe2-base-utils';
import { CzmViewer } from 'xbsj-xe2/dist-node/xe2-cesium-objects';
import { ESClassification } from '../../objs';
import { getCzmPickedInfoFromPickedInfo } from "../base/utils";
import { flyWithPositions, getPointerEventButton } from "../base";

export class CzmESClassification extends CzmESVisualObject<ESClassification> {
    static readonly type = this.register(ESClassification.type, this);
    private _polygon = this.disposeVar(new GeoClassificationPolygon());
    get polygon() { return this._polygon; }
    constructor(sceneObject: ESClassification, czmViewer: CzmViewer) {
        super(sceneObject, czmViewer);

        const viewer = czmViewer.viewer;
        if (!viewer) {
            console.warn(`viewer is undefined!`);
            return;
        }
        const polygon = this._polygon;
        czmViewer.add(polygon);
        this.dispose(() => czmViewer.delete(polygon))

        this.dispose(track([polygon, 'show'], [sceneObject, 'show']));
        this.dispose(bind([polygon, 'editing'], [sceneObject, 'editing']));
        this.dispose(bind([polygon, 'positions'], [sceneObject, 'points']));
        this.dispose(track([polygon, 'allowPicking'], [sceneObject, 'allowPicking']));
        this.dispose(track([polygon, 'depth'], [sceneObject, 'height']));

        this.dispose(track([polygon, 'showHelper'], [sceneObject, 'stroked']));
        this.dispose(track([polygon, 'outline'], [sceneObject, 'stroked']));
        this.dispose(track([polygon, 'outlineColor'], [sceneObject, 'strokeColor']));
        this.dispose(track([polygon, 'outlineWidth'], [sceneObject, 'strokeWidth']));

        this.dispose(track([polygon, 'fill'], [sceneObject, 'filled']));
        this.dispose(track([polygon, 'color'], [sceneObject, 'fillColor']));

        this.dispose(polygon.pickedEvent.disposableOn(pickedInfo => {
            if (getPointerEventButton(pickedInfo) === 0 && (sceneObject.allowPicking ?? false)) {
                const pickInfo = getCzmPickedInfoFromPickedInfo(pickedInfo)
                sceneObject.pickedEvent.emit(new SceneObjectPickedInfo(sceneObject, pickInfo));
            }
        }));
    }
    override flyTo(duration: number | undefined, id: number): boolean {
        const { sceneObject, czmViewer, polygon } = this;
        if (!czmViewer.actived) return false;
        if (sceneObject.flyToParam || sceneObject.flyInParam) {
            return super.flyTo(duration, id);
        } else {
            if (polygon.positions) {
                flyWithPositions(czmViewer, sceneObject, id, polygon.positions, duration);
                return true;
            }
            return false;
        }
    }
    override flyIn(duration: number | undefined, id: number): boolean {
        const { sceneObject, czmViewer, polygon } = this;
        if (!czmViewer.actived) return false;
        if (sceneObject.flyInParam) {
            return super.flyIn(duration, id);
        } else {
            if (polygon.positions) {
                flyWithPositions(czmViewer, sceneObject, id, polygon.positions, duration);
                return true;
            }
            return false;
        }
    }
}

