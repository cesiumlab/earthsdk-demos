import { EnumProperty, GroupProperty, SceneObjectPickedInfo, StringProperty } from "xbsj-xe2/dist-node/xe2-base-objects";
import { PartialWithUndefinedReactivePropsToNativeProps, ReactivePropsToNativePropsAndChanged, bind, extendClassProps, track } from "xbsj-xe2/dist-node/xe2-base-utils";
import { ESExcavate } from "./ESExcavate";
import { ESPit } from "./ESPit";

/**
 * https://www.wolai.com/earthsdk/ixfCjq4UjUqWy7SeVHnCnt
 * https://c0yh9tnn0na.feishu.cn/docx/NeN2dELwyotHYzxrxX9cNEC2nVc
 */
export class ESHole extends ESPit {
    static override readonly type = this.register('ESHole', this, { chsName: '挖坑(带材质+深度)', tags: ['ESObjects'], description: '挖坑' });
    override get typeName() { return 'ESHole'; }
    override get defaultProps() { return ESHole.createDefaultProps(); }
    override get json() { return this._innerGetJson() as JsonType; }
    override set json(value: JsonType) { this._innerSetJson(value); }

    private _excavate = this.dv(new ESExcavate());
    private _pit = this.dv(new ESPit());

    constructor(id?: string) {
        super(id);
        {
            this.d(this.components.disposableAdd(this._excavate));
            this.d(this.components.disposableAdd(this._pit));
        }
        {
            const { _excavate, _pit } = this;
            this.d(track([_excavate, 'show'], [this, 'show']));
            this.d(track([_pit, 'show'], [this, 'show']));

            this.d(track([_excavate, 'allowPicking'], [this, 'allowPicking']));
            this.d(track([_pit, 'allowPicking'], [this, 'allowPicking']));

            this.d(track([_excavate, 'collision'], [this, 'collision']));
            this.d(track([_pit, 'collision'], [this, 'collision']));

            this.dispose(bind([_pit, 'editing'], [this, 'editing']));
            this.dispose(bind([_pit, 'points'], [this, 'points']));
            this.dispose(track([_excavate, 'points'], [this, 'points']));

            this.dispose(track([_pit, 'pointed'], [this, 'pointed']));
            this.dispose(track([_excavate, 'pointed'], [this, 'pointed']));

            this.dispose(track([_pit, 'pointStyle'], [this, 'pointStyle']));
            this.dispose(track([_excavate, 'pointStyle'], [this, 'pointStyle']));

            this.dispose(track([_pit, 'stroked'], [this, 'stroked']));
            this.dispose(track([_excavate, 'stroked'], [this, 'stroked']));

            this.dispose(track([_pit, 'strokeStyle'], [this, 'strokeStyle']));
            this.dispose(track([_excavate, 'strokeStyle'], [this, 'strokeStyle']));

            this.dispose(track([_pit, 'filled'], [this, 'filled']));
            // this.dispose(track([_excavate, 'filled'], [this, 'filled']));

            this.dispose(track([_pit, 'fillStyle'], [this, 'fillStyle']));
            this.dispose(track([_excavate, 'fillStyle'], [this, 'fillStyle']));

            this.dispose(track([_excavate, 'mode'], [this, 'mode']));
            this.dispose(track([_excavate, 'targetID'], [this, 'targetID']));

            this.dispose(track([_pit, 'depth'], [this, 'depth']));
            this.dispose(track([_pit, 'sideImage'], [this, 'sideImage']));
            this.dispose(track([_pit, 'bottomImage'], [this, 'bottomImage']));
            this.dispose(track([_pit, 'opacity'], [this, 'opacity']));
            this.dispose(track([_pit, 'interpolation'], [this, 'interpolation']));

            this.dispose(bind([_pit, 'flyInParam'], [this, 'flyInParam']));
            this.dispose(bind([_pit, 'flyToParam'], [this, 'flyToParam']));

            this.dispose(_excavate.pickedEvent.don(pickedInfo => {
                if (this.allowPicking ?? false) {
                    this.pickedEvent.emit(new SceneObjectPickedInfo(this, pickedInfo));
                }
            }));

            this.dispose(_pit.pickedEvent.don(pickedInfo => {
                if (this.allowPicking ?? false) {
                    this.pickedEvent.emit(new SceneObjectPickedInfo(this, pickedInfo));
                }
            }));

            this.dispose(this.flyInEvent.don((duration?: number) => { _pit.flyIn(duration); }));
            this.dispose(this.flyToEvent.don((duration?: number) => { _pit.flyTo(duration); }));
            this.dispose(this.calcFlyToParamEvent.don(() => { _pit.calcFlyToParam(); }));
            this.dispose(this.calcFlyInParamEvent.don(() => { _pit.calcFlyInParam(); }));
        }
    }
    override getESProperties() {
        const properties = { ...super.getESProperties() };
        return {
            ...properties,
            basic: [
                new EnumProperty('模式', 'mode', false, false, [this, 'mode'], ESExcavate.defaults.modes, 'in'),
                new StringProperty('瓦片图层', 'targetID', false, false, [this, 'targetID'], ''),
                ...properties.basic,
            ]
        }
    }
    override getProperties(language: string) {
        return [
            ...super.getProperties(language),
            new GroupProperty('通用', '通用', [
                new EnumProperty('模式', 'mode', false, false, [this, 'mode'], ESExcavate.defaults.modes),
                new StringProperty('目标ID', 'targetID', false, false, [this, 'targetID'])
            ])
        ];
    }
}

export namespace ESHole {
    export const createDefaultProps = () => ({
        mode: "in",
        targetID: "",
        ...ESPit.createDefaultProps(),
    });
}
extendClassProps(ESHole.prototype, ESHole.createDefaultProps);
export interface ESHole extends ReactivePropsToNativePropsAndChanged<ReturnType<typeof ESHole.createDefaultProps>> { }
type JsonType = PartialWithUndefinedReactivePropsToNativeProps<ReturnType<typeof ESHole.createDefaultProps> & { type: string }>;


