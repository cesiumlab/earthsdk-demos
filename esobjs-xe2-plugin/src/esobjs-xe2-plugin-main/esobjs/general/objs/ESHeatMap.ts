import { BooleanProperty, EnumProperty, FunctionProperty, GroupProperty, JsonProperty, Number2Property, Number4Property, NumberProperty, StringProperty } from "xbsj-xe2/dist-node/xe2-base-objects";
import { PartialWithUndefinedReactivePropsToNativeProps, ReactivePropsToNativePropsAndChanged, extendClassProps, reactArray, reactArrayWithUndefined, reactDeepArrayWithUndefined, reactJson } from "xbsj-xe2/dist-node/xe2-base-utils";
import { SceneObjectKey } from "xbsj-xe2/dist-node/xe2-utils";
import { ESVisualObject } from "../../base/objs";

export type ESClassificationType = 'Terrain' | 'Cesium3dTile' | 'Both';

const dataMd = `
示例1:设置rectangle为 [
    119.39,
    39.9,
    120.39,
    40.9
]
\`\`\`
`;

export class ESHeatMap extends ESVisualObject {
    static readonly type = this.register('ESHeatMap', this, { chsName: '热力图', tags: ['ESObjects', '_ES_Impl_Cesium'], description: "ESHeatMap" });
    get typeName() { return 'ESHeatMap'; }
    override get defaultProps() { return ESHeatMap.createDefaultProps(); }
    get json() { return this._innerGetJson() as JsonType; }
    set json(value: JsonType) { this._innerSetJson(value); }

    static override defaults = {
        ...ESVisualObject.defaults,
        show: true,
        allowPicking: false,
        // origin: [0.5, 0.5] as [number, number],
        // rotationAngle: 0 as number,
        // color: [1, 1, 1, 1] as [number, number, number, number],
        // classificationType: 'Both' as ESClassificationType,
        // data: [] as [number, number, number][],
        dataMd: dataMd,
        //canvasWidth: 256,
        // max: 100,
        // min: 0,
        //radius: 50,
        // maxOpacity: 0.5,
        // minOpacity: 0,
        // blur: 0.85,
        // gradient: {
        //     ".5": "green",
        //     ".7": "yellow",
        //     ".95": "red"
        // },
        // pointEditing: false,
        // rectangle: [119.39 - 1, 39.9 - 1, 119.39 + 1, 39.9 + 1] as [number, number, number, number],
        // editing: false,
        // debug: false,
    };

    setTestCenterData() {
        if (!this.rectangle) {
            console.warn(`setRandomData error: !this.rectangle`);
            return;
        }

        const r = this.rectangle;
        const w = r[2] - r[0];
        const h = r[3] - r[1];
        if (w <= 0 || h <= 0) {
            console.warn(`setRandomData error: w <= 0 || h <= 0`);
            return;
        }

        this.data = [[r[0], r[1], 100], [r[2], r[3], 100], [w * .5 + r[0], h * .5 + r[1], 100]];
    }

    setTestRandomData(num: number) {
        if (!this.rectangle) {
            console.warn(`setRandomData error: !this.rectangle`);
            return;
        }

        const r = this.rectangle;
        const w = r[2] - r[0];
        const h = r[3] - r[1];
        if (w <= 0 || h <= 0) {
            console.warn(`setRandomData error: w <= 0 || h <= 0`);
            return;
        }

        const radius = this.radius;
        const canvasWidth = this.canvasWidth;
        const longitudeDegreesScale = Math.cos((r[1] + h * .5) * Math.PI / 180);
        const hratio = 2 * radius / canvasWidth;
        const wratio = hratio * longitudeDegreesScale;

        this.data = [...new Array(num).keys()].map(e => {
            return [
                (r[0] + wratio*w) + (w - 2*w*wratio) * Math.random(),
                (r[1] + hratio*h) + (h - 2*h*hratio) * Math.random(),
                Math.random() * 100,
            ]
        });
    }

    constructor(id?: SceneObjectKey) {
        super(id);
    }

    override getProperties(language?: string) {
        return [
            ...super.getProperties(language),
            new GroupProperty('通用', '通用', [
                new NumberProperty('旋转角', '旋转角', false, false, [this, 'rotationAngle']),
                new BooleanProperty('是否编辑', '是否编辑.', false, false, [this, 'editing']),
                new BooleanProperty('是否单点编辑', '是否单点编辑.', false, false, [this, 'pointEditing']),
                new Number4Property('rectangle', 'rectangle', false, false, [this, 'rectangle']),
                new Number4Property('颜色', '颜色', false, false, [this, 'color']),
                new EnumProperty('classificationType', 'classificationType', false, false, [this, 'classificationType'], [['Terrain', 'Terrain'], ['Cesium3dTile', 'Cesium3dTile'], ['Both', 'Both']]),
            ]),
            new GroupProperty('热力图', '热力图', [
                new JsonProperty('数据', '数据，需要经纬度以及对应的热力值', false, false, [this, 'data'], undefined, ESHeatMap.defaults.dataMd),
                new NumberProperty('画布宽度', '画布宽度', false, false, [this, 'canvasWidth']),
                new NumberProperty('最大值', '最大值', false, false, [this, 'max']),
                new NumberProperty('最小值', '最小值', false, false, [this, 'min']),
                new NumberProperty('半径', '半径', true, false, [this, 'radius']),
                new NumberProperty('最大不透明度', '最大不透明度(0-1),最高值将具有的最大不透明度', true, false, [this, 'maxOpacity']),
                new NumberProperty('最小不透明度', '最小不透明度(0-1),最低值将具有的最小不透明度', true, false, [this, 'minOpacity']),
                new NumberProperty('模糊', '模糊(0-1),应用于所有数据点的模糊因子。模糊因子越高，渐变越平滑', true, false, [this, 'blur']),
                new JsonProperty('渐变', '渐变', true, false, [this, 'gradient']),
                new FunctionProperty('setTestCenterData', 'setTestCenterData', [], () => this.setTestCenterData(), []),
                new FunctionProperty('setTestRandomData', 'setTestRandomData', ['number'], num => this.setTestRandomData(num), [5]),
                new BooleanProperty('调试', '调试.', true, false, [this, 'debug']),
            ]),
        ];
    }
}

export namespace ESHeatMap {
    export const createDefaultProps = () => ({
        // rotationAngle: undefined as number | undefined,
        // color: reactArrayWithUndefined<[number, number, number, number]>(undefined),
        // // imageUrl: undefined as string | undefined,
        // classificationType: undefined as CzmCustomGroundRectangleClassificationType | undefined,
        // ...ESVisualObject.createDefaultProps(),
        // data: reactDeepArrayWithUndefined<[number, number, number]>(undefined, (a, b) => a.every((e, i) => e === b[i]), s => [...s]),
        // // data: reactDeepArrayWithUndefined<{ x: number, y: number,value: number }>(undefined, (a, b) => {
        // //     return a.x === b.x && a.y === b.y && a.value === b.value
        // // }, s => ({ ...s })),
        // canvasWidth: undefined as number | undefined,
        // max: undefined as number | undefined,
        // min: undefined as number | undefined,
        // radius: undefined as number | undefined,
        // maxOpacity: undefined as number | undefined,
        // minOpacity: undefined as number | undefined,
        // blur: undefined as number | undefined,
        // gradient: reactJsonWithUndefined<{ [key: string]: string }>(undefined),
        // rectangle: reactArrayWithUndefined<[number, number, number, number]>(undefined),
        // editing: undefined as boolean | undefined,
        // pointEditing: undefined as boolean | undefined,

        ...ESVisualObject.createDefaultProps(),
        // show: undefined as boolean | undefined,
        // allowPicking: undefined as boolean | undefined,
        rotationAngle: 0,
        color: reactArray<[number, number, number, number]>([1, 1, 1, 1]),
        classificationType: 'Both' as ESClassificationType,
        data: reactDeepArrayWithUndefined<[number, number, number]>([], (a, b) => a.every((e, i) => e === b[i]), s => [...s]),
        canvasWidth: 256,
        max: 100,
        min: 0,
        radius: 25,
        maxOpacity: 0.5,
        minOpacity: 0,
        blur: 0.85,
        gradient: reactJson<{ [key: string]: string }>({
            ".5": "green",
            ".7": "yellow",
            ".95": "red"
        }),
        rectangle: reactArray<[number, number, number, number]>([119.39 - 1, 39.9 - 1, 119.39 + 1, 39.9 + 1]),
        editing: false,
        pointEditing: false,
        debug: false,
    });
}
extendClassProps(ESHeatMap.prototype, ESHeatMap.createDefaultProps);
export interface ESHeatMap extends ReactivePropsToNativePropsAndChanged<ReturnType<typeof ESHeatMap.createDefaultProps>> { }
type JsonType = PartialWithUndefinedReactivePropsToNativeProps<ReturnType<typeof ESHeatMap.createDefaultProps> & { type: string }>;
