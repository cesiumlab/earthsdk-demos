import { GroupProperty, JsonProperty } from 'xbsj-xe2/dist-node/xe2-base-objects';
import { PartialWithUndefinedReactivePropsToNativeProps, ReactivePropsToNativePropsAndChanged, extendClassProps, reactPositionsSet } from "xbsj-xe2/dist-node/xe2-base-utils";
import { ESGeoPolygon } from './ESGeoPolygon';

/**
 * https://www.wolai.com/earthsdk/6fsNXeZye81jUFUhL7U7xM
 * https://c0yh9tnn0na.feishu.cn/docx/QLdtd2eQGoUuPYxWXwacbxOqnhg
 */

export class ESPolygonWithHole extends ESGeoPolygon {
    static override readonly type = this.register('ESPolygonWithHole', this, { chsName: '内部裁切多边形', tags: ['ESObjects', '_ES_Impl_Cesium', '_ES_Impl_UE'], description: '带洞多边形' });
    override get typeName() { return 'ESPolygonWithHole'; }
    override get defaultProps() { return ESPolygonWithHole.createDefaultProps(); }
    override get json() { return this._innerGetJson() as JsonType; }
    override set json(value: JsonType) { this._innerSetJson(value); }
    static override defaults = {
        ...ESGeoPolygon.defaults,
        innerRings: [],//TODO
    }
    override getESProperties() {
        const properties = { ...super.getESProperties() };
        return {
            ...properties,
            basic: [
                ...properties.basic,
                new JsonProperty('裁切多边形数组', '必须在内部，且没有相交，裁切的多边形数组', true, false, [this, 'innerRings'], []),
            ],
        }
    }
    override getProperties(language: string) {
        return [
            ...super.getProperties(language),
            new GroupProperty('通用', '通用', [
                new JsonProperty('裁切多边形数组', '必须在内部，且没有相交，裁切的多边形数组', true, false, [this, 'innerRings'], []),
            ])
        ];
    }
}

export namespace ESPolygonWithHole {
    export const createDefaultProps = () => ({
        ...ESGeoPolygon.createDefaultProps(),
        innerRings: reactPositionsSet(undefined),
    })
}
extendClassProps(ESPolygonWithHole.prototype, ESPolygonWithHole.createDefaultProps);
export interface ESPolygonWithHole extends ReactivePropsToNativePropsAndChanged<ReturnType<typeof ESPolygonWithHole.createDefaultProps>> { }
type JsonType = PartialWithUndefinedReactivePropsToNativeProps<ReturnType<typeof ESPolygonWithHole.createDefaultProps> & { type: string }>;
