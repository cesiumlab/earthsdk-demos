import { GroupProperty, Number4Property, NumberProperty, StringProperty, Viewer } from "xbsj-xe2/dist-node/xe2-base-objects";
import { Destroyable, Event, PartialWithUndefinedReactivePropsToNativeProps, ReactivePropsToNativePropsAndChanged, bind, extendClassProps, reactArray, reactArrayWithUndefined, track } from "xbsj-xe2/dist-node/xe2-base-utils";
import { ESVisualObject, UeViewer } from "../../base";
import { ESImageryLayer } from "./ESImageryLayer";
import { SceneObjectKey } from "xbsj-xe2/dist-node/xe2-utils";
import * as Cesium from 'cesium';
import { CzmViewer } from "xbsj-xe2/dist-node/xe2-cesium-objects";

//3D级数转为2D高
const altToZoom = (cameraHeight: number) => {
    const levels = [
        { maxAlt: 250000000, level: 0 },
        { maxAlt: 25000000, level: 1 },
        { maxAlt: 9000000, level: 2 },
        { maxAlt: 7000000, level: 3 },
        { maxAlt: 4400000, level: 4 },
        { maxAlt: 2000000, level: 5 },
        { maxAlt: 1000000, level: 6 },
        { maxAlt: 493977, level: 7 },
        { maxAlt: 218047, level: 8 },
        { maxAlt: 124961, level: 9 },
        { maxAlt: 56110, level: 10 },
        { maxAlt: 40000, level: 11 },
        { maxAlt: 13222, level: 12 },
        { maxAlt: 7000, level: 13 },
        { maxAlt: 4000, level: 14 },
        { maxAlt: 2500, level: 15 },
        { maxAlt: 1500, level: 16 },
        { maxAlt: 600, level: 17 },
        { maxAlt: 250, level: 18 },
        { maxAlt: 150, level: 19 },
        { maxAlt: 50, level: 20 }
    ];

    for (const { maxAlt, level } of levels) {
        if (cameraHeight >= maxAlt) {
            return level;
        }
    }

    return 20; // 默认级别
}
const xyzToAllInfo = async (x: number, y: number, z: number) => {
    const response = await fetch(`https://tileser.giiiis.com/xyzinfo/${z}/${x}/${y}`);
    const jsonData = response.json();
    return jsonData;
}

const getCurrentTileCoordinates = async (viewer: Viewer) => {

    let position: [number, number, number] | undefined = undefined
    if (viewer instanceof CzmViewer) {
        position = viewer.getCameraInfo()?.position
    }
    if (viewer instanceof UeViewer) {
        const promise = await viewer.getCurrentCameraInfo()
        position = promise?.position
    }
    if (!position) return undefined;

    const longitude = position[0]
    const latitude = position[1]

    // 计算缩放级别 (Z)
    const cameraHeight = position[2];
    const level = altToZoom(cameraHeight);

    // 转换经纬度为瓦片坐标 (X, Y)
    let x = Math.floor((longitude + 180) / 360 * Math.pow(2, level + 1));
    let y = Math.floor((90 - latitude) / 180 * Math.pow(2, level));

    const allTimes = await xyzToAllInfo(x, y, level);

    return allTimes;
}


export class GetCurrentTileCoordinates extends Destroyable {
    constructor(viewer: Viewer, sceneObject: ESGeHistoryImagery, esImageryLayer: ESImageryLayer) {
        super();
        // 获取初始时间
        const updateImageryLayer = async () => {
            const initialTimes: string[] | undefined = await getCurrentTileCoordinates(viewer); // 获取时间数据
            sceneObject.datesEvent.emit(initialTimes, viewer)
        }
        updateImageryLayer()

        let timer: any = undefined
        timer = setInterval(() => {
            updateImageryLayer()
        }, 3000)
        this.dispose(() => timer && clearInterval(timer))

        {
            const update = () => {
                const currentDate = sceneObject.currentDate ? sceneObject.currentDate : 0
                esImageryLayer.url = `https://tileser.giiiis.com/timetile/tms/${currentDate}/tilemapresource.xml`
            }
            update()
            this.dispose(sceneObject.currentDateChanged.don(update))
        }
    }
}


/**
 * https://c0yh9tnn0na.feishu.cn/docx/H3hQd3KHCou74jxenckcsU58n8d
 */
export class ESGeHistoryImagery extends ESVisualObject {
    static readonly type = this.register('ESGeHistoryImagery', this, { chsName: '谷歌历史影像', tags: ['ESObjects', '_ES_Impl_UE'], description: "ESGeHistoryImagery" });
    get typeName() { return 'ESGeHistoryImagery'; }
    override get defaultProps() { return ESGeHistoryImagery.createDefaultProps(); }
    get json() { return this._innerGetJson() as JsonType; }
    set json(value: JsonType) { this._innerSetJson(value); }

    private _esImageryLayer = this.disposeVar(new ESImageryLayer());
    get esImageryLayer() { return this._esImageryLayer; }

    private _datesEvent = this.disposeVar(new Event<[string[] | undefined, Viewer]>());
    get datesEvent() { return this._datesEvent; }

    static override defaults = {
        ...ESVisualObject.defaults,
    }

    constructor(id?: SceneObjectKey) {
        super(id);
        const esImageryLayer = this._esImageryLayer;
        this.dispose(this.components.disposableAdd(esImageryLayer));
        // esImageryLayer.czmTilingScheme = 'GeographicTilingScheme';属性已删除
        esImageryLayer.zIndex = 1
        esImageryLayer.minimumLevel = 1
        esImageryLayer.maximumLevel = 18
        this.dispose(track([esImageryLayer, 'show'], [this, 'show']));
        this.dispose(track([esImageryLayer, 'zIndex'], [this, 'zIndex']));
        this.dispose(track([esImageryLayer, 'rectangle'], [this, 'rectangle']));
        this.dispose(bind([esImageryLayer, 'flyInParam'], [this, 'flyInParam']));
        this.dispose(bind([esImageryLayer, 'flyToParam'], [this, 'flyToParam']));

        this.registerAttachedObjectForContainer(viewer => new GetCurrentTileCoordinates(viewer, this, esImageryLayer));
        this.dispose(this.flyInEvent.don((duration?: number) => { esImageryLayer.flyIn(duration); }));
        this.dispose(this.flyToEvent.don((duration?: number) => { esImageryLayer.flyTo(duration); }));
        this.dispose(this.calcFlyToParamEvent.don(() => { esImageryLayer.calcFlyToParam(); }));
        this.dispose(this.calcFlyInParamEvent.don(() => { esImageryLayer.calcFlyInParam(); }));

    }
    override getESProperties() {
        const properties = { ...super.getESProperties() };
        return {
            ...properties,
            basic: [
                ...properties.basic,
                new StringProperty('时间', 'currentDate', false, false, [this, 'currentDate'], '0'),
                new NumberProperty('层级', '层级', false, false, [this, 'zIndex'], 0),
                new Number4Property('矩形范围', '西南东北', false, false, [this, 'rectangle'], [-180, -90, 180, 90]),
            ],
        }
    }
    override getProperties(language?: string) {
        return [
            ...super.getProperties(language),
            new GroupProperty('通用', '通用', [
                new StringProperty('currentDate', 'currentDate', false, false, [this, 'currentDate']),
                new NumberProperty('zIndex', '层级', false, false, [this, 'zIndex']),
                new Number4Property('矩形范围', '西南东北', false, false, [this, 'rectangle']),
            ]),
        ];
    }
}

export namespace ESGeHistoryImagery {
    export const createDefaultProps = () => ({
        ...ESVisualObject.createDefaultProps(),
        currentDate: "",
        zIndex: 0,
        rectangle: reactArray<[number, number, number, number]>([-180, -90, 180, 90]),
    });
}
extendClassProps(ESGeHistoryImagery.prototype, ESGeHistoryImagery.createDefaultProps);
export interface ESGeHistoryImagery extends ReactivePropsToNativePropsAndChanged<ReturnType<typeof ESGeHistoryImagery.createDefaultProps>> { }
type JsonType = PartialWithUndefinedReactivePropsToNativeProps<ReturnType<typeof ESGeHistoryImagery.createDefaultProps> & { type: string }>;
