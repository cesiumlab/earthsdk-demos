import { BooleanProperty, FunctionProperty, StringProperty, Viewer } from "xbsj-xe2/dist-node/xe2-base-objects";
import { Event, extendClassProps, PartialWithUndefinedReactivePropsToNativeProps, ReactivePropsToNativePropsAndChanged, react } from "xbsj-xe2/dist-node/xe2-base-utils";
import { ESObjectWithLocation } from "../../base/objs";
import { UeViewer } from "xbsj-xe2/dist-node/xe2-ue-objects";

export type ActorStatusType = 'bound' | 'created' | 'null';
/**
 * https://c0yh9tnn0na.feishu.cn/docx/EIqxdNSP3oN7aSxxa8ncuxxSnTd
 */
export class ESUnrealActor extends ESObjectWithLocation {
    static readonly type = this.register('ESUnrealActor', this, { chsName: 'UnrealActor', tags: ['ESObjects', '_ES_Impl_Cesium', '_ES_Impl_UE'], description: "UnrealActor" });
    get typeName() { return 'ESUnrealActor'; }
    override get defaultProps() { return ESUnrealActor.createDefaultProps(); }
    get json() { return this._innerGetJson() as JsonType; }
    set json(value: JsonType) { this._innerSetJson(value); }

    private _callFunctionEvent = this.disposeVar(new Event<[string, { [k: string]: any }]>());
    get callFunctionEvent() { return this._callFunctionEvent; }
    callFunction(fn: string, param: { [k: string]: any }) { this._callFunctionEvent.emit(fn, param); }

    async getBoundSphereWithChildren(viewer: Viewer) {
        if (!(viewer instanceof UeViewer)) return undefined;
        return await viewer.getBoundSphereWithChildren(this.id)
    }

    private _actorEvent = this.disposeVar(new Event<[status: ActorStatusType, viewer: Viewer]>());
    get actorEvent() { return this._actorEvent; }

    private _lastActorStatus = this.disposeVar(react<ActorStatusType>('null'));
    get lastActorStatus() { return this._lastActorStatus.value; }
    set lastActorStatus(value: ActorStatusType) { this._lastActorStatus.value = value; }
    get lastActorStatusChanged() { return this._lastActorStatus.changed; }
    override getESProperties() {
        const properties = { ...super.getESProperties() };
        return {
            ...properties,
            basic: [
                ...properties.basic,
                new StringProperty("actorTag", "actorTag", false, false, [this, 'actorTag'], ''),
                new StringProperty("actorClass", "actorClass", false, false, [this, 'actorClass'], ''),
                new BooleanProperty('是否高亮', '是否高亮highlight.', false, false, [this, 'highlight'], false),
            ],
        }
    }
    override getProperties(language?: string) {
        return [
            ...super.getProperties(language),
            new FunctionProperty('callFunction', 'callFunction', ['string', 'string'], (fn, param) => this.callFunction(fn, JSON.parse(param)), ['', '']),
            new StringProperty("actorTag", "actorTag", false, false, [this, 'actorTag']),
            new StringProperty("actorClass", "actorClass", false, false, [this, 'actorClass']),
            new BooleanProperty('是否高亮', '是否高亮highlight.', false, false, [this, 'highlight']),
        ];
    }
}

export namespace ESUnrealActor {
    export const createDefaultProps = () => ({
        ...ESObjectWithLocation.createDefaultProps(),
        actorTag: "",
        actorClass: "",
        highlight: false,
        allowPicking:true,
    });
}
extendClassProps(ESUnrealActor.prototype, ESUnrealActor.createDefaultProps);
export interface ESUnrealActor extends ReactivePropsToNativePropsAndChanged<ReturnType<typeof ESUnrealActor.createDefaultProps>> { }
type JsonType = PartialWithUndefinedReactivePropsToNativeProps<ReturnType<typeof ESUnrealActor.createDefaultProps> & { type: string }>;
