import { JsonValue, PartialWithUndefinedReactivePropsToNativeProps, ReactivePropsToNativePropsAndChanged, extendClassProps } from "xbsj-xe2/dist-node/xe2-base-utils";
import { SceneObjectKey } from "xbsj-xe2/dist-node/xe2-utils";
import { ESGeoPolygon } from "./ESGeoPolygon";
import { ESFillStyle } from "../../base/objs";

/**
 * https://www.wolai.com/earthsdk/owzYtkDnctKcA9CCiYAX2i
 * https://c0yh9tnn0na.feishu.cn/docx/Mh0SdHXpIoAdTCxHzrdcOJTqnOe
 */
export class ESAreaMeasurement extends ESGeoPolygon {
    static override readonly type = this.register('ESAreaMeasurement', this, { chsName: '面积测量', tags: ['ESObjects', '_ES_Impl_Cesium', '_ES_Impl_UE'], description: "面积测量" });
    override get typeName() { return 'ESAreaMeasurement'; }
    override get defaultProps() { return ESAreaMeasurement.createDefaultProps(); }
    override get json() { return this._innerGetJson() as JsonType; }
    override set json(value: JsonType) { this._innerSetJson(value); }

    override get fillMaterial() { return this.fillStyle.material; }
    override set fillMaterial(value: string) { this.fillStyle = { ...this.fillStyle, material: value } }

    override  get fillMaterialParams() { return this.fillStyle ? this.fillStyle.materialParams : ESAreaMeasurement.defaults.fillStyle.materialParams; }
    override set fillMaterialParams(value: JsonValue | undefined) { this.fillStyle = { ...this.fillStyle ?? ESAreaMeasurement.defaults.fillStyle, materialParams: value ?? ESAreaMeasurement.defaults.fillStyle.materialParams } }


    static override defaults = {
        ...ESGeoPolygon.defaults,
        fillStyle: {
            color: [1, 1, 1, 1],
            material: "Material'/EarthSDKForUE/M_ES_Material.M_ES_Material'",
            materialParams: { Opacity: 0.4 },
            ground:false,
        } as ESFillStyle,
        stroked: true,
        filled: true
    };

    constructor(id?: SceneObjectKey) {
        super(id);
        this.fillStyle.ground = true;
        this.strokeStyle.width = 2;
    }

    override getProperties(language: string) {
        return [
            ...super.getProperties(language),
        ]
    }
}

export namespace ESAreaMeasurement {
    export const createDefaultProps = () => ({
        ...ESGeoPolygon.createDefaultProps(),
    });
}
extendClassProps(ESAreaMeasurement.prototype, ESAreaMeasurement.createDefaultProps);
export interface ESAreaMeasurement extends ReactivePropsToNativePropsAndChanged<ReturnType<typeof ESAreaMeasurement.createDefaultProps>> { }
type JsonType = PartialWithUndefinedReactivePropsToNativeProps<ReturnType<typeof ESAreaMeasurement.createDefaultProps> & { type: string }>;
