import { GroupProperty, StringProperty } from "xbsj-xe2/dist-node/xe2-base-objects";
import { PartialWithUndefinedReactivePropsToNativeProps, ReactivePropsToNativePropsAndChanged, extendClassProps, react } from "xbsj-xe2/dist-node/xe2-base-utils";
import { SceneObjectKey } from "xbsj-xe2/dist-node/xe2-utils";
import { ESGeoPolygon } from "./ESGeoPolygon";

/**
 * https://c0yh9tnn0na.feishu.cn/docx/EcEydGFHHozy2WxdGntc6ujgnSZ
 */
export class ESPolygonFlattenedPlane extends ESGeoPolygon {
    static override readonly type = this.register('ESPolygonFlattenedPlane', this, { chsName: '压平', tags: ['ESObjects', '_ES_Impl_Cesium', '_ES_Impl_UE'], description: "ESPolygonFlattenedPlane" });
    override get typeName() { return 'ESPolygonFlattenedPlane'; }
    override get defaultProps() { return ESPolygonFlattenedPlane.createDefaultProps(); }
    override get json() { return this._innerGetJson() as JsonType; }
    override set json(value: JsonType) { this._innerSetJson(value); }

    private _czmFlattenedPlaneId = this.disposeVar(react<string>(""));
    get czmFlattenedPlaneId() { return this._czmFlattenedPlaneId.value; }
    set czmFlattenedPlaneId(value: string) { this._czmFlattenedPlaneId.value = value; }
    get czmFlattenedPlaneIdChanged() { return this._czmFlattenedPlaneId.changed; }

    constructor(id?: SceneObjectKey) {
        super(id);

    }
    static override defaults = {
        ...ESGeoPolygon.defaults,
    }
    override getESProperties() {
        const properties = { ...super.getESProperties() };
        return {
            ...properties,
            basic: [
                ...properties.basic,
                new StringProperty('瓦片图层', 'targetID', false, false, [this, 'targetID'], '')
            ]
        }
    }
    override getProperties(language: string) {
        return [
            ...super.getProperties(language),
            new GroupProperty('通用', '通用', [
                new GroupProperty('czm', 'czm', [
                    new StringProperty('targetID', 'targetID', false, false, [this, 'targetID']),
                ]),
            ]),
        ];
    }
}

export namespace ESPolygonFlattenedPlane {
    export const createDefaultProps = () => ({
        ...ESGeoPolygon.createDefaultProps(),
        targetID: "",
        filled: false,
    });
}
extendClassProps(ESPolygonFlattenedPlane.prototype, ESPolygonFlattenedPlane.createDefaultProps);
export interface ESPolygonFlattenedPlane extends ReactivePropsToNativePropsAndChanged<ReturnType<typeof ESPolygonFlattenedPlane.createDefaultProps>> { }
type JsonType = PartialWithUndefinedReactivePropsToNativeProps<ReturnType<typeof ESPolygonFlattenedPlane.createDefaultProps> & { type: string }>;