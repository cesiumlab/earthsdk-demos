import { GroupProperty, JsonProperty, NumberProperty } from "xbsj-xe2/dist-node/xe2-base-objects";
import { PartialWithUndefinedReactivePropsToNativeProps, ReactivePropsToNativePropsAndChanged, extendClassProps, react, reactJsonWithUndefined } from "xbsj-xe2/dist-node/xe2-base-utils";
import { SceneObjectKey } from "xbsj-xe2/dist-node/xe2-utils";
import { ESObjectWithLocation } from "../../base/objs";
import { ESJResource } from "../../base";


export type ESDataMeshColorStopType = {
    ratio: number;
    rgba: [number, number, number, number];
};

export class ESDataMesh extends ESObjectWithLocation {
    static readonly type = this.register('ESDataMesh', this, { chsName: '数值面着色', tags: ['ESObjects', '_ES_Impl_Cesium', '_ES_Impl_UE'], description: "数值面着色" });
    get typeName() { return 'ESDataMesh'; }
    override get defaultProps() { return ESDataMesh.createDefaultProps(); }
    get json() { return this._innerGetJson() as JsonType; }
    set json(value: JsonType) { this._innerSetJson(value); }

    static override defaults = {
        ...ESObjectWithLocation.defaults,
        // url: 'https://114.242.26.126:7002/data/202309/xly/water-assets2/',
        url: 'http://114.242.26.126:6003/ESDataMesh/water-assets2/',
        maxTime: 23,
        currentTime: 0,
        minPropValue: 0,
        maxPropValue: 1,
        colorStops: [
            { ratio: 0., rgba: [0, 0, 1, 1] },
            { ratio: .2, rgba: [0, 1, 0, 1] },
            { ratio: .8, rgba: [1, 1, 0, 1] },
            { ratio: 1., rgba: [1, 0, 0, 1] },
        ] as ESDataMeshColorStopType[],
    };

    private _maxTime = this.disposeVar(react<number | undefined>(ESDataMesh.defaults.maxTime));
    get maxTime() { return this._maxTime.value; }
    get maxTimeChanged() { return this._maxTime.changed; }

    constructor(id?: SceneObjectKey) {
        super(id);
    }

    override getProperties(language?: string) {
        return [
            ...super.getProperties(language),
            new GroupProperty('通用', '通用', [
                new JsonProperty('url', 'url', false, false, [this, 'url']),
                new NumberProperty('maxTime', 'maxTime', true, true, [this, 'maxTime'], ESDataMesh.defaults.maxTime), // 康总要求maxTime不能修改 20230905
                new NumberProperty('currentTime', 'currentTime', false, false, [this, 'currentTime']),
                new NumberProperty('minPropValue', 'minPropValue', false, false, [this, 'minPropValue']),
                new NumberProperty('maxPropValue', 'maxPropValue', false, false, [this, 'maxPropValue']),
                new JsonProperty('colorStops', 'colorStops', true, false, [this, 'colorStops'], ESDataMesh.defaults.colorStops),
            ]),
        ];
    }
}

export namespace ESDataMesh {
    export const createDefaultProps = () => ({
        ...ESObjectWithLocation.createDefaultProps(),
        url: 'http://114.242.26.126:6003/ESDataMesh/water-assets2/' as string | ESJResource,
        // maxTime: undefined as number | undefined, // 康总要求maxTime不能修改 20230905
        currentTime: 0,
        minPropValue: 0,
        maxPropValue: 1,
        colorStops: reactJsonWithUndefined<ESDataMeshColorStopType[]>(undefined),
    });
}
extendClassProps(ESDataMesh.prototype, ESDataMesh.createDefaultProps);
export interface ESDataMesh extends ReactivePropsToNativePropsAndChanged<ReturnType<typeof ESDataMesh.createDefaultProps>> { }
type JsonType = PartialWithUndefinedReactivePropsToNativeProps<ReturnType<typeof ESDataMesh.createDefaultProps> & { type: string }>;
