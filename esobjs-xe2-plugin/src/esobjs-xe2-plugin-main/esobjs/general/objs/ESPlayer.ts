import { AbsolutePlayer, BooleanProperty, DateProperty, ESSceneObject, GroupProperty, NumberProperty, PlayerProperty } from "xbsj-xe2/dist-node/xe2-base-objects";
import { PartialWithUndefinedReactivePropsToNativeProps, ReactivePropsToNativePropsAndChanged, bind, extendClassProps } from "xbsj-xe2/dist-node/xe2-base-utils";
/**
 * 播放器
 * https://c0yh9tnn0na.feishu.cn/docx/HTgcdl6X0oqn3Ixkst7cZcOJnYf
 */
export class ESPlayer extends ESSceneObject {
    static readonly type = this.register('ESPlayer', this, { chsName: 'ESPlayer', tags: ['ESObjects', '_ES_Impl_UE'], description: "ESPlayer" });
    get typeName() { return 'ESPlayer'; }
    override get defaultProps() { return ESPlayer.createDefaultProps(); }
    get json() { return this._innerGetJson() as JsonType; }
    set json(value: JsonType) { this._innerSetJson(value); }

    static override defaults = {
        ...ESSceneObject.defaults,
        currentTime: 0,
        startTime: 0,
        stopTime: 0,
        playing: false,
        loop: false,
        speed: 1,
    };

    private _absolutePlayer = this.disposeVar(new AbsolutePlayer());
    get absolutePlayer() { return this._absolutePlayer; }

    get ratio() { return this._absolutePlayer.ratio; }
    get ratioChanged() { return this._absolutePlayer.ratioChanged; }
    set ratio(value: number) { this._absolutePlayer.ratio = value; }

    constructor(id?: string) {
        super(id);

        const player = this.absolutePlayer;

        this.dispose(bind([player, 'currentTime'], [this, 'currentTime']));
        this.dispose(bind([player, 'startTime'], [this, 'startTime']));
        this.dispose(bind([player, 'stopTime'], [this, 'stopTime']));
        this.dispose(bind([player, 'playing'], [this, 'playing']));
        this.dispose(bind([player, 'loop'], [this, 'loop']));
        this.dispose(bind([player, 'speed'], [this, 'speed']));

        // this.dispose(this.currentTimeChanged.disposableOn(() => {
        //     if (this.currentTime !== undefined) {
        //         esSimTimeRv.value = this.currentTime;
        //     }
        // }));
    }

    override getProperties(language?: string) {
        return [
            ...super.getProperties(language),
            new GroupProperty('通用', '通用', [
                new PlayerProperty('播放器', '播放器', [this, 'playing'], [this, 'ratio'], [this, 'loop']),
                new DateProperty('currentTime', 'currentTime', false, false, [this, 'currentTime']),
                new DateProperty('startTime', 'startTime', false, false, [this, 'startTime']),
                new DateProperty('stopTime', 'stopTime', false, false, [this, 'stopTime']),
                new BooleanProperty('playing', 'playing', false, false, [this, 'playing']),
                new BooleanProperty('loop', 'loop', false, false, [this, 'loop']),
                new NumberProperty('speed', 'speed', false, false, [this, 'speed']),
            ]),
        ];
    }
}

export namespace ESPlayer {
    export const createDefaultProps = () => ({
        ...ESSceneObject.createDefaultProps(),
        currentTime: 0,
        startTime: 0,
        stopTime: 0,
        playing: false,
        loop: false,
        speed: 1,
    });
}
extendClassProps(ESPlayer.prototype, ESPlayer.createDefaultProps);
export interface ESPlayer extends ReactivePropsToNativePropsAndChanged<ReturnType<typeof ESPlayer.createDefaultProps>> { }
type JsonType = PartialWithUndefinedReactivePropsToNativeProps<ReturnType<typeof ESPlayer.createDefaultProps> & { type: string }>;
