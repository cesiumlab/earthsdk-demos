import { GroupProperty, NumberProperty, geoArea, getDistancesFromPositions } from "xbsj-xe2/dist-node/xe2-base-objects";
import { PartialWithUndefinedReactivePropsToNativeProps, ReactivePropsToNativePropsAndChanged, extendClassProps, react } from "xbsj-xe2/dist-node/xe2-base-utils";
import { SceneObjectKey } from "xbsj-xe2/dist-node/xe2-utils";
import { ESFillStyle, ESGeoVector } from "../../base/objs";

/**
 * https://www.wolai.com/earthsdk/6WaSjU4MvPi8CEaanmKDUP
 * https://c0yh9tnn0na.feishu.cn/docx/UOykdV4Xjo0YCixngLTc8btrnSf
 */
export class ESGeoPolygon extends ESGeoVector {
    static readonly type = this.register('ESGeoPolygon', this, { chsName: '地理多边形', tags: ['ESObjects', '_ES_Impl_Cesium', '_ES_Impl_UE'], description: "地理多边形" });
    get typeName() { return 'ESGeoPolygon'; }
    override get defaultProps() { return ESGeoPolygon.createDefaultProps(); }
    get json() { return this._innerGetJson() as JsonType; }
    set json(value: JsonType) { this._innerSetJson(value); }

    private _area = this.disposeVar(react(0));
    get area() { return this._area.value; }
    get areaChanged() { return this._area.changed; }

    private _perimeter = this.disposeVar(react(0));
    get perimeter() { return this._perimeter.value; }
    get perimeterChanged() { return this._perimeter.changed; }

    override getESProperties() {
        const properties = { ...super.getESProperties() };
        return {
            ...properties,
            coordinate: [
                ...properties.coordinate,
                new NumberProperty('面积', '面积 ㎡', false, true, [this, 'area']),
                new NumberProperty('周长', '周长 m', false, true, [this, 'perimeter'])]
        };
    };


    static override defaults = {
        ...ESGeoVector.defaults,
        fillStyle: {
            color: [1, 1, 1, 0.5],
            material: '',
            materialParams: {},
            ground: false,
        } as ESFillStyle,
        filled: true,
        stroked: true,
        strokeStyle: {
            width: 1,
            widthType: 'screen',
            color: [1, 1, 1, 1],
            material: '',
            materialParams: {}
        },
    };

    constructor(id?: SceneObjectKey) {
        super(id);
        this.collision = false;
        this.fillColor = [1, 1, 1, 0.5];
        this.strokeColor = [1, 1, 1, 1];
        this.strokeWidth = 1;

        const update = () => {
            if (this.points && this.points.length >= 3) {
                this._area.value = geoArea(this.points);
                const pos = [...this.points, this.points[0]];
                const distance = getDistancesFromPositions(pos, 'NONE');
                const totalDistance = distance[distance.length - 1];
                this._perimeter.value = totalDistance;
            } else {
                this._area.value = 0;
                this._perimeter.value = 0;
            }
        }
        update();
        this.dispose(this.pointsChanged.disposableOn(update));
    }

    override getProperties(language: string) {
        return [
            ...super.getProperties(language),
            // new GroupProperty('czm', 'czm', [
            //     new BooleanProperty('是否贴地', '是否贴地', false, false, [this, 'ground'])
            // ]),
            new GroupProperty('计算', '计算', [
                new NumberProperty('面积 ㎡', '面积 ㎡', false, true, [this, 'area']),
                new NumberProperty('周长 m', '周长 m', false, true, [this, 'perimeter'])
            ]),
        ];
    }
}

export namespace ESGeoPolygon {
    export const createDefaultProps = () => ({
        ...ESGeoVector.createDefaultProps(),
        // ground: false,
        filled:true,
        stroked:false,
    });
}
extendClassProps(ESGeoPolygon.prototype, ESGeoPolygon.createDefaultProps);
export interface ESGeoPolygon extends ReactivePropsToNativePropsAndChanged<ReturnType<typeof ESGeoPolygon.createDefaultProps>> { }
type JsonType = PartialWithUndefinedReactivePropsToNativeProps<ReturnType<typeof ESGeoPolygon.createDefaultProps> & { type: string }>;
