import { GeoPolygon } from "xbsj-xe2/dist-node/xe2-base-objects";

import { BooleanProperty, ColorProperty, GroupProperty, NumberProperty } from "xbsj-xe2/dist-node/xe2-base-objects";
import { PartialWithUndefinedReactivePropsToNativeProps, ReactivePropsToNativePropsAndChanged, extendClassProps, reactArray } from "xbsj-xe2/dist-node/xe2-base-utils";
import { ESGeoVector } from "../../base/objs";


export class ESDoubleArrow extends ESGeoVector {
    static readonly type = this.register('ESDoubleArrow', this, { chsName: '双箭头', tags: ['ESObjects', '_ES_Impl_Cesium', '_ES_Impl_UE'], description: "双箭头" });
    get typeName() { return 'ESDoubleArrow'; }
    override get defaultProps() { return ESDoubleArrow.createDefaultProps(); }
    get json() { return this._innerGetJson() as JsonType; }
    set json(value: JsonType) { this._innerSetJson(value); }

    constructor(id?: string) {
        super(id);

        {
            this.filled = true;
            this.stroked = true;

            this.strokeStyle = {
                width: 2,
                widthType: 'screen',
                color: [1, 1, 1, 1],
                material: '',
                materialParams: {},
                ground: false,
            }

            this.fillStyle = {
                color: [1, 1, 1, .5],
                material: '',
                materialParams: {},
                ground:false,
            }
        }
    }
    static override defaults = {
        ...ESGeoVector.defaults,
        pointEditing: false,
        ground: false,
        depth: 0,
    }
    override getProperties(language: string) {
        return [
            ...super.getProperties(language),
            new GroupProperty('通用', '通用', [
                new BooleanProperty('单点编辑', '是否开启单点编辑', false, false, [this, 'pointEditing']),
                new BooleanProperty('是否贴地', '是否贴地表绘制', false, false, [this, 'ground']),
                new NumberProperty('厚度', '厚度', false, false, [this, 'depth']),
            ]),
        ];
    }
}

export namespace ESDoubleArrow {
    export const createDefaultProps = () => ({
        ...ESGeoVector.createDefaultProps(),
        ground: false,
        pointEditing: false,
        depth: 0,
    });
}
extendClassProps(ESDoubleArrow.prototype, ESDoubleArrow.createDefaultProps);
export interface ESDoubleArrow extends ReactivePropsToNativePropsAndChanged<ReturnType<typeof ESDoubleArrow.createDefaultProps>> { }
type JsonType = PartialWithUndefinedReactivePropsToNativeProps<ReturnType<typeof ESDoubleArrow.createDefaultProps> & { type: string }>;
