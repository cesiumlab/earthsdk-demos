import { GroupProperty, NumberProperty } from "xbsj-xe2/dist-node/xe2-base-objects";
import { PartialWithUndefinedReactivePropsToNativeProps, ReactivePropsToNativePropsAndChanged, extendClassProps } from "xbsj-xe2/dist-node/xe2-base-utils";
import { ESFillStyle, ESGeoVector } from "../../base/objs";
/**
 * https://c0yh9tnn0na.feishu.cn/docx/OWVHdEumAoXLo6xMb5TcQ5OHnUh
 */
export class ESClassification extends ESGeoVector {
    static readonly type = this.register('ESClassification', this, { chsName: '倾斜单体化', tags: ['ESObjects', '_ES_Impl_Cesium', '_ES_Impl_UE'], description: "平尾箭头" });
    get typeName() { return 'ESClassification'; }
    override get defaultProps() { return ESClassification.createDefaultProps(); }
    get json() { return this._innerGetJson() as JsonType; }
    set json(value: JsonType) { this._innerSetJson(value); }

    constructor(id?: string) {
        super(id);
        this.filled = true;
        this.fillGround = true;
    }
    static override defaults = {
        ...ESGeoVector.defaults,
        fillStyle: {
            color: [1, 1, 1, 1],
            material: '',
            materialParams: {}
        } as ESFillStyle,
        filled: true
    }
    override getESProperties() {
        const properties = { ...super.getESProperties() };
        return {
            ...properties,
            basic: [
                ...properties.basic,
                new NumberProperty('高度', '高度', false, false, [this, 'height'],10),
            ]
        }
    }
    override getProperties(language: string) {
        return [
            ...super.getProperties(language),
            new GroupProperty('通用', '通用', [
                new NumberProperty('高度', '高度', false, false, [this, 'height']),
            ]),
        ];
    }
}

export namespace ESClassification {
    export const createDefaultProps = () => ({
        ...ESGeoVector.createDefaultProps(),
        height: 10,
    });
}
extendClassProps(ESClassification.prototype, ESClassification.createDefaultProps);
export interface ESClassification extends ReactivePropsToNativePropsAndChanged<ReturnType<typeof ESClassification.createDefaultProps>> { }
type JsonType = PartialWithUndefinedReactivePropsToNativeProps<ReturnType<typeof ESClassification.createDefaultProps> & { type: string }>;
