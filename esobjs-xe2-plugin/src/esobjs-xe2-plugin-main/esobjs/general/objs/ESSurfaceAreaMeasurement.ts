import { Event, Listener, PartialWithUndefinedReactivePropsToNativeProps, ReactivePropsToNativePropsAndChanged, extendClassProps } from "xbsj-xe2/dist-node/xe2-base-utils";
import { SceneObjectKey } from "xbsj-xe2/dist-node/xe2-utils";
import { GroupProperty, FunctionProperty, NumberProperty } from "xbsj-xe2/dist-node/xe2-base-objects";
import { ESGeoPolygon } from "./ESGeoPolygon";

/**
 * https://www.wolai.com/earthsdk/nyM7sCD88nKDUquCKf1koA
 * https://c0yh9tnn0na.feishu.cn/docx/OtjmdNHLgoQODUxBeTbcfg7rnhd
 * 表面积测量 未实现
 */

export class ESSurfaceAreaMeasurement extends ESGeoPolygon {
    static override readonly type = this.register('ESSurfaceAreaMeasurement', this, { chsName: '表面积测量', tags: ['ESObjects', '_ES_Impl_Cesium', '_ES_Impl_UE'], description: "表面积测量" });
    override get typeName() { return 'ESSurfaceAreaMeasurement'; }
    override get defaultProps() { return ESSurfaceAreaMeasurement.createDefaultProps(); }
    override get json() { return this._innerGetJson() as JsonType; }
    override set json(value: JsonType) { this._innerSetJson(value); }

    private _startEvent = this.disposeVar(new Event());
    get startEvent(): Listener { return this._startEvent; }
    start() { this._startEvent.emit(); }

    static override defaults = {
        ...ESGeoPolygon.defaults,
        interpolation: 0.5, //插值距离，单位米，为0时不插值
        offsetHeight: 0, //三角面整体偏移高度
    };

    constructor(id?: SceneObjectKey) {
        super(id);
        this.fillStyle.ground = true
    }

    override getProperties(language: string) {
        return [...super.getProperties(language),
        new GroupProperty('ESSurfaceAreaMeasurement', '表面积测量', [
            new FunctionProperty('开始计算', '开始计算', [], () => this.start(), []),
            new NumberProperty('插值距离', '插值距离,单位米,为0时不插值', false, false, [this, 'interpolation'], ESSurfaceAreaMeasurement.defaults.interpolation),
            new NumberProperty('偏移高度', '三角面整体偏移高度，单位米', false, false, [this, 'offsetHeight'], ESSurfaceAreaMeasurement.defaults.offsetHeight),
        ])]
    }
}

export namespace ESSurfaceAreaMeasurement {
    export const createDefaultProps = () => ({
        ...ESGeoPolygon.createDefaultProps(),
        interpolation: undefined as number | undefined,
        offsetHeight: undefined as number | undefined,
    });
}
extendClassProps(ESSurfaceAreaMeasurement.prototype, ESSurfaceAreaMeasurement.createDefaultProps);
export interface ESSurfaceAreaMeasurement extends ReactivePropsToNativePropsAndChanged<ReturnType<typeof ESSurfaceAreaMeasurement.createDefaultProps>> { }
type JsonType = PartialWithUndefinedReactivePropsToNativeProps<ReturnType<typeof ESSurfaceAreaMeasurement.createDefaultProps> & { type: string }>;
