import { GroupProperty, NumberProperty, StringProperty } from "xbsj-xe2/dist-node/xe2-base-objects";
import { ESJResource, ESObjectWithLocation } from "../../base";
import { extendClassProps, PartialWithUndefinedReactivePropsToNativeProps, react, ReactivePropsToNativePropsAndChanged } from "xbsj-xe2/dist-node/xe2-base-utils";
import { SceneObjectKey } from "xbsj-xe2/dist-node/xe2-utils";

export class ESGaussianSplatting extends ESObjectWithLocation {
    static readonly type = this.register('ESGaussianSplatting', this, { chsName: '高斯溅射模型', tags: ['ESObjects', '_ES_Impl_Cesium', '_ES_Impl_UE'], description: '用于加载高斯溅射模型的ES对象' });
    get typeName() { return 'ESGaussianSplatting'; }
    override get defaultProps() { return ESGaussianSplatting.createDefaultProps(); }
    get json() { return this._innerGetJson() as JsonType; }
    set json(value: JsonType) { this._innerSetJson(value); }
    static override defaults = {
        ...ESObjectWithLocation.defaults,
    }
    constructor(id?: SceneObjectKey) {
        super(id);
    }
    override getESProperties() {
        const properties = { ...super.getESProperties() };
        return {
            ...properties,
            basic: [
                ...properties.basic,
                new StringProperty('路径', 'url', true, false, [this, 'url']),
                new NumberProperty('进度', 'progress', false, true, [this, 'progress'], 0),]
        }
    }
    override getProperties(language: string) {
        return [
            ...super.getProperties(language),
            new GroupProperty('通用', '通用', [
                new StringProperty('路径', 'url', true, false, [this, 'url']),
                new NumberProperty('进度', 'progress', false, true, [this, 'progress'], 0),
            ])
        ];
    }
}
export namespace ESGaussianSplatting {
    export const createDefaultProps = () => ({
        ...ESObjectWithLocation.createDefaultProps(),
        url: '' as string | ESJResource,
        progress: 0
    })
}
extendClassProps(ESGaussianSplatting.prototype, ESGaussianSplatting.createDefaultProps);
export interface ESGaussianSplatting extends ReactivePropsToNativePropsAndChanged<ReturnType<typeof ESGaussianSplatting.createDefaultProps>> { }
type JsonType = PartialWithUndefinedReactivePropsToNativeProps<ReturnType<typeof ESGaussianSplatting.createDefaultProps> & { type: string }>;
