import { GroupProperty, NumberProperty } from "xbsj-xe2/dist-node/xe2-base-objects";
import { extendClassProps, PartialWithUndefinedReactivePropsToNativeProps, ReactivePropsToNativePropsAndChanged } from "xbsj-xe2/dist-node/xe2-base-utils";
import { SceneObjectKey } from "xbsj-xe2/dist-node/xe2-utils";
import { ESLocalVector2D } from "../../base/objs";

export class ESLocalRegularPolygon extends ESLocalVector2D {
    static readonly type = this.register('ESLocalRegularPolygon', this, { chsName: '局部坐标多边形', tags: ['ESObjects', '_ES_Impl_Cesium', '_ES_Impl_UE'], description: "ESLocalRegularPolygon" });
    get typeName() { return 'ESLocalRegularPolygon'; }
    override get defaultProps() { return { ...ESLocalRegularPolygon.createDefaultProps() }; }
    get json() { return this._innerGetJson() as JsonType; }
    set json(value: JsonType) { this._innerSetJson(value); }


    constructor(id?: SceneObjectKey) {
        super(id);
    }

    override getProperties(language?: string) {
        return [
            ...super.getProperties(language),
            new GroupProperty('ESLocalRegularPolygon', 'ESLocalRegularPolygon', [
                new NumberProperty('半径', '半径', true, false, [this, 'radius'], 1000000),
                new NumberProperty('part', 'part', true, false, [this, 'part']),
            ]),
        ];
    }
}

export namespace ESLocalRegularPolygon {
    export const createDefaultProps = () => ({
        ...ESLocalVector2D.createDefaultProps(),
        radius: undefined as number | undefined,
        part: undefined as number | undefined,
    });
}
extendClassProps(ESLocalRegularPolygon.prototype, ESLocalRegularPolygon.createDefaultProps);
export interface ESLocalRegularPolygon extends ReactivePropsToNativePropsAndChanged<ReturnType<typeof ESLocalRegularPolygon.createDefaultProps>> { }
type JsonType = PartialWithUndefinedReactivePropsToNativeProps<ReturnType<typeof ESLocalRegularPolygon.createDefaultProps> & { type: string }>;
