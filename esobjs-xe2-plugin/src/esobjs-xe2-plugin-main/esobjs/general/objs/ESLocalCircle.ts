import { GroupProperty, geoPolygonFromCircle, NumberProperty, BooleanProperty, PositionProperty } from "xbsj-xe2/dist-node/xe2-base-objects";
import { extendClassProps, react, PartialWithUndefinedReactivePropsToNativeProps, ReactivePropsToNativePropsAndChanged } from "xbsj-xe2/dist-node/xe2-base-utils";
import { SceneObjectKey } from "xbsj-xe2/dist-node/xe2-utils";
import { ESFillStyle, ESLocalVector2D } from "../../base/objs";
/**
 * https://c0yh9tnn0na.feishu.cn/docx/C6dndwAheoGnemxVeEhcqtm3nCh
 */
export class ESLocalCircle extends ESLocalVector2D {
    static readonly type = this.register('ESLocalCircle', this, { chsName: '局部坐标圆形', tags: ['ESObjects', '_ES_Impl_Cesium', '_ES_Impl_UE'], description: "ESLocalCircle" });
    get typeName() { return 'ESLocalCircle'; }
    override get defaultProps() { return { ...ESLocalCircle.createDefaultProps() }; }
    get json() { return this._innerGetJson() as JsonType; }
    set json(value: JsonType) { this._innerSetJson(value); }

    private _area = this.disposeVar(react(0));
    get area() { return this._area.value; }
    get areaChanged() { return this._area.changed; }

    private _perimeter = this.disposeVar(react(0));
    get perimeter() { return this._perimeter.value; }
    get perimeterChanged() { return this._perimeter.changed; }

    toPolygon(steps: number = 10, units?: string) {
        // TODO(树彭/秦英): vtxf打包通不过
        // @ts-ignore
        const polygon = geoPolygonFromCircle([...this.position], this.radius, steps, units)
        return polygon[0].map(e => { return [...e, this.position ? this.position[2] : 0] as [number, number, number] });
    }


    static override defaults = {
        ...ESLocalVector2D.defaults,
        fillStyle: {
            color: [1, 1, 1, 0.5],
            material: '',
            materialParams: {}
        } as ESFillStyle,
    }

    constructor(id?: SceneObjectKey) {
        super(id);
        this.radius = 1;
        this.filled = true;
        this.stroked = false;
        this.collision = false;
        this.strokeColor = [1, 1, 1, 1];
        this.fillColor = [1, 1, 1, 0.5];

        const update = () => {
            this._area.value = Math.PI * this.radius * this.radius;
            this._perimeter.value = 2 * Math.PI * this.radius;
        }
        update();
        this.dispose(this.radiusChanged.disposableOn(update));
    }
    override getESProperties() {
        const properties = { ...super.getESProperties() };
        return {
            ...properties,
            defaultMenu: 'StyleProprties',
            basic: [
                ...properties.basic,
                new NumberProperty('圆半径', '圆半径', true, false, [this, 'radius'], 1),
            ],
            general: [
                ...properties.general,
            ],
            dataSource: [
                ...properties.dataSource,
            ],
            location: [
                // ...properties.location,
            ],
            coordinate: [
                // ...properties.coordinate,
                new BooleanProperty('是否编辑', '是否编辑', false, false, [this, 'editing']),
                new NumberProperty('面积', '面积', false, true, [this, 'area']),
                new NumberProperty('周长', '周长', false, true, [this, 'perimeter']),
                new PositionProperty('三维坐标', '三维坐标', true, false, [this, 'position'], [0, 0, 0]),
            ],
            style: [
                ...properties.style,
            ],
        }
    }
    override getProperties(language?: string) {
        return [
            ...super.getProperties(language),
            new GroupProperty('ESLocalCircle', 'ESLocalCircle', [
                new NumberProperty('圆半径', '圆半径', false, false, [this, 'radius']),
                new NumberProperty('面积', '面积', false, true, [this, 'area']),
                new NumberProperty('周长', '周长', false, true, [this, 'perimeter'])
            ]),
        ];
    }
}

export namespace ESLocalCircle {
    export const createDefaultProps = () => ({
        ...ESLocalVector2D.createDefaultProps(),
        radius: 1,
    });
}
extendClassProps(ESLocalCircle.prototype, ESLocalCircle.createDefaultProps);
export interface ESLocalCircle extends ReactivePropsToNativePropsAndChanged<ReturnType<typeof ESLocalCircle.createDefaultProps>> { }
type JsonType = PartialWithUndefinedReactivePropsToNativeProps<ReturnType<typeof ESLocalCircle.createDefaultProps> & { type: string }>;
