import { GroupProperty, Number4Property, NumberProperty, FunctionProperty, JsonProperty } from "xbsj-xe2/dist-node/xe2-base-objects";
import { extendClassProps, PartialWithUndefinedReactivePropsToNativeProps, ReactivePropsToNativePropsAndChanged, reactArray } from "xbsj-xe2/dist-node/xe2-base-utils";
import { SceneObject, SceneObjectKey } from "xbsj-xe2/dist-node/xe2-utils";
import { ESVisualObject } from "../../base/objs";
import { ESJResource } from "../../base";

export function getFinalTerrainProviderUrlString(url: string) {
    let finalUrl;
    if (url.startsWith('ion://')) {
        const idStr = url.substring('ion://'.length);
        const id = +idStr;
        if (Number.isFinite(id)) {
            finalUrl = `Cesium.IonResource.fromAssetId(${id})`;
        }
    } else {
        finalUrl = `'${SceneObject.context.getStrFromEnv(url)}'`;
    }

    return finalUrl;
}

export function getCzmCodeFromCzmTerrain(czmTerrain: ESTerrainLayer) {
    if (!czmTerrain.url) return undefined;
    return `
var terrainProvider = new Cesium.CesiumTerrainProvider({
    url: ${getFinalTerrainProviderUrlString(typeof czmTerrain.url == 'object' ? czmTerrain.url.url : czmTerrain.url)},
});
viewer.terrainProvider = terrainProvider;
viewer.camera.flyTo({
    destination: Cesium.Rectangle.fromDegrees(west, south, east, north)
});
    `;
}
/**
 * https://www.wolai.com/earthsdk/mrRQPHx2hww6BgzBwEdPaV
 * https://c0yh9tnn0na.feishu.cn/docx/IVsAdLBbgojisLxBCsccuW6UnOd
 */
export class ESTerrainLayer extends ESVisualObject {
    static readonly type = this.register('ESTerrainLayer', this, { chsName: '地形图层', tags: ['ESObjects', '_ES_Impl_Cesium', '_ES_Impl_UE'], description: "地形图层" });
    get typeName() { return 'ESTerrainLayer'; }
    override get defaultProps() { return ESTerrainLayer.createDefaultProps(); }
    get json() { return this._innerGetJson() as JsonType; }
    set json(value: JsonType) { this._innerSetJson(value); }

    getCzmCode() { return getCzmCodeFromCzmTerrain(this); }

    static override defaults = {
        ...ESVisualObject.defaults,
        show: true,
        url: "http://inner.earthsdk.com/layer.json",
        rectangle: [-180, -90, 180, 90] as [number, number, number, number],
        zIndex: 0,
    }

    constructor(id?: SceneObjectKey) {
        super(id);
    }
    override getESProperties() {
        const properties = { ...super.getESProperties() };
        return {
            defaultMenu: 'DataSourceProprties',
            basic: [
                ...properties.basic,
                new NumberProperty('层级序号', '层级序号', false, false, [this, 'zIndex'], ESTerrainLayer.defaults.zIndex),
            ],
            general: [
                ...properties.general,
            ],
            dataSource: [
                ...properties.dataSource,
                new JsonProperty('地形服务地址', '地形服务地址', false, false, [this, 'url'], ESTerrainLayer.defaults.url),
                new Number4Property('矩形范围', '西南东北', false, false, [this, 'rectangle'], ESTerrainLayer.defaults.rectangle),
            ],
            location: [
                ...properties.location,
            ],
            coordinate: [
                ...properties.coordinate,
            ],
            style: [
                ...properties.style,
            ],
        }
    }
    override getProperties(language?: string) {
        return [
            ...super.getProperties(language),
            new GroupProperty('通用', '通用', [
                new JsonProperty('地形服务地址', '地形服务地址', false, false, [this, 'url']),
                new Number4Property('矩形范围', '西南东北', false, false, [this, 'rectangle']),
                new NumberProperty('层级', 'zIndex', false, false, [this, 'zIndex']),
                new FunctionProperty('获取Cesium代码', '获取Cesium代码', [], () => console.log(this.getCzmCode()), []),
            ]),
        ]
    }
}

export namespace ESTerrainLayer {
    export const createDefaultProps = () => ({
        /**
         * https://www.wolai.com/earthsdk/mrRQPHx2hww6BgzBwEdPaV#YwSV19hmkyuF1sZ2NWjdp
         */
        url: "http://inner.earthsdk.com/layer.json" as string | ESJResource,
        rectangle: reactArray<[number, number, number, number]>([-180, -90, 180, 90]),
        czmMaxzoom: undefined as number | undefined,
        czmMinzoom: undefined as number | undefined,
        zIndex: 0,
        ...ESVisualObject.createDefaultProps(),
    });
}
extendClassProps(ESTerrainLayer.prototype, ESTerrainLayer.createDefaultProps);
export interface ESTerrainLayer extends ReactivePropsToNativePropsAndChanged<ReturnType<typeof ESTerrainLayer.createDefaultProps>> { }
type JsonType = PartialWithUndefinedReactivePropsToNativeProps<ReturnType<typeof ESTerrainLayer.createDefaultProps> & { type: string }>;
