import { SceneObjectKey } from "xbsj-renderer/dist-node/xr-utils";
import { ESGeoPolygon } from "./ESGeoPolygon";
import { BooleanProperty, ColorProperty, EnumProperty, GroupProperty, NumberProperty } from "xbsj-xe2/dist-node/xe2-base-objects";
import { extendClassProps, PartialWithUndefinedReactivePropsToNativeProps, ReactivePropsToNativePropsAndChanged } from "xbsj-renderer/dist-node/xr-base-utils";

/**
 * 测试 挤压多边形体
 * https://c0yh9tnn0na.feishu.cn/docx/JkZedj43YoWWlvx5OakckT6Fndb
 */
export class ESGeoExtrudedPolygon extends ESGeoPolygon {
    static override readonly type = this.register('ESGeoExtrudedPolygon', this, { chsName: '挤压多边形体', tags: ['ESObjects', '_ES_Impl_Cesium', '_ES_Impl_UE'], description: "挤压多边形体" });
    override get typeName() { return 'ESGeoExtrudedPolygon'; }
    override get defaultProps() { return ESGeoExtrudedPolygon.createDefaultProps(); }
    override get json() { return this._innerGetJson() as JsonType; }
    override set json(value: JsonType) { this._innerSetJson(value); }

    static override defaults = {
        ...ESGeoPolygon.defaults,
        height: 0,
        extrudedHeight: 10,
        perPositionHeight: false,
    }

    constructor(id?: SceneObjectKey) {
        super(id);
        this.collision = false;
    }
    override getESProperties() {
        const properties = { ...super.getESProperties() };
        return {
            ...properties,
            coordinate: [
                ...properties.coordinate,
                new NumberProperty('高度', '高度 m', true, false, [this, 'height'], ESGeoExtrudedPolygon.defaults.height),
                new NumberProperty('拉伸高度', '拉伸高度 m', true, false, [this, 'extrudedHeight'], ESGeoExtrudedPolygon.defaults.extrudedHeight),
                new BooleanProperty('应用每个位置高度', '应用每个位置高度', true, false, [this, 'perPositionHeight'], ESGeoExtrudedPolygon.defaults.perPositionHeight)
            ], style: [
                new GroupProperty('点样式', '点样式集合', []),
                new BooleanProperty('开启', '开启点样式', false, false, [this, 'pointed'], false),
                new NumberProperty('点大小', '点大小(pointSize)', false, false, [this, 'pointSize'], 1),
                new EnumProperty('点类型', '点类型(pointSizeType)', false, false, [this, 'pointSizeType'], [['screen', 'screen'], ['world', 'world']], 'screen'),
                new ColorProperty('点颜色', '点颜色(pointColor)', false, false, [this, 'pointColor'], [1, 1, 1, 1]),

                new GroupProperty('线样式', '线样式集合', []),
                new BooleanProperty('开启', '开启线样式', false, false, [this, 'stroked'], false),
                new BooleanProperty('贴地', '是否贴地', false, false, [this, 'strokeGround'], false),
                new NumberProperty('线宽', '线宽(strokeWidth)', false, false, [this, 'strokeWidth'], 1),
                new EnumProperty('线类型', '线类型(strokeWidthType)', false, false, [this, 'strokeWidthType'], [['screen', 'screen'], ['world', 'world']], 'screen'),
                new ColorProperty('线颜色', '线颜色(strokeColor)', false, false, [this, 'strokeColor'], [1, 1, 1, 1]),

                new GroupProperty('面样式', '面样式集合', []),
                new BooleanProperty('开启', '开启填充样式', false, false, [this, 'filled'], true),
                new BooleanProperty('贴地', '是否贴地', false, false, [this, 'fillGround'], false),
                new ColorProperty('填充颜色', '填充颜色(fillColor)', false, false, [this, 'fillColor'], [1, 1, 1, 1]),
            ],
        };
    };
    override getProperties(language: string) {
        return [
            ...super.getProperties(language),
            new GroupProperty('通用', '通用', [
                new NumberProperty('高度', '高度 m', true, false, [this, 'height']),
                new NumberProperty('拉伸高度', '拉伸高度 m', true, false, [this, 'extrudedHeight']),
                new BooleanProperty('应用每个位置高度', '应用每个位置高度', true, false, [this, 'perPositionHeight'], ESGeoExtrudedPolygon.defaults.perPositionHeight)
            ]),
        ];
    }
}

export namespace ESGeoExtrudedPolygon {
    export const createDefaultProps = () => ({
        ...ESGeoPolygon.createDefaultProps(),
        height: 0,
        extrudedHeight: 10,
        perPositionHeight: false,
    });
}
extendClassProps(ESGeoExtrudedPolygon.prototype, ESGeoExtrudedPolygon.createDefaultProps);
export interface ESGeoExtrudedPolygon extends ReactivePropsToNativePropsAndChanged<ReturnType<typeof ESGeoExtrudedPolygon.createDefaultProps>> { }
type JsonType = PartialWithUndefinedReactivePropsToNativeProps<ReturnType<typeof ESGeoExtrudedPolygon.createDefaultProps> & { type: string }>;