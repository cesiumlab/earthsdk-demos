import { PartialWithUndefinedReactivePropsToNativeProps, ReactivePropsToNativePropsAndChanged, extendClassProps } from "xbsj-xe2/dist-node/xe2-base-utils";
import { SceneObjectKey } from "xbsj-xe2/dist-node/xe2-utils";
import { ESGeoVector, ESStrokeStyle } from "../../base/objs";
/**
 * https://www.wolai.com/earthsdk/37rp47JvTtZqEJESf5AuQu
 * https://c0yh9tnn0na.feishu.cn/docx/S3OLd09ixogy1PxmTNmcK1FknGe
 */
export class ESDirectionMeasurement extends ESGeoVector {
    static readonly type = this.register('ESDirectionMeasurement', this, { chsName: '方位角测量', tags: ['ESObjects', '_ES_Impl_Cesium', '_ES_Impl_UE'], description: "方位角测量" });
    get typeName() { return 'ESDirectionMeasurement'; }
    override get defaultProps() { return ESDirectionMeasurement.createDefaultProps(); }
    get json() { return this._innerGetJson() as JsonType; }
    set json(value: JsonType) { this._innerSetJson(value); }

    override get strokeWidth() { return this.strokeStyle.width; }
    override set strokeWidth(value: number) { this.strokeStyle = { ...this.strokeStyle, width: value } }


    constructor(id?: SceneObjectKey) {
        super(id);

        {
            this.stroked = true;
            this.strokeStyle = {
                width: 10,
                widthType: 'screen',
                color: [1, 1, 1, 1],
                material: '',
                materialParams: {},
                ground: false
            }

        }
    }
    static override defaults = {
        ...ESGeoVector.defaults
    };

    override getProperties(language?: string) {
        return [
            ...super.getProperties(language),
        ]
    }
}

export namespace ESDirectionMeasurement {
    export const createDefaultProps = () => ({
        ...ESGeoVector.createDefaultProps(),
    });
}
extendClassProps(ESDirectionMeasurement.prototype, ESDirectionMeasurement.createDefaultProps);
export interface ESDirectionMeasurement extends ReactivePropsToNativePropsAndChanged<ReturnType<typeof ESDirectionMeasurement.createDefaultProps>> { }
type JsonType = PartialWithUndefinedReactivePropsToNativeProps<ReturnType<typeof ESDirectionMeasurement.createDefaultProps> & { type: string }>;
