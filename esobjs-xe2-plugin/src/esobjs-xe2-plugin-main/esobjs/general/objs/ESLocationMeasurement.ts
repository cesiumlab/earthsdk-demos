import { PartialWithUndefinedReactivePropsToNativeProps, ReactivePropsToNativePropsAndChanged, extendClassProps } from "xbsj-xe2/dist-node/xe2-base-utils";
import { SceneObjectKey } from "xbsj-xe2/dist-node/xe2-utils";
import { ESObjectWithLocation } from "../../base/objs";
/**
 * https://www.wolai.com/earthsdk/dQJgf3fj4X1xUwZ38dfiMB
 * https://c0yh9tnn0na.feishu.cn/docx/OkMAdwR9toU1NJxrJiDcK0Lhnbe
 */
export class ESLocationMeasurement extends ESObjectWithLocation {
    static readonly type = this.register('ESLocationMeasurement', this, { chsName: '位置测量点', tags: ['ESObjects', '_ES_Impl_Cesium', '_ES_Impl_UE'], description: "位置测量点" });
    get typeName() { return 'ESLocationMeasurement'; }
    override get defaultProps() { return ESLocationMeasurement.createDefaultProps(); }
    get json() { return this._innerGetJson() as JsonType; }
    set json(value: JsonType) { this._innerSetJson(value); }

    static override defaults = {
        ...ESObjectWithLocation.defaults,
    }
    constructor(id?: SceneObjectKey) {
        super(id);
    }

    override getProperties(language?: string) {
        return [
            ...super.getProperties(language),
        ];
    }
}

export namespace ESLocationMeasurement {
    export const createDefaultProps = () => ({
        ...ESObjectWithLocation.createDefaultProps(),
    });
}
extendClassProps(ESLocationMeasurement.prototype, ESLocationMeasurement.createDefaultProps);
export interface ESLocationMeasurement extends ReactivePropsToNativePropsAndChanged<ReturnType<typeof ESLocationMeasurement.createDefaultProps>> { }
type JsonType = PartialWithUndefinedReactivePropsToNativeProps<ReturnType<typeof ESLocationMeasurement.createDefaultProps> & { type: string }>;
