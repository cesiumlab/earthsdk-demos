import { GroupProperty, NumberProperty, UriProperty, StringProperty, BooleanProperty, PickedInfo, ESSceneObject, PositionProperty, SceneObjectPickedInfo } from "xbsj-xe2/dist-node/xe2-base-objects";
import { extendClassProps, PartialWithUndefinedReactivePropsToNativeProps, reactArray, ReactivePropsToNativePropsAndChanged, Event, Listener, track, bind } from "xbsj-xe2/dist-node/xe2-base-utils";
import { SceneObjectKey } from "xbsj-xe2/dist-node/xe2-utils";
import { ESGltfModel } from "./ESGltfModel";
import { ESTextLabel } from './ESTextLabel';

export class ESModelWithLabelForTest extends ESSceneObject {
    static readonly type = this.register('ESModelWithLabelForTest', this, { chsName: '模型带标签(仅测试)', tags: ['ESObjects'], description: "模型带标签(仅测试)" });
    get typeName() { return 'ESModelWithLabelForTest'; }
    override get defaultProps() { return ESModelWithLabelForTest.createDefaultProps(); }
    get json() { return this._innerGetJson() as JsonType; }
    set json(value: JsonType) { this._innerSetJson(value); }

    private _pickedEvent = this.disposeVar(new Event<[PickedInfo]>());
    get pickedEvent() { return this._pickedEvent; }

    private _flyToEvent = this.disposeVar(new Event<[number | undefined]>());
    get flyToEvent(): Listener<[number | undefined]> { return this._flyToEvent; }
    flyTo(duration?: number) { this._flyToEvent.emit(duration); }

    constructor(id?: SceneObjectKey) {
        super(id);

        const eSGltfModel = this.disposeVar(new ESGltfModel());
        const eSTextLabel = this.disposeVar(new ESTextLabel());
        this.dispose(this.components.disposableAdd(eSGltfModel));
        this.dispose(this.components.disposableAdd(eSTextLabel));

        this.dispose(bind([eSGltfModel, 'position'], [this, 'position']));
        this.dispose(track([eSGltfModel, 'url'], [this, 'url']));
        
        {
            const update = () => {
                if (!this.position) {
                    eSTextLabel.position = [0, 0, 0];
                    return;
                }
    
                const [l, b, h] = this.position;
                eSTextLabel.position = [l, b, h + (this.labelHeight ?? 0)];
            }
            update();
            this.dispose(this.labelHeightChanged.disposableOn(update));
            this.dispose(this.positionChanged.disposableOn(update));
        }

        this.dispose(track([eSTextLabel, 'text'], [this, 'labelText']));

        {
            this.dispose(this._flyToEvent.disposableOn(duration => {
                eSGltfModel.flyTo(duration);
            }));
        }

        {
            this.dispose(eSGltfModel.pickedEvent.disposableOn(pickedInfo => {
                if (this.allowPicking ?? false) {
                    this._pickedEvent.emit(new SceneObjectPickedInfo(this, pickedInfo));
                }
            }));
            this.dispose(eSTextLabel.pickedEvent.disposableOn(pickedInfo => {
                if (this.allowPicking ?? false) {
                    this._pickedEvent.emit(new SceneObjectPickedInfo(this, pickedInfo));
                }
            }));
        }

        {
            this.dispose(bind([eSGltfModel, 'editing'], [this, 'positionEditing']));
        }

        {
            this.dispose(track([eSGltfModel, 'allowPicking'], [this, 'allowPicking']));
            this.dispose(track([eSTextLabel, 'allowPicking'], [this, 'allowPicking']));
        }
    }

    static override defaults = {
        ...ESSceneObject.defaults,
        url:'${esobjs-xe2-plugin-assets-script-dir}/xe2-assets/esobjs-xe2-plugin/glbs/Cesium_Air.glb',
        labelText:'请输入文字'
    };
    override getProperties(language?: string) {
        return [
            ...super.getProperties(language),
            new GroupProperty('通用', '通用', [
                new UriProperty("模型路径", "模型路径地址", false, false, [this, 'url']),
                new PositionProperty('模型位置', '模型位置', false, false, [this, 'position']),
                new NumberProperty('标签高度', '标签高度', false, false, [this, 'labelHeight']),
                new StringProperty('标签内容', '标签内容', false, false, [this, 'labelText'],ESModelWithLabelForTest.defaults.labelText),
                new BooleanProperty('是否允许拾取', '是否允许拾取', false, false, [this, 'allowPicking']),
                new BooleanProperty('位置编辑', '位置编辑', false, false, [this, 'positionEditing']),
            ]),
        ];
    }
}

export namespace ESModelWithLabelForTest {
    export const createDefaultProps = () => ({
        ...ESSceneObject.createDefaultProps(),
        position: reactArray<[number, number, number]>([0, 0, 0]),
        url: '${esobjs-xe2-plugin-assets-script-dir}/xe2-assets/esobjs-xe2-plugin/glbs/Cesium_Air.glb',
        labelHeight: 0,
        labelText: undefined as string | undefined,
        positionEditing: false,
        allowPicking: true,
    });
}
extendClassProps(ESModelWithLabelForTest.prototype, ESModelWithLabelForTest.createDefaultProps);
export interface ESModelWithLabelForTest extends ReactivePropsToNativePropsAndChanged<ReturnType<typeof ESModelWithLabelForTest.createDefaultProps>> { }
type JsonType = PartialWithUndefinedReactivePropsToNativeProps<ReturnType<typeof ESModelWithLabelForTest.createDefaultProps> & { type: string }>;

///////////////////////////////////////////////////////////////////////////////////////////
// 以下为上述代码的JS形式

// var { GroupProperty, NumberProperty, UriProperty, StringProperty, BooleanProperty, PickedInfo, ESSceneObject, PositionProperty, SceneObjectPickedInfo } = czmtoy['scene-manager'];
// var { extendClassProps, PartialWithUndefinedReactivePropsToNativeProps, reactArrayWithUndefined, ReactivePropsToNativePropsAndChanged, Event, Listener, track, bind } = xbsj['xr-base-utils'];
// var { ESGltfModel, ESTextLabel } = XE2['xe2-objects'];

// class ESModelWithLabelForTest2 extends ESSceneObject {
//     static type = this.register('ESModelWithLabelForTest2', this, { 
//         chsName: '模型', 
//         tags: ['MyObjects'], 
//         description: "通过Cesium的Entity API 实现的模型。" 
//     });
//     get typeName() { return 'ESModelWithLabelForTest2'; }
//     get json() { return this._innerGetJson(); }
//     set json(value) { this._innerSetJson(value); }
    
//     get defaultProps() { return ESModelWithLabelForTest2.createDefaultProps(); }

//     _flyToEvent = this.disposeVar(new Event());
//     get flyToEvent() { return this._flyToEvent; }
//     flyTo(duration) { this._flyToEvent.emit(duration); }

//     _pickedEvent = this.disposeVar(new Event());
//     get pickedEvent() { return this._pickedEvent; }

//     constructor(id) {
//         super(id);

//         const eSGltfModel = this.disposeVar(new ESGltfModel());
//         const eSTextLabel = this.disposeVar(new ESTextLabel());
        
//         this.dispose(this.components.disposableAdd(eSGltfModel));
//         this.dispose(this.components.disposableAdd(eSTextLabel));

//         this.dispose(bind([eSGltfModel, 'position'], [this, 'position']));
//         this.dispose(track([eSGltfModel, 'url'], [this, 'url']));
        
//         {
//             const update = () => {
//                 if (!this.position) {
//                     eSTextLabel.position = undefined;
//                     return;
//                 }
    
//                 const [l, b, h] = this.position;
//                 eSTextLabel.position = [l, b, h + (this.labelHeight ?? 0)];
//             }
//             update();
//             this.dispose(this.labelHeightChanged.disposableOn(update));
//             this.dispose(this.positionChanged.disposableOn(update));
//         }

//         this.dispose(track([eSTextLabel, 'text'], [this, 'labelText']));

//         this.dispose(this._flyToEvent.disposableOn(duration => {
//             eSGltfModel.flyTo(duration);
//         }));

//         {
//             this.dispose(eSGltfModel.pickedEvent.disposableOn(pickedInfo => {
//                 this._pickedEvent.emit(new SceneObjectPickedInfo(this, pickedInfo));
//             }));
//             this.dispose(eSTextLabel.pickedEvent.disposableOn(pickedInfo => {
//                 this._pickedEvent.emit(new SceneObjectPickedInfo(this, pickedInfo));
//             }));
//         }

//         {
//             this.dispose(bind([eSGltfModel, 'editing'], [this, 'positionEditing']));
//         }

//         {
//             this.dispose(track([eSGltfModel, 'allowPicking'], [this, 'allowPicking']));
//             this.dispose(track([eSTextLabel, 'allowPicking'], [this, 'allowPicking']));
//         }
//     }

//     getProperties(language) {
//         return [
//             ...super.getProperties(language),
//             new GroupProperty('通用', '通用', [
//                 new UriProperty("模型路径", "模型路径地址", true, false, [this, 'url'], '${esobjs-xe2-plugin-assets-script-dir}/xe2-assets/esobjs-xe2-plugin/glbs/Cesium_Air.glb'),
//                 new PositionProperty('模型位置', '模型位置', true, false, [this, 'position']),
//                 new NumberProperty('标签高度', '标签高度', true, false, [this, 'labelHeight'], 0),
//                 new StringProperty('标签内容', '标签内容', true, false, [this, 'labelText']),
//                 new BooleanProperty('是否允许拾取', '是否允许拾取', true, false, [this, 'allowPicking']),
//                 new BooleanProperty('位置编辑', '位置编辑', true, false, [this, 'positionEditing']),
//             ]),
//         ];
//     }
// }

// ESModelWithLabelForTest2.createDefaultProps = () => ({
//     ...ESSceneObject.createDefaultProps(),
//     position: reactArrayWithUndefined(undefined),
//     url: undefined,
//     labelHeight: undefined,
//     labelText: undefined,
//     positionEditing: undefined,
//     allowPicking: undefined,
// });
// extendClassProps(ESModelWithLabelForTest2.prototype, ESModelWithLabelForTest2.createDefaultProps);
