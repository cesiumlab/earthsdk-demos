import { BooleanProperty, ColorProperty, EnumProperty, GroupProperty, NumberProperty } from "xbsj-xe2/dist-node/xe2-base-objects";
import { extendClassProps, PartialWithUndefinedReactivePropsToNativeProps, ReactivePropsToNativePropsAndChanged } from "xbsj-xe2/dist-node/xe2-base-utils";
import { SceneObjectKey } from "xbsj-xe2/dist-node/xe2-utils";
import { ESFillStyle, ESGeoVector, ESStrokeStyle } from "../../base/objs";

/**
 * https://www.wolai.com/earthsdk/KumomxD1tKHbq242aFVwz
 * https://c0yh9tnn0na.feishu.cn/docx/DEh6dHacOoc94Kx1Pg4covs9nwd
 */
export class ESPipeFence extends ESGeoVector {
    static readonly type = this.register('ESPipeFence', this, { chsName: '管道电子围栏', tags: ['ESObjects', '_ES_Impl_Cesium', '_ES_Impl_UE'], description: "管道电子围栏" });
    get typeName() { return 'ESPipeFence'; }
    override get defaultProps() { return ESPipeFence.createDefaultProps(); }
    get json() { return this._innerGetJson() as JsonType; }
    set json(value: JsonType) { this._innerSetJson(value); }

    static override defaults = {
        ...ESGeoVector.defaults,
        strokeStyle: {
            width: 1,
            widthType: 'screen',
            color: [1, 1, 1, 1],
            material: '',
            materialParams: {}
        } as ESStrokeStyle,
        fillStyle: {
            color: [1, 1, 1, 1],
            material: '',
            materialParams: {}
        } as ESFillStyle,
        filled: true,
        stroked: true,
        materialModes: [["单箭头", 'singleArrow'], ["多箭头", "multipleArrows"]] as [name: string, value: string][],

    }
    override  _deprecated = [
        {
            "materialMode": {
                "blue": "multipleArrows",
                "purple": "singleArrow",
            }
        },
        "show"
    ];
    private _deprecatedWarningFunc = (() => { this._deprecatedWarning(); })();
    constructor(id?: SceneObjectKey) {
        super(id);
        this.fillColor = [1, 0, 0.73, 1]
    }
    override getESProperties() {
        const properties = { ...super.getESProperties() };
        return {
            ...properties,
            defaultMenu: 'BasicProprties',
            basic: [
                ...properties.basic,
                new NumberProperty('高度', 'height', false, false, [this, 'height'], 10),
                new NumberProperty('宽度', 'width', false, false, [this, 'width'], 10),
                new EnumProperty('模式', 'materialMode', false, false, [this, 'materialMode'], ESPipeFence.defaults.materialModes, 'purple'),
            ],
            style: [
                new GroupProperty('点样式', '点样式集合', []),
                new BooleanProperty('开启', '开启点样式', false, false, [this, 'pointed'], false),
                new NumberProperty('点大小', '点大小(pointSize)', false, false, [this, 'pointSize'], 1),
                new EnumProperty('点类型', '点类型(pointSizeType)', false, false, [this, 'pointSizeType'], [['screen', 'screen'], ['world', 'world']], 'screen'),
                new ColorProperty('点颜色', '点颜色(pointColor)', false, false, [this, 'pointColor'], [1, 1, 1, 1]),
                new GroupProperty('线样式', '线样式集合', []),
                new BooleanProperty('开启线样式', '开启线样式', false, false, [this, 'stroked'], false),
                new NumberProperty('线宽', '线宽(strokeWidth)', false, false, [this, 'strokeWidth'], 1),
                new EnumProperty('线类型', '线类型(strokeWidthType)', false, false, [this, 'strokeWidthType'], [['screen', 'screen'], ['world', 'world']], 'screen'),
                new ColorProperty('线颜色', '线颜色(strokeColor)', false, false, [this, 'strokeColor'], [1, 1, 1, 1]),
                new BooleanProperty('是否贴地', '是否贴地(线)', false, false, [this, 'strokeGround'], false),
                new GroupProperty('面样式', '面样式集合', []),
                new BooleanProperty('开启', '开启填充样式', false, false, [this, 'filled'], true),
                new ColorProperty('填充颜色', '填充颜色(fillColor)', false, false, [this, 'fillColor'], [1, 1, 1, 1]),
                new BooleanProperty('是否贴地', '是否贴地', false, false, [this, 'fillGround'], false),
            ],
        }
    }
    override getProperties(language?: string) {
        return [
            ...super.getProperties(language),
            new GroupProperty('通用', '通用', [
                new NumberProperty('高度', 'height', false, false, [this, 'height']),
                new NumberProperty('宽度', 'width', false, false, [this, 'width']),
                new EnumProperty('materialMode', 'materialMode', false, false, [this, 'materialMode'], ESPipeFence.defaults.materialModes),

            ]),
        ]
    }
}

export namespace ESPipeFence {
    export const createDefaultProps = () => ({
        ...ESGeoVector.createDefaultProps(),
        height: 10,
        width: 10,
        materialMode: 'singleArrow',
        filled: true,
    });
}
extendClassProps(ESPipeFence.prototype, ESPipeFence.createDefaultProps);
export interface ESPipeFence extends ReactivePropsToNativePropsAndChanged<ReturnType<typeof ESPipeFence.createDefaultProps>> { }
type JsonType = PartialWithUndefinedReactivePropsToNativeProps<ReturnType<typeof ESPipeFence.createDefaultProps> & { type: string }>;
