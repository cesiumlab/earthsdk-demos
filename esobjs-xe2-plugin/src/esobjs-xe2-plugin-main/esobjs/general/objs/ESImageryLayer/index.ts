import { CzmTilingSchemaJsonType, EnumProperty, FunctionProperty, GroupProperty, JsonProperty, Number4Property, NumberProperty, NumberSliderProperty, StringProperty, StringsProperty, UriProperty } from "xbsj-xe2/dist-node/xe2-base-objects";
import { PartialWithUndefinedReactivePropsToNativeProps, ReactivePropsToNativePropsAndChanged, extendClassProps, reactJson, reactArray, reactArrayWithUndefined, reactJsonWithUndefined } from "xbsj-xe2/dist-node/xe2-base-utils";
import { SceneObject, SceneObjectKey } from "xbsj-xe2/dist-node/xe2-utils";
import { ESJResource, tilingSchemaJsonMd } from "../../../base";
import { ESVisualObject } from "../../../base/objs";
import { ESImageryLayerOptionsType, optionsStr } from "./Type";
import { opacity } from "html2canvas/dist/types/css/property-descriptors/opacity";
export type CzmSplitDirectionType = 'LEFT' | 'NONE' | 'RIGHT';
// export type czmTilingSchemeType = 'WebMercatorTilingScheme' | 'GeographicTilingScheme' | 'ToGCJ02WebMercatorTilingScheme' | 'ToWGS84WebMercatorTilingScheme';

export function getCzmCodeFromESImageryLayer(imagery: ESImageryLayer) {
    if (!imagery.url) return undefined;
    const finalUrl = SceneObject.context.getStrFromEnv(typeof imagery.url == 'string' ? imagery.url : imagery.url.url);

    const configs: string[] = [];

    configs.push(`url: '${finalUrl}'`);
    const providerName = finalUrl.endsWith('.xml') ? 'TileMapServiceImageryProvider' : 'UrlTemplateImageryProvider';

    const czmCode = `\
var imageryProvider = new Cesium.${providerName}({
${configs.map(e => `    ${e}`).join(', \n')}
});
viewer.imageryLayers.addImageryProvider(imageryProvider);
viewer.camera.flyTo({
    destination: Cesium.Rectangle.fromDegrees(west, south, east, north)
});
\
`;
    return czmCode;
}

/**
 * https://www.wolai.com/earthsdk/sTpXjiETeVPfEwGfqDqUUw
 * https://c0yh9tnn0na.feishu.cn/docx/LbsQdHzkroksZjxNPvHcECOBnTg
 */
export class ESImageryLayer extends ESVisualObject {
    static readonly type = this.register('ESImageryLayer', this, { chsName: '影像图层', tags: ['ESObjects', '_ES_Impl_Cesium', '_ES_Impl_UE'], description: "影像图层" });
    get typeName() { return 'ESImageryLayer'; }
    override get defaultProps() { return ESImageryLayer.createDefaultProps(); }
    get json() { return this._innerGetJson() as JsonType; }
    set json(value: JsonType) { this._innerSetJson(value); }

    getCzmCode() { return getCzmCodeFromESImageryLayer(this); }

    static override defaults = {
        ...ESVisualObject.defaults,
        show: true,
        url: "",
        rectangle: [-180.0000000, -90.0000000, 180.0000000, 90.0000000] as [number, number, number, number],
        options: {},
        zIndex: 0,
        actorTag: "",
        componentTag: "",
        minimumLevel: 0,
        maximumLevel: 22,
        targetID: "",
        opacity: 1,

        czmSplitDirection: 'NONE' as CzmSplitDirectionType,
        czmAlpha: 1.0,
        czmBrightness: 1.0,
        czmContrast: 1.0,
        czmHue: 0.0,
        czmSaturation: 1.0,
        czmGamma: 1.0,
        czmSubdomains: []

    }
    constructor(id?: SceneObjectKey) {
        super(id);
    }

    override getESProperties() {
        const properties = { ...super.getESProperties() };
        return {
            ...properties,
            defaultMenu: 'DataSourceProprties',
            basic: [
                ...properties.basic,
                new NumberProperty('层级序号', '层级序号', false, false, [this, 'zIndex'], ESImageryLayer.defaults.zIndex),
            ],
            dataSource: [
                ...properties.dataSource,
                new JsonProperty('影像服务地址', '影像服务地址', false, false, [this, 'url'], ESImageryLayer.defaults.url),
                new Number4Property('矩形范围', '西南东北', true, false, [this, 'rectangle'], ESImageryLayer.defaults.rectangle),
                new NumberSliderProperty('最大级别', '最大级别.', true, false, [this, 'maximumLevel'], 1, [1, 24], ESImageryLayer.defaults.maximumLevel),
                new JsonProperty('options', 'options', true, false, [this, 'options'], ESImageryLayer.defaults.options),
                new StringProperty('targetID', 'targetID', false, false, [this, 'targetID'], ''),
                new NumberSliderProperty('opacity', 'opacity', false, false, [this, 'opacity'], 0.01, [0, 1], 1),
            ],
        }
    }

    override getProperties(language?: string) {
        return [
            ...super.getProperties(language),
            new GroupProperty('通用', '通用', [
                new JsonProperty('影像服务地址', '影像服务地址', false, false, [this, 'url']),
                new GroupProperty('通用', '通用', [
                    new Number4Property('矩形范围', '西南东北', true, false, [this, 'rectangle'], ESImageryLayer.defaults.rectangle),
                    new NumberProperty('高度', 'A numeric Property specifying the height.', false, false, [this, 'height']),
                    new NumberProperty('zIndex', '层级', false, false, [this, 'zIndex']),
                    new StringProperty('actorTag', 'actorTag', false, false, [this, 'actorTag']),
                    new StringProperty('componentTag', 'componentTag', false, false, [this, 'componentTag']),
                    new NumberProperty('minimumLevel', 'minimumLevel', false, false, [this, 'minimumLevel']),
                    new NumberProperty('maximumLevel', 'maximumLevel', false, false, [this, 'maximumLevel']),
                    new JsonProperty('options', 'options', true, false, [this, 'options'], {}, optionsStr),
                    new StringProperty('targetID', 'targetID', false, false, [this, 'targetID']),
                    new NumberProperty('opacity', 'opacity', false, false, [this, 'opacity']),
                    new FunctionProperty('获取Cesium代码', '获取Cesium代码', [], () => console.log(this.getCzmCode()), []),
                ]),
                new GroupProperty('czm', 'czm', [
                    new EnumProperty('czmSplitDirection', 'The ImagerySplitDirection split to apply to this layer.', false, false, [this, 'czmSplitDirection'], [['LEFT', 'LEFT'], ['NONE', 'NONE'], ['RIGHT', 'RIGHT']]),
                    new NumberProperty('透明度', '透明度,The alpha blending value of this layer, from 0.0 to 1.0.', false, false, [this, 'czmAlpha']),
                    new NumberProperty('亮度', '亮度,The brightness of this layer. 1.0 uses the unmodified imagery color.', false, false, [this, 'czmBrightness']),
                    new NumberProperty('对比度', '对比度,The contrast of this layer. 1.0 uses the unmodified imagery color.', false, false, [this, 'czmContrast']),
                    new NumberProperty('色相', '色相,The hue of this layer. 0.0 uses the unmodified imagery color. ', false, false, [this, 'czmHue']),
                    new NumberProperty('饱和度', '饱和度,The saturation of this layer. 1.0 uses the unmodified imagery color. ', false, false, [this, 'czmSaturation']),
                    new NumberProperty('伽马值', '伽马,The gamma correction to apply to this layer. 1.0 uses the unmodified imagery color.', false, false, [this, 'czmGamma']),
                ])
            ]),
        ]
    }
}

export namespace ESImageryLayer {
    export const createDefaultProps = () => ({
        /**
         * https://www.wolai.com/earthsdk/sTpXjiETeVPfEwGfqDqUUw#2d5sKtFaVmPHVoLcmDaxK6
         */
        url: "" as string | ESJResource,
        rectangle: reactJsonWithUndefined<[number, number, number, number]>(undefined),
        options: reactJsonWithUndefined<ESImageryLayerOptionsType>(undefined),
        zIndex: 0,
        actorTag: "",
        componentTag: "",
        maximumLevel: 22,
        minimumLevel: 0,
        targetID: "" as string | undefined,
        opacity: 1,

        czmSplitDirection: 'NONE',
        czmAlpha: 1.0, //	Number | function	1.0	optionalThe alpha blending value of this layer, from 0.0 to 1.0. This can either be a simple number or a function with the signature function(frameState, layer, x, y, level). The function is passed the current frame state, this layer, and the x, y, and level coordinates of the imagery tile for which the alpha is required, and it is expected to return the alpha value to use for the tile.
        czmBrightness: 1.0, //	Number | function	1.0	optionalThe brightness of this layer. 1.0 uses the unmodified imagery color. Less than 1.0 makes the imagery darker while greater than 1.0 makes it brighter. This can either be a simple number or a function with the signature function(frameState, layer, x, y, level). The function is passed the current frame state, this layer, and the x, y, and level coordinates of the imagery tile for which the brightness is required, and it is expected to return the brightness value to use for the tile. The function is executed for every frame and for every tile, so it must be fast.
        czmContrast: 1.0, //	Number | function	1.0	optionalThe contrast of this layer. 1.0 uses the unmodified imagery color. Less than 1.0 reduces the contrast while greater than 1.0 increases it. This can either be a simple number or a function with the signature function(frameState, layer, x, y, level). The function is passed the current frame state, this layer, and the x, y, and level coordinates of the imagery tile for which the contrast is required, and it is expected to return the contrast value to use for the tile. The function is executed for every frame and for every tile, so it must be fast.
        czmHue: 0.0, //	Number | function	0.0	optionalThe hue of this layer. 0.0 uses the unmodified imagery color. This can either be a simple number or a function with the signature function(frameState, layer, x, y, level). The function is passed the current frame state, this layer, and the x, y, and level coordinates of the imagery tile for which the hue is required, and it is expected to return the contrast value to use for the tile. The function is executed for every frame and for every tile, so it must be fast.
        czmSaturation: 1.0, //	Number | function	1.0	optionalThe saturation of this layer. 1.0 uses the unmodified imagery color. Less than 1.0 reduces the saturation while greater than 1.0 increases it. This can either be a simple number or a function with the signature function(frameState, layer, x, y, level). The function is passed the current frame state, this layer, and the x, y, and level coordinates of the imagery tile for which the saturation is required, and it is expected to return the contrast value to use for the tile. The function is executed for every frame and for every tile, so it must be fast.
        czmGamma: 1.0, //	Number | function	1.0	optionalThe gamma correction to apply to this layer. 1.0 uses the unmodified imagery color. This can either be a simple number or a function with the signature function(frameState, layer, x, y, level). The function is passed the current frame state, this layer, and the x, y, and level coordinates of the imagery tile for which the gamma is required, and it is expected to return the gamma value to use for the tile. The function is executed for every frame and for every tile, so it must be fast.

        ...ESVisualObject.createDefaultProps(),
    });
}
extendClassProps(ESImageryLayer.prototype, ESImageryLayer.createDefaultProps);
export interface ESImageryLayer extends ReactivePropsToNativePropsAndChanged<ReturnType<typeof ESImageryLayer.createDefaultProps>> { }
type JsonType = PartialWithUndefinedReactivePropsToNativeProps<ReturnType<typeof ESImageryLayer.createDefaultProps> & { type: string }>;
