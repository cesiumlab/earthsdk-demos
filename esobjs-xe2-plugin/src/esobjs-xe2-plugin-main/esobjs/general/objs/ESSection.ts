import { GroupProperty, NumberProperty, FunctionProperty, PositionProperty, JsonProperty, } from "xbsj-xe2/dist-node/xe2-base-objects";
import { PartialWithUndefinedReactivePropsToNativeProps, ReactivePropsToNativePropsAndChanged, extendClassProps, reactJsonWithUndefined } from "xbsj-xe2/dist-node/xe2-base-utils";
import { SceneObjectKey } from "xbsj-xe2/dist-node/xe2-utils";
import { ESObjectWithLocation } from "../../base/objs";

type CzmClippingPlaneJsonType = {
    normal: [number, number, number];
    distance: number;
};
type NativeNumber16Type = [number, number, number, number, number, number, number, number, number, number, number, number, number, number, number, number];
type CzmClippingPlaneCollectionJsonType = {
    planes?: CzmClippingPlaneJsonType[];
    enabled?: boolean; // true
    modelMatrix?: NativeNumber16Type; // Matrix4.IDENTITY
    unionClippingRegions?: boolean; // false 
    edgeColor?: [number, number, number, number]; // Color.White
    edgeWidth?: number; // 0
};
const defaultClippingPlanes = {
    "planes": [{ normal: [1, 0, 0], distance: 0 }],
    "enabled": true,
    "modelMatrix": [1, 0, 0, 0, 0, 1, 0, 0, 0, 0, 1, 0, 0, 0, 0, 1],
    "unionClippingRegions": false,
    "edgeColor": [1, 1, 1, 1],
    "edgeWidth": 2
};
const clippingPlanesMd = `
## 类型
\`\`\`
export type CzmClippingPlaneCollectionJsonType = {
    planes?: CzmClippingPlaneJsonType[];
    enabled?: boolean; // true
    modelMatrix?: NativeNumber16Type; // Matrix4.IDENTITY
    unionClippingRegions?: boolean; // false 
    edgeColor?: [number, number, number, number]; // Color.White
    edgeWidth?: number; // 0
};
\`\`\`
## 示例
\`\`\`
${JSON.stringify(defaultClippingPlanes, undefined, '    ')}
\`\`\`
`;
export class ESSection extends ESObjectWithLocation {
    static readonly type = this.register('ESSection', this, { chsName: '剖切', tags: ['ESObjects', '_ES_Impl_Cesium', '_ES_Impl_UE'], description: "信号传输器" });
    get typeName() { return 'ESSection'; }
    override get defaultProps() { return ESSection.createDefaultProps(); }
    get json() { return this._innerGetJson() as JsonType; }
    set json(value: JsonType) { this._innerSetJson(value); }
    constructor(id?: SceneObjectKey) {
        super(id);
    }
    static override defaults = {
        ...ESObjectWithLocation.defaults,
        pixelSize: 200,
        clippingPlanes: defaultClippingPlanes,
    }
    override getProperties(language?: string) {
        return [
            ...super.getProperties(language),
            new GroupProperty('通用', '通用', [
                new FunctionProperty("飞入", "飞入", ['number'], (duration: number) => this.flyTo(duration), [1000]),
                new NumberProperty('pixelSize', "pixelSize", false, false, [this, 'pixelSize']),
                new PositionProperty('本地偏移', '本地偏移，不是经纬度！', false, false, [this, 'localPosition']),
                new JsonProperty('clippingPlanes', 'The ClippingPlaneCollection used to selectively disable rendering the tileset.', true, false, [this, 'clippingPlanes'], ESSection.defaults.clippingPlanes, clippingPlanesMd),
            ]),
        ]
    }
}
export namespace ESSection {
    export const createDefaultProps = () => ({
        ...ESObjectWithLocation.createDefaultProps(),
        pixelSize: 200,
        localPosition: [-0.5, 0.5, 0],
        clippingPlanes: reactJsonWithUndefined<CzmClippingPlaneCollectionJsonType>(undefined),
    });
}
extendClassProps(ESSection.prototype, ESSection.createDefaultProps);
export interface ESSection extends ReactivePropsToNativePropsAndChanged<ReturnType<typeof ESSection.createDefaultProps>> { }
type JsonType = PartialWithUndefinedReactivePropsToNativeProps<ReturnType<typeof ESSection.createDefaultProps> & { type: string }>;
