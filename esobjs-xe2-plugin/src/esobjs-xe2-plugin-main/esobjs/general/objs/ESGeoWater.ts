import { GroupProperty, NumberProperty, Property, ColorProperty, EnumProperty, NumberSliderProperty, StringProperty, JsonProperty } from 'xbsj-xe2/dist-node/xe2-base-objects';
import { PartialWithUndefinedReactivePropsToNativeProps, ReactivePropsToNativePropsAndChanged, extendClassProps, reactJsonWithUndefined } from 'xbsj-xe2/dist-node/xe2-base-utils';
import { SceneObjectKey } from 'xbsj-xe2/dist-node/xe2-utils';
import { ESGeoPolygon } from './ESGeoPolygon';
import { ESColor } from '../../base';
/**
 * https://www.wolai.com/earthsdk/jRv9H5BbPGUaJ8MwxPf5oF
 * https://c0yh9tnn0na.feishu.cn/docx/K7WHdcVp3owapcxiCH7cMJ4Xnrc
 */
export class ESGeoWater extends ESGeoPolygon {
    static override readonly type = this.register('ESGeoWater', this, { chsName: '地理水面', tags: ['ESObjects', '_ES_Impl_Cesium', '_ES_Impl_UE'], description: '地理动态水面' });
    override get typeName() { return 'ESGeoWater'; }
    override get defaultProps() { return ESGeoWater.createDefaultProps(); }
    override get json() { return this._innerGetJson() as JsonType; }
    override set json(value: JsonType) { this._innerSetJson(value); }

    /**
     * @description 默认属性
     * baseWaterColor: [0.1497, 0.165, 0.0031, 1] 水的底色
     * frequency: 1000 频率：控制波数的数值,单位 次/千米
     * waveVelocity: 0.5 波动速率：控制水波纹扰动的速率
     * amplitude: 0.1 振幅：控制水波振幅的数值
     * specularIntensity: 0.8 镜面反射强度：控制镜面反射强度的数值
     * waterType:river 水域类型，当为custom是其他控制效果的参数生效，否则不生效，使用对应水域类型的预定效果
     * flowDirection: 0 流动方向：控制水流方向，局部坐标方向
     * flowSpeed: 0 流动速度：控制水流速度，单位 米/秒
     */
    static override defaults = {
        ...ESGeoPolygon.defaults,
        // 属性的类型若存在undefined的情况，这里配置为undefined时应该使用的默认值
        waterColor: [0.1497, 0.165, 0.0031, 0.8] as ESColor,
        frequency: 1000,
        waveVelocity: 0.5,
        amplitude: 0.1,
        specularIntensity: 0.8,
        waterTypes: [["river", "river"], ["ocean", "ocean"], ["lake", "lake"], ["custom", "custom"]] as [name: string, value: string][],
        waterType: "river",
        flowDirection: 0,
        flowSpeed: 0,
        waterImage: undefined
    }

    constructor(id?: SceneObjectKey) {
        super(id);
    }

    override getESProperties(): { coordinate: Property[]; style: Property[]; basic: Property[]; general: Property[]; dataSource: Property[]; location: Property[]; defaultMenu: string } {
        const properties = { ...super.getESProperties() };
        return {
            ...properties,
            defaultMenu: 'BasicProprties',
            basic: [
                ...properties.basic,
                new EnumProperty('水域类型', '当为custom是其他控制效果的参数生效，否则不生效，使用对应水域类型的预定效果', false, false, [this, 'waterType'], ESGeoWater.defaults.waterTypes, ESGeoWater.defaults.waterType),
                new NumberProperty('频率', '控制波数的数值(次/千米)', false, false, [this, 'frequency'], ESGeoWater.defaults.frequency),
                new NumberProperty('水流速度', '控制水流速度，单位 米/秒', false, false, [this, 'flowSpeed'], ESGeoWater.defaults.flowSpeed),
                new NumberSliderProperty('振幅', '控制水波振幅的数值', false, false, [this, 'amplitude'], 0.01, [0, 1], ESGeoWater.defaults.amplitude),
                new NumberSliderProperty('流向', '控制水流方向，局部坐标方向', false, false, [this, 'flowDirection'], 1, [0, 360], ESGeoWater.defaults.flowDirection),
                new NumberSliderProperty('波动频率', '控制水波纹扰动的速率', false, false, [this, 'waveVelocity'], 0.01, [0, 1], ESGeoWater.defaults.waveVelocity),
                new NumberSliderProperty('镜面反射强度', '控制镜面反射强度的数值', false, false, [this, 'specularIntensity'], 0.01, [0, 1], ESGeoWater.defaults.specularIntensity),
                new ColorProperty('水的底色', 'waterColor', false, false, [this, 'waterColor'], ESGeoWater.defaults.waterColor),
                new JsonProperty('水的图片', 'waterImage', false, false, [this, 'waterImage'], ESGeoWater.defaults.waterImage),
            ]
        }
    }

    override getProperties(language: string) {
        return [
            ...super.getProperties(language),
            // 属性UI配置
            new GroupProperty('通用', '通用', [
                new EnumProperty('水域类型', '当为custom是其他控制效果的参数生效，否则不生效，使用对应水域类型的预定效果', false, false, [this, 'waterType'], ESGeoWater.defaults.waterTypes),
                new JsonProperty('水的图片', 'waterImage', false, false, [this, 'waterImage'], ESGeoWater.defaults.waterImage),
                new ColorProperty('水的底色', 'waterColor', false, false, [this, 'waterColor'], ESGeoWater.defaults.waterColor),
                new NumberProperty('频率', '控制波数的数值(次/千米)', false, false, [this, 'frequency'], ESGeoWater.defaults.frequency),
                new NumberSliderProperty('波动频率', '控制水波纹扰动的速率', false, false, [this, 'waveVelocity'], 0.01, [0, 1], ESGeoWater.defaults.waveVelocity),
                new NumberSliderProperty('振幅', '控制水波振幅的数值', false, false, [this, 'amplitude'], 0.01, [0, 1], ESGeoWater.defaults.amplitude),
                new NumberSliderProperty('镜面反射强度', '控制镜面反射强度的数值', false, false, [this, 'specularIntensity'], 0.01, [0, 1], ESGeoWater.defaults.specularIntensity),
                new NumberSliderProperty('流向', '控制水流方向，局部坐标方向', false, false, [this, 'flowDirection'], 1, [0, 360], ESGeoWater.defaults.flowDirection),
                new NumberProperty('水流速度', '控制水流速度，单位 米/秒', false, false, [this, 'flowSpeed'], ESGeoWater.defaults.flowSpeed),
            ]),
        ];
    }
}

export namespace ESGeoWater {
    export const createDefaultProps = () => ({
        ...ESGeoPolygon.createDefaultProps(),
        // 属性配置
        waterImage: reactJsonWithUndefined<{ [xx: string]: any }>(undefined),
        waterColor: [0.1497, 0.165, 0.0031, 0.8] as ESColor,
        frequency: 1000,
        waveVelocity: 0.5,
        amplitude: 0.1,
        specularIntensity: 0.8,
        waterType: "river",
        flowDirection: 0,
        flowSpeed: 0,
        allowPicking: true
    });
}
extendClassProps(ESGeoWater.prototype, ESGeoWater.createDefaultProps);
export interface ESGeoWater extends ReactivePropsToNativePropsAndChanged<ReturnType<typeof ESGeoWater.createDefaultProps>> { }
type JsonType = PartialWithUndefinedReactivePropsToNativeProps<ReturnType<typeof ESGeoWater.createDefaultProps> & { type: string }>;
