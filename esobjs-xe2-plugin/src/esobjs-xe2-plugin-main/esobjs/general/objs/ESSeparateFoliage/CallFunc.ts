import { UeViewer } from "xbsj-xe2/dist-node/xe2-ue-objects";
import { ESTreeParam, ToCutDownScaleType, ToScaleType } from "../ESSeparateFoliage";

export type TreesFuncsType = {
    AddTrees: {
        params: {
            id: string,
            TreeParams: ESTreeParam[]
        },
        result: {
            error: string | undefined;
        }
    },

    RemoveAllTrees: {
        params: {
            id: string,
        },
        result: {
            error: string | undefined;
        }
    },
    UpdateTreeParams: {
        params: {
            id: string,
            TreeParams: ESTreeParam[]
        },
        result: {
            error: string | undefined;
        }
    },
    CutDownTrees: {
        params: {
            id: string,
            TreeIds: ToCutDownScaleType[],
            TimeLength: number
        },
        result: {
            error: string | undefined;
        }
    },
    GrowthSimulation: {
        params: {
            id: string,
            ToParams: ToScaleType[],
            TimeLength: number,
            SwitchTime: number
        },
        result: {
            error: string | undefined;
        }
    }

}

const addTreesCallFunc = async (ueViewer: UeViewer, id: string, trees: ESTreeParam[]) => {
    const { viewer } = ueViewer;
    if (!viewer) {
        console.error(`AddTrees: ueViewer.viewer is undefined`);
        return undefined;
    }
    const res = await viewer.callUeFunc<TreesFuncsType['AddTrees']['result']>({
        // @ts-ignore // TODO(ysp)
        f: 'AddTrees',
        p: { id, TreeParams: trees }
    })
    if (res.error) console.error(`AddTrees:`, res.error);
    return res;
}
const removeAllTreesCallFunc = async (ueViewer: UeViewer, id: string) => {
    const { viewer } = ueViewer;
    if (!viewer) {
        console.error(`RemoveAllTrees: ueViewer.viewer is undefined`);
        return undefined;
    }
    const res = await viewer.callUeFunc<TreesFuncsType['RemoveAllTrees']['result']>({
        // @ts-ignore // TODO(ysp)
        f: 'RemoveAllTrees',
        p: { id }
    })
    if (res.error) console.error(`RemoveAllTrees:`, res.error);
    return res;
}
const updateTreeParamsCallFunc = async (ueViewer: UeViewer, id: string, trees: ESTreeParam[]) => {
    const { viewer } = ueViewer;
    if (!viewer) {
        console.error(`UpdateTreeParams: ueViewer.viewer is undefined`);
        return undefined;
    }
    const res = await viewer.callUeFunc<TreesFuncsType['UpdateTreeParams']['result']>({
        // @ts-ignore // TODO(ysp)
        f: 'UpdateTreeParams',
        p: { id, TreeParams: trees }
    })
    if (res.error) console.error(`UpdateTreeParams:`, res.error);
    return res;
}
const cutDownTreesCallFunc = async (ueViewer: UeViewer, id: string, TreeIds: ToCutDownScaleType[], TimeLength: number) => {
    const { viewer } = ueViewer;
    if (!viewer) {
        console.error(`CutDownTrees: ueViewer.viewer is undefined`);
        return undefined;
    }
    const res = await viewer.callUeFunc<TreesFuncsType['CutDownTrees']['result']>({
        // @ts-ignore // TODO(ysp)
        f: 'CutDownTrees',
        p: { id, TreeIds, TimeLength }
    })
    if (res.error) console.error(`CutDownTrees:`, res.error);
    return res;
}
const growthSimulationCallFunc = async (ueViewer: UeViewer, id: string, ToParams: ToScaleType[], TimeLength: number, SwitchTime: number) => {
    const { viewer } = ueViewer;
    if (!viewer) {
        console.error(`GrowthSimulation: ueViewer.viewer is undefined`);
        return undefined;
    }
    const res = await viewer.callUeFunc<TreesFuncsType['GrowthSimulation']['result']>({
        // @ts-ignore // TODO(ysp)
        f: 'GrowthSimulation',
        p: { id, ToParams, SwitchTime, TimeLength }
    })
    if (res.error) console.error(`GrowthSimulation:`, res.error);
    return res;
}

export { addTreesCallFunc, removeAllTreesCallFunc, updateTreeParamsCallFunc, cutDownTreesCallFunc, growthSimulationCallFunc }
