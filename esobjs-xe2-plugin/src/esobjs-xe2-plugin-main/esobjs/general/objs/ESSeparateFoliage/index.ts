
import { GroupProperty, JsonProperty, NumberProperty } from "xbsj-xe2/dist-node/xe2-base-objects";
import { Event, PartialWithUndefinedReactivePropsToNativeProps, ReactivePropsToNativePropsAndChanged, extendClassProps, reactJsonWithUndefined } from "xbsj-xe2/dist-node/xe2-base-utils";
import { SceneObjectKey } from "xbsj-xe2/dist-node/xe2-utils";
import { ESVisualObject } from "../../../base/objs";
import { UeViewer } from "xbsj-xe2/dist-node/xe2-ue-objects";
import { addTreesCallFunc, cutDownTreesCallFunc, growthSimulationCallFunc, removeAllTreesCallFunc, updateTreeParamsCallFunc } from "./CallFunc";
export type ESTreeTypeSep = { name: string, meshPath: string }
export type ESTreeParam = { id: string, treeTypeId: number, location: [number, number, number], scale: [number, number, number] }

export type ToScaleType = { [key: string]: [[number, number, number], [number, number, number], [number, number, number], number] }

export type ToCutDownScaleType = { [key: string]: [[number, number, number], number] }

export class ESSeparateFoliage extends ESVisualObject {
    static readonly type = this.register('ESSeparateFoliage', this, { chsName: '单体控制森林', tags: ['ESObjects', '_ES_Impl_UE'], description: "ESSeparateFoliage" });
    get typeName() { return 'ESSeparateFoliage'; }
    override get defaultProps() { return ESSeparateFoliage.createDefaultProps(); }
    get json() { return this._innerGetJson() as JsonType; }
    set json(value: JsonType) { this._innerSetJson(value); }

    async addTreesCallFunc(ueViewer: UeViewer, trees: ESTreeParam[]) {
        return await addTreesCallFunc(ueViewer, this.id, trees)
    }
    async updateTreeParamsCallFunc(ueViewer: UeViewer, trees: ESTreeParam[]) {
        return await updateTreeParamsCallFunc(ueViewer, this.id, trees)
    }
    async cutDownTreesCallFunc(ueViewer: UeViewer, TreeIds: ToCutDownScaleType[], TimeLength: number) {
        return await cutDownTreesCallFunc(ueViewer, this.id, TreeIds, TimeLength)
    }
    async removeAllTreesCallFunc(ueViewer: UeViewer) {
        return await removeAllTreesCallFunc(ueViewer, this.id)
    }
    async growthSimulationCallFunc(ueViewer: UeViewer, ToParams: ToScaleType[], TimeLength: number, SwitchTime: number) {
        return await growthSimulationCallFunc(ueViewer, this.id, ToParams, TimeLength, SwitchTime)
    }
    async getIdByComponentNameAndHitItem(viewer: UeViewer, ComponentName: string, HitItem: number) {
        return await viewer.getIdByComponentNameAndHitItem(this.id, ComponentName, HitItem);
    }

    private _addTreesEvent = this.disposeVar(new Event<[TreeParams: ESTreeParam[]]>());
    get addTreesEvent() { return this._addTreesEvent; }
    addTrees(TreeParams: ESTreeParam[]) { this._addTreesEvent.emit(TreeParams); }

    private _removeAllTreesEvent = this.disposeVar(new Event());
    get removeAllTreesEvent() { return this._removeAllTreesEvent; }
    removeAllTrees() { this._removeAllTreesEvent.emit(); }

    private _updateTreeParamsEvent = this.disposeVar(new Event<[TreeParams: ESTreeParam[]]>());
    get updateTreeParamsEvent() { return this._updateTreeParamsEvent; }
    updateTreeParams(TreeParams: ESTreeParam[]) { this._updateTreeParamsEvent.emit(TreeParams); }

    private _cutDownTreesEvent = this.disposeVar(new Event<[TreeId: ToCutDownScaleType[], TimeLength: number]>());
    get cutDownTreesEvent() { return this._cutDownTreesEvent; }
    cutDownTrees(TreeId: ToCutDownScaleType[], TimeLength: number) { this._cutDownTreesEvent.emit(TreeId, TimeLength); }

    private _growthSimulationEvent = this.disposeVar(new Event<[ToParams: ToScaleType[], TimeLength: number, SwitchTime: number]>());
    get growthSimulationEvent() { return this._growthSimulationEvent; }
    growthSimulation(ToParams: ToScaleType[], TimeLength: number, SwitchTime: number) { this._growthSimulationEvent.emit(ToParams, TimeLength, SwitchTime); }

    static override defaults = {
        ...ESVisualObject.defaults,
        treeTypes: [] as ESTreeTypeSep[],
        stumpId: -1,//树桩id
        intervalTime: 0.1,//动画间隔时间
        switchIntervalTime: 0.5,//切换间隔时间
    }
    constructor(id?: SceneObjectKey) {
        super(id);
    }
    override getProperties(language?: string) {
        return [
            ...super.getProperties(language),
            new GroupProperty('通用', '通用', [
                new JsonProperty('treeTypes', '类型为 { name: string, meshPath: string }[]', true, false, [this, 'treeTypes'], [], '{ name: string, meshPath: string }[]'),
                new NumberProperty('树桩id', '树桩id', false, false, [this, 'stumpId']),
                new NumberProperty('动画间隔时间', '动画间隔时间', false, false, [this, 'intervalTime']),
                new NumberProperty('切换间隔时间', '切换间隔时间', false, false, [this, 'switchIntervalTime']),
            ]),
        ];
    }
}

export namespace ESSeparateFoliage {
    export const createDefaultProps = () => ({
        ...ESVisualObject.createDefaultProps(),
        treeTypes: reactJsonWithUndefined<ESTreeTypeSep[]>(undefined),
        stumpId: -1,
        intervalTime: 0.1,
        switchIntervalTime: 0.5,
    });
}
extendClassProps(ESSeparateFoliage.prototype, ESSeparateFoliage.createDefaultProps);
export interface ESSeparateFoliage extends ReactivePropsToNativePropsAndChanged<ReturnType<typeof ESSeparateFoliage.createDefaultProps>> { }
type JsonType = PartialWithUndefinedReactivePropsToNativeProps<ReturnType<typeof ESSeparateFoliage.createDefaultProps> & { type: string }>;
