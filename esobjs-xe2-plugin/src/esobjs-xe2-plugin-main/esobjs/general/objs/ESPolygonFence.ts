import { EnumProperty, GroupProperty, NumberProperty, geoArea, getDistancesFromPositions } from "xbsj-xe2/dist-node/xe2-base-objects";
import { PartialWithUndefinedReactivePropsToNativeProps, ReactivePropsToNativePropsAndChanged, extendClassProps, react } from "xbsj-xe2/dist-node/xe2-base-utils";
import { SceneObjectKey } from "xbsj-xe2/dist-node/xe2-utils";
import { ESFillStyle, ESGeoVector } from "../../base/objs";

/**
 * https://www.wolai.com/earthsdk/s3LNcfsVWEUHvchR6eeJSH
 * https://c0yh9tnn0na.feishu.cn/docx/ZMedd9HmUoxcTHxia7Gccvr3nUb
 */
export class ESPolygonFence extends ESGeoVector {
    static readonly type = this.register('ESPolygonFence', this, { chsName: '多边形电子围栏', tags: ['ESObjects', '_ES_Impl_Cesium', '_ES_Impl_UE'], description: "多边形电子围栏" });
    get typeName() { return 'ESPolygonFence'; }
    override get defaultProps() { return ESPolygonFence.createDefaultProps(); }
    get json() { return this._innerGetJson() as JsonType; }
    set json(value: JsonType) { this._innerSetJson(value); }

    static override defaults = {
        ...ESGeoVector.defaults,
        fillStyle: {
            color: [1, 1, 1, 1],
            material: '',
            materialParams: {}
        } as ESFillStyle,
        filled: true,
        // height: 10,
        // mode: 'danger',
        materialModes: [["模式一", 'danger'], ["模式二", "checkerboard"], ["模式三", "warning"], ["模式四", "cord"], ["模式五", "scanline"], ["模式六", "honeycomb"], ["模式七", "gradientColor"]] as [name: string, value: string][],
        // czmTextureSize: [1, 1] as [number, number],
    };

    private _area = this.disposeVar(react(0));
    get area() { return this._area.value; }
    get areaChanged() { return this._area.changed; }

    private _perimeter = this.disposeVar(react(0));
    get perimeter() { return this._perimeter.value; }
    get perimeterChanged() { return this._perimeter.changed; }

    constructor(id?: SceneObjectKey) {
        super(id);
        this.filled = true;
        this.collision = false;
        const update = () => {
            if (this.points && this.points.length >= 3) {
                this._area.value = geoArea(this.points);
                const pos = [...this.points, this.points[0]];
                const distance = getDistancesFromPositions(pos, 'NONE');
                const totalDistance = distance[distance.length - 1];
                this._perimeter.value = totalDistance;
            } else {
                this._area.value = 0;
                this._perimeter.value = 0;
            }
        }
        update();
        this.dispose(this.pointsChanged.disposableOn(update));
    }
    override getESProperties() {
        const properties = { ...super.getESProperties() };
        return {
            ...properties,
            defaultMenu:'BasicProprties',
            basic: [
                ...properties.basic,
                new NumberProperty('高度', 'height', false, false, [this, 'height'], 10),
                new EnumProperty('模式', 'materialMode', false, false, [this, 'materialMode'], ESPolygonFence.defaults.materialModes, 'danger'),
                // new NumberProperty('面积', '面积', false, true, [this, 'area']),
                // new NumberProperty('周长', '周长', false, true, [this, 'perimeter'])
            ],
            coordinate: [
                ...properties.coordinate,
                new NumberProperty('面积', '面积', false, true, [this, 'area']),
                new NumberProperty('周长', '周长', false, true, [this, 'perimeter'])
            ]
        }
    }
    override getProperties(language?: string) {
        return [
            ...super.getProperties(language),
            new GroupProperty('通用', '通用', [
                new NumberProperty('高度', 'height', false, false, [this, 'height']),
                new EnumProperty('materialMode', 'materialMode', false, false, [this, 'materialMode'], ESPolygonFence.defaults.materialModes),
            ]),
            new GroupProperty('计算', '计算', [
                new NumberProperty('面积', '面积', false, true, [this, 'area']),
                new NumberProperty('周长', '周长', false, true, [this, 'perimeter'])
            ]),
        ]
    }
}

export namespace ESPolygonFence {
    export const createDefaultProps = () => ({
        pointEditing: false,
        height: 10,
        materialMode: 'danger',
        ...ESGeoVector.createDefaultProps(),
    });
}
extendClassProps(ESPolygonFence.prototype, ESPolygonFence.createDefaultProps);
export interface ESPolygonFence extends ReactivePropsToNativePropsAndChanged<ReturnType<typeof ESPolygonFence.createDefaultProps>> { }
type JsonType = PartialWithUndefinedReactivePropsToNativeProps<ReturnType<typeof ESPolygonFence.createDefaultProps> & { type: string }>;
