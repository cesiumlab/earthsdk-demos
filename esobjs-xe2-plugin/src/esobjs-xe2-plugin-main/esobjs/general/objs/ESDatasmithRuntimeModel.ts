import { GroupProperty, JsonProperty, NumberProperty, StringProperty } from "xbsj-xe2/dist-node/xe2-base-objects";
import { JsonValue, PartialWithUndefinedReactivePropsToNativeProps, ReactivePropsToNativePropsAndChanged, extendClassProps, reactJsonWithUndefined } from "xbsj-xe2/dist-node/xe2-base-utils";
import { SceneObjectKey } from "xbsj-xe2/dist-node/xe2-utils";
import { ESObjectWithLocation } from "../../base/objs";
import { ESJResource } from "../../base";

/**
 * https://c0yh9tnn0na.feishu.cn/docx/L6QGdPWJQoYtWtxJBOEc3CeBnXd
 */
export class ESDatasmithRuntimeModel extends ESObjectWithLocation {
    static readonly type = this.register('ESDatasmithRuntimeModel', this, { chsName: 'Datasmith Model', tags: ['ESObjects', '_ES_Impl_Cesium', '_ES_Impl_UE'], description: "Datasmith Model" });
    get typeName() { return 'ESDatasmithRuntimeModel'; }
    override get defaultProps() { return ESDatasmithRuntimeModel.createDefaultProps(); }
    get json() { return this._innerGetJson() as JsonType; }
    set json(value: JsonType) { this._innerSetJson(value); }

    static override defaults = {
        ...ESObjectWithLocation.defaults,
        url: '',
        importOptions: {
            buildCollisions: "QueryAndPhysics",
            buildHierarchy: "Simplified",
            collisionType: "CTF_UseComplexAsSimple",
            bImportMetaData: true
        },
    };
    constructor(id?: SceneObjectKey) {
        super(id);
    }
    override getESProperties() {
        const properties = { ...super.getESProperties() };
        return {
            ...properties,
            basic: [
                ...properties.basic,
                new NumberProperty('下载进度', '下载进度', true, true, [this, 'downloadProgress']),
                new JsonProperty('importOptions', '导入参数', true, false, [this, 'importOptions'], ESDatasmithRuntimeModel.defaults.importOptions),
            ],
            dataSource: [
                ...properties.dataSource,
                new JsonProperty('路径', 'url', false, false, [this, 'url']),
            ],

        }
    }
    override getProperties(language?: string) {
        return [
            ...super.getProperties(language),
            new GroupProperty('通用', '通用', [
                new JsonProperty('url', 'url', false, false, [this, 'url']),
                new NumberProperty('下载进度', '下载进度', true, true, [this, 'downloadProgress']),
                new JsonProperty('importOptions', '导入参数', true, false, [this, 'importOptions'], ESDatasmithRuntimeModel.defaults.importOptions),
            ]),
        ];
    }
}

export namespace ESDatasmithRuntimeModel {
    export const createDefaultProps = () => ({
        ...ESObjectWithLocation.createDefaultProps(),
        url: '' as string | ESJResource,
        importOptions: reactJsonWithUndefined<JsonValue | undefined>(undefined),
        downloadProgress: 0
    });
}
extendClassProps(ESDatasmithRuntimeModel.prototype, ESDatasmithRuntimeModel.createDefaultProps);
export interface ESDatasmithRuntimeModel extends ReactivePropsToNativePropsAndChanged<ReturnType<typeof ESDatasmithRuntimeModel.createDefaultProps>> { }
type JsonType = PartialWithUndefinedReactivePropsToNativeProps<ReturnType<typeof ESDatasmithRuntimeModel.createDefaultProps> & { type: string }>;
