import { GroupProperty, Number2sProperty } from "xbsj-xe2/dist-node/xe2-base-objects";
import { extendClassProps, PartialWithUndefinedReactivePropsToNativeProps, ReactivePropsToNativePropsAndChanged } from "xbsj-xe2/dist-node/xe2-base-utils";
import { SceneObjectKey } from "xbsj-xe2/dist-node/xe2-utils";
import { ESLocalVector2D } from "../../base/objs";
import { reactPosition2Ds } from "../../utils";
/**
 * https://c0yh9tnn0na.feishu.cn/docx/WIBGdyBNMo5vhdxCMRScLZgsnDh
 */
export class ESLocalPolygon extends ESLocalVector2D {
    static readonly type = this.register('ESLocalPolygon', this, { chsName: '局部2D坐标多边形', tags: ['ESObjects', '_ES_Impl_Cesium', '_ES_Impl_UE'], description: "ESLocalPolygon" });
    get typeName() { return 'ESLocalPolygon'; }
    override get defaultProps() { return { ...ESLocalPolygon.createDefaultProps() }; }
    get json() { return this._innerGetJson() as JsonType; }
    set json(value: JsonType) { this._innerSetJson(value); }

    static override defaults = {
        ...ESLocalVector2D.defaults,
        points: [],
        filled: true,
    }

    constructor(id?: SceneObjectKey) {
        super(id);
        this.filled = true;
    }
    override getESProperties() {
        const properties = { ...super.getESProperties() };
        return {
            ...properties,
            coordinate: [
                ...properties.coordinate,
                new Number2sProperty('坐标', '偏移量[x,y],单位米,不含高度', false, false, [this, 'points'],[]),
            ],
        }
    }
    override getProperties(language?: string) {
        return [
            ...super.getProperties(language),
            new GroupProperty('ESLocalPolygon', 'ESLocalPolygon', [
                new Number2sProperty('位置偏移数组', '偏移量[x,y],单位米,不含高度', false, false, [this, 'points']),
            ]),
        ];
    }
}

export namespace ESLocalPolygon {
    export const createDefaultProps = () => ({
        ...ESLocalVector2D.createDefaultProps(),
        points: reactPosition2Ds(undefined),
    });
}
extendClassProps(ESLocalPolygon.prototype, ESLocalPolygon.createDefaultProps);
export interface ESLocalPolygon extends ReactivePropsToNativePropsAndChanged<ReturnType<typeof ESLocalPolygon.createDefaultProps>> { }
type JsonType = PartialWithUndefinedReactivePropsToNativeProps<ReturnType<typeof ESLocalPolygon.createDefaultProps> & { type: string }>;
