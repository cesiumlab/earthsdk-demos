import { SceneObject } from "xbsj-renderer/dist-node/xr-utils";
import { ES3DTileset } from "./index";
import { ESJResource } from "../.././../base";

export type FeatureColorJsonType = {
    value?: string | number,
    minValue?: number,
    maxValue?: number,
    rgba: [number, number, number, number]
}
export type FeatureVisableJsonType = {
    value?: string | number,
    minValue?: number,
    maxValue?: number,
    visable: boolean
}

export function getFinalCzm3DTilesUrlString(czm3DTilesUrl: string | ESJResource) {
    let finalUrl;
    let url = typeof czm3DTilesUrl == 'string' ? czm3DTilesUrl : czm3DTilesUrl.url;
    do {
        if (!url.startsWith('Ion(')) break;

        let rr = /Ion\((\d+)/.exec(url);
        if (!rr) break;

        let assetId = rr[1] && +rr[1];
        try {
            assetId && (finalUrl = `"await Cesium.IonResource.fromAssetId(${assetId}))"`);
        } catch (error) {
            console.error(`Ion资源未能获取到 error: ${error}`, error);
            break;
        }
    } while (false);

    finalUrl = finalUrl || `"${SceneObject.context.getStrFromEnv(url)}"`;

    return finalUrl;
}

export function getCzmCodeFromES3DTileset(eS3DTileset: ES3DTileset) {
    if (!eS3DTileset.url) return undefined;
    const finalUrl = getFinalCzm3DTilesUrlString(eS3DTileset.url);
    return `\
(async function () {
    var tileset = await Cesium.Cesium3DTileset.fromUrl(
        ${finalUrl}
    );
    viewer.scene.primitives.add(tileset);
    viewer.flyTo(tileset);
    return tileset;
})();
`;
}



export type ESJNumConditionItem = {
    field: string,
    op: "==" | "!=" | ">" | ">=" | "<" | "<=",
    value: number
}

export type ESJStrConditionItem = {
    field: string,
    op: "==" | "!=" | "contain" | "empty",
    value: string
}

export type ESJConditionItem = ESJNumConditionItem | ESJStrConditionItem | boolean;

export type ESJFeatureStyleConditionItemType = {
    condition: ESJConditionItem | ESJConditionItem[],
    color?: [number, number, number, number],
    show?: boolean
}
export type ESJStyleConditionItemType = {
    condition: ESJConditionItem
    color: [number, number, number, number],
    show: boolean
}


export type ESJFeatureStyleType = ESJFeatureStyleConditionItemType[];
