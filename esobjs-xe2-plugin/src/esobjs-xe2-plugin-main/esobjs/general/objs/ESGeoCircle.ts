import { BooleanProperty, ColorProperty, GroupProperty, NumberProperty } from "xbsj-xe2/dist-node/xe2-base-objects";
import { PartialWithUndefinedReactivePropsToNativeProps, ReactivePropsToNativePropsAndChanged, extendClassProps, reactArray } from "xbsj-xe2/dist-node/xe2-base-utils";
import { ESGeoVector } from "../../base/objs";

/**
 * @deprecated 目前已废弃
 */
export class ESGeoCircle extends ESGeoVector {
    static readonly type = this.register('ESGeoCircle', this, { chsName: '圆形', tags: ['ESObjects', '_ES_Impl_Cesium', '_ES_Impl_UE'], description: "圆形" });
    get typeName() { return 'ESGeoCircle'; }
    override get defaultProps() { return ESGeoCircle.createDefaultProps(); }
    get json() { return this._innerGetJson() as JsonType; }
    set json(value: JsonType) { this._innerSetJson(value); }

    constructor(id?: string) {
        super(id);
    }
    static override defaults = {
        ...ESGeoVector.defaults,
        pointEditing: false,
        ground: false,
        outline: true,
        outlineColor: [1, 1, 1, 1] as [number, number, number, number],
        outlineWidth: 2,
        fill: true,
        color: [1, 1, 1, .5] as [number, number, number, number],
        depth: 0,
    }
    override getProperties(language: string) {
        return [
            ...super.getProperties(language),
            new GroupProperty('通用', '通用', [
                new BooleanProperty('单点编辑', '是否开启单点编辑', false, false, [this, 'pointEditing']),
                new BooleanProperty('是否贴地', '是否贴地表绘制', false, false, [this, 'ground']),
                new BooleanProperty('是否显示轮廓线', '是否显示轮廓线.', false, false, [this, 'outline']),
                new ColorProperty('轮廓线颜色', '轮廓线颜色', false, false, [this, 'outlineColor']),
                new NumberProperty('轮廓线宽度', '轮廓线宽度', false, false, [this, 'outlineWidth']),
                new BooleanProperty('是否填充', '是否填充', false, false, [this, 'fill']),
                new ColorProperty('填充颜色', '填充颜色', false, false, [this, 'color']),
                new NumberProperty('厚度', '厚度', false, false, [this, 'depth']),
            ]),
        ];
    }
}

export namespace ESGeoCircle {
    export const createDefaultProps = () => ({
        ...ESGeoVector.createDefaultProps(),
        fill: true,
        ground: false,
        outline: true,
        outlineColor: reactArray<[number, number, number, number]>([1, 1, 1, 1]),
        outlineWidth:2,
        color: reactArray<[number, number, number, number]>([1, 1, 1, .5]),
        pointEditing: false,
        depth:0,
    });
}
extendClassProps(ESGeoCircle.prototype, ESGeoCircle.createDefaultProps);
export interface ESGeoCircle extends ReactivePropsToNativePropsAndChanged<ReturnType<typeof ESGeoCircle.createDefaultProps>> { }
type JsonType = PartialWithUndefinedReactivePropsToNativeProps<ReturnType<typeof ESGeoCircle.createDefaultProps> & { type: string }>;
