import { Destroyable, createNextAnimateFrameEvent, nextAnimateFrame } from "xbsj-xe2/dist-node/xe2-base-utils";
import { ESCameraView } from "../ESCameraView";
import { ESCameraViewCollection } from "./index";

export type ViewInfo = {
    viewDistance?: number | undefined,
    duration?: number | undefined,
    position: [number, number, number],
    rotation?: [number, number, number] | undefined,
    thumbnail?: string | undefined,
    name?: string | undefined
}

export type ViewOption = {
    size: [number, number],
    name: string
}



export class ViewWrapper extends Destroyable {
    private _view = this.disposeVar(new ESCameraView());
    get view() { return this._view; }

    get viewInfo() {
        const e = this._view;
        return {
            duration: e.duration,
            position: e.position,
            rotation: e.rotation,
            thumbnail: e.thumbnail,
            name: e.name,
        } as ViewInfo;
    }

    constructor(sceneObject: ESCameraViewCollection, viewInfo?: ViewInfo, option?: ViewOption) {
        super();
        this.dispose(sceneObject.components.disposableAdd(this._view));
        this._view.name = option?.name ?? `视角${sceneObject.views.length + 1}`;

        if (viewInfo) {
            const v = this._view;
            const e = viewInfo;
            v.duration = e.duration ?? 1;
            v.position = e.position;
            v.rotation = e.rotation ?? [0, 0, 0];
            v.thumbnail = e.thumbnail ?? "";
            v.name = e.name as string;
        } else {
            const size = option?.size ?? [64, 64];
            this.dispose(nextAnimateFrame(() => {
                this._view.resetWithCurrentCamera();
                this._view.capture(size[0], size[1]);
            }));
        }

        const v = this._view;
        const event = this.disposeVar(createNextAnimateFrameEvent(v.durationChanged, v.positionChanged, v.rotationChanged, v.thumbnailChanged, v.nameChanged));
        this.dispose(event.disposableOn(() => sceneObject.emitViewsWarpper()));
    }
}
