import { BooleanProperty, geoArea, getDistancesFromPositions, GroupProperty, NumberProperty } from "xbsj-xe2/dist-node/xe2-base-objects";
import { PartialWithUndefinedReactivePropsToNativeProps, react, ReactivePropsToNativePropsAndChanged, extendClassProps } from "xbsj-xe2/dist-node/xe2-base-utils";
import { ESGeoVector } from "../../base/objs";

/**
 * https://www.wolai.com/earthsdk/pxKbJ5g7Sf59UJgqwZpyzc
 * https://c0yh9tnn0na.feishu.cn/docx/E1KidM8S3olrNHx8hQFc5S5knAf
 */
export class ESGeoRectangle extends ESGeoVector {
    static readonly type = this.register('ESGeoRectangle', this, { chsName: '矩形', tags: ['ESObjects', '_ES_Impl_Cesium', '_ES_Impl_UE'], description: "矩形" });
    get typeName() { return 'ESGeoRectangle'; }
    override get defaultProps() { return ESGeoRectangle.createDefaultProps(); }
    get json() { return this._innerGetJson() as JsonType; }
    set json(value: JsonType) { this._innerSetJson(value); }

    private _area = this.disposeVar(react(0));
    get area() { return this._area.value; }
    get areaChanged() { return this._area.changed; }

    private _perimeter = this.disposeVar(react(0));
    get perimeter() { return this._perimeter.value; }
    get perimeterChanged() { return this._perimeter.changed; }

    toPolygon() {
        if (this.points && this.points.length >= 2) {
            const pos0 = [...this.points][0]
            const pos1 = [...this.points][1]
            return [pos0, [pos0[0], pos1[1], pos0[2]], pos1, [pos1[0], pos0[1], pos1[2]]] as [number, number, number][]
        } else {
            return undefined
        }
    }


    constructor(id?: string) {
        super(id);
        this.collision = false;
        this.stroked = true;
        this.filled = true;
        this.fillStyle = {
            color: [1, 1, 1, 0.5],
            material: '',
            materialParams: {},
            ground: false,
        };

        const update = () => {
            if (this.points && this.points.length >= 2) {
                const val = [...this.points]
                const pos0 = val[0]
                const pos1 = val[1]
                const pos = [pos0, [pos0[0], pos1[1], pos0[2]], pos1, [pos1[0], pos0[1], pos1[2]]] as [number, number, number][]
                this._area.value = geoArea(pos)
                const posi = [...pos, pos[0]];
                const distance = getDistancesFromPositions(posi, 'NONE');
                const totalDistance = distance[distance.length - 1];
                this._perimeter.value = totalDistance;
            } else {
                this._area.value = 0;
                this._perimeter.value = 0;
            }
        }
        update();
        this.dispose(this.pointsChanged.disposableOn(update));
    }
    static override defaults = { ...ESGeoVector.defaults }

    override getESProperties() {
        const properties = { ...super.getESProperties() };
        return {
            ...properties,
            coordinate: [
                ...properties.coordinate,
                new NumberProperty('面积', '面积', false, true, [this, 'area']),
                new NumberProperty('周长', '周长', false, true, [this, 'perimeter'])]
        };
    };

    override getProperties(language: string) {
        return [
            ...super.getProperties(language),
            // new GroupProperty('czm', 'czm', [
            //     new BooleanProperty('是否贴地', '是否贴地(ground)', false, false, [this, 'ground']),
            //     new BooleanProperty('是否单点编辑', '是否单点编辑(czmPointEditing).', false, false, [this, 'czmPointEditing']),
            // ]),
            new GroupProperty('计算', '计算', [
                new NumberProperty('面积', '面积', false, true, [this, 'area']),
                new NumberProperty('周长', '周长', false, true, [this, 'perimeter'])
            ]),
        ];
    }
}

export namespace ESGeoRectangle {
    export const createDefaultProps = () => ({
        ...ESGeoVector.createDefaultProps(),
        // ground: false,
        // czmPointEditing: false
    })
}
extendClassProps(ESGeoRectangle.prototype, ESGeoRectangle.createDefaultProps);
export interface ESGeoRectangle extends ReactivePropsToNativePropsAndChanged<ReturnType<typeof ESGeoRectangle.createDefaultProps>> { };
type JsonType = PartialWithUndefinedReactivePropsToNativeProps<ReturnType<typeof ESGeoRectangle.createDefaultProps> & { type: string }>;

