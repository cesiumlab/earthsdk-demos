import { BooleanProperty, ColorProperty, DashPatternProperty, EnumProperty, GroupProperty, NumberProperty } from "xbsj-xe2/dist-node/xe2-base-objects";
import { PartialWithUndefinedReactivePropsToNativeProps, ReactivePropsToNativePropsAndChanged, extendClassProps, reactArrayWithUndefined } from "xbsj-xe2/dist-node/xe2-base-utils";
import { SceneObjectKey } from "xbsj-xe2/dist-node/xe2-utils";
// import { ESGeoLineString } from "../../base/objs";
import { ESGeoLineString } from "./ESGeoLineString";
import { ESStrokeStyle } from "../../base/objs";
/**
 * https://www.wolai.com/earthsdk/wxeuk8gv9v4PzHBZ6pURww
 * https://c0yh9tnn0na.feishu.cn/docx/RvandzVSCoT1dExFX4rcapyOn8g
 */
export class ESDistanceMeasurement extends ESGeoLineString {
    static override readonly type = this.register('ESDistanceMeasurement', this, { chsName: '距离测量', tags: ['ESObjects', '_ES_Impl_Cesium', '_ES_Impl_UE'], description: "距离测量" });
    override get typeName() { return 'ESDistanceMeasurement'; }
    override get defaultProps() { return ESDistanceMeasurement.createDefaultProps(); }
    override get json() { return this._innerGetJson() as JsonType; }
    override set json(value: JsonType) { this._innerSetJson(value); }

    override get strokeColor() { return this.strokeStyle.color; }
    override set strokeColor(value: [number, number, number, number]) { this.strokeStyle = { ...this.strokeStyle, color: [...value] } }


    static override defaults = {
        ...ESGeoLineString.defaults,
        strokeStyle: {
            width: 10,
            widthType: 'screen',
            color: [1, 0, 0, 1],
            material: '',
            materialParams: {}
        } as ESStrokeStyle,
    };
    constructor(id?: SceneObjectKey) {
        super(id);
        this.strokeStyle.width = 2;
    }

    override getProperties(language?: string) {
        return [
            ...super.getProperties(language),
        ]
    }
}

export namespace ESDistanceMeasurement {
    export const createDefaultProps = () => ({
        ...ESGeoLineString.createDefaultProps(),
    });
}
extendClassProps(ESDistanceMeasurement.prototype, ESDistanceMeasurement.createDefaultProps);
export interface ESDistanceMeasurement extends ReactivePropsToNativePropsAndChanged<ReturnType<typeof ESDistanceMeasurement.createDefaultProps>> { }
type JsonType = PartialWithUndefinedReactivePropsToNativeProps<ReturnType<typeof ESDistanceMeasurement.createDefaultProps> & { type: string }>;
