import { BooleanProperty, GroupProperty, Number3Property, PositionProperty, UriProperty } from "xbsj-xe2/dist-node/xe2-base-objects";
import { PartialWithUndefinedReactivePropsToNativeProps, ReactivePropsToNativePropsAndChanged, extendClassProps, reactArray } from "xbsj-xe2/dist-node/xe2-base-utils";
import { SceneObjectKey } from "xbsj-xe2/dist-node/xe2-utils";
import { ESObjectWithLocation } from "../../base/objs";

export class ESBlastParticleSystem extends ESObjectWithLocation {
    static readonly type = this.register('ESBlastParticleSystem', this, { chsName: '粒子爆炸', tags: ['ESObjects', '_ES_Impl_Cesium', '_ES_Impl_UE'], description: "粒子爆炸" });
    get typeName() { return 'ESBlastParticleSystem'; }
    override get defaultProps() { return ESBlastParticleSystem.createDefaultProps(); }
    get json() { return this._innerGetJson() as JsonType; }
    set json(value: JsonType) { this._innerSetJson(value); }

    constructor(id?: SceneObjectKey) {
        super(id);
    }
    static override defaults = {
        ...ESObjectWithLocation.defaults,
        image: `\${esobjs-xe2-plugin-assets-script-dir}/xe2-assets/esobjs-xe2-plugin/images/smoke.png`,
        translation: [0, 0, 0] as [number, number, number],

    }
    override getESProperties() {
        const properties = { ...super.getESProperties() };
        return {
            ...properties,
            defaultMenu: 'GeneralProprties',
        };
    };
    override getProperties(language: string) {
        return [
            ...super.getProperties(language),
            new GroupProperty('通用', '通用', [
                new GroupProperty('czm', 'czm', [
                    new UriProperty('图片', 'The URI, HTMLImageElement, or HTMLCanvasElement to use for the billboard.', false, false, [this, 'image']),
                    new Number3Property('偏移', 'translation', false, false, [this, 'translation']),
                ]),
            ]),
        ];
    }
}

export namespace ESBlastParticleSystem {
    export const createDefaultProps = () => ({
        ...ESObjectWithLocation.createDefaultProps(),
        image: `\${esobjs-xe2-plugin-assets-script-dir}/xe2-assets/esobjs-xe2-plugin/images/smoke.png`,
        positionEditing: undefined as boolean | undefined,
        translation: reactArray<[number, number, number]>([0, 0, 0]),
    });
}
extendClassProps(ESBlastParticleSystem.prototype, ESBlastParticleSystem.createDefaultProps);
export interface ESBlastParticleSystem extends ReactivePropsToNativePropsAndChanged<ReturnType<typeof ESBlastParticleSystem.createDefaultProps>> { }
type JsonType = PartialWithUndefinedReactivePropsToNativeProps<ReturnType<typeof ESBlastParticleSystem.createDefaultProps> & { type: string }>;