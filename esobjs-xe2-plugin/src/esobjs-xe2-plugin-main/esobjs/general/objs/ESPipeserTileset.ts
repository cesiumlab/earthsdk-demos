import { FunctionProperty, EnumProperty, GroupProperty } from "xbsj-xe2/dist-node/xe2-base-objects";
import { PartialWithUndefinedReactivePropsToNativeProps, Event, ReactivePropsToNativePropsAndChanged, extendClassProps } from "xbsj-xe2/dist-node/xe2-base-utils";
import { SceneObjectKey } from "xbsj-xe2/dist-node/xe2-utils";
import { ES3DTileset } from "./ES3DTileset";
/**
 * https://www.wolai.com/earthsdk/oTfNEhFjwzUwkKHrHvSG2H
 * https://c0yh9tnn0na.feishu.cn/docx/BsardsehqoQpPzxwilfcM7E1nri
 */
export class ESPipeserTileset extends ES3DTileset {
    static override readonly type = this.register('ESPipeserTileset', this, { chsName: 'PipeSer图层', tags: ['ESObjects', '_ES_Impl_Cesium', '_ES_Impl_UE'], description: "PipeSer图层" });
    override get typeName() { return 'ESPipeserTileset'; }
    override get defaultProps() { return ESPipeserTileset.createDefaultProps(); }
    override get json() { return this._innerGetJson() as JsonType; }
    override set json(value: JsonType) { this._innerSetJson(value); }

    static description = {
        colorMode: [["default", 'default'], ['color', 'color'], ['blend', 'blend']] as [string, string][],
    }

    private _setLayerVisibleEvent = this.dv(new Event<[name: string, layerJson: string | ({ [key: string]: any }[])]>());
    get setLayerVisibleEvent() { return this._setLayerVisibleEvent; }
    setLayerVisible(name: string, layerJson: string | ({ [key: string]: any }[])) { this._setLayerVisibleEvent.emit(name, layerJson); }

    private _setLayerColorEvent = this.dv(new Event<[name: string, layerJson: string | ({ [key: string]: any }[])]>());
    get setLayerColorEvent() { return this._setLayerColorEvent; }
    setLayerColor(name: string, layerJson: string | ({ [key: string]: any }[])) { this._setLayerColorEvent.emit(name, layerJson); }

    constructor(id?: SceneObjectKey) {
        super(id);
    }

    override getProperties(language?: string) {
        return [
            ...super.getProperties(language),
            new GroupProperty('Pipeser', 'Pipeser', [
                new FunctionProperty("setLayerVisible", "setLayerVisible", ['string', 'string'], (name, layerJson) => this.setLayerVisible(name, layerJson), ['', '']),
                new FunctionProperty("setLayerColor", "setLayerColor", ['string', 'string'], (name, layerJson) => this.setLayerColor(name, layerJson), ['', '']),
                new EnumProperty('colorMode', 'colorMode', true, false, [this, 'colorMode'], ESPipeserTileset.description.colorMode, 'default'),
            ]),
        ]
    }
}

export namespace ESPipeserTileset {
    export const createDefaultProps = () => ({
        colorMode: 'default',
        ...ES3DTileset.createDefaultProps(),
    });
}
extendClassProps(ESPipeserTileset.prototype, ESPipeserTileset.createDefaultProps);
export interface ESPipeserTileset extends ReactivePropsToNativePropsAndChanged<ReturnType<typeof ESPipeserTileset.createDefaultProps>> { }
type JsonType = PartialWithUndefinedReactivePropsToNativeProps<ReturnType<typeof ESPipeserTileset.createDefaultProps> & { type: string }>;
