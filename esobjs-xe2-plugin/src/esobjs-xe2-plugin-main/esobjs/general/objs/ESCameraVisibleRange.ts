import { NumberProperty, PositionsEditing } from "xbsj-xe2/dist-node/xe2-base-objects";
import { PartialWithUndefinedReactivePropsToNativeProps, reactArrayWithUndefined, ReactivePropsToNativePropsAndChanged, extendClassProps, react } from "xbsj-xe2/dist-node/xe2-base-utils";
import { ESObjectWithLocation } from "../../base/objs";
import { SceneObjectKey } from "xbsj-xe2/dist-node/xe2-utils";

/**
 * https://www.wolai.com/earthsdk/uraiJ6nPZBuNTiNMsx78vP
 * https://c0yh9tnn0na.feishu.cn/docx/LIOFdmJOiokrj2xllUIc1rOancg
 */
export class ESCameraVisibleRange extends ESObjectWithLocation {
    static readonly type = this.register('ESCameraVisibleRange', this, { chsName: '摄像头可视域', tags: ['ESObjects', '_ES_Impl_Cesium', '_ES_Impl_UE'], description: "摄像头可视域" });
    get typeName() { return 'ESCameraVisibleRange'; }
    override get defaultProps() { return ESCameraVisibleRange.createDefaultProps(); }
    get json() { return this._innerGetJson() as JsonType; }
    set json(value: JsonType) { this._innerSetJson(value); }


    // private _points = this.disposeVar(reactArrayWithUndefined<[number, number, number][]>(undefined));
    // get points() { return this._points.value; }
    // get pointsChanged() { return this._points.changed; }                                                   

    constructor(id?: SceneObjectKey) {
        super(id);
        this.collision = false;
    }
    static override defaults = {
        ...ESObjectWithLocation.defaults,
        fov: 90,
        aspectRatio: 1.77778,
        far: 100,
        near: 5,
    };
    override getESProperties() {
        const properties = { ...super.getESProperties() };
        return {
            ...properties,
            basic: [
                ...properties.basic,
                new NumberProperty('宽高比', 'aspectRatio', false, false, [this, 'aspectRatio'], 1.77778),
                new NumberProperty('横向夹角', 'fov', false, false, [this, 'fov'], 90),
                new NumberProperty('视野长度', 'far', false, false, [this, 'far'], 100),
                new NumberProperty('近面距离', 'near', false, false, [this, 'near'], 5),
            ]
        }
    }
    override getProperties(language?: string) {
        return [
            ...super.getProperties(language),
            new NumberProperty('横向夹角', 'fov', false, false, [this, 'fov']),
            new NumberProperty('宽高比', 'aspectRatio', false, false, [this, 'aspectRatio']),
            new NumberProperty('视野长度', 'far', false, false, [this, 'far']),
            new NumberProperty('近面距离', 'near', false, false, [this, 'near']),
        ];
    }
}

export namespace ESCameraVisibleRange {
    export const createDefaultProps = () => ({
        ...ESObjectWithLocation.createDefaultProps(),
        fov: 90,
        aspectRatio: 1.77778,
        far: 100,
        near: 5,
    });
}
extendClassProps(ESCameraVisibleRange.prototype, ESCameraVisibleRange.createDefaultProps);
export interface ESCameraVisibleRange extends ReactivePropsToNativePropsAndChanged<ReturnType<typeof ESCameraVisibleRange.createDefaultProps>> { }
type JsonType = PartialWithUndefinedReactivePropsToNativeProps<ReturnType<typeof ESCameraVisibleRange.createDefaultProps> & { type: string }>;
