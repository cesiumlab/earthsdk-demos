import { PartialWithUndefinedReactivePropsToNativeProps, ReactivePropsToNativePropsAndChanged, extendClassProps } from "xbsj-xe2/dist-node/xe2-base-utils";
import { SceneObjectKey } from "xbsj-xe2/dist-node/xe2-utils";
import { ESGeoVector } from "../../base/objs";
/**
 * https://www.wolai.com/earthsdk/8nkhuLbWJ44X4sV5hD5HPU
 * https://c0yh9tnn0na.feishu.cn/docx/MzZxdZOg2oej6vxgAfxc35zdnGh
 */
export class ESHeightMeasurement extends ESGeoVector {
    static readonly type = this.register('ESHeightMeasurement', this, { chsName: '高度测量', tags: ['ESObjects', '_ES_Impl_Cesium', '_ES_Impl_UE'], description: "高度测量" });
    get typeName() { return 'ESHeightMeasurement'; }
    override get defaultProps() { return ESHeightMeasurement.createDefaultProps(); }
    get json() { return this._innerGetJson() as JsonType; }
    set json(value: JsonType) { this._innerSetJson(value); }
    constructor(id?: SceneObjectKey) {
        super(id);
        this.stroked = true;
        this.strokeStyle.width = 2;
    }
    static override defaults = {
        ...ESGeoVector.defaults,
    };

    override getProperties(language?: string) {
        return [...super.getProperties(language)]
    }
}

export namespace ESHeightMeasurement {
    export const createDefaultProps = () => ({
        ...ESGeoVector.createDefaultProps(),
    });
}
extendClassProps(ESHeightMeasurement.prototype, ESHeightMeasurement.createDefaultProps);
export interface ESHeightMeasurement extends ReactivePropsToNativePropsAndChanged<ReturnType<typeof ESHeightMeasurement.createDefaultProps>> { }
type JsonType = PartialWithUndefinedReactivePropsToNativeProps<ReturnType<typeof ESHeightMeasurement.createDefaultProps> & { type: string }>;
