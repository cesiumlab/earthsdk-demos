import { Player, getGeoJson, setGeoJson } from "xbsj-xe2/dist-node/xe2-base-objects";
import { BooleanProperty, GroupProperty, NonreactiveJsonStringProperty, NumberProperty } from "xbsj-xe2/dist-node/xe2-base-objects";
import { extendClassProps, JsonValue, PartialWithUndefinedReactivePropsToNativeProps, ReactivePropsToNativePropsAndChanged } from "xbsj-xe2/dist-node/xe2-base-utils";
import { SceneObjectKey } from "xbsj-xe2/dist-node/xe2-utils";
import { ESObjectWithLocation, ESStrokeStyle } from "../../base/objs";

/**
 * https://www.wolai.com/earthsdk/sAHf2XpnfBguZh2eJbTZcM
 * https://c0yh9tnn0na.feishu.cn/docx/MzFNdC8XqoJ2Fax1Tk4ceGWMnBg
 */
export class ESViewShed extends ESObjectWithLocation {
    static readonly type = this.register('ESViewShed', this, { chsName: '视域分析', tags: ['ESObjects', '_ES_Impl_Cesium', '_ES_Impl_UE'], description: "信号传输器" });
    get typeName() { return 'ESViewShed'; }
    override get defaultProps() { return ESViewShed.createDefaultProps(); }
    get json() { return this._innerGetJson() as JsonType; }
    set json(value: JsonType) { this._innerSetJson(value); }


    get geoJson() {
        const geoJsonObj = getGeoJson('LineString', this.position, this.name)
        return geoJsonObj;
    }

    set geoJson(value: JsonValue) {
        const objValue = setGeoJson(value)
        this.name = objValue.name
        this.position = objValue.position
    }

    get geoJsonStr() {
        try {
            return JSON.stringify(this.geoJson, undefined, '  ');
        } catch (error) {
            return '';
        }
    }
    set geoJsonStr(value: string) {
        if (!value) {
            return;
        }
        try {
            this.geoJson = JSON.parse(value);
        } catch (error) {
        }
    }

    constructor(id?: SceneObjectKey) {
        super(id);
        this.collision = false;
    }

    static override defaults = {
        ...ESObjectWithLocation.defaults,
        fov: 90,
        aspectRatio: 1.77778,
        near: 10,
        far: 100,
        zIndex: 1,
        showFrustum: true
    }
    override getESProperties() {
        const properties = { ...super.getESProperties() };
        return {
            ...properties,
            basic: [
                ...properties.basic,
                new BooleanProperty('视椎体', 'showFrustum', false, false, [this, 'showFrustum'], ESViewShed.defaults.showFrustum),
                new NumberProperty('宽高比', 'aspectRatio', false, false, [this, 'aspectRatio'], ESViewShed.defaults.aspectRatio),
                new NumberProperty('横向夹角', 'fov', false, false, [this, 'fov'], ESViewShed.defaults.fov),
                new NumberProperty('近面距离', 'near', false, false, [this, 'near'], ESViewShed.defaults.near),
                new NumberProperty('视野长度', 'far', false, false, [this, 'far'], ESViewShed.defaults.far),
                new NumberProperty('显示优先级', 'zIndex', false, false, [this, 'zIndex'], ESViewShed.defaults.zIndex),
            ],
        }
    }

    override getProperties(language?: string) {
        return [
            ...super.getProperties(language),
            new GroupProperty('通用', '通用', [
                new NumberProperty('横向夹角', "fov", false, false, [this, 'fov']),
                new NumberProperty('宽高比', "aspectRatio", false, false, [this, 'aspectRatio']),
                new NumberProperty('近面距离', "near", false, false, [this, 'near']),
                new NumberProperty('视野长度', "far", false, false, [this, 'far']),
                new NumberProperty('显示优先级', 'zIndex', false, false, [this, 'zIndex']),
                new BooleanProperty('视椎体', 'showFrustum', false, false, [this, 'showFrustum']),
                new NonreactiveJsonStringProperty('geoJson', '生成GeoJSON数据。', false, false, () => this.geoJsonStr, (value: string | undefined) => value && (this.geoJsonStr = value)),
            ]),
        ]
    }
}

export namespace ESViewShed {
    export const createDefaultProps = () => ({
        ...ESObjectWithLocation.createDefaultProps(),
        fov: 90,
        aspectRatio: 1.77778,
        near: 10,
        far: 100,
        zIndex: 1,
        showFrustum: true
    });
}

extendClassProps(ESViewShed.prototype, ESViewShed.createDefaultProps);
export interface ESViewShed extends ReactivePropsToNativePropsAndChanged<ReturnType<typeof ESViewShed.createDefaultProps>> { }
type JsonType = PartialWithUndefinedReactivePropsToNativeProps<ReturnType<typeof ESViewShed.createDefaultProps> & { type: string }>;