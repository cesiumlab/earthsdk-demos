import { EvalStringProperty, GroupProperty, Number2Property, NumberProperty, Viewer } from "xbsj-xe2/dist-node/xe2-base-objects";
import { Event, PartialWithUndefinedReactivePropsToNativeProps, ReactivePropsToNativePropsAndChanged, extendClassProps, react, reactArray } from "xbsj-xe2/dist-node/xe2-base-utils";
import { SceneObjectKey } from "xbsj-xe2/dist-node/xe2-utils";
import { ESObjectWithLocation, WidgetEventInfo } from "../../base/objs";

const defaulInstanceClassStr = `class MyDiv {
  // container是Poi的div
  // geoCustomDivPoi指向当前的GeoCustomDivPoi场景对象
  // viewer指定当前的视口
  constructor(container, eSGeoDiv, viewer) {       
      this._container = container;
      this._div = document.createElement('div');
      this._container.appendChild(this._div);

      this._div.style.width = '300px';
      this._div.style.height = '50px';
      this._div.style.background = 'rgba(120, 120, 0, 0.7)';
      this._div.style.color = 'white';
      this._div.style.fontSize = '30px';
      this._div.style.lineHeight = '50px';
      this._div.style.border = '1px solid white';
      this._div.innerText = 'Hello world!';
  }

  // 随机背景颜色，仅用于测试外部强制更新，此函数非必需
  update() {
      const r = (255 * Math.random()) | 0;
      const g = (255 * Math.random()) | 0;
      const b = (255 * Math.random()) | 0;
      this._div.style.background = \`rgba(\${r}, \${g}, \${b}, 0.8)\`;
  }

  // 销毁函数，注意此函数必需，否则会报错！
  destroy() {
      this._container.removeChild(this._div);
  }
}`;

const instanceClassStrReadMe = `\
示例代码：  
\`\`\`
${defaulInstanceClassStr}
\`\`\`
`;

const defaultInnerHTML = `\
<div style="width: 300px; height: 50px; background: rgba(120, 120, 0, 0.7); color: white; font-size: 30px; line-height: 50px; border: 1px solid white;">Hello world!</div>
`;

const innerHTMLReadMe = `\
示例代码：  
\`\`\`
${defaultInnerHTML}
\`\`\`
`;


//示例集合
const echartsFunStrMd = `
示例1:柱状图
\`\`\`
function init() {
    var option = {
        xAxis: {
          type: 'category',
          data: ['Mon', 'Tue', 'Wed', 'Thu', 'Fri', 'Sat', 'Sun']
        },
        yAxis: {
          type: 'value'
        },
        series: [
          {
            data: [
              120,
              {
                value: 200,
                itemStyle: {
                  color: '#a90000'
                }
              },
              150,
              80,
              70,
              110,
              130
            ],
            type: 'bar'
          }
        ]
      }
    return option
}
\`\`\`
示例2:折线图
\`\`\`
function init() {
    var option  = {
        title: {
          text: 'Stacked Line'
        },
        tooltip: {
          trigger: 'axis'
        },
        legend: {
          data: ['Email', 'Union Ads', 'Video Ads', 'Direct', 'Search Engine']
        },
        grid: {
          left: '3%',
          right: '4%',
          bottom: '3%',
          containLabel: true
        },
        toolbox: {
          feature: {
            saveAsImage: {}
          }
        },
        xAxis: {
          type: 'category',
          boundaryGap: false,
          data: ['Mon', 'Tue', 'Wed', 'Thu', 'Fri', 'Sat', 'Sun']
        },
        yAxis: {
          type: 'value'
        },
        series: [
          {
            name: 'Email',
            type: 'line',
            stack: 'Total',
            data: [120, 132, 101, 134, 90, 230, 210]
          },
          {
            name: 'Union Ads',
            type: 'line',
            stack: 'Total',
            data: [220, 182, 191, 234, 290, 330, 310]
          },
          {
            name: 'Video Ads',
            type: 'line',
            stack: 'Total',
            data: [150, 232, 201, 154, 190, 330, 410]
          },
          {
            name: 'Direct',
            type: 'line',
            stack: 'Total',
            data: [320, 332, 301, 334, 390, 330, 320]
          },
          {
            name: 'Search Engine',
            type: 'line',
            stack: 'Total',
            data: [820, 932, 901, 934, 1290, 1330, 1320]
          }
        ]
      }
    return option
}
\`\`\`
示例3:饼图
\`\`\`
function init() {
    var option = {
        title: {
          text: 'Referer of a Website',
          subtext: 'Fake Data',
          left: 'center'
        },
        tooltip: {
          trigger: 'item'
        },
        legend: {
          orient: 'vertical',
          left: 'left'
        },
        series: [
          {
            name: 'Access From',
            type: 'pie',
            radius: '50%',
            data: [
              { value: 1048, name: 'Search Engine' },
              { value: 735, name: 'Direct' },
              { value: 580, name: 'Email' },
              { value: 484, name: 'Union Ads' },
              { value: 300, name: 'Video Ads' }
            ],
            emphasis: {
              itemStyle: {
                shadowBlur: 10,
                shadowOffsetX: 0,
                shadowColor: 'rgba(0, 0, 0, 0.5)'
              }
            }
          }
        ]
      }
    return option
}
\`\`\`


`;

export type ESGeoDivInstanceClass<DivClass extends { destroy(): undefined } = { destroy(): undefined }> = (new (container: HTMLDivElement, eSGeoDiv: ESGeoDiv, viewer: Viewer) => DivClass);


/**
 * https://www.wolai.com/earthsdk/e17QPxZkVnG3ujXj8sJ2un
 * https://c0yh9tnn0na.feishu.cn/docx/EZzXdly4KopFHAx5iSrcBmeOnSf
 */
export class ESGeoDiv extends ESObjectWithLocation {
  static readonly type = this.register('ESGeoDiv', this, { chsName: 'ESGeoDiv', tags: ['ESObjects', '_ES_Impl_Cesium', '_ES_Impl_UE'], description: "ESGeoDiv" });
  get typeName() { return 'ESGeoDiv'; }
  override get defaultProps() { return ESGeoDiv.createDefaultProps(); }
  get json() { return this._innerGetJson() as JsonType; }
  set json(value: JsonType) { this._innerSetJson(value); }

  private _widgetEvent = this.dv(new Event<[WidgetEventInfo]>());
  get widgetEvent() { return this._widgetEvent };

  private _echartsFunReact = this.disposeVar(react<(() => any) | undefined>(undefined));
  get echartsFun() { return this._echartsFunReact.value; }
  set echartsFun(value: (() => any) | undefined) { this._echartsFunReact.value = value; }
  get echartsFunChanged() { return this._echartsFunReact.changed; }

  private _instanceClassReact = this.disposeVar(react<ESGeoDivInstanceClass | undefined>(undefined));
  get instanceClass() { return this._instanceClassReact.value; }
  set instanceClass(value: ESGeoDivInstanceClass | undefined) { this._instanceClassReact.value = value; }
  get instanceClassChanged() { return this._instanceClassReact.changed; }

  static override defaults = {
    ...ESObjectWithLocation.defaults,
    opacity: 1,
    anchor: [0.5, 1] as [number, number],
    instanceClassStr: defaulInstanceClassStr,
    instanceClassStrReadMe: instanceClassStrReadMe,
    innerHTML: defaultInnerHTML,
    zOrder: 0,
  };

  constructor(id?: SceneObjectKey) {
    super(id);

    const updateInstanceClassStr = () => {
      try {
        this.instanceClass = this.instanceClassStr && Function(`"use strict";return (${this.instanceClassStr})`)();
      } catch (error) {
        this.instanceClass = undefined;
      }
    };
    updateInstanceClassStr();
    this.dispose(this.instanceClassStrChanged.disposableOn(updateInstanceClassStr));

    {
      const update = () => {
        if (this.innerHTML === undefined) {
          this.instanceClassStr = undefined;
          return;
        }

        const instanceClassStr = `class MyDiv {
                    // container是Poi的div
                    // geoCustomDivPoi指向当前的GeoCustomDivPoi场景对象
                    // viewer指定当前的视口
                    constructor(container, eSGeoDiv, viewer) {
                        this._container = container;
                        this._div = document.createElement('div');
                        this._div.style.position = 'relative';
                        this._container.appendChild(this._div);
                        this._div.innerHTML = \`${this.innerHTML}\`;
                    }
                
                    // 销毁函数，注意此函数必需，否则会报错！
                    destroy() {
                        this._container.removeChild(this._div);
                    }
                }`;
        this.instanceClassStr = instanceClassStr;
      };
      update();
      this.dispose(this.innerHTMLChanged.disposableOn(update));
    }
  }
  override getESProperties() {
    const properties = { ...super.getESProperties() };
    return {
      ...properties,
      basic: [
        ...properties.basic,
        new NumberProperty('透明度', '透明度', true, false, [this, 'opacity'], ESGeoDiv.defaults.opacity),
        new NumberProperty('zOrder', 'zOrder', true, false, [this, 'zOrder'], ESGeoDiv.defaults.zOrder),
        new Number2Property('偏移比例', '偏移比例.', true, false, [this, 'anchor'], ESGeoDiv.defaults.anchor),
        new EvalStringProperty('实例类', '实例类', true, false, [this, 'instanceClassStr'], ESGeoDiv.defaults.instanceClassStr, ESGeoDiv.defaults.instanceClassStrReadMe),
        new EvalStringProperty('innerHTML', '注意设置此属性设置此属性会自动更新instanceClassStr变量', true, false, [this, 'innerHTML'], ESGeoDiv.defaults.innerHTML),
      ],
    }
  }
  override getProperties(language?: string) {
    return [
      ...super.getProperties(language),
      new GroupProperty('通用', '通用', [
        new Number2Property('偏移比例', '偏移比例.', true, false, [this, 'anchor'], ESGeoDiv.defaults.anchor),
        new NumberProperty('透明度', '透明度', true, false, [this, 'opacity'], ESGeoDiv.defaults.opacity),
        new EvalStringProperty('实例类', '实例类', true, false, [this, 'instanceClassStr'], ESGeoDiv.defaults.instanceClassStr, ESGeoDiv.defaults.instanceClassStrReadMe),
        new EvalStringProperty('innerHTML', '注意设置此属性设置此属性会自动更新instanceClassStr变量', true, false, [this, 'innerHTML'], ESGeoDiv.defaults.innerHTML),
        new NumberProperty('zOrder', 'zOrder', true, false, [this, 'zOrder'], ESGeoDiv.defaults.zOrder),
      ]),
    ]
  }
}

export namespace ESGeoDiv {
  export const createDefaultProps = () => ({
    ...ESObjectWithLocation.createDefaultProps(),
    opacity: 1,
    anchor: reactArray<[leftRatio: number, topRatio: number]>([0.5, 1]), // 为undefined时设置为[0.5, 1.0]
    instanceClassStr: undefined as string | undefined,
    innerHTML: defaultInnerHTML as string | undefined,
    zOrder: 0,
  });
}
extendClassProps(ESGeoDiv.prototype, ESGeoDiv.createDefaultProps);
export interface ESGeoDiv extends ReactivePropsToNativePropsAndChanged<ReturnType<typeof ESGeoDiv.createDefaultProps>> { }
type JsonType = PartialWithUndefinedReactivePropsToNativeProps<ReturnType<typeof ESGeoDiv.createDefaultProps> & { type: string }>;
