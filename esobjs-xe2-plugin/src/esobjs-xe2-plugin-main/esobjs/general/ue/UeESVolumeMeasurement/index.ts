import { ESVolumeMeasurement } from '../../objs';
import { UeViewer } from "xbsj-xe2/dist-node/xe2-ue-objects";
import { UeESGeoPolygon } from "../UeESGeoPolygon";

export class UeESVolumeMeasurement extends UeESGeoPolygon<ESVolumeMeasurement> {
    static override readonly type = this.register(ESVolumeMeasurement.type, this);
    constructor(sceneObject: ESVolumeMeasurement, ueViewer: UeViewer) {
        super(sceneObject, ueViewer);
        const viewer = ueViewer.viewer;
        if (!viewer) {
            console.warn(`viewer is undefined!`);
            return;
        }
        this.dispose(sceneObject.startEvent.disposableOn(() => {
            viewer.callUeFunc({
                f: 'Start',
                p: {
                    id: sceneObject.id
                }
            })
        }))
        this.dispose(sceneObject.clearEvent.disposableOn(() => {
            viewer.callUeFunc({
                f: 'Clear',
                p: {
                    id: sceneObject.id
                }
            })
        }))
    }
}
