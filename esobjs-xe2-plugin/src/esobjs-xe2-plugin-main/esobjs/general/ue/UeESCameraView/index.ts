import { ESCameraView } from '../../objs';
// import { flyInCallFunc, resetWithCurrentCameraCallFunc } from '../../../base/UeViewer/CallUeFuncs';
import { flyInCallFunc, resetWithCurrentCameraCallFunc } from 'xbsj-xe2/dist-node/xe2-ue-objects';
import { UeViewer } from '../../../base';
import { UeESObjectWithLocation } from '../../../base';
import { ObjResettingWithEvent } from 'xbsj-xe2/dist-node/xe2-utils';
import { UeCameraHelper } from './UeCameraHelper';

export class UeESCameraView extends UeESObjectWithLocation<ESCameraView> {
    static readonly type = this.register(ESCameraView.type, this);
    constructor(sceneObject: ESCameraView, ueViewer: UeViewer) {
        super(sceneObject, ueViewer);

        const viewer = ueViewer.viewer;
        if (!viewer) {
            console.warn(`viewer is undefined!`);
            return;
        }

        this.disposeVar(new ObjResettingWithEvent(sceneObject.showChanged, () => {
            if ((sceneObject.show ?? true) && sceneObject.position) {
                return new UeCameraHelper(sceneObject, ueViewer);
            } else {
                return undefined;
            }
        }))

        this.dispose(sceneObject.flyInEvent.disposableOn(duration => {
            flyInCallFunc(viewer, sceneObject.id, sceneObject.position, sceneObject.rotation, (duration ?? 1000) / 1000)
        }))

        this.dispose(sceneObject.resetWithCurrentCameraEvent.disposableOn(() => {
            resetWithCurrentCameraCallFunc(viewer, sceneObject.id)
        }));

        this.dispose(sceneObject.captureEvent.disposableOn((x, y) => {
            const str = ueViewer.capture(x, y)
            str.then((res) => { if (res) sceneObject.thumbnail = res })
        }));
    }
}
