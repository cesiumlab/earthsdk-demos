import { GeoAxis, GeoPoint } from "xbsj-xe2/dist-node/xe2-base-objects";
import { Destroyable, createNextAnimateFrameEvent } from "xbsj-xe2/dist-node/xe2-base-utils";
import { getCameraTargetPos } from "xbsj-xe2/dist-node/xe2-cesium-objects";
import { ESCameraView, ESGltfModel } from '../../objs';
import { UeViewer } from "../../../base";


export class UeCameraHelper extends Destroyable {
    private _model = this.disposeVar(new ESGltfModel());
    get model() { return this._model; }

    private _geoPoint = this.disposeVar(new GeoPoint());
    get geoPoint() { return this._geoPoint; }

    private _geoAxis = this.disposeVar(new GeoAxis());
    get geoAxis() { return this._geoAxis; }

    constructor(sceneObject: ESCameraView, ueViewer: UeViewer) {
        super();

        {
            ueViewer.add(this._model);
            this.dispose(() => ueViewer.delete(this._model));
            this._model.url = '${esobjs-xe2-plugin-assets-script-dir}/xe2-assets/esobjs-xe2-plugin/glbs/camera1/camera1.gltf';
            this._model.allowPicking = false;
            this._model.czmMaximumScale = 500000
            this._model.czmMinimumPixelSize = 300;
            {
                const update = () => {
                    if (!sceneObject.position) return;
                    this._model.position = getCameraTargetPos(sceneObject.position, sceneObject.rotation ?? [0, 0, 0], 0) ?? [0, 0, 0];
                    this._model.rotation = sceneObject.rotation;
                };
                update();
                const event = this.disposeVar(createNextAnimateFrameEvent(sceneObject.positionChanged, sceneObject.rotationChanged));
                this.dispose(event.disposableOn(update));
            }
            // {
            //     // 相机靠得太近就不显示！
            //     const update = () => {
            //         const cameraInfo = ueViewer.getCameraInfo()
            //         if (!cameraInfo) return;
            //         const cp = cameraInfo.position;
            //         const sp = this._model.position;
            //         if (!sp) return;

            //         const spc = Cesium.Cartesian3.fromDegrees(sp[0], sp[1], sp[2]);
            //         const cpc = Cesium.Cartesian3.fromDegrees(cp[0], cp[1], cp[2]);
            //         const d2 = Cesium.Cartesian3.distanceSquared(spc, cpc);
            //         this._model.show = d2 > 1;
            //     }
            //     update();
            //     const event = this.disposeVar(createNextAnimateFrameEvent(ueViewer.extensions.cameraChanged, this._model.positionChanged));
            //     this.dispose(event.disposableOn(update));
            // }
        }

        // {
        //     ueViewer.add(this._geoPoint);
        //     this.dispose(() => ueViewer.delete(this._geoPoint));
        //     this._geoPoint.pixelSize = 10;
        //     this._geoPoint.color = [1, 1, 0, 1];
        //     this._geoPoint.outlineColor = [0, 0, 0, 0.6];
        //     this._geoPoint.outlineWidth = 1;

        //     {
        //         const update = () => {
        //             if (!sceneObject.position) return;
        //             this._geoPoint.position = sceneObject.position;
        //         };
        //         update();
        //         const event = this.disposeVar(createNextAnimateFrameEvent(sceneObject.positionChanged));
        //         this.dispose(event.disposableOn(update));
        //     }
        // }

        {
            ueViewer.add(this._geoAxis);
            this.dispose(() => ueViewer.delete(this._geoAxis));
            this._geoAxis.width = 6;
            this._geoAxis.color = [1, 1, 0, 0.7];
            {
                const update = () => {
                    if (!sceneObject.position) return;
                    if (!this.model.position) return;
                    this._geoAxis.startPosition = this.model.position;
                    this._geoAxis.stopPosition = sceneObject.position;
                };
                update();
                const event = this.disposeVar(createNextAnimateFrameEvent(sceneObject.positionChanged, this._model.positionChanged));
                this.dispose(event.disposableOn(update));
            }
        }

    }
}
