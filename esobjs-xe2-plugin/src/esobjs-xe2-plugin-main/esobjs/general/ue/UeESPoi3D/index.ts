import { UeViewer } from "xbsj-xe2/dist-node/xe2-ue-objects";
import { UeESObjectWithLocation } from "../../../base";
import { ESPoi3D } from '../../objs';
export class UeESPoi3D extends UeESObjectWithLocation<ESPoi3D> {
    static readonly type = this.register(ESPoi3D.type, this);
    constructor(sceneObject: ESPoi3D, ueViewer: UeViewer) {
        super(sceneObject, ueViewer);

        const viewer = ueViewer.viewer;
        if (!viewer) {
            console.warn(`viewer is undefined!`);
            return;
        }
    }
}
