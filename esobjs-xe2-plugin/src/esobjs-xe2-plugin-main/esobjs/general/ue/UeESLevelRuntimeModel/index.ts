import { ESLevelRuntimeModel } from '../../objs';
import { UeViewer } from "xbsj-xe2/dist-node/xe2-ue-objects";
import { UeESObjectWithLocation } from "../../../base";
export class UeESLevelRuntimeModel extends UeESObjectWithLocation<ESLevelRuntimeModel> {
    static readonly type = this.register(ESLevelRuntimeModel.type, this);
    constructor(sceneObject: ESLevelRuntimeModel, ueViewer: UeViewer) {
        super(sceneObject, ueViewer);
        const viewer = ueViewer.viewer;
        if (!viewer) {
            console.warn(`viewer is undefined!`);
            return;
        }
    }
}
