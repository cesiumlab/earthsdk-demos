import { createNextAnimateFrameEvent } from "xbsj-xe2/dist-node/xe2-base-utils";
import { ESPath } from '../../objs';
import { UeViewer } from "xbsj-xe2/dist-node/xe2-ue-objects";
import { UeESGeoVector } from '../../../base';

export class UeESPath<T extends ESPath = ESPath> extends UeESGeoVector<T> {
    static readonly type = this.register<ESPath>(ESPath.type, this);

    static override forceUeUpdateProps = [
        ...UeESGeoVector.forceUeUpdateProps,
    ];

    static override propValFuncs = {
        ...UeESGeoVector.propValFuncs,
        materialMode: null,
    };
    constructor(sceneObject: T, ueViewer: UeViewer) {
        super(sceneObject, ueViewer);
        const viewer = ueViewer.viewer;
        if (!viewer) {
            console.warn(`viewer is undefined!`);
            return;
        }
        {
            // 兼容旧的属性，后期不支持设置purple和blue
            const update = () => {
                let materialMode = sceneObject.materialMode === "blue"
                    ? "multipleArrows"
                    : sceneObject.materialMode === "purple"
                        ? "singleArrow"
                        : sceneObject.materialMode;
                viewer.callUeFunc({
                    f: 'update',
                    p: {
                        id: sceneObject.id,
                        materialMode: materialMode,
                    }
                })
            };
            this.dispose(sceneObject.materialModeChanged.disposableOn(update));
            this.dispose(sceneObject.ueCreatedEvent.disposableOn(update));
        }
    }
}
