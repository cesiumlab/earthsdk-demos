import { UeESLocalVector2D } from './../../../base/ue/UeESLocalVector2D/index';
import { UeViewer } from "xbsj-xe2/dist-node/xe2-ue-objects";
import { ESLocalRectangle } from "../../objs";

export class UeESLocalRectangle extends UeESLocalVector2D<ESLocalRectangle>{
    static readonly type = this.register(ESLocalRectangle.type, this);
    constructor(sceneObject: ESLocalRectangle, ueViewer: UeViewer) {
        super(sceneObject, ueViewer);
        const viewer = ueViewer.viewer;
        if (!viewer) {
            console.warn(`viewer is undefined!`);
            return;
        }
    }
}