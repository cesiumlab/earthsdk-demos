import { createNextAnimateFrameEvent } from 'xbsj-xe2/dist-node/xe2-base-utils';
import { ESPolygonFence } from '../../';
import { UeViewer } from '../../../base';
import { UeESGeoVector } from '../../../base';

export class UeESPolygonFence extends UeESGeoVector<ESPolygonFence> {
    static readonly type = this.register(ESPolygonFence.type, this);

    constructor(sceneObject: ESPolygonFence, ueViewer: UeViewer) {
        super(sceneObject, ueViewer);

        const viewer = ueViewer.viewer;
        if (!viewer) {
            console.warn(`viewer is undefined!`);
            return;
        }
        // const update = () => {
        //     let fillStyle = ESPolygonFence.defaults.fillStyle
        //     try {
        //         fillStyle = { ...(sceneObject.fillStyle ?? ESPolygonFence.defaults.fillStyle) };
        //     } catch (e) {
        //         console.error('ESPolygonFence fillStyle 属性类型错误!', e)
        //         fillStyle = { ...ESPolygonFence.defaults.fillStyle }
        //     }
        //     fillStyle.materialParams = JSON.stringify(fillStyle.materialParams);
        //     viewer.callUeFunc({
        //         f: 'update',
        //         p: {
        //             id: sceneObject.id,
        //             filled: sceneObject.filled ?? ESPolygonFence.defaults.filled,
        //             fillStyle,
        //             height: sceneObject.height ?? ESPolygonFence.defaults.height,
        //             materialMode: sceneObject.mode ?? ESPolygonFence.defaults.mode,
        //         }
        //     })
        // };
        // const updateEvent = this.disposeVar(createNextAnimateFrameEvent(
        //     sceneObject.fillStyleChanged,
        //     sceneObject.filledChanged,
        //     sceneObject.heightChanged,
        //     sceneObject.modeChanged,
        // ));
        // this.dispose(updateEvent.disposableOn(update));
        // this.dispose(sceneObject.ueCreatedEvent.disposableOn(update));
    }
}
