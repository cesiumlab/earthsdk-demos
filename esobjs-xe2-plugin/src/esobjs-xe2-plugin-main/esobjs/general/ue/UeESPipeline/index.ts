import { ESPipeline } from "../../objs";
import { UeViewer } from "xbsj-xe2/dist-node/xe2-ue-objects";
import { UeESGeoLineString } from "../UeESGeoLineString";

export class UeESPipeline extends UeESGeoLineString<ESPipeline> {
    static override readonly type = this.register(ESPipeline.type, this);
    constructor(sceneObject: ESPipeline, ueViewer: UeViewer) {
        super(sceneObject, ueViewer);

        const viewer = ueViewer.viewer;
        if (!viewer) {
            console.warn(`viewer is undefined!`);
            return;
        }
    }
}