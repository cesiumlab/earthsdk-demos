import { UeViewer } from 'xbsj-xe2/dist-node/xe2-ue-objects';
import { ESEntityCluster } from './../../objs';
import { UeESVisualObject } from '@/esobjs-xe2-plugin-main/esobjs/base';

export class UeESEntityCluster extends UeESVisualObject<ESEntityCluster> {
    static readonly type = this.register(ESEntityCluster.type, this);

    constructor(sceneObject: ESEntityCluster, ueViewer: UeViewer) {
        super(sceneObject, ueViewer);
        const viewer = ueViewer.viewer;
        if (!viewer) {
            console.warn(`viewer is undefined!`);
            return;
        }

        this.d(ueViewer.widgetEvent.don((info) => {
            if (info.objId !== sceneObject.id) return
            //@ts-ignore
            const { type, properties } = info;
            //@ts-ignore
            sceneObject.widgetEvent.emit({ type, properties });
        }))
    }
}