import { ESBoxClipping } from '../../objs';
import { UeViewer } from 'xbsj-xe2/dist-node/xe2-ue-objects';
import { UeESObjectWithLocation } from '../../../base';
export class UeESBoxClipping extends UeESObjectWithLocation<ESBoxClipping>{
    static readonly type = this.register(ESBoxClipping.type, this);
    constructor(sceneObject: ESBoxClipping, ueViewer: UeViewer) {
        super(sceneObject, ueViewer);
        const viewer = ueViewer.viewer;
        if (!viewer) {
            console.warn(`viewer is underfined!`);
            return;
        }
    }
}
