import { UeViewer } from "xbsj-xe2/dist-node/xe2-ue-objects";
import { ESAreaMeasurement } from '../../objs';
import { UeESGeoPolygon } from "../UeESGeoPolygon";
import { createNextAnimateFrameEvent } from "xbsj-xe2/dist-node/xe2-base-utils";

export class UeESAreaMeasurement extends UeESGeoPolygon<ESAreaMeasurement>   {
    static override readonly type = this.register(ESAreaMeasurement.type, this);
    constructor(sceneObject: ESAreaMeasurement, ueViewer: UeViewer) {
        super(sceneObject, ueViewer);
        const viewer = ueViewer.viewer;
        if (!viewer) {
            console.warn(`viewer is undefined!`);
            return;
        }
        const update = () => {
            let fillStyle = ESAreaMeasurement.defaults.fillStyle
            try {
                fillStyle = { ...(sceneObject.fillStyle ?? ESAreaMeasurement.defaults.fillStyle) };
            } catch (e) {
                console.error('ESAreaMeasurement fillStyle 属性类型错误!', e)
                fillStyle = { ...ESAreaMeasurement.defaults.fillStyle }
            }
            fillStyle.materialParams = JSON.stringify(fillStyle.materialParams);
            viewer.callUeFunc({
                f: 'update',
                p: {
                    id: sceneObject.id,
                    stroked: sceneObject.stroked ?? ESAreaMeasurement.defaults.stroked,
                    fillStyle,
                    filled: sceneObject.filled ?? ESAreaMeasurement.defaults.filled,
                }
            })
            console.log(fillStyle);

        };
        const updateEvent = this.disposeVar(createNextAnimateFrameEvent(
            sceneObject.fillStyleChanged,
            sceneObject.strokedChanged,
            sceneObject.filledChanged,
        ));
        this.dispose(updateEvent.disposableOn(update));
        this.dispose(sceneObject.ueCreatedEvent.disposableOn(update));
    }
}
