import { UeViewer } from '../../../base';
import { UeESGeoPolygon } from '../UeESGeoPolygon';
import { ESPolygonWithHole } from '../../objs';

export class UeESPolygonPrimitive extends UeESGeoPolygon<ESPolygonWithHole> {
    static override readonly type = this.register(ESPolygonWithHole.type, this);

    constructor(sceneObject: ESPolygonWithHole, ueViewer: UeViewer) {
        super(sceneObject, ueViewer);

        const viewer = ueViewer.viewer;
        if (!viewer) {
            console.warn(`viewer is undefined!`);
            return;
        }
    }
}
