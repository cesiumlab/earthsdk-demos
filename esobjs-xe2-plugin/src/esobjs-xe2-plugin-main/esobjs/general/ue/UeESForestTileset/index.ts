import { createNextAnimateFrameEvent, reactJson } from "xbsj-xe2/dist-node/xe2-base-utils";
import { ESForestTileset } from '../../objs';
import { UeViewer } from "xbsj-xe2/dist-node/xe2-ue-objects";
import { UeESVisualObject, ESSceneObject, ESJResource } from "../../../base";

export class UeESForestTileset extends UeESVisualObject<ESForestTileset> {
    static readonly type = this.register(ESForestTileset.type, this);
    constructor(sceneObject: ESForestTileset, ueViewer: UeViewer) {
        super(sceneObject, ueViewer);

        const viewer = ueViewer.viewer;
        if (!viewer) {
            console.warn(`viewer is undefined!`);
            return;
        }
        const urlReact = this.ad(reactJson<ESJResource | string>(""))
        {
            const update = () => {
                if (typeof sceneObject.url === 'string') {
                    urlReact.value = ESSceneObject.context.getStrFromEnv(sceneObject.url);
                } else {
                    const temp = sceneObject.url;
                    temp.url = ESSceneObject.context.getStrFromEnv(temp.url);
                    urlReact.value = temp;
                }
            }
            update();
            this.ad(sceneObject.urlChanged.don(update))
        }
        const update = () => {
            viewer.callUeFunc({
                f: 'update',
                p: {
                    id: sceneObject.id,
                    url: urlReact.value ?? ESForestTileset.defaults.url,
                    treeTypes: sceneObject.treeTypes ?? ESForestTileset.defaults.treeTypes,
                    xiaoBanWidget: sceneObject.xiaoBanWidget ?? ESForestTileset.defaults.xiaoBanWidgetDefault,
                    youShiSZ: sceneObject.youShiSZ ?? ESForestTileset.defaults.youShiSZ,
                    diLei: sceneObject.diLei ?? ESForestTileset.defaults.diLei,
                    senLinLB: sceneObject.senLinLB ?? ESForestTileset.defaults.senLinLB,
                    labelMaterial: sceneObject.labelMaterial ?? ESForestTileset.defaults.labelMaterial,
                    heightOffset: sceneObject.heightOffset ?? ESForestTileset.defaults.heightOffset,
                }
            })
        };
        const updateEvent = this.disposeVar(createNextAnimateFrameEvent(
            urlReact.changed,
            sceneObject.treeTypesChanged,
            sceneObject.xiaoBanWidgetChanged,
            sceneObject.youShiSZChanged,
            sceneObject.diLeiChanged,
            sceneObject.senLinLBChanged,
            sceneObject.labelMaterialChanged,
            sceneObject.heightOffsetChanged,
        ));
        this.dispose(updateEvent.disposableOn(update));
        this.dispose(sceneObject.ueCreatedEvent.disposableOn(update));
    }
}
