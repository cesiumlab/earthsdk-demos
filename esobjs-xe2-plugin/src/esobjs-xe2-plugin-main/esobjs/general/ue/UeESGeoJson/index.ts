import { UeESVisualObject } from '@/esobjs-xe2-plugin-main/esobjs/base';
import { ESGeoJson } from './../../objs';
import { UeViewer } from 'xbsj-xe2/dist-node/xe2-ue-objects';

export class UeESGeoJson extends UeESVisualObject<ESGeoJson> {
    static readonly type = this.register(ESGeoJson.type, this);

    constructor(sceneObject: ESGeoJson, ueViewer: UeViewer) {
        super(sceneObject, ueViewer);

        const viewer = ueViewer.viewer;
        if (!viewer) {
            console.warn(`viewer is undefined!`);
            return;
        }
        this.dispose(sceneObject.flyToFeatureIndexEvent.disposableOn((index, duration) => {
            viewer.callUeFunc({
                f: 'flyToFeatureIndex',
                p: {
                    id: sceneObject.id,
                    index,
                    duration
                }
            })
        }))
        this.dispose(sceneObject.flyToFeatureEvent.disposableOn((key, value, duration) => {
            viewer.callUeFunc({
                f: 'flyToFeature',
                p: {
                    id: sceneObject.id,
                    key,
                    value,
                    duration
                }
            })
        }))
    }
}