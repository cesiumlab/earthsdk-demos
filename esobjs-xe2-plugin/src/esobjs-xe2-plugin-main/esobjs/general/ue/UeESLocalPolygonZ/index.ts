import { SceneObjectPickedInfo } from 'xbsj-xe2/dist-node/xe2-base-objects';
import { createNextAnimateFrameEvent } from "xbsj-xe2/dist-node/xe2-base-utils";
import { UeObject, UeViewer } from "xbsj-xe2/dist-node/xe2-ue-objects";
import { calcFlyToParamCallFunc, destroyCallFunc, flyToCallFunc } from "xbsj-xe2/dist-node/xe2-ue-objects";
import { ESLocalPolygonZ } from '../../objs';
import { ESGeoVector, ESVisualObject } from "../../../base";

export class UeESLocalPolygonZ extends UeObject<ESLocalPolygonZ> {
    static readonly type = this.register(ESLocalPolygonZ.type, this);
    constructor(sceneObject: ESLocalPolygonZ, ueViewer: UeViewer) {
        super(sceneObject, ueViewer);

        const viewer = ueViewer.viewer;
        if (!viewer) {
            console.warn(`viewer is undefined!`);
            return;
        }

        // const update = () => {
        //     viewer.callUeFunc({
        //         f: 'update',
        //         p: {
        //             id: sceneObject.id,
        //             name: sceneObject.name,
        //             points: sceneObject.points ?? [],
        //             show: sceneObject.show ?? true,
        //             collision: sceneObject.collision ?? true,
        //             allowPicking: sceneObject.allowPicking ?? false,
        //             flyToParam: sceneObject.flyToParam ?? ESVisualObject.defaults.flyToParam,

        //             stroked: sceneObject.stroked ?? false,
        //             strokeStyle: sceneObject.strokeStyle ?? ESGeoVector.defaults.strokeStyle,
        //             pointStyle: sceneObject.pointStyle ?? ESGeoVector.defaults.pointStyle,
        //             pointed: sceneObject.pointed ?? false,
        //             fillStyle: sceneObject.fillStyle ?? ESGeoVector.defaults.fillStyle,
        //             filled: sceneObject.filled ?? true,

        //             position: sceneObject.position ?? [0, 0, 0],
        //             rotation: sceneObject.rotation ?? [0, 0, 0],
        //             editing: sceneObject.editing ?? false,
        //         }
        //     })
        // };
        // const updateEvent = this.disposeVar(createNextAnimateFrameEvent(
        //     sceneObject.showChanged,
        //     sceneObject.pointsChanged,
        //     sceneObject.positionChanged,
        //     sceneObject.rotationChanged,
        //     sceneObject.flyToParamChanged,

        //     sceneObject.editingChanged,
        //     sceneObject.strokeStyleChanged,
        //     sceneObject.strokedChanged,
        //     sceneObject.pointStyleChanged,
        //     sceneObject.pointedChanged,
        //     sceneObject.fillStyleChanged,
        //     sceneObject.filledChanged,
        //     sceneObject.collisionChanged,
        //     sceneObject.allowPickingChanged,
        //     sceneObject.nameChanged,
        // ));
        // this.dispose(updateEvent.disposableOn(update));

        // let created = false;
        // this.dispose(() => {
        //     if (created) {
        //         destroyCallFunc(viewer, sceneObject.id)
        //     }
        // });

        // viewer.callUeFunc({
        //     f: 'create',
        //     p: {
        //         type: 'ESLocalPolygonZ',
        //         id: sceneObject.id,
        //     }
        // }).then(() => {
        //     created = true;
        //     update();
        // }).catch(err => console.error(err));

        this.dispose(sceneObject.flyToEvent.disposableOn(async (duration, id) => {
            const res = await flyToCallFunc(viewer, sceneObject.id, duration)
            let mode: 'cancelled' | 'over' | 'error' = 'over';
            if (res === undefined) {
                mode = 'error'
            } else if (res.endType === 0) {
                mode = 'over'
            } else if (res.endType === 1) {
                mode = 'cancelled'
            }
            sceneObject.flyOverEvent.emit(id, mode, ueViewer);
        }));

        this.dispose(sceneObject.calcFlyToParamEvent.disposableOn(() => {
            calcFlyToParamCallFunc(viewer, sceneObject.id)
        }));
        this.dispose(sceneObject.calcFlyInParamEvent.disposableOn(async () => {
            if (!ueViewer.actived) return;
            const cameraInfo = await ueViewer.getCurrentCameraInfo();
            if (!cameraInfo) return;
            const { position, rotation } = cameraInfo;
            sceneObject.flyInParam = { position, rotation, flyDuration: 1 };
        }));


        this.dispose(ueViewer.propChanged.disposableOn((info) => {
            if (info.objId !== sceneObject.id) return
            Object.keys(info.props).forEach(key => {
                const prop = info.props[key] === null ? undefined : info.props[key]
                //@ts-ignore
                sceneObject[key] = prop
            });
        }));

        this.dispose(ueViewer.uePickedEvent.disposableOn((uePickInfo) => {
            if (!uePickInfo) return;
            const { uePickResult } = uePickInfo;
            if (!(uePickResult && uePickResult.id)) return;
            if (uePickResult.id === sceneObject.id) {
                if (sceneObject.allowPicking ?? false) {
                    sceneObject.pickedEvent.emit(new SceneObjectPickedInfo(sceneObject, uePickInfo));
                } else {
                    //@ts-ignore
                    ueViewer.sceneObjectNotAllowPickEvent.emit(uePickInfo);;
                }
            }
        }));

    }
}
