import { ESGeoRectangle } from '../../objs';
import { UeViewer } from "xbsj-xe2/dist-node/xe2-ue-objects";
import { UeESGeoVector } from '../../../base';

export class UeESGeoRectangle<T extends ESGeoRectangle = ESGeoRectangle> extends UeESGeoVector<T>  {
    static readonly type = this.register<ESGeoRectangle>(ESGeoRectangle.type, this);
    constructor(sceneObject: T, ueViewer: UeViewer) {
        super(sceneObject, ueViewer);

    }
}