import { UeViewer } from 'xbsj-xe2/dist-node/xe2-ue-objects';
import { ESSunshineAnalysis } from './../../objs';
import { UeESGeoPolygon } from '../UeESGeoPolygon';

export class UeESSunshineAnalysis extends UeESGeoPolygon<ESSunshineAnalysis> {
    static override readonly type = this.register(ESSunshineAnalysis.type, this);

    constructor(sceneObject: ESSunshineAnalysis, ueViewer: UeViewer) {
        super(sceneObject, ueViewer);
        const viewer = ueViewer.viewer;
        if (!viewer) {
            console.warn(`viewer is undefined!`);
            return;
        }
        this.d(sceneObject.startEvent.don(() => {
            viewer.callUeFunc({
                f: 'Start',
                p: {
                    id: sceneObject.id
                }
            })
        }))
        this.d(sceneObject.stopEvent.don(() => {
            viewer.callUeFunc({
                f: "Stop",
                p: {
                    id: sceneObject.id
                }
            })
        }))
    }
}