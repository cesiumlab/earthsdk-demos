import { UeViewer } from 'xbsj-xe2/dist-node/xe2-ue-objects';
import { ESPoi2D } from './../../objs';
import { UeESLabel } from '@/esobjs-xe2-plugin-main/esobjs/base';

export class UeESPoi2D extends UeESLabel<ESPoi2D> {
    static readonly type = this.register(ESPoi2D.type, this);

    constructor(sceneObject: ESPoi2D, ueViewer: UeViewer) {
        super(sceneObject, ueViewer);
        const viewer = ueViewer.viewer;
        if (!viewer) {
            console.warn(`viewer is undefined!`);
            return;
        }
        this.ad(sceneObject.getInitialStyleEvent.don(async () => {
            try {
                const res: any = await viewer.callUeFunc({
                    f: 'GetInitialStyle',
                    p: { id: sceneObject.id },
                })
                if (res.error != "") console.error(`getInitialStyleEvent:`, res.error);
                sceneObject.initialStyle.value = structuredClone(res.re);
            } catch (error) {
                console.error(`getInitialStyleEvent:`, error);
                sceneObject.initialStyle.value = undefined;
            }
        }))
    }
}