import { UeESObjectWithLocation, UeViewer } from "@/esobjs-xe2-plugin-main/esobjs/base";
import { ESFireParticleSystem } from "../../objs";

export class UeESFireParticleSystem extends UeESObjectWithLocation<ESFireParticleSystem>{
    static readonly type = this.register(ESFireParticleSystem.type,this);
    constructor(sceneObject:ESFireParticleSystem,ueViewer:UeViewer){
        super(sceneObject,ueViewer);
        const viewer = ueViewer.viewer;
        if(!viewer){
            console.warn('viewer is undefined!');
            return;            
        }
    }
}