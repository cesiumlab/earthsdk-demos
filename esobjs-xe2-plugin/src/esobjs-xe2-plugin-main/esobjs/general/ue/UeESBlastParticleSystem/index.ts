import { UeESObjectWithLocation, UeViewer } from "@/esobjs-xe2-plugin-main/esobjs/base";
import { ESBlastParticleSystem } from "../../objs";

export class UeESBlastParticleSystem extends UeESObjectWithLocation<ESBlastParticleSystem>{
    static readonly type = this.register(ESBlastParticleSystem.type,this);
    constructor(sceneObject:ESBlastParticleSystem,ueViewer:UeViewer){
        super(sceneObject,ueViewer);
        const viewer = ueViewer.viewer;
        if(!viewer){
            console.warn('viewer is undefined!');
            return;            
        }
    }
}