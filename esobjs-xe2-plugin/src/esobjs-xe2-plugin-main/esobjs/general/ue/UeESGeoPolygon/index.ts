import { createNextAnimateFrameEvent } from "xbsj-xe2/dist-node/xe2-base-utils";
import { ESGeoPolygon } from '../../objs';
import { UeViewer } from "xbsj-xe2/dist-node/xe2-ue-objects";
import { UeESGeoVector } from '../../../base';

export class UeESGeoPolygon<T extends ESGeoPolygon = ESGeoPolygon> extends UeESGeoVector<T>   {
    static readonly type = this.register<ESGeoPolygon>(ESGeoPolygon.type, this);
    constructor(sceneObject: T, ueViewer: UeViewer) {
        super(sceneObject, ueViewer);
        const viewer = ueViewer.viewer;
        if (!viewer) {
            console.warn(`viewer is undefined!`);
            return;
        }
        // const update = () => {
        //     let fillStyle = ESGeoPolygon.defaults.fillStyle
        //     try {
        //         fillStyle = { ...(sceneObject.fillStyle ?? ESGeoPolygon.defaults.fillStyle) };
        //     } catch (e) {
        //         console.error('ESGeoPolygon fillStyle 属性类型错误!', e)
        //         fillStyle = { ...ESGeoPolygon.defaults.fillStyle }
        //     }
        //     fillStyle.materialParams = JSON.stringify(fillStyle.materialParams);
        //     viewer.callUeFunc({
        //         f: 'update',
        //         p: {
        //             id: sceneObject.id,
        //             filled: sceneObject.filled ?? ESGeoPolygon.defaults.filled,
        //             fillStyle
        //         }
        //     })
        // };
        // const updateEvent = this.disposeVar(createNextAnimateFrameEvent(
        //     sceneObject.fillStyleChanged,
        //     sceneObject.filledChanged,
        // ));
        // this.dispose(updateEvent.disposableOn(update));
        // this.dispose(sceneObject.ueCreatedEvent.disposableOn(update));
    }
}
