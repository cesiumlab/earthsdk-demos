import { UeESObjectWithLocation } from "../../../base";
import { ESLocalSkyBox } from "../../objs";
import { UeViewer } from "xbsj-xe2/dist-node/xe2-ue-objects";

export class UeESLocalSkyBox extends UeESObjectWithLocation<ESLocalSkyBox>{
    static readonly type = this.register(ESLocalSkyBox.type, this);
    constructor(sceneObject: ESLocalSkyBox, ueViewer: UeViewer) {
        super(sceneObject, ueViewer);
        const viewer = ueViewer.viewer;
        if (!viewer) {
            console.warn(`viewer is undefined!`);
            return;
        }
    }
}