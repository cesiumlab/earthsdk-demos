import { ESImageLabel } from '../../objs';
import { ESSceneObject, rpToap } from "../../../base";
import { UeViewer } from "xbsj-xe2/dist-node/xe2-ue-objects";
import { UeESLabel } from '../../..';

export class UeESImageLabel<T extends ESImageLabel = ESImageLabel> extends UeESLabel<T> {
    static readonly type = this.register<ESImageLabel>(ESImageLabel.type, this);

    static override forceUeUpdateProps = [
        ...UeESLabel.forceUeUpdateProps,
        'url',
    ];

    static override  propValFuncs = {
        ...UeESLabel.propValFuncs,
        url: (val: string) => {
            if (typeof val === 'string') return ESSceneObject.context.getStrFromEnv(rpToap(val))
            // @ts-ignore
            val.url = ESSceneObject.context.getStrFromEnv(rpToap(val.url)
            );
            return val;
        },
    };

    constructor(sceneObject: T, ueViewer: UeViewer) {
        super(sceneObject, ueViewer);

        const viewer = ueViewer.viewer;
        if (!viewer) {
            console.warn(`viewer is undefined!`);
            return;
        }
        sceneObject.anchor = [0.5, 1]

        // const urlReact = this.disposeVar(ESSceneObject.context.createEvnStrReact([sceneObject, 'url'], ESImageLabel.defaults.url));
        // const update = () => {
        //     viewer.callUeFunc({
        //         f: 'update',
        //         p: {
        //             id: sceneObject.id,
        //             url: urlReact.value,
        //         }
        //     })
        // };

        // this.dispose(sceneObject.ueCreatedEvent.disposableOn(update));

        // this.dispose(urlReact.changed.disposableOn(update));
    }
}
