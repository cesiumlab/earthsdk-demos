import { ESExcavate } from "../../objs";
import { UeViewer } from "xbsj-xe2/dist-node/xe2-ue-objects";
import { UeESGeoPolygon } from "../UeESGeoPolygon";

export class UeESExcavate extends UeESGeoPolygon<ESExcavate>{
    static override readonly type = this.register(ESExcavate.type, this);
    constructor(sceneObject: ESExcavate, ueViewer: UeViewer) {
        super(sceneObject, ueViewer);

        const viewer = ueViewer.viewer;
        if (!viewer) {
            console.warn(`viewer is undefined!`);
            return;
        }
    }
}