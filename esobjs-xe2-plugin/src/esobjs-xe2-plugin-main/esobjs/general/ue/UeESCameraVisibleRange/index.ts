import { createNextAnimateFrameEvent } from "xbsj-xe2/dist-node/xe2-base-utils";
import { ESCameraVisibleRange } from '../../objs';
import { UeViewer } from "xbsj-xe2/dist-node/xe2-ue-objects";
import { UeESObjectWithLocation } from "../../../base";
export class UeESCameraVisibleRange extends UeESObjectWithLocation<ESCameraVisibleRange> {
    static readonly type = this.register(ESCameraVisibleRange.type, this);
    constructor(sceneObject: ESCameraVisibleRange, ueViewer: UeViewer) {
        super(sceneObject, ueViewer);

        const viewer = ueViewer.viewer;
        if (!viewer) {
            console.warn(`viewer is undefined!`);
            return;
        }
        
        // const update = () => {
        //     viewer.callUeFunc({
        //         f: 'update',
        //         p: {
        //             id: sceneObject.id,
        //             fov: sceneObject.fov ?? ESCameraVisibleRange.defaults.fov,
        //             aspectRatio: sceneObject.aspectRatio ?? ESCameraVisibleRange.defaults.aspectRatio,
        //             farClipDistance: sceneObject.farClipDistance ?? ESCameraVisibleRange.defaults.farClipDistance,
        //         }
        //     })
        // };

        // const updateEvent = this.disposeVar(createNextAnimateFrameEvent(
        //     sceneObject.fovChanged,
        //     sceneObject.aspectRatioChanged,
        //     sceneObject.farClipDistanceChanged,
        // ));
        // this.dispose(updateEvent.disposableOn(update));
        // this.dispose(sceneObject.ueCreatedEvent.disposableOn(update));
    }
}
