
import { ESDistanceMeasurement } from '../../objs';
import { UeViewer } from "xbsj-xe2/dist-node/xe2-ue-objects";
import { UeESGeoLineString } from "../UeESGeoLineString";
import { createNextAnimateFrameEvent } from "xbsj-xe2/dist-node/xe2-base-utils";
import { UeESGeoVector } from "../../../base";
export class UeESDistanceMeasurement extends UeESGeoVector<ESDistanceMeasurement>   {
    static readonly type = this.register(ESDistanceMeasurement.type, this);
    constructor(sceneObject: ESDistanceMeasurement, ueViewer: UeViewer) {
        super(sceneObject, ueViewer);
        const viewer = ueViewer.viewer;
        if (!viewer) {
            console.warn(`viewer is undefined!`);
            return;
        }
        const update = () => {
            let strokeStyle;
            try {
                strokeStyle = { ...(sceneObject.strokeStyle ?? ESDistanceMeasurement.defaults.strokeStyle) };
            } catch (e) {
                console.error('ESDistanceMeasurement strokeStyle 属性类型错误!', e)
                strokeStyle = { ...ESDistanceMeasurement.defaults.strokeStyle }
            }
            viewer.callUeFunc({
                f: 'update',
                p: {
                    id: sceneObject.id,
                    strokeStyle
                }
            })
        };
        const updateEvent = this.disposeVar(createNextAnimateFrameEvent(
            sceneObject.strokeStyleChanged,
        ));
        this.dispose(updateEvent.disposableOn(update));
        this.dispose(sceneObject.ueCreatedEvent.disposableOn(update));
    }
}
