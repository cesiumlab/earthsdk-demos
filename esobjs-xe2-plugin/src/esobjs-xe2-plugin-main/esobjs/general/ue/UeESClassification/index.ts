import { createNextAnimateFrameEvent } from 'xbsj-xe2/dist-node/xe2-base-utils';
import { ESClassification } from '../../objs';
import { UeViewer } from '../../../base';
import { UeESGeoVector } from '../../../base';
export class UeESClassification extends UeESGeoVector<ESClassification> {
    static readonly type = this.register(ESClassification.type, this);
    constructor(sceneObject: ESClassification, ueViewer: UeViewer) {
        super(sceneObject, ueViewer);
        const viewer = ueViewer.viewer;
        if (!viewer) {
            console.warn(`viewer is undefined!`);
            return;
        }
        const update = () => {
            let fillStyle = ESClassification.defaults.fillStyle
            try {
                fillStyle = { ...(sceneObject.fillStyle ?? ESClassification.defaults.fillStyle) };
            } catch (e) {
                console.error('ESClassification fillStyle 属性类型错误!', e)
                fillStyle = { ...ESClassification.defaults.fillStyle }
            }
            fillStyle.materialParams = JSON.stringify(fillStyle.materialParams);
            viewer.callUeFunc({
                f: 'update',
                p: {
                    id: sceneObject.id,
                    filled: sceneObject.filled ?? ESClassification.defaults.filled,
                    fillStyle: fillStyle
                }
            })
        };
        const updateEvent = this.disposeVar(createNextAnimateFrameEvent(
            sceneObject.fillStyleChanged,
            sceneObject.filledChanged,
        ));
        
        this.dispose(updateEvent.disposableOn(update));
        this.dispose(sceneObject.ueCreatedEvent.disposableOn(update));
    }
}
