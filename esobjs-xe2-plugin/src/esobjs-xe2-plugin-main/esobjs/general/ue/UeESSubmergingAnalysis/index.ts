import { UeViewer } from 'xbsj-xe2/dist-node/xe2-ue-objects';
import { ESSubmergingAnalysis } from './../../objs';
import { UeESObjectWithLocation } from '@/esobjs-xe2-plugin-main/esobjs/base';

export class UeESSubmergingAnalysis extends UeESObjectWithLocation<ESSubmergingAnalysis> {
    static readonly type = this.register(ESSubmergingAnalysis.type, this);

    constructor(sceneObject: ESSubmergingAnalysis, ueViewer: UeViewer) {
        super(sceneObject, ueViewer);
        const viewer = ueViewer.viewer;
        if (!viewer) {
            console.warn(`viewer is undefined!`);
            return;
        }
    }
}