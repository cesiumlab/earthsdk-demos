import { ESCar } from '../../objs';
import { UeViewer } from "xbsj-xe2/dist-node/xe2-ue-objects";
import { UeESObjectWithLocation } from "../../../base";
export class UeESCar extends UeESObjectWithLocation<ESCar> {
    static readonly type = this.register(ESCar.type, this);
    constructor(sceneObject: ESCar, ueViewer: UeViewer) {
        super(sceneObject, ueViewer);
        const viewer = ueViewer.viewer;
        if (!viewer) {
            console.warn(`viewer is undefined!`);
            return;
        }
    }
}
