import { ESVisibilityAnalysis } from '../../objs';
import { UeViewer } from 'xbsj-xe2/dist-node/xe2-ue-objects';
import { UeESGeoVector } from '../../../base';
export class UeESVisibilityAnalysis extends UeESGeoVector<ESVisibilityAnalysis>{
    static readonly type = this.register(ESVisibilityAnalysis.type, this);
    constructor(sceneObject: ESVisibilityAnalysis, ueViewer: UeViewer) {
        super(sceneObject, ueViewer);
        const viewer = ueViewer.viewer;
        if (!viewer) {
            console.warn(`viewer is underfined!`);
            return;
        }
    }
}
