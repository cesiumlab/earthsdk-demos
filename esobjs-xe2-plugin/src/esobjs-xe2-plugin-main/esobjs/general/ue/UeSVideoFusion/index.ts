import { UeViewer } from "xbsj-xe2/dist-node/xe2-ue-objects";
import { UeESObjectWithLocation } from "../../../base";
import { ESVideoFusion } from '../../objs';
export class UeESVideoFusion extends UeESObjectWithLocation<ESVideoFusion> {
    static readonly type = this.register(ESVideoFusion.type, this);
    constructor(sceneObject: ESVideoFusion, ueViewer: UeViewer) {
        super(sceneObject, ueViewer);

        const viewer = ueViewer.viewer;
        if (!viewer) {
            console.warn(`viewer is undefined!`);
            return;
        }
    }
}
