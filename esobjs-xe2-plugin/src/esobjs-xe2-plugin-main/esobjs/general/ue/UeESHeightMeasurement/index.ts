import { UeViewer } from "xbsj-xe2/dist-node/xe2-ue-objects";
import { UeESGeoVector } from "../../../base";
import { ESHeightMeasurement } from '../../objs';
export class UeESHeightMeasurement extends UeESGeoVector<ESHeightMeasurement> {
    static readonly type = this.register(ESHeightMeasurement.type, this);
    constructor(sceneObject: ESHeightMeasurement, ueViewer: UeViewer) {
        super(sceneObject, ueViewer);

        const viewer = ueViewer.viewer;
        if (!viewer) {
            console.warn(`viewer is undefined!`);
            return;
        }
    }
}
