import { ESTextLabel } from '../../objs';
import { UeViewer, UeESLabel } from '../../../base';

export class UeESTextLabel extends UeESLabel<ESTextLabel> {
    static readonly type = this.register(ESTextLabel.type, this);
    constructor(sceneObject: ESTextLabel, ueViewer: UeViewer) {
        super(sceneObject, ueViewer);

        const viewer = ueViewer.viewer;
        if (!viewer) {
            console.warn(`viewer is undefined!`);
            return;
        }

        // const update = () => {
        //     viewer.callUeFunc({
        //         f: 'update',
        //         p: {
        //             id: sceneObject.id,
        //             text: sceneObject.text ?? ESTextLabel.defaults.text,
        //             color: sceneObject.color ?? ESTextLabel.defaults.color,
        //             fontSize: sceneObject.fontSize ?? ESTextLabel.defaults.fontSize,
        //         }
        //     })
        // };
        // const updateEvent = this.disposeVar(createNextAnimateFrameEvent(
        //     sceneObject.textChanged,
        //     sceneObject.colorChanged,
        //     sceneObject.fontSizeChanged,
        // ));
        // this.dispose(updateEvent.disposableOn(update));
        // this.dispose(sceneObject.ueCreatedEvent.disposableOn(update));
    }
}
