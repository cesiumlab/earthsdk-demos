import { ESHuman } from '../../objs';
import { UeViewer } from "xbsj-xe2/dist-node/xe2-ue-objects";
import { UeESObjectWithLocation } from "../../../base";
export class UeESHuman extends UeESObjectWithLocation<ESHuman> {
    static readonly type = this.register(ESHuman.type, this);
    constructor(sceneObject: ESHuman, ueViewer: UeViewer) {
        super(sceneObject, ueViewer);

        const viewer = ueViewer.viewer;
        if (!viewer) {
            console.warn(`viewer is undefined!`);
            return;
        }
        // const update = () => {
        //     viewer.callUeFunc({
        //         f: 'update',
        //         p: {
        //             id: sceneObject.id,
        //             mode: sceneObject.mode ?? ESHuman.defaults.mode,
        //             animation: sceneObject.animation ?? ESHuman.defaults.animation,
        //         }
        //     })
        // };
        // this.dispose(sceneObject.modeChanged.disposableOn(update));
        // this.dispose(sceneObject.animationChanged.disposableOn(update));
        // this.dispose(sceneObject.ueCreatedEvent.disposableOn(update));

        type AIMoveToType = {
            AIMoveTo: {
                params: {
                    id: string,
                    Destination: [number, number, number],
                    Time: number
                },
                result: {
                    error: string | undefined;
                }
            }
        }

        const aiMoveToCallFunc = async (ueViewer: UeViewer, id: string, Destination: [number, number, number], Time: number) => {
            const { viewer } = ueViewer;
            if (!viewer) {
                console.error(`AIMoveTo: ueViewer.viewer is undefined`);
                return undefined;
            }
            const res = await viewer.callUeFunc<AIMoveToType['AIMoveTo']['result']>({
                // @ts-ignore // TODO(ysp)
                f: 'AIMoveTo',
                p: { id, Destination, Time }
            })
            if (res.error) console.error(`AIMoveTo:`, res.error);
            return res;
        }

        this.dispose(sceneObject.aiMoveToEvent.disposableOn((Destination, Time) => {
            aiMoveToCallFunc(ueViewer, sceneObject.id, Destination, Time)
        }))

        type StopAIMoveType = {
            StopAIMove: {
                params: {
                    id: string,
                },
                result: {
                    error: string | undefined;
                }
            }
        }
        const StopAIMoveCallFunc = async (ueViewer: UeViewer, id: string) => {
            const { viewer } = ueViewer;
            if (!viewer) {
                console.error(`StopAIMove: ueViewer.viewer is undefined`);
                return undefined;
            }
            const res = await viewer.callUeFunc<StopAIMoveType['StopAIMove']['result']>({
                f: 'StopAIMove',
                p: { id }
            })
            if (res.error) console.error(`StopAIMove:`, res.error);
            return res;
        }

        this.dispose(sceneObject.stopAIMoveEvent.disposableOn(() => {
            StopAIMoveCallFunc(ueViewer, sceneObject.id)
        }))

    }
}
