import { UeViewer } from "xbsj-xe2/dist-node/xe2-ue-objects";
import { UeESObjectWithLocation } from "../../../base";
import { ESGaussianSplatting } from '../../objs';
export class UeESGaussianSplatting extends UeESObjectWithLocation<ESGaussianSplatting> {
    static readonly type = this.register(ESGaussianSplatting.type, this);
    constructor(sceneObject: ESGaussianSplatting, ueViewer: UeViewer) {
        super(sceneObject, ueViewer);

        const viewer = ueViewer.viewer;
        if (!viewer) {
            console.warn(`viewer is undefined!`);
            return;
        }
    }
}
