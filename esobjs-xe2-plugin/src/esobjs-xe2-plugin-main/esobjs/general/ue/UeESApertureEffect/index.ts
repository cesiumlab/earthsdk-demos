import { ESApertureEffect } from "../../objs";
import { UeViewer } from "xbsj-xe2/dist-node/xe2-ue-objects";
import { UeESObjectWithLocation } from "../../../base";

export class UeESApertureEffect extends UeESObjectWithLocation<ESApertureEffect>{
    static readonly type = this.register(ESApertureEffect.type, this);
    constructor(sceneObject: ESApertureEffect, ueViewer: UeViewer) {
        super(sceneObject, ueViewer);

        const viewer = ueViewer.viewer;
        if (!viewer) {
            console.warn(`viewer is undefined!`);
            return;
        }
    }
}