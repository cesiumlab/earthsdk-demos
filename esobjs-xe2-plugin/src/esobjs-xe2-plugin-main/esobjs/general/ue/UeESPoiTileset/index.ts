import { createNextAnimateFrameEvent } from "xbsj-xe2/dist-node/xe2-base-utils";
import { UeViewer } from "xbsj-xe2/dist-node/xe2-ue-objects";
import { ESSceneObject, UeESVisualObject } from "../../../base";
import { ESPoiTileset } from '../../objs';

export class UeESPoiTileset extends UeESVisualObject<ESPoiTileset> {
    static readonly type = this.register(ESPoiTileset.type, this);

    static override forceUeUpdateProps = [
        ...UeESVisualObject.forceUeUpdateProps,
        'url',
    ];

    static override propValFuncs = {
        ...UeESVisualObject.propValFuncs,
        url: (val: string) => {
            if (typeof val === 'string') return ESSceneObject.context.getStrFromEnv(val)
            // @ts-ignore
            val.url = ESSceneObject.context.getStrFromEnv(val.url
            );
            return val;
        },
    };

    constructor(sceneObject: ESPoiTileset, ueViewer: UeViewer) {
        super(sceneObject, ueViewer);
        const viewer = ueViewer.viewer;
        if (!viewer) {
            console.warn(`viewer is undefined!`);
            return;
        }
        // const urlReact = this.disposeVar(ESSceneObject.context.createEvnStrReact([sceneObject, 'url']));
        // const update = () => {
        //     viewer.callUeFunc({
        //         f: 'update',
        //         p: {
        //             id: sceneObject.id,
        //             url: urlReact.value ?? ESPoiTileset.defaults.url,
        //             poiTypes: sceneObject.poiTypes ?? ESPoiTileset.defaults.poiTypes,
        //             heightOffset: sceneObject.heightOffset ?? ESPoiTileset.defaults.heightOffset,
        //         }
        //     })
        // };
        // const updateEvent = this.disposeVar(createNextAnimateFrameEvent(
        //     urlReact.changed,
        //     sceneObject.poiTypesChanged,
        //     sceneObject.heightOffsetChanged,
        // ));
        // this.dispose(updateEvent.disposableOn(update));
        // this.dispose(sceneObject.ueCreatedEvent.disposableOn(update));

    }
}
