import { ESGeoExtrudedPolygon } from "../../objs";
import { UeViewer } from "xbsj-xe2/dist-node/xe2-ue-objects";
import { UeESGeoPolygon } from "../UeESGeoPolygon";
import { createNextAnimateFrameEvent } from "xbsj-xe2/dist-node/xe2-utils";

export class UeESGeoExtrudedPolygon extends UeESGeoPolygon<ESGeoExtrudedPolygon> {
    static override readonly type = this.register(ESGeoExtrudedPolygon.type, this);
    constructor(sceneObject: ESGeoExtrudedPolygon, ueViewer: UeViewer) {
        super(sceneObject, ueViewer);

        const viewer = ueViewer.viewer;
        if (!viewer) {
            console.warn(`viewer is undefined!`);
            return;
        }
        const update = () => {
            viewer.callUeFunc({
                f: 'update',
                p: {
                    id: sceneObject.id,
                    height: sceneObject.height ?? ESGeoExtrudedPolygon.defaults.height,
                    extrudedHeight: sceneObject.extrudedHeight ?? ESGeoExtrudedPolygon.defaults.extrudedHeight,
                    perPositionHeight: sceneObject.perPositionHeight ?? ESGeoExtrudedPolygon.defaults.perPositionHeight,
                }
            })
        };
        const updateEvent = this.disposeVar(createNextAnimateFrameEvent(
            sceneObject.heightChanged,
            sceneObject.extrudedHeightChanged,
            sceneObject.perPositionHeightChanged
        ));
        this.dispose(updateEvent.disposableOn(update));
        this.dispose(sceneObject.ueCreatedEvent.disposableOn(update));
    }
}