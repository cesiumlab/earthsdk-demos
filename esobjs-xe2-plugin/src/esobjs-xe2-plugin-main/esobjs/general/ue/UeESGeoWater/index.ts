import { ESGeoWater } from './../../objs';
import { UeViewer } from 'xbsj-xe2/dist-node/xe2-ue-objects';
import { UeESGeoPolygon } from '../UeESGeoPolygon';

export class UeESGeoWater extends UeESGeoPolygon<ESGeoWater> {
    static override readonly type = this.register(ESGeoWater.type, this);

    constructor(sceneObject: ESGeoWater, ueViewer: UeViewer) {
        super(sceneObject, ueViewer);
        
        const viewer = ueViewer.viewer;
        if (!viewer) {
            console.warn(`viewer is undefined!`);
            return;
        }
    }
}