import { ESPolygonFlattenedPlane } from "../../objs";
import { UeViewer } from "xbsj-xe2/dist-node/xe2-ue-objects";
import { UeESGeoPolygon } from "../UeESGeoPolygon";

export class UeESPolygonFlattenedPlane extends UeESGeoPolygon<ESPolygonFlattenedPlane>{
    static override readonly type = this.register(ESPolygonFlattenedPlane.type, this);
    constructor(sceneObject: ESPolygonFlattenedPlane, ueViewer: UeViewer) {
        super(sceneObject, ueViewer);

        const viewer = ueViewer.viewer;
        if (!viewer) {
            console.warn(`viewer is undefined!`);
            return;
        }
    }
}