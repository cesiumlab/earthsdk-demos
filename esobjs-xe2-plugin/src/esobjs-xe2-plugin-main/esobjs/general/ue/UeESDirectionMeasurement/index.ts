import { ESDirectionMeasurement } from "../../objs";
import { UeViewer } from "xbsj-xe2/dist-node/xe2-ue-objects";
import { UeESGeoVector } from "@/esobjs-xe2-plugin-main/esobjs/base";

export class UeESDirectionMeasurement extends UeESGeoVector<ESDirectionMeasurement>{
    static readonly type = this.register(ESDirectionMeasurement.type, this);
    constructor(sceneObject: ESDirectionMeasurement, ueViewer: UeViewer) {
        super(sceneObject, ueViewer);
        const viewer = ueViewer.viewer;
        if (!viewer) {
            console.warn(`viewer is undefined!`);
            return;
        }
    }
}