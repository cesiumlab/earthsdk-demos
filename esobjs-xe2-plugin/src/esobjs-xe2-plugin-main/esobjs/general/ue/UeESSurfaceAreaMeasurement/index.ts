import { UeViewer } from "xbsj-xe2/dist-node/xe2-ue-objects";
import { ESSurfaceAreaMeasurement } from '../../objs';
import { UeESGeoPolygon } from "../UeESGeoPolygon";
import { createNextAnimateFrameEvent } from "xbsj-xe2/dist-node/xe2-base-utils";

/**
 * 未实现
 */
export class UeESSurfaceAreaMeasurement extends UeESGeoPolygon<ESSurfaceAreaMeasurement>   {
    static override readonly type = this.register(ESSurfaceAreaMeasurement.type, this);
    constructor(sceneObject: ESSurfaceAreaMeasurement, ueViewer: UeViewer) {
        super(sceneObject, ueViewer);
        const viewer = ueViewer.viewer;
        if (!viewer) {
            console.warn(`viewer is undefined!`);
            return;
        }

        this.dispose(sceneObject.startEvent.disposableOn(() => {
            viewer.callUeFunc({
                f: 'Start',
                p: {
                    id: sceneObject.id
                }
            })
        }))
    }
}
