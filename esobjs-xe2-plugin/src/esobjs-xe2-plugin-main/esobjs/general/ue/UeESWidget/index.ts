import { createNextAnimateFrameEvent } from "xbsj-xe2/dist-node/xe2-base-utils";
import { ESWidget, ESWidgetInfoType } from '../../objs';
import { UeViewer } from "xbsj-xe2/dist-node/xe2-ue-objects";
import { UeESLabel } from "../../../base";

export class UeESWidget extends UeESLabel<ESWidget> {
    static readonly type = this.register(ESWidget.type, this);

    static override propValFuncs = {
        ...UeESLabel.propValFuncs,
        info: (val: ESWidgetInfoType) => JSON.stringify(val ?? {}),
    };

    constructor(sceneObject: ESWidget, ueViewer: UeViewer) {
        super(sceneObject, ueViewer);

        const viewer = ueViewer.viewer;
        if (!viewer) {
            console.warn(`viewer is undefined!`);
            return;
        }
    }
}
