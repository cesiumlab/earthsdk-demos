import { createNextAnimateFrameEvent } from 'xbsj-xe2/dist-node/xe2-base-utils';
import { ESSceneObject, UeESVisualObject } from '../../../base';
import { UeViewer } from '../../../base';
import { ESTerrainLayer } from '../../';

export class UeESTerrainLayer extends UeESVisualObject<ESTerrainLayer> {
    static readonly type = this.register(ESTerrainLayer.type, this);

    static override forceUeUpdateProps = [
        ...UeESVisualObject.forceUeUpdateProps,
        'url',
    ];

    static override propValFuncs = {
        ...UeESVisualObject.propValFuncs,
        url: (val: string) => {
            if (typeof val === 'string') return ESSceneObject.context.getStrFromEnv(val)
            // @ts-ignore
            val.url = ESSceneObject.context.getStrFromEnv(val.url
            );
            return val;
        },
        czmMaxzoom: null,
        czmMinzoom: null,
    };

    constructor(sceneObject: ESTerrainLayer, ueViewer: UeViewer) {
        super(sceneObject, ueViewer);

        const viewer = ueViewer.viewer;
        if (!viewer) {
            console.warn(`viewer is undefined!`);
            return;
        }
        // const urlReact = this.disposeVar(ESSceneObject.context.createEvnStrReact([sceneObject, 'url'], ESTerrainLayer.defaults.url));
        // const update = () => {
        //     viewer.callUeFunc({
        //         f: 'update',
        //         p: {
        //             id: sceneObject.id,
        //             url: urlReact.value ?? ESTerrainLayer.defaults.url,
        //             zIndex: sceneObject.zIndex ?? ESTerrainLayer.defaults.zIndex,
        //         }
        //     })
        // };

        // const updateEvent = this.disposeVar(createNextAnimateFrameEvent(
        //     urlReact.changed,
        //     sceneObject.zIndexChanged,
        // ));
        // this.dispose(updateEvent.disposableOn(update));
        // this.dispose(sceneObject.ueCreatedEvent.disposableOn(update));
    }
}
