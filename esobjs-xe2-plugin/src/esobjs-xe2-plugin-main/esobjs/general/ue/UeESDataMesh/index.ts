import { createNextAnimateFrameEvent, reactJson } from "xbsj-xe2/dist-node/xe2-base-utils";
import { ESDataMesh } from '../../objs';
import { UeViewer } from "xbsj-xe2/dist-node/xe2-ue-objects";
import { UeESObjectWithLocation, ESSceneObject, ESJResource } from "../../../base";
export class UeESDataMesh extends UeESObjectWithLocation<ESDataMesh> {
    static readonly type = this.register(ESDataMesh.type, this);
    constructor(sceneObject: ESDataMesh, ueViewer: UeViewer) {
        super(sceneObject, ueViewer);

        const viewer = ueViewer.viewer;
        if (!viewer) {
            console.warn(`viewer is undefined!`);
            return;
        }
        const urlReact = this.ad(reactJson<ESJResource | string>(""))
        {
            const update = () => {
                if (typeof sceneObject.url === 'string') {
                    urlReact.value = ESSceneObject.context.getStrFromEnv(sceneObject.url ?? ESDataMesh.defaults.url);
                } else {
                    const temp = sceneObject.url;
                    temp.url = ESSceneObject.context.getStrFromEnv(temp.url ?? ESDataMesh.defaults.url);
                    urlReact.value = temp;
                }
            }
            update();
            this.ad(sceneObject.urlChanged.don(update))
        }
        const update = () => {
            const colorStops = sceneObject.colorStops ?? ESDataMesh.defaults.colorStops;
            viewer.callUeFunc({
                f: 'update',
                p: {
                    id: sceneObject.id,
                    url: urlReact.value ?? ESDataMesh.defaults.url,
                    currentTime: sceneObject.currentTime ?? ESDataMesh.defaults.currentTime,
                    minPropValue: sceneObject.minPropValue ?? ESDataMesh.defaults.minPropValue,
                    maxPropValue: sceneObject.maxPropValue ?? ESDataMesh.defaults.maxPropValue,
                    colorStops: JSON.stringify(colorStops),
                }
            })
        };
        const updateEvent = this.disposeVar(createNextAnimateFrameEvent(
            urlReact.changed,
            sceneObject.currentTimeChanged,
            sceneObject.minPropValueChanged,
            sceneObject.maxPropValueChanged,
            sceneObject.colorStopsChanged,
        ));
        this.dispose(updateEvent.disposableOn(update));
        this.dispose(sceneObject.ueCreatedEvent.disposableOn(update));
    }
}
