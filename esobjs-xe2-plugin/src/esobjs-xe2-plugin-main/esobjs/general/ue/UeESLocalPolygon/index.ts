import { createNextAnimateFrameEvent } from "xbsj-xe2/dist-node/xe2-base-utils";
import { UeViewer } from "xbsj-xe2/dist-node/xe2-ue-objects";
import { UeESLocalVector2D } from "../../../base";
import { ESLocalPolygon } from '../../objs';

export class UeESLocalPolygon<T extends ESLocalPolygon = ESLocalPolygon> extends UeESLocalVector2D<T> {
    static readonly type = this.register<ESLocalPolygon>(ESLocalPolygon.type, this);
    constructor(sceneObject: T, ueViewer: UeViewer) {
        super(sceneObject, ueViewer);

        const viewer = ueViewer.viewer;
        if (!viewer) {
            console.warn(`viewer is undefined!`);
            return;
        }
        // const update = () => {
        //     viewer.callUeFunc({
        //         f: 'update',
        //         p: {
        //             id: sceneObject.id,
        //             points: sceneObject.points ?? ESLocalPolygon.defaults.points,
        //             filled: sceneObject.filled ?? ESLocalPolygon.defaults.filled,
        //         }
        //     })
        // };
        // const updateEvent = this.disposeVar(createNextAnimateFrameEvent(
        //     sceneObject.pointsChanged,
        //     sceneObject.filledChanged,
        // ));
        // this.dispose(updateEvent.disposableOn(update));
        // this.dispose(sceneObject.ueCreatedEvent.disposableOn(update));

    }
}
