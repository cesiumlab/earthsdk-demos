import { createNextAnimateFrameEvent } from "xbsj-xe2/dist-node/xe2-base-utils";
import { UeViewer } from "xbsj-xe2/dist-node/xe2-ue-objects";
import { UeESLocalVector2D } from "../../../base";
import { ESLocalCircle } from '../../objs';

export class UeESLocalCircle extends UeESLocalVector2D<ESLocalCircle> {
    static readonly type = this.register(ESLocalCircle.type, this);
    constructor(sceneObject: ESLocalCircle, ueViewer: UeViewer) {
        super(sceneObject, ueViewer);

        const viewer = ueViewer.viewer;
        if (!viewer) {
            console.warn(`viewer is undefined!`);
            return;
        }
        // const update = () => {
        //     viewer.callUeFunc({
        //         f: 'update',
        //         p: {
        //             id: sceneObject.id,
        //             radius: sceneObject.radius ?? ESLocalCircle.defaults.radius,
        //             filled: sceneObject.filled ?? ESLocalCircle.defaults.filled,
        //         }
        //     })
        // };
        // const updateEvent = this.disposeVar(createNextAnimateFrameEvent(
        //     sceneObject.radiusChanged,
        //     sceneObject.filledChanged,
        // ));
        // this.dispose(updateEvent.disposableOn(update));
        // this.dispose(sceneObject.ueCreatedEvent.disposableOn(update));

    }
}
