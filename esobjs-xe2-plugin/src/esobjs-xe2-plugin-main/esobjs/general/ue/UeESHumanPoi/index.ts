import { UeObject, UeViewer } from "xbsj-xe2/dist-node/xe2-ue-objects";
import { ESHumanPoi } from "../../objs";
export class UeESHumanPoi extends UeObject<ESHumanPoi> {
    static readonly type = this.register(ESHumanPoi.type, this);
    constructor(sceneObject: ESHumanPoi, ueViewer: UeViewer) {
        super(sceneObject, ueViewer);
        const viewer = ueViewer.viewer;
        if (!viewer) {
            console.warn(`viewer is undefined!`);
            return;
        }
        {
            sceneObject.poi.actorTag = sceneObject.human.id;
            sceneObject.poi.positionOffset = [0, 0, sceneObject.poiOffsetHeight - 0.8];
            this.d(sceneObject.poiOffsetHeightChanged.don(() => {
                sceneObject.poi.positionOffset = [0, 0, sceneObject.poiOffsetHeight - 0.8];
            }));

        }
        this.d(() => { sceneObject.poi.actorTag = ''; });
    }
}
