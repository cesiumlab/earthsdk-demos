import { ESClippingPlane } from '../../objs';
import { UeViewer } from 'xbsj-xe2/dist-node/xe2-ue-objects';
import { UeESObjectWithLocation } from '../../../base';
export class UeESClippingPlane extends UeESObjectWithLocation<ESClippingPlane>{
    static readonly type = this.register(ESClippingPlane.type, this);
    constructor(sceneObject: ESClippingPlane, ueViewer: UeViewer) {
        super(sceneObject, ueViewer);
        const viewer = ueViewer.viewer;
        if (!viewer) {
            console.warn(`viewer is underfined!`);
            return;
        }
    }
}