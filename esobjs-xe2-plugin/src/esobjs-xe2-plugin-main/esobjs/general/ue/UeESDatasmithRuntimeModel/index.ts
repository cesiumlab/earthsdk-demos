import { ESDatasmithRuntimeModel } from '../../objs';
import { UeViewer } from "xbsj-xe2/dist-node/xe2-ue-objects";
import { UeESObjectWithLocation } from "../../../base";
export class UeESDatasmithRuntimeModel extends UeESObjectWithLocation<ESDatasmithRuntimeModel> {
    static readonly type = this.register(ESDatasmithRuntimeModel.type, this);
    constructor(sceneObject: ESDatasmithRuntimeModel, ueViewer: UeViewer) {
        super(sceneObject, ueViewer);
        const viewer = ueViewer.viewer;
        if (!viewer) {
            console.warn(`viewer is undefined!`);
            return;
        }
    }
}
