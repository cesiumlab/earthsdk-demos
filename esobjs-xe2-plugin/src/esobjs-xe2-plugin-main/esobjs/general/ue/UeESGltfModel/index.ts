import { ESGltfModel } from '../../objs';
import { UeViewer } from "xbsj-xe2/dist-node/xe2-ue-objects";
import { ESSceneObject, UeESObjectWithLocation, rpToap } from "../../../base";
import { setNodePositionCallFunc, setNodeRotationCallFunc, setNodeScaleCallFunc } from "xbsj-xe2/dist-node/xe2-ue-objects";

export class UeESGltfModel<T extends ESGltfModel = ESGltfModel> extends UeESObjectWithLocation<T> {
    static override forceUeUpdateProps = [
        ...UeESObjectWithLocation.forceUeUpdateProps,
        'url',
    ];

    static override  propValFuncs = {
        ...UeESObjectWithLocation.propValFuncs,
        url: (val: string) => {
            if (typeof val === 'string') return ESSceneObject.context.getStrFromEnv(rpToap(val))
            // @ts-ignore
            val.url = ESSceneObject.context.getStrFromEnv(rpToap(val.url)
            );
            return val;
        },
        czmMaximumScale: null,
        czmMinimumPixelSize: null,
        czmNativeScale: null,
        czmColor: null,
    };

    static readonly type = this.register<ESGltfModel>(ESGltfModel.type, this);

    constructor(sceneObject: T, ueViewer: UeViewer) {
        super(sceneObject, ueViewer);

        const viewer = ueViewer.viewer;
        if (!viewer) {
            console.warn(`viewer is undefined!`);
            return;
        }
        // const urlReact = this.disposeVar(ESSceneObject.context.createEvnStrReact([sceneObject, 'url'], ESGltfModel.defaults.url));
        // const update = () => {
        //     viewer.callUeFunc({
        //         f: 'update',
        //         p: {
        //             id: sceneObject.id,
        //             url: urlReact.value,
        //         }
        //     })
        // };
        // this.dispose(urlReact.changed.disposableOn(update));
        // this.dispose(sceneObject.ueCreatedEvent.disposableOn(update));

        this.dispose(sceneObject.setNodePositionEvent.disposableOn((NodeName, NodePosition) => {
            setNodePositionCallFunc(viewer, sceneObject.id, NodeName, NodePosition)
        }));

        this.dispose(sceneObject.setNodeRotationEvent.disposableOn((NodeName, NodeRotation) => {
            setNodeRotationCallFunc(viewer, sceneObject.id, NodeName, NodeRotation)
        }));

        this.dispose(sceneObject.setNodeScaleEvent.disposableOn((NodeName, NodeScale) => {
            setNodeScaleCallFunc(viewer, sceneObject.id, NodeName, NodeScale)
        }));
    }
}
