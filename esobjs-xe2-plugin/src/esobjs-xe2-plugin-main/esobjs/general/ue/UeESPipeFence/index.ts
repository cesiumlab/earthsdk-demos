import { createNextAnimateFrameEvent } from 'xbsj-xe2/dist-node/xe2-base-utils';
import { ESPipeFence } from '../../objs';
import { UeViewer } from '../../../base';
import { UeESGeoVector } from '../../../base';

export class UeESPipeFence extends UeESGeoVector<ESPipeFence> {
    static readonly type = this.register(ESPipeFence.type, this);

    constructor(sceneObject: ESPipeFence, ueViewer: UeViewer) {
        super(sceneObject, ueViewer);

        const viewer = ueViewer.viewer;
        if (!viewer) {
            console.warn(`viewer is undefined!`);
            return;
        }
        // const update = () => {

        //     let fillStyle = ESPipeFence.defaults.fillStyle
        //     try {
        //         fillStyle = { ...(sceneObject.fillStyle ?? ESPipeFence.defaults.fillStyle) };
        //     } catch (e) {
        //         console.error('ESPipeFence fillStyle 属性类型错误!', e)
        //         fillStyle = { ...ESPipeFence.defaults.fillStyle }
        //     }
        //     fillStyle.materialParams = JSON.stringify(fillStyle.materialParams);

        //     let strokeStyle = ESPipeFence.defaults.strokeStyle
        //     try {
        //         strokeStyle = { ...(sceneObject.strokeStyle ?? ESPipeFence.defaults.strokeStyle) };
        //     } catch (e) {
        //         console.error('ESPipeFence strokeStyle 属性类型错误!', e)
        //         strokeStyle = { ...ESPipeFence.defaults.strokeStyle }
        //     }
        //     strokeStyle.materialParams = JSON.stringify(strokeStyle.materialParams);

        //     viewer.callUeFunc({
        //         f: 'update',
        //         p: {
        //             id: sceneObject.id,
        //             height: sceneObject.height ?? ESPipeFence.defaults.height,
        //             width: sceneObject.width ?? ESPipeFence.defaults.width,
        //             stroked: sceneObject.stroked ?? ESPipeFence.defaults.stroked,
        //             strokeStyle: strokeStyle,
        //             filled: sceneObject.filled ?? ESPipeFence.defaults.filled,
        //             fillStyle: fillStyle
        //         }
        //     })
        // };
        // const updateEvent = this.disposeVar(createNextAnimateFrameEvent(
        //     sceneObject.widthChanged,
        //     sceneObject.heightChanged,
        //     sceneObject.strokeStyleChanged,
        //     sceneObject.strokedChanged,
        //     sceneObject.fillStyleChanged,
        //     sceneObject.filledChanged,
        // ));
        // this.dispose(updateEvent.disposableOn(update));
        // this.dispose(sceneObject.ueCreatedEvent.disposableOn(update));
    }
}
