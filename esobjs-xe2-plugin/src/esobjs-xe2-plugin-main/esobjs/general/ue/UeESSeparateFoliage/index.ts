import { createNextAnimateFrameEvent } from "xbsj-xe2/dist-node/xe2-base-utils";
import { ESSeparateFoliage, ESTreeParam } from '../../objs';
import { UeViewer } from "xbsj-xe2/dist-node/xe2-ue-objects";
import { UeESVisualObject } from "../../../base";
import { UeFuncsType } from "xbsj-xe2/dist-node/xe2-ue-objects";
import { addTreesCallFunc, removeAllTreesCallFunc, updateTreeParamsCallFunc, cutDownTreesCallFunc, growthSimulationCallFunc } from "../../objs/ESSeparateFoliage/CallFunc";

export class UeESSeparateFoliage extends UeESVisualObject<ESSeparateFoliage> {
    static readonly type = this.register(ESSeparateFoliage.type, this);
    constructor(sceneObject: ESSeparateFoliage, ueViewer: UeViewer) {
        super(sceneObject, ueViewer);

        const viewer = ueViewer.viewer;
        if (!viewer) {
            console.warn(`viewer is undefined!`);
            return;
        }
        // const update = () => {
        //     viewer.callUeFunc({
        //         f: 'update',
        //         p: {
        //             id: sceneObject.id,
        //             treeTypes: sceneObject.treeTypes ?? ESSeparateFoliage.defaults.treeTypes,
        //             stumpId: sceneObject.stumpId ?? ESSeparateFoliage.defaults.stumpId,
        //             intervalTime: sceneObject.intervalTime ?? ESSeparateFoliage.defaults.intervalTime,
        //             switchIntervalTime: sceneObject.switchIntervalTime ?? ESSeparateFoliage.defaults.switchIntervalTime,
        //         }
        //     })
        // };
        // const updateEvent = this.disposeVar(createNextAnimateFrameEvent(
        //     sceneObject.treeTypesChanged,
        //     sceneObject.stumpIdChanged,
        //     sceneObject.intervalTimeChanged,
        //     sceneObject.switchIntervalTimeChanged,
        // ));
        // this.dispose(updateEvent.disposableOn(update));
        // this.dispose(sceneObject.ueCreatedEvent.disposableOn(update));

        this.dispose(sceneObject.addTreesEvent.disposableOn((TreeParams: ESTreeParam[]) => {
            addTreesCallFunc(ueViewer, sceneObject.id, TreeParams)
        }));

        this.dispose(sceneObject.removeAllTreesEvent.disposableOn(() => {
            removeAllTreesCallFunc(ueViewer, sceneObject.id,)
        }));

        this.dispose(sceneObject.updateTreeParamsEvent.disposableOn((TreeParams: ESTreeParam[]) => {
            updateTreeParamsCallFunc(ueViewer, sceneObject.id, TreeParams)
        }));

        this.dispose(sceneObject.cutDownTreesEvent.disposableOn((TreeIds, TimeLength) => {
            cutDownTreesCallFunc(ueViewer, sceneObject.id, TreeIds, TimeLength)
        }));

        this.dispose(sceneObject.growthSimulationEvent.disposableOn((ToParams, TimeLength, SwitchTime) => {
            growthSimulationCallFunc(ueViewer, sceneObject.id, ToParams, TimeLength, SwitchTime)
        }));
    }
}
