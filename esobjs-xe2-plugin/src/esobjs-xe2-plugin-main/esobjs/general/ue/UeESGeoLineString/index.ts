import { ESGeoLineString } from '../../objs';
import { UeViewer } from "xbsj-xe2/dist-node/xe2-ue-objects";
import { UeESGeoVector } from '../../../base';
import { createNextAnimateFrameEvent } from "xbsj-xe2/dist-node/xe2-base-utils";

export class UeESGeoLineString<T extends ESGeoLineString = ESGeoLineString> extends UeESGeoVector<T> {
    // export class UeESGeoLineString<T extends ESGeoLineString = ESGeoLineString> extends UeESGeoVector<T>{
    static readonly type = this.register<ESGeoLineString>(ESGeoLineString.type, this);
    constructor(sceneObject: T, ueViewer: UeViewer) {
        super(sceneObject, ueViewer);
        const viewer = ueViewer.viewer;
        if (!viewer) {
            console.warn(`viewer is undefined!`);
            return;
        }
        // const update = () => {
        //     let strokeStyle = ESGeoLineString.defaults.strokeStyle
        //     try {
        //         strokeStyle = { ...(sceneObject.strokeStyle ?? ESGeoLineString.defaults.strokeStyle) };
        //     } catch (e) {
        //         console.error('ESGeoLineString strokeStyle 属性类型错误!', e)
        //         strokeStyle = { ...ESGeoLineString.defaults.strokeStyle }
        //     }
        //     strokeStyle.materialParams = JSON.stringify(strokeStyle.materialParams);
        //     viewer.callUeFunc({
        //         f: 'update',
        //         p: {
        //             id: sceneObject.id,
        //             stroked: sceneObject.stroked ?? ESGeoLineString.defaults.stroked,
        //             strokeStyle,
        //         }
        //     })
        // };
        // const updateEvent = this.disposeVar(createNextAnimateFrameEvent(
        //     sceneObject.strokedChanged,
        //     sceneObject.strokeStyleChanged,
        // ));
        // this.dispose(updateEvent.disposableOn(update));
        // this.dispose(sceneObject.ueCreatedEvent.disposableOn(update));
    }
}
