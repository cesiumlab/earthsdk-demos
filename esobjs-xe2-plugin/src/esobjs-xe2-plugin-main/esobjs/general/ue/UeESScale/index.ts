import { CustomDiv, Viewer } from "xbsj-xe2/dist-node/xe2-base-objects";
import { Destroyable } from "xbsj-xe2/dist-node/xe2-base-utils";
import { UeObject, UeViewer } from '../../../base';
import { ESScale } from '../../objs';

export class UeESScale extends UeObject<ESScale> {
    static readonly type = this.register(ESScale.type, this);
    constructor(sceneObject: ESScale, ueViewer: UeViewer) {
        super(sceneObject, ueViewer);

        const viewer = ueViewer.viewer;
        if (!viewer) {
            console.warn(`viewer is undefined!`);
            return;
        }
        const customDiv = this.disposeVar(new CustomDiv());
        ueViewer.add(customDiv);

        this.dispose(() => ueViewer.delete(customDiv))
        // this.dispose(bind([customDiv, 'show'], [sceneObject, 'show']));
        customDiv.instanceClass = class MyDiv extends Destroyable {
            // subContainer是外部视口的div容器，可以在这里创建自己需要的DOM元素
            // customDiv指向当前的CustomDiv场景对象
            // viewer指定当前的视口
            constructor(private _subContainer: HTMLDivElement, customDiv: CustomDiv, viewer?: Viewer | undefined) {
                super();
                const div = document.createElement('div');
                this._subContainer.appendChild(div);
                this.dispose(() => this._subContainer.removeChild(div));
                {
                    const update = () => {
                        div.style.display = (sceneObject.show ?? ESScale.defaults.show) ? 'block' : 'none';
                    }
                    update()
                    sceneObject.dispose(sceneObject.showChanged.disposableOn(update))
                }
                div.style.position = 'fixed';
                div.style.width = '125px';
                div.style.height = '30px'
                div.style.border = '1px solid rgba(49,50,56,.8)'
                div.style.padding = '0 5px';
                div.style.backgroundColor = 'rgba(37,38,42,.8)';
                div.style.borderRadius = '15px';
                div.style.pointerEvents = 'auto';
                div.style.transition = ' right 0.4s linear';
                {
                    const update = () => {
                        div.style.bottom = `${sceneObject.cssPosition ? sceneObject.cssPosition[0] : ESScale.defaults.cssPosition[0]}px`;
                        div.style.right = `${sceneObject.cssPosition ? sceneObject.cssPosition[1] : ESScale.defaults.cssPosition[1]}px`;
                    }
                    update()
                    sceneObject.dispose(sceneObject.cssPositionChanged.disposableOn(update))
                }


                const box = document.createElement('div');
                div.appendChild(box);
                this.dispose(() => div.removeChild(box));
                box.style.width = '125px';
                box.style.display = 'inline-block';
                box.style.textAlign = 'center';
                box.style.fontSize = '14px';
                box.style.fontWeight = 'lighter';
                box.style.lineHeight = '30px';
                box.style.color = '#fff';


                const rbox = document.createElement('div');
                div.appendChild(rbox);
                this.dispose(() => div.removeChild(rbox));
                rbox.style.borderRight = '1px solid #fff';
                rbox.style.borderLeft = '1px solid #fff';
                rbox.style.borderBottom = '1px solid #fff';
                rbox.style.position = 'absolute';
                rbox.style.height = '10px';
                rbox.style.top = '15px';
                box.innerHTML = '--km'
                rbox.style.width = '100px';
                const l = (135 - 100) / 2;
                rbox.style.left = `${l}px`;

                {
                    const aaa = () => {
                        ueViewer.getLengthInPixel().then(res => {
                            // if (!res || res.length > 20000) {
                            //     div.style.display = 'none'
                            //     return
                            // }
                            // div.style.display = 'block'
                            if (!res) return
                            let a;
                            if (res.length * 100 > 1000) {

                                a = `${(res.length * 100 / 1000).toFixed(0)}km`
                            } else {
                                a = `${(res.length * 100).toFixed(0)}m`
                            }
                            box.innerHTML = a
                            rbox.style.width = '100px';
                            const l = (135 - 100) / 2;
                            rbox.style.left = `${l}px`;

                            // if(res){
                            //     let a;
                            //     if (res.length * 100 > 1000) {
                            //         a = `${(res.length * 100 / 1000).toFixed(0)}km`
                            //     } else {
                            //         a = `${(res.length * 100).toFixed(0)}m`
                            //     }
                            //     box.innerHTML = a
                            //     rbox.style.width = '100px';
                            //     const l = (135 - 100) / 2;
                            //     rbox.style.left = `${l}px`;
                            // }
                            // if(!res){
                            //     div.style.display = 'none'
                            //     return
                            // }else {
                            //     if (res.length > 20000) {
                            //         div.style.display = 'none'
                            //     } else {
                            //         // div.style.display = 'block';
                            //         let a;
                            //         if (res.length * 100 > 1000) {
                            //             a = `${(res.length * 100 / 1000).toFixed(0)}km`
                            //         } else {
                            //             a = `${(res.length * 100).toFixed(0)}m`
                            //         }
                            //         box.innerHTML = a
                            //         rbox.style.width = '100px';
                            //         const l = (135 - 100) / 2;
                            //         rbox.style.left = `${l}px`;
                            //     }
                            // }
                        }).catch(error => {
                            console.log(error);
                        })
                    }
                    let timer: any
                    const update = async () => {
                        aaa()
                        if (sceneObject.show) {
                            div.style.display = 'block';
                            timer = setInterval(() => {
                                aaa()
                            }, 1000)
                        } else {
                            clearInterval(timer);
                            div.style.display = 'none';
                        }
                    }
                    this.dispose(sceneObject.showChanged.disposableOn(update));
                    update()
                    this.dispose(() => clearInterval(timer));
                }
            }
        }


    }
}
