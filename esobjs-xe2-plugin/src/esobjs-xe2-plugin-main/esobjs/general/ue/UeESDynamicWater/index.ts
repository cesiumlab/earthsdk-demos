import { UeViewer } from 'xbsj-xe2/dist-node/xe2-ue-objects';
import { ESDynamicWater } from './../../objs';
import { UeESLocalPolygon } from '../UeESLocalPolygon';

export class UeESDynamicWater extends UeESLocalPolygon<ESDynamicWater> {
    static override readonly type = this.register(ESDynamicWater.type, this);

    constructor(sceneObject: ESDynamicWater, ueViewer: UeViewer) {
        super(sceneObject, ueViewer);
        const viewer = ueViewer.viewer;
        if (!viewer) {
            console.warn(`viewer is undefined!`);
            return;
        }
    }
}