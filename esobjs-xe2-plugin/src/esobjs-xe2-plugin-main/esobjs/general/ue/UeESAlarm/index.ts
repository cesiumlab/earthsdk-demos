import { UeViewer } from "xbsj-xe2/dist-node/xe2-ue-objects";
import { UeESObjectWithLocation } from "../../../base";
import { ESAlarm } from '../../objs';
export class UeESAlarm extends UeESObjectWithLocation<ESAlarm> {
    static readonly type = this.register(ESAlarm.type, this);
    constructor(sceneObject: ESAlarm, ueViewer: UeViewer) {
        super(sceneObject, ueViewer);

        const viewer = ueViewer.viewer;
        if (!viewer) {
            console.warn(`viewer is undefined!`);
            return;
        }
    }
}
