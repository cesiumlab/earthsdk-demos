import { ESUEWidget, ESUEWidgetInfoType } from '../../objs';
import { callFunctionCallFunc, UeViewer } from "xbsj-xe2/dist-node/xe2-ue-objects";
import { UeESLabel } from "../../../base";

export class UeESUEWidget extends UeESLabel<ESUEWidget> {
    static readonly type = this.register(ESUEWidget.type, this);

    static override propValFuncs = {
        ...UeESLabel.propValFuncs,
        info: (val: ESUEWidgetInfoType) => JSON.stringify(val ?? {}),
    };

    constructor(sceneObject: ESUEWidget, ueViewer: UeViewer) {
        super(sceneObject, ueViewer);

        const viewer = ueViewer.viewer;
        if (!viewer) {
            console.warn(`viewer is undefined!`);
            return;
        }
        this.dispose(sceneObject.callFunctionEvent.disposableOn((fn, p) => {
            callFunctionCallFunc(viewer, sceneObject.id, fn, p);
        }))
        // const update = () => {
        //     let info = ''
        //     try {
        //         info = JSON.stringify(sceneObject.info ?? ESUEWidget.defaults.info)
        //     } catch (error) {
        //         console.error('ESUEWidget info 属性设置错误!', error)
        //         info = JSON.stringify(ESUEWidget.defaults.info)
        //     }
        //     viewer.callUeFunc({
        //         f: 'update',
        //         p: {
        //             id: sceneObject.id,
        //             widgetClass: sceneObject.widgetClass ?? ESUEWidget.defaults.widgetClass,
        //             socketName: sceneObject.socketName ?? ESUEWidget.defaults.socketName,
        //             positionOffset: sceneObject.positionOffset ?? ESUEWidget.defaults.positionOffset,
        //             info,
        //             actorTag: sceneObject.actorTag ?? ESUEWidget.defaults.actorTag,
        //             rotationOffset: sceneObject.rotationOffset ?? ESUEWidget.defaults.rotationOffset,
        //         }
        //     })
        // };
        // const updateEvent = this.disposeVar(createNextAnimateFrameEvent(
        //     sceneObject.infoChanged,
        //     sceneObject.actorTagChanged,
        //     sceneObject.positionOffsetChanged,
        //     sceneObject.rotationOffsetChanged,
        //     sceneObject.widgetClassChanged,
        //     sceneObject.socketNameChanged,
        // ));
        // this.dispose(updateEvent.disposableOn(update));
        // this.dispose(sceneObject.ueCreatedEvent.disposableOn(update));

        // this.dispose(sceneObject.flushEvent.disposableOn(() => updateEvent.flush()));
    }
}
