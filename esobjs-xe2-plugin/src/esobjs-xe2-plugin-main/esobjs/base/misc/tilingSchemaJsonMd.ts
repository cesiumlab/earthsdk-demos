
export const tilingSchemaJsonMd =
    `
# 示例

\`\`\`javascript
    {
        "type": "WebMercatorTilingScheme"
    }
\`\`\`

# 声明文件

### CzmTilingSchemaJsonType
\`\`\`javascript
    export type CzmTilingSchemaJsonType = {
        type: 'WebMercatorTilingScheme';
        ellipsoid?: [x: number, y: number, z: number]; //	Ellipsoid	Ellipsoid.WGS84	optional The ellipsoid whose surface is being tiled. Defaults to the WGS84 ellipsoid.
        numberOfLevelZeroTilesX?: number; //	Number	1	optional The number of tiles in the X direction at level zero of the tile tree.
        numberOfLevelZeroTilesY?: number; //	Number	1	optional The number of tiles in the Y direction at level zero of the tile tree.
        rectangleSouthwestInMeters?: [number, number]; //	Cartesian2		optional The southwest corner of the rectangle covered by the tiling scheme, in meters. If this parameter or rectangleNortheastInMeters is not specified, the entire globe is covered in the longitude direction and an equal distance is covered in the latitude direction, resulting in a square projection.
        rectangleNortheastInMeters?: [number, number]; //	Cartesian2		optional The northeast corner of the rectangle covered by the tiling scheme, in meters. If this parameter or rectangleSouthwestInMeters is not specified, the entire globe is covered in the longitude direction and an equal distance is covered in the latitude direction, resulting in a square projection.
    } | {
        type: 'GeographicTilingScheme';
        ellipsoid?: [x: number, y: number, z: number]; //	Ellipsoid	Ellipsoid.WGS84	optionalThe ellipsoid whose surface is being tiled. Defaults to the WGS84 ellipsoid.
        rectangle?: [west: number, south: number, east: number, north: number]; //	Rectangle	Rectangle.MAX_VALUE	optional The rectangle, in radians, covered by the tiling scheme.
        numberOfLevelZeroTilesX?: number; //	Number	2	optional The number of tiles in the X direction at level zero of the tile tree.
        numberOfLevelZeroTilesY?: number; //	Number	1	optional The number of tiles in the Y direction at level zero of the tile tree.
    } | {
        type: 'ToGCJ02WebMercatorTilingScheme';
        ellipsoid?: [x: number, y: number, z: number]; //	Ellipsoid	Ellipsoid.WGS84	optional The ellipsoid whose surface is being tiled. Defaults to the WGS84 ellipsoid.
        numberOfLevelZeroTilesX?: number; //	Number	1	optional The number of tiles in the X direction at level zero of the tile tree.
        numberOfLevelZeroTilesY?: number; //	Number	1	optional The number of tiles in the Y direction at level zero of the tile tree.
        rectangleSouthwestInMeters?: [number, number]; //	Cartesian2		optional The southwest corner of the rectangle covered by the tiling scheme, in meters. If this parameter or rectangleNortheastInMeters is not specified, the entire globe is covered in the longitude direction and an equal distance is covered in the latitude direction, resulting in a square projection.
        rectangleNortheastInMeters?: [number, number]; //	Cartesian2		optional The northeast corner of the rectangle covered by the tiling scheme, in meters. If this parameter or rectangleSouthwestInMeters is not specified, the entire globe is covered in the longitude direction and an equal distance is covered in the latitude direction, resulting in a square projection.
    } | {
        type: 'ToWGS84WebMercatorTilingScheme';
        ellipsoid?: [x: number, y: number, z: number]; //	Ellipsoid	Ellipsoid.WGS84	optional The ellipsoid whose surface is being tiled. Defaults to the WGS84 ellipsoid.
        numberOfLevelZeroTilesX?: number; //	Number	1	optional The number of tiles in the X direction at level zero of the tile tree.
        numberOfLevelZeroTilesY?: number; //	Number	1	optional The number of tiles in the Y direction at level zero of the tile tree.
        rectangleSouthwestInMeters?: [number, number]; //	Cartesian2		optional The southwest corner of the rectangle covered by the tiling scheme, in meters. If this parameter or rectangleNortheastInMeters is not specified, the entire globe is covered in the longitude direction and an equal distance is covered in the latitude direction, resulting in a square projection.
        rectangleNortheastInMeters?: [number, number]; //	Cartesian2		optional The northeast corner of the rectangle covered by the tiling scheme, in meters. If this parameter or rectangleSouthwestInMeters is not specified, the entire globe is covered in the longitude direction and an equal distance is covered in the latitude direction, resulting in a square projection.
    }

\`\`\`
`
