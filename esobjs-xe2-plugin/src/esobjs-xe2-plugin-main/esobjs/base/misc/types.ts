export type ESFlyToParam = { distance: number, heading: number, pitch: number, flyDuration: number, hDelta: number, pDelta: number };
export type ESFlyInParam = { position: [number, number, number], rotation: [number, number, number], flyDuration: number };
export type ESColor = [number, number, number, number];
export type ESImageBaseInfo = { width: number, height: number, url: string, base64: string };
export type ESEntityClusterStyle = {
    cluster?: {
        value?: number,
        minValue?: number,
        maxValue?: number,
        mode?: string,
        style?: { [xx: string]: any }
    }[],
    nonCluster?: {
        mode?: string,
        style?: { [xx: string]: any }
    }
}
export type WaterAttribute = {
    waterImage?: { [xx: string]: any },
    waterColor?: ESColor,
    frequency?: number,
    waveVelocity?: number,
    amplitude?: number,
    specularIntensity?: number,
    flowDirection?: number,
    flowSpeed?: number,
}
export type ESJResource = {
    url: string;
    queryParameters?: any;
    templateValues?: any;
    headers?: any;
    proxy?: any;
    retryCallback?: any;
    retryAttempts?: number;
    request?: any;
    parseUrl?: boolean;
};