import { BooleanProperty, EnumProperty, GroupProperty, Number2Property, Number3Property, Number3sProperty, NumberProperty, PositionProperty, ReactVarProperty, RotationProperty, StringProperty } from "xbsj-xe2/dist-node/xe2-base-objects";
import { Event, PartialWithUndefinedReactivePropsToNativeProps, ReactivePropsToNativePropsAndChanged, extendClassProps, reactArray } from "xbsj-xe2/dist-node/xe2-base-utils";
import { ESObjectWithLocation } from "./ESObjectWithLocation";

export type WidgetEventInfo = {
    type: "leftClick" | "rightClick" | "mouseEnter" | "mouseLeave" | "childMouseLeave" | "childMouseEnter";
    add?: {
        children?: string[];
        mousePos?: [number, number];
    }
}

const rotationTypeEnum = [['固定朝向', 0], ['面向屏幕旋转', 1], ['绕自身Z轴旋转', 2]] as [string, number][];
const renderModeEnum = [['单面不透明', 0], ['双面不透明', 1], ['单面遮罩', 2], ['双面遮罩', 3], ['单面透明', 4], ['双面透明', 5], ['单面未遮挡透明', 6], ['双面未遮挡透明', 7]] as [string, number][];
/**
 * https://c0yh9tnn0na.feishu.cn/docx/KxWzdfJtco5bSuxJcB6cJkFgnge
 */
export abstract class ESLabel extends ESObjectWithLocation {

    private _widgetEvent = this.dv(new Event<[WidgetEventInfo]>());
    get widgetEvent() { return this._widgetEvent };

    static override defaults = {
        ...ESObjectWithLocation.defaults,
        // screenRender: true,
        // size: [100, 100] as [number, number],
        // anchor: [0.5, 0.5] as [number, number],
        // sizeByContent: true,
        // renderMode: 0,
        // rotationType: 1,
    };
    override getESProperties() {
        const properties = { ...super.getESProperties() };

        return {
            ...properties,
            basic: [
                ...properties.basic,
                new BooleanProperty('屏幕渲染', '是否开启屏幕渲染模式', false, false, [this, 'screenRender'], true),
                new BooleanProperty('尺寸自适应', '尺寸是否根据内容自动计算', false, false, [this, 'sizeByContent'], true),
                new Number2Property('尺寸大小', '尺寸自适应关闭才会生效', false, false, [this, 'size'], [100, 100]),
                new Number2Property('偏移比例', '偏移比例(anchor)', false, false, [this, 'anchor'], [0.5, 1]),
                new Number2Property('像素偏移', '像素偏移(offset)', false, false, [this, 'offset'], [0, 0]),
                // new NumberProperty('渲染模式', '八种渲染模式(0~7),当Widget中透明度只有(0,1)两种时可以选择2', false, false, [this, 'renderMode']),
                new EnumProperty('渲染模式', '八种渲染模式(0~7),当Widget中透明度只有(0,1)两种时可以选择2', false, false, [this, 'renderMode'], renderModeEnum, 0),
                // new NumberProperty('漫游旋转类型', '三种漫游旋转类型', false, false, [this, 'rotationType']),
                new EnumProperty('漫游旋转类型', '三种漫游旋转类型(0,1,2)', false, false, [this, 'rotationType'], rotationTypeEnum, 1),
                // new NumberProperty('排序', 'zOrder排序', false, false, [this, 'zOrder'], 0),
                new GroupProperty('ue', 'ue', []),
                new StringProperty("绑定对象", "actorTag", false, false, [this, 'actorTag']),
                new StringProperty("插槽名称", "socketName", false, false, [this, 'socketName']),
                new Number3Property('位置偏移', 'positionOffset(米)', false, false, [this, 'positionOffset']),
                new Number3Property('相对姿态', 'rotationOffset(米)', false, false, [this, 'rotationOffset']),
            ],
        }

    };
    override getProperties(language?: string) {
        return [
            ...super.getProperties(language),
            new GroupProperty('标签属性', '标签对象ESLabel的属性', [
                new BooleanProperty('屏幕渲染', '是否开启屏幕渲染模式', false, false, [this, 'screenRender'], true),
                new BooleanProperty('尺寸自适应', '尺寸是否根据内容自动计算', false, false, [this, 'sizeByContent'], true),
                new Number2Property('尺寸大小', '尺寸自适应关闭才会生效', false, false, [this, 'size'], [100, 100]),
                new Number2Property('偏移比例', '偏移比例(anchor)', false, false, [this, 'anchor'], [0.5, 1]),
                new Number2Property('像素偏移', '像素偏移(offset)', false, false, [this, 'offset'], [0, 0]),
                // new NumberProperty('渲染模式', '八种渲染模式(0~7),当Widget中透明度只有(0,1)两种时可以选择2', false, false, [this, 'renderMode']),
                new EnumProperty('渲染模式', '八种渲染模式(0~7),当Widget中透明度只有(0,1)两种时可以选择2', false, false, [this, 'renderMode'], renderModeEnum, 0),
                // new NumberProperty('漫游旋转类型', '三种漫游旋转类型', false, false, [this, 'rotationType']),
                new EnumProperty('漫游旋转类型', '三种漫游旋转类型(0,1,2)', false, false, [this, 'rotationType'], rotationTypeEnum, 1),
                new NumberProperty('排序', 'zOrder排序', false, false, [this, 'zOrder'], 0),
            ]),
            new GroupProperty('UE特有属性', 'UE特有属性', [
                new StringProperty("actorTag", "actorTag", false, false, [this, 'actorTag']),
                new StringProperty("socketName", "socketName", false, false, [this, 'socketName']),
                new PositionProperty('positionOffset', 'positionOffset(米)', false, false, [this, 'positionOffset']),
                new RotationProperty('rotationOffset', 'rotationOffset(米)', false, false, [this, 'rotationOffset']),
            ])
        ];
    }
}

export namespace ESLabel {
    export const createDefaultProps = () => ({
        ...ESObjectWithLocation.createDefaultProps(),
        screenRender: true,
        size: reactArray<[number, number]>([100, 100]),
        anchor: reactArray<[number, number]>([0.5, 1]),
        offset: reactArray<[number, number]>([0, 0]),
        sizeByContent: true,
        renderMode: 0,
        rotationType: 1,
        zOrder: 0,
        // UE特有属性
        actorTag: "",
        socketName: "",
        positionOffset: reactArray<[number, number, number]>([0, 0, 0]),
        rotationOffset: reactArray<[number, number, number]>([0, 0, 0]),
    });
}
extendClassProps(ESLabel.prototype, ESLabel.createDefaultProps);
export interface ESLabel extends ReactivePropsToNativePropsAndChanged<ReturnType<typeof ESLabel.createDefaultProps>> { }
type JsonType = PartialWithUndefinedReactivePropsToNativeProps<ReturnType<typeof ESLabel.createDefaultProps> & { type: string }>;
