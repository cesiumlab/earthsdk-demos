import { ReactVarProperty, Property, BooleanProperty, StringProperty, FunctionProperty, GroupProperty, NumberProperty, JsonProperty, PickedInfo, Viewer } from "xbsj-xe2/dist-node/xe2-base-objects";
import { Event, Listener, PartialWithUndefinedReactivePropsToNativeProps, ReactivePropsToNativePropsAndChanged, extendClassProps, reactPlainObjectWithUndefined, reactJsonWithUndefined } from "xbsj-xe2/dist-node/xe2-base-utils";
import { ESSceneObject } from "./ESSceneObject";
import { UeViewer } from "xbsj-xe2/dist-node/xe2-ue-objects";
import { ESFlyInParam, ESFlyToParam } from "../misc";

/**
 * https://www.wolai.com/earthsdk/u1sLHxcj6PErXf8ubcvC4j
 * https://c0yh9tnn0na.feishu.cn/docx/VWGAdUaWxonnOzxHs9FcczdEn7e
 */
export abstract class ESVisualObject extends ESSceneObject {
    /**
     * 弃用变量管理器
     * 请勿使用该属性
     */
    _deprecated: ({ [k: string]: any } | string)[] = [];
    /**
      * 弃用变量管理器
      * 请勿使用该方法
      */
    _deprecatedWarning() {
        const includes = this._deprecated;
        for (let i = 0; i < includes.length; i++) {
            const element = includes[i];
            if (typeof element === "string") {
                const flag = Reflect.has(this, element + 'Changed');
                if (flag) {
                    //@ts-ignore
                    this.d(this[element + 'Changed'].don(() => {
                        console.warn(`注意：${this.typeName} 的 ${element} 属性下版本将会被移除！`);
                    }));
                }
            } else {
                Object.keys(element).forEach((item: any) => {
                    const flag = Reflect.has(this, item + 'Changed');
                    if (flag) {
                        //@ts-ignore
                        this.d(this[item + 'Changed'].don((val) => {
                            const valueProp = element[item];
                            if (typeof valueProp === "string") {
                                console.warn(`注意：${this.typeName} 的 ${item} 属性下版本将会被移除！`);
                            } else {
                                Object.keys(valueProp).forEach((key => {
                                    if (val === key) {
                                        console.warn(`注意：${this.typeName} 的 ${item} 属性值 ${key} 下版本将会被移除,推荐使用属性值 ${valueProp[key]}`);
                                    }
                                }))
                            }
                        }))
                    }
                })
            }
        }
    }


    private _flyInEvent = this.disposeVar(new Event<[duration: number | undefined, id: number]>());
    get flyInEvent(): Listener<[duration: number | undefined, id: number]> { return this._flyInEvent; }
    static _lastFlyInId = 0;
    flyIn(duration?: number) {
        ++ESVisualObject._lastFlyInId;
        this._flyInEvent.emit(duration, ESVisualObject._lastFlyInId);
    }

    private _flyToEvent = this.disposeVar(new Event<[duration: number | undefined, id: number]>());
    get flyToEvent(): Listener<[duration: number | undefined, id: number]> { return this._flyToEvent; }
    static _lastFlyToId = 0;
    flyTo(duration?: number) {
        ++ESVisualObject._lastFlyToId;
        this._flyToEvent.emit(duration, ESVisualObject._lastFlyToId);
    }
    // flyTo的mode cancelled表示飞行过程中被强制取消了 over表示飞行正常结束 error表示出现其他错误
    private _flyOverEvent = this.disposeVar(new Event<[id: number, mode: 'cancelled' | 'over' | 'error', viewer: Viewer]>());
    get flyOverEvent() { return this._flyOverEvent; }

    private _pickedEvent = this.disposeVar(new Event<[PickedInfo]>());
    get pickedEvent() { return this._pickedEvent; }

    private _calcFlyToParamEvent = this.disposeVar(new Event());
    get calcFlyToParamEvent(): Listener { return this._calcFlyToParamEvent; }
    calcFlyToParam() { this._calcFlyToParamEvent.emit(); }

    /**
     * 是否使用ESObjectWithLocation类中的calcFlyToParam
     */
    public useCalcFlyToParamInESObjectWithLocation = false;

    /**
     * 清空飞向参数  
     * https://www.wolai.com/earthsdk/u1sLHxcj6PErXf8ubcvC4j#pFGFyqbrwMcf6dJZq4YTnG
     */
    emptyFlyToParam() {
        this.flyToParam = undefined;
    }

    /**
     * 清空飞入参数  
     * https://www.wolai.com/earthsdk/u1sLHxcj6PErXf8ubcvC4j#mLbWgmfVmj3tFb1e1Zn9H
     */
    emptyFlyInParam() {
        this.flyInParam = undefined;
    }

    private _calcFlyInParamEvent = this.disposeVar(new Event());
    get calcFlyInParamEvent(): Listener { return this._calcFlyInParamEvent; }
    calcFlyInParam() { this._calcFlyInParamEvent.emit(); }

    async getBoundSphere(viewer: Viewer) {
        if (viewer instanceof UeViewer) {
            return await viewer.getBoundSphere(this.id)
        } else {
            return undefined;
        }
    }

    static override defaults = {
        ...ESSceneObject.defaults,
        show: true,
        collision: true,
        allowPicking: false,
        flyToParam: { distance: 0, heading: 0, pitch: 0, flyDuration: 1, hDelta: 0, pDelta: 0 } as ESFlyToParam,
        flyInParam: { position: [0, 0, 0], rotation: [0, 0, 0], flyDuration: 1 } as ESFlyInParam,
    };

    getESProperties() {
        return {
            defaultMenu: 'GeneralProprties',
            basic: [],
            general: [
                new StringProperty('名称', 'name', true, false, [this, 'name']),
                new StringProperty('唯一标识', 'id', false, true, [this, 'id']),
                new BooleanProperty('是否显示', 'show', false, false, [this, 'show'], true),
                new BooleanProperty('开启碰撞', 'collision', false, false, [this, 'collision'], true),
                new BooleanProperty('允许拾取', 'allowPicking', false, false, [this, 'allowPicking'], false),
                // new FunctionProperty("飞入", "飞入flyTo", ['number'], (duration: number) => this.flyTo(duration ?? 1), [1]),
                // new FunctionProperty("飞向", "飞向flyIn", ['number'], (duration: number) => this.flyIn(duration ?? 1), [1]),
                new FunctionProperty("保存观察视角", "保存当前flyToParam", [], () => this.calcFlyToParam(), []),
                new FunctionProperty("清空飞向参数", "清空飞向参数", [], () => this.emptyFlyToParam(), []),
                new FunctionProperty("清空飞入参数", "清空飞入参数", [], () => this.emptyFlyInParam(), []),
                new FunctionProperty('保存飞入参数', '保存飞入参数flyInParam', [], () => this.calcFlyInParam(), []),
            ],
            dataSource: [],
            location: [],
            coordinate: [],
            style: [],
        } as {
            defaultMenu: string;
            basic: Property[];
            general: Property[];
            dataSource: Property[];
            location: Property[];
            coordinate: Property[];
            style: Property[];
        }
    };

    override getProperties(language?: string) {
        return [
            ...super.getProperties(language),
            new GroupProperty('可视对象', '可视对象ESVisualObject', [
                new BooleanProperty('是否显示', '是否显示.', false, false, [this, 'show'], ESVisualObject.defaults.show),
                new BooleanProperty('是否开启碰撞监测', 'collision,是否开启碰撞监测.', false, false, [this, 'collision'], ESVisualObject.defaults.collision),
                new BooleanProperty('是否允许拾取', '是否允许被鼠标点击拾取.', false, false, [this, 'allowPicking'], ESVisualObject.defaults.allowPicking),
                new GroupProperty('飞行定位', '飞向参数', [
                    new FunctionProperty("飞向", "飞向", ['number'], (duration: number) => this.flyTo(duration), [1]),
                    new JsonProperty('flyToParam', 'flyToParam', true, false, [this, 'flyToParam'], ESVisualObject.defaults.flyToParam),
                    new FunctionProperty("获取当前参数", "获取当前flyToParam", [], () => this.calcFlyToParam(), []),
                    new NumberProperty('flyToDistance', 'flyToDistance', true, false, [this, 'flyToDistance'], ESVisualObject.defaults.flyToParam.distance),
                    new NumberProperty('flyToHeading', 'flyToHeading', true, false, [this, 'flyToHeading'], ESVisualObject.defaults.flyToParam.heading),
                    new NumberProperty('flyToPitch', 'flyToPitch', true, false, [this, 'flyToPitch'], ESVisualObject.defaults.flyToParam.pitch),
                    new NumberProperty('flyToFlyDuration', 'flyToFlyDuration', true, false, [this, 'flyToFlyDuration'], ESVisualObject.defaults.flyToParam.flyDuration),
                    new NumberProperty('flyToHDelta', 'flyToHDelta', true, false, [this, 'flyToHDelta'], ESVisualObject.defaults.flyToParam.hDelta),
                    new NumberProperty('flyToPDelta', 'flyToPDelta', true, false, [this, 'flyToPDelta'], ESVisualObject.defaults.flyToParam.pDelta),
                ]),
                new GroupProperty('flyInParam', 'flyInParam', [
                    new FunctionProperty("飞入", "飞入", ["number"], (duration: number) => this.flyIn(duration), [1]),
                    new JsonProperty('flyInParam', 'flyInParam', true, false, [this, 'flyInParam'], ESVisualObject.defaults.flyInParam),
                    new FunctionProperty('获取当前相机参数', '获取当前相机参数', [], () => this.calcFlyInParam(), []),
                ]),
            ]),
        ];
    }
    get flyToDistance() { return this.flyToParam && this.flyToParam.distance; }
    get flyToHeading() { return this.flyToParam && this.flyToParam.heading; }
    get flyToPitch() { return this.flyToParam && this.flyToParam.pitch; }
    get flyToFlyDuration() { return this.flyToParam && this.flyToParam.flyDuration; }
    get flyToHDelta() { return this.flyToParam && this.flyToParam.hDelta; }
    get flyToPDelta() { return this.flyToParam && this.flyToParam.pDelta; }

    get flyToDistanceChanged() { return this.flyToParamChanged; }
    get flyToHeadingChanged() { return this.flyToParamChanged; }
    get flyToPitchChanged() { return this.flyToParamChanged; }
    get flyToFlyDurationChanged() { return this.flyToParamChanged; }
    get flyToHDeltaChanged() { return this.flyToParamChanged; }
    get flyToPDeltaChanged() { return this.flyToParamChanged; }

    set flyToDistance(value: number | undefined) { this.flyToParam = (value !== undefined) ? ({ ...(this.flyToParam ?? ESVisualObject.defaults.flyToParam), distance: value }) : undefined; }
    set flyToHeading(value: number | undefined) { this.flyToParam = (value !== undefined) ? ({ ...(this.flyToParam ?? ESVisualObject.defaults.flyToParam), heading: value }) : undefined; }
    set flyToPitch(value: number | undefined) { this.flyToParam = (value !== undefined) ? ({ ...(this.flyToParam ?? ESVisualObject.defaults.flyToParam), pitch: value }) : undefined; }
    set flyToFlyDuration(value: number | undefined) { this.flyToParam = (value !== undefined) ? ({ ...(this.flyToParam ?? ESVisualObject.defaults.flyToParam), flyDuration: value }) : undefined; }
    set flyToHDelta(value: number | undefined) { this.flyToParam = (value !== undefined) ? ({ ...(this.flyToParam ?? ESVisualObject.defaults.flyToParam), hDelta: value }) : undefined; }
    set flyToPDelta(value: number | undefined) { this.flyToParam = (value !== undefined) ? ({ ...(this.flyToParam ?? ESVisualObject.defaults.flyToParam), pDelta: value }) : undefined; }
}

export namespace ESVisualObject {
    export const createDefaultProps = () => ({
        ...ESSceneObject.createDefaultProps(),
        /**
         * https://www.wolai.com/earthsdk/u1sLHxcj6PErXf8ubcvC4j#aV7NLcX7GfjAvfEJwY3qVR
         */
        show: true,
        collision: true,
        allowPicking: false,
        flyToParam: reactPlainObjectWithUndefined<ESFlyToParam>(undefined),
        flyInParam: reactJsonWithUndefined<ESFlyInParam>(undefined),
    });
}
extendClassProps(ESVisualObject.prototype, ESVisualObject.createDefaultProps);
export interface ESVisualObject extends ReactivePropsToNativePropsAndChanged<ReturnType<typeof ESVisualObject.createDefaultProps>> { }
type JsonType = PartialWithUndefinedReactivePropsToNativeProps<ReturnType<typeof ESVisualObject.createDefaultProps> & { type: string }>;
