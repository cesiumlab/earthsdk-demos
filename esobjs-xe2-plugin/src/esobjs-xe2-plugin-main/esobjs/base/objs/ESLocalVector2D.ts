import { ReactVarProperty } from "xbsj-xe2/dist-node/xe2-base-objects";
import { extendClassProps, PartialWithUndefinedReactivePropsToNativeProps, ReactivePropsToNativePropsAndChanged } from "xbsj-xe2/dist-node/xe2-base-utils";

import { ESLocalVector } from "./ESLocalVector";
/**
 * https://c0yh9tnn0na.feishu.cn/docx/CBHZdsYHNolnxGx3BLwcFWsInlb
 */
export abstract class ESLocalVector2D extends ESLocalVector {

    static override defaults = {
        ...ESLocalVector.defaults,
    };
    override getESProperties() { return { ...super.getESProperties() } };
    override getProperties(language?: string) {
        return [
            ...super.getProperties(language),
        ];
    }
}

export namespace ESLocalVector2D {
    export const createDefaultProps = () => ({
        ...ESLocalVector.createDefaultProps(),
    });
}
extendClassProps(ESLocalVector2D.prototype, ESLocalVector2D.createDefaultProps);
export interface ESLocalVector2D extends ReactivePropsToNativePropsAndChanged<ReturnType<typeof ESLocalVector2D.createDefaultProps>> { }
type JsonType = PartialWithUndefinedReactivePropsToNativeProps<ReturnType<typeof ESLocalVector2D.createDefaultProps> & { type: string }>;
