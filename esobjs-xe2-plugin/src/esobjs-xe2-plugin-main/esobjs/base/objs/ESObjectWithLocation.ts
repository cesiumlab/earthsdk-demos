import { BooleanProperty, FunctionProperty, GroupProperty, Number3Property, PositionProperty, RotationProperty, ReactVarProperty, NumberProperty } from "xbsj-xe2/dist-node/xe2-base-objects";
import { Event, PartialWithUndefinedReactivePropsToNativeProps, ReactivePropsToNativePropsAndChanged, extendClassProps, react, reactArray, Listener } from "xbsj-xe2/dist-node/xe2-base-utils";
import { ESVisualObject } from "./ESVisualObject";
/**
 * https://www.wolai.com/earthsdk/tE8S54xN8LZtbQRd9GP8Ya
 * https://c0yh9tnn0na.feishu.cn/docx/EahMd0UTjoY76IxL6EVctgkunJf
 */
export abstract class ESObjectWithLocation extends ESVisualObject {
    private _smoothMoveEvent = this.disposeVar(new Event<[[number, number, number], number]>());
    get smoothMoveEvent() { return this._smoothMoveEvent; }

    private _statusDis = this.disposeVar(react<boolean>(true));
    get statusDis() { return this._statusDis.value; }
    get statusDisChanged() { return this._statusDis.changed; }
    /**
     * 平滑移动到指定位置
     * @param Destination - 目标位置，格式为[经度, 纬度, 高度]
     * @param Time - 平滑移动所需的时间，单位为秒
     */
    smoothMove(Destination: [number, number, number], Time: number) { this._smoothMoveEvent.emit(Destination, Time); }

    private _smoothMoveWithRotationEvent = this.disposeVar(new Event<[[number, number, number], [number, number, number], number]>());
    get smoothMoveWithRotationEvent() { return this._smoothMoveWithRotationEvent; }
    /**
     * 平滑偏移到指定位置和姿态
     * @param destination - 目标位置，格式为[经度, 纬度, 高度]
     * @param newRotation - 目标姿态，格式为[偏航角, 俯仰角, 翻转角]
     * @param time - 平滑移动所需的时间，单位为秒
     */
    smoothMoveWithRotation(Destination: [number, number, number], NewRotation: [number, number, number], Time: number) { this._smoothMoveWithRotationEvent.emit(Destination, NewRotation, Time); }


    private _smoothMoveOnGroundEvent = this.disposeVar(new Event<[number, number, number, string]>());
    get smoothMoveOnGroundEvent() { return this._smoothMoveOnGroundEvent; };
    /**
     * 贴地平滑移动
     * @param Lon - 目标位置的经度
     * @param Lat - 目标位置的纬度
     * @param Time - 平滑移动所需的时间，单位为秒
     * @param Ground - 地面类型，ue特有属性
     */
    smoothMoveOnGround(Lon: number, Lat: number, Time: number, Ground: string) { this._smoothMoveOnGroundEvent.emit(Lon, Lat, Time, Ground); }

    private _smoothMoveWithRotationOnGroundEvent = this.disposeVar(new Event<[[number, number, number], number, number, number, string]>());
    get smoothMoveWithRotationOnGroundEvent() { return this._smoothMoveWithRotationOnGroundEvent; }
    /**
     * 贴地平滑偏移到指定位置和姿态
     * @param newRotation - 目标姿态，格式为[偏航角, 俯仰角, 翻转角]
     * @param lon - 目标位置的经度
     * @param lat - 目标位置的纬度
     * @param time - 平滑移动所需的时间，单位为秒
     * @param ground - 地面类型，ue特有属性
     */
    smoothMoveWithRotationOnGround(NewRotation: [number, number, number], Lon: number, Lat: number, Time: number, Ground: string) { this._smoothMoveWithRotationOnGroundEvent.emit(NewRotation, Lon, Lat, Time, Ground); }

    private _automaticLandingEvent = this.disposeVar(new Event<[flag: boolean]>());
    get automaticLandingEvent(): Listener<[flag: boolean]> { return this._automaticLandingEvent; }
    automaticLanding() {
        const a = this.collision
        this.collision = false
        setTimeout(() => {
            this._automaticLandingEvent.emit(a);
        }, 100)
    }
    //下面的未更改
    private _smoothMoveKeepPitchEvent = this.disposeVar(new Event<[[number, number, number], number]>());
    get smoothMoveKeepPitchEvent() { return this._smoothMoveKeepPitchEvent; }
    smoothMoveKeepPitch(Destination: [number, number, number], Time: number) { this._smoothMoveKeepPitchEvent.emit(Destination, Time); }

    private _smoothMoveRelativelyEvent = this.disposeVar(new Event<[[number, number, number], number]>());
    get smoothMoveRelativelyEvent() { return this._smoothMoveRelativelyEvent; }
    smoothMoveRelatively(RelativePosition: [number, number, number], Time: number) { this._smoothMoveRelativelyEvent.emit(RelativePosition, Time); }

    private _smoothMoveRelativelyWithRotationEvent = this.disposeVar(new Event<[[number, number, number], [number, number, number], number]>());
    get smoothMoveRelativelyWithRotationEvent() { return this._smoothMoveRelativelyWithRotationEvent; }
    smoothMoveRelativelyWithRotation(RelativePosition: [number, number, number], NewRotation: [number, number, number], Time: number) { this._smoothMoveRelativelyWithRotationEvent.emit(RelativePosition, NewRotation, Time); }

    // automaticLanding
    private _editing = this.disposeVar(react<boolean>(false));
    get editing() { return this._editing.value; }
    get editingChanged() { return this._editing.changed; }
    set editing(value: boolean) { this._editing.value = value; }

    static override defaults = {
        ...ESVisualObject.defaults,
        // position: [
        //     116.39939899795445,
        //     39.909684089625195,
        //     0.16519451659038167
        // ] as [number, number, number],
        // rotation: [0, 0, 0] as [number, number, number],
        // editing: false,
        // scale: [1, 1, 1] as [number, number, number]
    }

    /**
     * 是否使用ESObjectWithLocation类中的calcFlyToParam
     */
    public override useCalcFlyToParamInESObjectWithLocation = true;
    override getESProperties() {
        const properties = { ...super.getESProperties() };
        return {
            ...properties,
            defaultMenu: 'BasicProprties',
            location: [
                ...properties.location,
                new BooleanProperty('是否编辑', '是否编辑', false, false, [this, 'editing']),
                new FunctionProperty("自动落地", "自动落地", [], () => this.automaticLanding(), []),
                new PositionProperty('位置数组', '经度，纬度，高度，度为单位', false, false, [this, 'position'], [0, 0, 0]),
                new RotationProperty('姿态数组', '偏航角，俯仰角，翻转角，度为单位', false, false, [this, 'rotation'], [0, 0, 0]),
                new Number3Property('缩放', '缩放', false, false, [this, 'scale'], [1, 1, 1]),
                new NumberProperty('最小可见距离', '单位米', false, false, [this, 'minVisibleDistance'],0),
                new NumberProperty('最大可见距离', '单位米', false, false, [this, 'maxVisibleDistance'],0),
            ]
        };
    };
    override getProperties(language?: string) {
        return [
            ...super.getProperties(language),
            new GroupProperty('位置姿态对象', '位置姿态对象ESObjectWithLocation', [
                new FunctionProperty('平滑移动', 'smoothMove', ['numbers', 'number'], (Destination: [number, number, number], Time: number) => this.smoothMove(Destination, Time), [[0, 0, 0], 0]),
                new FunctionProperty('固定方向平滑移动', 'smoothMoveKeepPitch', ['numbers', 'number'], (Destination: [number, number, number], Time: number) => this.smoothMoveKeepPitch(Destination, Time), [[0, 0, 0], 0]),
                new FunctionProperty('平滑偏移', 'smoothMoveWithRotation', ['numbers', 'numbers', 'number'], (destination: [number, number, number], newRotation: [number, number, number], time: number) => this.smoothMoveWithRotation(destination, newRotation, time), [[0, 0, 0], [0, 0, 0], 0]),
                new FunctionProperty('贴地平滑移动', 'smoothMoveOnGround', ['number', 'number', 'number', 'string'], (Lon: number, Lat: number, Time: number, Ground: string) => this.smoothMoveOnGround(Lon, Lat, Time, Ground), [0, 0, 0, '']),
                new FunctionProperty('贴地平滑偏移', 'smoothMoveWithRotationOnGround', ['numbers', 'number', 'number', 'number', 'string'], (NewRotation: [number, number, number], Lon: number, Lat: number, Time: number, Ground: string) => this.smoothMoveWithRotationOnGround(NewRotation, Lon, Lat, Time, Ground), [[0, 0, 0], 0, 0, 0, '']),
                new FunctionProperty('smoothMoveRelatively', 'smoothMoveRelatively', ['numbers', 'number'], (RelativePosition: [number, number, number], Time: number) => this.smoothMoveRelatively(RelativePosition, Time), [[0, 0, 0], 0]),
                new FunctionProperty('smoothMoveRelativelyWithRotation', 'smoothMoveRelativelyWithRotation', ['numbers', 'numbers', 'number'], (RelativePosition: [number, number, number], NewRotation: [number, number, number], time: number) => this.smoothMoveRelativelyWithRotation(RelativePosition, NewRotation, time), [[0, 0, 0], [0, 0, 0], 0]),

                new PositionProperty('位置数组', '经度，纬度，高度，度为单位', false, false, [this, 'position']),
                new RotationProperty('姿态数组', '偏航角，俯仰角，翻转角，度为单位', false, false, [this, 'rotation']),
                new Number3Property('缩放', '缩放', false, false, [this, 'scale']),
                new BooleanProperty('是否编辑', '是否开启编辑状态', true, false, [this, 'editing'], false),
                new NumberProperty('最小可见距离', '单位米', false, false, [this, 'minVisibleDistance']),
                new NumberProperty('最大可见距离', '单位米', false, false, [this, 'maxVisibleDistance']),
                new BooleanProperty('是否应用距离显隐','是否应用距离显隐',false,false,[this,'enableVisibleDistance'])
            ]),
        ];
    }
}

export namespace ESObjectWithLocation {
    export const createDefaultProps = () => ({
        position: reactArray<[number, number, number]>([0, 0, 0]),
        rotation: reactArray<[number, number, number]>([0, 0, 0]),
        // editing: undefined as boolean | undefined,
        scale: reactArray<[number, number, number]>([1, 1, 1]),
        minVisibleDistance: react<number>(0),
        maxVisibleDistance: react<number>(0),
        ...ESVisualObject.createDefaultProps(),
    });
}
extendClassProps(ESObjectWithLocation.prototype, ESObjectWithLocation.createDefaultProps);
export interface ESObjectWithLocation extends ReactivePropsToNativePropsAndChanged<ReturnType<typeof ESObjectWithLocation.createDefaultProps>> { }
type JsonType = PartialWithUndefinedReactivePropsToNativeProps<ReturnType<typeof ESObjectWithLocation.createDefaultProps> & { type: string }>;
