import { createNextAnimateFrameEvent } from 'xbsj-xe2/dist-node/xe2-base-utils';
import { ESLabel } from '../../objs';
import { UeViewer } from 'xbsj-xe2/dist-node/xe2-ue-objects';
import { UeESObjectWithLocation } from '../UeESObjectWithLocation';

export class UeESLabel<T extends ESLabel = ESLabel> extends UeESObjectWithLocation<T> {
    constructor(sceneObject: T, ueViewer: UeViewer) {
        super(sceneObject, ueViewer);

        const viewer = ueViewer.viewer;
        if (!viewer) {
            console.warn(`viewer is undefined!`);
            return;
        }

        this.d(ueViewer.widgetEvent.don((info) => {
            if (info.objId !== sceneObject.id) return
            const { type, add } = info;
            sceneObject.widgetEvent.emit({ type, add });
        }))
        // 暂时保留，以后需要删掉
        // const update = () => {
        //     viewer.callUeFunc({
        //         f: 'update',
        //         p: {
        //             id: sceneObject.id,
        //             screenRender: sceneObject.screenRender,
        //             size: sceneObject.size,
        //             anchor: sceneObject.anchor,
        //             sizeByContent: sceneObject.sizeByContent,
        //             renderMode: sceneObject.renderMode,
        //             rotationType: sceneObject.rotationType,
        //         }
        //     })
        // };

        // const updateEvent = this.disposeVar(createNextAnimateFrameEvent(
        //     sceneObject.screenRenderChanged,
        //     sceneObject.sizeChanged,
        //     sceneObject.anchorChanged,
        //     sceneObject.sizeByContentChanged,
        //     sceneObject.renderModeChanged,
        //     sceneObject.rotationTypeChanged,
        // ));
        // this.dispose(updateEvent.disposableOn(update));
        // this.dispose(sceneObject.ueCreatedEvent.disposableOn(update));
        // this.dispose(sceneObject.flushEvent.disposableOn(() => updateEvent.flush()));
    }
}
