import { smoothMoveCallFunc, smoothMoveOnGroundCallFunc, smoothMoveWithRotationCallFunc, smoothMoveWithRotationOnGroundCallFunc } from 'xbsj-xe2/dist-node/xe2-ue-objects';
import { UeViewer } from 'xbsj-xe2/dist-node/xe2-ue-objects';
import { ESObjectWithLocation } from '../../objs';
import { UeESVisualObject } from '../UeESVisualObject';
import { createNextAnimateFrameEvent } from 'xbsj-renderer/dist-node/xr-base-utils';

export class UeESObjectWithLocation<T extends ESObjectWithLocation = ESObjectWithLocation> extends UeESVisualObject<T> {

    static override forceUeUpdateProps = [
        ...UeESVisualObject.forceUeUpdateProps,
        'editing',
    ];
    // static override propValFuncs = {
    //     ...UeESVisualObject.propValFuncs,
    // }
    constructor(sceneObject: T, ueViewer: UeViewer) {
        super(sceneObject, ueViewer);

        const viewer = ueViewer.viewer;
        if (!viewer) {
            console.warn(`viewer is undefined!`);
            return;
        }

        const update = () => {
            //多发一次测试
            if (sceneObject.editing) return;
            viewer.callUeFunc({
                f: 'update',
                p: {
                    id: sceneObject.id,
                    position: sceneObject.position,
                    rotation: sceneObject.rotation
                }
            })
        };
        const updateEvent = this.disposeVar(createNextAnimateFrameEvent(
            sceneObject.positionChanged,
            sceneObject.rotationChanged,
            sceneObject.editingChanged,
        ));
        this.dispose(updateEvent.don(() => { setTimeout(update, 0) }));
        this.d(sceneObject.flushEvent.don(() => updateEvent.flush()));//实时刷新position和rotation

        // 暂时保留，以后需要删掉
        // const update = () => {
        //     viewer.callUeFunc({
        //         f: 'update',
        //         p: {
        //             id: sceneObject.id,
        //             position: sceneObject.position,
        //             rotation: sceneObject.rotation,
        //             editing: sceneObject.editing,
        //             scale: sceneObject.scale,
        //         }
        //     })
        // };
        // const updateEvent = this.disposeVar(createNextAnimateFrameEvent(
        //     sceneObject.positionChanged,
        //     sceneObject.rotationChanged,
        //     sceneObject.editingChanged,
        //     sceneObject.scaleChanged,
        // ));
        // this.dispose(updateEvent.disposableOn(update));
        // this.dispose(sceneObject.ueCreatedEvent.disposableOn(update));
        // this.d(sceneObject.flushEvent.don(() => updateEvent.flush()));

        this.dispose(sceneObject.smoothMoveEvent.disposableOn((Destination: [number, number, number], Time: number) => {
            smoothMoveCallFunc(viewer, sceneObject.id, Destination, Time)
        }))
        this.dispose(sceneObject.smoothMoveWithRotationEvent.disposableOn((Destination: [number, number, number], NewRotation: [number, number, number], Time: number) => {
            smoothMoveWithRotationCallFunc(viewer, sceneObject.id, Destination, NewRotation, Time)
        }))
        this.dispose(sceneObject.smoothMoveOnGroundEvent.disposableOn((Lon: number, Lat: number, Time: number, Ground: string) => {
            smoothMoveOnGroundCallFunc(viewer, sceneObject.id, Lon, Lat, Ground, Time)
        }))
        this.dispose(sceneObject.smoothMoveWithRotationOnGroundEvent.disposableOn((NewRotation: [number, number, number], Lon: number, Lat: number, Time: number, Ground: string) => {
            smoothMoveWithRotationOnGroundCallFunc(viewer, sceneObject.id, NewRotation, Lon, Lat, Time, Ground)
        }))

        type CallUeFuncResult = {
            error: string | undefined;
        }

        this.dispose(sceneObject.smoothMoveKeepPitchEvent.disposableOn(async (Destination: [number, number, number], Time: number) => {
            const res = await viewer.callUeFunc<CallUeFuncResult>({
                f: 'SmoothMoveKeepPitch',
                p: {
                    id: sceneObject.id,
                    Destination, Time
                }
            });
            if (res.error) console.error(`SmoothMoveKeepPitch:`, res.error);
        }))


        //自动落地
        this.dispose(sceneObject.automaticLandingEvent.disposableOn((flag) => {
            const posi = [sceneObject.position[0], sceneObject.position[1]] as [number, number]
            ueViewer.getHeightByLonLat(sceneObject.position[0], sceneObject.position[1], 'Visibility').then((res) => {
                if (res && res.height) {
                    sceneObject.position = [...posi, res.height]
                    sceneObject.collision = flag
                }
            })

            // if (sceneObject.collision) {
            //     sceneObject.collision = false
            //     ueViewer.getHeightByLonLat(sceneObject.position[0], sceneObject.position[1], 'Visibility').then((res) => {
            //         if (res && res.height) {
            //             console.log(' res.height', res.height);

            //             console.log(sceneObject.collision);
            //             sceneObject.position = [...posi, res.height]
            //             sceneObject.collision = true
            //         }
            //     })
            // } else {
            //     ueViewer.getHeightByLonLat(sceneObject.position[0], sceneObject.position[1], 'Visibility').then((res) => {
            //         if (res && res.height) {
            //             sceneObject.position = [...posi, res.height]
            //         }
            //     })
            // }
        }));


        type SmoothMoveRelativelyType = {
            SmoothMoveRelatively: {
                params: {
                    id: string,
                    RelativePosition: [number, number, number],
                    Time: number
                },
                result: {
                    error: string | undefined;
                }
            }
        }
        type SmoothMoveRelativelyWithRotationType = {
            SmoothMoveRelativelyWithRotation: {
                params: {
                    id: string,
                    RelativePosition: [number, number, number],
                    NewRotation: [number, number, number,]
                    Time: number
                },
                result: {
                    error: string | undefined;
                }
            }
        }
        const SmoothMoveRelativelyCallFunc = async (ueViewer: UeViewer, id: string, RelativePosition: [number, number, number], Time: number) => {
            const { viewer } = ueViewer;
            if (!viewer) {
                console.error(`SmoothMoveRelatively: ueViewer.viewer is undefined`);
                return undefined;
            }
            const res = await viewer.callUeFunc<SmoothMoveRelativelyType['SmoothMoveRelatively']['result']>({
                f: 'SmoothMoveRelatively',
                p: { id, RelativePosition, Time }
            })
            if (res.error) console.error(`SmoothMoveRelatively:`, res.error);
            return res;
        }

        this.dispose(sceneObject.smoothMoveRelativelyEvent.disposableOn((RelativePosition, Time) => {
            SmoothMoveRelativelyCallFunc(ueViewer, sceneObject.id, RelativePosition, Time)
        }))

        const SmoothMoveRelativelyWithRotationCallFunc = async (ueViewer: UeViewer, id: string, RelativePosition: [number, number, number], NewRotation: [number, number, number], Time: number) => {
            const { viewer } = ueViewer;
            if (!viewer) {
                console.error(`SmoothMoveRelativelyWithRotation: ueViewer.viewer is undefined`);
                return undefined;
            }
            const res = await viewer.callUeFunc<SmoothMoveRelativelyWithRotationType['SmoothMoveRelativelyWithRotation']['result']>({
                f: 'SmoothMoveRelativelyWithRotation',
                p: { id, RelativePosition, NewRotation, Time }
            })
            if (res.error) console.error(`SmoothMoveRelativelyWithRotation:`, res.error);
            return res;
        }
        this.dispose(sceneObject.smoothMoveRelativelyWithRotationEvent.disposableOn((RelativePosition, NewRotation, Time) => {
            SmoothMoveRelativelyWithRotationCallFunc(ueViewer, sceneObject.id, RelativePosition, NewRotation, Time)
        }))

    }
}
