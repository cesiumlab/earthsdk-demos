import { SceneObjectPickedInfo } from 'xbsj-xe2/dist-node/xe2-base-objects';
import { createNextAnimateFrameEvent } from 'xbsj-xe2/dist-node/xe2-base-utils';
import { UeViewer, calcFlyToParamCallFunc, destroyCallFunc, flyToCallFunc, UeObject, flyInCallFunc } from "xbsj-xe2/dist-node/xe2-ue-objects";
import { ESVisualObject } from '../../objs';
import { UeESSceneObject } from '../UeESSceneObject';


export class UeESVisualObject<T extends ESVisualObject = ESVisualObject> extends UeESSceneObject<T> {
    constructor(sceneObject: T, ueViewer: UeViewer) {
        super(sceneObject, ueViewer);
        const viewer = ueViewer.viewer;
        if (!viewer) {
            console.warn(`viewer is undefined!`);
            return;
        }

        // const update = () => {
        //     viewer.callUeFunc({
        //         f: 'update',
        //         p: {
        //             id: sceneObject.id,
        //             show: sceneObject.show,
        //             collision: sceneObject.collision,
        //             allowPicking: sceneObject.allowPicking,
        //             flyToParam: sceneObject.flyToParam ?? ESVisualObject.defaults.flyToParam,
        //             flyInParam: sceneObject.flyInParam ?? ESVisualObject.defaults.flyInParam,
        //         }
        //     })
        // };
        // const updateEvent = this.disposeVar(createNextAnimateFrameEvent(
        //     sceneObject.showChanged,
        //     sceneObject.flyToParamChanged,
        //     sceneObject.allowPickingChanged,
        //     sceneObject.collisionChanged,
        //     sceneObject.flyInParamChanged,
        // ));
        // this.dispose(updateEvent.disposableOn(update));
        // this.dispose(sceneObject.flushEvent.disposableOn(() => updateEvent.flush()));

        // {
        //     let created = false;
        //     this.dispose(() => {
        //         if (created) destroyCallFunc(viewer, sceneObject.id)
        //     });

        //     viewer.callUeFunc({
        //         f: 'create',
        //         p: {
        //             type: sceneObject.typeName,
        //             id: sceneObject.id,
        //         }
        //     }).then(() => {
        //         created = true;
        //         // update();
        //         sceneObject.ueCreatedEvent.emit();
        //     }).catch(err => console.error(err));
        // }

        this.dispose(sceneObject.flyToEvent.disposableOn(async (duration, id) => {
            const res = await flyToCallFunc(viewer, sceneObject.id, duration)
            let mode: 'cancelled' | 'over' | 'error' = 'over';
            if (res === undefined) {
                mode = 'error'
            } else if (res.endType === 0) {
                mode = 'over'
            } else if (res.endType === 1) {
                mode = 'cancelled'
            }
            sceneObject.flyOverEvent.emit(id, mode, ueViewer);
        }));
        this.dispose(sceneObject.flyInEvent.disposableOn(async (duration, id) => {
            const res = await flyInCallFunc(viewer, sceneObject.id, sceneObject.flyInParam?.position, sceneObject.flyInParam?.rotation, (duration ?? 1))
            let mode: 'cancelled' | 'over' | 'error' = 'over';
            if (res === undefined) {
                mode = 'error'
            } else if (res.endType === 0) {
                mode = 'over'
            } else if (res.endType === 1) {
                mode = 'cancelled'
            }
            sceneObject.flyOverEvent.emit(id, mode, ueViewer);
        }));

        this.dispose(sceneObject.calcFlyToParamEvent.disposableOn(() => {
            calcFlyToParamCallFunc(viewer, sceneObject.id)
        }));
        this.dispose(sceneObject.calcFlyInParamEvent.disposableOn(async () => {
            if (!ueViewer.actived) return;
            const cameraInfo = await ueViewer.getCurrentCameraInfo();
            if (!cameraInfo) return;
            const { position, rotation } = cameraInfo;
            sceneObject.flyInParam = { position, rotation, flyDuration: 1 };
        }));

        this.dispose(ueViewer.propChanged.disposableOn((info) => {
            if (info.objId !== sceneObject.id) return
            Object.keys(info.props).forEach(key => {
                const prop = info.props[key] === null ? undefined : info.props[key]
                // @ts-ignore
                sceneObject[key] = prop
            });
        }));

        this.dispose(ueViewer.uePickedEvent.disposableOn((uePickInfo) => {
            if (!uePickInfo) return;
            const { uePickResult } = uePickInfo;
            if (!(uePickResult && uePickResult.id)) return;
            if (uePickResult.id === sceneObject.id) {
                if (sceneObject.allowPicking ?? false) {
                    sceneObject.pickedEvent.emit(new SceneObjectPickedInfo(sceneObject, uePickInfo));
                } else {

                    //@ts-ignore
                    ueViewer.sceneObjectNotAllowPickEvent.emit(uePickInfo);
                }
            }
        }));

    }
}
