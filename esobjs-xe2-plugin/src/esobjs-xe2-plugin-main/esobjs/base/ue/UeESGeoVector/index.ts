import { createNextAnimateFrameEvent } from 'xbsj-xe2/dist-node/xe2-base-utils';
import { ESFillStyle, ESGeoVector, ESPointStyle, ESStrokeStyle } from '../../objs';
import { UeViewer } from 'xbsj-xe2/dist-node/xe2-ue-objects';
import { UeESVisualObject } from '../UeESVisualObject';

export class UeESGeoVector<T extends ESGeoVector = ESGeoVector> extends UeESVisualObject<T> {
    static override forceUeUpdateProps = [
        ...UeESVisualObject.forceUeUpdateProps,
        'editing',
    ];

    static override propValFuncs = {
        ...UeESVisualObject.propValFuncs,
        pointStyle: (val: ESPointStyle) => ({
            ...val,
            materialParams: JSON.stringify(val.materialParams ?? {}),
        }),
        strokeStyle: (val: ESStrokeStyle) => ({
            ...val,
            materialParams: JSON.stringify(val.materialParams ?? {}),
        }),
        fillStyle: (val: ESFillStyle) => ({
            ...val,
            materialParams: JSON.stringify(val.materialParams ?? {}),
        }),
    };

    constructor(sceneObject: T, ueViewer: UeViewer) {
        super(sceneObject, ueViewer);
        const viewer = ueViewer.viewer;
        if (!viewer) {
            console.warn(`viewer is undefined!`);
            return;
        }

        const update = () => {
            //多发一次测试
            if (sceneObject.editing) return;
            viewer.callUeFunc({
                f: 'update',
                p: {
                    id: sceneObject.id,
                    points: sceneObject.points
                }
            })
        };
        const updateEvent = this.disposeVar(createNextAnimateFrameEvent(
            sceneObject.pointsChanged,
            sceneObject.editingChanged,
        ));
        this.dispose(updateEvent.don(() => { setTimeout(update, 0) }));

        // 暂时保留，以后需要删掉
        // const update = () => {
        //     // let pointStyle = ESGeoVector.defaults.pointStyle
        //     // let strokeStyle = ESGeoVector.defaults.strokeStyle
        //     // let fillStyle = ESGeoVector.defaults.fillStyle
        //     // try {
        //     //     pointStyle = { ...(sceneObject.pointStyle ?? ESGeoVector.defaults.pointStyle) };
        //     //     strokeStyle = { ...(sceneObject.strokeStyle ?? ESGeoVector.defaults.strokeStyle) };
        //     //     fillStyle = { ...(sceneObject.fillStyle ?? ESGeoVector.defaults.fillStyle) };
        //     // } catch (e) {
        //     //     console.error('ESGeoVector 属性类型错误!', e)
        //     //     strokeStyle = { ...ESGeoVector.defaults.strokeStyle }
        //     //     pointStyle = { ...ESGeoVector.defaults.pointStyle }
        //     //     fillStyle = { ...ESGeoVector.defaults.fillStyle }
        //     // }
        //     // strokeStyle.materialParams = JSON.stringify(strokeStyle.materialParams);
        //     // pointStyle.materialParams = JSON.stringify(pointStyle.materialParams);
        //     // fillStyle.materialParams = JSON.stringify(fillStyle.materialParams);

        //     viewer.callUeFunc({
        //         f: 'update',
        //         p: {
        //             id: sceneObject.id,
        //             // pointed: sceneObject.pointed ?? ESGeoVector.defaults.pointed,
        //             // pointStyle,
        //             // stroked: sceneObject.stroked ?? ESGeoVector.defaults.stroked,
        //             // strokeStyle,
        //             // filled: sceneObject.filled ?? ESGeoVector.defaults.filled,
        //             // fillStyle,
        //             points: sceneObject.points ?? ESGeoVector.defaults.points,
        //             editing: sceneObject.editing,
        //         }
        //     })
        // };

        // const updateEvent = this.disposeVar(createNextAnimateFrameEvent(
        //     // sceneObject.pointedChanged,
        //     // sceneObject.pointStyleChanged,
        //     // sceneObject.strokedChanged,
        //     // sceneObject.strokeStyleChanged,
        //     // sceneObject.filledChanged,
        //     // sceneObject.fillStyleChanged,
        //     sceneObject.pointsChanged,
        //     sceneObject.editingChanged,
        // ));
        // this.dispose(updateEvent.disposableOn(update));
        // this.dispose(sceneObject.ueCreatedEvent.disposableOn(update));
        // this.dispose(sceneObject.flushEvent.disposableOn(() => updateEvent.flush()));
    }
}
