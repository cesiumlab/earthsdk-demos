export { UeESSceneObject } from 'xbsj-xe2/dist-node/xe2-ue-objects';

// import { ESSceneObject } from 'xbsj-xe2/dist-node/xe2-base-objects';
// import { Event, createNextAnimateFrameEvent, JsonValue } from 'xbsj-xe2/dist-node/xe2-base-utils';
// import { UeObject, UeViewer, destroyCallFunc, UeObjectPropValFuncType } from "xbsj-xe2/dist-node/xe2-ue-objects";

// export class UeESSceneObject<T extends ESSceneObject = ESSceneObject> extends UeObject<T> {
//     constructor(sceneObject: T, ueViewer: UeViewer) {
//         super(sceneObject, ueViewer);
//         const viewer = ueViewer.viewer;
//         if (!viewer) {
//             console.warn(`viewer is undefined!`);
//             return;
//         }

//         //////////////////////////////////////////////////////////////////////////
//         // ES对象属性自动更新机制
//         this.dispose(this.createdEvent.disposableOn(() => {
//             // @ts-ignore
//             const propValFuncs = this.constructor.propValFuncs as unknown as { [k: string]: UeObjectPropValFuncType | null | undefined };

//             const dp = sceneObject.defaultProps as { string: any };
//             const propNames = Object.keys(dp).filter(e => (propValFuncs[e] !== null));
//             // @ts-ignore
//             propNames.push(...this.constructor.forceUeUpdateProps);

//             // @ts-ignore
//             const undefinedDefaults = (sceneObject.constructor.defaults as { [k: string]: any });

//             const newProps = { val: {} as { [k: string]: any} };
//             const propChangeds: Event[] = [];

//             for (let propName of propNames) {
//                 const propChangedName = propName + 'Changed';
//                 // @ts-ignore
//                 const propChanged = sceneObject[propChangedName] as Event;
//                 propChangeds.push(propChanged);

//                 const vf = propValFuncs[propName];
//                 if (vf === null) {
//                     console.error(`vf === null error`);
//                     throw new Error(`vf === null error`);
//                 }

//                 const update = () => {
//                     // @ts-ignore
//                     const rawPv = sceneObject[propName];
//                     let propValue = (vf ? vf(rawPv, this, ueViewer, sceneObject) : rawPv) as JsonValue;
//                     if (propValue === undefined) {
//                         propValue = undefinedDefaults[propName];
//                     }
//                     if (propValue === undefined) {
//                         console.warn(`UE自动更新的属性，不应该为undefined！ 属性名：${propName} 对象名：${sceneObject.name} 对象id: ${sceneObject.id}`);
//                     }
//                     newProps.val[propName] = propValue;
//                 }
//                 update();
//                 this.dispose(propChanged.disposableOn(update));
//             }
    
//             const ueUpdate = () => {
//                 viewer.callUeFunc({
//                     f: 'update',
//                     p: {
//                         id: sceneObject.id,
//                         ...newProps.val,
//                     }
//                 });
//                 newProps.val = {};
//             };
    
//             // const updateEvent = this.disposeVar(new Event());
//             const updateEvent = this.disposeVar(createNextAnimateFrameEvent(...propChangeds));
//             this.dispose(updateEvent.disposableOn(ueUpdate));
//             this.dispose(sceneObject.ueCreatedEvent.disposableOn(ueUpdate));
//             this.dispose(sceneObject.flushEvent.disposableOn(() => updateEvent.flush()));
//         }));

//         // {
//         //     //////////////////////////////////////////////////////////////////////////
//         //     // ES对象属性自动更新机制

//         //     const dp = sceneObject.defaultProps as { string: any };
//         //     // @ts-ignore
//         //     const noNeedUeUpdateProps = (sceneObject.constructor.noNeedUeUpdateProps as string[]);
//         //     // @ts-ignore
//         //     const undefinedDefaults = (sceneObject.constructor.defaults as { [k: string]: any });
    
//         //     const propNames = Object.keys(dp).filter(e => !noNeedUeUpdateProps.includes(e));
//         //     // @ts-ignore
//         //     propNames.push(...sceneObject.constructor.forceUeUpdateProps);
    
//         //     // @ts-ignore
//         //     const propChangedNames = propNames.map(e => e + 'Changed');
//         //     // @ts-ignore
//         //     const propChangeds = propChangedNames.map(e => sceneObject[e] as Event);
    
//         //     const ueUpdate = () => {
//         //         const propEntries = propNames.map(e => {
//         //             // @ts-ignore
//         //             let propValue = sceneObject[e] as JsonValue;
//         //             if (propValue === undefined) {
//         //                 propValue = undefinedDefaults[e];
//         //             }
//         //             if (propValue === undefined) {
//         //                 console.warn(`UE自动更新的属性，不应该为undefined！ 属性名：${e} 对象名：${sceneObject.name} 对象id: ${sceneObject.id}`);
//         //             }
//         //             return [e, propValue];
//         //         });
//         //         const props = Object.fromEntries(propEntries);
//         //         // props类似以下形式
//         //         // const props = {
//         //         //     show: sceneObject.show,
//         //         //     collision: sceneObject.collision,
//         //         //     allowPicking: sceneObject.allowPicking,
//         //         // };
    
//         //         viewer.callUeFunc({
//         //             f: 'update',
//         //             p: {
//         //                 id: sceneObject.id,
//         //                 ...props,
//         //             }
//         //         });
//         //     };
    
//         //     // const updateEvent = this.disposeVar(new Event());
//         //     const updateEvent = this.disposeVar(createNextAnimateFrameEvent(...propChangeds));
//         //     this.dispose(updateEvent.disposableOn(ueUpdate));
//         //     this.dispose(sceneObject.ueCreatedEvent.disposableOn(ueUpdate));
//         //     this.dispose(sceneObject.flushEvent.disposableOn(() => updateEvent.flush()));
//         // }

//         {
//             /////////////////////////////////////////////////////////////////////
//             // ueCreatedEvent的实现
//             let created = false;
//             this.dispose(() => {
//                 if (created) destroyCallFunc(viewer, sceneObject.id)
//             });

//             viewer.callUeFunc({
//                 f: 'create',
//                 p: {
//                     type: sceneObject.typeName,
//                     id: sceneObject.id,
//                 }
//             }).then(() => {
//                 created = true;
//                 // update();
//                 sceneObject.ueCreatedEvent.emit();
//             }).catch(err => console.error(err));
//         }
//     }
// }
