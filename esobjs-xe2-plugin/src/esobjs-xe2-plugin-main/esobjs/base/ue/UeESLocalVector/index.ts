import { createNextAnimateFrameEvent } from 'xbsj-xe2/dist-node/xe2-base-utils';
import { ESFillStyle, ESLocalVector, ESPointStyle, ESStrokeStyle } from '../../objs';
import { UeViewer } from 'xbsj-xe2/dist-node/xe2-ue-objects';
import { UeESObjectWithLocation } from '../UeESObjectWithLocation';

export class UeESLocalVector<T extends ESLocalVector = ESLocalVector> extends UeESObjectWithLocation<T> {
    static override propValFuncs = {
        ...UeESObjectWithLocation.propValFuncs,
        pointStyle: (val: ESPointStyle) => ({
            ...val,
            materialParams: JSON.stringify(val.materialParams ?? {}),
        }),
        strokeStyle: (val: ESStrokeStyle) => ({
            ...val,
            materialParams: JSON.stringify(val.materialParams ?? {}),
        }),
        fillStyle: (val: ESFillStyle) => ({
            ...val,
            materialParams: JSON.stringify(val.materialParams ?? {}),
        }),
    };

    constructor(sceneObject: T, ueViewer: UeViewer) {
        super(sceneObject, ueViewer);

        const viewer = ueViewer.viewer;
        if (!viewer) {
            console.warn(`viewer is undefined!`);
            return;
        }

        // 暂时保留，以后需要删掉
        // const update = () => {
        //     let pointStyle = ESLocalVector.defaults.pointStyle
        //     let strokeStyle = ESLocalVector.defaults.strokeStyle
        //     let fillStyle = ESLocalVector.defaults.fillStyle
        //     try {
        //         pointStyle = { ...(sceneObject.pointStyle ?? ESLocalVector.defaults.pointStyle) };
        //         strokeStyle = { ...(sceneObject.strokeStyle ?? ESLocalVector.defaults.strokeStyle) };
        //         fillStyle = { ...(sceneObject.fillStyle ?? ESLocalVector.defaults.fillStyle) };
        //     } catch (e) {
        //         console.error('ESLocalVector 属性类型错误!', e)
        //         strokeStyle = { ...ESLocalVector.defaults.strokeStyle }
        //         pointStyle = { ...ESLocalVector.defaults.pointStyle }
        //         fillStyle = { ...ESLocalVector.defaults.fillStyle }
        //     }
        //     strokeStyle.materialParams = JSON.stringify(strokeStyle.materialParams);
        //     pointStyle.materialParams = JSON.stringify(pointStyle.materialParams);
        //     fillStyle.materialParams = JSON.stringify(fillStyle.materialParams);

        //     viewer.callUeFunc({
        //         f: 'update',
        //         p: {
        //             id: sceneObject.id,
        //             pointed: sceneObject.pointed,
        //             pointStyle,
        //             stroked: sceneObject.stroked,
        //             strokeStyle,
        //             filled: sceneObject.filled,
        //             fillStyle,
        //         }
        //     })
        // };
        // const updateEvent = this.disposeVar(createNextAnimateFrameEvent(
        //     sceneObject.pointedChanged,
        //     sceneObject.pointStyleChanged,
        //     sceneObject.strokedChanged,
        //     sceneObject.strokeStyleChanged,
        //     sceneObject.filledChanged,
        //     sceneObject.fillStyleChanged,
        // ));
        // this.dispose(updateEvent.disposableOn(update));
        // this.dispose(sceneObject.ueCreatedEvent.disposableOn(update));
        // this.dispose(sceneObject.flushEvent.disposableOn(() => updateEvent.flush()));
    }
}
