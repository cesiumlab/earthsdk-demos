import { CzmBoxClippingPlanes, CzmViewer, getCameraPosition } from "xbsj-xe2/dist-node/xe2-cesium-objects";
import { ESObjectWithLocation } from '../objs';
import { CzmESVisualObject } from './CzmESVisualObject';
import { CzmESPrsEditing } from "../../utils/CzmESPrsEditing";
import { SmoothMoveController } from "../misc";
import { getDistancesFromPositions } from "xbsj-xe2/dist-node/xe2-base-objects";
import { createNextAnimateFrameEvent } from "xbsj-renderer/dist-node/xr-base-utils";

export class CzmESObjectWithLocation<T extends ESObjectWithLocation = ESObjectWithLocation> extends CzmESVisualObject<T> {
    private _sPrsEditing = this.disposeVar(new CzmESPrsEditing(this.czmViewer, [this.sceneObject, 'editing'], [this.sceneObject, 'position'], [this.sceneObject, 'rotation'], {
        rotation: { showHelper: false, }
    }));
    private _sPrsEditingInit = (() => this._sPrsEditing.enabled = true)();
    get sPrsEditing() { return this._sPrsEditing; }

    private _smoothMoving = this.disposeVar(new SmoothMoveController(this.czmViewer));
    private _positionBinding = this.d(this._smoothMoving.currentPositionChanged.don((p) => {
        p && (this.sceneObject.position = p);
    }));
    /**
      * 平滑移动+姿态，姿态插值转向
      */
    private _rotationBinding = this.d(this._smoothMoving.currentRotationChanged.don((r) => {
        r && this._smoothMoving.isRotating && (this.sceneObject.rotation = r);
    }));

    /**
     * 平滑移动时，姿态朝向终点
     */
    private _headingBinding = this.d(this._smoothMoving.currentHeadingChanged.don((h) => {
        if ((h !== undefined) && (!this._smoothMoving.isRotating)) {
            const r = this.sceneObject.rotation;
            this.sceneObject.rotation = [h - 90, r[1], r[2]];
        }
    }));

    smoothMove(P: [number, number, number], T: number) {
        this._smoothMoving.isGround = false;
        this._smoothMoving.isRotating = false;
        this._smoothMoving.restart(this.sceneObject.position, this.sceneObject.rotation, 0);
        this._smoothMoving.restart(P, this.sceneObject.rotation, T);
    }

    smoothMoveWithRotation(P: [number, number, number], R: [number, number, number], T: number) {
        this._smoothMoving.isGround = false;
        this._smoothMoving.isRotating = true;
        this._smoothMoving.restart(this.sceneObject.position, this.sceneObject.rotation, 0);
        this._smoothMoving.restart(P, R, T);
    }

    smoothMoveOnGround(Lon: number, Lat: number, T: number) {
        const height = this.czmViewer.getTerrainHeight([Lon, Lat]) ?? 0
        this._smoothMoving.isGround = true;
        this._smoothMoving.isRotating = false;
        this._smoothMoving.restart(this.sceneObject.position, this.sceneObject.rotation, 0);
        this._smoothMoving.restart([Lon, Lat, height], this.sceneObject.rotation, T);
    }

    smoothMoveWithRotationOnGround(R: [number, number, number], Lon: number, Lat: number, T: number) {
        const height = this.czmViewer.getTerrainHeight([Lon, Lat]) ?? 0
        this._smoothMoving.isGround = true;
        this._smoothMoving.isRotating = true;
        this._smoothMoving.restart(this.sceneObject.position, this.sceneObject.rotation, 0);
        this._smoothMoving.restart([Lon, Lat, height], R, T);
    }

    constructor(sceneObject: T, czmViewer: CzmViewer) {
        super(sceneObject, czmViewer);
        const viewer = czmViewer.viewer;
        if (!viewer) {
            console.warn(`viewer is undefined!`);
            return;
        }

        this.d(sceneObject.smoothMoveEvent.don((Destination, Time) => {
            this.smoothMove(Destination, Time * 1000);
        }));

        this.d(sceneObject.smoothMoveWithRotationEvent.don((Destination, Rotation, Time) => {
            this.smoothMoveWithRotation(Destination, Rotation, Time * 1000);
        }));

        this.d(sceneObject.smoothMoveOnGroundEvent.don((Lon, Lat, Time) => {
            this.smoothMoveOnGround(Lon, Lat, Time * 1000);
        }));

        this.d(sceneObject.smoothMoveWithRotationOnGroundEvent.don((Rotation, Lon, Lat, Time) => {
            this.smoothMoveWithRotationOnGround(Rotation, Lon, Lat, Time * 1000);
        }));

        this.d(sceneObject.calcFlyToParamEvent.don(() => {
            if (!sceneObject.useCalcFlyToParamInESObjectWithLocation) {
                return;
            }
            if (!sceneObject.position) {
                console.warn(`!sceneObject(${sceneObject.name}-${sceneObject.id}).position`);
                return;
            }
            const flyToParam = czmViewer.calcFlyToParam(sceneObject.position);
            if (!flyToParam) {
                console.warn(`czmViewer.calcFlyToParam error.`);
                return;
            }
            sceneObject.flyToParam = flyToParam;
        }));
        //自动落地
        this.d(sceneObject.automaticLandingEvent.don((flag) => {
            const posi = [sceneObject.position[0], sceneObject.position[1]] as [number, number]
            const pos = czmViewer.getTerrainHeight(posi)
            if (pos) {
                sceneObject.position = [...posi, pos]
            } else {
                console.warn('不存在相交')
            }
            sceneObject.collision = flag
        }));
        {
            //可视距离
            const event = this.dv(createNextAnimateFrameEvent(
                sceneObject.minVisibleDistanceChanged,
                sceneObject.maxVisibleDistanceChanged,
            ))
            this.d(event.don(() => this.visibleDistance(sceneObject, czmViewer)));
            this.d(czmViewer.viewer.camera.changed.addEventListener(() => this.visibleDistance(sceneObject, czmViewer)))
        }
    }

    visibleDistance(sceneObject: T, czmViewer: CzmViewer) {
        const czmSceneObject = CzmESObjectWithLocation.getCzmObject(this);
        if (czmViewer.viewer?.camera && sceneObject.show && czmSceneObject.length != 0) {
            const dis = getDistancesFromPositions([sceneObject.position, getCameraPosition(czmViewer.viewer.camera)], 'NONE')[0];
            let show = false;
            if (sceneObject.minVisibleDistance < sceneObject.maxVisibleDistance) {
                show = sceneObject.minVisibleDistance < dis && dis < sceneObject.maxVisibleDistance;
            } else if (sceneObject.maxVisibleDistance == 0) {
                show = dis > sceneObject.minVisibleDistance;
            }
            //@ts-ignore
            czmSceneObject.forEach(item => {
                const tempShow = sceneObject.show && show;
                if (Reflect.has(item, 'showHelper')) {
                    item.showHelper = tempShow;
                }
                if (Reflect.has(item, 'enabled')) {
                    item.enabled = tempShow;
                }
                if (Reflect.has(item, 'show')) {
                    item.show = tempShow;
                }
            })
        }
    }
    static getCzmObject(czmObject: any): any[] {
        const czmSceneObject = [];
        for (const key in czmObject) {
            if (Object.prototype.hasOwnProperty.call(czmObject, key)) {
                if (!key.includes("czmViewer") && key.includes("czm") || !Number.isNaN(+key)) {
                    if (Array.isArray(czmObject[key])) {
                        czmSceneObject.push(...CzmESObjectWithLocation.getCzmObject(czmObject[key]))
                    } else {
                        czmSceneObject.push(czmObject[key]);
                    }
                } else if (czmObject[key] && czmObject[key].obj) {
                    czmSceneObject.push(...CzmESObjectWithLocation.getCzmObject(czmObject[key].obj))
                }
            }
        }
        return czmSceneObject;
    }

    override flyTo(duration: number | undefined, id: number) {
        const { sceneObject, czmViewer } = this;
        if (!czmViewer.actived) return false;
        // 先检查flyToParam能否使用
        if (sceneObject.flyToParam && sceneObject.position) {
            const { position, flyToParam } = sceneObject;
            const { distance, heading, pitch, flyDuration, hDelta, pDelta } = flyToParam;
            this.flyToWithPromise(id, position, distance, [heading, pitch, 0], duration ?? flyDuration, hDelta, pDelta);
            return true;
        }
        return super.flyTo(duration, id);
    }
    // flyIn飞行只有两种情况，一种有flyInParam,一种没有，CzmESObjectWithLocation派生出的类较简单，
    // 无flyInParam直接通过position和rotation即可飞行，无需在派生类重写flyIn
    override flyIn(duration: number | undefined, id: number) {
        const { sceneObject, czmViewer } = this;
        if (!czmViewer.actived) return false;
        if (!sceneObject.flyInParam && sceneObject.position && sceneObject.rotation) {
            const d = duration ?? 1
            const newRotation: [number, number, number] = [...sceneObject.rotation];
            newRotation[0] += 90;
            this.flyToWithPromise(id, sceneObject.position, undefined, newRotation, d)
            return true;
        }
        return super.flyIn(duration, id);
    }
}
