import { CzmObject, CzmViewer } from "xbsj-xe2/dist-node/xe2-cesium-objects";
import { ESVisualObject } from '../objs';

export class CzmESVisualObject<T extends ESVisualObject = ESVisualObject> extends CzmObject<T> {
    constructor(sceneObject: T, czmViewer: CzmViewer) {
        super(sceneObject, czmViewer);
        const viewer = czmViewer.viewer;
        if (!viewer) {
            console.warn(`viewer is undefined!`);
            return;
        }
        this.dispose(sceneObject.flyToEvent.disposableOn((duration, id) => this.flyTo(duration, id)));
        this.dispose(sceneObject.flyInEvent.disposableOn((duration, id) => this.flyIn(duration, id)));
        this.dispose(sceneObject.calcFlyInParamEvent.disposableOn(() => {
            if (!czmViewer.actived) return;
            const cameraInfo = czmViewer.getCameraInfo();
            if (!cameraInfo) return;
            const { position, rotation } = cameraInfo;
            sceneObject.flyInParam = { position, rotation, flyDuration: 1 };
        }));

        this.dispose(sceneObject.calcFlyToParamEvent.disposableOn(() => {
            if (!sceneObject.useCalcFlyToParamInESObjectWithLocation) {
                console.warn('calcFlyToParam无法调用,该对象缺少position属性!');
            }
        }));

        // this.dispose(sceneObject.calcFlyToParamEvent.disposableOn(() => {
        //     if (!czmViewer.actived) return;
        //     if (Reflect.has(sceneObject, 'position')) {
        //         //@ts-ignore
        //         if (!sceneObject.position) {
        //             console.warn(`!sceneObject(${sceneObject.name}-${sceneObject.id}).position`);
        //             return;
        //         }
        //         //@ts-ignore
        //         const flyToParam = czmViewer.calcFlyToParam(sceneObject.position);
        //         if (!flyToParam) {
        //             console.warn(`czmViewer.calcFlyToParam error.`);
        //             return;
        //         }
        //         //@ts-ignore
        //         sceneObject.flyToParam = { ...flyToParam, position: sceneObject.position };
        //     } else {
        //         const position = [116.397478, 39.908735, 60] as [number, number, number];
        //         const flyToParam = czmViewer.calcFlyToParam(position);
        //         if (!flyToParam) {
        //             console.warn(`czmViewer.calcFlyToParam error.`);
        //             return;
        //         }
        //         sceneObject.flyToParam = flyToParam;
        //     }
        // }));

    }

    /**
     * 
     * @param id 
     * @param position 
     * @param viewDistance 
     * @param rotation 
     * @param duration 注意单位是秒
     * @param hdelta 
     * @param pdelta 
     */
    protected flyToWithPromise(
        id: number,
        position: [number, number, number],
        viewDistance?: number | undefined,
        rotation?: [number, number, number] | undefined,
        duration?: number | undefined,
        hdelta?: number | undefined,
        pdelta?: number | undefined) {

        const { sceneObject, czmViewer } = this;
        const p = czmViewer.flyTo(position, viewDistance, rotation, duration && duration * 1000, hdelta, pdelta);
        if (!p) {
            sceneObject.flyOverEvent.emit(id, 'error', czmViewer);
        } else {
            p.then(r => {
                sceneObject.flyOverEvent.emit(id, r ? 'over' : 'cancelled', czmViewer);
            });
        }
    }

    /**
     * 
     * @param duration 注意ES对象的时间单位都是秒
     * @param id 
     * @returns 
     */
    flyTo(duration: number | undefined, id: number) {
        const { sceneObject, czmViewer } = this;
        if (!czmViewer.actived) return false;

        // // 先检查flyToParam能否使用
        // if (sceneObject.flyToParam) {
        //     let position = [116.397478, 39.908735, 60] as [number, number, number];
        //     if (Reflect.has(sceneObject, 'position')) {
        //         //@ts-ignore
        //         position = sceneObject.position;
        //     }
        //     if (!position) return false;
        //     const { flyToParam } = sceneObject;
        //     const { distance, heading, pitch, flyDuration, hDelta, pDelta } = flyToParam;
        //     this.flyToWithPromise(id, position, distance, [heading, pitch, 0], duration ?? flyDuration, hDelta, pDelta);
        //     return true;
        // }

        //没有position属性flyToParam存在报错！！
        if (sceneObject.flyToParam) {
            // if (!(Reflect.has(sceneObject, 'position'))) {
            if (!sceneObject.useCalcFlyToParamInESObjectWithLocation) {
                console.warn('flyToParam无法使用,该对象缺少position属性,若飞行失败,请清空flyToParam后重试!');
            }
        }

        // 再检查flyInParam能否使用
        if (sceneObject.flyInParam) {
            const { position, rotation, flyDuration } = sceneObject.flyInParam;
            const d = duration ?? flyDuration;
            this.flyToWithPromise(id, position, undefined, rotation, d);
            return true;
        }

        return false;
    }
    /**
     * 
     * @param duration 注意ES对象的时间单位都是秒
     * @param id 
     * @returns 
     */
    flyIn(duration: number | undefined, id: number) {
        const { sceneObject, czmViewer } = this;
        if (!czmViewer.actived) return false;
        // 检查flyInParam能否使用
        if (sceneObject.flyInParam) {
            const { position, rotation, flyDuration } = sceneObject.flyInParam;
            const d = duration ?? flyDuration;
            this.flyToWithPromise(id, position, undefined, rotation, d);
            return true;
        }
        return false;
    }
}
