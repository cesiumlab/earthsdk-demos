import { CzmViewer } from "xbsj-xe2/dist-node/xe2-cesium-objects";
import { ESLabel, ESLocalVector } from "../objs";
import { CzmESObjectWithLocation } from "./CzmESObjectWithLocation";

export class CzmESLocalVector<T extends ESLocalVector = ESLocalVector> extends CzmESObjectWithLocation<T>{
    constructor(sceneObject: T, czmViewer: CzmViewer) {
        super(sceneObject, czmViewer);
        const viewer = czmViewer.viewer;
        if (!viewer) {
            console.warn('viewer is undefined!');
            return;
        }
    }
}