import { CzmViewer } from "xbsj-xe2/dist-node/xe2-cesium-objects";
import { ESLabel } from "../objs";
import { CzmESObjectWithLocation } from "./CzmESObjectWithLocation";

export class CzmESLabel<T extends ESLabel = ESLabel> extends CzmESObjectWithLocation<T>{
    constructor(sceneObject: T, czmViewer: CzmViewer) {
        super(sceneObject, czmViewer);
        const viewer = czmViewer.viewer;
        if (!viewer) {
            console.warn('viewer is undefined!');
            return;
        }
    }
}