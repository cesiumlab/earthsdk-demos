import { CzmViewer } from "xbsj-xe2/dist-node/xe2-cesium-objects";
import { ESLabel, ESLocalVector, ESLocalVector2D } from "../objs";
import { CzmESObjectWithLocation } from "./CzmESObjectWithLocation";
import { CzmESLocalVector } from "./CzmESLocalVector";

export class CzmESLocalVector2D<T extends ESLocalVector2D = ESLocalVector2D> extends CzmESLocalVector<T>{
    constructor(sceneObject: T, czmViewer: CzmViewer) {
        super(sceneObject, czmViewer);
        const viewer = czmViewer.viewer;
        if (!viewer) {
            console.warn('viewer is undefined!');
            return;
        }
    }
}