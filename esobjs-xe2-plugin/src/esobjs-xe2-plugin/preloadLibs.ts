import { getCurrentScriptPath } from "./getCurrentScriptPath";

function preloadLibs() {
    const currentScriptPath = getCurrentScriptPath();
    const scriptName = 'esobjs-xe2-plugin.js';
    const xe2RootPath = currentScriptPath.slice(0, -scriptName.length);

    // 发布时使用
    document.write(`\
        <script src="${xe2RootPath}esobjs-xe2-plugin-main.js"></script>
    `);
}

preloadLibs();