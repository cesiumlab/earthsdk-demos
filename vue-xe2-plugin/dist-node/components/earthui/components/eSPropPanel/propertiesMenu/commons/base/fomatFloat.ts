export function fomatFloat(src: number, pos: number) {
    return Math.round(src * Math.pow(10, pos)) / Math.pow(10, pos);
}