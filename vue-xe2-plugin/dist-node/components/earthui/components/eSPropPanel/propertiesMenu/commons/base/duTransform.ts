//度转度分秒
export function doToDoFenMiao(du: number): [number, number, number] {
    du = Math.abs(du);
    let v1 = Math.floor(du);//度
    let v2 = Math.floor((du - v1) * 60);//分
    let v3 = Math.round((du - v1) * 3600 % 60);//秒
    return [v1, v2, v3];
}

//度分秒转度
export function duFenMiaoToDu(dufenMiao: [number, number, number]) {
    var du = dufenMiao[0];
    var fen = dufenMiao[1];
    var miao = dufenMiao[2];
    return Math.abs(du) + (Math.abs(fen) / 60 + Math.abs(miao) / 3600);
}
//度转度分
export function duToDoFen(degree: number): [number, number] {
    let absoluteDegree = Math.abs(degree);
    let intDegree = Math.floor(absoluteDegree);
    let minute = (absoluteDegree - intDegree) * 60;
    return [intDegree, Number(minute.toFixed(3))]
}
//度分转度
export function duFenToDo(duFen: [number, number]) {
    let degree = duFen[0]
    let minute = duFen[1]
    let result = degree + minute / 60;
    return result;
}