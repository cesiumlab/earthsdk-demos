import { ESObjectsManager } from 'esobjs-xe2-plugin/dist-node/esobjs-xe2-plugin-main';
import { parse } from 'search-params';
import { PositionsEditing, SceneObject } from 'xbsj-xe2/dist-node/xe2-base-objects';
import { react } from "xbsj-xe2/dist-node/xe2-base-utils";
import { CzmGlobeMaterial } from "xbsj-xe2/dist-node/xe2-cesium-objects";
import { ViewsManager } from "./CameraViewsManager";
import { ClassicNavigatorManager } from "./ClassicNavigator";
import { MeasurementManager } from "./MeasurementManager";
import { Reprocess } from "./Reprocess";

export class XbsjEarthUi extends ESObjectsManager {
    private _showSceneTreeView = this.dv(react<boolean>(true));//图层管理
    get showSceneTreeView() { return this._showSceneTreeView.value; }
    get showSceneTreeViewChanged() { return this._showSceneTreeView.changed; }
    set showSceneTreeView(value: boolean) { this._showSceneTreeView.value = value; }

    private _viewsManager = this.dv(new ViewsManager(this));//视角
    get viewsManager() { return this._viewsManager; }

    private _reprocess = this.dv(new Reprocess(this));//后处理
    get reprocess() { return this._reprocess; }

    private _navigatorManager = this.dv(new ClassicNavigatorManager(this));//导航条
    get navigatorManager() { return this._navigatorManager; }

    private _globeMaterial = this.createSceneObject(CzmGlobeMaterial);//全球材质，地形着色
    get globeMaterial() { return this._globeMaterial; }

    private _measurementManager = this.dv(new MeasurementManager(this));//测量
    get measurementManager() { return this._measurementManager; }

    private _propSceneTree = this.dv(react<any>(undefined));//属性对象
    get propSceneTree() { return this._propSceneTree.value; }
    get propSceneTreeChanged() { return this._propSceneTree.changed; }
    set propSceneTree(value: any) { this._propSceneTree.value = value; }

    private _rightModuleShow = this.dv(react<boolean>(true));//属性对象
    get rightModuleShow() { return this._rightModuleShow.value; }
    get rightModuleShowChanged() { return this._rightModuleShow.changed; }
    set rightModuleShow(value: boolean) { this._rightModuleShow.value = value; }

    private _cesiumLabUrl = this.dv(react<string>(''));//cesium地址
    get cesiumLabUrl() { return this._cesiumLabUrl.value; }
    get cesiumLabUrlChanged() { return this._cesiumLabUrl.changed; }
    set cesiumLabUrl(value: string) { this._cesiumLabUrl.value = value; }

    private _esssUrl = this.dv(react<string>(''));//esss地址
    get esssUrl() { return this._esssUrl.value; }
    get esssUrlChanged() { return this._esssUrl.changed; }
    set esssUrl(value: string) { this._esssUrl.value = value; }

    get czmlabPath() { return SceneObject.context.getStrFromEnv('${czmlab-path}'); }//环境变量
    set czmlabPath(target: string) { SceneObject.context.setEnv('czmlab-path', target); }

    private _ueIsShow = this.dv(react<boolean>(false));//视口
    get ueIsShow() { return this._ueIsShow.value; }
    get ueIsShowChanged() { return this._ueIsShow.changed; }
    set ueIsShow(value: boolean) { this._ueIsShow.value = value; }

    private _animationShow = this.dv(react<boolean>(false));//动画时间线
    get animationShow() { return this._animationShow.value; }
    get animationShowChanged() { return this._animationShow.changed; }
    set animationShow(value: boolean) { this._animationShow.value = value; }

    private _sceneTreeCheckedIcon = this.dv(react<boolean>(false));//场景树的全选按钮的控制
    get sceneTreeCheckedIcon() { return this._sceneTreeCheckedIcon.value; }
    get sceneTreeCheckedIconChanged() { return this._sceneTreeCheckedIcon.changed; }
    set sceneTreeCheckedIcon(value: boolean) { this._sceneTreeCheckedIcon.value = value; }

    private _roamMode = this.dv(react<string>('Map'));//漫游模式'Walk'|'Map'|'Line'|'RotateGlobe'
    get roamMode() { return this._roamMode.value; }
    get roamModeChanged() { return this._roamMode.changed; }
    set roamMode(value: string) { this._roamMode.value = value; }

    private _cesiumPick = this.dv(react<any>({}));//cesium拾取
    get cesiumPick() { return this._cesiumPick.value; }
    get cesiumPickChanged() { return this._cesiumPick.changed; }
    set cesiumPick(value: any) { this._cesiumPick.value = value; }

    private _uePick = this.dv(react<any>(''));//ue拾取
    get uePick() { return this._uePick.value; }
    get uePickChanged() { return this._uePick.changed; }
    set uePick(value: any) { this._uePick.value = value; }

    private _Interpolation = this.dv(react<number>(500));//插值数据
    get Interpolation() { return this._Interpolation.value; }
    get InterpolationChanged() { return this._Interpolation.changed; }
    set Interpolation(value: number) { this._Interpolation.value = value; }

    private _cesiumLabToken = this.dv(react<string>(''));//cesiumlabToken
    get cesiumLabToken() { return this._cesiumLabToken.value; }
    get cesiumLabTokenChanged() { return this._cesiumLabToken.changed; }
    set cesiumLabToken(value: string) { this._cesiumLabToken.value = value; }

    constructor() {
        super();
        {//初始化地址
            const search = window.location.search.substring(1)
            const parseSearch = parse(search)
            if (parseSearch.from === 'cesiumLab') {
                this.cesiumLabUrl = window.location.origin
            } else if (parseSearch.from === 'esss') {
                this.esssUrl = window.location.origin
                localStorage.setItem('esssUrl', this.esssUrl)
            } else {
                this.cesiumLabUrl = ''
                this.esssUrl = ''
            }
            
        }
        {//禁止拾取
            //@ts-ignore
            this._projectManager.editingOnPicking = false
        }
        {//设置此项目的编辑配置
            PositionsEditing.defaultConfig = { // 多点编辑
                "editor": { // 编辑状态的配置
                    "showCoordinates": true, // 是否显示坐标架
                    "showCircle": true, // 是否显示辅助圈
                    "disableX": false, // 是否取消X轴向的平移
                    "disableY": false, // 是否取消Y轴向的平移
                    "disableXY": false, // 是否取消XY平面上的平移操作
                    "disableZ": false, // 是否取消Z轴上的平移操作
                    "disableZAxis": false // 是否取消Z轴上的旋转操作，注意是旋转操作
                },
                "picker": { // 拾取状态的配置
                    "clickEnabled": true, // 单击拾取点
                    "dbClickEnabled": true // 双击拾取点
                },
                "noModifingAfterAdding": false, // 新增结束以后不进入修改状态
                "hideCursorInfo": true, // 关闭鼠标提示
            }
        }
        {//判断视口
            this.d(this.activeViewerChanged.don(() => {
                if (!this.activeViewer) return
                if (this.activeViewer.typeName === 'ESUeViewer') {
                    this.ueIsShow = true
                } else {
                    this.ueIsShow = false
                }
            }))
        }
    }
}
