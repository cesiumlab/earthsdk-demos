import { Ref } from "vue";
import { Destroyable } from "xbsj-xe2/dist-node/xe2-base-utils";
import { XbsjEarthUi } from "../../../../scripts/xbsjEarthUi";
import { ESPath } from "esobjs-xe2-plugin/dist-node/esobjs-xe2-plugin-main";
export abstract class Dragger extends Destroyable {
    protected _startX = this._event.offsetX
    protected _target = this._event.target as HTMLElement
    protected _pointerId = this._event.pointerId
    constructor(protected _event: PointerEvent, protected _xScale: Ref<number>) {
        super();
        this._target.setPointerCapture(this._pointerId)
        this.dispose(() => this._target.releasePointerCapture(this._pointerId))
    }
    abstract update(event: PointerEvent): void
}


export class StartTimeLineDragger extends Dragger {
    private _start = this._startTimeRef.value
    private _stop = this._stopTimeRef.value
    private _total = this._stopTimeRef.value - this._startTimeRef.value
    constructor(event: PointerEvent, xScale: Ref<number>, private width: number, private _startTimeRef: Ref<number>, private _stopTimeRef: Ref<number>) {
        super(event, xScale)
    }
    update(event: PointerEvent) {
        const diff = (event.offsetX - this._startX)//移动的像素
        // console.log('_start',this._start);
        // console.log('_stop',this._stop);
        // console.log('_total',this._total);
        
        // const one = this._total / this.width
        // const totalPixl = one * (diff)
        // this._startTimeRef.value=this._start + totalPixl
        // console.log(this._startTimeRef.value);

    }
}

