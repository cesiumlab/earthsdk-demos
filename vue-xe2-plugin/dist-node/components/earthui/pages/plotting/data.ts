
import { defineAsyncComponent,shallowRef } from 'vue';
export const textTypeList: { name: string, text: string, type: { textSize: number, textColor: [number, number, number, number] } }[] = [
    {
        name: '白小文本',
        text: '请输入文本',
        type: {
            textSize: 14,
            textColor: [1, 1, 1, 1]
        }
    },
    {
        name: '白中文本',
        text: '请输入文本',
        type: {
            textSize: 18,
            textColor: [1, 1, 1, 1]
        }
    },
    {
        name: '白大文本',
        text: '请输入文本',
        type: {
            textSize: 22,
            textColor: [1, 1, 1, 1]
        }
    },
    {
        name: '红小文本',
        text: '请输入文本',
        type: {
            textSize: 14,
            textColor: [1, 0, 0, 1]
        }
    },
    {
        name: '红中文本',
        text: '请输入文本',
        type: {
            textSize: 18,
            textColor: [1, 0, 0, 1]
        }
    },
    {
        name: '红大文本',
        text: '请输入文本',
        type: {
            textSize: 22,
            textColor: [1, 0, 0, 1]
        }
    },
    {
        name: '绿小文本',
        text: '请输入文本',
        type: {
            textSize: 14,
            textColor: [0, 1, 0, 1]
        }
    },
    {
        name: '绿中文本',
        text: '请输入文本',
        type: {
            textSize: 18,
            textColor: [0, 1, 0, 1]
        }
    },
    {
        name: '绿大文本',
        text: '请输入文本',
        type: {
            textSize: 22,
            textColor: [0, 1, 0, 1]
        }
    },
    {
        name: '蓝小文本',
        text: '请输入文本',
        type: {
            textSize: 14,
            textColor: [0, 0, 1, 1]
        }
    },
    {
        name: '蓝中文本',
        text: '请输入文本',
        type: {
            textSize: 18,
            textColor: [0, 0, 1, 1]
        }
    },
    {
        name: '蓝大文本',
        text: '请输入文本',
        type: {
            textSize: 22,
            textColor: [0, 0, 1, 1]
        }
    }
]
//图标点
export const imageTypeList: { name: string, img: string, type: string }[] = [
    {
        name: '蓝色相机',
        img: require('../../assets/plotting/points/CameraBlue.png'),
        type: ('inner://CameraBlue.png')
    }, {
        name: '绿色相机',
        img: require('../../assets/plotting/points/CameraGreen.png'),
        type: ('inner://CameraGreen.png')
    }, {
        name: '蓝色车辆',
        img: require('../../assets/plotting/points/CarBlue.png'),
        type: ('inner://CarBlue.png')
    }, {
        name: '红色车辆',
        img: require('../../assets/plotting/points/CarRed.png'),
        type: ('inner://CarRed.png')
    }, {
        name: '渡口',
        img: require('../../assets/plotting/points/Dukou.png'),
        type: ('inner://Dukou.png')
    }, {
        name: '界限',
        img: require('../../assets/plotting/points/Jiexian.png'),
        type: ('inner://Jiexian.png')
    }, {
        name: '警示',
        img: require('../../assets/plotting/points/Jingshi.png'),
        type: ('inner://Jingshi.png')
    }, {
        name: '蓝色料斗',
        img: require('../../assets/plotting/points/LiaodouBlue.png'),
        type: ('inner://LiaodouBlue.png')
    }, {
        name: '红色料斗',
        img: require('../../assets/plotting/points/LiaodouRed.png'),
        type: ('inner://LiaodouRed.png')
    }, {
        name: '蓝色龙门吊',
        img: require('../../assets/plotting/points/LongmendiaoBlue.png'),
        type: ('inner://LongmendiaoBlue.png')
    }, {
        name: '红色龙门吊',
        img: require('../../assets/plotting/points/LongmendiaoRed.png'),
        type: ('inner://LongmendiaoRed.png')
    }, {
        name: '锚',
        img: require('../../assets/plotting/points/Mao.png'),
        type: ('inner://Mao.png')
    }, {
        name: '蓝色门机',
        img: require('../../assets/plotting/points/MenjiBlue.png'),
        type: ('inner://MenjiBlue.png')
    }, {
        name: '红色门机',
        img: require('../../assets/plotting/points/MenjiRed.png'),
        type: ('inner://MenjiRed.png')
    }, {
        name: '鸣笛',
        img: require('../../assets/plotting/points/Mingdi.png'),
        type: ('inner://Mingdi.png')
    }, {
        name: '气象站',
        img: require('../../assets/plotting/points/Qixiang.png'),
        type: ('inner://Qixiang.png')
    }, {
        name: '蓝色人员',
        img: require('../../assets/plotting/points/RenyuanBlue.png'),
        type: ('inner://RenyuanBlue.png')
    }, {
        name: '红色人员',
        img: require('../../assets/plotting/points/RenyuanRed.png'),
        type: ('inner://RenyuanRed.png')
    }, {
        name: '圆形蓝色人员',
        img: require('../../assets/plotting/points/RenyuanCircleBlue.png'),
        type: ('inner://RenyuanCircleBlue.png')
    }, {
        name: '圆形红色人员',
        img: require('../../assets/plotting/points/RenyuanCircleRed.png'),
        type: ('inner://RenyuanCircleRed.png')
    }
]
//poi2d
export const poi2dList: { name: string, img: string, mode: string }[] = [//多选模式类型
    {
        mode: 'SquareH01',
        img: require('../../assets/plotting/poi2d/mode1.png'),
        name: '模式1'
    },
    {
        mode: 'SquareH02',
        img: require('../../assets/plotting/poi2d/mode2.png'),
        name: '模式2'
    }, {
        mode: 'SquareV01',
        img: require('../../assets/plotting/poi2d/mode3.png'),
        name: '模式3'
    }, {
        mode: 'SquareV02',
        img: require('../../assets/plotting/poi2d/mode4.png'),
        name: '模式4'
    }, {
        mode: 'SquareV03',
        img: require('../../assets/plotting/poi2d/mode5.png'),
        name: '模式5'
    }, {
        mode: 'SquareV04',
        img: require('../../assets/plotting/poi2d/mode6.png'),
        name: '模式6'
    }, {
        mode: 'Flag01',
        img: require('../../assets/plotting/poi2d/mode7.png'),
        name: '模式7'
    }, {
        mode: 'Flag02',
        img: require('../../assets/plotting/poi2d/mode8.png'),
        name: '模式8'
    }, {
        mode: 'Linear01',
        img: require('../../assets/plotting/poi2d/mode9.png'),
        name: '模式9'
    }, {
        mode: 'Linear02',
        img: require('../../assets/plotting/poi2d/mode10.png'),
        name: '模式10'
    }, {
        mode: 'Linear03',
        img: require('../../assets/plotting/poi2d/mode11.png'),
        name: '模式11'
    }, {
        mode: 'CircularH01',
        img: require('../../assets/plotting/poi2d/mode12.png'),
        name: '模式12'
    }, {
        mode: 'CircularH02',
        img: require('../../assets/plotting/poi2d/mode13.png'),
        name: '模式13'
    }, {
        mode: 'CircularV01',
        img: require('../../assets/plotting/poi2d/mode14.png'),
        name: '模式14'
    }, {
        mode: 'CircularV02',
        img: require('../../assets/plotting/poi2d/mode15.png'),
        name: '模式15'
    }, {
        mode: 'CircularV03',
        img: require('../../assets/plotting/poi2d/mode16.png'),
        name: '模式16'
    }, {
        mode: 'CircularV04',
        img: require('../../assets/plotting/poi2d/mode17.png'),
        name: '模式17'
    }, {
        mode: 'CircularV05',
        img: require('../../assets/plotting/poi2d/mode18.png'),
        name: '模式18'
    }, {
        mode: 'P3D01',
        img: require('../../assets/plotting/poi2d/mode19.png'),
        name: '模式19'
    }, {
        mode: 'P3D02',
        img: require('../../assets/plotting/poi2d/mode20.png'),
        name: '模式20'
    }, {
        mode: 'P3D03',
        img: require('../../assets/plotting/poi2d/mode21.png'),
        name: '模式21'
    }, {
        mode: 'P3D04',
        img: require('../../assets/plotting/poi2d/mode22.png'),
        name: '模式22'
    }, {
        mode: 'P3D05',
        img: require('../../assets/plotting/poi2d/mode23.png'),
        name: '模式23'
    }, {
        mode: 'P3D06',
        img: require('../../assets/plotting/poi2d/mode24.png'),
        name: '模式24'
    }, {
        mode: 'P3D07',
        img: require('../../assets/plotting/poi2d/mode25.png'),
        name: '模式25'
    }, {
        mode: 'Diamond01',
        img: require('../../assets/plotting/poi2d/mode26.png'),
        name: '模式26'
    }, {
        mode: 'Diamond02',
        img: require('../../assets/plotting/poi2d/mode27.png'),
        name: '模式27'
    },
]

//矢量
export const vectorObjectList: { type: string, zh: string, icon: string, leftButton: boolean, com: any }[] = [
    {
        type: 'esGeoLineString',
        zh: '折线',
        icon: 'zhexian',
        leftButton: true,
        com: shallowRef(defineAsyncComponent(() => { return import('./esObj/EsGeoLineString.vue') }))
    },
    {
        type: 'esGeoPolygon',
        zh: '多边形',
        icon: 'duobianxing',
        leftButton: false,
        com: shallowRef(defineAsyncComponent(() => { return import('./esObj/EsGeoPolygon.vue') }))
    }, {
        type: 'esLocalCircle',
        zh: '圆',
        icon: 'yuan',
        leftButton: true,
        com: shallowRef(defineAsyncComponent(() => { return import('./esObj/EsLocalCircle.vue') }))
    },
    {
        type: 'esGeoRectangle',
        zh: '矩形',
        icon: 'juxing',
        leftButton: false,
        com: shallowRef(defineAsyncComponent(() => { return import('./esObj/EsGeoRectangle.vue') }))
    }
]
//注记
export const annotationObjectList: { type: string, zh: string, icon: string, leftButton: boolean, com: any }[] = [
    {
        type: 'esTextLabel',
        zh: '文字标注',
        icon: 'wenzibiaozhu',
        leftButton: true,
        com: shallowRef(defineAsyncComponent(() => { return import('./esObj/EsTextLabel.vue') }))

    },
    {
        type: 'esImageLabel',
        zh: '图标点',
        icon: 'tubiaodian',
        leftButton: false,
        com: shallowRef(defineAsyncComponent(() => { return import('./esObj/EsImageLabel.vue') }))
    },
    {
        type: 'esWidget',
        zh: '部件',
        icon: 'bujian',
        leftButton: true,
        com: shallowRef(defineAsyncComponent(() => { return import('./esObj/EsWidget.vue') }))
    },
    {
        type: 'esGeoDiv',
        zh: '自定义DIV',
        icon: 'zidingyi2',
        leftButton: false,
        com: shallowRef(defineAsyncComponent(() => { return import('./esObj/EsGeoDiv.vue') }))
    },
    {
        type: 'esPoi3D',
        zh: 'Poi3D',
        icon: 'poi3D',
        leftButton: true,
        com: shallowRef(defineAsyncComponent(() => { return import('./esObj/EsPoi3D.vue') }))
    },
    {
        type: 'esPoi2D',
        zh: 'Poi2D',
        icon: 'poi2D',
        leftButton: false,
        com: shallowRef(defineAsyncComponent(() => { return import('./esObj/EsPoi2D.vue') }))
    },
    {
        type: 'esEntityCluster',
        zh: 'Poi聚合',
        icon: 'juhe',
        leftButton: true,
        com: shallowRef(defineAsyncComponent(() => { return import('./esObj/EsEntityCluster.vue') }))
    }
]
//园区
export const parkObjectList: { type: string, zh: string, icon: string, leftButton: boolean, com: any }[] = [
    {
        type: 'esHuman',
        zh: '人员',
        icon: 'renyuan',
        leftButton: true,
        com: shallowRef(defineAsyncComponent(() => { return import('./esObj/EsHuman.vue') }))
    },
    {
        type: 'esCar',
        zh: '车辆',
        icon: 'cheliang',
        leftButton: false,
        com: shallowRef(defineAsyncComponent(() => { return import('./esObj/EsCar.vue') }))
    },
    {
        type: 'esGltfModel',
        zh: '建筑',
        icon: 'jianzhu',
        leftButton: true,
        com: shallowRef(defineAsyncComponent(() => { return import('./esObj/EsGltfModel.vue') }))
    },
    {
        type: 'esHumanPoi',
        zh: '人员Poi',
        icon: 'renyuanPoi',
        leftButton: false,
        com: shallowRef(defineAsyncComponent(() => { return import('./esObj/EsHumanPoi.vue') }))
    },
]
//特效
export const effectObjectList: { type: string, zh: string, icon: string, leftButton: boolean, com: any }[] = [
    {
        type: 'esPath',
        zh: '路径',
        icon: 'lujingdonghua',
        leftButton: true,
        com: shallowRef(defineAsyncComponent(() => { return import('./esObj/EsPath.vue') }))
    },
    {
        type: 'esApertureEffect',
        zh: '光圈特效',
        icon: 'guangquantexiao',
        leftButton: false,
        com: shallowRef(defineAsyncComponent(() => { return import('./esObj/EsApertureEffect.vue') }))
    },
    {
        type: 'esPolygonFence',
        zh: '电子围栏',
        icon: 'dianziweilan',
        leftButton: true,
        com: shallowRef(defineAsyncComponent(() => { return import('./esObj/EsPolygonFence.vue') }))
    },
    {
        type: 'esPipeFence',
        zh: '管道围栏',
        icon: 'weilan',
        leftButton: false,
        com: shallowRef(defineAsyncComponent(() => { return import('./esObj/EsPipeFence.vue') }))
    },
    {
        type: 'esAlarm',
        zh: '报警',
        icon: 'baojing',
        leftButton: true,
        com: shallowRef(defineAsyncComponent(() => { return import('./esObj/EsAlarm.vue') }))
    },
    {
        type: 'esCameraVisibleRange',
        zh: '摄像头',
        icon: 'shexiangtou',
        leftButton: false,
        com: shallowRef(defineAsyncComponent(() => { return import('./esObj/EsCameraVisibleRange.vue') }))
    },
    {
        type: 'esVideoFusion',
        zh: '视频融合',
        icon: 'shipinronghe',
        leftButton: true,
        com: shallowRef(defineAsyncComponent(() => { return import('./esObj/EsVideoFusion.vue') }))
    },
    {
        type: 'esLocalSkyBox',
        zh: '天空盒',
        icon: 'tiankonghe',
        leftButton: false,
        com: shallowRef(defineAsyncComponent(() => { return import('./esObj/EsLocalSkyBox.vue') }))
    },
    {
        type: 'esPit',
        zh: '坑',
        icon: 'keng1',
        leftButton: true,
        com: shallowRef(defineAsyncComponent(() => { return import('./esObj/EsPit.vue') }))
    },
    {
        type: 'esPipeline',
        zh: '管线',
        icon: 'guanxian',
        leftButton: false,
        com: shallowRef(defineAsyncComponent(() => { return import('./esObj/EsPipeline.vue') }))
    }, {
        type: 'esDynamicWater',
        zh: '局部水面',
        icon: 'jubushuimian',
        leftButton: true,
        com: shallowRef(defineAsyncComponent(() => { return import('./esObj/EsDynamicWater.vue') }))
    },
    {
        type: 'esGeoWater',
        zh: '地理水面',
        icon: 'dilishuimian',
        leftButton: false,
        com: shallowRef(defineAsyncComponent(() => { return import('./esObj/EsGeoWater.vue') }))
    },
    {
        type: 'esHole',
        zh: '组合挖坑',
        icon: 'zuhewakeng',
        leftButton: true,
        com: shallowRef(defineAsyncComponent(() => { return import('./esObj/EsHole.vue') }))
    },
    {
        type: 'esBlastParticleSystem',
        zh: '粒子爆炸',
        icon: 'lizibaozha',
        leftButton: false,
        com: shallowRef(defineAsyncComponent(() => { return import('./esObj/EsBlastParticleSystem.vue') }))
    },
    {
        type: 'esFireParticleSystem',
        zh: '粒子烟火',
        icon: 'liziyanhuo',
        leftButton: true,
        com: shallowRef(defineAsyncComponent(() => { return import('./esObj/EsFireParticleSystem.vue') }))
    },

]
//actor
export const ueObjectList: { type: string, zh: string, icon: string, leftButton: boolean, com: any }[] = [
    {
        type: 'esUnrealActor',
        zh: 'Actor',
        icon: 'actor',
        leftButton: true,
        com: shallowRef(defineAsyncComponent(() => { return import('./esObj/EsUnrealActor.vue') }))
    },
    {
        type: 'esDatasmithRuntimeModel',
        zh: 'Datasmith',
        icon: 'DataMesh',
        leftButton: false,
        com: shallowRef(defineAsyncComponent(() => { return import('./esObj/EsDatasmithRuntimeModel.vue') }))
    }, {
        type: 'esLevelRuntimeModel',
        zh: '关卡包',
        icon: 'guanqiabao',
        leftButton: true,
        com: shallowRef(defineAsyncComponent(() => { return import('./esObj/EsLevelRuntimeModel.vue') }))
    }
]

