import Vue from 'vue'
import App from './App.vue'
import getMyObjectsManager from './getMyProjectManager'

Vue.config.productionTip = false
Vue.prototype.$objm = getMyObjectsManager()

new Vue({
  render: h => h(App),
}).$mount('#app')
