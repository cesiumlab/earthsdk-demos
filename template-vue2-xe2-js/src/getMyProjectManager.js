import MyProjectManager from './MyProjectManager';

const myProjectManager = new MyProjectManager();
window.g_myProjectManager = myProjectManager;
export default function getMyProjectManager() {
    return window.g_myProjectManager;
}
